function addJob() {
    $("#jobs").append('<br clear="left"><br><div class="col-md-5"><input type="text" name="jobs[]" class="form-control flat" placeholder="Occupation / Job Title"></div><div class="col-md-5"><input type="text" name="jobs[]" class="form-control flat" placeholder="Occupation / Job Title"></div><div class="col-md-1"><button type="button" class="btn btn-primary btn-flat" onclick="addJob()"><i class="fa fa-plus"></i></button></div>');
}


$("#program").change(function () {
    if ($(this).val() != '') {
        $("#subprograms").html('');
        $.get('/get/subprograms/' + $(this).val(), function (data, status) {
            if (status == "success") {
                for (var i = 0; i < data.length; i++) {
                    $("#subprograms").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }

});


$("#project_program").change(function () {
    if ($(this).val() != '') {
        $("#project_results").html('<option value="">Choose Here ...</option>');
        $.get('/wda/projects/get/results/' + $(this).val(), function (data, status) {
            if (status == "success") {
                for (var i = 0; i < data.length; i++) {
                    $("#project_results").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }

});

$("#project").change(function () {
    if ($(this).val() != '') {
        $("#project_program").html('<option value="">Choose Here ...</option>');
        $.get('/wda/projects/get/program/' + $(this).val(), function (data, status) {
            if (status == "success") {
                for (var i = 0; i < data.length; i++) {
                    $("#project_program").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }

});


$("#program2").change(function () {
    if ($(this).val() != '') {
        $("#subprograms2").html('<option value="">Choose Here</option>');
        $.get('/get/subprograms/' + $(this).val(), function (data, status) {
            if (status == "success") {
                for (var i = 0; i < data.length; i++) {
                    $("#subprograms2").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }
});

$("#sp_rp").change(function () {
    if ($(this).val() != '') {
        $("#sp_program").html('');
        $.get('/wda/sp/get/programs/' + $(this).val(), function (data, status) {
            if (status == "success") {
                $("#sp_program").append('<option value="">Choose Here</option>');
                for (var i = 0; i < data.length; i++) {
                    $("#sp_program").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }
});

$("#sp_program").change(function () {
    if ($(this).val() != '') {
        $("#outcomes").html('');
        $.get('/wda/sp/get/results/' + $(this).val(), function (data, status) {
            if (status == "success") {
                $("#outcomes").append('<option value="">Choose Here</option>');
                for (var i = 0; i < data.length; i++) {
                    $("#outcomes").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }
});

$("#ap_rp").change(function () {
    if ($(this).val() != '') {
        $("#ap_programs").html('');
        $.get('/wda/ap/get/programs/' + $(this).val(), function (data, status) {
            if (status == "success") {
                $("#ap_programs").append('<option value="">Choose Here</option>');
                for (var i = 0; i < data.length; i++) {
                    $("#ap_programs").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }
});

$("#ap_programs").change(function () {
    if ($(this).val() != '') {
        $("#ap_results").html('');
        $.get('/wda/ap/get/results/' + $(this).val(), function (data, status) {
            if (status == "success") {
                $("#ap_results").append('<option value="">Choose Here</option>');
                for (var i = 0; i < data.length; i++) {
                    $("#ap_results").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    }
});

function addBuilding() {
    $("#addBuilding").append('<div class="col-md-6"><select class="form-control flat" name="infrastructure[]"><option value="">Choose Here</option>' + infra_sec +'</select></div><div class="col-md-5"><input type="number" class="form-control flat" name="quantity[]" placeholder="Quantity"></div><div class="col-md-1"><button type="button" class="btn btn-info" onclick="addBuilding()" ><i class="fa fa-plus"></i></button></div>');
}

function addBuilding2() {
    $("#addBuilding2").append('<div class="col-md-6"><select class="form-control flat" name="infrastructure[]"><option value="">Choose Here</option><option>Classrooms</option><option>Workshops</option><option>Smart Classrooms</option><option>Library</option><option>Stores</option><option>Trainer rooms</option><option>Manager rooms</option><option>Administration rooms</option><option>Multipurpose hall</option><option>Dining hall</option><option>Playground</option><option>Dorms</option><option>Waste Management</option><option>Garden</option><option>Toilet</option></select></div><div class="col-md-5"><input type="number" class="form-control flat" name="quantity[]" placeholder="Quantity"></div><div class="col-md-1"><button type="button" class="btn btn-info" onclick="addBuilding()" ><i class="fa fa-plus"></i></button></div>');
}

$("#province").change(function () {

    var value = $(this).val();

    var kigali = ['Gasabo', 'Kicukiro', 'Nyarugenge'];
    var north = ['Burera', 'Gakenke', 'Gicumbi', 'Musanze', 'Rulindo'];
    var south = ['Gisagara', 'Huye', 'Kamonyi', 'Muhanga', 'Nyamagabe', 'Nyanza', 'Nyaruguru', 'Ruhango'];
    var east = ['Bugesera', 'Gatsibo', 'Kayonza', 'Kirehe', 'Ngoma', 'Nyagatare', 'Rwamagana'];
    var west = ['Karongi', 'Ngororero', 'Nyabihu', 'Nyamasheke', 'Rubavu', 'Rusizi', 'Rutsiro'];

    var provinces = {'kigali': kigali, 'north': north, 'south': south, 'east': east, 'west': west,};

    $("#districts").html('');
    provinces[value].forEach(function (item) {
        $("#districts").append('<option>' + item + '</option>');
    });

});

$("#sector").change(function () {
    let url = $(this).data('url') + "/";
    $.get(url + $(this).val(), function (data, status) {
        if (status == "success") {
            $("#trades").html('<option value="">Choose Here ...</option>');
            for (var i = 0; i < data.length; i++) {
                $("#trades").append('<option value="' + data[i]['id'] + '">' + data[i]['sub_field_name'] + '</option>');
            }
        }
    });
});

$("#trades").change(function () {
    let url = $(this).data('url') + "/";
    $.get(url + $(this).val(), function (data, status) {
        if (status == "success") {
            $("#qua").html('<option value="">Choose Here ...</option>');
            for (var i = 0; i < data.length; i++) {
                if ($.trim(data[i]['uuid']).length > 0)
                    $("#qua").append('<option value="' + data[i]['uuid'] + '">' + data[i]['qualification_title'] + '</option>');
            }
        }
    });
});


$("#sectorDamo").change(function () {
    $.get('/get/subsectors/' + $(this).val(), function (data, status) {
        if (status == "success") {
            $("#sub_sectorsDamo").html('<option value="">Choose Here</option>');
            for (var i = 0; i < data.length; i++) {
                $("#sub_sectorsDamo").append('<option value="' + data[i]['id'] + '">' + data[i]['sub_field_name'] + '</option>');
            }
        }
    });
});


$("#sub_sectorsDamo").change(function () {
    $.get('/get/currqualifications/' + $(this).val(), function (data, status) {
        console.log(data);
        if (status == "success") {
            $("#curr_qualifications2").html('');
            for (var i = 0; i < data.length; i++) {
                $("#curr_qualifications2").append('<option value="' + data[i]['id'] + '">' + data[i]['qualification_title'] + '</option>');
            }
        }
    });
});

$("#sector2").change(function () {
    $.get('/get/subsectors/' + $(this).val(), function (data, status) {

        if (status == "success") {
            $("#sub_sectors2").html('<option value="">Choose Here</option>');
            for (var i = 0; i < data.length; i++) {
                $("#sub_sectors2").append('<option value="' + data[i]['id'] + '">' + data[i]['sub_field_name'] + '</option>');
            }
        }
    });
});

// Programs Offered

$("#sector3").change(function () {
    $.get('/get/subsectors/' + $(this).val(), function (data, status) {
        if (status == "success") {
            $("#sub_sectors3").html('');
            for (var i = 0; i < data.length; i++) {
                $("#sub_sectors3").append('<option value="' + data[i]['id'] + '">' + data[i]['sub_field_name'] + '</option>');
            }
        }
    });
});


$("#sub_sectors2").change(function () {
    $.get('/get/currqualifications/' + $(this).val(), function (data, status) {
        console.log(data);
        if (status == "success") {
            $("#curr_qualifications").html('');
            for (var i = 0; i < data.length; i++) {
                $("#curr_qualifications").append('<option value="' + data[i]['id'] + '">' + data[i]['qualification_title'] + '</option>');
            }
        }
    });
});

$("#open_upload").click(function () {
    $("#upload_file").trigger("click");
});
