const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function isNumber(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
        return false;
    }
    return true;
}

function clock() {
    var currentTime = new Date()
    var hours = currentTime.getHours()
    var minutes = currentTime.getMinutes()
    var seconds = currentTime.getSeconds()

    $("#clock").html(hours + ":" + minutes + ":" + seconds);
}

function addtime(start_nber, level_id) {
    $.get('/new_time/start_nber/'+ start_nber+'/level_id/'+ level_id, function(data) {
        $("#plus_times").append(data);
    });
}

function removeTime(nber) {

    alert(nber);

    if (window.confirm('Are you sure you want to remove this row?')) {
        var nber_rows = document.getElementsByName('row'+nber);
        for (var i = 0; i < nber_rows.length ; i++) {
            document.getElementsByName('row'+nber)[i].style.display = 'none';
            document.getElementsByName('row'+nber)[i].style.visibilty = 'hidden';
        }
    }

    $("#plus"+nber).fadeIn();

    alert(nber);
}

setInterval(clock, 1000);

function toggle(source) {
    checkboxes = document.getElementsByName('std[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }
}

function addCourse() {
    $("#coursesBody").append('<tr><td>'+course_i+'</td><td><input type="text" class="form-control" name="course_name[]" placeholder="Course Name"></td><td><select class="form-control" name="category[]"><option>GENERAL SUBJECT</option><option>CORE SUBJECT</option></select></td><td><select class="form-control" id="teachers" name="teacher[]" style="width: 100%" aria-describedby="basic-addon2"><option value="">Choose Here</option>'+teachers_sec+'</select></td><td><input type="number" class="form-control" name="max_point[]" min="0"  max="500" maxlength="3" style="font-weight: bold;font-size: 18px" ></td><td><select class="form-control" name="term[]"><option value="4">Whole Year</option><option value="1">Term 1</option><option value="2">Term 2</option><option value="3">Term 3</option></select><td style="text-align: center;"><i class="fa fa-plus" style="font-size: 20px;cursor: pointer;margin-top: 5px;" onclick="addCourse()"></i></td></tr>');
    course_i++;
}

function abc() {
    var total = $("#total").val();
    var all_marks = document.getElementsByName('my_new_marks[]');

    if (total == "" )
    {
        for (var i=0;i<all_marks.length;i++)
        {
            all_marks[i].disabled = true;
            document.getElementById('submit').disabled = true;
        }
    }
    else
    {
        for (var i=0;i<all_marks.length;i++)
        {
            all_marks[i].disabled = false;
            $("#submit").attr('disabled', false);
        }
    }
}

function abcUpdate() {
    var total = $("#u_total").val();
    var all_marks = document.getElementsByName('my_marks[]');

    if (total == "" )
    {
        for (var i=0;i<all_marks.length;i++)
        {
            all_marks[i].disabled = true;
            $("#update").attr('disabled', true);
        }
    }
    else
    {
        for (var i=0;i<all_marks.length;i++)
        {
            all_marks[i].disabled = false;
            $("#update").attr('disabled', false);
        }
    }
}


function validateNewMarksTerm() {
    var total = document.getElementById('total').value;
    var btn_status = 'on';

    var all_marks = document.getElementsByName('my_new_cat[]');
    var warn = document.getElementsByName('cat_warning[]');
    var count=0;

    for(var i = 0; i < all_marks.length; i++)
    {
        if (parseFloat(all_marks[i].value)>parseFloat(total))
        {
            all_marks[i].style.color = "red";
            warn[i].style.color = "red";
            warn[i].style.fontWeight = "bold";
            warn[i].innerHTML = "Enter Marks Less Than " + total;
            document.getElementById('submit').disabled = true;
            var_status = 'off';
        }
        else
        {
            all_marks[i].style.color="black";
            warn[i].innerHTML="";

            if(count==all_marks.length-1)
            {
                document.getElementById("submit").disabled=false;
                var_status = 'on';

            }

            else
            {
                document.getElementById("submit").disabled=true;
                var_status = 'off';
            }
            count++;
        }

    }


    all_marks = document.getElementsByName('my_new_ex[]');
    warn = document.getElementsByName('ex_warning[]');
    count=0;

    for(var i = 0; i < all_marks.length; i++)
    {
        if (parseFloat(all_marks[i].value)>parseFloat(total))
        {
            all_marks[i].style.color = "red";
            warn[i].style.color = "red";
            warn[i].style.fontWeight = "bold";
            warn[i].innerHTML = "Enter Marks Less Than " + total;
            document.getElementById('submit').disabled = true;
        }
        else
        {
            all_marks[i].style.color="black";
            warn[i].innerHTML="";



            if(count==all_marks.length-1)
            {
                if (btn_status == 'on') {
                    document.getElementById("submit").disabled=false;
                }

                return true;
            }

            else
            {
                document.getElementById("submit").disabled=true;
            }
            count++;
        }

    }
}

function validateNewMarks() {
    var total = document.getElementById('total').value;
    var all_marks = document.getElementsByName('my_new_marks[]');
    var warn = document.getElementsByName('warning[]');
    var count=0;

    for(var i = 0; i < all_marks.length; i++)
    {
        if (parseFloat(all_marks[i].value)>parseFloat(total))
        {
            all_marks[i].style.color = "red";
            warn[i].style.color = "red";
            warn[i].style.fontWeight = "bold";
            warn[i].innerHTML = "Enter Marks Less Than " + total;
            document.getElementById('submit').disabled = true;
        }
        else
        {
            all_marks[i].style.color="black";
            warn[i].innerHTML="";

            if(count==all_marks.length-1)
            {
                document.getElementById("submit").disabled=false;
                return true;
            }

            else
            {
                document.getElementById("submit").disabled=true;
            }
            count++;
        }
    }
}


function validateUpdatedMarks()
{
    var total = $("#u_total").val();
    var all_marks = document.getElementsByName('my_marks[]');
    var warn = document.getElementsByName('u_warning[]');
    var count=0;

    for(var i = 0; i < all_marks.length; i++)
    {
        if (parseFloat(all_marks[i].value)>parseFloat(total))
        {
            all_marks[i].style.color = "red";
            warn[i].style.color = "red";
            warn[i].style.fontWeight = "bold";
            warn[i].innerHTML = "Enter Marks Less Than " + total;
            document.getElementById('Update').disabled = true;
        }
        else
        {
            all_marks[i].style.color="black";
            warn[i].innerHTML="";

            if(count==all_marks.length-1)
            {
                $("#update").attr('disabled', false);
                return true;
            }

            else
            {
                $("#update").attr('disabled', true);
            }
            count++;
        }
    }
}

function checkRemaining() {
    var all_fees = document.getElementsByName('my_fees[]');
    var all_remaining = document.getElementsByName('my_remaining[]');
    var count=0;

    for(var i = 0; i < all_fees.length; i++) {
        var re = new RegExp(',', 'g');
        var str = all_fees[i].value;
        if (parseFloat(str.replace(re, '')) > parseFloat(all_remaining[i].value) ) {
            all_fees[i].style.color = "red";
            $("#saveFees").attr('disabled', true);
        } else {
            all_fees[i].style.color = "black";

            if(count==all_fees.length-1)
            {
                $("#saveFees").attr('disabled', false);
                return true;
            }

            else
            {
                $("#saveFees").attr('disabled', true);
            }
            count++;
        }
    }
}

$("#province").on('change', function(){

    var kigali = ['Gasabo', 'Kicukiro', 'Nyarugenge'];
    var north  = ['Burera', 'Gakenke', 'Gicumbi', 'Musanze', 'Rulindo'];
    var south  = ['Gisagara','Huye','Kamonyi','Muhanga','Nyamagabe', 'Nyanza','Nyaruguru','Ruhango'];
    var east   = ['Bugesera','Gatsibo','Kayonza','Kirehe','Ngoma','Nyagatare','Rwamagana'];
    var west   = ['Karongi', 'Ngororero', 'Nyabihu','Nyamasheke','Rubavu','Rusizi','Rutsiro'];

    var selectedProvince = $(this).val();

    if (selectedProvince == 'Kigali City') {
        $("#district").html('');
        for (var i = 0; i < kigali.length; i++) {
            $("#district").append("<option>"+kigali[i]+"</option>")
        }
    }
    if (selectedProvince == 'Northern Province') {
        $("#district").html('');
        for (var i = 0; i < north.length; i++) {
            $("#district").append("<option>"+north[i]+"</option>")
        }
    }
    if (selectedProvince == 'Southern Province') {
        $("#district").html('');
        for (var i = 0; i < south.length; i++) {
            $("#district").append("<option>"+south[i]+"</option>")
        }
    }
    if (selectedProvince == 'Eastern Province') {
        $("#district").html('');
        for (var i = 0; i < east.length; i++) {
            $("#district").append("<option>"+east[i]+"</option>")
        }
    }

    if (selectedProvince == 'Western Province') {
        $("#district").html('');
        for (var i = 0; i < west.length; i++) {
            $("#district").append("<option>"+west[i]+"</option>")
        }
    }
});

$('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
});

// Old Password Check

$("#old_pass").on('keyup', function() {
    if ($(this).val().length > 0) {
        $.get('/school/password/change/'+$(this).val(), function(data) {
            if (data == 'true') {
                $("#oldPassMsg").html(' <i class="fa fa-check" style="color: green;font-size: 18px"></i>');
                $("#newPass").attr('disabled', false);
                $("#confPass").attr('disabled', false);
            } else {
                $("#oldPassMsg").html(' <i class="fa fa-close" style="color: #cc0000;font-size: 18px"></i>');
                $("#newPass").attr('disabled', true);
                $("#confPass").attr('disabled', true);
            }

        });
    }
});


// New Password Confirmation
$("#newPass, #confPass").on('keyup', function() {
    var first  = $("#newPass").val();
    var second = $("#confPass").val();
    if (second.length > 0) {
        if (first != second) {
            $("#confPassMsg").html(' <i class="fa fa-close" style="color: #cc0000;font-size: 18px" title="Passwords Dont Match"></i>');
        } else {
            $("#confPassMsg").html(' <i class="fa fa-check" style="color: green;font-size: 18px" title="Passwords Matched"></i>');
        }
    }
});

// Department change in students registration
$("#department").change(function() {
    var dep = $(this).val();
    $("#levels").html('');
    $.get('/school/data/dep/' + dep, function(data) {
        if (data.length == 0) {
            alert('No levels registered in the selected department.')
        } else {
            for (var i = 0; i < data.length; i++) {
                $("#levels").append('<option value="'+data[i].id+'">'+data[i].name+'</option>')
            }
        }

    });
});

// Search

$("#search").on('keyup', function() {
    var keyword = $(this).val();

    if (keyword.length > 1) {
        $(".search").show();
        $.get('/school/student/search/' + keyword, function(data) {
            if (data.length == 0 ) {
                $(".searchBody").html('<tr><td colspan="25">No Result Found For Given Entry...</td></tr>');
            } else {
                for (var i = 0; i < data.length; i++) {
                    $(".searchBody").html('<tr><td></td><td><a href="/student/'+data[i]['reg_no']+'"> '+data[i]['reg_no']+' </a></td><td><a href="/student/'+data[i]['reg_no']+'">'+data[i]['fname']+'</a></td><td><a href="/student/'+data[i]['reg_no']+'">'+data[i]['lname']+'</a></td><td>'+data[i]['level_name']+'</td><td>'+data[i]['gender']+'</td><td></td><td>'+data[i]['byear']+'</td><td>'+data[i]['mode']+'</td><td>'+data[i]['ft_name']+'</td><td>'+data[i]['ft_phone']+'</td><td>'+data[i]['mt_name']+'</td><td>'+data[i]['mt_phone']+'</td><td>'+data[i]['gd_name']+'</td><td>'+data[i]['gd_phone']+'</td><td>'+data[i]['nationality']+'</td><td>'+data[i]['province']+'</td><td>'+data[i]['district']+'</td><td>'+data[i]['sector']+'</td><td>'+data[i]['religion']+'</td><td>'+data[i]['sport']+'</td><td>'+data[i]['orphan']+'</td><td>'+data[i]['sponsor']+'</td></tr>');
                }
            }

        });
    } else {
        $(".searchBody").html('<tr><td colspan="25">Start Typing....</td></tr>');
    }
});

// Add Fault

function addFault() {
    $("#faults").append('<tr><td>'+i+'</td><td><input type="text" class="form-control" name=""></td><td><textarea class="form-control"></textarea></td><td style="width: 90px;font-weight: bold;"> <input type="number" min="0" max="100" class="form-control"  name=""></td><td><textarea class="form-control"></textarea></td><td style="width: 90px;font-weight: bold;"> <input type="number" min="0" max="100" class="form-control"  name=""></td><td><textarea class="form-control"></textarea></td><td style="width: 90px;font-weight: bold;"> <input type="number" min="0" max="100" class="form-control"  name=""></td><th><i class="fa fa-plus" style="margin-top: 10px;cursor: pointer;font-size: 25px" onclick="addFault()"></i></th></tr>');
    i++;
}

// Remove Fault

function show(id) {
    $("#"+id).show();
}

// Hide ID

function hide(id) {
    $("#"+id).hide();
}

// Check if the clicked student is the only one to show the number of times

var checked = 0;
var reg_no = '';
function checkIfOne() {

    checkboxes = document.getElementsByName('std[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        if(checkboxes[i].checked) {
            checked++;
            reg_no = checkboxes[i].value;
        }
    }
}

// Level List in Modal

function levelList(level_id, id) {
    $.get('/school/level/list/'+level_id, function(data) {
        $("#"+id).html('');
        if (data.length == 0) {
            $("#"+id).append('<tr><td colspan="4" style="text-align: center">No Students Registered In This Level</td></tr>')
        } else {
            for (var i = 0; i < data.length; i++) {
                var tr = '<tr><td><input type="checkbox" name="std[]" checked value="'+data[i]['id']+'" id="other_check" onclick="checkIfOne()" ></td><td>' + data[i]['reg_no'] +'</td><td>'+ data[i]['fname']+ ' ' + data[i]['lname'] + ' </td></tr>';
                $("#"+id).append(tr);
                console.log(tr);

            }
            $("#submitP").show();
        }
    });
}

// Record a payment

$("#Fyear, #Fterm, #Flevel").change(function() {
    var year  = $("#Fyear").val();
    var term  = $("#Fterm").val();
    var level_id = $("#Flevel").val();

    if (level_id.length > 0) {
        $("#demo7").html('');
        $.get('/school/level/fees/list/' + level_id + '/year/' + year + '/term/' + term, function(data) {
            if (data.length == 0) {
                $("#demo7").html('<tr><td colspan="7" style="text-align: center">No Students In The Selected Level</td></tr>');
            } else {
                $("#demo7Settings").html('<input type="hidden" name="acad_year" value="'+year+'" ><input type="hidden" name="term" value="'+term+'" >');
                var to_payTotal = 0;
                var paidTotal = 0;
                var remainingTotal = 0;
                for (var i = 0; i < data.length; i++) {
                    $("#demo7").append('<tr><td>'+ (i + 1) +'</td><td>'+data[i]['reg_no']+'<input type="hidden" name="reg_no[]" value="'+data[i]['reg_no']+'"></td><td>'+data[i]['fname']+ ' ' +data[i]['lname']+ '</td><td style="text-align: right">'+numberWithCommas(data[i]['to_pay'])+' Rwf</td><td style="text-align: right">'+numberWithCommas(data[i]['paid'])+' Rwf</td><td style="text-align: right">'+numberWithCommas(data[i]['remaining'])+' Rwf <input type="hidden" name="my_remaining[]" value="'+data[i]['remaining']+'"></td><td><input type="text" id="'+i+'" class="form-control" name="my_fees[]" onKeyup="checkRemaining()" style="width: 150px;border-radius: 0px;float: right;font-weight: bold"></td></tr>');
                    $('#'+i).autoNumeric('init');
                    to_payTotal += data[i]['to_pay'];
                    paidTotal += data[i]['paid'];
                    remainingTotal += data[i]['remaining'];
                }

                $("#demo7").append('<tr><td></td><td></td><th>Total</th><th style="text-align: center">'+numberWithCommas(to_payTotal)+' Rwf</th><th style="text-align: center">'+numberWithCommas(paidTotal)+' Rwf</th><th style="text-align: center">'+numberWithCommas(remainingTotal)+' Rwf</th><td></td></tr>')
            }
        });

    }
});

$("#allParents").click(function(){
    if (this.checked == true) {
        $(".sms-nav").hide();
        $("#smsLevelList").attr('disabled', true);
    } else {
        $(".sms-nav").show();
        $("#smsLevelList").attr('disabled', false);
    }
});

function smsStudentSelect(fname, reg_no) {
    $("#selectedStds").append('<li><span><input type="hidden" name="reg_no[]" value="'+reg_no+'">'+ fname + ' </span></li>');
}

// levle list in sms

$("#smsLevelList").change(function() {
    $("#studentsList").html('');
    $.get('/school/level/list/'+$(this).val(), function(data) {
        $("#studentsList").html('');
        if (data.length == 0) {
            $("#studentsList").append('<li><span style="text-align: center">No Students Registered In This Level</span></li>');
        } else {
            for (var i = 0; i < data.length; i++) {
                var name = data[i]['fname'] +' '+ data[i]['lname'];
                $("#studentsList").append('<li><span onclick="smsStudentSelect(\''+ name + '\', \''+data[i]['reg_no']+'\')">'+ data[i]['fname'] +' '+ data[i]['lname'] + ' ['+ data[i]['ft_phone'] + ', '+ data[i]['mt_phone'] + '] ' + ' </span></li>');
            }
        }
    });
});

$("#levelCourses, #levelAllCoursesTerm, #levelCoursesExam").change(function(){
    var level_id = $(this).val();
    var term     = $("#term").val();

    if (term != undefined) {
        var url = '/school/data/courses/level/' + level_id + '/term/' + term;
    } else {
        var url = '/school/data/courses/level/' + level_id;
    }
    console.log(url);
    $("#myCourses, #myCoursesExam").html('');
    if (level_id.length != 0) {
        $.get(url, function(data) {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $("#myCourses, #myCoursesExam").append('<option value="'+data[i]['id']+'">'+data[i]['name']+'</option>')
                }
            } else {
                $("#myCourses, #myCoursesExam").html('<option value="">No Courses Found </option>');
            }
        });
    }
});


$("#levelAllCourses").change(function(){
    var level_id = $(this).val();

    if (level_id.length != 0) {
        $("#myCourses").html('');
        if ($("#period").val() == 6) {
            $("#myCourses").html('<option value="0">Exam Courses</option>');
        } else {
            $.get('/school/data/allcourses/level/' + level_id, function(data) {
                if (data.length > 0) {
                    $("#myCourses").append('<option value="0">All Courses</option>')
                    for (var i = 0; i < data.length; i++) {
                        $("#myCourses").append('<option value="'+data[i]['id']+'">'+data[i]['name']+'</option>')
                    }
                } else {
                    $("#myCourses").html('<option value="">No Courses Found </option>');
                }
            });
        }
    }
});

$("#levelAllCourses2").change(function(){
    var level_id = $(this).val();

    if (level_id.length != 0) {
        $("#myCourses").html('');
        if ($("#period").val() == 6) {
            $("#myCourses").html('<option value="0">Exam Courses</option>');
        } else {
            $.get('/school/data/allcourses/level/' + level_id, function(data) {
                if (data.length > 0) {

                    for (var i = 0; i < data.length; i++) {
                        $("#myCourses").append('<option value="'+data[i]['id']+'">'+data[i]['name']+'</option>')
                    }
                } else {
                    $("#myCourses").html('<option value="">No Courses Found </option>');
                }
            });
        }
    }
});


$("#period").change(function() {
    var level_id = $("#levelAllCourses").val();

    if (level_id.length != 0) {
        $("#myCourses").html('');
        if ($(this).val() == 6) {
            $("#myCourses").html('<option value="0">Exam Courses</option>');
        } else {
            $.get('/school/data/allcourses/level/' + level_id, function(data) {
                if (data.length > 0) {
                    $("#myCourses").append('<option value="0">All Courses</option>')
                    for (var i = 0; i < data.length; i++) {
                        $("#myCourses").append('<option value="'+data[i]['id']+'">'+data[i]['name']+'</option>')
                    }
                } else {
                    $("#myCourses").html('<option value="">No Courses Found </option>');
                }
            });
        }

    }
});

// Reporting Category Change

$("#category1").change(function() {
    var category = $(this).val();

    $("#values1").html('');

    $.get('/school/category/'+category, function(data) {
        if (data.length == 0) {

        } else {
            $("#values1").append('<option value=0>All Values</option>');
            for (var i = 0; i < data.length; i++) {
                $("#values1").append('<option>'+data[i][category]+'</option>')
            }
        }
    });
});

$("#category2").change(function() {
    var category = $(this).val();

    $("#values2").html('');

    $.get('/school/category/'+category, function(data) {
        if (data.length == 0) {

        } else {
            $("#values2").append('<option value=0>All Values</option>');
            for (var i = 0; i < data.length; i++) {
                $("#values2").append('<option>'+data[i][category]+'</option>')
            }
        }
    });
});

$("#category3").change(function() {
    var category = $(this).val();

    $("#values3").html('');

    $.get('/school/category/'+category, function(data) {
        if (data.length == 0) {

        } else {
            $("#values3").append('<option value=0>All Values</option>');
            for (var i = 0; i < data.length; i++) {
                $("#values3").append('<option>'+data[i][category]+'</option>')
            }
        }
    });
});

$("#category4").change(function() {
    var category = $(this).val();

    $("#values4").html('');

    $.get('/school/category/'+category, function(data) {
        if (data.length == 0) {

        } else {
            $("#values4").append('<option value=0>All Values</option>');
            for (var i = 0; i < data.length; i++) {
                $("#values4").append('<option>'+data[i][category]+'</option>')
            }
        }
    });
});

$("#category5").change(function() {
    var category = $(this).val();

    $("#values5").html('');

    $.get('/school/category/'+category, function(data) {
        if (data.length == 0) {

        } else {
            $("#values5").append('<option value=0>All Values</option>');
            for (var i = 0; i < data.length; i++) {
                $("#values5").append('<option>'+data[i][category]+'</option>')
            }
        }
    });
});

// Fault Change to calculate the number of times

$("#fault").change(function() {
    var fault_id = $(this).val();

    if (checked == 1) {

        $.get('/school/faults/times/reg_no/'+reg_no+'/fault/'+fault_id, function(data) {
            $("#basic-addon2").html('<b>' +data['times'] + ' Time[s] </b>');
            $("#first_times").val(data['times']);
            $("#first_marks").html(data['marks'] + ' Marks <input type="hidden" name="first_marks" value="'+data['marks']+'" >');
        });
    }
    else {
        $.get('/school/faults/marks/' + fault_id, function(data) {
            $("#basic-addon2").html('');
            $("#first_times").val(0);
            $("#first_marks").html(data + ' Marks  <input type="hidden" name="first_marks" value="'+data+'" >');
        });
    }
});

$("#fault2").change(function() {
    var fault_id = $(this).val();
    if (checked == 1) {
        $.get('/school/faults/times/reg_no/'+reg_no+'/fault/'+fault_id, function(data) {
            $("#basic-addon3").html('<b>' +data['times'] + ' Time[s] </b>');
            $("#second_times").val(data['times']);
            $("#second_marks").html(data['marks'] + ' Marks  <input type="hidden" name="second_marks" value="'+data['marks']+'" >');
        });
    }
    else {
        $.get('/school/faults/marks/' + fault_id, function(data) {
            $("#basic-addon3").html('');
            $("#second_times").val(0);
            $("#second_marks").html(data + ' Marks  <input type="hidden" name="second_marks" value="'+data+'" >');
        });
    }
});

$("#fault3").change(function() {
    var fault_id = $(this).val();
    if (checked == 1) {
        $.get('/school/faults/times/reg_no/'+reg_no+'/fault/'+fault_id, function(data) {
            $("#basic-addon4").html('<b>' +data['times'] + ' Time[s] </b>');
            $("#third_times").val(data['times']);
            $("#third_marks").html(data['marks'] + ' Marks  <input type="hidden" name="third_marks" value="'+data['marks']+'" >');
        });
    }
    else {
        $.get('/school/faults/marks/' + fault_id, function(data) {
            $("#basic-addon4").html('');
            $("#third_times").val(0);
            $("#third_marks").html(data + ' Marks  <input type="hidden" name="third_marks" value="'+data+'" >');
        });
    }
});


