
// Old Password Check

$("#old_pass").on('keyup', function() {
    if ($(this).val().length > 0) {
        $.get('/password/change/'+$(this).val(), function(data) {
            if (data == 'true') {
                $("#oldPassMsg").html(' <i class="fa fa-check" style="color: green;font-size: 18px"></i>');
                $("#newPass").attr('disabled', false);
                $("#confPass").attr('disabled', false);
            } else {
                $("#oldPassMsg").html(' <i class="fa fa-close" style="color: #cc0000;font-size: 18px"></i>');
                $("#newPass").attr('disabled', true);
                $("#confPass").attr('disabled', true);
            }

        });
    }
});


// New Password Confirmation
$("#newPass, #confPass").on('keyup', function() {
    var first  = $("#newPass").val();
    var second = $("#confPass").val();
    if (second.length > 0) {
        if (first != second) {
            $("#confPassMsg").html(' <i class="fa fa-close" style="color: #cc0000;font-size: 18px" title="Passwords Dont Match"></i>');
        } else {
            $("#confPassMsg").html(' <i class="fa fa-check" style="color: green;font-size: 18px" title="Passwords Matched"></i>');
        }
    }
});