$(function () {
    $("#province").change(function () {
        var p = $(this).val();
        $.get("/location/d/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#district").html(html);
        });
    });

    $("#district").change(function () {
        var p = $(this).val();
        $.get("/location/s/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#sector").html(html);
        });
    });

    $("#sector").change(function () {
        var p = $(this).val();
        $.get("/location/c/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#cell").html(html);
        });
    });

    $("#cell").change(function () {
        var p = $(this).val();
        $.get("/location/v/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#village").html(html);
        });
    });

    

    var d = new Date();
    var n = d.getFullYear();
    var o = n - 16;
    $(".datepicker").datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        startView: 2,
        endDate: '-16y',
        startDate: '1960-01-01'
    });
    var dyz = $(".datepickerA").data('dayz');
    $(".datepickerA").datepicker({
        minViewMode: 2,
        autoclose: true,
        format: "yyyy",
        endDate: '-' + dyz + 'd',
    }).on("changeYear", function (e) {
        var date = new Date(e.date);
        var g = date.getFullYear();
        var e = $("#examiner").val();
        if ((e == 1 && g == 2017)) {
            $("#aggregates_obtained, #school_attended").attr("readonly", true);
            $("#certificate, [for='certificate'] ").hide();
            // $("#option_offered").select2("enable", true);
        }
        else {
            $("#aggregates_obtained, #school_attended").attr("readonly", false);
            $("#certificate, [for='certificate']").show().attr("required", true);
        }
    });

    // $("* .select2").select2();

    $("#aCollege").change(function () {
        var id = $(this).val();
        getingOPtions(id, "/poly/d/", $("#aDepartment"), 'd');
    });
    $("#aDepartment").change(function () {
        var id = $(this).val();
        var cl = $("#aCollege").val();
        $("#aDepartment").change(function () {
            var id = $(this).val();
            var cl = $("#aCollege").val();
            getingOPtions(cl, "/poly/c/"+id+"/", $("#aProgram"), 'c');
        });(cl, "/poly/c/"+id+"/", $("#aProgram"), 'c');
    });
    $("#aProgram").change(function () {
        var id = $(this).val();
        getingOPtions(id, "/poly/m/", $("#aModule"), 'm');
    });

    function getingOPtions(id, url, elem, val, cl) {
        $.get(url + id, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select ...</option>";
            $.each(obj, function (key, value) {
                var v = "";
                var a = "";
                if (val == 'd')
                    v = value.department_name;
                if (val == 'c') {
                    v = value.program_name;
                    var cd = value.choice_code;
                    var aa = value.study_area;
                    if (cd != undefined){
                        if (cd.length > 0)
                            a = " ( " + cd + " ) ";
                    }
                }
                if (val == "m")
                    v = value.course_unit_name;
                html += "<option value='" + value.id + "'>" + v + a + "</option>";
            });
            elem.html(html);
            console.log(html);
        });
    }

    $("#index_number").on('keyup', function (e) {
        e.preventDefault();
        var token = $(this).data('token');
        var url = $(this).data('url');
        var v = $(this).val();
        var d = $(".datepickerA").val();

        if (d == 2017) {
            $.post(url, {
                '_token': token,
                index: v
            }, function (data) {
                var obj = jQuery.parseJSON(data);
                if (obj.TOT) {
                    $("#aggregates_obtained").val(obj.TOT);
                    $("#school_attended").val(obj.school_name);
                } else
                    $("#aggregates_obtained, #school_attended").val("");
            });
        }else
            $("#aggregates_obtained, #school_attended").val("");

    });
    
});