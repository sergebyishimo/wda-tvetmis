$(function () {

    let p = $("*#nprovince");
    let url = "/location/p/a";
    $.get(url, {}, function (data) {
        var obj = jQuery.parseJSON(data);
        var html = "<option value='' selected disabled>Select Province</option>";
        var def = p.data('province');
        $.each(obj, function (key, value) {
            $.each(value, function (k, v) {
                if (def == v)
                    html += "<option value='" + v + "' selected>" + v + "</option>";
                else
                    html += "<option value='" + v + "'>" + v + "</option>";
            });
        });
        p.html(html);
    });

    $("#nprovince, #anprovince").change(function () {
        var p = $(this).val();
        $.get("/location/d/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#ndistrict").html(html);
        });
    });


    $("#ndistrict").change(function () {
        var p = $(this).val();
        $.get("/location/s/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#nsector").html(html);
        });
    });

    $("#nsector").change(function () {
        var p = $(this).val();
        $.get("/location/c/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#ncell").html(html);
        });
    });

    $("#ncell").change(function () {
        var p = $(this).val();
        $.get("/location/v/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });
            $("#nvillage").html(html);
        });
    });

    /* Search for filters */

    let pf = $("*#fprovince");
    let urlf = "/filter-location/p/a";
    $.get(urlf, {}, function (data) {
        var obj = jQuery.parseJSON(data);

        var html = "<option value='' selected disabled>Select Province</option>";
        var def = p.data('province');
        $.each(obj, function (key, value) {
           /* $.each(value, function (k, v) {*/
                if (def == value)
                    html += "<option value='" + value.province + "' selected>" + value.province +" ( "+ value.total + " )</option>";
                else
                    html += "<option value='" + value.province + "'>" + value.province +" ( "+ value.total + " )</option>";
           /* });*/
        });
        pf.html(html);
    });

    $("#fprovince, #afprovince").change(function () {
        var pf = $(this).val();
        $.get("/filter-location/d/" + pf, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
               /* $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });*/
                html += "<option value='" + value.District + "'>" + value.District +" ( "+ value.total + " )</option>";
            });
            $("#fdistrict").html(html);
        });
    });


    $("#fdistrict").change(function () {
        var pf = $(this).val();
        $.get("/filter-location/s/" + pf, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                /*$.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });*/
                html += "<option value='" + value.sector + "'>" + value.sector +" ( "+ value.total + " )</option>";
            });
            $("#fsector").html(html);
        });
    });

    $("#fsector").change(function () {
        var pf = $(this).val();
        $.get("/filter-location/c/" + pf, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
               /* $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });*/
                html += "<option value='" + value.school_type + "'>" + value.school_type +" ( "+ value.total + " )</option>";

            });
            $("#fcell").html(html);
        });
    });
    $("#fcell").change(function () {
        var p = $(this).val();
        $.get("/filter-location/v/" + p, {}, function (data) {
            var obj = jQuery.parseJSON(data);
            var html = "<option value='' selected disabled>Select..</option>";
            $.each(obj, function (key, value) {
                $.each(value, function (k, v) {
                    html += "<option value='" + v + "'>" + v + "</option>";
                });
            });

        });
    });
});
