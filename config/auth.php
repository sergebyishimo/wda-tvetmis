<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'lecturer' => [
            'driver' => 'session',
            'provider' => 'lecturers',
        ],

        'district' => [
            'driver' => 'session',
            'provider' => 'districts',
        ],

        'admin' => [
            'driver' => 'session',
            'provider' => 'admins',
        ],

        'college' => [
            'driver' => 'session',
            'provider' => 'colleges',
        ],

        'rp' => [
            'driver' => 'session',
            'provider' => 'rps',
        ],

        'wda' => [
            'driver' => 'session',
            'provider' => 'wdas',
        ],

        'examiner' => [
            'driver' => 'session',
            'provider' => 'examiners',
        ],

        'reb' => [
            'driver' => 'session',
            'provider' => 'rebs',
        ],

        'student' => [
            'driver' => 'session',
            'provider' => 'students',
        ],

        'school' => [
            'driver' => 'session',
            'provider' => 'schools',
        ],

        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'lecturers' => [
            'driver' => 'eloquent',
            'model' => App\Lecturer::class,
        ],

        'districts' => [
            'driver' => 'eloquent',
            'model' => App\District::class,
        ],

        'admins' => [
            'driver' => 'eloquent',
            'model' => App\Admin::class,
        ],

        'colleges' => [
            'driver' => 'eloquent',
            'model' => App\College::class,
        ],

        'rps' => [
            'driver' => 'eloquent',
            'model' => App\Rp::class,
        ],

        'wdas' => [
            'driver' => 'eloquent',
            'model' => App\Wda::class,
        ],

        'examiners' => [
            'driver' => 'eloquent',
            'model' => App\Examiner::class,
        ],

        'rebs' => [
            'driver' => 'eloquent',
            'model' => App\Reb::class,
        ],

        'students' => [
            'driver' => 'eloquent',
            'model' => App\Rp\Student::class,
        ],

        'schools' => [
            'driver' => 'eloquent',
            'model' => App\SchoolUser::class,
        ],

        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

    'passwords' => [
        'lecturers' => [
            'provider' => 'lecturers',
            'table' => 'lecturer_password_resets',
            'expire' => 60,
        ],

        'districts' => [
            'provider' => 'districts',
            'table' => 'district_password_resets',
            'expire' => 60,
        ],

        'admins' => [
            'provider' => 'admins',
            'table' => 'admin_password_resets',
            'expire' => 60,
        ],

        'colleges' => [
            'provider' => 'colleges',
            'table' => 'college_password_resets',
            'expire' => 60,
        ],

        'rps' => [
            'provider' => 'rps',
            'table' => 'rp_password_resets',
            'expire' => 60,
        ],

        'wdas' => [
            'provider' => 'wdas',
            'table' => 'wda_password_resets',
            'expire' => 60,
        ],

        'examiners' => [
            'provider' => 'examiners',
            'table' => 'examiner_password_resets',
            'expire' => 60,
        ],

        'rebs' => [
            'provider' => 'rebs',
            'table' => 'reb_password_resets',
            'expire' => 60,
        ],

        'students' => [
            'provider' => 'students',
            'table' => 'student_password_resets',
            'expire' => 60,
        ],

        'schools' => [
            'provider' => 'schools',
            'table' => 'school_password_resets',
            'expire' => 60,
        ],

        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],

];
