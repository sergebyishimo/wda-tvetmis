<?php
return [

    "thresholds" => [
        "pass" => 30,
        "fail" => 10,
    ]
];