<?php

return [

    'weight' => [
        'title' => 1.5,
        'content' => 1,
    ],

    'limit-results' => 1000,

    /**
     *  Enable wildcard after words
     */
    'enable_wildcards' => true,
];
