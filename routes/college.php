<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('college')->user();

    //dd($users);

    $programs_source = college()->programs()->distinct('id')->get();

    $college_programs = App\Model\Rp\CollegeProgram::where('college_id', college()->id)->get();

    $programs = [];
    $programs_statistics = [];
    foreach ($college_programs as $cp) {
        $program_source = App\Model\Rp\RpProgram::find($cp->program_id);
        $programs[] = substr($program_source->program_name, 0, 12) . '...';
        $programs_statistics[] = App\Rp\CollegeStudent::where('college_id', auth()->guard('college')->user()->id)->where('course_id', $cp->program_id)->count();
    }


    //dd($users);

    JavaScript::put([
        'programs' => $programs,
        'programs_statistics' => $programs_statistics
    ]);

    return view('college.home');
})->name('home');

Route::get('/', function () {
    return redirect('/college/home');
});

Route::get('/programs', function () {
    $programs = App\Model\Rp\CollegeProgram::where('college_id', college('college')->id)->get();
    return view('college.programs', compact('programs'));
})->name('programs');

Route::get('/profile', function () {
    return view('rp.profile');
})->name('profile');

Route::get('/registration/enrolled', "CollegeAuth\StudentsController@enrol")->name('students.enrol');
Route::get('/admission/letter/{stdid}', "CollegeAuth\StudentsController@admissionLetter")->name('admission.letter');

Route::namespace('RpAuth')->group(function () {
    Route::group(['prefix' => 'admission'], function () {
        Route::get('/privates/finished', "Admission\PrivateStudentsController@finished")->name('admission.privates.finished');
        Route::get('/privates/rejected', "Admission\PrivateStudentsController@rejected")->name('admission.privates.rejected');
        Route::resource('/privates', "Admission\PrivateStudentsController");
    });
});

Route::post('/profile', "CollegeAuth\CollegeController@updateProfile")->name('update.profile');
Route::get('/department/{id}', "CollegeAuth\CourseController@getDepartment");
Route::get('/registration/not-finished', "CollegeAuth\RegistrationController@notFinished")->name('reg.not-finished');
Route::get('/registration/finished', "CollegeAuth\RegistrationController@finished")->name('reg.finished');
Route::get('/registration/edit/{student_reg}', "CollegeAuth\RegistrationController@edit")->name('registration.edit');
Route::post('/registration/editsave', "CollegeAuth\RegistrationController@editSave")->name('registration.edit.save');
Route::delete('/registration/continuing/delete/{id}', "CollegeAuth\RegistrationController@destroy")->name('reg.delete');
Route::get('/register/students', "CollegeAuth\RegistrationController@registerStudents")->name('reg.new.student');
Route::match(['GET', 'POST'], '/registration/uploads/continuing', "CollegeAuth\RegistrationController@uploadContinuingStudents")->name('reg.uploads.continuing.student');
Route::post('/registration/uploads/references', "CollegeAuth\RegistrationController@references")->name('reg.uploads.references');
Route::get('/registration/uploads/uploaded', "CollegeAuth\RegistrationController@uploaded")->name('reg.uploads.uploaded');
Route::get('/registration/uploads/uploaded/edit/{continuing_id}', "CollegeAuth\RegistrationController@editContinuing");
Route::post('/registration/uploads/uploaded/edit', "CollegeAuth\RegistrationController@editContinuing");
Route::post('/registration/uploads/uploaded', "CollegeAuth\RegistrationController@uploaded")->name('reg.uploads.uploaded.save');
Route::post('/register/students', "CollegeAuth\RegistrationController@registerStoreStudents")->name('reg.store.student');
Route::resource('/registration', "CollegeAuth\RegistrationController")->only([
    'index', 'show'
]);
Route::get('/student/querying', "CollegeAuth\StudentsController@single")->name('student.single');
Route::post('/student/querying', "CollegeAuth\StudentsController@queryExecuting")->name('query.execute');

Route::resource('/payments', "CollegeAuth\PaymentsController")->only([
    'index', 'store'
]);
Route::get('/payments/paid', "CollegeAuth\PaymentsController@finished")->name('reg.paid');
Route::get('/payments/history', "CollegeAuth\PaymentsController@history")->name('payments.history');
Route::get('/payments/category', "CollegeAuth\PaymentsController@category")->name('payments.category');
Route::post('/payments/category', "CollegeAuth\PaymentsController@storeCategory")->name('payments.category.store');
Route::get('/payment/category/{id}/{action}', "CollegeAuth\PaymentsController@categoryDo")->name('payment.category.action');
//Route::get('/payment/paid/outside', "CollegeAuth\PaymentsController@outside")->name('payments.outside');
//Route::post('/payment/paid/outside', "CollegeAuth\PaymentsController@outside")->name('payments.outside');
Route::delete('/payment/paid/outside/{student}', "CollegeAuth\PaymentsController@outside")->name('payments.outside.destroy');
Route::group(['prefix' => 'payments'], function () {
    Route::get('historic', "RpAuth\PaymentsController@getHistoricData")->name('payments.get.historic');
    Route::get('/history', "RpAuth\PaymentsController@history")->name('payments.history');

    Route::get("/filtering", "RpAuth\PaymentsController@filtering")->name('payments.filtering');
    Route::post("/filtering", "RpAuth\PaymentsController@makeFiltering")->name('payments.post.filtering');
});


Route::resource('/courses', "CollegeAuth\CourseController");
Route::resource("/social", "CollegeAuth\SocialController");
Route::get('/get/sponsored/students/', "CollegeAuth\SocialController@getSponsoredStudents")->name('get.sponsored.students');
Route::get('/get/sponsored/students/others', "CollegeAuth\SocialController@getSponsoredOtherStudents")->name('get.sponsored.students.others');

Route::get('/transfer/student', "RpAuth\CollegesController@transferStudents")->name('student.transfer');
Route::get('/coming/transfer/students', "RpAuth\CollegesController@transferComingStudents")->name('student.transfer.coming');
Route::get('/going/transfer/students', "RpAuth\CollegesController@transferGoingStudents")->name('student.transfer.going');
Route::get('/college/student/{pol}/{id}', "RpAuth\CollegesController@student")->name('show.student');
Route::post('/transfer/student', "RpAuth\CollegesController@transferDecision")->name('student.decision.transfer');

Route::get('payment/override', "CollegeAuth\PaymentsController@override")->name('payments.override');
Route::post('payment/override', "CollegeAuth\PaymentsController@overrideStore")->name('payments.override.store');
Route::delete('payment/destroy/{id}', "CollegeAuth\PaymentsController@overrideDestroy")->name('payments.override.destroy');

Route::get('/admitted', "CollegeAuth\StudentsController@admitted")->name('students.admitted');
Route::post('/student', "CollegeAuth\StudentsController@indexRp")->name('students.search');

Route::resource('students', "CollegeAuth\StudentsController");

Route::get('marks/upload', "CollegeAuth\MarksController@upload")->name('marks.upload');
Route::get('marks/other/upload', "CollegeAuth\MarksController@otherUpload")->name('marks.other.upload');
Route::post('marks/upload', "CollegeAuth\MarksController@storeUpload")->name('marks.store.marks');
Route::get('marks/download', "CollegeAuth\MarksController@download")->name('marks.download');

Route::get('marks/transcript', "CollegeAuth\MarksController@getTranscript")->name('marks.processed.transcript');
Route::post('marks/transcript', "CollegeAuth\MarksController@postTranscript")->name('marks.processed.post.transcript');

Route::resource('marks', "CollegeAuth\MarksController");
// Route::get('/marks/view', "CollegeAuth\MarksController@index")->name('marks.view');

Route::get('datatable/object-data/uploadedStudents', 'CollegeAuth\RegistrationController@data');

Route::resource('/departments', 'CollegeAuth\DepartmentController');
Route::resource('/options', 'CollegeAuth\OptionController');
Route::resource('/modules', 'CollegeAuth\ModuleController');
//
Route::get("/lecturers/upload", "CollegeAuth\LecturersController@massUpload")->name('lecturers.upload');
Route::get("/lecturers/assign", "CollegeAuth\LecturersController@courseAssigning")->name('lecturers.assign');
Route::post("/lecturers/assign", "CollegeAuth\LecturersController@courseAssigning")->name('lecturer.assigning');
Route::get("/lecturers/assigned/{lecturer}", "CollegeAuth\LecturersController@getAssignedCourse")->name('lecturers.assigned.course');
Route::resource('/lecturers', "CollegeAuth\LecturersController");

Route::resource('/accommodation', 'ColllegeAuth\AccommodationController');
Route::resource('/suspension', 'CollegeAuth\SuspensionController');

Route::get('/data/{type}/{id}', "CollegeAuth\CollegeController@data");
Route::get('/correct/old/reg', "CollegeAuth\CollegeController@correctOldReg");

Route::resource('/cards', 'StudentsCardsController');