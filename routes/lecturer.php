<?php

Route::group(['namespace' => 'LecturerAuth'], function () {

    Route::get('/home', "GroupingController@home")->name('home');

    Route::resource('grouping', "GroupingController");
    Route::group(['prefix' => 'data'], function () {
        Route::get('/category/{module}', "GroupingController@ajaxMyCategories")->name('ajax.get.categories');
        Route::get('/grouping/{module}', "MarksController@ajaxGetGroup")->name('ajax.get.grouped');
    });

    /**
     * Period Marks
     */

    Route::get('mark/select/class', "MarksController@selectClass")->name('marks.select.class');
    Route::post('mark/select/class', "MarksController@selectClass")->name('marks.post.select.class');
    Route::post('marks/save/marks', "MarksController@saveMarks")->name('marks.save');
    Route::post('marks/save/uploaded/marks', "MarksController@saveUploadedMarks")->name('marks.save.uploaded');

    Route::get('mark/view/class', "MarksController@viewClass")->name('marks.view.class');

    /**
     * Exam Marks
     */

    Route::get('mark/select/exam/class', "MarksController@selectClassForExam")->name('marks.select.class.exam');
    Route::post('marks/save/exam-marks', "MarksController@saveExamMarks")->name('marks.save.exam');
    Route::post('marks/save/uploaded/exam-marks', "MarksController@saveUploadedExamMarks")->name('marks.save.exam.uploaded');

    Route::get('mark/view/class/exam', "MarksController@viewExamClass")->name('marks.view.exam.class');

    Route::post('mark/submit/marks', 'MarksController@submitMarks')->name('marks.submit.marks');

});

/**
 * Geo Location
 */

Route::get("/geo-location", "CollegeAuth\LecturersController@courseAssigning")->name('assign');
Route::post("/geo-location", "CollegeAuth\LecturersController@courseAssigning")->name('assigning');
Route::get("/location/{lecturer}", "CollegeAuth\LecturersController@getAssignedCourse")->name('assigned.course');
Route::resource('/lecturers', "CollegeAuth\LecturersController");

/**
 * Get AJAX data
 */
Route::get('/data/{type}/{id}', "CollegeAuth\CollegeController@data");