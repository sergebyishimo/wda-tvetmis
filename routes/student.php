<?php

Route::get('/', "StudentAuth\StudentController@home");
Route::get('/home', "StudentAuth\StudentController@home")->name('home');

Route::get('/profile', function () {
    return view('rp.profile');
})->name('profile');


Route::group(['middleware' => []], function () {
    Route::get('/registering', 'StudentAuth\RegisterController@showRegistrationForm')->name('registering');
    Route::post("/registering", "StudentAuth\RegisterController@registering")->name("register_applicant");
});

Route::prefix('/new')->group(function () {
    Route::get('/home', function () {
        $user = Auth::guard('student')->user();

        $functionalfees = \App\Model\Rp\FunctionalFees::all();
        $dfs = "first_year";
        $col = "";
        $sponsor = get_my_info('sponsorship_status');
        if ($sponsor == "private")
            $col = "private_sponsored_amount";
        elseif ($sponsor == "government")
            $col = "govt_sponsored_amount";
        if (isContinuingStudent($user->id))
            $dfs = getMyYearOfStudy(true) . "_year";

        $fees = $functionalfees->whereIn('paid_when', [$dfs, 'each_year']);

        $notifications = \App\Notification::all();

        return view('student.profile', compact('fees', 'col', 'notifications'));
    });

    Route::get('/registration', function () {
        return view('student.register');
    });

    Route::get('/transfer', function () {
        return view("student.transfer");
    });

    Route::get('/pending', function () {
        return view('student.pending', compact('invoices'));
    });

    Route::get('/history', function () {
        $payments = \App\Model\Rp\CollegePayment::where('std_id', auth()->guard('student')->user()->id)->orderBy('op_date', 'desc')->paginate(30);
        return view('student.history', compact('payments'));
    });

    Route::get('/request', function () {
        return view('student.showFormCode', compact('feeCategoriesWithFees', 'col'));
    });

});


Route::get('/admission/letter', "StudentAuth\StudentController@admissionLetter")->name('admission.letter');
Route::get('/registering/prof', "StudentAuth\RegisterController@prof")->name('registration.prof');

Route::group(['middleware' => ['enrol_active']], function () {
    Route::resource('enroll', "StudentAuth\EnrolController");
});

Route::get("/transfer/request", "StudentAuth\RegisterController@transferRequest")->name('transfer');
Route::post("/transfer/request", "StudentAuth\RegisterController@transferRequestSubmit")->name('submit.transfer.request');
Route::post("/transfer/canceling", "StudentAuth\RegisterController@cancelTransferRequest")->name('transfer.cancel');

Route::group(['namespace' => 'StudentAuth'], function () {
    Route::group(['prefix' => 'payment'], function () {
        Route::get('code', "PaymentController@showFormCode")->name('payment.code.form');
        Route::post('code', "PaymentController@generateCode")->name('payment.code.generate');
        Route::post('submit-code', "PaymentController@generateCode")->name('payment.code.generate.submit');
        Route::get('pending', "PaymentController@pending")->name('payment.pending');
        Route::delete('pending/{code}', "PaymentController@deletePending")->name('payment.pending.delete');
        Route::get('history', "PaymentController@history")->name('payment.history');

    });
});

Route::group(['middleware' => ['system_active']], function () {
    Route::group(['middleware' => ['payed']], function () {
        Route::post("/registering/update", "StudentAuth\RegisterController@updateRegistering")->name("register_update");
    });
});

Route::get('/instruction/{type}', "RpAuth\InstructionsController@show")->name('instructions');

Route::post('/invoice', "PaymentsController@invoice")->name('print.invoice')->middleware(['payed', 'student']);

Route::post('/info/{name}/print', "StudentAuth\StudentController@print")->name('print');
//Route::post("/info/{name}/update", "StudentAuth\StudentController@update")->middleware('system_active')->name('update');
Route::get("/info/{name}", "StudentAuth\StudentController@edit")->name('editstd');

Route::post('/indexing_std', "HomeController@indexNumber")->name('indexing');

Route::get('/get/{loc}/{id}/{pol}', "HomeController@getDepartment");
Route::get('/get/{loc}/{id}/{pol}', "HomeController@getDepartment");

Route::get('/academic/current', 'StudentAuth\AcademicController@results')->name('academic.results');
Route::resource('/academic', 'StudentAuth\AcademicController');
Route::resource('/accommodation', 'StudentAuth\AccommodationController');
Route::resource('/suspension', 'StudentAuth\SuspensionController');