<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/school/sms/discipline/{school}/{fault}/{marks}/{student}/{token}", "Api\ApiController@sendDisciplineSMS");
Route::get("/school/sms/permission/{school}/{destination}/{reason}/{leaving}/{student}/{token}", "Api\ApiController@sendPermissionSMS");
Route::get("/school/payment/{school}/{student}/{term}/{year}", "Api\ApiController@checkPayment");

Route::group(['namespace' => 'Api\Payments'], function () {
    Route::group(['prefix' => 'invoices'], function (){
        Route::get("/{token}/{student}","CollegePaymentApiController@invoices");
        Route::post("/feedback","CollegePaymentApiController@feedback");
    });
    Route::group(['prefix' => 'invoice'], function (){
        Route::get("/{token}/{code}","CollegePaymentApiController@invoice");
    });
});

Route::get("/exam/results/{token}/{index_number}", "Api\ApiResultController@resultsAll");
