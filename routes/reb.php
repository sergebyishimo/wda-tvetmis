<?php
Route::group(['middleware' => ['isEmailVerified']], function () {

});

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('reb')->user();
    $staff = Auth::guard('reb')->user();
    //dd($users);

    return view('reb.home', compact('staff'));
})->name('home');

Route::get('/staff/my/info', "RebAuth\StaffController@myInfo")->name('staff.update.myinfo');
Route::post('/staff/my/info', "RebAuth\StaffController@updateInfo")->name('staff.update.submit.myinfo');

Route::get("/staff/my/attachment", "RebAuth\StaffController@attachment")->name("staff.get.attachment");
Route::post("/staff/my/attachment", "RebAuth\StaffController@postAttachment")->name("staff.post.attachment");

Route::get("/staff/my/working-experience", "RebAuth\StaffController@workingExperience")->name("staff.get.experience");
Route::post("/staff/my/working-experience", "RebAuth\StaffController@postWorkingExperience")->name("staff.post.experience");

Route::get("/staff/my/teaching-experience", "RebAuth\StaffController@teachingExperience")->name("staff.get.teaching");
Route::post("/staff/my/teaching-experience", "RebAuth\StaffController@postTeachingExperience")->name("staff.post.teaching");

Route::get("/staff/my/marking-background", "RebAuth\StaffController@markingBackground")->name("staff.background.marking");
Route::post("/staff/my/marking-background", "RebAuth\StaffController@postMarkingBackground")->name("staff.post.background.marking");

Route::get("/staff/my/marker-application", "RebAuth\StaffController@markerApplication")->name("teach.marker.application");
Route::post("/staff/my/marker-application", "RebAuth\StaffController@storeMarkerApplication")->name("teach.store.marker.application");

Route::post('/staff/cancel/application', "RebAuth\StaffController@cancelApplication")->name('staff.cancel.application');

Route::post("/staff/delete/backgrounds/{id}/{model}", "RebAuth\StaffController@destroyBackgrounds")->name('staff.background.destroy');

