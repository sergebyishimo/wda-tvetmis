<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/', "HomeController@view")->name('verify');
Route::match(['GET', 'POST'], '/schools/{slugname?}', ['as' => 'home.schools', 'uses' => 'HomeController@schools']);
Route::get('/schoolsmap', ['as' => 'home.schools.map', 'uses' => 'HomeController@schoolsmap']);
Route::match(['POST'], '/findaschool', ['as' => 'home.schools.findaschool', 'uses' => 'HomeController@findaschool']);
Route::post('/sendmessage', ['as' => 'home.schools.sendmessage', 'uses' => 'HomeController@sendmessage']);

//Contact Us

Route::get('/request-account', "HomeController@requestAccount")->name('school.request.account');
Route::post('/request-account', "HomeController@requestAccount")->name('school.request.account');
Route::get('/contact-us', "HomeController@contactUs")->name('contact.us');
Route::post('/contact-us', "HomeController@contactUs")->name('contact.us');;

Route::group(['prefix' => 'school'], function () {
    Route::get('/login', 'SchoolAuth\LoginController@showLoginForm')->name('school.login');
    Route::post('/login', 'SchoolAuth\LoginController@login');
    Route::post('/logout', 'SchoolAuth\LoginController@logout')->name('school.logout');

//  Route::get('/register', 'SchoolAuth\RegisterController@showRegistrationForm')->name('school.register');
//  Route::post('/register', 'SchoolAuth\RegisterController@register');

    Route::post('/password/email', 'SchoolAuth\ForgotPasswordController@sendResetLinkEmail')->name('school.password.request');
    Route::post('/password/reset', 'SchoolAuth\ResetPasswordController@reset')->name('school.password.email');
    Route::get('/password/reset', 'SchoolAuth\ForgotPasswordController@showLinkRequestForm')->name('school.password.reset');
    Route::get('/password/reset/{token}', 'SchoolAuth\ResetPasswordController@showResetForm');
});

Route::get('/dashboard', 'HomeController@dashboard')->name('home.dashboard');
Route::get('/attachments', 'HomeController@attachments')->name('home.attachments');

Route::group(['prefix' => 'student'], function () {
    Route::get('/login', 'StudentAuth\LoginController@showLoginForm')->name('student.login');
    Route::post('/login', 'StudentAuth\LoginController@login');
    Route::post('/logout', 'StudentAuth\LoginController@logout')->name('student.logout');

//  Route::get('/register', 'StudentAuth\RegisterController@showRegistrationForm')->name('student.register');
//    Route::post('/register', 'StudentAuth\RegisterController@register');

    Route::get('/signup/private', "StudentAuth\SignupsController@private")->name('student.signup.private');
    Route::get('/signup/continuing', "StudentAuth\SignupsController@continuing")->name('student.signup.continuing');

    Route::post('/signup/private', "StudentAuth\SignupsController@postPrivate")->name('student.post.signup.private');
    Route::post('/signup/continuing', "StudentAuth\SignupsController@poastContinuing")->name('student.post.signup.continuing');

    Route::post('/password/email', 'StudentAuth\ForgotPasswordController@sendResetLinkEmail')->name('student.password.request');
    Route::post('/password/reset', 'StudentAuth\ResetPasswordController@reset')->name('student.password.email');
    Route::get('/password/reset', 'StudentAuth\ForgotPasswordController@showLinkRequestForm')->name('student.password.reset');
    Route::get('/password/reset/{token}', 'StudentAuth\ResetPasswordController@showResetForm');
});

Route::get("/location/{where}/{value}", "ToAllController@getlocation")->name('boundary');
Route::get("/filter-location/{where}/{value}", "ToAllController@getFilterlocation")->name('boundaryFilter');
//Route::get("/location/{where}/{value}", "HomeController@getlocation")->name('boundary');
Route::get("/poly/{where}/{value}", "HomeController@ajaxGetSchools")->name("schooling");
Route::get("/poly/{where}/{col}/{value}", "HomeController@ajaxGetSchools")->name("schooling");

Route::get('/get/subsectors/{sector_id}', 'HomeController@getSubSectors');
Route::get('/get/currqualifications/{sub_sector_id}', 'HomeController@getCurrQualifications');

Route::get('/api/programs/c/{college_id}/d/{department_id}', 'HomeController@getPrograms');
Route::get('/api/programs/c/{college_id}', 'HomeController@getPrograms');

Route::group(['prefix' => 'admin'], function () {
    //Voyager::routes();
});

Route::group(['prefix' => 'reb'], function () {
    Route::get('/login', 'RebAuth\LoginController@showLoginForm')->name('reb.login');
    Route::post('/login', 'RebAuth\LoginController@login');
    Route::post('/logout', 'RebAuth\LoginController@logout')->name('logout');

    Route::get('/register', 'RebAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'RebAuth\RegisterController@register');
    Route::get('/register/verify', 'RebAuth\RegisterController@verify')->name('verifyEmailLink');
    Route::get('/register/verify/resend', 'RebAuth\RegisterController@showResendVerificationEmailForm')->name('showResendVerificationEmailForm');
    Route::post('/register/verify/resend', 'RebAuth\RegisterController@resendVerificationEmail')->name('resendVerificationEmail');

    Route::post('/password/email', 'RebAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'RebAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'RebAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'RebAuth\ResetPasswordController@showResetForm');

});

Route::group(['prefix' => 'examiner'], function () {
    Route::get('/login', 'ExaminerAuth\LoginController@showLoginForm')->name('examiner.login');
    Route::post('/login', 'ExaminerAuth\LoginController@login');
    Route::post('/logout', 'ExaminerAuth\LoginController@logout')->name('logout');

//    Route::get('/register', 'ExaminerAuth\RegisterController@showRegistrationForm')->name('register');
    Route::group(['middleware' => ['admin']], function () {
        Route::post('/register', 'ExaminerAuth\RegisterController@register')->name('register.examiner');
    });

    Route::post('/password/email', 'ExaminerAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'ExaminerAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'ExaminerAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'ExaminerAuth\ResetPasswordController@showResetForm');
});


Route::group(['prefix' => 'wda'], function () {
    Route::get('/login', 'WdaAuth\LoginController@showLoginForm')->name('wda.login');
    Route::post('/login', 'WdaAuth\LoginController@login');
    Route::post('/logout', 'WdaAuth\LoginController@logout')->name('logout');

//    Route::get('/register', 'WdaAuth\RegisterController@showRegistrationForm')->name('register');
    Route::group(['middleware' => ['admin']], function () {
        Route::post('/register', 'WdaAuth\RegisterController@register')->name('register.wda');
    });

    Route::post('/password/email', 'WdaAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'WdaAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'WdaAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'WdaAuth\ResetPasswordController@showResetForm');
    
});

/**
 *** Curricula Guidance
 **** View Curricula
 */

Route::prefix('curriculum')->group(function () {
    Route::get('/', 'CurriculumController@view')->name('curricula.index');
    Route::get('/trades/{id}', 'CurriculumController@tradesView')->name('curricula.view.trades');
    Route::get('/{id}/view', 'CurriculumController@curriculumView')->name('curriculum.view');
    Route::post('/{id}/except', 'CurriculumController@view');
    Route::get('/{id}/{trade}', 'CurriculumController@view')->name('curricula.view');
});

Route::group(['prefix' => 'rp'], function () {
    Route::get('/login', 'RpAuth\LoginController@showLoginForm')->name('rp.login');
    Route::post('/login', 'RpAuth\LoginController@login');
    Route::post('/logout', 'RpAuth\LoginController@logout')->name('logout');

//  Route::get('/register', 'RpAuth\RegisterController@showRegistrationForm')->name('register');
    Route::group(['middleware' => ['admin_and_rp']], function () {
        Route::post('/register', 'RpAuth\RegisterController@register')->name('register.rp');
    });

    Route::post('/password/email', 'RpAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'RpAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'RpAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'RpAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'college'], function () {
    Route::get('/login', 'CollegeAuth\LoginController@showLoginForm')->name('college.login');
    Route::post('/login', 'CollegeAuth\LoginController@login');
    Route::post('/logout', 'CollegeAuth\LoginController@logout')->name('logout');

//  Route::get('/register', 'CollegeAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'CollegeAuth\RegisterController@register');

    Route::post('/password/email', 'CollegeAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'CollegeAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'CollegeAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'CollegeAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

//    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
//    Route::post('/register', 'AdminAuth\RegisterController@register');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'district'], function () {
    Route::get('/login', 'DistrictAuth\LoginController@showLoginForm')->name('district.login');
    Route::post('/login', 'DistrictAuth\LoginController@login');
    Route::post('/logout', 'DistrictAuth\LoginController@logout')->name('logout');

//  Route::get('/register', 'DistrictAuth\RegisterController@showRegistrationForm')->name('register');
    Route::group(['middleware' => ['wda']], function () {
        Route::post('/register', 'DistrictAuth\RegisterController@register')->name('register.district');
    });

    Route::post('/password/email', 'DistrictAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'DistrictAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'DistrictAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'DistrictAuth\ResetPasswordController@showResetForm');
});
Route::resource('/notifications', 'NotificationController')->only(['index', 'create', 'store']);
Route::get('/notifications/{id}', 'NotificationController@delete')->name('delete.notification');

//logs

Route::group(['middleware' => 'admin'], function () {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

//Route::get('/letter', function () {
//    $registration = \App\FeeCategory::where('category_name', "like", "%" . "registration" . "%")->first();
//    $schoolRFees = \App\FeeCategory::where('category_name', "like", "%" . "school fees" . "%")->first();
//    $rep = "";
//    $totalRegFees = "";
//    $schoolFees = "";
//
//    if ($registration->fees) {
//        $totalRegFees = $registration->fees->pluck('govt_sponsored_amount')->sum() . " Rwf";
//        $x = 0;
//        foreach ($registration->fees as $fee) {
//            if ($x > 0)
//                $rep .= "; ";
//            $rep .= $fee->fee_name . " " . number_format($fee->govt_sponsored_amount) . " Rwf";
//            $x++;
//        }
//    }
//
//    if ($schoolRFees->fees)
//        $schoolFees = number_format($schoolRFees->fees->pluck('govt_sponsored_amount')->sum()) . " Rwf";
//
//    return view('student.admission.letters', compact('registration', 'schoolFees', 'rep', 'totalRegFees', 'schoolFees'));
//});

Route::post('/password/change', function (\Illuminate\Http\Request $request) {
    $guard = null;
    $guards = ['school', 'college', 'wda', 'rp'];
    for ($i = 0; $i < count($guards); $i++) {
        if (auth()->guard($guards[$i])->check()) {
            $guard = $guards[$i];
            break;
        }
    }

    if (\Hash::check($request->old_pass, auth()->guard($guard)->user()->password)) {


        if ($request->new_pass == $request->conf_pass) {

            $user = auth()->guard($guard)->user();
            if ($user->privilege == 2) {
                $school = school(true);
                $school->password = $request->new_pass;
//                    $school->updateVersion = updateVersionColumn('schools');
                $school->save();
            }
            $user->password = bcrypt($request->new_pass);
            if ($guard == 'school') {
                $user->android_pass = sha1(trim($request->new_pass));
                $user->updateVersion = updateVersionColumn(auth()->guard($guard)->user()->getTable());
            }
            $user->save();


            return back();
        } else {
            return redirect()->intended('/home')->withErrors(['status' => '0', 'message' => 'The passwords you entered doesn\'t match!!']);
        }
    } else {
        return redirect()->intended('/home')->withErrors(['status' => '0', 'message' => 'The old password you entered is invalid!! ']);
    }

})->name('change.password');
Route::get('/password/change/{pass}', function ($pass) {

    $guard = null;
    $guards = ['school', 'college', 'wda', 'rp'];
    for ($i = 0; $i < count($guards); $i++) {
        if (auth()->guard($guards[$i])->check()) {
            $guard = $guards[$i];
            break;
        }
    }

    if (\Hash::check($pass, auth()->guard($guard)->user()->password)) {
        return 'true';
    } else {
        return 'false';
    }
})->name('get.password.change');

Route::match(['GET', 'POST'], '/profile', 'ProfileController@profile')->name('get.user.profile');

//Lecturers

Route::group(['prefix' => 'lecturer'], function () {
  Route::get('/login', 'LecturerAuth\LoginController@showLoginForm')->name('lecturer.login');
  Route::post('/login', 'LecturerAuth\LoginController@login');
  Route::post('/logout', 'LecturerAuth\LoginController@logout')->name('logout');

//  Route::get('/register', 'LecturerAuth\RegisterController@showRegistrationForm')->name('register');
//  Route::post('/register', 'LecturerAuth\RegisterController@register');

  Route::post('/password/email', 'LecturerAuth\ForgotPasswordController@sendResetLinkEmail')->name('lecturer.password.request');
  Route::post('/password/reset', 'LecturerAuth\ResetPasswordController@reset')->name('lecturer.password.email');
  Route::get('/password/reset', 'LecturerAuth\ForgotPasswordController@showLinkRequestForm')->name('lecturer.password.reset');
  Route::get('/password/reset/{token}', 'LecturerAuth\ResetPasswordController@showResetForm');
});


Route::get('/results', 'HomeController@results')->name('results');
Route::post('/results', 'HomeController@results')->name('results.view');
Route::get('/results/2018', 'HomeController@resultsAll')->name('results.view.2018');
Route::post('/results/2018', 'HomeController@resultsAll')->name('results.view.2018.view');