<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('rp')->user();

    $departments = App\Model\Rp\RpDepartment::all();

    $deps = [];
    $deps_statistics = [];
    foreach ($departments as $dep) {
        $deps[] = $dep->department_name;
        $deps_statistics[] = App\Rp\CollegeStudent::where('department_id', $dep->id)->count();
    }

    //dd($users);

    JavaScript::put([
        'deps' => $deps,
        'deps_statistics' => $deps_statistics
    ]);

    return view('rp.home');
})->name('home');

Route::get('/profile', function () {
    return view('rp.profile');
})->name('profile');

Route::get('/students', "CollegeAuth\StudentsController@indexRp")->name('students');
Route::post('/students', "CollegeAuth\StudentsController@indexRp")->name('students');

Route::get('/student/querying', "CollegeAuth\StudentsController@single")->name('student.single');
Route::post('/student/querying', "CollegeAuth\StudentsController@queryExecuting")->name('query.execute');

Route::resource('rps', "AdminAuth\RpController");

Route::namespace('RpAuth')->group(function () {
    Route::post('/profile', "RpController@updateProfile")->name('update.profile');

    Route::resource('/colleges', "CollegesController")->names([
        'index' => 'colleges',
        'store' => 'colleges.upload',
        'create' => 'colleges.view.upload'
    ]);

    Route::resource('users', "UsersController");
    Route::resource('instructions', "InstructionsController");

    Route::get('/exceptional/students', "CollegesController@exceptionStudent")->name('exceptional.students');
    Route::post('/exceptional/students', "CollegesController@exceptionStudent")->name('exceptional.students.post');
    Route::delete('/exceptional/students/{id}', "CollegesController@getStudentsShortExpDestroy")->name('exceptional.students.delete');

    Route::group(['prefix' => 'settings'], function () {
        Route::get('system/activation', "SystemSettingsController@setTime")->name('settings.set.time');
        Route::post('system/activation', "SystemSettingsController@postSetTime")->name('settings.post.set.time');

        Route::get('enrol', "SystemSettingsController@enrol")->name('settings.set.enrol');
        Route::post('enrol', "SystemSettingsController@postEnrol")->name('settings.set.enrol');

        Route::get('/academic-year/semester', "SystemSettingsController@semester")->name('settings.set.semester');
        Route::post('/academic-year/semester', "SystemSettingsController@postSemester")->name('settings.set.semester');
    });

    Route::get('college/account/{id}', "CollegesController@account")->name('college.account');

    Route::patch('college/account/{id}', "CollegesController@upadateAccount")->name('college.save.account');

    Route::get('/college/student/{pol}/{id}', "CollegesController@student")->name('college.show.student');

    Route::get('/college/transfer/student', "CollegesController@transferStudents")->name('student.transfer');

    Route::post('/college/transfer/student', "CollegesController@transferDecision")->name('student.decision.transfer');

    Route::get('/emails', "EmailsController@portal")->name('emails');

    Route::post('/emails', "EmailsController@send")->name('emails');

    Route::get('/get/data/all/students', "CollegesController@getDataAllStudents")->name('get.data.students.all');

    Route::resource('/polytechnics', "PolytechnicsController");
    Route::resource('/departments', "RpDepartmentController");
    Route::resource('/programs', "RpProgramController");
    Route::resource('/functionalfees', "FunctionFeeController");
    Route::resource('/functionalfeescategory', "FunctionalFeeCategoryController");
    Route::resource('/managefuncfeecategory', "ManageFeeCategoriesController");
    Route::resource('/manageprograms', "ManageProgramsController");
    Route::get('/manageprograms/{id}/edit/{program_id}', 'ManageProgramsController@edit')->name('manageprograms.edit');
    Route::delete('/manageprograms/{id}/delete/{program_id}', 'ManageProgramsController@destroy')->name('manageprograms.destroy');
    Route::get('/managefuncfeecategory/{id}/edit/{fee_id}', 'ManageFeeCategoriesController@edit')->name('managefuncfeecategory.edit');
    Route::delete('/managefuncfeecategory/{id}/delete/{fee_id}', 'ManageFeeCategoriesController@destroy')->name('managefuncfeecategory.destroy');
    Route::resource('/courseunits', "CourseUnitController");

    Route::get("/letter", "CollegeAuth\CollegeController@letter")->name('letter');
    Route::post("/letter", "CollegeAuth\CollegeController@storeLetter")->name('store.letter');

    Route::group(['prefix' => 'admission'], function () {
        Route::get('/privates/finished', "Admission\PrivateStudentsController@finished")->name('admission.privates.finished');
        Route::resource('/privates', "Admission\PrivateStudentsController");
    });

});

Route::resource("/social", "CollegeAuth\SocialController");
Route::get('/no-prof', "CollegeAuth\StudentsController@getStudentWithoutProf")->name('students.no.prof');
Route::get('/get/sponsored/students/', "CollegeAuth\SocialController@getSponsoredStudents")->name('get.sponsored.students');
Route::get('/get/sponsored/students/others', "CollegeAuth\SocialController@getSponsoredOtherStudents")->name('get.sponsored.students.others');

Route::get('/registration/enrol', "CollegeAuth\StudentsController@enrol")->name('students.enrol');
//

Route::get('/registration/not-finished', "CollegeAuth\RegistrationController@notFinished")->name('reg.not-finished');

Route::get('/registration/finished', "CollegeAuth\RegistrationController@finished")->name('reg.finished');

Route::get('/registration/edit/{student_reg}', "CollegeAuth\RegistrationController@edit")->name('registration.edit');
Route::post('/registration/editsave', "CollegeAuth\RegistrationController@editSave")->name('registration.edit.save');

Route::resource('/registration', "CollegeAuth\RegistrationController")->only([
    'index', 'show', 'edit'
]);

Route::get('/payment/paid/outside', "CollegeAuth\PaymentsController@outside")->name('payments.outside');
Route::post('/payment/paid/outside', "CollegeAuth\PaymentsController@outside")->name('payments.outside');
Route::delete('/payment/paid/outside/{student}', "CollegeAuth\PaymentsController@outside")->name('payments.outside.destroy');
Route::group(['prefix' => 'payments'], function () {
    Route::get('/paid', "CollegeAuth\PaymentsController@finished")->name('reg.paid');
//    Route::get('/get/invoices', "RpAuth\PaymentsController@getInvoices")->name('payments.get.invoices');
    Route::get('/get/master/invoices', "RpAuth\PaymentsController@getMasterInvoices")->name('payments.get.invoices');
    Route::get('/get/detail/{code}', "RpAuth\PaymentsController@getDetailsInvoice")->name('payments.get.invoice.detail');
    Route::get('/invoices', "RpAuth\PaymentsController@invoices")->name('payments.invoices');
    Route::delete('/invoice/{code}', "RpAuth\PaymentsController@getDestroyInvoice")->name('payments.get.invoice.destroy');

    Route::get('historic', "RpAuth\PaymentsController@getHistoricData")->name('payments.get.historic');
    Route::get('/history', "RpAuth\PaymentsController@history")->name('payments.history');

    Route::get("/filtering", "RpAuth\PaymentsController@filtering")->name('payments.filtering');
    Route::post("/filtering", "RpAuth\PaymentsController@makeFiltering")->name('payments.post.filtering');

    Route::get('/make/manually', "RpAuth\PaymentsController@manually")->name('payments.manually');
    Route::post('/make/manually', "RpAuth\PaymentsController@manuallyStore")->name('payments.manually.post');
    Route::delete('/make/manually/{id}', "RpAuth\PaymentsController@revertManually")->name('payments.manually.revert');

});

Route::group(['prefix' => 'ajax'], function () {
    Route::get('/get/short/students', "RpAuth\AjaxController@getStudentsShort")->name('ajax.get.short.students');
    Route::post('/get/short/students', "RpAuth\AjaxController@getStudentsShortExp")->name('ajax.get.short.students.exp');
    Route::get('/get/payment/category', "RpAuth\AjaxController@getPaymentCategories")->name('ajax.get.payment.category');
});

Route::get('/data/{type}/{id}/{college}', "RpAuth\CollegesController@data");