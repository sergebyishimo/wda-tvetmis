<?php
Route::get('/', function () {
    return redirect('/school/home');
});

Route::get('/home', "SchoolAuth\SchoolController@home")->name('home');

/**
 * School Normal Settings
 */

Route::post('/settings', "SchoolAuth\SchoolController@settingsUpdate")->name('save.settings');
Route::post('/password/change', "SchoolAuth\SchoolController@passwordChange")->name('change.password');
Route::get('/password/change/{pass}', "SchoolAuth\SchoolController@passwordCheck")->name('get.password.change');
Route::get('/info', "SchoolAuth\SchoolController@getinfo")->name('get.info');
Route::post('/info', "SchoolAuth\SchoolController@updateInfo")->name('post.info');

/**
 * Staff Section
 */

Route::resource("/staff", "SchoolAuth\StaffController")->names([
    'index' => 'staff.index'
]);

Route::get('/staff/my/info', "SchoolAuth\StaffController@myInfo")->name('staff.update.myinfo');
Route::post('/staff/my/info', "SchoolAuth\StaffController@updateInfo")->name('staff.update.submit.myinfo');
Route::get('/staff/my/profile', "SchoolAuth\StaffController@profile")->name('staff.profile');

Route::get('/staff/my/info/family', "SchoolAuth\StaffController@familyInfo")->name('staff.family.info');
Route::post('/staff/my/info/family', "SchoolAuth\StaffController@familyInfo")->name('staff.family.info');

Route::get('/staff/my/info/qualification', "SchoolAuth\StaffController@staffQualification")->name('staff.qualification');
Route::post('/staff/my/info/qualification', "SchoolAuth\StaffController@staffQualification")->name('staff.qualification');

Route::get('/staff/my/info/language-computer', "SchoolAuth\StaffController@languageAndComputer")->name('staff.lang.pc');
Route::post('/staff/my/info/language-computer', "SchoolAuth\StaffController@languageAndComputer")->name('staff.lang.pc');

Route::get('/staff/my/info/internship-program', "SchoolAuth\StaffController@internshipProgram")->name('staff.intern');
Route::post('/staff/my/info/internship-program', "SchoolAuth\StaffController@internshipProgram")->name('staff.intern');

Route::get('/staff/my/info/medical-history', "SchoolAuth\StaffController@staffMedical")->name('staff.medical');
Route::post('/staff/my/info/medical-history', "SchoolAuth\StaffController@staffMedical")->name('staff.medical');

Route::get('/staff/my/info/training-reference', "SchoolAuth\StaffController@trainingReference")->name('staff.training');
Route::post('/staff/my/info/training-reference', "SchoolAuth\StaffController@trainingReference")->name('staff.training');

Route::get("/staff/my/attachment", "SchoolAuth\StaffController@attachment")->name("staff.get.attachment");
Route::post("/staff/my/attachment", "SchoolAuth\StaffController@postAttachment")->name("staff.post.attachment");

Route::get("/staff/my/working-experience", "SchoolAuth\StaffController@workingExperience")->name("staff.get.experience");
Route::post("/staff/my/working-experience", "SchoolAuth\StaffController@postWorkingExperience")->name("staff.post.experience");

Route::get("/staff/my/teaching-experience", "SchoolAuth\StaffController@teachingExperience")->name("staff.get.teaching");
Route::post("/staff/my/teaching-experience", "SchoolAuth\StaffController@postTeachingExperience")->name("staff.post.teaching");

Route::get("/staff/my/marking-background", "SchoolAuth\StaffController@markingBackground")->name("staff.background.marking");
Route::post("/staff/my/marking-background", "SchoolAuth\StaffController@postMarkingBackground")->name("staff.post.background.marking");

Route::get("/staff/my/assessor-background", "SchoolAuth\StaffController@assessorBackground")->name("staff.background.assessor");
Route::post("/staff/my/assessor-background", "SchoolAuth\StaffController@postAssessorBackground")->name("staff.post.background.assessor");

Route::get("/staff/my/marker-application", "SchoolAuth\StaffController@markerApplication")->name("teach.marker.application");
Route::post("/staff/my/marker-application", "SchoolAuth\StaffController@storeMarkerApplication")->name("teach.store.marker.application");

Route::get("/staff/my/assessor-application", "SchoolAuth\StaffController@assessorApplication")->name("teach.assessor.application");
Route::post("/staff/my/assessor-application", "SchoolAuth\StaffController@storeAssessorApplication")->name("teach.store.assessor.application");

Route::post('/staff/cancel/application', "SchoolAuth\StaffController@cancelApplication")->name('staff.cancel.application');

Route::post("/staff/delete/backgrounds/{id}/{model}", "SchoolAuth\StaffController@destroyBackgrounds")->name('staff.background.destroy');

/**
 * Class Section
 */

Route::resource('/class', "SchoolAuth\ClassController");
Route::get('/rtqf', "SchoolAuth\ClassController@getRtqf")->name('get.levels');
Route::post('/department', "SchoolAuth\ClassController@createDepartment")->name('store.department');
Route::delete('/class/dep/{id}', "SchoolAuth\ClassController@destroyDepartment")->name('destroy.department');
Route::patch('/class/dep/{id}/students', "SchoolAuth\ClassController@updateStudentsCapacity")->name('updade.students.capacity.department');
Route::patch('/class/dep/{id}/rooms', "SchoolAuth\ClassController@updateRoomsCapacity")->name('updade.rooms.capacity.department');
Route::get('/level/list/{level}', "SchoolAuth\ClassController@getStudents");

/**
 * Course Section
 */

Route::resource('/course', "SchoolAuth\CourseController");
Route::post('/courses-list', "SchoolAuth\CourseController@show")->name('course.listing');
Route::get('/levels', "SchoolAuth\CourseController@getLevels")->name('get.course.levels');

/**
 * Student Section
 */

Route::resource('/students', "SchoolAuth\StudentsController");
Route::get("/upload/pictures", "SchoolAuth\StudentsController@uploadPictures")->name("students.upload.pictures");
Route::post("/upload/pictures", "SchoolAuth\StudentsController@uploadPicturesStore")->name("students.upload.pictures.store");
Route::get("/upload/students", "SchoolAuth\StudentsController@uploadStudents")->name("students.upload");
Route::post("/upload/students", "SchoolAuth\StudentsController@uploadStudentsStore")->name("students.upload.store");

Route::post('/students/class', "SchoolAuth\StudentsController@studentList")->name('students.view.class');
Route::post('/parent/class', "SchoolAuth\StudentsController@studentListP")->name('p.students.view.class');

Route::post('/student/assign/card', "SchoolAuth\StudentsController@assignCard")->name('students.assign.card');
Route::post('/parent/assign/card', "SchoolAuth\StudentsController@assignCard")->name('parents.assign.card');
Route::post('/student/temp-card/{who}', "SchoolAuth\StudentsController@getNewCard")->name('students.get.card');
Route::get('/student/remove/{id}/{who}', "SchoolAuth\StudentsController@removeCard")->name('students.remove.card');
Route::post('/student/truncate/temp-card/{who}', "SchoolAuth\StudentsController@truncateTempCard")->name('students.delete.card');

/**
 * Fees Section
 */

Route::resource('/fees', "SchoolAuth\FeesController")->except(['update']);
Route::post('/fees/update', "SchoolAuth\FeesController@update")->name('fees.update');
Route::post('/fees/updating', "SchoolAuth\FeesController@updating")->name('fees.updating');
Route::get('/fees/payments', function () {
    return abort(404);
});
Route::post('/fees/payments', "SchoolAuth\FeesController@getPayments")->name('fees.payment');
Route::post('/fees/update/payments', "SchoolAuth\FeesController@getPayments")->name('fees.update.payment');
Route::post('/fees/additional', "SchoolAuth\FeesController@feesAdditional")->name('fees.additional');
/**
 * Discipline Section
 */

Route::resource('/discipline', "SchoolAuth\DisciplineController")->except(['update', 'view']);
Route::post('/discipline/view', "SchoolAuth\DisciplineController@show")->name('discipline.view');

/**
 * Permissions Section
 */

Route::resource('/permission', "SchoolAuth\SchoolPermissionController")->except(['update', 'view', 'create', 'destroy']);
Route::post('/permission/verify', "SchoolAuth\SchoolPermissionController@create")->name('permission.verify');
Route::get('/permission/verify/{id}', "SchoolAuth\SchoolPermissionController@destroy")->name('permission.destroy');
/**
 * Marks Section
 */

Route::get('/marks/', "SchoolAuth\MarksController@index")->name('marks.index');

Route::get('/data/courses/level/{id}', 'SchoolAuth\MarksController@courses');
Route::get('/data/courses/level/{level_id}/term/{term}', 'SchoolAuth\MarksController@courses');
Route::get('/data/allcourses/level/{id}', 'SchoolAuth\MarksController@allCourses');

Route::post('/marks/entry/periodic', 'SchoolAuth\MarksController@period')->name('marks.period.entry');
Route::post('/marks/entry/periodic/save', 'SchoolAuth\MarksController@periodSave')->name('marks.entry.period');
Route::post('/marks/entry/periodic/update', 'SchoolAuth\MarksController@periodicMarksUpdate')->name('marks.entry.period.update');

Route::get('/exam/marks', 'SchoolAuth\MarksController@examMarks')->name('marks.get.exam');
Route::post('/exam/marks', 'SchoolAuth\MarksController@examMarks')->name('marks.view.exam');
Route::post('/exam', 'SchoolAuth\MarksController@examMarksEntry')->name('marks.save.exam');
Route::post('/exam/update', 'SchoolAuth\MarksController@examMarksUpdate')->name('marks.update.exam');

Route::get('/marks/term/', 'SchoolAuth\MarksController@termEntry');
Route::post('/marks/term/entry', 'SchoolAuth\MarksController@termEntry')->name('marks.term.period.exam');
Route::get('/marks/upload/file', 'SchoolAuth\MarksController@uploadView')->name('marks.file.exam');
Route::post('/marks/upload', 'SchoolAuth\MarksController@download')->name('download.marks.excel');
Route::post('/marks/upload/file', 'SchoolAuth\MarksController@upload')->name('uploading.marks');

Route::get('/marks/reports', 'SchoolAuth\MarksController@report');
Route::post('/marks/reports', 'SchoolAuth\MarksController@report')->name('reporting.marks');
Route::get('/marks/report', 'SchoolAuth\MarksController@studentReport');
Route::get('/marks/level/report', 'SchoolAuth\MarksController@levelReport');
Route::get('/marks/downloadreport', 'SchoolAuth\MarksController@downloadReport');

Route::get('/periodic/results', 'SchoolAuth\MarksController@periodicResults');
Route::get('/periodic/results?download', 'SchoolAuth\PdfGenerateController@periodicResults');
Route::post('/periodic/results', 'SchoolAuth\MarksController@periodicResults')->name("marks.periodic.results");
Route::get('/periodic/results/print/', 'SchoolAuth\MarksController@periodicResults');

Route::get('generate-pdf', 'SchoolAuth\PdfGenerateController@pdfview')->name('generate-pdf');

/**
 * SMS Section
 */

Route::resource('/sms', "SchoolAuth\SMSController");
Route::get("/sms/send/{to}", "SchoolAuth\SMSController@send")->name('sms.send');

/**
 * Attendance Section
 */

Route::resource("/attendance", "SchoolAuth\AttendanceController")->only(['index']);
Route::post('/attendance', "SchoolAuth\AttendanceController@postCourse")->name('attendance.post.course');

Route::get("/attendance/in-out", "SchoolAuth\AttendanceController@inout")->name('attendance.in_out');
Route::post("/attendance/in-out", "SchoolAuth\AttendanceController@postInOut")->name('attendance.post.in_out');

Route::get("/attendance/event", "SchoolAuth\AttendanceController@event")->name('attendance.event');
Route::post("/attendance/event", "SchoolAuth\AttendanceController@postEvent")->name('attendance.post.event');

Route::get("/attendance/staff", "SchoolAuth\AttendanceController@staff")->name('attendance.staff');
Route::post("/attendance/staff", "SchoolAuth\AttendanceController@postStaff")->name('attendance.post.staff');

Route::get("/attendance/visiting", "SchoolAuth\AttendanceController@visiting")->name('attendance.visiting');
Route::post("/attendance/visiting", "SchoolAuth\AttendanceController@postVisiting")->name('attendance.post.visiting');

/**
 * Accreditation Section
 */

Route::namespace('SchoolAuth')->group(function () {

    /** QM Manual **/

    Route::resource('/procedures', "ProceduresController")->only(['index']);
    Route::resource('/responsibilities', "ResponsibilitiesController")->only(['index']);
    Route::resource('/accreditation-types', "AccreditationTypesController")->only(['index']);
    Route::resource('/timelines', "TimelinesController")->only(['index']);

    Route::get('/accreditation/view', 'AccrController@view')->name('accr');
    Route::get('/accreditation/view/{application_id}', 'AccrController@viewMore')->name('accr.view');
    Route::get('/accreditation/download/attachments/{application_id}', 'AccrController@download')->name('accr.download');
    Route::get('/accreditation/print/{applicationId}', 'AccrController@toPrint')->name('accr.print');
    /**
     * School Assessment
     * School Level
     */

    Route::get('/accreditation/my-assessment', "AccrController@assessmentView")->name('myAssessment');
    Route::get('/accreditation/my-assessment/{fun}', "AccrController@assessmentViewSchool")->name('view.myAssessment');
    Route::post('/accreditation/my-assessment/{fun}', "AccrController@printAssessmentViewSchool")->name('print.view.myAssessment');


    /** QM SelfAssessment **/

    Route::get('/accreditation/input', 'AccrController@input')->name('accr.input');
    Route::post('/accreditation/input/save', 'AccrController@save')->name('accr.input.save');
    Route::delete('/accreditation/input/save', 'AccrController@save')->name('accr.input.delete');
    Route::get('/accreditation/input/{object}/edit/{obj_id}', 'AccrController@schoolInput')->name('accr.input.edit');
    Route::post('/accreditation/input/confirm', 'AccrController@confirm')->name('accr.confirm');

    Route::post('/accreditation/confirm', 'AccrController@confirmationSubmit')->name('confirm.submit');

    Route::get('/accreditation/process', 'AccrController@process')->name('accr.process');
    Route::post('/accreditation/process/save/', 'AccrController@process')->name('accr.save.process');

    Route::get('/accreditation/output', 'AccrController@output')->name('accr.output');
    Route::post('/accreditation/output', 'AccrController@output');

    /** QM Application **/

    Route::get('/accreditation/school-applications', 'AccrController@schoolInput')->name('accr.application');
    Route::get('/accreditation/school-applications/view', 'AccrController@view')->name('accr.application.view');


});

/**
 * Requests Section
 */

Route::resource('/requests', "SchoolAuth\SchoolRequestController");


/**
 * Desks Section
 */

Route::resource('/desks', "SchoolAuth\SchoolDeskController");


/**
 * School Activity Report Section
 */

Route::resource('/activityreports', "SchoolAuth\SchoolActivityReportController");

/**
 * School Workshops Section
 */

Route::resource('/workshops', "SchoolAuth\SchoolWorkshopController");

/**
 * Incubation Center Section
 */

Route::resource('/incubationcenters', "SchoolAuth\IncubationCenterController");

/**
 * Incubation Center Section
 */

Route::resource('/productionunit', "SchoolAuth\ProductionUnitController");


/**
 * Incubation Center Section
 */

Route::resource('/otherinfrastructure', "SchoolAuth\OtherInfrastructureController");


/**
 * admissions  Section
 */

Route::resource('/admissions', "SchoolAuth\SchoolAdmissionController");


/**
 * shortcourses  Section
 */

Route::resource('/shortcourses', "SchoolAuth\SchoolShortcourseController");

/**
 * announcements  Section
 */

Route::resource('/announcements', "SchoolAuth\SchoolAnnouncementController");


/**
 * costoftrainings  Section
 */

Route::resource('/costoftrainings', "SchoolAuth\CostOfTrainingController");

Route::prefix('get')->group(function () {
    Route::get('/trades/{sector_id}', 'WdaAuth\CurriculumController@getTrades');
    Route::get('/quafication/{trade_id}', 'WdaAuth\CurriculumController@getQualifications');
    Route::get('/levels', 'WdaAuth\CurriculumController@getLevels')->name('get.levels');
    Route::get('/getdept', 'WdaAuth\CurriculumController@getDept')->name('get.dept');
});

Route::resource('/gallery', "SchoolAuth\SchoolGalleryController");