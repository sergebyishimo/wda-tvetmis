<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('district')->user();

    //dd($users);

    return view('district.home');
});

Route::namespace('DistrictAuth')->group(function () {
    Route::get('/home', "HomeController@index")->name('home');
    Route::get('/schools', 'SchoolController@view')->name('schools');
    Route::get('/school/view/{id}', 'SchoolController@viewDetails')->name('view.school');
    Route::get('/school/{id}', 'SchoolController@edit')->name('edit.school');

    Route::get('/schools/internet/', 'SchoolController@internet')->name('school.internet');
    Route::get('/schools/electricity/', 'SchoolController@electricity')->name('school.electricity');
    Route::get('/schools/water/', 'SchoolController@water')->name('school.water');

    Route::get('/accreditation/school/assessment', "ProvisionalAQAController@assessmentView")->name('school.assessment');
    Route::get('/accreditation/school/assessment/{fun}/{school}', "ProvisionalAQAController@assessmentViewSchool")->name('view.school.assessment');
    Route::post('/accreditation/school/assessment/{fun}/{school}', "ProvisionalAQAController@storeAssessmentSchool")->name('store.school.assessment');

    Route::get('/accreditation', 'ProvisionalAQAController@view')->name('accr.application');
    Route::get('/accreditation/view/{application_id}', 'ProvisionalAQAController@viewMore')->name('view.accr.application');
    Route::post('/accreditation/view/{application_id}', 'ProvisionalAQAController@viewMore')->name('view.accr.application');
    Route::post('/accreditation/save', 'ProvisionalAQAController@saveData')->name('accr.application.save');

});