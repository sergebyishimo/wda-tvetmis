<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('examiner')->user();

    //dd($users);

    return view('examiner.home');
})->name('home');

Route::get('/', function (){
    return redirect('examiner/home');
});

Route::get('/application-activation', "ExaminserAuth\AppController@activation")->name('activation');
Route::post('/application-activation', "ExaminserAuth\AppController@updateActivation")->name('update.activation');

Route::get('/marking-applicants', "ExaminserAuth\AppController@applicantMarking")->name('applicant.marking');

Route::get('/assessing-applicants', "ExaminserAuth\AppController@applicantAssessing")->name('applicant.assessing');

Route::get('/reb-applicants', "ExaminserAuth\AppController@applicantReb")->name('applicant.reb');

Route::get('/datatable/object-data/{type}', 'ExaminserAuth\AppController@data');

Route::get('read/staff/{id}', "ExaminserAuth\AppController@readStaff")->name('read.staff');

