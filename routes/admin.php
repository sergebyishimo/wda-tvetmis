<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.home');
})->name('home');

Route::namespace('AdminAuth')->group(function (){
    Route::prefix('users')->group(function (){
        Route::resource('wda', "WdaController");
        Route::resource('rp', "RpController");
        Route::resource('college', "CollegeController");
        Route::resource('school', "SchoolsController");
        Route::resource('examiner', "ExaminerController");
    });
});

