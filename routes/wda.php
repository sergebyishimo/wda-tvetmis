<?php

Route::get('/home', "WdaAuth\HomeController@index")->name('home');
Route::group(['prefix' => 'ajax'], function (){
    Route::namespace('WdaAuth')->group(function () {
        Route::get('schools', 'SchoolController@ajaxData')->name('ajax.get.schools');
        Route::get('quality_area', 'FinalAQAController@ajaxDataQualityArea')->name('ajax.get.quality.area');
        Route::get('school_assessment_status', 'FinalAQAController@schoolAssessmentStatus')->name('ajax.get.assessment.status');
    });
});
Route::namespace('WdaAuth')->group(function () {
    /**
     * School Management
     **/

    /**
     *** Manage Schools
     */
    Route::get('/school', 'SchoolController@view')->name('schools');
    Route::get('/school/editAllSchools', 'SchoolController@editAllSchools')->name('editAllSchools');
    Route::post('/school/editAllSchools', 'SchoolController@editAllSchools')->name('editAllSchools');
    Route::post('/school/editAllSchoolsSave', 'SchoolController@editAllSchoolsSave')->name('editAllSchoolsSave');
    Route::get('/school/create', "SchoolController@createSchool")->name("school.create");
    Route::post('/school/upload', "SchoolController@uploadSchools")->name("school.upload");
    Route::post('/school/create', 'SchoolController@save')->name('store.school');
    Route::delete('/school', 'SchoolController@delete')->name('deslete.school');
    Route::get('/school/view/{id}', 'SchoolController@viewDetails')->name('view.school');
    Route::delete('/school/view/{id}', 'SchoolController@deleteSchoolDepartments')->name('school.delete.department');
    Route::post('/school/view/staff/upload', 'SchoolController@uploadStaff')->name('view.school.staff.upload');
    Route::get('/school/view/{id}/staff/{staffid}', 'SchoolController@ViewStaffProfile')->name('view.school.staff.profile');
    Route::get('/school/archive', "SchoolController@archive")->name('school.archive');
    Route::post('/school/archive', "SchoolController@archive")->name('school.restore');
    Route::get('/school/{id}', 'SchoolController@edit')->name('edit.school');
    Route::delete('school/students/delete', 'SchoolController@deleteStudents')->name('school.students.delete');
    Route::delete('/school/students/delete/all', 'SchoolController@deleteAllStudents')->name('schools.students.delete.all');
    Route::delete('/school/graduates/delete/all', 'SchoolController@deleteAllGraduates')->name('schools.graduates.delete.all');
    Route::delete('school/staffs/delete', 'SchoolController@deleteStaffs')->name('school.staff.delete');
    Route::get('/schools/internet/', 'SchoolController@internet')->name('school.internet');
    Route::get('/schools/electricity/', 'SchoolController@electricity')->name('school.electricity');
    Route::get('/schools/water/', 'SchoolController@water')->name('school.water');

    Route::group(['prefix' => 'search'], function (){
        Route::get('/', "SearchingController@index")->name('search.index');
        Route::post('/', "SearchingController@index")->name('search.index');
        Route::get('/box/{model}', "SearchingController@searchBased")->name('search.based.model');
        Route::get('/box/{model}/{cols}', "SearchingController@searchBased")->name('search.based.columns');
        Route::get('/box/{model}/{cols}/{value}', "SearchingController@searchBased")->name('search.based.value');
        Route::get('/box/{model}/{cols}/{value}/{cond}', "SearchingController@searchBased")->name('search.based.condition');
    });
    Route::group(['prefix' => 'filter'], function (){
        Route::get('/', "SearchingController@filter")->name('search.filter');
      });

    Route::get('/staffs', "SchoolController@staffsList")->name('staff.list');
    Route::get('/staffs/summary', "SchoolController@staffsSummary")->name('staff.summary');
    Route::get('/staffs/upload', "SchoolController@staffsUpload")->name('staff.upload');
    Route::post('/staffs/upload', "SchoolController@staffsUploadSave")->name('staff.upload.save');

    //Datatable
    Route::get('/datatable/object-data/{type}', 'SchoolController@data')->name('get.objectdata');

//    Route::get('', "");

    /**
     * Manage Qualification Based on Schools
     */

    Route::get("/school/assign/qualification", "SchoolController@schoolQualification")->name('school.assign.qualification');
    Route::post("/school/assign/qualification", "SchoolController@schoolDepartment")->name("school.save.department");

    /**
     **** School Assessment
     *
     ****** School Applications
     */

    Route::get('/accreditation', 'FinalAQAController@view')->name('accr.application');
    Route::get('/accreditation/view/{application_id}/{school}', 'FinalAQAController@viewMore')->name('view.accr.application');
    Route::post('/accreditation/view/{application_id}', 'FinalAQAController@viewMore')->name('update.accr.application');
    Route::post('/accreditation/save', 'FinalAQAController@saveData')->name('save.accr.application');
    Route::post('/accreditation/decision', 'FinalAQAController@makeDecision')->name('accreditation.make.decision');
    Route::get('/accreditation/print/{applicationId}', 'FinalAQAController@toPrint')->name('print.accr.application');

    /**
     ****** School Self Assessment
     */

    Route::get('/accreditation/school/assessment', "FinalAQAController@assessmentView")->name('school.assessment');
    Route::delete('/accreditation/school/assessment', "FinalAQAController@deleteSchoolAssessments")->name('school.delete.assessment');
    Route::get('/accreditation/school/assessment/exporting', "FinalAQAController@exportingSchoolAssessments")->name('school.export.assessment');
    Route::get('/accreditation/school/assessment/answers', "FinalAQAController@assessmentViewAnswers")->name('schools.assessment.answers');
    Route::get('/accreditation/school/assessment/get/{type}', "FinalAQAController@getDataAssessments")->name('get.school.assessment');
    Route::get('/accreditation/school/assessment/{fun}/{school}', "FinalAQAController@assessmentViewSchool")->name('view.school.assessment');
    Route::get('/accreditation/school/assessment/{fun}/{school}/{sub}', "FinalAQAController@assessmentViewSchool")->name('sorting.school.assessment');
    Route::post('/accreditation/school/assessment/{fun}/{school}', "FinalAQAController@storeAssessmentSchool")->name('store.school.assessment');

    /**
     ****** Overall Report
     */

    Route::get('accreditation/audit/overall', "FinalAQAController@overall")->name('audit.overall.report');
    Route::get("accreditation/audit/overall/get", "FinalAQAController@getOverall")->name('audit.overall.report.get');
    Route::get("accreditation/audit/overall/export", "FinalAQAController@exportOverall")->name('audit.overall.report.export');
    /**
     ****** School Summary
     */

    Route::get('/accreditation/audit', "FinalAQAController@summary")->name('school.summary');
    Route::post('/accreditation/audit/upload', "FinalAQAController@uploadSummary")->name('school.summary.upload.save');
    Route::get('/accreditation/audit/{school}', "FinalAQAController@summary")->name('get.school.summary');
    // Data
    Route::get('/accreditation/audit/quality', "AccrController@qualityAudit")->name('audit.manage.quality');
    Route::post('/accreditation/audit/quality', "AccrController@qualityAudit")->name('audit.manage.quality.store');
    Route::get('/accreditation/audit/quality/{id}', "AccrController@qualityAudit")->name('audit.manage.quality.edit');
    Route::delete('/accreditation/audit/quality/{id}', "AccrController@qualityAudit")->name('audit.manage.quality.destroy');
    //
    Route::get('/accreditation/audit/indicators', "AccrController@indicatorsAudit")->name('audit.manage.indicator');
    Route::post('/accreditation/audit/indicators', "AccrController@indicatorsAudit")->name('audit.manage.indicator.store');
    Route::get('/accreditation/audit/indicators/{id}', "AccrController@indicatorsAudit")->name('audit.manage.indicator.edit');
    Route::delete('/accreditation/audit/indicators/{id}', "AccrController@indicatorsAudit")->name('audit.manage.indicator.destroy');

    /**
     ****** Export Format Excel
     */

    Route::get("/accreditation/manage/export-import", "FinalAQAController@exportImport")->name("import.export.format");
    Route::post("/accreditation/manage/export-format", "FinalAQAController@formatToExport")->name("export.format");
    Route::post("/accreditation/manage/import-data", "FinalAQAController@formatToImport")->name("import.format");

    /*
     * ****** Certificate for schools
     */
    Route::get('/accreditation/certificate/{school}', 'FinalAQAController@viewCertificate')->name('view.accreditation.certificate');

    /**
     *** Manage Data
     *
     ****** Attachment
     */

    Route::get('/accreditation/manage/attachments', 'AccrController@attachmentsInput')->name('manage.attachments');
    Route::post('/accreditation/manage/attachments', 'AccrController@attachmentsInput')->name('manage.store.attachment');
    Route::get('/accreditation/manage/attachments/{att_id}', 'AccrController@attachmentsInput')->name('manage.edit.attachment');
    Route::delete('/accreditation/manage/attachments', 'AccrController@attachmentsInput')->name('manage.destroy.attachment');

    /**
     ****** Infrastructure Source
     */

    Route::get('/accreditation/manage/infrastructures', 'AccrController@infrastructuresInput')->name('manage.infrastructures');
    Route::post('/accreditation/manage/infrastructures', 'AccrController@infrastructuresInput')->name('manage.store.infrastructure');
    Route::get('/accreditation/manage/infrastructures/{infra_id}', 'AccrController@infrastructuresInput')->name('manage.edit.infrastructure');
    Route::delete('/accreditation/manage/infrastructures', 'AccrController@infrastructuresInput')->name('manage.destroy.infrastructure');

    /**
     *** Manage Data
     *
     ****** Criteria Section
     */

    Route::get('/accreditation/manage/criteria-section', 'AccrController@criteriaSectionInput')->name('manage.criteria');
    Route::post('/accreditation/manage/criteria-section', 'AccrController@criteriaSectionInput')->name('manage.store.criteria');
    Route::get('/accreditation/manage/criteria-section/{id}', 'AccrController@criteriaSectionInput')->name('manage.edit.criteria');
    Route::delete('/accreditation/manage/criteria-section', 'AccrController@criteriaSectionInput')->name('manage.destroy.criteria');

    /**
     ****** Indicators Source
     */

    Route::get('/accreditation/manage/indicators', 'AccrController@indicatorsInput')->name('manage.indicators');
    Route::post('/accreditation/manage/indicators', 'AccrController@indicatorsInput')->name('manage.store.indicator');
    Route::get('/accreditation/manage/indicator/{id}', 'AccrController@indicatorsInput')->name('manage.edit.indicator');
    Route::delete('/accreditation/manage/indicator', 'AccrController@indicatorsInput')->name('manage.destroy.indicator');

    /**
     ****** Quality Area Source
     */
    Route::get('/accreditation/manage/ajax', "AccrController@ajaxData")->name('get.manage.quality.ajax');
    Route::get('/accreditation/manage/quality-areas', "AccrController@qualityArea")->name('manage.quality.area');
    Route::post('/accreditation/manage/quality-areas', "AccrController@storeQualityArea")->name('manage.store.quality.area');
    Route::get('/accreditation/manage/quality-areas/{id}', "AccrController@qualityArea")->name('manage.edit.quality.area');
    Route::post('/accreditation/manage/quality-areas/{id}', "AccrController@updateQualityArea")->name('manage.update.quality.area');
    Route::delete('/accreditation/manage/quality-areas/{id}', "AccrController@destroyQualityArea")->name('manage.destroy.quality.area');


    /**
     *** QM Manual
     */

    Route::get('/qm-manual', "FinalAQAController@qmmanual")->name('qm.manual');

    Route::resource('/procedures', "ProceduresController");
    Route::resource('/responsibilities', "ResponsibilitiesController");
    Route::resource('/accreditation-types', "AccreditationTypesController");
    Route::resource('/timelines', "TimelinesController");
    Route::get('/qm-manual/{timeline}', "FinalAQAController@qmmanual")->name('edit.timeline');

    /**
     *** General Settings
     */

    Route::get('/accreditation/settings', 'AccrController@generalSettings')->name('accr.settings');
    Route::post('/accreditation/settings', 'AccrController@generalSettings')->name('accr.store.settings');
    Route::delete('/accreditation/settings', 'AccrController@generalSettings')->name('accr.destroy.settings');
    Route::get('/accreditation/settings/notifications/edit/{noti_id}', 'AccrController@generalSettings')->name('accr.edit.settings');

    /**
     *** Curricula Guidance
     **** View Curricula
     */

    Route::prefix('curricula')->group(function () {
        Route::get('/', 'CurriculumController@view')->name('curricula.index');
        Route::get('/schools/{id}', 'CurriculumController@SchoolsView')->name('curricula.view.schools');
        Route::get('/students/{id}', 'CurriculumController@StudentsView')->name('curricula.view.students');
        Route::get('/students/{id}/{school}', 'CurriculumController@StudentsView')->name('curricula.view.school.students');
        Route::get('/get/students-of-qualification/{q}/{s}', 'CurriculumController@getStudentsOfQualification')->name('curricula.get.students');
        Route::get('/{id}/{trade}', 'CurriculumController@view')->name('curricula.trade.view.details');
        Route::get('/{id}', 'CurriculumController@tradesView')->name('curricula.edit');
    });

    /**
     *** Curricula Guidance
     *** Data Management
     *
     **** Sectors Routes
     */

    Route::prefix('sectors')->group(function () {
        Route::get('/', 'CurriculumController@sectors')->name('sectors.index');
        Route::get('/{id}', 'CurriculumController@sectors')->name('sectors.edit');
        Route::post('/', 'CurriculumController@sectors')->name('sectors.store');
        Route::delete('/', 'CurriculumController@sectors')->name('sectors.destroy');
    });

    /**
     *** Curricula Guidance
     *** Data Management
     *
     **** Sub Sectors Routes
     */

    Route::prefix('trades')->group(function () {
        Route::get('/', 'CurriculumController@trades')->name('trades.index');
        Route::get('/{id}', 'CurriculumController@trades')->name('trades.edit');
        Route::post('/', 'CurriculumController@trades')->name('trades.store');
        Route::delete('/', 'CurriculumController@trades')->name('trades.destroy');
    });

    /**
     *** Curricula Guidance
     *** Data Management
     *
     **** RTQF Routes
     */

    Route::prefix('rtqfs')->group(function () {
        Route::get('/', 'CurriculumController@rtqfs')->name('rtqfs.index');
        Route::get('/{id}', 'CurriculumController@rtqfs')->name('rtqfs.edit');
        Route::post('/', 'CurriculumController@rtqfs')->name('rtqfs.store');
        Route::delete('/', 'CurriculumController@rtqfs')->name('rtqfs.destroy');
    });

    /**
     *** Curricula Guidance
     *** Data Management
     *
     **** Curriculum Routes
     */

    Route::prefix('curriculum')->group(function () {
        Route::get('/', 'CurriculumController@curriculum')->name('curriculum.index');
        Route::get('/{id}', 'CurriculumController@curriculum')->name('curriculum.edit');
        Route::post('/', 'CurriculumController@curriculum')->name('curriculum.store');
        Route::delete('/', 'CurriculumController@curriculum')->name('curriculum.destroy');
        Route::get('/{id}/view', 'CurriculumController@curriculumView')->name('curriculum.view');
    });

    /**
     *** Curricula Guidance
     *** Data Management
     *
     **** Modules Routes
     */

    Route::prefix('modules')->group(function () {

        Route::get('/', 'CurriculumController@modules')->name('modules.index');
        Route::post('/', 'CurriculumController@modules')->name('modules.store');
        Route::delete('/', 'CurriculumController@modules')->name('modules.destroy');

        Route::get('/uploading', 'CurriculumController@modulesUploading')->name('modules.uploading');
        Route::post('/uploading', 'CurriculumController@modulesUploading')->name('modules.post.uploading');

        Route::get('/categories', 'CurriculumController@moduleCategories')->name('modules.category.index');
        Route::post('/categories', 'CurriculumController@moduleCategories')->name('modules.category.index');
        Route::delete('/categories', 'CurriculumController@moduleCategories')->name('modules.category.destroy');
        Route::get('/categories/{id}', 'CurriculumController@moduleCategories')->name('modules.category.edit');

        Route::get('/{id}', 'CurriculumController@modules')->name('modules.edit');
    });

    // Get Routes

    Route::prefix('get')->group(function () {
        Route::get('/trades/{sector_id}', 'CurriculumController@getTrades');
        Route::get('/quafication/{trade_id}', 'CurriculumController@getQualifications');
        Route::get('/levels', 'CurriculumController@getLevels')->name('get.levels');
        Route::get('/getdept', 'CurriculumController@getDept')->name('get.dept');
    });

    /**
     *** Monitoring & Evaluation
     */

    Route::prefix('sp')->group(function () {

        Route::get('/monitoring', 'StrategicPlanController@monitoring')->name('sp.monitoring');

        // Reporting Period
        Route::prefix('rp')->group(function () {
            Route::get('/', 'StrategicPlanController@reportingPeriod')->name('sp.rp.index');
            Route::get('/{rp_id}', 'StrategicPlanController@reportingPeriod')->name('sp.rp.edit');
            Route::post('/', 'StrategicPlanController@reportingPeriod')->name('sp.rp.store');
            Route::delete('/', 'StrategicPlanController@reportingPeriod')->name('sp.rp.destroy');
        });

        // Programs Routes
        Route::prefix('programs')->group(function () {
            Route::get('/', 'StrategicPlanController@programs')->name('sp.programs.index');
            Route::post('/', 'StrategicPlanController@programs')->name('sp.programs.store');
            Route::get('/{program_id}', 'StrategicPlanController@programs')->name('sp.programs.edit');
            Route::delete('/', 'StrategicPlanController@programs')->name('sp.programs.destroy');
        });

        // Results Routes
        Route::prefix('results')->group(function () {
            Route::get('/', 'StrategicPlanController@results')->name('sp.results.index');
            Route::post('/', 'StrategicPlanController@results')->name('sp.results.store');
            Route::get('/{id}', 'StrategicPlanController@results')->name('sp.results.edit');
            Route::delete('/', 'StrategicPlanController@results')->name('sp.results.destroy');
        });

        // Indicators Routes
        Route::prefix('indicators')->group(function () {
            Route::get('/', 'StrategicPlanController@indicators')->name('sp.indicators.index');
            Route::post('/', 'StrategicPlanController@indicators')->name('sp.indicators.store');
            Route::get('/{id}', 'StrategicPlanController@indicators')->name('sp.indicators.edit');
            Route::delete('/', 'StrategicPlanController@indicators')->name('sp.indicators.destroy');
        });

        Route::get('/get/programs/{sp_id}', 'StrategicPlanController@getPrograms');
        Route::get('get/results/{program_id}', 'StrategicPlanController@getResults');

        Route::prefix('report')->group(function () {
            Route::get('/', 'StrategicPlanController@report')->name('sp.report.index');
            Route::get('/{rp_id}', 'StrategicPlanController@report')->name('sp.report.edit');
            Route::post('/', 'StrategicPlanController@report');
            Route::get('/result/{result_id}', 'StrategicPlanController@reportResult')->name('sp.report.result');
            Route::get('/more/{result_id}', 'StrategicPlanController@reportMore')->name('sp.report.more');
            Route::get('/indicator/{indicator_id}', 'StrategicPlanController@reporttIndicator')->name('sp.report.indicator');
            Route::get('/print/{period_id}', 'StrategicPlanController@toPrint')->name('sp.report.print');
        });

        Route::get('/export', 'StrategicPlanController@export')->name('sp.export');
        Route::post('/export', 'StrategicPlanController@export')->name('sp.post.export');

    });

    // Action Plan Routes

    Route::prefix('ap')->group(function () {

        // Reporting Periods Routes

        Route::prefix('rp')->group(function () {
            Route::get('/', 'ActionPlanController@reportingPeriod')->name('ap.rp.index');
            Route::get('/{rp_id}', 'ActionPlanController@reportingPeriod')->name('ap.rp.edit');
            Route::post('/', 'ActionPlanController@reportingPeriod')->name('ap.rp.store');
            Route::delete('/', 'ActionPlanController@reportingPeriod')->name('ap.rp.destroy');
        });

        // Programs Routes

        Route::prefix('programs')->group(function () {
            Route::get('/', 'ActionPlanController@programs')->name('ap.programs.index');
            Route::post('/', 'ActionPlanController@programs')->name('ap.programs.store');
            Route::get('/{program_id}', 'ActionPlanController@programs')->name('ap.programs.edit');
            Route::delete('/', 'ActionPlanController@programs')->name('ap.programs.destroy');
        });

        // Results Routes
        Route::prefix('results')->group(function () {
            Route::get('/', 'ActionPlanController@results')->name('ap.results.index');
            Route::post('/', 'ActionPlanController@results')->name('ap.results.store');
            Route::get('/{id}', 'ActionPlanController@results')->name('ap.results.edit');
            Route::delete('/', 'ActionPlanController@results')->name('ap.results.destroy');
        });

        // Indicators Routes
        Route::prefix('indicators')->group(function () {
            Route::get('/', 'ActionPlanController@indicators')->name('ap.indicators.index');
            Route::post('/', 'ActionPlanController@indicators')->name('ap.indicators.store');
            Route::get('/{id}', 'ActionPlanController@indicators')->name('ap.indicators.edit');
            Route::delete('/', 'ActionPlanController@indicators')->name('ap.indicators.destroy');
        });

        Route::get('/get/programs/{rp_id}', 'ActionPlanController@getPrograms');
        Route::get('/get/results/{program_id}', 'ActionPlanController@getResults');

        // Report Routes

        Route::prefix('report')->group(function () {
            Route::get('/', 'ActionPlanController@report')->name('ap.report.index');
            Route::get('/older', 'ActionPlanController@reportOlder')->name('ap.report.older');
            Route::get('/{program_id}', 'ActionPlanController@report')->name('ap.report.edit');
            Route::post('/', 'ActionPlanController@report')->name('ap.report.store');
            Route::post('/older', 'ActionPlanController@reportOlder')->name('ap.report.post.older');
            Route::get('/result/{result_id}', 'ActionPlanController@reportResult')->name('ap.report.result');
            Route::get('/print/{period_id}', 'ActionPlanController@toPrint')->name('ap.report.print');
        });
        Route::get('/export', 'ActionPlanController@export')->name('ap.export');
        Route::post('/export', 'ActionPlanController@export')->name('ap.post.export');
    });

    // Projects Route

    Route::prefix('projects')->group(function () {

        // Development Partners Routes

        Route::prefix('dev-partners')->group(function () {
            Route::get('/', 'ProjectsController@developmentPartners')->name('pr.dev.partners.index');
            Route::get('/{dev_id}', 'ProjectsController@developmentPartners')->name('pr.dev.partners.edit');
            Route::post('/', 'ProjectsController@developmentPartners')->name('pr.dev.partners.store');
            Route::delete('/', 'ProjectsController@developmentPartners')->name('pr.dev.partners.destroy');

        });

        Route::prefix('finance-types')->group(function () {
            Route::get('/', 'ProjectsController@financetypes')->name('pr.fin.types.index');
            Route::get('/{fin_id}', 'ProjectsController@financetypes')->name('pr.fin.types.edit');
            Route::post('/', 'ProjectsController@financetypes')->name('pr.fin.types.store');
            Route::delete('/', 'ProjectsController@financetypes')->name('pr.fin.types.destroy');

        });

        Route::prefix('info')->group(function () {
            Route::get('/', 'ProjectsController@basicInfo')->name('pr.info.index');
            Route::get('/{project_id}', 'ProjectsController@basicInfo')->name('pr.info.edit');
            Route::post('/', 'ProjectsController@basicInfo')->name('pr.info.store');
            Route::delete('/', 'ProjectsController@basicInfo')->name('pr.info.destroy');

        });

        Route::prefix('programs')->group(function () {
            Route::get('/', 'ProjectsController@programs')->name('pr.programs.index');
            Route::get('/{program_id}', 'ProjectsController@programs')->name('pr.programs.edit');
            Route::post('/', 'ProjectsController@programs')->name('pr.programs.store');
            Route::delete('/', 'ProjectsController@programs')->name('pr.programs.destroy');
        });

        Route::prefix('results')->group(function () {
            Route::get('/', 'ProjectsController@results')->name('pr.results.index');
            Route::post('/', 'ProjectsController@results')->name('pr.results.store');
            Route::get('/{result_id}', 'ProjectsController@results')->name('pr.results.edit');
            Route::delete('/', 'ProjectsController@results')->name('pr.results.destroy');
        });

        Route::prefix('indicators')->group(function () {
            Route::get('/', 'ProjectsController@indicators')->name('pr.indicators.index');
            Route::post('/', 'ProjectsController@indicators')->name('pr.indicators.store');
            Route::get('/{indicator_id}', 'ProjectsController@indicators')->name('pr.indicators.edit');
            Route::delete('/', 'ProjectsController@indicators')->name('pr.indicators.destroy');
        });

        Route::prefix('report')->group(function () {
            Route::get('/', 'ProjectsController@report')->name('pr.report.index');
            Route::get('/{project_id}', 'ProjectsController@report')->name('pr.report.edit');
            Route::post('/', 'ProjectsController@report')->name('pr.report.store');
            Route::get('/result/{result_id}', 'ProjectsController@reportResult')->name('pr.report.result');
        });

        Route::get('/get/results/{program_id}', 'ProjectsController@getResults')->name('pr.export.results');
        Route::get('/get/program/{project_id}', 'ProjectsController@getProgram');
        Route::get('/export', 'ProjectsController@export')->name('pr.export');
        Route::post('/export', 'ProjectsController@export')->name('pr.export.post');
    });

    // Settings

    Route::get('/settings', 'SettingsController@generalSettings')->name('settings');
    Route::post('/settings', 'SettingsController@generalSettings')->name('settings.store');
    Route::delete('/settings', 'SettingsController@generalSettings')->name('settings.destroy');
    Route::get('/settings/notifications/edit/{noti_id}', 'SettingsController@generalSettings')->name('settings.edit');

    /**
     * District Route
     */

    Route::resource('district', "DistrictsController");
    Route::resource('calendar', "CalendarController");
    Route::resource('uploadstudents', "UploadStudentsController");
    
    Route::resource('attachments', "AttachmentsController");
    Route::get('wda/attachments/upload', "AttachmentsController@upload")->name('attachments.upload');
    Route::post('/wda/attachments/upload', "AttachmentsController@upload");
    Route::get('/attachments/remove/{att_id}', "AttachmentsController@remove")->name('attachments.remove');

    Route::resource('audit', "AuditController");
    /**
     * National Examination Result
     */
    Route::resource('nationalexams', 'NationalExamController');
    Route::get('uploadedmarks',['as'=>'uploadedmarks','uses'=>'NationalExamController@uploaded']);
    Route::get('overallreport',['as'=>'nationalexams.overallreport','uses'=>'NationalExamController@overallReport']);
    Route::get('perprogram',['as'=>'nationalexams.perprogram','uses'=>'NationalExamController@perProgram']);
    Route::get('registered',['as'=>'nationalexams.registered','uses'=>'NationalExamController@registered']);
    Route::get('get/data/registered',['as'=>'get.nationalexams.registered.data','uses'=>'NationalExamController@getRegisteredData']);
    Route::resource('combinations','CombinationController');
    Route::resource('oldcourses','OldCourseController');
    Route::resource('grading','GradingController');
    Route::resource('weightscale','WeightScaleController');
    Route::resource('examremarks','ExamRemarkController');
    Route::get('students-results-query','NationalExamController@search')->name('nationalexams.search');
    Route::post('students-results-query','NationalExamController@postQuery')->name('nationalexams.post.search');
    Route::get('get-students-results-query','NationalExamController@getResultsFromQueryGiven')->name('nationalexams.get.search');
    Route::get('/get/program/courses', "NationalExamController@getCourses")->name('get.program.courses');
    Route::get('/get/data/examination-results', "NationalExamController@getExaminationResults")->name('get.data.examination.results');

    /**
     * Students Managements
     */

    Route::get('/studentmanagement/summary', 'WdaStudentController@summary')->name('studentmanagement.summary');
    Route::resource('studentmanagement','WdaStudentController');
    Route::post('studentmanagement/search','WdaStudentController@search')->name('studentmanagement.search');
    Route::get('data/studentdata','WdaStudentController@getStudentsData')->name('data.get.studentdata');


    /**
     *  Mineduc Section
     */
    Route::prefix('mineduc')->group(function () {
        Route::get('/','MineducQualityController@index')->name('mineduc.index');
        Route::match(['GET', 'POST'], 'qualityaudit', 'MineducQualityController@qualityAudit')->name('mineduc.qualityaudit');
        Route::match(['GET', 'POST'], 'createschool', 'MineducQualityController@createSchool')->name('mineduc.createschool');
        Route::get('/summary','MineducQualityController@summary')->name('mineduc.summary');
        Route::get('/auditingdata','MineducQualityController@getAuditingReport')->name('data.get.auditingdata');

        //Quality Section
        Route::get('/audit/section', "MineducQualityController@qualitySection")->name('mineduc.quality.section.index');
        Route::post('/audit/manage/quality-section', "MineducQualityController@storeQualitySection")->name('mineduc.quality.section.store');
        Route::get('/audit/manage/quality-section', "MineducQualityController@qualitySection")->name('mineduc.quality.section.edit');
        Route::post('/audit/manage/quality-section/{id}', "MineducQualityController@updateQualitySection")->name('mineduc.quality.section.update');
        Route::delete('/audit/manage/quality-section/{id}', "MineducQualityController@destroyQualitySection")->name('mineduc.quality.section.destroy');

        //Quality Indicators
        Route::get('/audit/indicators', "MineducQualityController@qualityIndicator")->name('mineduc.quality.indicator.index');
        Route::post('/audit/manage/quality-indicators', "MineducQualityController@storeQualityIndicator")->name('mineduc.quality.indicator.store');
        Route::get('/audit/manage/quality-indicators', "MineducQualityController@qualityIndicator")->name('mineduc.quality.indicator.edit');
        Route::post('/audit/manage/quality-indicators/{id}', "MineducQualityController@updateQualityIndicator")->name('mineduc.quality.indicator.update');
        Route::delete('/audit/manage/quality-indicators/{id}', "MineducQualityController@destroyQualityIndicator")->name('mineduc.quality.indicator.destroy');
    });

});

Route::get('cost-of-trainings', "SchoolAuth\CostOfTrainingController@index")->name('school.costoftrainings');
Route::get('data/get/cost-of-trainings', "SchoolAuth\CostOfTrainingController@getData")->name('data.get.costoftrainings');

Route::resource('/gallery', "SchoolAuth\SchoolGalleryController")->names(['index' => 'gallery', 'store' => 'gallery.store', 'delete' => 'gallery.destroy', 'show' => 'gallery.show']);
Route::delete('/gallery/delete/one/{id}', "SchoolAuth\SchoolGalleryController@deleteOne")->name('gallery.delete');

Route::get('/students/data/assigned', 'WdaAuth\SchoolController@assigned');
Route::get('/students/data/assigned/migrate', 'WdaAuth\SchoolController@migrate');