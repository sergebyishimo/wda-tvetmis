<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class SchoolActivityReport extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_school_activity_report";


    public function school()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id');
    }

}
