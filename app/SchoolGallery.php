<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolGallery extends Model
{
    protected  $table = 'school_galleries';
    protected $fillable = ['school_id', 'photo', 'description'];
}
