<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class DisciplineHistory extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = ['school_id', 'student_id', 'term', 'academic_year', 'marks', 'marks_had', 'fault', 'comment', 'sms'];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
