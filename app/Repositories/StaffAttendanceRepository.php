<?php
/**
 * Created by PhpStorm.
 * User: hirwaf
 * Date: 6/11/18
 * Time: 8:13 PM
 */

namespace App\Repositories;


class StaffAttendanceRepository
{
    private $start, $end, $staff;
    private $worked = 0, $absent = 0, $late = 0, $early = 0;

    function __construct($start, $end, $staff = null)
    {
        $this->start = $start;
        $this->end = $end;
        $this->staff = $staff;
    }

    public function daysIn()
    {
        $start = new \DateTime($this->start);
        $end = new \DateTime($this->end);
        $oneday = new \DateInterval("P1D");

        $days = array();
        foreach (new \DatePeriod($start, $oneday, $end->add($oneday)) as $day) {
            $day_num = $day->format("N"); /* 'N' number days 1 (mon) to 7 (sun) */
            if ($day_num < 6) { /* weekday */
                $days[] = $day->format("Y-m-d");
            }
        }
        return $days;
    }

    public function normalWDay()
    {
        return count($this->daysIn());
    }

    public function checkStaff($staff)
    {
        foreach ($this->daysIn() as $item) {
            $att = school(true)
                ->attendanceStaff()
                ->where('date', $item)
                ->where('staff_id', $staff);
            if ($att->count())
                $this->worked += 1;
        }
    }

    public function getLate()
    {
        return $this->late;
    }

    public function getEarly()
    {
        return $this->early;
    }

    public function getWorkedDay()
    {
        return $this->worked;
    }

    public function getAbsentDay()
    {
        return $this->normalWDay() - $this->worked;
    }

    public function getStartDate()
    {
        return $this->start;
    }

    public function getAttRate()
    {
        return ($this->getWorkedDay() * 100) / $this->normalWDay();
    }

    public function getEndDate()
    {
        return $this->end;
    }
}