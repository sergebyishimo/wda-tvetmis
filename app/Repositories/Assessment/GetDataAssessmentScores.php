<?php

namespace App\Repositories\Assessment;

use App\Model\Accr\Functions;
use App\Model\Accr\SchoolInformation;
use App\QueryCacher;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class GetDataAssessmentScores
{
    private $request, $excluded, $function, $datatable, $dispatch;

    function __construct(Request $request, $function, $excluded = [], $datatable = true, $dispatch = false)
    {
        $this->request = $request;
        $this->excluded = $excluded;
        $this->function = $function;
        $this->datatable = $datatable;
        $this->dispatch = $dispatch;

    }

    private function getKey($ad = '')
    {
        if ($this->function == 'i')
            return "input" . $ad;
        if ($this->function == 'p')
            return "process" . $ad;
    }

    private function dataTable()
    {
        $request = $this->request;
//        if ($this->queryCacher($this->getKey('_scores')) && $this->dispatch === false) {
//            $data = $this->getQueryCacher($this->getKey('_scores'));
//            $cdata = collect($data);
//
//            $dataTables = DataTables::of($cdata);
//        } else {
            $submittedInputs = DB::table('accr_school_self_assessiment_statuses')
                ->join('accr_schools_information', 'accr_school_self_assessiment_statuses.school_id', '=', 'accr_schools_information.id')
                ->join('accr_functions', 'accr_school_self_assessiment_statuses.function_id', '=', 'accr_functions.id')
                ->join('accr_source_school_type', 'accr_schools_information.school_type', '=', 'accr_source_school_type.id')
                ->where('accr_school_self_assessiment_statuses.academic_year', '=', date('Y'));

            if ($this->function == 'i') {
                $functionInput = Functions::findOrFail(1);
                $qualityInput = $functionInput->qualityAreas;
                $submittedInputs->where('accr_functions.id', '=', 1);
            } elseif ($this->function == 'p') {
                $functionInput = Functions::findOrFail(2);
                $qualityInput = $functionInput->qualityAreas;
                $submittedInputs->where('accr_functions.id', '=', 2);
            } else {
                if ($this->json === true)
                    return response()->json([]);
                else
                    return [];
            }

            $submittedInputs->select([
                'accr_school_self_assessiment_statuses.*',
                'accr_schools_information.school_name',
                'accr_schools_information.province',
                'accr_schools_information.district',
                'accr_schools_information.school_status',
                'accr_source_school_type.school_type',
                'accr_source_school_type.id as school_type_id',
                'accr_functions.name',
            ]);

            $submittedInputs
                ->orderBy('accr_school_self_assessiment_statuses.created_at', 'ASC');

            $dataTables = DataTables::of($submittedInputs);
            $pw = 0;
            $school = null;
//            $dataTables->addColumn('school_scores', function ($assessment) use ($qualityInput) {
//                global $school;
//                $school = SchoolInformation::find($assessment->school_id);
//                $tt = getFunctionScores($qualityInput, $school, true, $assessment->submit_id);
//                $hv = getFunctionScores($qualityInput, $school, false, $assessment->submit_id);
//                $pp = ($hv * 100) / $tt;
//                $pp = ceil($pp);
//                return $pp;
//            });
            $dataTables->addColumn('wda_scores', function ($assessment) use ($qualityInput) {
                $tt = getFunctionScores($qualityInput, $assessment->school_id, true, $assessment->submit_id, 'wda');
                $hv = getFunctionScores($qualityInput, $assessment->school_id, false, $assessment->submit_id, 'wda');
                $pp = ($hv * 100) / $tt;
                $pp = ceil($pp);
                $pw = $pp;
                return $pp;
            });
//            $dataTables->addColumn('district_scores', function ($assessment) use ($qualityInput) {
//                global $school;
//                $tt = getFunctionScores($qualityInput, $school, true, $assessment->submit_id, 'district');
//                $hv = getFunctionScores($qualityInput, $school, false, $assessment->submit_id, 'district');
//                $pp = ($hv * 100) / $tt;
//                $pp = ceil($pp);
//                return $pp;
//            });
//            $dataTables->addColumn('rating', function ($assessment) {
//                global $pw;
//                $class = getRattingScore($pw, 'c');
//                $rating = getRattingScore($pw);
//                return '<label class="label label-' . $class . ' text-sm p-1 rounded shadow-sm">' . $rating . '</label>';
//            });
//            $dataTables->addColumn('stage', function ($assessment) {
//                if ($assessment->district_confirm == null):
//                    return '<label class="label label-info p-1 rounded shadow-sm" > At district </label>';
//                elseif ($assessment->wda_confirm == null):
//                    return '<label class="label label-info p-1 rounded shadow-sm" > At WDA </label>';
//                else:
//                    return '<label class="label label-info p-1 rounded shadow-sm" > Viewed</label>';
//                endif;
//            });
//            $dataTables->addColumn('accreditation', function ($assessment) {
//                if ($assessment->district_confirm == 1 && $assessment->wda_confirm == 1):
//                    return '<label class="label label-success p-1 rounded shadow-sm">Done</label>';
//                else:
//                    return '<label class="label label-warning p-1 rounded shadow-sm">In Process</label>';
//                endif;
//            });
//            $dataTables->addColumn('button', function ($assessment) {
//                $url = route('wda.sorting.school.assessment', [$assessment->function_id, $assessment->school_id, $assessment->submit_id]);
//                $msg = "Are sure you want to delete ...\n" . $assessment->school_name;
//                $urlD = route('wda.school.delete.assessment');
//                $action = '<form method="POST" data-msg="' . $msg . '" id="formDeleteAssessment">' . '' .
//                    '<input type="hidden" name="_method" value="delete">
//                <input type="hidden" name="_d_ass" value="' . $assessment->id . '">
//                <input type="hidden" name="_d_school" value="' . $assessment->school_id . '">
//                <input type="hidden" name="_d_submit" value="' . $assessment->submit_id . '">
//                <input type="hidden" name="_d_function" value="' . $assessment->function_id . '">
//                <button title="Delete ' . $assessment->school_name . '"
//                class="btn btn-danger btn-sm btn-block mt-1">
//                Delete
//                </button></form>';
//                return '<a href="' . $url . '" class="btn btn-sm btn-dark btn-block">View</a>' . $action;
//            });
            if ($qualityInput) {
                foreach ($qualityInput as $quality) {
                    $dataTables->addColumn(strtolower(str_replace(' ', '_', $quality->name)), function ($assessment) use ($quality) {
                        return getQualityAreaScores($quality->criteria, $assessment->school_id, $assessment->submit_id);
                    });
                    foreach ($quality->criteria as $criteria_section) {
                        $dataTables->addColumn(strtolower(str_replace(' ', '_', $criteria_section->criteria_section)), function ($assessment) use ($criteria_section) {
                            return getCriteriaScores($criteria_section['criterias'], $assessment->school_id, $assessment->submit_id);
                        });
                    }
                }
            }
//        }

        return $dataTables
            ->rawColumns(['button', 'accreditation', 'stage', 'rating'])
            ->make(true);
    }

    private function renderResults()
    {
        $data = $this->dataTable();
        if (count($this->excluded) || $this->datatable === false)
            $data = $this->excluded($data);

        return $data;
    }

    private function excluded($data)
    {
        $list = [];
        foreach ($data as $items) {
            if (is_array($items) && array_key_exists('data', $items))
                $list = $items['data'];
        }
        $export = [];
        foreach ($list as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    if (!in_array($k, $this->excluded))
                        $lo[$k] = $v;
                }
                $export[] = $lo;
            }
        }
        return $export;
    }

    private function queryCacher($key)
    {
        if (QueryCacher::where('query', $key)->first())
            return true;

        return false;
    }

    private function getQueryCacher($key)
    {
        $data = QueryCacher::where('query', $key)->first();
        return json_decode($data->results);
    }

    public function getResults()
    {
        return $this->renderResults();
    }

}