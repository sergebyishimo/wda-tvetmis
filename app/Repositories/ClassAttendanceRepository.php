<?php

namespace App\Repositories;

class ClassAttendanceRepository
{

    private $level, $course, $reg, $month;

    private $dat;

    function __construct($class, $course, $month, $reg)
    {
        $this->level = $class;
        $this->course = $course;
        $this->reg = $reg;
        $this->month = $month;
        $this->dat = "";
    }

    private function getAbsentReg($day)
    {
        $sql = school(true)->classAttendance()
            ->where("level_id", $this->level)
            ->where("course_id", $this->course)
            ->groupBy("tapDate");

        $ab = array();
        if ($sql->count() > 0) {
            foreach ($sql->get() as $q) {
                $tapDate = $q->tapDate;
                if ($tapDate == $this->month . "-" . $day) {
                    $abs = str_replace(' ', '', $q->absent);
                    $ab = explode(',', $abs);
                    $this->dat = $tapDate;
                    break;
                }
            }
        }
        return $ab;
    }

    public function isAbsent($day)
    {
        $abs = $this->getAbsentReg($day);
        $date = $this->month . "-" . ($day < 10 ? '0'.$day : $day);
        if ($this->isWeekend($date))
            return "";
        else {
            if ($this->dat == $date) {
                return in_array($this->reg, $abs) ? "A" : "P";
            }
            return "-";
        }
    }

    private function isWeekend($date)
    {
        return (date('N', strtotime($date)) >= 6);
    }


}