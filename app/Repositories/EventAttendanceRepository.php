<?php

namespace App\Repositories;

class EventAttendanceRepository
{

    private $level, $student, $month, $type;

    private $dat;

    function __construct($month, $student, $type)
    {
        $this->student = $student;
        $this->month = $month;
        $this->type = $type;
        $this->dat = "";
    }

    private function getAttendedStudent($day)
    {
        $sql = school(true)->eventAttendance()
            ->where("student_id", $this->student)
            ->where("type", $this->type)
            ->groupBy("tapDate");

        $ab = array();
        if ($sql->count() > 0) {
            foreach ($sql->get() as $q) {
                $tapDate = $q->tapDate;
                if ($tapDate == $this->month . "-" . $day) {
                    $ab[] = $q->student_id;
                    $this->dat = $tapDate;
                    break;
                }
            }
        }
        return $ab;
    }

    public function isAttend($day)
    {
        $abs = $this->getAttendedStudent($day);
        $date = $this->month . "-" . $day;
        if ($this->isWeekend($date))
            return "";
        else {
            if ($this->dat == $date) {
                return in_array($this->student, $abs) ? "P" : "A";
            }
            return "-";
        }
    }

    private function isWeekend($date)
    {
        return (date('N', strtotime($date)) >= 6);
    }


}