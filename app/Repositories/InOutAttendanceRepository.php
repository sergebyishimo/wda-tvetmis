<?php
/**
 * Created by PhpStorm.
 * User: hirwaf
 * Date: 6/10/18
 * Time: 5:47 PM
 */

namespace App\Repositories;


class InOutAttendanceRepository
{
    protected $start;
    protected $end;
    protected $db;
    protected $level, $acadyear;

    function __construct($start, $end, $level)
    {
        $this->start = $start;
        $this->end = $end;
        $this->level = $level;

        $this->acadyear = date("Y");
    }

    private function valid()
    {
        if ($this->start < $this->end)
            return true;

        return false;
    }

    public function getDate()
    {
        $sql = "SELECT ";
    }

    public function getDateRange()
    {
        $this->valid();

        $begin = new \DateTime($this->start);
        $end = new \DateTime($this->end);
        $end->modify('+1 day');

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval, $end);

        return $daterange;
    }

    public function getStudents()
    {
        $sql = school(true)->students()
            ->where("status", "active")
            ->where("level_id", $this->level)
            ->orderBy("fname", "ASC")
            ->orderBy("lname", "ASC");

        if ($sql->count() > 0)
            return ($sql->get());
        return [];
    }

    public function getStudentsByDate($date, $class)
    {
        /* $sql = "SELECT i.std_id, i.tapTime, CONCAT( s.std_firstname,  ' ', s.std_lastname ) AS std_names
                FROM in_out_attendance i
                LEFT JOIN students s ON i.std_id = s.registration_id AND s.combination_id = '$class'
                WHERE i.tapDate =  '$date'
                GROUP BY i.std_id
                ORDER BY s.std_firstname ASC"; */

        $sql = school(true)->attendanceInOut()
            ->join("students as s", "s.id", "=", "attendance_in_outs.student_id")
            ->where("s.level_id", $class)
            ->where("s.status", "active")
            ->group("attendance_in_outs.student_id")
            ->orderBy("s.fname", "ASC")
            ->orderBy("s.lname", "ASC");

        if ($sql->count() > 0)
            return $sql->get();

        return [];
    }

    public function getStudentsAbsentByDate($date, $class)
    {
        $datee = date('Y');

        $sql = school(true)->students()
            ->where("status", "active")
            ->where("level_id", $class)
            ->orderBy("fname", "ASC")
            ->orderBy("lname", "DESC");

        $students = array();
        if ($sql->count() > 0) {
            foreach ($sql->get() as $sdt) {
                $stdID = $sdt->id;
                $sql = school(true)->attendanceInOut()
                    ->where("student_id", $stdID)
                    ->where("date", $date);

                $op = $sql->count();

                if ($op == 0)
                    $students[] = trim($sdt->fname . " " . $sdt->lname);
            }
            return $students;
        }

        return [];
    }

    public function get($std, $date, $feedback)
    {
        $query = $this->getSQL($std, $date);
        $c = $query->count();

        if ($c > 0) {
            switch ($feedback) {
                case "IN":
                    $query = $this->getSQL($std, $date);
                    $results = $query->first();
                    return $results->cometime;
                    break;
                case "OUT":
                    if ($c > 1) {
                        $query = $this->getSQL($std, $date, ['leavetime', 'DESC']);
                        $results = $query->first();
                        return $results->leavetime;
                    }
                    return null;
                    break;
                default:
                    return null;
                    break;
            }
        }
        return null;
    }

    private function getSQL($std, $date, $w = "")
    {
        $sql = school(true)->attendanceInOut()
            ->where("student_id", $std)
            ->where("date", $date);
        if ( ! is_array($w) )
            return $sql->orderby("cometime", "ASC");

        return $sql->orderby($w[0], $w[1]);
    }


}