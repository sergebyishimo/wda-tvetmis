<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateVersion extends Model
{
    public $incrementing = false;
    protected $connection = "mysql";
    protected $fillable = [
        'school_id', 'name', 'updateVersion'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }


}
