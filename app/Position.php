<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Position extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = true;

    protected $fillable = [
        'school_id', 'acad_year', 'term', 'std_reg_no', 'position', 'last_update'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'std_reg_no', 'reg_no');
    }
}
