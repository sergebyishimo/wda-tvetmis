<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use Uuid;
    protected $connection = "mysql";
    use SoftDeletes;

    public $incrementing = false;

    protected $fillable = [
        'school_id', 'student_id', 'term', 'operator', 'bank_slip', 'bank_number', 'transaction_id', 'amount',
        'created_at', 'updated_at'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
