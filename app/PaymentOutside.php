<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class PaymentOutside extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'rp_mysql';

    protected $fillable = [
        'college_id', 'student_id', 'payment_category', 'attachment', 'academic_year', 'date_paid', 'amount',
        'payment_type', 'bank'
    ];

    public function category()
    {
        return $this->belongsTo(FeeCategory::class,'payment_category', 'id');
    }

}
