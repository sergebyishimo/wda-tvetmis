<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FailedModule extends Model
{
    protected $table = "rp_failed_modules";

    protected $fillable = ['college_id',  'student_id', 'academic_year', 'module_id', 'semester'];
}
