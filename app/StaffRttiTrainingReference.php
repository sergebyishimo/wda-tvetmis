<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffRttiTrainingReference extends Model
{
    protected $connection = "mysql";

    public $timestamps = false;

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id');
    }

    public function source()
    {
        return $this->belongsTo(StaffRttiTrainingReferenceSource::class, 'training_reference_id');
    }

}
