<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class IncubationCenter extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_incubation_centers";

    public  function  school(){
        return $this->belongsTo(SchoolInformation::class,'school_id');
    }
}
