<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SourceSponsor extends Model
{
    protected $fillable = ['sponsor'];
}
