<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffQualification extends Model
{
    protected $table = "staff_qualifications";

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }
}
