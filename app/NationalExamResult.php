<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class NationalExamResult extends Model
{
    protected $table = "national_exam_result";

    protected  $fillable = ['school_id','student_index','section_id','course_id','marks','created_at','updated_at'];

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id','school_code');
    }

    public function course()
    {
        return $this->belongsTo(OldCourse::class, 'course_id');
    }

    public function combination()
    {
        return $this->belongsTo(Combination::class, 'section_id');
    }
    public  function student(){
        return $this->belongsTo(Student::class,'student_index','index_number');
    }
    public  function  computedmarks(){
        return $this->hasOne(ComputedAggregate::class,'student_index','student_index');
    }

}
