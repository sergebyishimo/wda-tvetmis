<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAssessorBackground extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'school_id', 'staff_id', 'academic_year', 'education_program_assessed', 'assessement_center'
    ];

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }

}
