<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class Sector extends Model
{
    use Uuid;
    use SoftDeletes;

    protected $connection = "mysql";
    protected $fillable = [
        'sector_name', 'acronym'
    ];

    public function subSector()
    {
        return $this->hasMany(App\Model\Accr\TrainingTradesSource::class, 'sector_id');
    }
}
