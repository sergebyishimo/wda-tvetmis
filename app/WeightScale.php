<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeightScale extends Model
{

    public  function  grade(){
        return $this->belongsTo(WdaGrade::class,'grade_id');
    }
}
