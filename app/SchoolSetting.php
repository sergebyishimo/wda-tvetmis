<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolSetting extends Model
{
    protected $connection = "mysql";
    protected $table = "school_settings";

    protected $primaryKey = 'school';

    protected $fillable = [
        'active_period', 'active_term', 'school', 'updateVersion'
    ];

    public function school()
    {
        return $this->belongsTo(School::class, 'school', 'id');
    }

}
