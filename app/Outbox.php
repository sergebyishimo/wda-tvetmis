<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Outbox extends Model
{
    use Uuid;

    protected $connection = "mysql";
    public $incrementing = false;

    protected $table = "outbox";

    protected $fillable = [
        'school_id', 'type', 'acad_year', 'term', 'period', 'receiver', 'message', 'sent_at'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
