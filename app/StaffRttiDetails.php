<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffRttiDetails extends Model
{
    protected $connection = "mysql";

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class,'staff_id');
    }
}
