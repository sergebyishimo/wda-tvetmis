<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Kblais\Uuid\Uuid;

class MineducQualityAuditIndicators extends Model
{
    use Uuid;
    public $incrementing = false;
    public $fillable = ['id','section_id','indicator_name','grade1','grade2','grade3','grade4','grade5','evidences'];

    function  section(){
        return $this->belongsTo(MineducQualityAuditSection::class,'section_id');
    }
}
