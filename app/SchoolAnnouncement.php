<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolAnnouncement extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_school_announcements";

    public function school()
    {
        return $this->belongsTo(SchoolAnnouncement::class, 'school_id');
    }
}
