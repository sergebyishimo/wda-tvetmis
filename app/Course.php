<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Course extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = [
        'school_id', 'level_id', 'module_id', 'max_point',
        'staffs_info_id', 'term', 'status', 'updateVersion'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function teacher()
    {
        return $this->belongsTo(StaffsInfo::class, "staffs_info_id");
    }

    public function class_attendance()
    {
        return $this->hasMany(ClassAttendance::class);
    }

}
