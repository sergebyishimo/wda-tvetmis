<?php

namespace App;

use App\Notifications\SchoolResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class SchoolUser extends Authenticatable
{
    use Notifiable;

//    use SoftDeletes;
//    protected $dates = ['deleted_at'];

    use Uuid;

    protected $connection = "mysql";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type', 'privilege', 'school_id', 'android_pass', 'updateVersion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SchoolResetPassword($token));
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function schoolPrivilege()
    {
        return $this->hasOne(SchoolUserPrivilege::class, 'id', 'privilege');
    }

    public function getDistrictIdAttribute()
    {
        return $this->school->district ? : "";
    }

}
