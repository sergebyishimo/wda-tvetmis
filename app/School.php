<?php

namespace App;

use App\Model\Accr\AccrApplication;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SchoolSelfAssessimentStatus;
use App\Model\Accr\SchoolType;
use App\Model\Accr\SourceSchoolOwnership;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;
use OwenIt\Auditing\Contracts\Auditable;

class School extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use \Swis\LaravelFulltext\Indexable;

    public $incrementing = false;

    protected $connection = 'accr_mysql';

    protected $table = 'schools_information';

    public function getNameAttribute()
    {
        return $this->school_name;
    }

    public function getAcronymAttribute()
    {
        return $this->school_acronym;
    }

    public function getStudyingModeAttribute()
    {
        return $this->boarding_or_day;
    }

    public function getDisciplineMaxAttribute()
    {
        return $this->discipline_totalmarks;
    }

    public function getPhoneNumberAttribute()
    {
        return $this->phone;
    }

    public function getStatusAttribute()
    {
        return $this->school_status;
    }

    public function getLogoAttribute()
    {
        return $this->school_logo;
    }

    public function getMidAttribute()
    {
        return $this->school_code;
    }

    public static function count()
    {
        return self::where('status', 1)->count();
    }

    public function schoolUser()
    {
        return $this->hasOne(SchoolUser::class);
    }

    public function schoolUsers()
    {
        return $this->hasMany(SchoolUser::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'school', 'id');
    }

    public function settings()
    {
        return $this->hasOne(SchoolSetting::class, 'school', 'id');
    }

    public function staffs()
    {
        return $this->hasMany(StaffsInfo::class);
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    public function levels()
    {
        return $this->hasMany(Level::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function fees()
    {
        return $this->hasMany(FeesExpected::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function discipline()
    {
        return $this->hasMany(Discipline::class);
    }

    public function disciplineHistory()
    {
        return $this->hasMany(DisciplineHistory::class);
    }

    public function studentPermissions()
    {
        return $this->hasMany(SchoolPermission::class, 'school_id');
    }

    public function periodicEntry()
    {
        return $this->hasMany(PeriodicEntry::class);
    }

    public function examEntry()
    {
        return $this->hasMany(ExamEntry::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function outbox()
    {
        return $this->hasMany(Outbox::class);
    }

    public function attendanceStaff()
    {
        return $this->hasMany(AttendanceStaff::class);
    }

    public function attendanceInOut()
    {
        return $this->hasMany(AttendanceInOut::class);
    }

    public function classAttendance()
    {
        return $this->hasMany(ClassAttendance::class);
    }

    public function eventAttendance()
    {
        return $this->hasMany(EventAttendance::class);
    }

    public function update_version()
    {
        return $this->hasMany(UpdateVersion::class);
    }

    public function visit()
    {
        return $this->hasMany(Visit::class);
    }

    public function tempCards()
    {
        return $this->hasMany(TempCard::class);
    }

    public function additionalFees()
    {
        return $this->hasMany(AdditionalFees::class);
    }

    public function accrApplication()
    {
        return $this->hasMany(AccrApplication::class,'school_id', 'id');
    }

    public function selfAssessmentStatus()
    {
        return $this->hasMany(SchoolSelfAssessimentStatus::class,'school_id', 'id');
    }

    /**
     * Destroy the models for the given IDs.
     *
     * @param  array|int $ids
     * @return int
     */
    public static function destroy($ids)
    {
        // We'll initialize a count here so we will return the total number of deletes
        // for the operation. The developers can then check this number as a boolean
        // type value or get this total count of records deleted for logging, etc.
        $count = 0;

        $ids = is_array($ids) ? $ids : func_get_args();

        // We will actually pull the models from the database table and call delete on
        // each of them individually so that their events get fired properly with a
        // correct set of attributes in case the developers wants to check these.
        $key = ($instance = new static)->getKeyName();

        foreach ($instance->whereIn($key, $ids)->get() as $model) {
            $tmpM = $model->id;
            if ($model->delete()) {
                $schoolUsers = SchoolUser::where('school_id', $tmpM)->get();
                $schoolStaffs = StaffsInfo::where('school_id', $tmpM)->get();
                $schoolStudents = Student::where('school', $tmpM)->get();
                if ($schoolUsers->count() > 0) {
                    foreach ($schoolUsers as $item) {
                        $item->delete();
                    }
                }
                if ($schoolStaffs->count() > 0) {
                    foreach ($schoolStaffs as $item) {
                        $item->delete();
                    }
                }

                if ($schoolStudents->count() > 0) {
                    foreach ($schoolStudents as $item) {
                        $item->delete();
                    }
                }

                $count++;
            }
        }

        return $count;
    }

    public function desks()
    {
        return $this->hasMany(SchoolDesk::class,'school_id', 'id');
    }

    public function requests()
    {
        return $this->hasMany(SchoolRequest::class,'school_id', 'id');
    }

    public function activityreports()
    {
        return $this->hasMany(SchoolActivityReport::class,'school_id', 'id');
    }

    public function workshops()
    {
        return $this->hasMany(SchoolWorkshop::class,'school_id', 'id');
    }
    public function incubationcenters(){
        return $this->hasMany(IncubationCenter::class,'school_id','id');
    }

    public  function productionunits(){
        return $this->hasMany(ProductionUnit::class,'school_id','id');
    }

    public  function otherinfrastructures(){
        return $this->hasMany(OtherInfrastructure::class,'school_id','id');
    }

    public function admissionrequirement()
    {
        return $this->hasOne(SchoolAdmission::class, 'school_id', 'id');
    }

    public function shortcourses()
    {
        return $this->hasMany(ShortCourse::class, 'school_id', 'id');
    }

    public function announcements()
    {
        return $this->hasMany(SchoolAnnouncement::class, 'school_id', 'id');
    }

    public function costoftraining()
    {
        return $this->hasMany(CostOfTraining::class, 'school_id', 'id');
    }

    public function schooltype()
    {
        return $this->belongsTo(SchoolType::class, 'school_type');
    }

    public function ownership()
    {
        return $this->belongsTo(SourceSchoolOwnership::class, 'owner_type', 'id');
    }
}
