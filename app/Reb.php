<?php

namespace App;

use App\Notifications\RebResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Reb extends Authenticatable /*implements CanVerifyEmailContract*/
{
    protected $connection = 'mysql';

    use Notifiable;

    /*use CanVerifyEmail;*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new RebResetPassword($token));
    }

    public function attachement()
    {
        return $this->hasMany(RebStaffProfileAttachmentsFinal::class, 'staff_id', 'id');
    }

    public function markingApplications()
    {
        return $this->hasMany(RebStaffMarkerApplication::class, 'staff_id', 'id');
    }

    public function getCurrentMarkingAttribute()
    {
        if ($this->markingApplications->count() > 0) {
            $ch = $this->markingApplications()->where('academic_year', date('Y'))->first();
            if ($ch)
                return $ch;
        }

        return false;
    }

    public function getSchoolNameAttribute()
    {
        if ($this->school)
            return $this->school;

        return "";

    }

    public function getSchoolAcronymAttribute()
    {
        if ($this->school)
            return $this->school;

        return "";

    }

    public function markingBackgrounds()
    {
        return $this->hasMany(RebStaffMarkingBackground::class, 'staff_id', 'id');
    }

    public function teachingExperience()
    {
        return $this->hasMany(RebStaffTeachingExperience::class, 'staff_id', 'id');
    }

    public function workingExperience()
    {
        return $this->hasMany(RebStaffWorkexperience::class, 'staff_id', 'id');
    }

}
