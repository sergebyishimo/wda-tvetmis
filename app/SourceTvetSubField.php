<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SourceTvetSubField extends Model
{
    public $timestamps = false;

    protected $table = "source_tvet_sub_field";

    protected $connection = 'mysql';
}
