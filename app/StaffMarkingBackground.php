<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffMarkingBackground extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'school_id', 'staff_id', 'academic_year', 'marking_position', 'marking_institution', 'education_program',
        'marking_center', 'subject_exam_marked'
    ];

    public function staff()
    {
        $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }

}
