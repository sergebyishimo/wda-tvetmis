<?php

namespace App;

use App\Rp\AdmittedStudent;
use App\Rp\CollegeStudent;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SponsorStudent extends Model implements Auditable
{
    protected $connection = "rp_mysql";

    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'college_id', 'student_id', 'category_id', 'sponsor', 'percentage'
    ];

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'college_id');
    }

    public function admitted()
    {
        return $this->belongsTo(AdmittedStudent::class, 'student_id', 'std_id');
    }

    public function student()
    {
        return $this->belongsTo(CollegeStudent::class, 'student_id', 'student_reg');
    }

    public function category() {
        return $this->belongsTo(FeeCategory::class, 'category_id', 'id');
    }
}
