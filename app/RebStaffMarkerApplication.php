<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RebStaffMarkerApplication extends Model
{
    public $timestamps = false;

    protected $connection = 'reb_mysql';

    protected $table = "staff_marker_applications";

    protected $fillable = [
        'school_id', 'staff_id', 'academic_year', 'education_program_id', 'trade_marked_before', 'school_recommendation',
        'wda_selection', 'wda_confirmation', 'scores', 'application_date', 'first_subject_applied_for', 'first_subject_marked_before',
        'second_subject_applied_for', 'second_subject_marked_before', 'third_subject_applied_for', 'third_subject_marked_before'
    ];

    public function staff()
    {
        return $this->belongsTo(Reb::class, 'staff_id', 'id');
    }

    public function tvetSubField()
    {
        return $this->belongsTo(SourceTvetSubField::class,'education_program_id', 'id');
    }


    public function getGenderAttribute()
    {
        if ($this->staff)
            return $this->staff->gender;
        return "";
    }

    public function getPhotoAttribute()
    {
        if ($this->staff)
            return $this->staff->photo;
        return "";
    }

    public function getSchoolAttribute()
    {
        if ($this->staff)
            return $this->staff->school;
        return "";
    }

    public function getPhone_numberAttribute()
    {
        if ($this->staff)
            return $this->staff->phone_number;
        return "";
    }

    public function getNational_id_numberAttribute()
    {
        if ($this->staff)
            return $this->staff->national_id_number;
        return "";
    }

    public function getEmailAttribute()
    {
        if ($this->staff)
            return $this->staff->email;
        return "";
    }

    public function getQualificationAttribute()
    {
        if ($this->staff)
            return $this->staff->qualification;
        return "";
    }

    public function getInstitutionAttribute()
    {
        if ($this->staff)
            return $this->staff->institution;
        return "";
    }

    public function getGraduated_yearAttribute()
    {
        if ($this->staff)
            return $this->staff->graduated_year;
        return "";
    }

    public function getNationalityAttribute()
    {
        if ($this->staff)
            return $this->staff->nationality;
        return "";
    }

    public function getProvinceAttribute()
    {
        if ($this->staff)
            return $this->staff->province;
        return "";
    }

    public function getDistrictAttribute()
    {
        if ($this->staff)
            return $this->staff->district;
        return "";
    }

    public function getSectorAttribute()
    {
        if ($this->staff)
            return $this->staff->sector;
        return "";
    }

}
