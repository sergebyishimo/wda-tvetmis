<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostOfTraining extends Model
{
    
    protected $connection = "mysql";
    protected $table = "cost_of_training";

    public function school()
    {
        return $this->belongsTo(SchoolAnnouncement::class, 'school_id');
    }
}
