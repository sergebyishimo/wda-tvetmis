<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class Module extends Model
{
    use Uuid;
    protected $connection = "mysql";
    use SoftDeletes;

    protected $fillable = [
        'qualification_id', 'module_code', 'module_title',
        'credits', 'competence', 'module_class', 'learning_hours',
        'attachment', 'competence_type', 'competence_class'
    ];

    public function courses()
    {
        return $this->hasOne(Course::class);
    }

    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }

    public function type()
    {
        return $this->belongsTo(ModuleType::class, "module_type_id");
    }
}
