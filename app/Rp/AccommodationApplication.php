<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class AccommodationApplication extends Model
{
    protected $table = 'rp_accomodation_applications';
    protected $fillable = ['college_id', 'student_reg', 'academic_year', 'file'];

    public function student() {
        $this->belongsTo(\App\Rp\CollegeStudent::class, 'student_reg', 'id');
    }
}
