<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class Suspension extends Model
{
    protected $fillable = ['college_id', 'student_reg', 'letter', 'resuming_academic_year', 'resuming_semester'];
    protected $table = 'rp_suspenssions';

    public function student() {
        return $this->belongsTo(\App\Rp\AdmittedStudent::class, 'student_reg', 'std_id');
    }
}
