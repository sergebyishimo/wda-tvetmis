<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class CollegeModule extends Model
{
    public function option() {
        return $this->belongsTo(\App\Rp\CollegeOption::class, 'option_id');
    }
}
