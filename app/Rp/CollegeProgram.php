<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class CollegeProgram extends Model
{
    public function department() {
        return $this->belongsTo('App\Rp\CollegeDepartment', 'department_id');
    }
}
