<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class CollegeOption extends Model
{
    protected $connection = "mysql";
    public function department() {
        return $this->belongsTo(\App\Rp\CollegeDepartment::class, 'department_id');
    }

    public function modules() {
        return $this->hasMany(\App\Rp\CollegeModule::class, 'option_id');
    }

    public function getProgramNameAttribute()
    {
        return $this->option_name;
    }
}
