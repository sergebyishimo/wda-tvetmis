<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class MineducQualityAuditSection extends Model
{
    use Uuid;
    public $incrementing = false;
    public $fillable = ['id','section_name'];

    function  indicators(){
        return $this->hasMany(MineducQualityAuditIndicators::class,'section_id','id');
    }
}
