<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class ShortCourse extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "short_courses";

    public function school()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id');
    }
}
