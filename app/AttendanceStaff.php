<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class AttendanceStaff extends Model
{
    use Uuid;
    protected $connection = "mysql";
    protected $fillable = [
        'school_id', 'date', 'cometime', 'leavetime', 'staff_id', 'flag',
        'worktime', 'late', 'early'
    ];
    public $incrementing = false;

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }
}
