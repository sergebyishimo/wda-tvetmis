<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalFees extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;
    protected $fillable = [
        'school_id', 'department_id', 'level_id', 'student_id', 'label', 'amount', 'acad_year', 'term', 'status'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
