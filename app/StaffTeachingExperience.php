<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffTeachingExperience extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'school_id', 'staff_id', 'tvet_field', 'tvet_sub_field', 'institution_taught', 'qualification_name',
        'teaching_competences', 'class_or_level_taught', 'from_date', 'to_date', 'attachments', 'attachment_name',
        'attachment_size', 'subject_taught'
    ];

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }
}
