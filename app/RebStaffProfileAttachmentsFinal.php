<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RebStaffProfileAttachmentsFinal extends Model
{
    public $timestamps = false;

    protected $connection = 'reb_mysql';

    protected $table = "staff_profile_attachments_finals";

    public function staff()
    {
        return $this->belongsTo(Reb::class, 'staff_id', 'id');
    }

    public function description()
    {
        return $this->belongsTo(Attachment::class, 'attachment_description', 'id');
    }

    public function getNameAttribute()
    {
        if ($this->description)
            return $this->description->name;
        else
            return "";
    }
}
