<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapLecturerRoutes();

        $this->mapDistrictRoutes();

        $this->mapAdminRoutes();

        $this->mapCollegeRoutes();

        $this->mapRpRoutes();

        $this->mapWdaRoutes();

        $this->mapExaminerRoutes();

        $this->mapRebRoutes();

        $this->mapStudentRoutes();

        $this->mapSchoolRoutes();

        //
    }

    /**
     * Define the "school" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapSchoolRoutes()
    {
        Route::group([
            'middleware' => ['web', 'school', 'auth:school'],
            'prefix' => 'school',
            'as' => 'school.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/school.php');
        });
    }

    /**
     * Define the "student" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapStudentRoutes()
    {
        Route::group([
            'middleware' => ['web', 'student', 'auth:student'],
            'prefix' => 'student',
            'as' => 'student.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/student.php');
        });
    }

    /**
     * Define the "reb" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapRebRoutes()
    {
        Route::group([
            'middleware' => ['web', 'reb', 'auth:reb'],
            'prefix' => 'reb',
            'as' => 'reb.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/reb.php');
        });
    }

    /**
     * Define the "examiner" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapExaminerRoutes()
    {
        Route::group([
            'middleware' => ['web', 'examiner', 'auth:examiner'],
            'prefix' => 'examiner',
            'as' => 'examiner.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/examiner.php');
        });
    }

    /**
     * Define the "wda" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWdaRoutes()
    {
        Route::group([
            'middleware' => ['web', 'wda', 'auth:wda'],
            'prefix' => 'wda',
            'as' => 'wda.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/wda.php');
        });
    }

    /**
     * Define the "rp" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapRpRoutes()
    {
        Route::group([
            'middleware' => ['web', 'rp', 'auth:rp'],
            'prefix' => 'rp',
            'as' => 'rp.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/rp.php');
        });
    }

    /**
     * Define the "college" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapCollegeRoutes()
    {
        Route::group([
            'middleware' => ['web', 'college', 'auth:college'],
            'prefix' => 'college',
            'as' => 'college.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/college.php');
        });
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => ['web', 'admin', 'auth:admin'],
            'prefix' => 'admin',
            'as' => 'admin.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/admin.php');
        });
    }

    /**
     * Define the "district" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapDistrictRoutes()
    {
        Route::group([
            'middleware' => ['web', 'district', 'auth:district'],
            'prefix' => 'district',
            'as' => 'district.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/district.php');
        });
    }

    /**
     * Define the "lecturer" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapLecturerRoutes()
    {
        Route::group([
            'middleware' => ['web', 'lecturer', 'auth:lecturer'],
            'prefix' => 'lecturer',
            'as' => 'lecturer.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/lecturer.php');
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
