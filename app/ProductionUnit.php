<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class ProductionUnit extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_production_units";

    public  function  school(){
        return $this->belongsTo(SchoolInformation::class,'school_id');
    }
}
