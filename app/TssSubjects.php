<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TssSubjects extends Model
{
    public $timestamps = false;

    protected $table = "tss_subjects";
}
