<?php

namespace App\Transformers;

use App\CollegeContinuingStudent;
use App\ListAllContinuigStudents;
use League\Fractal\TransformerAbstract;

class ListAllContinuigStudentsTransformer extends TransformerAbstract
{
    /**
     * @param CollegeContinuingStudent $student
     * @return array
     */
    public function transform(CollegeContinuingStudent $student)
    {
        $action = '<a href="/college/registration/uploads/uploaded/edit/'.$student->id.'" class="btn btn-primary"> Edit </a><form action="'.route('college.reg.delete',['id'=>$student->id]).'" method="post"> <input type=hidden name="_method" value="DELETE"> '. csrf_field() .' <button type="submit" class="btn btn-danger">Delete</button> </form>';

        return [
            'student_names' => $student->first_name. " " . $student->last_name,
            'option_name' => $student->option ?  $student->option->option_name : "",
            'old_reg_nber' => $student->registration_number,
            'new_reg_nber' => $student->student_reg,
            'sponsor' => strtoupper($student->sponsor_id ),
            'year_of_study' => $student->year_of_study,
            'uploaded_at' => date('d/m/Y', strtotime($student->created_at)),
            'action' => $action
        ];
    }
}