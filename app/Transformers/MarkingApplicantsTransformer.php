<?php

namespace App\Transformers;

use App\StaffMarkerApplication;
use League\Fractal\TransformerAbstract;

class MarkingApplicantsTransformer extends TransformerAbstract
{
    /**
     * @param StaffMarkerApplication $staff
     * @return array
     */
    public function transform(StaffMarkerApplication $staff)
    {
        return [
            'photo' => '<img src="' . getStaffPhoto($staff->photo) . '" alt="' . ucwords($staff->first_name.' ' .$staff->last_name) . '" class="img-responsive img-rounded" style="width: 100px;">',
            'names' => '<a href="' . route('examiner.read.staff', $staff->sid) . '">' . ucwords($staff->first_name.' ' .$staff->last_name) . '</a>',
            'gender' => $staff->gender,
            'nationID' => $staff->national_id_number ,
            'phone_number' => $staff->phone_number ,
            'school' => $staff->name,
            'first_subject_applied_for' => $staff->first_subject_applied_for ?: "----",
            'second_subject_applied_for' => $staff->second_subject_applied_for /*?: "----"*/,
            'third_subject_applied_for' => $staff->third_subject_applied_for /*?: "----"*/,
            'education_program_id' => $staff->tvetSubField ? $staff->tvetSubField->sub_field_name : "",
            'province' => $staff->staff ? $staff->staff->province : "",
            'district' => $staff->staff ? $staff->staff->district : "",
            'sector' => $staff->staff ? $staff->staff->sector : "",
            'qualification' => $staff->staff ? $staff->staff->qualification : "",
            'graduated_year' => $staff->staff ? $staff->staff->graduated_year : ""
        ];
    }
}