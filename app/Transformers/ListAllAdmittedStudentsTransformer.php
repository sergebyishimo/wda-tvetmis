<?php

namespace App\Transformers;

use App\Rp\AdmittedStudent;
use League\Fractal\TransformerAbstract;

class ListAllAdmittedStudentsTransformer extends TransformerAbstract
{
    /**
     * @param \App\Rp\AdmittedStudent $student
     * @return array
     */
    public function transform(AdmittedStudent $student)
    {
        $photo = '<img src="'.regGetStudentPhoto($student).'" alt="'.$student->names.'" style="width: 5em;height: 5em;box-shadow: 0px 0px 10px #8d9499" class="img-circle img-responsive">';
        if(auth()->guard('college')->check()) {
            $action = '<a href="'.route('college.registration.show', strtolower($student->student_reg)).'"
                    class="btn btn-primary btn-sm pull-right">See More</a>
                <a href="'.route('college.registration.edit', strtolower($student->std_id)).'"
                        class="btn btn-success btn-sm pull-right">Edit</a>';
            }
        elseif(auth()->guard('rp')->check()) {
            $action = '<a href="'.route('rp.registration.show', strtolower($student->student_reg)).'"
                                                                class="btn btn-primary btn-sm pull-right">See More</a>';
        } else {
            $action = '';
        }

        return [
            'photo' => $photo,
            'student_reg_number' => $student->std_id,
            'student_name' => $student->first_name . ' ' . $student->other_names,
            'department' => $student->department ? $student->department->department : "",
            'course' => $student->course ? $student->course->choices : "",
            'aggregates' => $student->aggregates_obtained,
            'sponsorship_status' => ucwords($student->sponsorship_status == '.' ? 'government' : $student->sponsorship_status),
            'action' => $action
        ];
    }
}