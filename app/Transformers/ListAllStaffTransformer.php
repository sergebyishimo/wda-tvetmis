<?php

namespace App\Transformers;

use App\Model\Accr\SchoolInformation;
use App\StaffsInfo;
use Creativeorange\Gravatar\Facades\Gravatar;
use League\Fractal\TransformerAbstract;

class ListAllStaffTransformer extends TransformerAbstract
{
    /**
     * @param StaffsInfo $staff
     * @return array
     */
    public function transform(StaffsInfo $staff)
    {
        if (StaffsInfo::find($staff->id) && SchoolInformation::find($staff->school_id)){
            $name = '<a href="' . route('wda.view.school.staff.profile', ['id' => $staff->school_id, 'staffid' => $staff->id]) . '">' . ucwords($staff->first_name . ' ' . $staff->last_name) . '</a>';
        }else
            $name = ucwords($staff->first_name . ' ' . $staff->last_name);

        if ($staff->photo != null)
            $image = getStaffPhoto($staff->photo);
        else {
            if ($staff->email && strpos($staff->email, "@") !== false)
                $image = Gravatar::get($staff->email);
            else
                $image = asset('img/user_default.png');
        }

        return [
            'photo' => '<img src="' . $image . '" alt="' . ucwords($staff->first_name . ' ' . $staff->last_name) . '" class="img-responsive img-rounded" style="width: 100px;">',
            'names' => $name,
            'school' => $staff->school ? $staff->school->name : "",
            'gender' => $staff->gender,
            'nationID' => $staff->national_id_number,
            'phone_number' => $staff->phone_number,
            'email' => $staff->email,
            'province' => $staff->school ? $staff->school->province : "",
            'district' => $staff->school ? $staff->school->district : "",
            'tvet_trainings' => ucwords($staff->tvet_trainings) == 'Yes'? "Yes" : "No",
        ];
    }
}