<?php

namespace App\Transformers;

use App\Model\Accr\SchoolType;
use App\Model\Accr\SchoolInformation;
use Creativeorange\Gravatar\Facades\Gravatar;
use League\Fractal\TransformerAbstract;

class ListAllSchoolTransformer extends TransformerAbstract {

    public function transform(SchoolInformation $school) {

        $name = '<a href="/schools/'.$school->school_acronym.'" target="_blank" style="color: #333">' . $school->school_name .'</a>';
        

        if($school->school_type) {
            $type = SchoolType::find($school->school_type);
            if($type) {
                $type_str = $type->school_type;
            } else {
                $type_str = '';
            }
        }else {
            $type_str = '';
        }

        $action = '<form method="POST">'.csrf_field().'<input type="hidden" name="_method" value="delete"> <input type="hidden" name="delete_school" value="'.$school->id.'"> <a href="'.route('wda.view.school', $school->id).'" title="View More" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i></a> <a href="'.route('wda.edit.school', $school->id).'" title="Edit" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a> <button type="button" class="btn btn-primary btn-sm" data-school="'.$school->id.'" data-school-name="'.$school->school_name.'" data-toggle="modal" data-target="#assignQualifications"> <i class="fa fa-anchor"></i> </button> <button type="submit" class="btn btn-danger btn-sm pull-right btn-flat" title="Delete" onclick="return window.confirm("Are you sure you want to remove this school:\n '.$school->school_name.'?)"> <i class="fa fa-trash"></i> </button></form>';


        return [
            'school_name' => $name,
            'province' => $school->province ? $school->province : "",
            'district' => $school->district ? $school->district : "",
            'school_type' => $type_str,
            'accreditation' => $school->accreditation_status ? $school->accreditation_status : "",
            'school_activity' => $school->school_activity ? $school->school_activity : "",
            'school_status' => $school->school_status ? $school->school_status : "",
            'phone' => $school->phone,
            'action' => $action,
        ];
    }

}