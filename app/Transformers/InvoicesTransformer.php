<?php

namespace App\Transformers;

use App\Rp\PendingPaymentInvoice;
use League\Fractal\TransformerAbstract;

class InvoicesTransformer extends TransformerAbstract
{
    /**
     * @param PendingPaymentInvoice $invoices
     * @return array
     */
    public function transform(PendingPaymentInvoice $invoices)
    {
        return [
            'code' => $invoices->code,
            'student' => $invoices->student_id,
            'title' => $invoices->name,
            'total' => $invoices->amount,
            'paid_amount' => $invoices->paid,
            'installment' => $invoices->partial ? "Yes" : "No",
            'created_at' => $invoices->created_at->diffForHumans(),
            'updated_at' => $invoices->updated_at->diffForHumans(),
        ];
    }
}