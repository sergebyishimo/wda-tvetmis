<?php

namespace App\Transformers;

use App\Reb;
use App\RebStaffMarkerApplication;
use League\Fractal\TransformerAbstract;

class RebApplicantsTransformer extends TransformerAbstract
{
    /**
     * @param RebStaffMarkerApplication $staff
     * @return array
     */
    public function transform(Reb $staff)
    {
        return [
            'photo' => '<img src="' . getStaffPhoto($staff->photo) . '" alt="' . $staff->name . '" class="img-responsive img-rounded" style="width: 100px;">',
            'names' => '<a href="' . route('examiner.read.staff', $staff->staff_id) . '">' . $staff->name . '</a>',
            'gender' => $staff->gender,
            'nationID' => $staff->national_id_number ?: "Not Provided",
            'phone_number' => $staff->phone_number ?: "Not Provided",
            'school' => $staff->school,
            'course' => $staff->first_subject_applied_for ?: "----",
            'education_program_id' => $staff->sub_field_name,
            'province' => $staff->province,
            'district' => $staff->district,
            'sector' => $staff->sector
        ];
    }
}