<?php

namespace App\Transformers;

use App\StaffAssessorApplication;
use League\Fractal\TransformerAbstract;

class AssessingApplicantsTransformer extends TransformerAbstract
{
    /**
     * @param StaffAssessorApplication $staff
     * @return array
     */
    public function transform(StaffAssessorApplication $staff)
    {
        return [
            'photo' => '<img src="' . getStaffPhoto($staff->photo) . '" alt="' . ucwords($staff->first_name . ' ' . $staff->last_name) . '" class="img-responsive img-rounded" style="width: 100px;">',
            'names' => '<a href="' . route('examiner.read.staff', $staff->sid) . '">' . ucwords($staff->first_name . ' ' . $staff->last_name) . '</a>',
            'gender' => $staff->gender,
            'nationID' => $staff->national_id_number,
            'phone_number' => $staff->phone_number,
            'school' => $staff->school,
            'trade' => $staff->sub_field_name,
            'trade_assessed_before' => $staff->trade_assessed_before ? 'YES' : 'No',
            'province' => $staff->staff ? $staff->staff->province : "",
            'district' => $staff->staff ? $staff->staff->district : "",
            'sector' => $staff->staff ? $staff->staff->sector : "",
            'qualification' => $staff->staff ? $staff->staff->qualification : "",
            'graduated_year' => $staff->staff ? $staff->staff->graduated_year : ""
        ];
    }
}