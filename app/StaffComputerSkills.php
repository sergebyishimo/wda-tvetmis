<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffComputerSkills extends Model
{
    protected $connection = "mysql";

    public $timestamps = false;

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id');
    }
}
