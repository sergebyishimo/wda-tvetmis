<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRejectedStudentEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data, $private;

    public $subject = "RP Application Rejected";
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $private = null)
    {
        $this->data = $data;
        $this->private = $private;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        return $this->view('email.rejected_private', compact('data'));
    }
}
