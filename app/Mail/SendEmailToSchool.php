<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToSchool extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    protected $name;
    protected $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $name, $email)
    {
        $this->data = $data;
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $name = $this->name;
        $email = $this->email;
        return $this->from($email)->view('email.schoolmessage', compact('data', 'name'));
    }
}
