<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMailContinuing extends Mailable
{
    use Queueable, SerializesModels;
    private $name, $stdid, $w, $with;
    private $additional;
    public $subject = "Continuing Student Sign-up Succeed";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $stdid, $where = null, $with = null, $additional=null)
    {
        $this->name = $name;
        $this->stdid = $stdid;
        $this->w = $where;
        $this->with = $with;
        $this->additional = $additional;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->name;
        $stdid = $this->stdid;

        $attachment = $this->with;
        $additional = $this->additional;
        $view = $this->w == null ? "email.register" : "";
        $rep = $this->view('email.signup_continuing', compact('name', 'stdid', 'attachment', 'additional'));
        return $rep;
    }
}
