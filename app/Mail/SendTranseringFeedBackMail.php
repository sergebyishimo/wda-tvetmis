<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTranseringFeedBackMail extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = "Transfer request feedback";
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = "email.rejected_transfer";
        if (isset($this->data['re']) && $this->data['re'] == "a")
            $view = "email.accept_transfer";
        return $this->view($view);
    }
}
