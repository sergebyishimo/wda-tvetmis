<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdmittedEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data, $private;

    public $subject = "RP Admission Confirmation";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $private = null)
    {
        $this->data = $data;
        $this->private = $private;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;

        if ($this->private === true)
            $view = 'email.admitted_private';
        else
            $view = 'email.admitted';

        return $this->view($view, compact('data'));
    }
}
