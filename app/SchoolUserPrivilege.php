<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolUserPrivilege extends Model
{
    protected $connection = "mysql";
    public function schoolUser()
    {
        return $this->belongsTo(SchoolUser::class);
    }
}
