<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComputedAggregate extends Model
{
    protected  $table = "nationa_exam_computed_aggregates";
    protected  $fillable = ['student_index','total_aggregates','school_id','combination_id','total_marks'];

    public function mark(){
        return $this->belongsTo(NationalExamResult::class,'student_index');
    }
}
