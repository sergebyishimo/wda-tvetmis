<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffProfileAttachmentsFinal extends Model
{
    public $timestamps = false;

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }

    public function description()
    {
        return $this->belongsTo(Attachment::class, 'attachment_description', 'id');
    }

    public function getNameAttribute()
    {
        if ($this->description)
            return $this->description->name;
        else
            return "";
    }
}
