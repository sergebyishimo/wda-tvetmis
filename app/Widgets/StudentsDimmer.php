<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use App\Student;
use TCG\Voyager\Widgets\BaseDimmer;

class StudentsDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Student::where('status', 'active')->count();
        $string = trans_choice('strings.dimmer.student', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'voyager-people',
            'title' => "{$count} {$string}",
            'text' => __('strings.dimmer.student_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('strings.dimmer.student_link_text'),
                //'link' => route('voyager.schools.index'),
                'link' => '',
            ],
            'image' => voyager_asset('images/widget-backgrounds/01.jpg'),
        ]));
    }
}
