<?php
/**
 * Created by PhpStorm.
 * User: hirwaf
 * Date: 5/12/18
 * Time: 11:39 PM
 */

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use App\School;

class SchoolDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = School::count();
        $string = trans_choice('strings.dimmer.school', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'voyager-bookmark',
            'title' => "{$count} {$string}",
            'text' => __('strings.dimmer.school_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('strings.dimmer.school_link_text'),
                'link' => route('voyager.schools.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/02.jpg'),
        ]));
    }
}