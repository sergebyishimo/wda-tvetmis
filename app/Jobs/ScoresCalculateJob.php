<?php

namespace App\Jobs;

use App\QueryCacher;
use App\Repositories\Assessment\GetDataAssessmentScores;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ScoresCalculateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $repoInput = new GetDataAssessmentScores(request(), 'i', [], false, false);
        $repoProcess = new GetDataAssessmentScores(request(), 'p', [], false, false);

        $input = json_encode($repoInput->getResults());
        $process = json_encode($repoProcess->getResults());

        if (!empty($repoInput->getResults()))
            QueryCacher::updateOrCreate([
                'query' => 'input_scores'
            ], [
                    'query' => 'input_scores',
                    'results' => $input
                ]
            );

        if (!empty($repoProcess->getResults()))
            QueryCacher::updateOrCreate([
                'query' => 'process_scores'
            ], [
                    'query' => 'process_scores',
                    'results' => $process
                ]
            );
    }
}
