<?php

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

function school($string = "", $skul = null)
{
    if ($string != "" && $string !== true) {
        if (auth()->guard('school')->check())
            $string = auth()->guard('school')->user()->school->{$string} ?: "";
    } elseif ($string === true) {
        return auth()->guard('school')->user()->school;
    }

    return $string;
}

function activeMenu($route = "")
{
    if (\Request::route()) {
        $name = \Request::route()->getName();
        if ($route != "" && $name == $route && !is_array($route))
            return "active";
        elseif (is_array($route) && in_array($name, $route, TRUE) == TRUE)
            return "active";
    }

    return "";
}

function getSchoolUserPrivilege($staff)
{
    $p = "";

    if ($staff != null || $staff != "") {
        $select = \DB::table('staff_privileges')->where('school_id', school(true)->id)
            ->where('staff_info_id', $staff)
            ->orderBy("created_at", "ASC");
        if ($select->count() > 0) {
            foreach ($select->get() as $item) {
                $pp = \App\SchoolUserPrivilege::find($item->school_user_privilege_id);
                if ($pp != null)
                    $p .= $pp->name . ", ";
            }
        }
    }

    return rtrim($p, ', ');
}

function getQualifications($schoolL = "")
{
    $department = [];
    $school = school(true)->id;
    if ($schoolL == "") {
        $qual = \App\Qualification::all();
        if ($qual->count() > 0) {
            foreach ($qual as $item) {
                $dc = \App\Department::where('school_id', $school)->where('qualification_id', $item->id);
                if ($dc->count() <= 0)
                    $department[] = $item;
            }
        }
    } elseif ($school == $schoolL) {
        $op = school(true)->departments;
        if ($op->count() > 0) {
            foreach ($op as $item) {
                $q = $item->qualification;
                $department[] = $q;
            }
        }
    }

    return $department;
}

function getSchoolTeach()
{
    $teacher = [];
    $school = school(true)->id;
    $staff = school(true)->staffs;
    $pt = 7;
    if ($staff->count() > 0) {
        foreach ($staff as $item) {
            $ch = \DB::table('staff_privileges')->where('school_id', $school)
                ->where('staff_info_id', $item->id)
                ->where('school_user_privilege_id', $pt);
            if ($ch->count() > 0)
                $teacher[] = $item;
        }
    }

    return $teacher;
}

function checkIfHaveLevel($l, $q)
{
    $school = school(true);
    $dep = $school->departments()->where('qualification_id', $q);
    if ($dep->count() > 0) {
        $dep = $dep->first()->id;
        $check = $school->levels()->where('rtqf_id', $l)->where('department_id', $dep);
        return $check->count() > 0 ? true : false;
    }
    return false;
}

function getDepartments($arr = false)
{
    $dep = school(true)->departments()->where('status', '1');
    if ($arr === false)
        return $dep;
    elseif ($arr === true) {
        $arrs = [];
        if ($dep->count() > 0) {
            foreach ($dep->get() as $item) {
                $arrs[$item->id] = $item->qualification->qualification_title;
            }
        }
        return $arrs;
    }

}

function getDepartmentsBySchoolId($school_id, $arr = false)
{
    $dep = \App\School::where('id', $school_id)->first()->departments();
    if ($arr === false)
        return $dep;
    elseif ($arr === true) {
        $arrs = [];
        if ($dep->count() > 0) {
            foreach ($dep->get() as $item) {
                $arrs[$item->id] = $item->qualification->qualification_title;
            }
        }
        return $arrs;
    }
}

function getTerm($current = false)
{
    $term = ['1' => 'Term 1', '2' => 'Term 2', '3' => 'Term 3'];
    if ($current === true)
        return isset($term[school(true)->settings->active_term]) ? school(true)->settings->active_term : 1;

    if ($current === 4)
        $term['4'] = "All Terms";

    return $term;
}

function getPeriod($current = false)
{
    $periods = ['1' => 'Period 1', '2' => 'Period 2', '3' => 'Period 3', '6' => 'Exam Period'];

    if ($current === true)
        return isset($periods[school(true)->settings->active_period]) ? school(true)->settings->active_period : 1;

    if (is_int($current) && $current <= 6 && isset($periods[$current]))
        return $periods[$current];

    return $periods;
}

function getMyCourseInfo($module, $level, $term, $marks = true, $col = "")
{
    $courses = school(true)->courses()
        ->where('module_id', $module)
        ->where('level_id', $level)
        ->where("term", $term);
    if ($courses->count() > 0) {
        if ($marks === true)
            return $courses->first()->max_point;
        elseif ($marks === false) {
            $ko = $courses->first();
            if ($col == "")
                return $ko;
            else
                return $ko->{$col} ?: "";
        }
    }

    return "";
}

function getSchoolLevels($arr = false)
{
    if ($arr !== true) {
        return school(true)->levels;
    } else {

    }
}

function getProvince($array = false)
{
    $response = \DB::table('rwanda_boundaries')
        ->select("Province")
        ->groupBy("Province")
        ->get();
//    dd($response);
    if ($array === true) {
        $arr = [];
        if ($response->count() > 0) {
            foreach ($response as $item) {
                $arr[$item->Province] = $item->Province;
            }
        }
        return $arr;
    }

    return $response;
}
function getProvinceSchool($array = false)
{
    $response = \DB::select( \DB::raw(" SELECT province,COUNT(*) AS total FROM `accr_schools_information` GROUP BY province") );
//    dd($response);
    if ($array === true) {
        $arr = [];
        if ($response->count() > 0) {
            foreach ($response as $item) {
                $arr[$item->Province] = $item->Province;
//                dd($arr[$item->Province]);
            }
        }
        return $arr;
    }

    return $response;
}
function getDistrictSchool($s = null)
{
    if ($s == null) {
        $response = \DB::select(
            \DB::raw("SELECT District,COUNT(*) AS total FROM `accr_schools_information` GROUP BY District") );

        return $response;
    } else {
        $response = \DB::select(
            \DB::raw("SELECT District,COUNT(*) AS total FROM `accr_schools_information` WHERE Province='$s' GROUP BY District") );


        return $response;
    }
}
function getSectorSchool($s = null)
{

    $response = \DB::select(\DB::raw("SELECT sector,COUNT(*) AS total FROM `accr_schools_information` WHERE District='$s' GROUP BY Sector") );

        return $response;

}
function getSchoolType($s = null)
{
        $response = \DB::select(
            \DB::raw("SELECT `accr_source_school_type`.`school_type`,COUNT(*) AS `total` FROM `accr_schools_information` JOIN `accr_source_school_type` ON `accr_schools_information`.`school_type` = `accr_source_school_type`.`id` WHERE `sector`='$s' GROUP BY `accr_source_school_type`.`school_type`") );

        return $response;
}

function getDistrict($s = null)
{
    if ($s == null) {
        $response = \DB::table('rwanda_boundaries')
            ->select("District")
            ->groupBy("District")
            ->get();
        return $response;
    } else {
        $response = \DB::table('rwanda_boundaries')
            ->select("District")
            ->where("Province", $s)
            ->groupBy("District")
            ->get();
        return $response;
    }
}

function getSector($s)
{
    $response = \DB::table('rwanda_boundaries')
        ->select("Sector")
        ->where("District", $s)
        ->groupBy("Sector")
        ->get();

    return $response;
}

function getCell($s)
{
    $response = \DB::table('rwanda_boundaries')
        ->select("Cell")
        ->where("Sector", $s)
        ->groupBy("Cell")
        ->get();

    return $response;
}

function getVillage($s)
{
    $response = \DB::table('rwanda_boundaries')
        ->select("Village")
        ->where("Cell", $s)
//        ->groupBy("Village")
        ->get();

    return $response;
}

function getSponsors()
{
    return [
        'Self Sponsored' => 'Self Sponsored',
        'Government Sponsored' => 'Government Sponsored',
        'School Sponsored' => 'School Sponsored'
    ];
}

function getInsurance()
{
    return [
        'Mutelle' => 'Mutelle',
        'RAMA' => 'RAMA',
        'MMI' => 'MMI',
        'UR Insurance' => 'UR Insurance'
    ];
}

function getUbudehe()
{
    return [
        'Category One' => 'Category One',
        'Category Two' => 'Category Two',
        'Category Three' => 'Category Three',
        'Category Four' => 'Category Four',
    ];
}

function generateReg($kop = 0)
{
    $q = true;
    $add = 1;
    while ($q) {
        $op = \App\Student::withTrashed()->count() + $add + $kop;
        $opp = "";
        if ($op <= 9)
            $opp = "0000" . $op;
        elseif ($op < 100)
            $opp = "000" . $op;
        elseif ($op < 999)
            $opp = "00" . $op;
        elseif ($op < 9999)
            $opp = "0" . $op;
        else
            $opp = $op;
        $op = date('y') . "RP";
        $res = $op . $opp;
        if (\App\Student::withTrashed()->where('reg_no', $res)->count() <= 0)
            break;
        else
            $add++;
    }
    return $res;
}

function getFeeStudentsFormat()
{
    $response = [];
    $students = school(true)->students()->where('status', "active")
        ->orderBy('fname', "ASC")
        ->orderBy('lname', 'ASC')
        ->orderBy('created_at', "DESC");

    if ($students->count() > 0) {
        foreach ($students->get() as $student) {
            $response[$student->id] = $student->reg_no . " - " . trim(ucwords($student->fname . " " . $student->lname));
        }
    }

    return $response;
}

function getStaffsFormat()
{
    $response = [];
    $staffs = school(true)->staffs()->where('status', "active")
        ->orderBy('first_name', "ASC")
        ->orderBy('last_name', 'ASC')
        ->orderBy('created_at', "DESC");

    if ($staffs->count() > 0) {
        foreach ($staffs->get() as $staff) {
            $response[$staff->id] = trim(ucwords($staff->first_name . " " . $staff->last_name));
        }
    }

    return $response;
}

function getStudentPhoto($student)
{
    $photo = null;
    if (is_object($student))
        $photo = $student->photo;
    elseif (is_string($photo))
        $photo = $student;

    if ($photo != null) {
        if (\Storage::disk(config('voyager.storage.disk'))->exists($photo))
            return asset('storage/' . $photo);
        else
            return asset('img/user_default.png');

    } else {
        $arr = [
            $student->reg_no . ".jpg",
            $student->reg_no . ".png",
            $student->reg_no . ".jpeg"
        ];
        $f = asset('img/user_default.png');
        foreach ($arr as $file) {
            if (\Storage::disk(config('voyager.storage.disk'))->exists("students/" . $file)) {
                $f = asset('storage/students/' . $file);
                break;
            }
        }

        return $f;
    }
}

function getStaffPhoto($staff = null)
{
    if (is_object($staff)) {
        if ($staff->photo != null) {
            if (\Storage::disk(config('voyager.storage.disk'))->exists($staff->photo))
                return asset('storage/' . $staff->photo);
        }
    } else {
        if ($staff) {
            if (\Storage::disk(config('voyager.storage.disk'))->exists($staff))
                return asset('storage/' . $staff);
        }
    }

    return asset('img/photo_default.png');
}

function getStaffDocument($staff, $type)
{
    $document = "";
    $arr = [
        $staff->id . ".jpg",
        $staff->id . ".png",
        $staff->id . ".jpeg"
    ];
    $f = "";
    if ($type == 'a')
        $f = "staffs/documents/academic/";
    elseif ($type == 'c')
        $f = "staffs/documents/others/";

    foreach ($arr as $file) {
        if (\Storage::disk(config('voyager.storage.disk'))->exists($f . $file)) {
            $document = asset('storage/' . $f . $file);
            break;
        }
    }

    return $document;
}

function checkIfCompletedInfo()
{
    if (!isAllowed(1) && !isAllowed()) {
        if (getStaff('updated_at') == getStaff('created_at') || getStaff('national_id_number') == null || getStaff('photo') == null) {
            return false;
        }
    }

    return true;
}

function academicYear($start = null, $end = null, $normal = false)
{
    if ($normal !== true)
        if ($end != null) {
            $i = $end + 1;
        } else {
            $i = date('Y') + 1;
        }
    else {
        if ($end != null) {
            $i = $end;
        } else {
            $i = date('Y');
        }
    }

    $ar = [];
    if ($normal === true) {
        for (; $i >= ($start ?: 2008); $i--) {
            $ar[$i] = $i;
        }
    } else {
        for (; $i >= ($start ?: 2008); $i--) {
            $ar[($i - 1) . ' - ' . $i] = ($i - 1) . ' - ' . $i;
        }
    }

    return $ar;
}

function availableStatus()
{
    return [
        'Active' => 'Active',
        'Alumni' => 'Alumni'
    ];
}

function getExpectedFees($level, $term, $res, $school = null)
{
    if ($school == null)
        $school = school(true);

    $fees = $school->fees()
        ->where('level_id', $level)
        ->where('term', $term);
    if ($fees->count() > 0)
        return $fees->first()->{$res} ?: 0;

    return 0;
}

function getPaidFees($student, $term, $year, $res = "amount", $school = null)
{
    if ($school == null)
        $school = school(true);

    $fees = $school->payments()
        ->where('student_id', $student)
        ->whereYear('created_at', '=', $year);
    if ($term != 'a')
        $fees = $fees->where('term', $term);

    if ($fees->count() > 0)
        return $fees->sum($res) ?: 0;

    return 0;
}

function getRemainPayment($e, $p)
{
    return $e - $p;
}

function getDisciplineMarks($student, $term = null, $year = null)
{
    $term = $term == null ? school(true)->settings->active_term : $term;
    $year = $year == null ? date('y') : $year;
    $cm = school('discipline_max') ?: 40;
    $check = school(true)->discipline()
        ->where('student_id', $student)
        ->where('acad_year', $year)
        ->where('term', $term);

    if ($check->count() > 0)
        $cm = $check->first()->marks;

    return $cm;
}

function getDisciplineHistory($student, $year = null, $term = null)
{
    $term = $term == null ? school(true)->settings->active_term : $term;
    $year = $year == null ? date('y') : $year;
    $go = school(true)->disciplineHistory()
        ->where('student_id', $student)
        ->where('academic_year', $year)
        ->where('term', $term)
        ->orderBy('created_at');

    if ($go->count() > 0)
        return $go;

    return [];
}

function getStudentGPhone($student)
{
    if ($student != null) {
        if ($student->ft_phone)
            return $student->ft_phone;
        elseif ($student->mt_phone)
            return $student->mt_phone;
        elseif ($student->gd_phone)
            return $student->gd_phone;
    }
    return null;
}

function sendSMS($numbers, $message, $type = null, $aschool = null)
{
    $client = new \GuzzleHttp\Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://api.rmlconnect.net/bulksms/bulksms',
        // You can set any number of default request options.
        'timeout' => 100.0,
    ]);
    $parms = [
        'username' => 'gofir',
        'password' => '0vTuzdce',
        'type' => 0,
        'dlr' => 1,
        'destination' => validSMSNumber($numbers),
        'source' => $aschool == null ? school('acronym') : $aschool->acronym,
        'message' => $message
    ];

    $uri = "http://api.rmlconnect.net/bulksms/bulksms?" . http_build_query($parms);
    $res = $client->get($uri);
    $resp = $res->getBody()->getContents();

    $cResp = explode("|", $resp);
    if ($cResp[0] == '1701') {
        \App\Outbox::create([
            'school_id' => $aschool == null ? school('id') : $aschool->id,
            'type' => $type,
            'acad_year' => date('Y'),
            'term' => $aschool == null ? settings('active_term') : $aschool->settings->active_term,
            'period' => $aschool == null ? settings('active_period') : $aschool->settings->active_period,
            'receiver' => $numbers,
            'message' => $message
        ]);
        return true;
    }
    return false;
}

function validSMSNumber($num)
{
    if (preg_match('[^\+250|250]', $num))
        return trim($num, "+");
    else {
        if ($num{0} == '0')
            return "25" . $num;
        else
            return "250" . $num;
    }
}

function getPeriodMarksType()
{
    $type = ['Quiz' => 'Quiz', 'Test' => 'Test', 'Work' => 'Work'];

    return $type;
}

function isAllowed($p = 2)
{
    if (\Auth::guard('school')->check())
        return \Auth::guard('school')->user()->privilege == $p;

    return false;
}

function getStaff($col = null, $related = null)
{
    if (\Auth::guard('school')->check()) {
        $emailL = \Auth::guard('school')->user()->email;
        $staff = school(true)->staffs()->where('email', $emailL);
        if ($staff->count() > 0) {
            $f = $staff->first();
            if ($related != null) {
                $rf = $f->{$related};
                if ($rf)
                    $f = $rf;
            }
            if ($col != null) {
                if (isset($f->{$col}))
                    return $f->{$col};
            } else
                return $staff;
        }
    }

    return "";
}

function getStaffTrainingReference($staff, $training)
{
    $model = \App\StaffRttiTrainingReference::where('staff_id', $staff)
        ->where('training_reference_id', $training)
        ->first();
    if ($model)
        return $model->response;

    return "";
}

function getChildrenRage($staff, $range = null)
{
    $staff = \App\StaffsInfo::find($staff);
    if ($staff) {
        if ($staff->detail) {
            $ranges = $staff->detail->age_range_children;
            if ($ranges) {
                $ranges = explode(',', $ranges);
                if ($range != null) {
                    if (in_array($range, $ranges))
                        return $range;
                    else
                        return false;
                }
                return $ranges;
            }
        }
    }

    return null;
}

function settings($col = null)
{
    if ($col === null)
        return school(true)->settings;
    else
        return school(true)->settings->{$col} ?: "";
}

function getGrade($result)
{
    if ($result >= 75) {
        echo "A";
    } elseif ($result >= 60) {
        echo "B";
    } elseif ($result >= 45) {
        echo "C";
    } elseif ($result >= 30) {
        echo "D";
    } elseif ($result >= 15) {
        echo "E";
    } elseif ($result >= 10) {
        echo "S";
    } else {
        echo "F";
    }
}

function getPOB()
{
    return "2280 KIGALI";
}

function num_2_format($num)
{
    return number_format($num, 2);
}

function getAttendanceEvent($k = null)
{
    $events = ['p' => 'Prep', 'm' => 'Meeting', 'r' => 'Refectory'];
    if (isset($events[$k]))
        return $events[$k];

    return $events;
}

function getUpdateVersionTables()
{
    $tables = [
        'students', 'school_users', 'school_settings', 'courses', 'staffs_info',
        'departments', 'levels'
    ];

    return $tables;
}

function updateVersionColumn($table, $checking = false, $school = null)
{
    if (in_array($table, getUpdateVersionTables())) {
        if ($school == null)
            $up = school(true)->update_version();
        else {
            $user = \App\School::find($school);
            $up = $user->update_version();
        }
        $cv = $checking === true ? false : 1;
        if ($up->count() > 0) {
            $cvo = $up->where('name', $table);
            if ($cvo->count() > 0)
                $cv = $checking === true ? true : $cvo->first()->updateVersion;
        }
    }
    return $cv;
}

function addUpdateVersionTableNamesToSchool()
{
    $tb_names = getUpdateVersionTables();
    if (count($tb_names) > 0) {
        foreach ($tb_names as $table) {
            $ck = updateVersionColumn($table, true);
            if ($ck === false) {
                \App\UpdateVersion::create([
                    'school_id' => school('id'),
                    'name' => $table,
                    'updateVersion' => 1
                ]);
            }
        }
    }
}

function trigerSchoolUpdateVersion()
{
    $cUpS = school(true)->update_version->count();
    $cTn = count(getUpdateVersionTables());
    if ($cUpS < $cTn)
        addUpdateVersionTableNamesToSchool();
}

function classAttendance($level, $course, $month, $reg)
{
    return new \App\Repositories\ClassAttendanceRepository($level, $course, $month, $reg);
}

function eventAttendance($month, $student, $type)
{
    return new \App\Repositories\EventAttendanceRepository($month, $student, $type);
}

function inOutAttendance($start, $end, $level)
{
    return new \App\Repositories\InOutAttendanceRepository($start, $end, $level);
}

function staffAttendance($start, $end, $staff = null)
{
    return new \App\Repositories\StaffAttendanceRepository($start, $end, $staff);
}

function getAdditionalFees($term, $year, $label = null, $department = null, $level = null, $student = null)
{
    $additional = school(true)
        ->additionalFees()
        ->where('term', $term)
        ->where('acad_year', $year);
    if ($label != null)
        $additional->where('label', $label);
    if ($department != null)
        $additional->where('department_id', $department);
    if ($level != null)
        $additional->where('level_id', $level);
    if ($student != null)
        $additional->where('student_id', $student);
    if ($additional->count() > 0)
        return $additional->get();
    else
        return null;
}

function getStudentAdditionalFees($term, $year, $student)
{
    $level = $student->level_id;
    $department = $student->level->department_id;
    $additional = school(true)
        ->additionalFees()
        ->where('term', $term)
        ->where('acad_year', $year)
        ->orWhere('student_id', $student->id)
        ->orWhere('department_id', $department)
        ->orWhere('level_id', $level);
    if ($additional->count() > 0)
        return $additional->get();
    else
        return null;
}

function getAttachmentDescriptionForSelect()
{
    $tr = [];
    $aty = \App\Attachment::all(['id', 'name']);
    if ($aty) {
        foreach ($aty as $item) {
            $tr[$item->id] = $item->name;
        }
    }

    return $tr;
}

function getMarkingInstitution()
{
    return [
        'WDA' => 'WDA',
        'REB' => 'REB',
        'OTHER' => 'OTHER'
    ];
}

function getYesNo($get = null)
{
    $arr = [
        '1' => 'Yes',
        '2' => 'No'
    ];

    if ($get != null && isset($arr[$get]))
        return $arr[$get];

    return $arr;
}

function getTraMarker($id)
{

}

function getSelectTradeMarker($id = null)
{
    $po = [];
    $source = \App\SourceTvetSubField::orderBy('sub_field_name', 'ASC');
    if ($source->count() > 0) {
        foreach ($source->get() as $item) {
            $po[$item->id] = $item->sub_field_name;
        }
    }
    if ($id != null && isset($po[$id]))
        return $po[$id];

    return $po;
}

function getPosition($id = null)
{
    $pos = [];
    $table = \DB::table('source_staff_marking_assessment_positions')->orderBy('position', "ASC");
    if ($table->count() > 0) {
        foreach ($table->get() as $item) {
            $pos[$item->id] = $item->position;
        }
    }

    if ($id != null && isset($pos[$id]))
        return $pos[$id];

    return $pos;
}

function getSubjectExamMarked($id = null)
{
    $sub = [];
    $table = \App\TssSubjects::orderBy('option_trade', 'ASC')->get();
    if ($table->count() > 0) {
        foreach ($table as $item) {
            $sub[$item->id] = $item->option_code . " - " . $item->subject;
        }
    }
    if ($id != null && isset($sub[$id]))
        return $sub[$id];
    return $sub;
}

function getLevelTaught($id = null)
{
    $lv = [];
    $table = \DB::table('source_student_year_of_study')->select('*')->get();
    if ($table->count() > 0) {
        foreach ($table as $item) {
            $lv[$item->id] = $item->year_of_study;
        }
    }
    if ($id != null && isset($lv[$id]))
        return $lv[$id];

    return $lv;
}

function getStatusWorkExperience($id = null)
{
    $lv = [];
    $table = \DB::table('source_staff_work_status')->select('*')->get();
    if ($table->count() > 0) {
        foreach ($table as $item) {
            $lv[$item->id] = $item->work_status;
        }
    }
    if ($id != null && isset($lv[$id]))
        return $lv[$id];

    return $lv;
}

function getInfoReb($col = '')
{
    if (auth()->guard('reb')->check()) {
        $user = auth()->guard('reb')->user();
        $user = \App\Reb::findOrFail($user->id);
        if ($col != "")
            return $user->{$col};

        elseif ($col === true)
            return $user;
    }

    return "";
}

function getMarkingActivation($col = '')
{
    $active = \App\MarkingActivation::all();
    if ($active) {
        $active = $active->first();

        if (isset($active->{$col}))
            return $active->{$col};
    }

    return 0;
}

function getPrivilege($id)
{
    $po = \App\SchoolUserPrivilege::find($id);
    if ($po)
        return $po->name;

    return "";
}

function getStudyQualification($co = null)
{
    $rt = [
        "a3" => "A3",
        'a2' => 'A2',
        'a1' => 'A1',
        "a0" => 'A0',
        "master" => "Master",
        "phd" => "PHD",
        "other" => "Other"
    ];
    return $co != null ? isset($rt[$co]) ? $rt[$co] : "" : $rt;
}

function reverseSlug($slug)
{
    return implode(" ", explode('-', str_replace("#", "", $slug)));
}

function slugify($string)
{
    return str_replace(" ", "-", str_replace("#", "", $string));
}

function getLangRating($co = null)
{
    $rt = [
        "1" => "Poor",
        '2' => 'Adequate',
        '3' => 'Good',
        "4" => 'Very Good',
        "5" => "Excellent"
    ];
    return $co != null ? isset($rt[$co]) ? $rt[$co] : "" : $rt;
}

function getLanguage($co = null)
{
    $rt = [
        "English" => "English",
        'French' => 'French',
        'Kinyarwanda' => 'Kinyarwanda',
        "Swahili" => 'Swahili'
    ];
    return $co != null ? isset($rt[$co]) ? $rt[$co] : "" : $rt;
}

function checkIfDistrictHasAccount($district)
{
    $check = \App\District::where('district', $district)->first();
    if ($check)
        return true;
    return false;
}

function getRtqfLevels()
{
    $rtqf_levels = \App\Rtqf::pluck('level_name', 'id');
    return $rtqf_levels;
}

// Curriculum sector & sub sectors

function getWSectors()
{
    $sector = \App\Sector::pluck('sector_name', 'id');
    return $sector;
}

function getWSubSectors()
{
    $subsector = \App\SubSector::pluck('sub_sector_name', 'id');
    return $subsector;
}

function checkIfSync($model, $uuid = null)
{
    $sync = false;

    if ($model == 'se')
        $sync = \App\Sector::find($uuid);
    elseif ($model == 'sb')
        $sync = \App\SubSector::find($uuid);
    elseif ($model == "rt")
        $sync = \App\Rtqf::find($uuid);
    elseif ($model == 'qu')
        $sync = \App\Qualification::find($uuid);

    if (!is_null($sync))
        return true;

    return false;
}

function getUUIDFrom($model, $id)
{
    $sync = null;

    if ($model == 'se')
        $sync = \App\Model\Accr\TrainingSectorsSource::find($id);
    elseif ($model == 'sb')
        $sync = \App\Model\Accr\TrainingTradesSource::find($id);
    elseif ($model == "rt")
        $sync = \App\Model\Accr\RtqfSource::find($id);
    elseif ($model == 'qu')
        $sync = \App\Model\Accr\CurriculumQualification::find($id);

    if (!is_null($sync))
        return $sync->uuid;

    return $sync;
}

function getMyRqtf($q)
{
    $arr = [];
    $qua = \App\QualificationRtqf::where('qualification_id', $q);
    if ($qua->count() > 0) {
        foreach ($qua->get() as $item) {
            $arr[] = $item->rtqf_id;
        }
    }

    return $arr;
}

function hasQualification($school, $q)
{
    $dept = \App\Department::where('school_id', $school)->where('qualification_id', $q)->first();
    if ($dept)
        return true;

    return false;
}

function hasRqtf($school, $d, $r)
{
    $check = \App\Level::where('school_id', $school)->where('department_id', $d)->where('rtqf_id', $r)->first();
    if ($check)
        return true;

    return false;
}

function getDepartmentFrom($school, $qua, $col)
{
    $check = \App\Department::where('school_id', $school)
        ->where('qualification_id', $qua)->first();
    if ($check)
        return $check->{$col};

    return null;
}

function getIDFromUUID($model, $uuid)
{
    $sync = null;

    if ($model == 'se')
        $sync = \App\Model\Accr\TrainingSectorsSource::where('uuid', $uuid);
    elseif ($model == 'sb')
        $sync = \App\Model\Accr\TrainingTradesSource::where('uuid', $uuid);
    elseif ($model == "rt")
        $sync = \App\Model\Accr\RtqfSource::where('uuid', $uuid);
    elseif ($model == 'qu')
        $sync = \App\Model\Accr\CurriculumQualification::where('uuid', $uuid);

    if (!is_null($sync))
        return $sync->first()->id;

    return $sync;
}

function getFromSchool($school, $col = null, $model = '1')
{
    if ($model === '1')
        $school = \App\Model\Accr\SchoolInformation::find($school);
    elseif ($model === '2')
        $school = \App\School::find($school);

    if ($col != null) {
        if ($school)
            return $school->{$col} ?: "";
    }

    return $school;
}

function storageHas($file, $disk = null)
{
    if ($disk == null)
        $disk = config('voyager.storage.disk');

    $check = \Storage::disk($disk)->exists($file);

    return $check;
}

function givemeLevels($school)
{
    $levels = getFromSchool($school->id, 'levels', '2');
    $arr = [];
    foreach ($levels as $level) {
        array_push($arr, $level->rtqf->pluck('level_name'));
    }
    if (empty($arr)) {
        $mlevel = Collection::make($arr);
        if (isset($mlevel->unique('level_name')[0]))
            return $mlevel->unique('level_name')[0];
    }

    return null;
}

function getRating($r)
{
    switch ($r) {
        case 1 :
            return "Poor";
            break;
        case 2 :
            return "Adequate";
            break;
        case 3 :
            return "Good";
            break;
        case 4 :
            return "Very Good";
            break;
        case 5 :
            return "Excellent";
            break;
        default:
            return "";
            break;
    }
}

function getSearchModel($model = null)
{
    $models = [
        'school' => 'School Information',
//        'staffs' => 'School Staffs',
//        'student' => 'School Students'
    ];
    if ($model != null)
        $models = isset($models[$model]) ? $models[$model] : "";

    return $models;
}

function getModelFormKey($model)
{
    $models = [
        'school' => new \App\Model\Accr\SchoolInformation(),
        'staffs' => new \App\StaffsInfo(),
        'student' => new \App\Student()
    ];
    if ($model != null) {
        $model = isset($models[$model]) ? $models[$model] : null;
        return $model;
    }

    return null;
}

function getConditionSearch()
{
    return [
        '=' => 'Equal to',
        '<>' => 'Not Equal',
        '#' => 'Count',
        '0' => 'Empty',
        '00' => 'Not Empty',
        '>' => 'Greater than',
        '<' => 'Less than',
        '>=' => 'Greater than or Equal to',
        '<=' => 'Less than or Equal to',

    ];
}

function getStringFromKeY($v)
{
    return ucwords(str_replace('_', ' ', $v));
}

function getStudentsNber($rtqf_id, $qualification = null, $school = null, $additional = null)
{
    $rtqf_source = App\Model\Accr\RtqfSource::find($rtqf_id);

    $rtqf = App\Rtqf::find($rtqf_source->uuid);
    $levels = App\Level::where('rtqf_id', $rtqf->id)->get();
    if ($qualification) {
        $levels = \DB::table('levels')
            ->join('departments', 'levels.department_id', '=', 'departments.id')
            ->join('accr_curriculum_qualifications', 'departments.qualification_id', '=', 'accr_curriculum_qualifications.uuid')
            ->where('levels.rtqf_id', $rtqf->id)
            ->where('accr_curriculum_qualifications.uuid', $qualification)
            ->select([
                'levels.id', 'accr_curriculum_qualifications.uuid', 'levels.rtqf_id'
            ])
            ->get();
    }
    $students = 0;
    foreach ($levels as $level) {
        $query = App\Student::where('level_id', $level->id)->where('acad_year', date('Y'));
        if ($school)
            $query->where('school', $school);
        if ($additional && is_array($additional)) {
            foreach ($additional as $item) {
                $field = isset($item['key']) ? $item['key'] : null;
                $operator = isset($item['operator']) ? $item['operator'] : null;
                $needed = isset($item['needed']) ? $item['needed'] : null;
                if ($field && $operator && $needed)
                    $query->where($field, $operator, $needed);
            }
        }
        $students += $query->count();
    }

    return $students;
}

function getQualificationSchools($quali_id, $counting = true)
{
    $quali_source = App\Model\Accr\CurriculumQualification::find($quali_id);
    if ($counting) {
        $count = App\Department::where('qualification_id', $quali_source->uuid)->count();
        return $count;
    } else {
        $depart = App\Department::where('qualification_id', $quali_source->uuid)->get();
        if ($depart->count())
            return $depart->unique('school_id');

        return null;
    }
}

function getQualificationStudents($quali_id, $counting = true, $schoolID = null)
{
    $quali_source = App\Model\Accr\CurriculumQualification::find($quali_id);
    $schools = App\Department::where('qualification_id', $quali_source->uuid)->select('school_id')->get();
    if ($counting) {
        $count = 0;
        foreach ($schools as $school) {
            if ($schoolID != null) {
                if ($schoolID == $school->school_id)
                    $count += App\Student::where('school', $school->school_id)->count();
            } else
                $count += App\Student::where('school', $school->school_id)->count();
        }
        return $count;
    } else {
        $students = collect();;
        foreach ($schools as $school) {
            $op = App\Student::where('school', $school->school_id)->get();
            if ($op->count() > 0) {
                foreach ($op as $foo) {
                    if ($schoolID != null) {
                        if ($schoolID == $school->school_id)
                            $students->push($foo);
                    } else
                        $students->push($foo);
                }
            }
        }
        return $students;
    }
}

function countRemainToSend($from = "admitted", $email = null)
{
    $plunk = \App\Rp\EmailSended::all()->pluck('email');

    if ($from == 'admitted') {
        if ($plunk->count() > 0)
            $model = \App\Rp\AdmittedStudent::where('year_of_study', date('Y'))
                ->whereNotIn('email', $plunk)
                ->get();
        else
            $model = \App\Rp\AdmittedStudent::where('year_of_study', date('Y'))->get();
    }

    $count = $model ? $model->count() : "";

    return $count;
}

function getStudent($student, $col = null)
{
    $padm = \App\Model\Rp\AdmissionPrivate::where('std_id', $student)->first();
    $adm = \App\Rp\AdmittedStudent::where('std_id', $student)->first();
    $cst = \App\Rp\CollegeStudent::where('student_reg', $student)->first();

    $model = null;

    if ($padm)
        $model = $padm;
    if ($adm)
        $model = $adm;
    if ($cst)
        $model = $cst;

    if ($model == null)
        return "";
    if ($col != null)
        return $model->{$col} ?: "";
    else
        return $model;
}

function getInvoicesItems($code)
{
    return \App\Rp\PaymentInvoice::where('code', $code)->get();
}

function getStudentInvoices($student, $activity = '0')
{
    $invoices = \App\Rp\PendingPaymentInvoice::where('student_id', $student);
    if ($activity != 'a')
        $invoices = $invoices->where('active', $activity);

    if ($invoices->count() <= 0)
        return null;
    else
        return $invoices->get();
}

function getInvoice($code, $col = null)
{
    $invoice = \App\Rp\PendingPaymentInvoice::where('code', $code)->first();
    if ($col == null)
        return $invoice;
    else
        return $invoice->{$col} ?: "";
}

function getInstruction($type)
{
    $instruction = \App\Model\Rp\StudentInstructions::where('type', $type)->first();
    if ($instruction)
        return $instruction->instructions;

    return "";
}

function sponsored($stdid, $feeCategory = null, $getP = false, $col = null)
{
    $category = null;
    if ($feeCategory != null)
        $category = App\FeeCategory::where('category_name', "LIKE", ucwords($feeCategory))->first();

    $sponsor = App\SponsorStudent::where('student_id', $stdid);

    if ($category) {
        $sponsor = $sponsor->where('category_id', $category->id)->first();
        if ($sponsor) {
            if ($getP === true) {
                if ($col == null or $col == "")
                    return $sponsor->percentage;
                elseif ($col === true)
                    return $sponsor;
                else
                    return $sponsor->{$col};
            } else
                return true;
        }
    }

    return 0;
}

function isSponsored($stdid)
{
    $sponsor = App\SponsorStudent::where('student_id', $stdid);
    if ($sponsor->count() > 0)
        return true;

    return false;
}

function getNoticeBoard($col = null)
{
    $notice = App\Notification::orderBy('created_at', "DESC")->first();
    if ($notice)
        return ($col != null) ? $notice->{$col} : $notice;

    return false;
}

function paidOutSide($std, $acdemic = null, $category = null)
{
    if ($acdemic == null)
        $acdemic = getCurrentAcademicYear();
    $student = \App\PaymentOutside::where('student_id', $std)
        ->where('academic_year', $acdemic);
    if ($category != null)
        $student->where('payment_category', $category);

    $student = $student->first();
    if ($student)
        return true;

    return false;
}


function getPaymentSum($college_id, $type = null)
{
    $sum = 0;
    if ($type != null) {
        $payments = \App\Model\Rp\CollegePayment::join('pending_payment_invoices', 'college_payments.invoice_code', '=', 'pending_payment_invoices.code')
            ->where('college_payments.college_id', $college_id)
            ->where('pending_payment_invoices.name', 'LIKE', "%" . $type . "%")
            ->select('college_payments.paid_amount')
            ->get();
        foreach ($payments as $pay) {
            $sum += $pay->paid_amount;
        }
    } else {
        $sum = \App\Model\Rp\CollegePayment::where('college_id', $college_id)->sum('paid_amount');
    }

    return $sum;
}


function getContinuingProgram($std_id)
{
    $student = \App\CollegeContinuingStudent::where('student_reg', $std_id)->first();

    if ($student) {
        $option = \App\Rp\CollegeOption::find($student->option_id);
        if ($option) {
            $dep = \App\Rp\CollegeDepartment::find($option->department_id);
            if ($dep) {
                return $dep->program_id;
            } else {
                return '';
            }
        } else {
            return '';
        }
    } else {
        return '';
    }
}

function getNationalExamGrade($marks, $index = false)
{
    $wg = \App\WeightScale::where([
        ['weightscale_from', '<=', $marks],
        ['weightscale_to', '>=', $marks],
    ])->first();

    if ($wg) {
        if ($index === true)
            return $wg->grade ? $wg->grade->grade_index : 0;
        return $wg->grade->grade_name;
    }
    return 0;
}

function getGradeListOnMarks($take = null, $end = 15)
{
    $grades = [];
    for ($i = 1; $i <= $end; $i++) {
        $grades[$i] = "grade" . $i;
    }
    if ($take != null) {
        if (isset($grades[$take]))
            return $grades[$take];
        else
            return "";
    }
    return $grades;
}


function getWeightScale($marks)
{
    $wg = \App\WeightScale::where([
        ['weightscale_from', '<=', $marks],
        ['weightscale_to', '>=', $marks],
    ])->first();

    if ($wg) {
        return $wg;
    }
    return null;
}

function calculateScore($weightscale, $numberofexams)
{
    if ($numberofexams > 0 && $weightscale != null) {
        $gi = $weightscale ? $weightscale->grade->grade_index : 0;
        return $gi;
    }
    return "";
}

function calculateRemark($score)
{
    if ($score == null)
        return;
    if ($score >= config('nationalexam.thresholds.pass', 30)) {
        return "PASS";
    } elseif ($score >= config('nationalexam.thresholds.fail', 10)) {
        return "FAIL";
    } else {
        return "UNCLASSIFIED";
    }
}

function getStudentAggregate($student_index)
{
    $allMarks = \DB::table('national_exam_result')
        ->where('student_index', $student_index)
        ->groupBy('course_id')
        ->select(['marks', 'student_index'])
        ->get();
    if ($allMarks) {
        $sum = 0;
        foreach ($allMarks as $marks) {
            $sum += (int)getNationalExamGrade($marks->marks, true);
        }
        return $sum;
    }
    return "";
}

function getStudentMarksByCourse($studentINdex, $couse)
{
    $presults = App\NationalExamResult::where('student_index', $studentINdex)
        ->where('course_id', $couse)
        ->first();
    if ($presults)
        return $presults->marks;
    return 0;
}

function solveTotalCanditate(Collection $students, Array $level_5_id, string $school_id): int
{
    return $students->where('school', $school_id)->whereIn('level_id', $level_5_id)->count();
}

function solveCandidatePerCourse(Collection $students, Array $level_5_id, string $school_id, string $program)
{
//    $std = $students->where('school',$school_id)->whereIn('level_id',$level_5_id);
    $std = $students->where('school', $school_id);
    //filter specific candidates based on program they study
    $program_students = $std->filter(function ($d) use ($program) {
        return strposWrapper($d->index_number, $program, false);
    });
    return $program_students->count();
}

function strposWrapper($a, $b, $case_sensitive = true)
{
    if ($case_sensitive) {
        if (strpos($a, $b) !== false) {
            return true;
        }
        return false;
    } else {
        if (stripos($a, $b) !== false) {
            return true;
        }
        return false;
    }
}

function resolveProgramFromIndex($student_index)
{
    preg_match_all('/([a-zA-Z]+)/', $student_index, $matches);
    $col = collect($matches);
    return implode($col->unique()->first());
}