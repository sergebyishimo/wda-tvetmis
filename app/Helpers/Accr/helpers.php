<?php

function getSourceRating($arr = false)
{
    $rating = \DB::connection('accr_mysql')->table('source_graph_accr_input')->select('over_all_rank')->distinct('over_all_rank');

    $s = "";
    if ($arr === true)
        $s = [];

    if ($rating->count() > 0) {
        foreach ($rating->get() as $item) {
            if ($arr === true)
                $s[] = $item->over_all_rank;
            else
                $s .= "'" . $item->over_all_rank . "',";

        }
    }
    if ($arr === true)
        return $s;

    return trim($s, ',');
}

function getSchoolRatings()
{
    $rating = getSourceRating(true);
    $s = "";
    if (count($rating)) {
        foreach ($rating as $item) {
            $school = \DB::connection('accr_mysql')->table('source_graph_accr_input')->where('over_all_rank', $item);
            $s .= "'" . $school->count() . "',";
        }
    }
    return trim($s, ',');
}

function getFunctionsForSelect()
{
    $functions = [];
    $datas = \App\Model\Accr\Functions::all();

    foreach ($datas as $data) {
        $functions[$data->id] = $data->name;
    }

    return $functions;
}

function getTotalWeightForQualityAreas($criterias)
{
    $total = 0;
    if ($criterias) {
        foreach ($criterias as $criteria) {
            $total += $criteria->criterias->sum('weight');
        }
    }

    return $total;
}

function getQualityAreaScores($criterias, $school, $sub = null, $year = null)
{
    $scores = 0;
    if ($criterias) {
        foreach ($criterias as $criteria) {
            foreach ($criteria->criterias as $item) {
                if (strtolower($item->getWdaAnswer($school, $sub, $year)) == 'yes')
                    $scores += $item->weight;
            }
        }
    }

    return $scores;
}

function getFunctionScores($qualities, $school, $over = false, $submiteId = null, $where = null, $year = null)
{
    $scores = 0;
    if ($qualities) {
        foreach ($qualities as $quality) {
            if ($quality->criteria) {
                foreach ($quality->criteria as $criteria) {
                    if ($criteria->criterias) {
                        foreach ($criteria->criterias as $item) {
                            if ($over == true)
                                $scores += $item->weight;
                            else {
                                if ($where == 'wda')
                                    $answer = $item->getWdaAnswer($school, $submiteId, $year);
                                elseif ($where == 'district')
                                    $answer = $item->getDistrictAnswer($school, $submiteId, $year);
                                else
                                    $answer = $item->getSchoolAnswer($school, $submiteId, $year);

                                if (strtolower($answer) == 'yes')
                                    $scores += $item->weight;
                            }
                        }
                    }
                }
            }
        }
    }

    return $scores;
}

function getCriteriaScores($criteria, $school, $sub = null, $year = null)
{
    $scores = 0;
    if ($criteria->count() > 0) {
        foreach ($criteria as $item) {
            if (strtolower($item->getWdaAnswer($school, $sub, $year)) == 'yes')
                $scores += $item->weight;
        }
    }
    return $scores;
}

function getCurrentSelfAssessmentStatus($school = null, $function_id)
{
    if ($school == null) {
        $school = auth()->user()->school;
    }

//    $status = $school->selfAssessmentStatus()
//        ->where('function_id', $function_id)
//        ->where('academic_year', date('Y'))->first();

//    return $status;

    return null;
}

function getRattingScore($total = 0, $class = null)
{
    if ($total >= 80)
        return $class == null ? "Excellent" : "success";
    elseif ($total <= 79 && $total >= 60)
        return $class == null ? "Good" : "primary";
    elseif ($total <= 59 && $total >= 40)
        return $class == null ? "Moderate" : "info";
    elseif ($total <= 39 && $total >= 20)
        return $class == null ? "Poor" : "warning";
    elseif ($total <= 19)
        return $class == null ? "Very Poor" : "danger";
}

function getCommentsDistrict($school, $function, $quality, $col = 'strength', $check = false, $sub = null)
{
    $comment = \App\Model\Accr\AccrSchoolAssessmentComment::where('school_id', $school)
        ->where('function_id', $function)
        ->where('quality_id', $quality)
        ->where('from', 'district')
        ->where('academic_year', date('Y'));
    if ($sub)
        $comment->where('submit_id', $sub);

    if ($check === false) {
        $comment = $comment->first();

        if ($comment) {
            if ($comment->{$col})
                return $comment->{$col};
        }
    } elseif ($check === true) {
        if ($comment->count() > 0)
            return $comment->first();
        else
            return false;
    }

    return null;
}

function getCommentsWda($school, $function, $quality, $col = 'strength', $check = false, $sub = null)
{
    $comment = \App\Model\Accr\AccrSchoolAssessmentComment::where('school_id', $school)
        ->where('function_id', $function)
        ->where('quality_id', $quality)
        ->where('from', 'wda')
        ->where('academic_year', date('Y'));

    if ($sub)
        $comment->where('submit_id', $sub);

    if ($check === false) {
        $comment = $comment->first();

        if ($comment) {
            if ($comment->{$col})
                return $comment->{$col};
        }
    } elseif ($check === true) {
        if ($comment->count() > 0)
            return $comment->first();
        else
            return false;
    }

    return null;
}

function getRatingLabeling()
{
    $r = ['Excellent' => 0, 'Good' => 0, 'Moderate' => 0, 'Poor' => 0, 'Very Poor' => 0];

    return $r;
}

function getGraphScores($function = 'Input')
{
    $r = ['Excellent' => 0, 'Good' => 0, 'Moderate' => 0, 'Poor' => 0, 'Very Poor' => 0];

    $func = \App\Model\Accr\Functions::where('name', ucfirst($function))->first();
    if ($func) {
        $submited = \App\Model\Accr\SchoolSelfAssessimentStatus::where('function_id', $func->id)
            ->where('academic_year', date('Y'))
            ->where('school_confirm', 1);
        if ($submited->count() > 0) {
            foreach ($submited->get() as $item) {
                $tt = getFunctionScores($func->qualityAreas, $item->school, true);
                $hv = getFunctionScores($func->qualityAreas, $item->school);
                $pp = ($hv * 100) / $tt;
                $pp = getRattingScore(ceil($pp));
                if (isset($r[$pp]))
                    $r[$pp] += 1;
            }
        }
    }

    return $r;
}

function getWhichWeek($date)
{
    $date = strtotime($date);
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}

function getWeeks($month, $year)
{
    $num_of_days = date("t", mktime(0, 0, 0, $month, 1, $year));
    $lastday = date("t", mktime(0, 0, 0, $month, 1, $year));
    $no_of_weeks = 0;
    $count_weeks = 0;
    while ($no_of_weeks < $lastday) {
        $no_of_weeks += 7;
        $count_weeks++;
    }
    return $count_weeks;
}

function getMonths($len = 'F')
{
    $months = [];
    for ($m = 1; $m <= 12; $m++) {
        $months[$m] = date($len, mktime(0, 0, 0, $m, 1, date('Y')));
    }

    return $months;
}

function getDateRange($from, $to)
{
    $period = new DatePeriod(
        new DateTime($from),
        new DateInterval('P1D'),
        new DateTime($to)
    );

    return $period;
}

function getApplicationAccreditationType($id, $col = "type")
{
    $appl = null;
    if (is_int($id))
        $appl = \App\Model\Accr\AccrApplication::find($id);
    elseif (is_object($id))
        $appl = $id;
    if (!is_null($appl)) {
        $type = $appl->accreditationType;
        if ($type)
            return $type->{$col} ?: "";
    }
    return "";

}

function getWDAPermission($key = null)
{
    /**
     * 1 = Browser
     * 2 = Read
     * 3 = Edit
     * 4 = Add
     * 5 = Delete
     * 7 = All
     */

    $perms = [
        '1' => 'WDA Person',
        '2' => 'Curriculum Management'
    ];
    if ($key != null) {
        if (isset($perms[$key]))
            return $perms[$key];
        else
            return "";
    }
    return $perms;
}

function getRPPermission($key = null)
{
    /**
     * 1 = Browser
     * 2 = Read
     * 3 = Edit
     * 4 = Add
     * 5 = Delete
     * 7 = All
     */
    $perms = [
        '7' => 'RP Full User',
        '12' => 'RP Limited User',
        '2' => 'RP External Partner'
    ];

    if ($key != null) {
        if (isset($perms[$key]))
            return $perms[$key];
        else
            return "";
    }
    return $perms;
}

function wdaAllowed($permission = 1)
{
    if (auth()->guard('wda')->check()) {
        $wda = auth()->guard('wda')->user()->permission;
        if ($wda == $permission)
            return true;
        elseif (strpos((string)$wda, (string)$permission) !== false)
            return true;
    }
    return false;
}

function rpAllowed($permission = 1)
{
    /**
     * 1 = Browser
     * 2 = Read
     * 3 = Edit
     * 4 = Add
     * 5 = Delete
     * 7 = All
     */

    if (auth()->guard('rp')->check()) {
        $perms = auth()->guard('rp')->user()->permission;
        if ($perms == $permission)
            return true;
        elseif ($perms == 7)
            return true;
        elseif (strpos((string)$perms, (string)$permission) !== false)
            return true;
    }
    return false;
}

function getQualificationFormSubsector($subsectors = null, $counting = false)
{
    $qualifications = [];
    if ($subsectors != null) {
        if (is_object($subsectors) or is_array($subsectors)) {
            foreach ($subsectors as $subsector) {
                if ($subsector->curriculumQualification)
                    $qualifications[] = $subsector->curriculumQualification();
            }
        } elseif (is_integer($subsectors)) {

        }
    }
    if ($counting === true) {
        $count = 0;
        if (count($qualifications)) {
            foreach ($qualifications as $qualification) {
                $count += $qualification->count();
            }
        }
        return $count;
    }
    return $qualifications;
}

function getModuleCompetenceClass($key = null)
{
    $types = [
        'Complementary Competency' => 'Complementary Competency',
        'Core Competency' => 'Core Competency'
    ];

    if ($key != null) {
        if (isset($types[$key]))
            return $types[$key];
        else
            return "";
    }

    return $types;
}

function getModuleCompetenceType($key = null)
{
    $types = [
        'Complementary' => 'Complementary',
        'General' => 'General',
        'Specific' => 'Specific',
    ];

    if ($key != null) {
        if (isset($types[$key]))
            return $types[$key];
        else
            return "";
    }

    return $types;
}

function computingStudentsMarks()
{
    $vv = App\NationalExamResult::join('students', 'students.index_number', '=', 'national_exam_result.student_index')
        ->join('accr_schools_information', 'accr_schools_information.school_code', '=', 'national_exam_result.school_id')
        ->whereYear('national_exam_result.created_at', date('Y'))
        ->select([
            'national_exam_result.*',
            'students.gender',
            'accr_schools_information.province',
            'accr_schools_information.district',
        ])->get();
    if ($vv->count() > 0)
        $vv = $vv;
    else
        $vv = [];
    $bb = []; #grade
    $mk = []; #marks
    $dataSC = [];
    $dataCC = [];
    $gender = [];
    $province = [];
    $district = [];
    $insert = [];
    foreach ($vv as $v) {
        if (isset($bb[$v->student_index])) {
            $bb[$v->student_index] += getNationalExamGrade((int)$v->marks, true);
            $mk[$v->student_index] += (int)$v->marks;
        } else {
            $bb[$v->student_index] = getNationalExamGrade((int)$v->marks, true);
            $mk[$v->student_index] = (int)$v->marks;
            $dataSC[$v->student_index] = trim($v->school_id);
            $dataCC[$v->student_index] = $v->section_id;
            $gender[$v->student_index] = $v->gender;
            $province[$v->student_index] = $v->province;
            $district[$v->student_index] = $v->district;
        }
    }

    if (count($bb)) {
        foreach ($bb as $k => $b) {
            $insert[] = [
                'student_index' => $k,
                'total_aggregates' => $b,
                'total_marks' => isset($mk[$k]) ? $mk[$k] : 0,
                'school_id' => isset($dataSC[$k]) ? $dataSC[$k] : '-',
                'combination_id' => isset($dataCC[$k]) ? $dataCC[$k] : '-',
                'created_at' => \Carbon\Carbon::now(),
                'gender' => isset($gender[$k]) ? $gender[$k] : null,
                'province' => isset($province[$k]) ? $province[$k] : null,
                'district' => isset($district[$k]) ? $district[$k] : null
            ];
        }

        if (count($insert)) {
            App\ComputedAggregate::whereYear('created_at', date('Y'))->delete();
            App\ComputedAggregate::insert($insert);
        }
    }
}

function getNumbersForDistrictFromCollection($collection, $district, $needed, $additional = null)
{
    if (!empty($collection)) {
        switch ($needed) {
            case 'schools':
                $count = 0;
                $schools = $collection->where('district', $district)->all();
                $school_holder = null;
                foreach ($schools as $school) {
                    if ($school->school_id != $school_holder) {
                        $count += 1;
                        $school_holder = $school->school_id;
                    }
                }
                return $count;
                break;
            case 'male':
                $filtered = $collection->filter(function ($item) use ($district) {
                    if ($item->district == $district)
                        return strtolower($item->gender) == strtolower('male');
                });
                return $filtered->count();
                break;
            case 'female':
                $filtered = $collection->filter(function ($item) use ($district) {
                    if ($item->district == $district)
                        return strtolower($item->gender) == strtolower('female');
                });
                return $filtered->count();
                break;
            case 'total':
                return $collection->where('district', $district)->count();
                break;
            case 'fail':
                $tt = 0;
                $tf = 0;
                $ttp = "";
                foreach ($collection as $data) {
                    if ($district == $data->district) {
                        $ttp .= '-' . $data->total_aggregates;
                        $t = calculateRemark($data->total_aggregates);
                        $cp = ($t == "FAIL" || $t == "UNCLASSIFIED") ? true : false;
                        if ($cp)
                            $tf++;
                        $tt++;
                    }
                }
                if ($tt > 0) {
                    $per = ($tf * 100) / $tt;
                    return round($per, 2);
                }
                return '-';
                break;
            case 'pass':
                $tt = 0;
                $tf = 0;
                $ttp = "";
                foreach ($collection as $data) {
                    if ($district == $data->district) {
                        $ttp .= '-' . $data->total_aggregates;
                        $t = calculateRemark($data->total_aggregates);
                        $cp = ($t == "PASS") ? true : false;
                        if ($cp)
                            $tf++;
                        $tt++;
                    }
                }
                if ($tt > 0) {
                    $per = ($tf * 100) / $tt;
                    return round($per, 2);
                }
                return '-';
                break;
            case 'rank':
                return '-';
                break;
            case 'special':
                return '-';
                break;
            case 'overall':
                return '-';
                break;
            case 'year':
                $holder = "";
                $filtered = $collection->filter(function ($item) use ($additional, $holder) {
                    if ($holder != $item->district) {
                        $holder = $item->district;
                        return date('Y', strtotime($item->created_at)) == $additional;
                    } else
                        $holder = "";
                });
                return $filtered->count();
                break;
            case 'province':
                $holder = "";
                $filtered = $collection->filter(function ($item) use ($additional, $holder) {
                    if ($holder != $item->district) {
                        $holder = $item->district;
                        return strtolower($item->province) == strtolower($additional);
                    } else
                        $holder = "";
                });
                return $filtered->count();
                break;
            default:
                return '-';
                break;
        }
    }
}

function countCriteriaFromCriteriaSection($section)
{
    return \App\Model\Accr\AccrCriteria::where('criteria_section_id', $section)->count();
}

function countCriteriaFromQualityArea($quality)
{
    return \DB::table('accr_accr_criterias')
        ->join('accr_accr_criteria_sections', 'accr_accr_criteria_sections.id', '=', 'accr_accr_criterias.criteria_section_id')
        ->where('accr_accr_criteria_sections.quality_area_id', $quality)
        ->select(['accr_accr_criterias.*'])
        ->count();
}

function calcCriteriaPercentage($collection, $which = 'YES')
{
    $value = 0.0;

    if ($collection) {
        $all = $collection->where('academic_year', date('Y'))
            ->count();
        $apo = $collection->where('academic_year', date('Y'))
            ->where('answer', $which)
            ->count();
        $value = ($apo * 100) / $all;
    }
    return ceil($value) . "% (" . $apo . ")";
}

function getPercentageForIndicator($indicator, $cond, $need, $district = null)
{
    $value = 0.0;
    $which = strtoupper($need);

    $collection = \DB::table('accr_accr_criteria_answers')
        ->join('accr_schools_information', 'accr_schools_information.id', '=', 'accr_accr_criteria_answers.school_id')
        ->where('accr_accr_criteria_answers.criteria_id', $indicator);

    if ($cond == 'gov')
        $collection->where('accr_schools_information.school_status', 'LIKE', 'Government Aid');
    elseif ($cond == 'prv')
        $collection->where('accr_schools_information.school_status', 'LIKE', 'Private');
    elseif ($cond == 'pub')
        $collection->where('accr_schools_information.school_status', 'LIKE', 'Public');
    elseif ($district == $cond)
        $collection->where('accr_schools_information.district', 'LIKE', $district);

    $collection->where('accr_accr_criteria_answers.academic_year', date('Y'));

    if ($collection) {
        $all = $collection->count();
        $apo = $collection->where('answer', $which)
            ->count();
        $value = ($apo * 100) / $all;
    }
    return round($value, 1, PHP_ROUND_HALF_UP) . "% (" . $apo . ")";
}