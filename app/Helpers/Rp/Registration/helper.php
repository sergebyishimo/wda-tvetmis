<?php

function getDashboardUrl()
{
    if (auth()->guard('rp')->check())
        return route('rp.home');

    if (auth()->guard('college')->check())
        return route('college.home');

    if (auth()->guard('student')->check())
        return route('student.home');

    if (auth()->guard('admin')->check())
        return route('admin.home');

    return "/home";
}

function getProfileUrl()
{
    if (auth()->guard('rp')->check())
        return route('rp.profile');

    if (auth()->guard('college')->check())
        return route('college.profile');

    if (auth()->guard('student')->check())
        return route('student.profile');

    if (auth()->guard('admin')->check())
        return route('admin.users.edit', auth()->guard('admin')->user()->id);

    return "/user/profile";
}

function getLogoutUrl()
{
    if (auth()->guard('rp')->check())
        return route('rp.logout');
    if (auth()->guard('college')->check())
        return route('college.logout');
    if (auth()->guard('student')->check())
        return route('student.logout');
    return "/logout";
}

function applicant()
{
    return new \App\Rp\Applicant();
}

function reg_settings($col = null, $year = null, $default = '', $type = null)
{
    $year = $year == null ? date('y') : $year;
    $settings = \App\Rp\Settings::where('academic_year', $year);
    if ($type != null)
        $settings->where('type', $type);

    if ($settings->count() > 0) {
        $settings = $settings->first();
        if ($col != null)
            return $settings->{$col} ?: "";
        else
            return $settings;
    }
    return $default;
}

function reg_sendSMS($phone, $message = null)
{
    $phone = validSMSNumber($phone);
}

//function validSMSNumber($num, $zero = false)
//{
//    if ($zero == false) {
//        if (preg_match('[^\+250|250]', $num))
//            return trim($num, "+");
//        else {
//            if ($num{0} == '0')
//                return "25" . $num;
//            else
//                return "250" . $num;
//        }
//    } elseif ($zero == true) {
//        if ($num{0} != '0')
//            return "0" . $num;
//    }
//}

function getCollege($id = null, $col = null, $def = "", $pass = false)
{
    if ($id != null) {
        $name = \DB::connection('admission_mysql')->table('polytechnics')->where('id', $id);
        if ($name->count() > 0)
            return $col == null ? $name->first() : $name->first()->{$col} ? $name->first()->{$col} : $def;
    } elseif ($pass === true) {
        return \App\Rp\Polytechnic::all();
    }

    return $def;
}

function getCollegeForSelect($false = false)
{
    $items = [];
    if ($false == true)
        $colleges = getCollege() ?: [];
    else
        $colleges = getCollege(null, null, "", true) ?: [];

    foreach ($colleges as $college) {
        $items[$college->id] = $college->short_name;
    }

    return $items;
}

function getDepartment($id, $col = null, $cont = false)
{
    if (auth()->guard('student')->check()) {
        if (isContinuingStudent(get_my_info()))
            $name = App\Rp\CollegeDepartment::where('id', $id);
    } else {
        $name = \DB::connection('rp_mysql')->table('departments')->where('id', $id);
        if ($cont === true)
            $name = App\Rp\CollegeDepartment::where('id', $id);
    }
    if (isset($name)) {
        if ($name->count() > 0)
            return $col == null ? $name->first() : $name->first()->{$col} ? $name->first()->{$col} : "";
    }

    return "";
}

function getCourse($id, $col = null, $cont = false)
{
    if (auth()->guard('student')->check()) {
        if (isContinuingStudent(get_my_info()))
            $name = App\Rp\CollegeOption::where('id', $id);
    } else {
        $name = \DB::connection('rp_mysql')->table('programs')->where('id', $id);
        if ($cont === true)
            $name = App\Rp\CollegeOption::where('id', $id);
    }
    if (isset($name)) {
        if ($name->count() > 0)
            return $col == null ? $name->first() : $name->first()->{$col} ? $name->first()->{$col} : "";
    }
    return "";
}

function applied($id = null, $application = null)
{
    if ($id == null)
        $id = auth()->guard('student')->check() ? auth()->guard('student')->user()->id : $id;

    if ($application === true) #Check If Student Done Application
        $std = \App\Model\Rp\AdmissionPrivate::where('std_id', $id)
            ->whereNotNull('gender')
            ->whereNotNull('index_number')
            ->whereNotNull('national_id_number')
            ->whereNull('rejected');
    else # Check If Student Done Registration
        $std = \App\Rp\CollegeStudent::where("student_reg", $id);

    if ($std->count() <= 0)
        return false;

    return true;
}

function admissionPaid($id = null)
{
    if ($id == null)
        $id = auth()->guard('student')->check() ? auth()->guard('student')->user()->id : $id;

    $year = date('Y');
    $std = \App\Rp\Payment::where("std_id", $id)
        ->where('admission', '1')
        ->whereYear('created_at', '=', $year)->first();
    if ($std)
        return true;
    else
        return false;

}

function isAdmitted($id = null)
{
    if ($id == null)
        $id = auth()->guard('student')->check() ? auth()->guard('student')->user()->id : $id;

    $admitted = \App\Rp\AdmittedStudent::where('std_id', $id)->first();

    return $admitted ? true : false;
}

function comeFromPrivatePortal($id = null)
{
    if ($id == null)
        $id = auth()->guard('student')->check() ? auth()->guard('student')->user()->id : $id;

    $private = \App\Model\Rp\AdmissionPrivate::where('std_id', $id)->whereYear('created_at', '=', date('Y'))->first();

    if ($private)
        return true;
    else
        return false;
}

function get_my_payment()
{
    if (auth()->guard('student')->check()) {
        $stdid = auth()->guard('student')->user()->id;
        $payment = \App\Rp\Payment::where('std_id', $stdid)->orderBy('created_at', 'DESC');
        if ($payment->count() > 0) {
            return $payment->first();
        }
    }

    return null;
}

function enrol_enabled($model = false, $key = null)
{
    $now = date('Y');
    $ch = \App\Rp\Settings::where('academic_year', $now)
        ->where('enabled', 1)
        ->where('type', 'enrol')
        ->orderBy('created_at', "DESC");
    $get = false;

    if ($ch->count() > 0) {
        $get = $ch->first();

        if ($model === true) {
            $ko = ['start' => $get->start, 'end' => $get->ends];
            if ($key != null)
                return isset($ko[$key]) ? $ko[$key] : date('Y-m-d');
        }

        $today = date("Y-m-d");
        if (check_in_range($get->start, $get->ends, $today))
            return true;
        else {
            if ($today >= $get->ends)
                $get = "Enrolling was closed on " . date("M d, Y", strtotime($get->ends));
            else
                $get = "Come back on " . date("M d, Y", strtotime($get->start)) . " that when enrol is scheduled !";
        }
    }
    return $get;
}

function system_enabled($model = false, $key = null)
{
    $now = date('Y');
    $ch = \App\Rp\Settings::where('academic_year', $now)
        ->where('enabled', 1)
        ->where('type', 'registration')
        ->orWhereNull('type')
        ->orderBy('created_at', "DESC");
    $get = null;
    if ($ch->count() > 0) {
        $get = $ch->first();

        if ($model === true) {
            $ko = ['start' => $get->start, 'end' => $get->ends];
            if ($key != null)
                return isset($ko[$key]) ? $ko[$key] : date('Y-m-d');
        }

        $today = date("Y-m-d");
        if (check_in_range($get->start, $get->ends, $today))
            return true;
        else {
            if ($today >= $get->ends) {
                $get = "System was closed on " . date("M d, Y", strtotime($get->ends));

                if (isStudentEnabledForReg())
                    return true;

            } else
                $get = "Come back on " . date("M d, Y", strtotime($get->start)) . " that when registration is scheduled !";
        }

    }

    return $get;

}

function isStudentEnabledForReg($student = null)
{
    if ($student == null) {
        if (auth()->guard('student')->check())
            $student = auth()->guard('student')->user()->id;
    }

    $xp = \App\Model\Rp\StudentExp::where('student_id', $student)
        ->where('academic_year', getCurrentAcademicYear())->first();
    if ($xp)
        return true;

    return false;
}

function check_in_range($start_date, $end_date, $date)
{
    // Convert to timestamp
    $start_ts = strtotime($start_date);
    $end_ts = strtotime($end_date);
    $user_ts = strtotime($date);

    // Check that user date is between start & end
    return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}

function get_my_info($col = "std_id", $f = false)
{
    if (auth()->guard('student')->check()) {
        $user = auth()->guard('student')->user()->id;
        if (strtolower(auth()->guard('student')->user()->sponsorship) === 'private')
            $std = new \App\Model\Rp\AdmissionPrivate();
        else
            $std = new \App\Rp\AdmittedStudent();

        if (applied($user)) {
            $std = new \App\Rp\CollegeStudent();
            $col = $col === "std_id" ? "student_reg" : $col;
        }
        if ($std->getinfo($col) != "")
            return get_from($col, $std->getinfo($col), $f);
    }
    return null;
}

function getStudentInfo($stdid, $col = "std_id", $f = false, $model = false)
{
    if (!isAdmitted($stdid))
        $std = new \App\Model\Rp\AdmissionPrivate();
    else
        $std = new \App\Rp\AdmittedStudent();
    if (applied($stdid)) {
        $std = new \App\Rp\CollegeStudent();
        $col = $col === "std_id" ? "student_reg" : $col;
    }
    if ($model === true)
        return $std;
    if ($std->getinfo($col, $stdid) != "")
        return get_from($col, $std->getinfo($col, $stdid), $f, $stdid);

    return "";
}

function get_from($col, $v, $f = false, $student = null)
{
    if ($f === false) {
        switch (strtolower($col)) {
            case "phone":
                return validSMSNumber($v, true);
                break;
            case "country":
                $find = \App\Rp\Country::find($v);
                if ($find)
                    return $find->type;
                break;
            case "payment_verified":
                return $v ? "YES" : "NO";
                break;
            case "disability":
                $find = \App\Rp\Disability::find($v);
                if ($find)
                    return $find->type;
                break;
            case "option_offered":
                $find = \App\Rp\OptionOffered::find($v);
                if ($find)
                    return $find->option_name;
                break;
            case "your_bank":
                $find = \App\Rp\Bank::find($v);
                if ($find)
                    return $find->bank_name;
                break;
            case "dob":
                return date("Y", strtotime($v));
                break;
            case "want_student_loan":
                return $v == 1 ? "YES" : "NO";
                break;
            case "examiner":
                $find = \App\Rp\Examiners::find($v);
                if ($find)
                    return $find->name;
                break;
            case "ubudehe":
                $find = \App\Rp\Ubudehe::find($v);
                if ($find)
                    return $find->ubudehe;
                break;
            case "college_id":
                $find = \App\Rp\Polytechnic::find($v);
                if ($find)
                    return $find->polytechnic;
                break;
            case "department_id":
                $find = \App\Model\Rp\RpDepartment::find($v);
                if (auth()->guard('student')->check())
                    $student = auth()->guard('student')->user()->id;
                if (isContinuingStudent($student))
                    $find = \App\Rp\CollegeDepartment::find($v);
                if ($find)
                    return $find->department_name;
                break;
            case "course_id":
                $find = \App\Model\Rp\RpProgram::find($v);
                if (auth()->guard('student')->check())
                    $student = auth()->guard('student')->user()->id;
                if (isContinuingStudent($student))
                    $find = \App\Rp\CollegeOption::find($v);
                if ($find)
                    return $find->program_name;
                break;
            default:
                return $v;
        }
    }
    return $v;
}

function regGetProvince()
{
    $response = \DB::connection('admission_mysql')->table('rwanda_boundaries')
        ->select("Province")
        ->groupBy("Province")
        ->get();

    return $response;
}

function regGetDistrict($s)
{
    $response = \DB::connection('admission_mysql')->table('rwanda_boundaries')
        ->select("District")
        ->where("Province", $s)
        ->groupBy("District")
        ->get();

    return $response;
}

function regGetSector($s)
{
    $response = \DB::connection('admission_mysql')->table('rwanda_boundaries')
        ->select("Sector")
        ->where("District", $s)
        ->groupBy("Sector")
        ->get();

    return $response;
}

function regGetCell($s)
{
    $response = \DB::connection('admission_mysql')->table('rwanda_boundaries')
        ->select("Cell")
        ->where("Sector", $s)
        ->groupBy("Cell")
        ->get();

    return $response;
}

function regGetVillage($s)
{
    $response = \DB::connection('admission_mysql')->table('rwanda_boundaries')
        ->select("Village")
        ->where("Cell", $s)
//        ->groupBy("Village")
        ->get();

    return $response;
}

function seoUrl($string)
{
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

function splitNameFomStudent($v = 1)
{
    $po = explode(" ", auth()->guard('student')->user()->name);
    if ($v == 1)
        return $po[0] ?: "";
    else {
        $c = count($po);
        $l = "";
        for ($i = 1; $i < $c; $i++) {
            $l .= $po[$i];
        }
        return $l;
    }

    return "";
}

function collegeHasAccount($id)
{

}

function getUserGravatar()
{
    $file = asset('/img/app_logo.jpg');
    if (auth()->guard('college')->check()) {
        $college = auth()->guard('college')->user();
        $ex = ['jpg', 'png', 'jpeg'];
        foreach ($ex as $item) {
            if (file_exists(public_path() . '/img/logos/' . $college->username . '.' . $item)) {
                $file = asset('/img/logos/' . $college->username . '.' . $item);
                break;
            }
        }
    } elseif (auth()->guard('rp')->check())
        return \Gravatar::get(auth()->guard('rp')->user()->email);

    return $file;
}

function college($col = null, $extra = null)
{
    if (auth()->guard('college')->check() == true)
        return $col != null ? auth()->guard('college')->user()->{$col} : auth()->guard('college')->user();

    if (is_integer($col)) {
        $college = \App\College::where('college_id', $col)->first();
        if ($college)
            return $college->{$extra};
    }

    return null;
}

function getAmountPerCategory($category, $year = null, $col = null)
{
    $year = $year ?: date('Y');

    $details = \App\Rp\CollegePaymentDetail::where('category_id', $category)
        ->where('college_id', college('id'))
        ->where('academic_year', $year);
    if ($details->first()) {
        if ($col == null) {
            return $details->first()->amount;
        } else {
            return $details->first()->{$col};
        }
    }

    return 0;
}

function getPaymentDetail($category, $year = null, $col = null)
{
    return getAmountPerCategory($category, $year, $col);
}

function getAdmittedStudents($college = null, $year = null)
{
    $college = $college ?: college('college_id');
    $year = $year ?: date('Y');
    $model = \App\Rp\AdmittedStudent::where('year_of_study', $year);
    if ($college) {
        $students = $model->where('college_id', $college);

        return $students->get();
    } else
        return $model;

}

function getRegisteredStudents($college = null, $year = null)
{
    $college = $college ?: college('college_id');
    $year = $year ?: date('Y');
    $model = \App\Rp\CollegeStudent::where('year_of_study', $year);
    if ($college) {
        $students = $model->where('college_id', $college);

        return $students->get();
    } else
        return $model;
}

function regGetStudentPhoto($student)
{
    if (is_object($student))
        $photo = $student->photo;
    else
        $photo = $student;

    $file = \Storage::disk('public')->exists($photo);
    if ($file)
        return asset('storage/' . $photo);
    else {
        if (!$photo)
            return asset('img/photo_default.png');
        $url = "http://application.rp.ac.rw/storage/" . $photo;
        #$url = "http://localhost:9092/storage/" . $photo;
        downloadPhoto($file);
        return $url;
    }
}

function regGetStudentFile($url)
{
    return regGetStudentPhoto($url);
}

function downloadPhoto($file)
{
    if ($file) {
        $url = "http://application.rp.ac.rw/storage/" . $file;
        #$url = "http://localhost:9092/storage/" . $file;
        $contents = file_get_contents($url);
        $name = $file;
        \Storage::disk('public')->put($name, $contents);
    }
}

function getNotFinishedStudents($id = null, $year = null)
{
    $year = $year == null ? date("Y") : $year;
    if ($id == null) {
        $college = college('college_id');
//        $finished = college('myStudents')->pluck('student_reg')->all();
        if (auth()->guard('rp')->check()) {
            $finished = \App\Rp\CollegeStudent::all();
            if ($finished)
                $finished = $finished->pluck('student_reg');
            else
                $finished = [];
        } else
            $finished = \App\Rp\CollegeStudent::where('college_id', $college)->pluck('student_reg');

//        dd($finished);
        if (auth()->guard('rp')->check())
            $students = \App\Rp\AdmittedStudent::whereNotIn('std_id', $finished)
                ->orderBy('created_at', 'desc')
                ->get();
        else
            $students = \App\Rp\AdmittedStudent::where('college_id', $college)
                ->where('year_of_study', $year)
                ->whereNotIn('std_id', $finished)->get();

    } else {
        $college = college($id, 'college_id');
        $finished = college($id, 'myStudents')->pluck('student_reg')->all();
        $students = \App\Rp\AdmittedStudent::where('college_id', $college)
            ->where('year_of_study', $year)
            ->whereNotIn('std_id', $finished)->get();
    }

    return $students;
}

function regGenerateReg($kop = 0)
{
    $q = true;
    $add = 1;
    while ($q) {
        $op = \App\Rp\Applicant::where('year_of_study', date("Y"))->count() + $add + $kop;
        $opp = "";
        if ($op <= 9)
            $opp = "0000" . $op;
        elseif ($op < 100)
            $opp = "000" . $op;
        elseif ($op < 999)
            $opp = "00" . $op;
        elseif ($op < 9999)
            $opp = "0" . $op;
        else
            $opp = $op;
        $op = date('y') . "RP";
        $res = $op . $opp;
        $adm = \App\Rp\AdmittedStudent::where('std_id', $res)->count();
        $cst = \App\Rp\CollegeStudent::where('student_reg', $res)->count();
        $std = \App\Rp\Student::withTrashed()->where('id', $res)->count();
        if ($adm <= 0 && $cst <= 0)
            break;
        else
            $add++;
    }
    return $res;
}

function privateGenerateReg($kop = 0)
{
    $q = true;
    $add = 1;
    while ($q) {
        $op = \App\Rp\Applicant::where('year_of_study', date("Y"))->count() + $add + $kop;
        $opp = "";
        if ($op <= 9)
            $opp = "0000" . $op;
        elseif ($op < 100)
            $opp = "000" . $op;
        elseif ($op < 999)
            $opp = "00" . $op;
        elseif ($op < 9999)
            $opp = "0" . $op;
        else
            $opp = $op;
        $op = date('y') . "RP";
        $res = $op . $opp;
        $adm = \App\Rp\AdmittedStudent::where('std_id', $res)->count();
        $cst = \App\Rp\CollegeStudent::where('student_reg', $res)->count();
        $std = \App\Rp\Student::withTrashed()->where('id', $res)->count();
        if ($adm <= 0 && $cst <= 0 && $std <= 0)
            break;
        else
            $add++;
    }
    return $res;
}

function continuingGenerateReg($kop = 0, $year, $sponsor = "")
{
    $q = true;
    $add = 1;
    $y = ['1' => '17', '2' => '17', '3' => '16'];
    while ($q) {
        $op = \App\CollegeContinuingStudent::count() + $add + $kop;
        $opp = "";
        if ($op <= 9)
            $opp = "0000" . $op;
        elseif ($op < 100)
            $opp = "000" . $op;
        elseif ($op < 999)
            $opp = "00" . $op;
        elseif ($op < 9999)
            $opp = "0" . $op;
        else
            $opp = $op;
        $op = $y[$year] . "RP";
        $res = $op . $opp . $sponsor;
        $adm = \App\CollegeContinuingStudent::where('student_reg', $res)->count();
        $cst = \App\Rp\CollegeStudent::where('student_reg', $res)->count();
        $lst = \App\Rp\Student::withTrashed()->where('id', $res)->count();

        if ($adm <= 0 && $cst <= 0 && $lst <= 0)
            break;
        else
            $add++;
    }
    return $res;
}

function selectMyDepartment()
{
    $departments = college('MyDepartment');
    $dep = [];
    foreach ($departments as $department) {
        $dep[$department->id] = $department->department;
    }

    return $dep;
}

function getNamesForName($name)
{
    $name = explode(" ", trim($name));
    $first = "";
    $last = "";
    if (count($name) > 0) {
        foreach ($name as $k => $item) {
            if ($k == 0)
                $first = $item;
            else
                $last .= $item . " ";
        }
    }

    return [$first, $last];
}

function isPrivate($stid)
{
    if (strlen($stid) > 1) {
        $lo = substr($stid, -1);
        if (strtoupper($lo) == "P")
            return true;
        else
            return false;
    }
    return null;
}

function admission()
{
    $user = auth()->guard('student');
    if ($user->check()) {
        $sdid = get_my_info('admission');
        return $sdid == '1' ? true : false;
    }
    return true;
}

function studyCategory()
{
    return [
        'Day' => 'Day',
        'Night' => 'Night',
        'Weekend' => 'Weekend'
    ];
}

function maritalStatus()
{
    return [
        'Single' => 'Single',
        'Married' => 'Married',
        'Divorced' => 'Divorced',
        'Widowed' => 'Widowed'
    ];
}

function chronicDisease()
{
    $disease = \App\Rp\ChronicDisease::all();
    foreach ($disease as $item) {
        $disease[$item->name] = ucwords($item->name);
    }
    return $disease;
}

function getAdmissionLetter($college = null, $year = null)
{
    if ($college == null)
        $college = college('college_id');
    if ($year == null)
        $year = date("Y");

    $letter = \App\Rp\AdmissionLetter::where('college_id', $college)->first();
    if ($letter)
        return $letter->letter;
    else
        return "";

}

function getCollegeLogo()
{
    if (auth()->guard('student')->check()) {
        $logo = get_my_info('college')->logo;
        return Storage::disk('public')->url($logo);
    }

    return "";
}

function checkTransfer($obj = false, $col = null)
{
    $transfer = \App\Rp\TransferRequest::where('student_id', get_my_info());
    if ($obj === true) {
        $trans = $transfer->first();
        if ($trans)
            return $col != null ? $trans->{$col} : "";
    }
    if ($transfer->count() > 0)
        return true;
    else
        return false;
}

function recentStudents()
{
    $students = \App\Rp\CollegeStudent::where('year_of_study', date('Y'))
        ->orderBy("created_at", "DESC")
        ->get();

    return $students;
}

function otherYears($sty)
{
    $y = ['2' => '17', '3' => '16'];

    return isset($y[$sty]) ? $y[$sty] : null;
}

function isContinuingStudent($stdid)
{
    $student = \App\Rp\AdmittedStudent::where('std_id', $stdid)->first();

    if ($student) {
        if ($student->year_of_study == 1 || $student->year_of_study == 2018)
            return false;
        else
            return true;
    }

    $student = \App\Rp\CollegeStudent::where('student_reg', $stdid)->first();
    if ($student) {
        if ($student->year_of_study == 1 || $student->year_of_study == 2018)
            return false;
        else
            return true;
    }

    return false;

//    $str = substr($stdid, 0, 2);
//    $crt = date('y');
//
//    if ($str == $crt)
//        return false;
//    else
//        return true;
}

function getMyYearOfStudy($letter = false, $year = null)
{
    if ($year == null)
        $yea = get_my_info('year_of_study');
    else
        $yea = $year;

    $v = strlen($yea);
    $do = 0;
    if ($v == 2) {
        $c = date('y');
        $do = $c - $yea;
    } elseif ($v == 1)
        $do = $yea;
    elseif ($v == 4) {
        $c = date('Y');
        $do = $c - $yea;
    }

    if ($v > 1)
        $do += 1;

    $key = [
        '1' => 'first',
        '2' => 'second',
        '3' => 'third',
    ];

    if ($letter === true)
        return isset($key[$do]) ? $key[$do] : 'undefined';

    return $do;
}

function getApplicationFees($col = null)
{
    $application = \App\Model\Rp\FunctionalFees::where('fee_name', 'LIKE', 'Application')->first();
    if ($application) {
        if ($col == null)
            return $application->fee_name;
        else
            return $application->{$col};

    }
}

function generatePaymentCode()
{
    $o = 1;
    $default = 8;
    do {
        if ($o > $default)
            $default++;
        else
            $o++;
        $unique = generate_random_letters($default);
    } while (is_in_table($unique));

    return $unique;
}

function is_in_table($unique)
{
    $check = App\Rp\PaymentInvoice::where('code', $unique)->count();
    $checkP = App\Rp\PendingPaymentInvoice::where('code', $unique)->count();
    if ($check >= 1 || $checkP >= 1)
        return true;
    elseif ($check <= 0 && $checkP <= 0)
        return false;
}

function generate_random_letters($length)
{
    $random = '';
    for ($i = 0; $i < $length; $i++) {
        $random .= chr(rand(ord('a'), ord('z')));
    }
    return date('y') . $random;
}

function getPaidAmountFromInvoice($code, $student, $formatted = true)
{
    $pending = \App\Rp\PendingPaymentInvoice::where('student_id', $student)
        ->where('code', $code)->first();
    if ($pending)
        return $formatted ? number_format($pending->paid) : $pending->paid;
    else
        return 0;
}

function getRemainAmountFromInvoice($code, $student, $formatted = true)
{
    $pending = \App\Rp\PendingPaymentInvoice::where('student_id', $student)
        ->where('code', $code)->first();
    if ($pending) {
        $remain = $pending->amount - $pending->paid;
        return $formatted ? number_format($remain) : $remain;
    } else
        return 0;
}

function getSponsorsList($sp = null)
{

}

function enrolled($sid, $model = false, $col = null)
{
    $student = App\Model\Rp\EnrolStudent::where('student_id', $sid)->first();
    if ($model == false) {
        if (is_null($student))
            return false;
        else
            return true;
    } elseif ($model == true) {
        if ($student) {
            if ($col)
                return $student->{$col};
        }
    }

    return null;
}

function doHavePendingCategory($name, $student)
{
    $pending = \App\Rp\PendingPaymentInvoice::where('student_id', $student)
        ->where('name', 'LIKE', "%" . ucwords($name) . "%")
        ->where("active", true);
    if ($pending->count() <= 0)
        return true;

    return false;
}

function doStudentPaidThisCategory($name, $student)
{
    $pending = \App\Rp\PendingPaymentInvoice::where('student_id', $student)
        ->where('name', 'LIKE', "%" . ucwords($name) . "%")
        ->where('amount', '<=', 'paid')
        ->where("active", false)
        ->whereYear('created_at', date('Y'));
    if ($pending->count() > 0)
        return true;
    else {
        $category = getPaymentCategoryID($name);
        if (paidOutSide($student, null, $category) === true)
            return true;
    }

    return false;
}

function isStudentRepeated($std)
{
    $student = \App\CollegeContinuingStudent::where('student_reg', $std)->first();
    if ($student) {
        if ($student->repeat == 1)
            return true;
    }
    return false;
}

function getPaymentCategoryID($category)
{
    $category = App\FeeCategory::where('category_name', "LIKE", ucwords($category))->first();
    if ($category)
        return $category->id;

    return null;
}

function isInvoiceCodeExists($code)
{
    return is_in_table($code);
}

function isRegistered($student, $academic_year = null)
{
    if ($academic_year == null)
        $academic_year = getCurrentAcademicYear();

    $registered = \App\Model\Rp\StudentRegistered::where('student_id', $student)
        ->where('academic_year', $academic_year);

    if ($registered->count() > 0)
        return true;

    return false;


}

function getModalForSingleStudentSearch($key = null)
{
    $modals = [
//        'i' => 'Invoice',
//        'p' => 'Payments',
        'u' => 'Login Account',
        'm' => 'Information',
        'r' => 'Repeating Student'
    ];

    if (isset($modals[$key]))
        return $modals[$key];

    return $modals;
}

function getActionForSingleStudent($key = null)
{
    $actions = [
        'e' => 'Edit',
        'd' => 'Delete',
        'v' => 'View'
    ];
    if (isset($actions[$key]))
        return $actions[$key];

    return $actions;
}

function getOutSidePaymentsCategories($key = null)
{
    $categories = [
        '109' => 'Paid without Invoice',
        '110' => 'Paid without StudentId or Invoice',
        '111' => 'Paid by money transferring',
        '112' => 'Paid in other banks'
    ];
    if ($key != null) {
        if (isset($categories[$key]))
            return $categories[$key];
        else
            return "";
    }

    return $categories;
}

function getCurrentAcademicYear()
{
    return config('mis.academic_year', date('Y - ') . date('Y', strtotime('+1y')));
}

function getCurrentSemester()
{
    return config('mis.semester', 1);
}

function getClassification($avg)
{
    switch ($avg) {
        case $avg > 80:
            return "First Class";
            break;
        case $avg < 79:
        case $avg > 70:
            return "Second Class Upper Division";
            break;
        case $avg < 69:
        case $avg > 60:
            return "Second Class Lower Division";
            break;
        case $avg < 59:
        case $avg > 50:
            return "Pass";
            break;
        case $avg < 49:
            return "Fail";
            break;
    }
}

function getLecturerCourse($course, $needed)
{
    if ($course) {
        switch ($needed) {
            case 'name':
                if ($course->year_of_study == 1) {
                    if ($course->course)
                        return $course->course->course_unit_name;
                } else {
                    if ($course->courseY)
                        return $course->courseY->module_name;
                }
                break;
            case 'program':
                if ($course->year_of_study == 1) {
                    if ($course->course) {
                        if ($course->course->program)
                            return $course->course->program->program_name;
                    }
                } else {
                    if ($course->courseY) {
                        if ($course->courseY->option)
                            return $course->courseY->option->option_name;
                    }
                }
                break;
            case 'semester':
                if ($course->year_of_study == 1) {
                    if ($course->course) {
                        if ($course->course->program)
                            return $course->course->semester ? $course->course->semester->semester : "";
                    }
                } else {
                    if ($course->courseY) {
                        if ($course->courseY->option)
                            return $course->courseY->semester;
                    }
                }
                break;

            case 'code':
            case 'module_code':
                if ($course->year_of_study == 1) {
                    if ($course->course)
                        return $course->course ? $course->course->course_unit_code : "";
                } else {
                    if ($course->courseY)
                        return $course->courseY->module_code;
                }
                break;
        }
    }

    return "";
}

function lecturer($id = null, $needed = null)
{
    $lecturer = null;
    if ($id != null)
        $lecturer = \App\Lecturer::find($id);
    elseif (auth()->guard('lecturer')->check())
        $lecturer = auth()->guard('lecturer')->user();
    if ($lecturer) {
        if ($needed != null)
            return $lecturer->{$needed} ?: null;
    }
    return $lecturer;
}

function getStudentModuleMarks($collection, $category = null, $needed = null)
{
    if ($collection) {
        if ($collection->count() > 0) {
            if ($category)
                $collection = $collection->where('category_id', 'LIKE', $category)->first();
            if ($collection && $needed) {
                if ($needed == "sum")
                    $collection = $collection->sum('marks');
                else
                    $collection = $collection->{$needed};
            }
            return $collection;
        }
    }
    return null;
}

function getStudentExamModuleMarks($collection, $needed = null)
{
    if ($collection) {
        if ($collection->count() > 0) {
            $collection = $collection->first();
            if ($collection && $needed)
                $collection = $collection->{$needed};
            return $collection;
        }
    }
    return null;
}

function isExamTime()
{
    return true;
}

function getStudentFromLecturer($course)
{
    if ($course)
        return App\Rp\CollegeStudent::where('college_id', lecturer(null, 'college_id'))
            ->where('department_id', $course->department_id)
            ->where('course_id', $course->program_id)
            ->where('year_of_study', $course->year_of_study)
            ->orderBy('first_name', 'ASC')
            ->orderBy('other_names', 'ASC')
            ->get(['student_reg', 'first_name', 'other_names']);
    else
        return collect([]);
}

function getCollegeCategoryStaff($key = null)
{
    $arr = [
        'administrative' => 'Administrative',
        'academic' => 'Academic'
    ];
    if ($key) {
        if (isset($arr[$key]))
            return $arr[$key];
        else
            return "";
    }

    return $arr;
}

function collegeAllowed($permission = 1, $role = 'd')
{
    /**
     * 1 = Browser
     * 2 = Read
     * 3 = Edit
     * 4 = Add
     * 5 = Delete
     * 7 = All
     */

    if (auth()->guard('college')->check()) {
        $perms = auth()->guard('college')->user()->permission;
        $uRole = auth()->guard('college')->user()->roles;

        if ($perms == $permission && $role == $uRole)
            return true;
        elseif ($perms == 7 && $role == $uRole)
            return true;
        elseif (strpos((string)$perms, (string)$permission) !== false && $role == $uRole)
            return true;
    }
    return false;
}

function getCollegePermission($key = null)
{
    $permissions = [
        '1' => 'Browse',
        '2' => 'Read',
        '3' => 'Edit',
        '4' => 'Add',
        '5' => 'Delete',
        '7' => 'All'
    ];
    if ($key) {
        if (isset($permissions[$key]))
            return $permissions[$key];
        else
            return "";
    }

    return $permissions;


}

function getCollegeUserRole($key = null)
{
    $roles = [
        '0' => 'Principal',
        '8' => 'Deputy Principal Academic and Training (DPAT)',
        '9' => 'Division Manager (DM)',
        'd' => 'Director of Academic Services (DAS)',
        'r' => 'Admission and Registration Officer',
        'a' => 'Academic Records Officer'
    ];

    if ($key != null) {
        if (isset($roles[$key]))
            return $roles[$key];
    }

    return $roles;
}

function isHOD($id = null)
{
    if ($id == null){
        if (auth()->guard('lecturer')->check())
            return auth()->guard('lecturer')->user()->is_head_of_department == 1 ? true : false;

        return false;
    }
    else {
        $l = \App\Lecturer::find($id);
        return $l->is_head_of_department == 1 ? true : false;
    }
}