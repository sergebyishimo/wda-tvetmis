<?php

namespace App;

use App\Model\Accr\QualificationLevel;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class StaffsInfo extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Uuid;

//    use SoftDeletes;
//    protected $dates = ['deleted_at'];
    protected $connection = "mysql";
    protected $table = "staffs_info";

    protected $fillable = ['school_id', 'first_name', 'last_name', 'staff_category', 'phone_number', 'gender', 'email', 'nationality', 'qualification_level', 'qualification', 'institution', 'graduated_year'];

    protected $appends = ['names'];

    public $incrementing = false;

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function department()
    {
        return $this->hasOne(Department::class);
    }

    public function attachement()
    {
        return $this->hasMany(StaffProfileAttachmentsFinal::class, 'staff_id', 'id');
    }

    public function markingApplications()
    {
        return $this->hasMany(StaffMarkerApplication::class, 'staff_id', 'id');
    }

    public function getCurrentMarkingAttribute()
    {
        if ($this->markingApplications) {
            $ch = $this->markingApplications()->where('academic_year', date('Y'))->first();
            if ($ch)
                return $ch;
        }

        return false;
    }

    public function assessorApplications()
    {
        return $this->hasMany(StaffAssessorApplication::class, 'staff_id', 'id');
    }

    public function getCurrentAssessorAttribute()
    {
        if ($this->assessorApplications) {
            $ch = $this->assessorApplications()->where('academic_year', date('Y'))->first();
            if ($ch)
                return $ch;
        }
        return false;
    }

    public function level()
    {
        return $this->hasOne(Level::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function attendance_staff()
    {
        return $this->hasMany(AttendanceStaff::class, 'staff_id', 'id');
    }

    public function getNamesAttribute()
    {
        return ucwords(trim($this->first_name . " " . $this->last_name));
    }

    public function getSchoolNameAttribute()
    {
        if ($this->school)
            return $this->school->name;

        return "";

    }

    public function getSchoolAcronymAttribute()
    {
        if ($this->school)
            return $this->school->acronym;

        return "";

    }

    public function assessorBackgrounds()
    {
        return $this->hasMany(StaffAssessorBackground::class, 'staff_id', 'id');
    }

    public function markingBackgrounds()
    {
        return $this->hasMany(StaffMarkingBackground::class, 'staff_id', 'id');
    }

    public function teachingExperience()
    {
        return $this->hasMany(StaffTeachingExperience::class, 'staff_id', 'id');
    }

    public function workingExperience()
    {
        return $this->hasMany(StaffWorkexperience::class, 'staff_id');
    }

    public function detail()
    {
        return $this->hasOne(StaffRttiDetails::class, 'staff_id');
    }

    public function qualifications()
    {
        return $this->hasMany(StaffQualification::class, 'staff_id');
    }

    public function qualificationLevel()
    {
        return $this->hasMany(QualificationLevel::class, 'qualification_level');
    }

    public function languages()
    {
        return $this->hasOne(StaffLanguage::class, 'staff_id');
    }

    public function computer()
    {
        return $this->hasOne(StaffComputerSkills::class, 'staff_id');
    }

    public function internship()
    {
        return $this->hasMany(StaffIap::class, 'staff_id');
    }

    public function medical()
    {
        return $this->hasOne(StaffMedicalHistory::class, 'staff_id');
    }

    public function training()
    {
        return $this->hasMany(StaffRttiTrainingReference::class, 'staff_id');
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function modules() {
        return $this->hasMany(TrainersModulesTaught::class, 'staff_id');
    }

}
