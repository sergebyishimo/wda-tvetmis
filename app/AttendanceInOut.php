<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class AttendanceInOut extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;
    protected $fillable = [
        'date', 'cometime', 'leavetime', 'std_id', 'school_id'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'std_id', 'id');
    }

}
