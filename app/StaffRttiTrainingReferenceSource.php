<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffRttiTrainingReferenceSource extends Model
{
    protected $fillable = [
        'training_reference'
    ];

}
