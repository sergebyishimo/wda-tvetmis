<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table = "attachments";
    protected $connection = "mysql";

    public function attachment()
    {
        return $this->hasMany(StaffProfileAttachmentsFinal::class, 'attachment_description', 'id');
    }
}
