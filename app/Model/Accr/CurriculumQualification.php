<?php

namespace App\Model\Accr;

use App\Module;
use Illuminate\Database\Eloquent\Model;
use App\Department;

class CurriculumQualification extends Model
{
    protected $table="curriculum_qualifications";
    protected $connection = 'accr_mysql';

    public function subsector()
    {
    	return $this->belongsTo(TrainingTradesSource::class, 'sub_sector_id');
    }

    public function rtqf()
    {
    	return $this->belongsTo(RtqfSource::class, 'rtqf_level_id');
    }

    public function departments() {
        return $this->hasMany(Department::class, 'qualification_id', 'uuid');
    }
    public function modules() {
        return $this->hasMany(Module::class, 'qualification_id', 'uuid');
    }
}
