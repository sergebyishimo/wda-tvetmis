<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ProjectProgram extends Model
{
    protected $connection = 'accr_mysql';

    public function project()
    {
    	return $this->belongsTo(Project::class, 'project_id');
    }

    public function results()
    {
    	return $this->hasMany(ProjectResult::class, 'program_id');
    }
}
