<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrBuildingsAndPlots extends Model
{
    protected $connection = 'accr_mysql';

    public function infras()
    {
    	return $this->hasMany(AccrBuildingPlotInfrastructure::class, 'building_id');
    }
}
