<?php namespace App\Model\Accr;



use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

  protected $connection = 'accr_mysql';

  protected $throwValidationExceptions = true;

  protected $fillable = [
    'name',
    'display_name',
    'description',
  ];

  protected $rules = [
    'name'      => 'required|unique:permissions',
  ];
}
