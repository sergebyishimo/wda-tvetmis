<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrWelcomeMessage extends Model
{
    protected $connection = 'accr_mysql';
}
