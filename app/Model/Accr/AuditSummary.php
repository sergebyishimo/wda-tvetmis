<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AuditSummary extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable =[
        'school_id', 'quality_id', 'indicator_id', 'summary'
    ];

    public function school()
    {
        return $this->belongsTo(SchoolInformation::class,'school_id');
    }

    public function quality()
    {
        return $this->belongsTo(AuditQuality::class,'quality_id');
    }

    public function indicator()
    {
        return $this->belongsTo(AuditIndicator::class,'indicator_id');
    }

}
