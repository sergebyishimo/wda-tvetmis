<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class TrainingTradesSource extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'training_trades_source';

    public function getRealCurrIdAttribute()
    {

        $curr = CurriculumQualification::where('rtqf_level_id', $this->real_rtqf_id)->where('sub_sector_id', $this->id)->first();

        if ($curr) {
            return $curr->id;
        } else {
            return '';
        }
    }

    public function getRealTitleAttribute()
    {

        $curr = CurriculumQualification::where('rtqf_level_id', $this->real_rtqf_id)->where('sub_sector_id', $this->id)->first();

        if ($curr) {
            return $curr->qualification_title;
        } else {
            return '';
        }
    }

    public function getRealJobsAttribute()
    {

        $curr = CurriculumQualification::where('rtqf_level_id', $this->real_rtqf_id)->where('sub_sector_id', $this->id)->first();

        if ($curr) {
            return $curr->job_titles;
        } else {
            return '';
        }
    }

    public function sector()
    {
        return $this->belongsTo(TrainingSectorsSource::class, 'sector_id');
    }

    public function curriculumQualification()
    {
        return $this->hasMany(CurriculumQualification::class, 'sub_sector_id');
    }

}
