<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ProgramsOffered extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'programs_offered';

    public function subsector()
    {
    	return $this->belongsTo(TrainingTradesSource::class, 'sub_sector_id');
    }

    public function rtqf_level()
    {
    	return $this->belongsTo(RtqfSource::class, 'rtqf_level_id');
    }

    public function qualification()
    {
        return $this->belongsTo(CurriculumQualification::class, 'qualification_id');
    }
}
