<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class CbtSchool extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'cbt_school';
}
