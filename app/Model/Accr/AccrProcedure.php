<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrProcedure extends Model
{
    protected $connection = 'accr_mysql';
    protected $fillable = [
        'description', 'attachment'
    ];
}
