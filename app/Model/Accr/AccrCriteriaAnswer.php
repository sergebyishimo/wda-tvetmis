<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrCriteriaAnswer extends Model
{
    protected $connection = 'accr_mysql';

    public function criteria()
    {
        return $this->belongsTo(AccrCriteria::class, '	criteria_id', 'id');
    }
}
