<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ApResult extends Model
{
    protected $connection = 'accr_mysql';

    public function program() {
        return $this->belongsTo(ApProgram::class, 'program_id');
    }

    public function indicators() {
        return $this->hasMany(ApIndicator::class, 'result_id');
    }
}
