<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class DistrictProvisionalAQA extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'district_provisional_aqa';
}
