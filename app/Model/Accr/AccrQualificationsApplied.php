<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrQualificationsApplied extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'accr_qualifications_applied_for';

    public function curr()
    {
    	return $this->belongsTo(CurriculumQualification::class, 'curriculum_qualification_id');
    }

    public function rtqf()
    {
    	return $this->belongsTo(RtqfSource::class, 'rtqf_level_id');
    }

    public function application()
    {
        return $this->belongsTo(AccrApplication::class, 'application_id');
    }

}
