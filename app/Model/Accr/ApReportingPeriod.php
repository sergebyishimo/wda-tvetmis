<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ApReportingPeriod extends Model
{
    protected $connection = 'accr_mysql';

    public function programs()
    {
    	return $this->hasMany(ApProgram::class, 'rp_id');
    }
}
