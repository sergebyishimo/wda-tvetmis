<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ApProgram extends Model
{
    protected $connection = 'accr_mysql';

	public function rp()
	{
		return $this->belongsTo(ApReportingPeriod::class, 'rp_id');
	}

    public function results()
    {
        return $this->hasMany(ApResult::class, 'program_id');
    }
}
