<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class RtqfSource extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'rtqf_source';

    public function curriculumQualification()
    {
        return $this->hasMany(CurriculumQualification::class, 'rtqf_level_id');
    }
}
