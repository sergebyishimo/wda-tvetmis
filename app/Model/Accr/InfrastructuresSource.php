<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class InfrastructuresSource extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'infrastructures_source';
}
