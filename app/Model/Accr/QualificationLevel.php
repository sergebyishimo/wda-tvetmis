<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class QualificationLevel extends Model
{
    protected $connection = 'accr_mysql';
}
