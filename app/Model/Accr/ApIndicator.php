<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ApIndicator extends Model
{
    protected $connection = 'accr_mysql';

    public function result()
	{
		return $this->belongsTo(ApResult::class, 'result_id');
	}
}
