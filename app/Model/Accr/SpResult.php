<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SpResult extends Model
{
    protected $connection = 'accr_mysql';

    public function program()
    {
        return $this->belongsTo(SpProgram::class, 'program_id');
    }

    public function indicators()
    {
        return $this->hasMany(SpIndicator::class, 'result_id');
    }
}
