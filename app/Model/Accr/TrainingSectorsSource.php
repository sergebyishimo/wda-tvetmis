<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class TrainingSectorsSource extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'training_sectors_source';

    public function subsectors()
    {
        return $this->hasMany(TrainingTradesSource::class, 'sector_id');
    }
}
