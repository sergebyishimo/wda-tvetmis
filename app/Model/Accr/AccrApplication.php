<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrApplication extends Model
{
    protected $connection = 'accr_mysql';

    public function applied_for()
    {
        return $this->hasMany(AccrQualificationsApplied::class, 'application_id');
    }

    public function school()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id', 'id');
    }

    public function accrAttachment()
    {
        return $this->hasMany(AccrAttachment::class, 'application_id', 'id');
    }

    public function accrQualificationsApplied()
    {
        return $this->hasMany(AccrQualificationsApplied::class, 'application_id');
    }

    public function accreditationType()
    {
        return $this->belongsTo(AccreditationType::class, 'accreditation_type_id');
    }
}
