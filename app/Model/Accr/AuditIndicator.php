<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AuditIndicator extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'quality_id', 'name'
    ];

    public function quality()
    {
        return $this->belongsTo(AuditQuality::class, 'quality_id');
    }

    public function auditSummary()
    {
        return $this->hasMany(AuditSummary::class, 'indicator_id');
    }

}
