<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrCriteriaSection extends Model
{
    protected $connection = 'accr_mysql';

    public function criterias()
    {
    	return $this->hasMany(AccrCriteria::class, 'criteria_section_id');
    }
}
