<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ApSubProgram extends Model
{
    protected $connection = 'accr_mysql';

	public function program()
	{
		return $this->belongsTo(ApProgram::class, 'ap_program_id');
	}

    public function outputs()
    {
        return $this->hasMany(ApOutput::class, 'ap_sub_program_id');
    }

    public function real_name()
    {
    	return $this->name;
    }

}
