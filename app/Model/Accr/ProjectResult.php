<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ProjectResult extends Model
{
    protected $connection = 'accr_mysql';

    public function program() {
        return $this->belongsTo(ProjectProgram::class, 'program_id');
    }

    public function indicators() {
        return $this->hasMany(ProjectsIndicator::class, 'result_id');
    }
}
