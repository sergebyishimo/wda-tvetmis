<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
    protected $connection = 'accr_mysql';

    public function output()
    {
        return $this->belongsTo(Output::class);
    }
}
