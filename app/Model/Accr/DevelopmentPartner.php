<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class DevelopmentPartner extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'mande_minecofin_development_partner';
    
}
