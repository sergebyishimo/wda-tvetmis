<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ProjectFinanceTypeSource extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'mande_minecofin_project_finance_type_source'; 
}
