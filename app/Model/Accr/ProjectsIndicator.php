<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ProjectsIndicator extends Model
{
    protected $connection = 'accr_mysql';

    public function result()
    {
    	return $this->belongsTo(ProjectResult::class, 'result_id');
    }
}
