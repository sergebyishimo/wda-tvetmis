<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SourceSchoolOwnership extends Model
{
    protected $connection = 'accr_mysql';
    protected $table = 'source_school_ownership';

}
