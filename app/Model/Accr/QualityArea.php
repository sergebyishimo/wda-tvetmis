<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class QualityArea extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'function_id', 'name'
    ];

    public function functions()
    {
        return $this->belongsTo(Functions::class, 'function_id');
    }

    public function criteria()
    {
        return $this->hasMany(AccrCriteriaSection::class,'quality_area_id');
    }

}
