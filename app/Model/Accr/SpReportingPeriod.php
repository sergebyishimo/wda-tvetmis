<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SpReportingPeriod extends Model
{
    protected $connection = 'accr_mysql';
}
