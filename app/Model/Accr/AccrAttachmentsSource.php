<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrAttachmentsSource extends Model
{
    //
    protected $connection = 'accr_mysql';
    protected $table = 'accr_attachments_source';

}
