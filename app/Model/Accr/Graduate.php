<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Graduate extends Model
{
    protected $connection = 'accr_mysql';
    protected $fillable = ['names', 'gender', 'trade', 'year', 'phone_number', 'employment_status'];
}
