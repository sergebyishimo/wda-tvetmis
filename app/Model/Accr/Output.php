<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Output extends Model
{
    protected $connection = 'accr_mysql';

    public function outcome()
    {
        return $this->belongsTo(Outcome::class);
    }

    public function indicators()
    {
        return $this->hasMany(Indicator::class);
    }

    public function activities()
    {
        return $this->hasMany(Activities::class);
    }
}
