<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrSchoolAssessmentComment extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'school_id', 'function_id', 'quality_id', 'strength', 'weakness', 'recommendation',
        'timeline', 'from', 'academic_year'
    ];
}
