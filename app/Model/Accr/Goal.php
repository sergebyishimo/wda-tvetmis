<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $connection = 'accr_mysql';

    public function outcomes()
    {
        return $this->hasMany('App\Outcome');
    }
}
