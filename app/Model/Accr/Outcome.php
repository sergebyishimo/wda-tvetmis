<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Outcome extends Model
{
    protected $connection = 'accr_mysql';

    public function goal()
    {
        return $this->belongsTo(Goal::class);
    }

    public function outputs()
    {
        return $this->hasMany(Output::class);
    }
}
