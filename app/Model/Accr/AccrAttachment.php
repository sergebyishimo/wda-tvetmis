<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrAttachment extends Model
{
    protected $connection = 'accr_mysql';

    public function source()
    {
    	return $this->belongsTo(AccrAttachmentsSource::class, 'attachment_id','id');
    }

    public function accrApplication()
    {
        return $this->belongsTo(AccrApplication::class,'application_id', 'id');
    }

    public function getSourceAttribute()
    {
        if ($this->source())
            return $this->source()->first()->attachment_name;

        return "";
    }

}
