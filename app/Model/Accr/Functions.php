<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Functions extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'name', 'description'
    ];

    public function qualityAreas()
    {
        return $this->hasMany(QualityArea::class, 'function_id');
    }

    public function selfAssessmentStatus()
    {
        return $this->hasMany(SchoolSelfAssessimentStatus::class, 'function_id');
    }

}
