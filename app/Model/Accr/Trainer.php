<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $connection = 'accr_mysql';

    public function q_level()
    {
        return $this->belongsTo(QualificationLevel::class, 'qualification_level_id');
    }
}
