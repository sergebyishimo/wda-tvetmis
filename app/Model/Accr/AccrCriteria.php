<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Null_;
use Carbon\Carbon;

class AccrCriteria extends Model
{
    protected $connection = 'accr_mysql';

    public function section()
    {
        return $this->belongsTo(AccrCriteriaSection::class, 'criteria_section_id');
    }

    public function qualityArea()
    {
        return $this->belongsTo(QualityArea::class, 'quality_area_id');
    }

    public function answers()
    {
        return $this->hasMany(AccrCriteriaAnswer::class, 'criteria_id', 'id');
    }

    public function getMyAnswerAttribute()
    {
        if (auth()->check()) {
            $school = auth()->user()->school_id;
            if ($school) {
                $answer = $this->answers()->where('school_id', $school)
                    ->where('academic_year', date('Y'))
                    ->whereDate('created_at', Carbon::today())
                    ->first();
                if ($answer)
                    return $answer->answer ?: "";
            }
        }

        return "";
    }

    public function getSchoolAnswer($school, $sub = null, $year = null)
    {
        if ($school) {
            $answer = $this->answers()->where('school_id', $school)
                ->where('academic_year', date('Y'));
            if ($sub)
                $answer = $answer->where('submit_id', $sub);

            if ($year)
                $answer = $answer->whereYear('created_at', $year);

            $answer = $answer->first();
            if ($answer)
                return strtoupper($answer->answer) ?: "";
        }

        return Null;
    }

    public function getWdaAnswer($school, $sub = null, $year = null)
    {
        if ($school) {
            $answer = $this->answers()->where('school_id', $school)
                ->where('academic_year', date('Y'));
            if ($sub)
                $answer = $answer->where('submit_id', $sub);
            if ($year)
                $answer = $answer->whereYear('created_at', $year);

            $answer = $answer->first();
            if ($answer)
                return strtoupper($answer->wda) ?: "";
        }

        return Null;
    }

    public function getDistrictAnswer($school, $sub = null, $year = null)
    {
        if ($school) {
            $answer = $this->answers()->where('school_id', $school)
                ->where('academic_year', date('Y'));
            if ($sub)
                $answer = $answer->where('submit_id', $sub);

            if ($year)
                $answer = $answer->whereYear('created_at', $year);

            $answer = $answer->first();
            if ($answer)
                return strtoupper($answer->district) ?: "";
        }

        return Null;
    }

    public function getDistrictComment($school = null, $sub = null)
    {
        if ($school != null) {
            $answer = $this->answers()
                ->where('school_id', $school)
                ->where('academic_year', date('Y'));
            if ($sub)
                $answer = $answer->where('submit_id', $sub);

            $answer = $answer->first();

            if ($answer)
                return strtoupper($answer->district);

        }

        return null;
    }

    public function getWdaComment($school = null, $sub = null)
    {
        if ($school != null) {
            $answer = $this->answers()
                ->where('school_id', $school)
                ->where('academic_year', date('Y'));

            if ($sub)
                $answer->where('submit_id', $sub);

            $answer = $answer->first();

            if ($answer)
                return strtoupper($answer->wda);
        }

        return null;
    }
}
