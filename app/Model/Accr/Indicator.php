<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    protected $connection = 'accr_mysql';

    public function output()
    {
        return $this->belongsTo(Output::class);
    }

    public function plans()
    {
        return $this->hasMany(IndicatorPlan::class);
    }
}
