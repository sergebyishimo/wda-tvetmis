<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrAccreditationType extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'description', 'attachment'
    ];
}
