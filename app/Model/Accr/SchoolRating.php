<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SchoolRating extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'source_rating';

    public function schools()
    {
        return $this->hasMany(SchoolInformation::class, 'school_rating', 'id');
    }
}
