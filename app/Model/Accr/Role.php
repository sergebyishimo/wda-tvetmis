<?php namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];

    protected $rules = [
        'name' => 'required|unique:roles',
        'display_name' => 'required|unique:roles',
    ];
}
