<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccrTimeline extends Model
{
    protected $connection = 'accr_mysql';
    protected $table = "timelines";
}
