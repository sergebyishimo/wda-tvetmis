<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SpIndicator extends Model
{
    protected $connection = 'accr_mysql';

    public function result()
    {
        return $this->belongsTo(SpResult::class, 'result_id');
    }
}
