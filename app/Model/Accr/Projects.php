<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'mande_minecofin_projects';
}
