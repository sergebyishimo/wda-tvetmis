<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class WdaProvisionalAQA extends Model
{
    protected $connection = 'accr_mysql';
    protected $table = 'wda_provisional_aqa';
}
