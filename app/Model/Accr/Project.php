<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'mande_minecofin_projects';
}
