<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AccreditationType extends Model
{
    protected $fillable = ['type'];
    protected $connection = 'accr_mysql';

    public function accrApplication()
    {
        return $this->hasMany(AccrApplication::class, 'accreditation_type_id');
    }
}
