<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ProjectsOutput extends Model
{
    protected $connection = 'accr_mysql';

    public function outcome($value = '')
    {
        return $this->belongsTo(ProjectsOutcome::class, 'outcome_id');
    }

    public function indicators($value = '')
    {
        return $this->hasMany(ProjectsIndicator::class, 'output_id');
    }

}
