<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SchoolSelfAssessimentStatus extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'school_id', 'function_id', 'school_confirm',
        'district_confirm', 'wda_confirm', 'academic_year', 'submit_id'
    ];

    public function functions()
    {
        return $this->belongsTo(Functions::class, 'function_id');
    }

    public function school()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id', 'id');
    }

}
