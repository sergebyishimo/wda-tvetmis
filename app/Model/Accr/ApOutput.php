<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ApOutput extends Model
{
    protected $connection = 'accr_mysql';

	public function subprogram()
	{
		return $this->belongsTo(ApSubProgram::class, 'ap_sub_program_id');
	}

    public function indicators()
    {
        return $this->hasMany(ApIndicator::class);
    }
}
