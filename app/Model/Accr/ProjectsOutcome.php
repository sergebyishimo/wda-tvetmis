<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class ProjectsOutcome extends Model
{
    protected $connection = 'accr_mysql';

    public function program()
    {
    	return $this->belongsTo(ProjectProgram::class, 'program_id');
    }

    public function outputs()
    {
    	return $this->hasMany(ProjectsOutput::class, 'outcome_id');
    }
}
