<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SpProgram extends Model
{
    protected $connection = 'accr_mysql';

    public function rp()
	{
		return $this->belongsTo(SpReportingPeriod::class, 'rp_id');
	}

    public function results()
    {
        return $this->hasMany(SpResult::class, 'program_id');
    }
}
