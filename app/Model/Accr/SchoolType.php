<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class SchoolType extends Model
{
    protected $connection = 'accr_mysql';

    protected $table = 'source_school_type';

    public function schools()
    {
        return $this->hasMany(SchoolInformation::class, 'school_type', 'id');
    }
}
