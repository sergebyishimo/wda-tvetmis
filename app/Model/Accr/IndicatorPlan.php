<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class IndicatorPlan extends Model
{
    protected $connection = 'accr_mysql';

    public function indicator()
    {
        return $this->belongsTo(Indicator::class);
    }

    public function quarters()
    {
        return $this->hasMany(IndicatorActionPlan::class);
    }
}
