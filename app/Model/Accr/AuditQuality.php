<?php

namespace App\Model\Accr;

use Illuminate\Database\Eloquent\Model;

class AuditQuality extends Model
{
    protected $connection = 'accr_mysql';

    protected $fillable = [
        'name'
    ];

    public function indicator()
    {
        return $this->hasMany(AuditIndicator::class, 'quality_id');
    }

    public function auditSummary()
    {
        return $this->hasMany(AuditSummary::class, 'quality_id');
    }
}
