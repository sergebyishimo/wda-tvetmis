<?php

namespace App\Model\Accr;

use App\CostOfTraining;
use App\Department;
use App\IncubationCenter;
use App\Level;
use App\OtherInfrastructure;
use App\ProductionUnit;
use App\SchoolActivityReport;
use App\SchoolAdmission;
use App\SchoolAnnouncement;
use App\SchoolDesk;
use App\SchoolRequest;
use App\SchoolUser;
use App\SchoolWorkshop;
use App\ShortCourse;
use App\StaffsInfo;
use App\Student;
use App\Traits\RelationshipsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kblais\Uuid\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class SchoolInformation extends Model implements Auditable
{
    //CbtSchool
    use Uuid;
    use SoftDeletes;
    use RelationshipsTrait;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'accr_mysql';

    protected $table = 'schools_information';
    public $incrementing = false;


    public function user()
    {
        return $this->hasMany(SchoolUser::class, 'school_id', 'id');
    }

    public function accrApplication()
    {
        return $this->hasMany(AccrApplication::class, 'school_id', 'id');
    }

    public function selfAssessmentStatus()
    {
        return $this->hasMany(SchoolSelfAssessimentStatus::class, 'school_id', 'id');
    }

    public function desks()
    {
        return $this->hasMany(SchoolDesk::class, 'school_id', 'id');
    }

    public function requests()
    {
        return $this->hasMany(SchoolRequest::class, 'school_id', 'id');
    }


    public function activityreports()
    {
        return $this->hasMany(SchoolActivityReport::class, 'school_id', 'id');
    }

    public function workshops()
    {
        return $this->hasMany(SchoolWorkshop::class, 'school_id', 'id');
    }

    public function incubationcenters()
    {
        return $this->hasMany(IncubationCenter::class, 'school_id', 'id');
    }

    public function productionunits()
    {
        return $this->hasMany(ProductionUnit::class, 'school_id', 'id');
    }

    public function otherinfrastructures()
    {
        return $this->hasMany(OtherInfrastructure::class, 'school_id', 'id');
    }

    public function admissionrequirement()
    {
        return $this->hasOne(SchoolAdmission::class, 'school_id', 'id');
    }

    public function departments()
    {
        return $this->hasMany(Department::class, 'school_id', 'id');
    }

    public function shortcourses()
    {
        return $this->hasMany(ShortCourse::class, 'school_id', 'id');
    }

    public function staffs()
    {
        return $this->hasMany(StaffsInfo::class, 'school_id', 'id');
    }

    public function announcements()
    {
        return $this->hasMany(SchoolAnnouncement::class, 'school_id', 'id');
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'school', 'id');
    }

    public function schooltype()
    {
        return $this->belongsTo(SchoolType::class, 'school_type');
    }

    public function schoolrating()
    {
        return $this->belongsTo(SchoolRating::class, 'school_rating', 'id');
    }

    public function levels()
    {
        return $this->hasMany(Level::class,'school_id','id');
    }

    public function costoftraining()
    {
        return $this->hasMany(CostOfTraining::class, 'school_id', 'id');
    }

    public function ownership()
    {
        return $this->belongsTo(SourceSchoolOwnership::class, 'owner_type', 'id');
    }

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function auditSummary()
    {
        return $this->hasMany(AuditSummary::class, 'school_id');
    }

    public function getSchoolTypeNameAttribute()
    {
//        $sty = SchoolType::find($this->school_type);
        $sty = $this->schooltype->school_type;
        if ($sty)
            return $sty;

        return "";
    }

    public function qualification($id=null)
    {
        $rest = DB::table('accr_schools_information')
            ->join('departments', 'departments.school_id', '=', 'accr_schools_information.id')
            ->where('departments.qualification_id', $id);

        return $rest;
    }
}
