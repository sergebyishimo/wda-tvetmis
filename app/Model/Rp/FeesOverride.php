<?php

namespace App\Model\Rp;

use App\College;
use App\FeeCategory;
use App\Rp\AdmittedStudent;
use App\Rp\CollegeStudent;
use App\Rp\Polytechnic;
use App\Rp\Student;
use Illuminate\Database\Eloquent\Model;

class FeesOverride extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'student_id', 'fee_category', 'installment_due_date', 'installment_amount', 'academic_year', 'college_id', 'category_id'
    ];

    public function student()
    {
        return $this->belongsTo(CollegeStudent::class,'student_id', 'student_reg');
    }

    public function college()
    {
        return $this->belongsTo(Polytechnic::class, 'college_id', 'id');
    }

    public function admitted()
    {
        return $this->belongsTo(AdmittedStudent::class,'student_id', 'std_id');
    }

    public function admittedPrivate()
    {
        return $this->belongsTo(AdmissionPrivate::class, 'student_id', 'std_id');
    }

    public function category()
    {
        return $this->belongsTo(FeeCategory::class, "fee_category", "id");
    }
}
