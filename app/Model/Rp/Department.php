<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    protected $connection = "admission_mysql";

    public function polytechnic()
    {
        return $this->belongsTo(Polytechnic::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class,  'department');
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

}
