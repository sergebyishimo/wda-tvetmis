<?php

namespace App\Model\Rp;

use App\FeeCategory;
use Illuminate\Database\Eloquent\Model;

class FunctionalFees extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "functional_fees";

    public function Categories()
    {
        return $this->belongsToMany(FeeCategory::class, "source_functional_fees_category", "fee_id", "fee_category_id");
    }
}
