<?php

namespace App\Rp;

use App\SchoolWorkshop;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $connection = "admission_mysql";

    public function workshops(){
        return $this->hasMany(SchoolWorkshop::class,'sector','id');
    }

}
