<?php

namespace App\Model\Rp;

use App\College;
use App\Lecturer;
use App\Rp\CollegeModule;
use Illuminate\Database\Eloquent\Model;

class LecturerAndCourse extends Model
{
    protected $fillable = [
        'college_id', 'lecturer_id', 'course_id', 'department_id', 'program_id', 'year_of_study', 'semester', 'academic_year'
    ];

    public function college()
    {
        return $this->belongsTo(College::class,'college_id', 'college_id');
    }

    public function lecture()
    {
        return $this->belongsTo(Lecturer::class,'lecturer_id','id');
    }

    public function courseY()
    {
        return $this->belongsTo(CollegeModule::class,'course_id','id');
    }

    public function course()
    {
        return $this->belongsTo(RpCourseUnit::class,'course_id','id');
    }

}
