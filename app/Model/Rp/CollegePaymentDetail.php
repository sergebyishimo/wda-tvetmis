<?php

namespace App\Rp;

use App\College;
use Illuminate\Database\Eloquent\Model;

class CollegePaymentDetail extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'college_id', 'category_id', 'amount', 'academic_year'
    ];

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'id');
    }

    public function paymentCategory()
    {
        return $this->belongsTo(PaymentCategory::class, 'category_id', 'id');
    }


}
