<?php

namespace App\Model\Rp;

use App\College;
use App\Rp\CollegeModule;
use App\Rp\CollegeStudent;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class CatMarks extends Model
{
    use Uuid;

    protected $connection = "mysql";

    protected $fillable = [
        'college_id', 'module_id', 'student_id', 'semester', 'category_id', 'marks', 'year_of_study', 'academic_year'
    ];

    public function courseY()
    {
        return $this->belongsTo(CollegeModule::class,'module_id','id');
    }

    public function course()
    {
        return $this->belongsTo(RpCourseUnit::class,'module_id','id');
    }

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'college_id');
    }

    public function student()
    {
        return $this->belongsTo(CollegeStudent::class,'student_id', 'student_reg');
    }

    public function category()
    {
        return $this->belongsTo(MarksCategorizing::class,'category_id', 'id');
    }

}
