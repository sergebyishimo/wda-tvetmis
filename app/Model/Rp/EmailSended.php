<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class EmailSended extends Model
{
    protected $connection = 'rp_mysql';

    protected $fillable = ['email', 'from'];
}
