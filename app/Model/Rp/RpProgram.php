<?php

namespace App\Model\Rp;


use App\Rp\CollegeStudent;
use App\Rtqf;
use App\Rp\Polytechnic;
use Illuminate\Database\Eloquent\Model;

class RpProgram extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "programs";

    public function departement()
    {
        return $this->belongsTo(RpDepartment::class, 'department_id');
    }

    public function colleges()
    {
        return $this->belongsToMany(Polytechnic::class, "college_programs", "program_id", "college_id");
    }

    public function department()
    {
        return $this->belongsTo(RpDepartment::class, 'department_id');
    }

    public function rtqf()
    {
        return $this->belongsTo(Rtqf::class, 'rtqf_level');
    }

    public function courseunits()
    {
        return $this->hasMany(RpCourseUnit::class, 'program_id', 'id');
    }

    public function students() {
        return $this->hasMany(CollegeStudent::class, 'course_id');
    }

    public function getChoicesAttribute()
    {
        return $this->program_name;
    }

    public function getOptionNameAttribute()
    {
        return $this->program_name;
    }
}
