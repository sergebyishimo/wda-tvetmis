<?php

namespace App\Rp;

use App\College;
use Illuminate\Database\Eloquent\Model;

class AdmissionLetter extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'college_id', 'letter'
    ];

    public function college()
    {
        $this->belongsTo(College::class);
    }
}
