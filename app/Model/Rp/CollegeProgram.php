<?php

namespace App\Model\Rp;

use App\Rp\Polytechnic;
use Illuminate\Database\Eloquent\Model;

class CollegeProgram extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "college_programs";
}
