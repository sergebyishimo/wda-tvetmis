<?php

namespace App\Model\Rp;

use App\Rp\AdmittedStudent;
use App\Rp\Polytechnic;
use Illuminate\Database\Eloquent\Model;

class EnrolStudent extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'college_id', 'student_id', 'enrol_date'
    ];

    public function college()
    {
        return $this->belongsTo(Polytechnic::class, 'college_id', 'id');
    }

    public function admitted()
    {
        return $this->belongsTo(AdmittedStudent::class,'student_id', 'std_id');
    }

    public function admittedPrivate()
    {
        return $this->belongsTo(AdmissionPrivate::class, 'student_id', 'std_id');
    }
}
