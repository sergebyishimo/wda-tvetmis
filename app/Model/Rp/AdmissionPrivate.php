<?php

namespace App\Model\Rp;

use App\College;
use App\Rp\Country;
use App\Rp\PaymentInvoice;
use Illuminate\Database\Eloquent\Model;

class AdmissionPrivate extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'college_id', 'std_id', 'first_name', 'other_names', 'email', 'phone', 'gender', 'ubudehe', 'dob',
        'want_student_loan', 'parents_phone', 'national_id_number', 'province', 'district', 'sector', 'cell',
        'village', 'bank_slip_number', 'scanned_bank_slip', 'photo', 'scan_of_diploma_or_certificate', 'your_bank',
        'your_bank_account', 'disability', 'examiner', 'index_number', 'school_attended', 'graduation_year', 'option_offered',
        'aggregates_obtained', 'payment_verified', 'scan_national_id_passport', 'year_of_study', 'country', 'sponsorship_status',
        'department_id', 'course_id', 'admission', 'student_category', 'address'
    ];

    public function getinfo($col, $id = null)
    {
        if ($id == null)
            $stdid = auth()->guard('student')->user()->id;
        else
            $stdid = $id;

        $check = $this->where('std_id', $stdid);
        if ($check->count() > 0) {
            $get = $check->first();
            return $get->{$col} ?: "";
        }
        return "";
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'college_id');
    }

    public function getNamesAttribute()
    {
        return ucwords(trim($this->first_name . " " . $this->other_names));
    }

    public function department()
    {
        return $this->belongsTo(RpDepartment::class,'department_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo(RpProgram::class,'course_id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(PaymentInvoice::class,'student_id', 'std_id');
    }

    public function overrideFees()
    {
        $this->hasMany(FeesOverride::class,'student_id', 'std_id');
    }
}