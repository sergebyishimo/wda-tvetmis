<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $connection = "rp_mysql";
    protected $table = 'enables';
    protected $fillable = [
        'title', 'academic_year', 'start', 'ends'
    ];
}
