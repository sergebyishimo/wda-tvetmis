<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class ChronicDisease extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = ['name'];
}
