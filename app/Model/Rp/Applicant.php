<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $connection = "admission_mysql";

    protected $table = "applicatants";

    public function firstChoice($pol)
    {
        $this->where('first_choice_polytechnic', $pol);
    }

    public function secondChoice($pol)
    {
        $this->where('second_choice_polytechnic', $pol);
    }

    public function thirdChoice($pol)
    {
        $this->where('third_choice_polytechnic', $pol);
    }

}
