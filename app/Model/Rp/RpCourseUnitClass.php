<?php

namespace App\Model\Rp;

use Illuminate\Database\Eloquent\Model;

class RpCourseUnitClass extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "source_course_unit_class";

    public function courseunits()
    {
        return $this->hasMany(RpCourseUnit::class, 'unit_class_id', 'id');
    }
}
