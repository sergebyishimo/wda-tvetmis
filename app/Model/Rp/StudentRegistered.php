<?php

namespace App\Model\Rp;

use App\Rp\CollegeStudent;
use App\Rp\Polytechnic;
use Illuminate\Database\Eloquent\Model;

class StudentRegistered extends Model
{
    protected $connection = 'rp_mysql';

    protected $table = 'student_registered';

    protected $fillable = [
        'academic_year', 'college_id', 'student_id', 'prof', 'year_of_study'
    ];

    public function college()
    {
        return $this->belongsTo(Polytechnic::class, 'college_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo(CollegeStudent::class,'student_id','student_reg');
    }

}
