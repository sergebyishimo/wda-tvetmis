<?php

namespace App\Rp;

use App\Model\Rp\FunctionalFees;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentInvoice extends Model
{
    use softDeletes;

    protected $connection = 'rp_mysql';

    protected $fillable = [
        'student_id', 'function_fee', 'amount', 'code', 'paid', 'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function admittedStudent()
    {
        return $this->belongsTo(AdmittedStudent::class, 'student_id', 'std_id');
    }

    public function fee()
    {
        return $this->belongsTo(FunctionalFees::class,'function_fee', 'id');
    }

    public function pending()
    {
        return $this->belongsTo(PendingPaymentInvoice::class, 'code', 'code');
    }

}
