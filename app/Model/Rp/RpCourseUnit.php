<?php

namespace App\Model\Rp;

use Illuminate\Database\Eloquent\Model;

class RpCourseUnit extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "program_course_units";

    public function semester()
    {
        return $this->belongsTo(RpSemester::class, 'semester_id');
    }

    public function unitclass()
    {
        return $this->belongsTo(RpCourseUnitClass::class, 'unit_class_id');
    }

    public function program()
    {
        return $this->belongsTo(RpProgram::class, 'program_id');
    }

    public function levelofstudy()
    {
        return $this->belongsTo(RpLevelOfStudy::class, 'level_of_study_id');
    }
}
