<?php

namespace App\Model\Rp;

use App\Rp\AdmittedStudent;
use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Contracts\Auditable;

class StudentExp extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $connection = "rp_mysql";

    protected $table = "student_exps";

    protected $fillable = [
      'student_id', 'academic_year', 'description', 'college_id'
    ];

    public function student()
    {
        return $this->belongsTo(AdmittedStudent::class, 'student_id', 'std_id');
    }
}
