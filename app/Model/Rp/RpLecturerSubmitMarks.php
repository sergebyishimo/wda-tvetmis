<?php

namespace App\Model\Rp;

use App\Lecturer;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class RpLecturerSubmitMarks extends Model
{
    use Uuid;
    public $incrementing = false;

    protected $fillable = [
        'college_id', 'lecturer_id', 'academic_year', 'module_id', 'semester', 'seen'
    ];

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class, 'lecturer_id');
    }
}
