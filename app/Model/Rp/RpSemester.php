<?php

namespace App\Model\Rp;

use Illuminate\Database\Eloquent\Model;

class RpSemester extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "source_semesters";

    public function courseunits()
    {
        return $this->hasMany(RpCourseUnit::class, 'semester_id', 'id');
    }
}
