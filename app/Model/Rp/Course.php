<?php

namespace App\Rp;

use App\Model\Rp\RpDepartment;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $connection = "admission_mysql";

    public function colleges()
    {
        return $this->belongsToMany(Polytechnic::class, "college_programs", "program_id", "college_id");
    }

    public function department()
    {
        return $this->belongsTo(RpDepartment::class, 'department_id');
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function getChoicesAttribute()
    {
        return $this->program_name;
    }

    public function courseunits()
    {
        return $this->hasMany(RpCourseUnit::class, 'program_id', 'id');
    }
}
