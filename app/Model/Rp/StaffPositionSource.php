<?php

namespace App\Model\Rp;

use App\Rp\Teacher;
use Illuminate\Database\Eloquent\Model;

class StaffPositionSource extends Model
{
    protected $connection = "rp_mysql";

    protected $table = "source_college_positions";

    protected $fillable = [
        'position_name', 'category', 'level', 'index'
    ];

    public function teacher()
    {
        return $this->hasMany(Teacher::class, 'position', 'id');
    }

}
