<?php

namespace App\Model\Rp;

use Illuminate\Database\Eloquent\Model;
use App\Rp\AdmittedStudent;
use App\Model\Rp\RpDepartment;

class CollegePayment extends Model
{
    protected $connection = "rp_mysql";

    public function student() {
        return $this->belongsTo(AdmittedStudent::class, 'std_id', 'std_id');
    }

    public function getDepartmentAttribute() {

        $student = AdmittedStudent::where('std_id', $this->std_id)->first();
        if($student){
            $department = RpDepartment::find($student->department_id);
        } else {
            $department = null;
        }
        return $department;
    }

    public function getCourseAttribute() {

        $student = AdmittedStudent::where('std_id', $this->std_id)->first();
        if($student){
            $course = RpProgram::find($student->course_id);
        } else {
            $course = null;
        }
        return $course;
    }
}
