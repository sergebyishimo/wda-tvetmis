<?php

namespace App\Model\Rp;

use Illuminate\Database\Eloquent\Model;

class StudentInstructions extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'college_id', 'program_id', 'student_id', 'type', 'instructions'
    ];

}
