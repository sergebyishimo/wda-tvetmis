<?php

namespace App\Rp;

use App\College;
use App\CollegeContinuingStudent;
use App\Model\Rp\FeesOverride;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use App\Model\Rp\RpSocialStudent;
use App\Model\Rp\StudentExp;
use App\Model\Rp\StudentRegistered;
use App\SponsorStudent;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AdmittedStudent extends Model  implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $fillable = [
        'college_id', 'std_id', 'first_name', 'other_names', 'email', 'phone', 'gender', 'ubudehe', 'dob',
        'want_student_loan', 'parents_phone', 'national_id_number', 'province', 'district', 'sector', 'cell',
        'village', 'bank_slip_number', 'scanned_bank_slip', 'photo', 'scan_of_diploma_or_certificate', 'your_bank',
        'your_bank_account', 'disability', 'examiner', 'index_number', 'school_attended', 'graduation_year', 'option_offered',
        'aggregates_obtained', 'payment_verified', 'scan_national_id_passport', 'year_of_study', 'country', 'sponsorship_status',
        'department_id', 'course_id', 'admission', 'student_category', 'academic_year'
    ];

    protected $connection = "rp_mysql";

    public function getinfo($col, $id = null)
    {
        if ($id == null)
            $stdid = auth()->guard('student')->user()->id;
        else
            $stdid = $id;

        $check = $this->where('std_id', $stdid);
        if ($check->count() > 0) {
            $get = $check->first();
            return $get->{$col} ?: "";
        }
        return "";
    }

    public function getStudentRegAttribute()
    {
        return $this->std_id;
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'college_id');
    }

    public function getNamesAttribute()
    {
        return ucwords(trim($this->first_name . " " . $this->other_names));
    }

    public function department()
    {
        return $this->belongsTo(RpDepartment::class,'department_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo(RpProgram::class,'course_id', 'id');
    }

    //continuing link(relationship)
    public function continuing()
    {
        return $this->hasOne(CollegeContinuingStudent::class, 'student_reg');
    }

    public function invoices()
    {
        return $this->hasMany(PaymentInvoice::class,'student_id', 'std_id');
    }

    public function social()
    {
        return $this->hasOne(SponsorStudent::class, 'student_id', 'std_id');
    }

    public function overrideFees()
    {
        return $this->hasMany(FeesOverride::class,'student_id', 'std_id');
    }

    public function studentEnabled()
    {
        return $this->hasMany(StudentExp::class,'student_id', 'std_id');
    }

    public function registered()
    {
        return $this->hasMany(StudentRegistered::class, 'student_id', 'std_id');
    }

}
