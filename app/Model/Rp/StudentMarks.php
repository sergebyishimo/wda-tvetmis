<?php

namespace App\Model\Rp;

use Illuminate\Database\Eloquent\Model;
use App\CollegeContinuingStudent;

class StudentMarks extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'registration_number', 'college_id', 'module_id', 'semester', 'obtained_marks', 'academic_year', 'retake'
    ];


    public function student() {
        return $this->belongsTo(CollegeContinuingStudent::class, 'registration_number', 'student_reg');
    }


}
