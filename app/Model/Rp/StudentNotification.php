<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class StudentNotification extends Model
{
    protected $connection = "rp_mysql";

    protected $table = "student_notifications";

    protected $fillable = [
        'student_id', 'message', 'read'
    ];

}
