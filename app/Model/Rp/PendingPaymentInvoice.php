<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class PendingPaymentInvoice extends Model
{
    protected $connection = "rp_mysql";

    protected $fillable = [
        'student_id', 'name', 'code', 'amount', 'paid', 'active', 'partial', 'academic_year'
    ];

    public function invoices()
    {
        return $this->hasMany(PaymentInvoice::class, 'code', 'code');
    }
}
