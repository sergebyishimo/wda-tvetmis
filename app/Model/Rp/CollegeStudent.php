<?php

namespace App\Rp;

use App\College;
use App\CollegeContinuingStudent;
use App\Model\Rp\CatMarks;
use App\Model\Rp\FeesOverride;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpExamMarks;
use App\Model\Rp\RpProgram;
use App\Model\Rp\RpSocialStudent;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CollegeStudent extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $connection = "rp_mysql";

    protected $fillable = [
        'student_reg', 'college_id', 'course_id', 'student_category', 'department_id',
        'first_name', 'other_names', 'email', 'phone', 'gender', 'ubudehe', 'dob', 'marital_status',
        'has_chronic_disease', 'want_student_loan', 'parents_phone', 'father_name', 'father_nation_id',
        'father_phone', 'mother_name', 'mother_nation_id', 'mother_phone', 'guardian_name',
        'guardian_nation_id', 'guardian_phone', 'national_id_number', 'province', 'district',
        'sector', 'cell', 'village', 'bank_slip_number', 'scanned_bank_slip', 'photo', 'scan_of_diploma_or_certificate',
        'your_bank', 'your_bank_account', 'disability', 'examiner', 'index_number', 'school_attended',
        'graduation_year', 'option_offered', 'aggregates_obtained', 'payment_verified', 'scan_national_id_passport',
        'year_of_study', 'country', 'sponsorship_status', 'address'
    ];

    public function getinfo($col, $id = null)
    {
        if ($id == null)
            $stdid = auth()->guard('student')->user()->id;
        else
            $stdid = $id;

        $check = $this->where('student_reg', $stdid);
        if ($check->count() > 0) {
            $get = $check->first();
            return $get->{$col} ?: "";
        }
        return "";
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'college_id');
    }

    public function getNamesAttribute()
    {
        return ucwords(trim($this->first_name . " " . $this->other_names));
    }

    public function getStdIdAttribute()
    {
        return $this->student_reg;
    }

    public function getAddressAttribute()
    {
        return ucwords($this->district . ", " . $this->sector . "");
    }

    public function department()
    {
        return $this->belongsTo(RpDepartment::class, 'department_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo(RpProgram::class, 'course_id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(PaymentInvoice::class, 'student_id', 'student_reg');
    }

    public function social()
    {
        return $this->hasOne(RpSocialStudent::class, 'student_id', 'student_reg');
    }

    public function overrideFees()
    {
        $this->hasMany(FeesOverride::class, 'student_id', 'student_reg');
    }

    public function continuing()
    {
        return $this->belongsTo(CollegeContinuingStudent::class, 'student_reg', 'student_reg');
    }

    public function catMarks()
    {
        return $this->hasMany(CatMarks::class, 'student_id', 'student_reg');
    }

    public function examMarks()
    {
        return $this->hasMany(RpExamMarks::class, 'student_id', 'student_reg');
    }

    public function getModuleMarks($module)
    {
        $marks = $this->catMarks()
            ->where('module_id', $module)
            ->where('academic_year', getCurrentAcademicYear())
            ->where('semester', getCurrentSemester())
            ->get();

        return $marks;
    }

    public function getExamModuleMarks($module)
    {
        $marks = $this->examMarks()
            ->where('module_id', $module)
            ->where('academic_year', getCurrentAcademicYear())
            ->where('semester', getCurrentSemester())
            ->get();

        return $marks;
    }

}
