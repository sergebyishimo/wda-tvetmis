<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $connection = "rp_mysql";

    protected $table = "college_payments";

    protected $fillable = [
        'college_id', 'std_id', 'year', 'paid_amount', 'bank_slipno', 'op_date', 'bank_account', 'bank',
        'operator', 'pay_mode', 'pay_status', 'trans_id', 'admission', 'invoice_code', 'academic_year'
    ];

    public function invoice()
    {
        return $this->belongsTo(PendingPaymentInvoice::class, 'invoice_code', 'code');
    }

}
