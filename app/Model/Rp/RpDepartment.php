<?php

namespace App\Model\Rp;

use App\Rp\Polytechnic;
use Illuminate\Database\Eloquent\Model;

class RpDepartment extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "departments";

    public function programs()
    {
        return $this->hasManyThrough(RpProgram::class, CollegeProgram::class, 'program_id', 'id');
    }

    public function college()
    {
        return $this->belongsToMany(Polytechnic::class, 'college_programs', "department_id", 'college_id');
    }

    public function getDepartmentAttribute()
    {
        return $this->department_name;
    }
}
