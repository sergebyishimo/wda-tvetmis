<?php

namespace App\Model\Rp;

use Illuminate\Database\Eloquent\Model;

class RpLevelOfStudy extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "source_level_of_study";
}
