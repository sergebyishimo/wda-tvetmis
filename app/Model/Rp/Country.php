<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $connection = "admission_mysql";

    protected $fillable = [
        'country_name', 'flag', 'enabled'
    ];

    public function applicant()
    {
        return $this->hasOne(AdmittedStudent::class);
    }

    public function students()
    {
        return $this->hasOne(CollegeStudent::class);
    }
}
