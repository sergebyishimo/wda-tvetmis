<?php

namespace App\Rp;

use App\College;
use Illuminate\Database\Eloquent\Model;

class PaymentCategory extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "payment_categories";

    protected $fillable = [
        'college_id', 'name', 'active'
    ];

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'id');
    }

    public function paymentDetails()
    {
        return $this->hasMany(CollegePaymentDetail::class, 'college_id', 'id');
    }

}
