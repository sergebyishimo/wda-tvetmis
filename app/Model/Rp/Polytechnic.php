<?php

namespace App\Rp;

use App\College;
use App\Model\Rp\CollegeProgram;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use Illuminate\Database\Eloquent\Model;

class   Polytechnic extends Model
{
    protected $connection = "admission_mysql";

    protected $table = "polytechnics";

    protected $fillable = [
        'polytechnic', 'short_name'
    ];

    public function user()
    {
        return $this->hasOne(College::class, 'college_id', 'id');
    }

    public function getUserAttribute()
    {
        return $this->user();
    }

    public function admins()
    {
        return $this->hasMany(Admin::class, 'college_id', 'id');
    }

    public function programs()
    {
        return $this->belongsToMany(RpProgram::class, 'college_programs', 'college_id', "program_id");
    }

    public function departments()
    {
        return $this->belongsToMany(RpDepartment::class, 'college_programs','college_id', 'department_id');
    }

    public function students()
    {
        return $this->hasMany(CollegeStudent::class,'college_id', 'id');
    }

    public function admittedStudents()
    {
        return $this->hasMany(AdmittedStudent::class,'college_id', 'id');
    }
}
