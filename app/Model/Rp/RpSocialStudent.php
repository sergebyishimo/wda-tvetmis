<?php

namespace App\Model\Rp;

use App\Rp\AdmittedStudent;
use App\Rp\CollegeStudent;
use Illuminate\Database\Eloquent\Model;

class RpSocialStudent extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "social_students";
    protected $fillable = [
        'student_id', 'sponsor', 'percentage'
    ];

    public function student()
    {
        return $this->belongsTo(CollegeStudent::class, 'student_id', 'student_reg');
    }

    public function admission()
    {
        return $this->belongsTo(AdmittedStudent::class,'student_id', 'std_id');
    }
}
