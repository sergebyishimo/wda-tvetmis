<?php

namespace App\Rp;

use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use Illuminate\Database\Eloquent\Model;

class TransferRequest extends Model
{
    protected $connection = "rp_mysql";

    protected $table = 'transfer_requests';

    protected $fillable = [
        'student_id', 'letter', 'college_id', 'department_id',
        'course_id', 'read', 'from', 'release', 'from_college_id', 'from_department_id',
        'from_course_id', 'from_confirm', 'to_confirm'
    ];

    public function polytechnic()
    {
        return $this->belongsTo(Polytechnic::class, 'college_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function department()
    {
        return $this->belongsTo(RpDepartment::class);
    }

    public function course()
    {
        return $this->belongsTo(RpProgram::class);
    }

    public function getCollegeAttribute()
    {
        if ($this->polytechnic)
            return $this->polytechnic->polytechnic;
        else
            return "";
    }

    public function getCollegeAcronymAttribute()
    {
        if ($this->polytechnic)
            return $this->polytechnic->short_name;
        else
            return "";
    }
}
