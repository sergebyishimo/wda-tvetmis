<?php

namespace App\Rp;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SponsorshipStatus extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $connection = "rp_mysql";

    public function students()
    {
        return $this->hasMany(CollegeStudent::class, 'sponsorship_status', 'name');
    }

}
