<?php

namespace App\Model\Rp;

use App\College;
use App\Lecturer;
use App\Rp\CollegeModule;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class MarksCategorizing extends Model
{
    use Uuid;
    public $incrementing = false;


    protected $fillable = [
        'college_id', 'lecturer_id', 'program_id', 'module_id', 'title', 'max_marks', 'academic_year', 'date_taken',
        'year_of_study', 'semester'
    ];

    public function college()
    {
        return $this->belongsTo(College::class, 'college_id', 'college_id');
    }

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class, 'lecturer_id');
    }

    public function courseY()
    {
        return $this->belongsTo(CollegeModule::class,'module_id','id');
    }

    public function course()
    {
        return $this->belongsTo(RpCourseUnit::class,'module_id','id');
    }

    public function cat()
    {
        return $this->hasMany(CatMarks::class,'category_id', 'id');
    }

}
