<?php

namespace App\Model\Rp;

use App\Lecturer;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;
use OwenIt\Auditing\Contracts\Auditable;

class HeadOfDepartment extends Model implements Auditable
{
    use Uuid;
    use  \OwenIt\Auditing\Auditable;

    public $incrementing = false;

    protected $connection = "rp_mysql";

    protected $fillable = [
        'lecturer_id', 'department_id', 'academic_year', 'year_of_study'
    ];

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class, 'lecturer_id', 'id');
    }
}
