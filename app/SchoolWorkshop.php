<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class SchoolWorkshop extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_school_workshops";


    public function school()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id');
    }

    public function wsector(){
        return $this->belongsTo(Sector::class,'sector');
    }

    public function wsubsector(){
        return $this->belongsTo(SubSector::class,'sub_sector');
    }
}
