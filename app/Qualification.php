<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class Qualification extends Model
{
    use Uuid;
    protected $connection = "mysql";
    use SoftDeletes;

    protected $fillable = [
        'qualification_code', 'qualification_title', 'sector_id', 'sub_sector_id', 'rtqf_id',
        'credits', 'release_date', 'status', 'description', 'occupational_profile', 'qualification_summary',
        'job_related_information', 'entry_requirements', 'information_about_pathways', 'employability_and_life_skills',
        'qualification_arrangement', 'competency_standards', 'qualification_attachment', 'training_manual', 'trainees_manual',
        'trainers_manual'
    ];

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    public function rtqfs()
    {
        return $this->belongsToMany(Rtqf::class, "qualification_rtqfs");
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function subSector()
    {
        return $this->belongsTo(SubSector::class, 'sub_sector_id');
    }
}
