<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class ExamEntry extends Model
{

    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = [
        'school_id', 'std_reg_no', 'term', 'course_id', 'obt_marks', 'total', 'acad_year', 'given_date'
    ];

    /**
     * The table associated with the model
     *
     * @var string
     */

    protected $table = 'exam_marks';

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'std_reg_no', 'reg_no');
    }
}
