<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffIap extends Model
{
    protected $connection = "mysql";

    public $timestamps = false;

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }
}
