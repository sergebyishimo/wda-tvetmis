<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleType extends Model
{
    protected $connection = "mysql";
    use SoftDeletes;

    protected $fillable = [
        'title', 'description'
    ];

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

}
