<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class TempCard extends Model
{
    use Uuid;

    public $incrementing = false;
    protected $connection = "mysql";
    protected $fillable = [
        'school_id', 'type', 'card', 'who'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

}
