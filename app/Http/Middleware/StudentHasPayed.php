<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class StudentHasPayed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'student')
    {
        if ( Auth::guard($guard)->check() ) {
            $userID = Auth::guard($guard)->user()->id;
            $sponsored = sponsored($userID, "Registration", true);
            $outside = paidOutSide($userID);
            if (Auth::guard($guard)->user()->payments != 1 && $sponsored != 100 && !$outside)
                return abort(403);
        }else
            return redirect('student/login');
            
        return $next($request);
    }
}
