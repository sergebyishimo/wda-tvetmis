<?php

namespace App\Http\Middleware;

use App\AdmittedStudent;
use Closure;
use Auth;

class RedirectIfApplied
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'student')
    {
        if (!Auth::guard($guard)->check()) {
            return redirect('student/login');
        }else{
            if (applied(get_my_info()) == false)
                return $next($request);
            else
                return redirect(route('student.editstd', seoUrl(Auth::guard($guard)->user()->name)));
        }
    }
}
