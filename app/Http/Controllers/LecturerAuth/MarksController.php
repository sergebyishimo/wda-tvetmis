<?php

namespace App\Http\Controllers\LecturerAuth;

use App\FailedModule;
use App\Lecturer;
use App\Model\Rp\CatMarks;
use App\Model\Rp\LecturerAndCourse;
use App\Model\Rp\MarksCategorizing;
use App\Model\Rp\RpCourseUnit;
use App\Model\Rp\RpExamMarks;
use App\Model\Rp\RpLecturerSubmitMarks;
use App\Model\Rp\RpSemester;
use App\Model\Rp\StudentMarks;
use App\Rp\CollegeModule;
use App\Rp\CollegeStudent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class MarksController extends Controller
{

    /**
     * MarksController constructor.
     */

    public function __construct()
    {
        $this->middleware('lecturer');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function selectClass(Request $request)
    {
        $user = lecturer();
        $modules = $user->courses;
        $cModules = [];
        foreach ($modules as $module) {
            if (getLecturerCourse($module, 'semester') == getCurrentSemester())
                $cModules[$module->id] = getLecturerCourse($module, 'name') . " | YR: " . $module->year_of_study;
        }

        if ($request->has('group')) {
            $course = LecturerAndCourse::findOrFail($request->input('module'));
            $marksCategory = MarksCategorizing::find($request->input('group'));

            if (!$marksCategory)
                return back()->with(['status' => '0', 'message' => 'Marks Category not set ???']);

            $students = CollegeStudent::where('college_id', lecturer(null, 'college_id'))
                ->where('department_id', $course->department_id)
                ->where('course_id', $course->program_id)
                ->where('year_of_study', $course->year_of_study)
                ->orderBy('first_name', 'ASC')
                ->orderBy('other_names', 'ASC')
                ->get(['student_reg', 'first_name', 'other_names']);
            return view('lecturer.marks.select_class', compact('cModules', 'students', 'marksCategory', 'course'));
        }
        return view('lecturer.marks.select_class', compact('cModules'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */

    public function selectClassForExam(Request $request)
    {
        $user = lecturer();
        $modules = $user->courses;
        $cModules = [];
        foreach ($modules as $module) {
            if (getLecturerCourse($module, 'semester') == getCurrentSemester())
                $cModules[$module->id] = getLecturerCourse($module, 'name') . " | YR: " . $module->year_of_study;
        }

        if ($request->has('module')) {
            $course = LecturerAndCourse::findOrFail($request->input('module'));

            $students = CollegeStudent::where('college_id', lecturer(null, 'college_id'))
                ->where('department_id', $course->department_id)
                ->where('course_id', $course->program_id)
                ->where('year_of_study', $course->year_of_study)
                ->orderBy('first_name', 'ASC')
                ->orderBy('other_names', 'ASC')
                ->get(['student_reg', 'first_name', 'other_names']);

            return view('lecturer.marks.select_class_exam', compact('cModules', 'students', 'marksCategory', 'course'));
        }
        return view('lecturer.marks.select_class_exam', compact('cModules'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function viewClass(Request $request)
    {
        $user = lecturer();
        $modules = $user->courses;
        $cModules = [];

        foreach ($modules as $module) {
            if (getLecturerCourse($module, 'semester') == getCurrentSemester())
                $cModules[$module->id] = getLecturerCourse($module, 'name') . " | YR: " . $module->year_of_study;
        }

        if ($request->has('module')) {
            $course = $user->courses()->where('id', $request->input('module'))->first();

            if (!$course)
                return back()->with(['status' => '0', 'message' => "Incorrect Module !!"]);

            $semester = getLecturerCourse($course, 'semester');

            $marksCategories = MarksCategorizing::where('academic_year', $course->academic_year)
                ->orderBy('title', 'ASC')
                ->get(['id', 'title', 'max_marks']);

            $students = CollegeStudent::where('college_id', lecturer(null, 'college_id'))
                ->where('department_id', $course->department_id)
                ->where('course_id', $course->program_id)
                ->where('year_of_study', $course->year_of_study)
                ->orderBy('first_name', 'ASC')
                ->orderBy('other_names', 'ASC')
                ->get(['student_reg', 'first_name', 'other_names']);
        }

        return view('lecturer.marks.view_class', compact('cModules', 'marksCategories', 'students', 'course'));
    }

    /**
     * @param $module
     * @return \Illuminate\Http\JsonResponse
     */

    public function ajaxGetGroup($module)
    {
        $course = LecturerAndCourse::find($module);
        if (!$course)
            return response()->json([]);
        $semester = getLecturerCourse($course, 'semester');

        $marksCategories = MarksCategorizing::where('academic_year', $course->academic_year)
            ->orderBy('title', 'ASC')
            ->get(['id', 'title', 'max_marks']);

        return response()->json($marksCategories->toArray());

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function saveMarks(Request $request)
    {
        $category = MarksCategorizing::find($request->input('category_id'));
        if (!$category)
            return back()->with(['status' => 0, 'message' => 'Oops something went wrong !!']);
        $students = $request->input('marks');
        $skiped = [];
        $max = (double)$category->max_marks;
        $saved = false;

        foreach ($students as $id => $m) {
            if ((double)$m > $max) {
                $skiped[] = $id . " has {$m} which is greater than maximum ( " . $max . " )";
                continue;
            }

            $marks = CatMarks::where('college_id', $category->college_id)
                ->where('student_id', $id)
                ->where('category_id', $category->id)
                ->where('academic_year', $category->academic_year)
                ->where('module_id', $category->module_id)
                ->where('semester', $category->semester)->first();

            if (!$marks)
                $marks = new CatMarks();

            $marks->college_id = $category->college_id;
            $marks->module_id = $category->module_id;
            $marks->student_id = $id;
            $marks->semester = $category->semester;
            $marks->category_id = $category->id;
            $marks->marks = (double)$m;
            $marks->year_of_study = $category->year_of_study;
            $marks->academic_year = $category->academic_year;

            if ($marks->save())
                $saved = true;
        }

        if ($saved && count($skiped) <= 0)
            return back()->with(['status' => 1, 'message' => 'Marks uploaded successfully']);
        elseif (count($skiped) > 0)
            return back()->with(['status' => 3, 'message' => $skiped]);
        else
            return back()->with(['status' => 0, 'message' => 'Fail to upload']);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function viewExamClass(Request $request)
    {
        $user = lecturer();
        $modules = $user->courses;
        $cModules = [];

        foreach ($modules as $module) {
            if (getLecturerCourse($module, 'semester') == getCurrentSemester())
                $cModules[$module->id] = getLecturerCourse($module, 'name') . " | YR: " . $module->year_of_study;
        }
        if ($request->has('module')) {
            $college = lecturer(null, 'college_id');

            $course = LecturerAndCourse::find($request->input('module'));

            if (!$course)
                return back()->with(['status' => 1, 'message' => 'Oops something went wrong !!']);

            $totalCat = MarksCategorizing::where('marks_categorizings.academic_year', getCurrentAcademicYear())
                ->has('cat')
                ->sum('max_marks');
            if (!isset($totalCat))
                $totalCat = 0;

            $students = CollegeStudent::where('college_id', lecturer(null, 'college_id'))
                ->where('department_id', $course->department_id)
                ->where('course_id', $course->program_id)
                ->where('year_of_study', $course->year_of_study)
                ->orderBy('first_name', 'ASC')
                ->orderBy('other_names', 'ASC')
                ->get(['student_reg', 'first_name', 'other_names']);
        }

        return view('lecturer.marks.view_class_exam', compact('cModules', 'totalCat', 'students', 'course'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function saveExamMarks(Request $request)
    {
        $course = LecturerAndCourse::find($request->input('course'));

        if (!$course)
            return back()->with(['status' => 0, 'message' => 'Oops something went wrong !!']);

        $students = $request->input('marks');

        $students = array_filter($students, function ($std) {
            return !is_null($std);
        });

        $skiped = [];
        $max = (double)50;
        $saved = false;

        foreach ($students as $id => $m) {
            if ((double)$m > $max) {
                $skiped[] = $id . " has {$m} which is greater than maximum ( " . $max . " )";
                continue;
            }

            $marks = RpExamMarks::where('college_id', $course->college_id)
                ->where('student_id', $id)
                ->where('academic_year', getCurrentAcademicYear())
                ->where('module_id', $course->course_id)
                ->where('semester', getCurrentSemester())->first();

            if (!$marks)
                $marks = new RpExamMarks();

            $marks->college_id = $course->college_id;
            $marks->module_id = $course->course_id;
            $marks->student_id = $id;
            $marks->semester = getCurrentSemester();
            $marks->marks = $m;
            $marks->academic_year = getCurrentAcademicYear();

            if ($marks->save())
                $saved = true;
        }

        if ($saved && count($skiped) <= 0)
            return back()->with(['status' => 1, 'message' => 'Marks uploaded successfully']);
        elseif (count($skiped) > 0)
            return back()->with(['status' => 3, 'message' => $skiped]);
        else
            return back()->with(['status' => 0, 'message' => 'Oops something went wrong !!']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */

    public function saveUploadedMarks(Request $request)
    {
        $category = MarksCategorizing::find($request->input('category_id'));

        if (!$category)
            return back()->with(['status' => 0, 'message' => 'Oops something went wrong !!']);

        if (!$request->hasFile('marks'))
            return back()->with(['status' => 1, 'message' => 'File not find !!!']);

        $file = $request->file('marks');

        if ($file->getClientOriginalExtension() != 'xlsx' && $file->getClientOriginalExtension() != 'xls')
            return back()->with(['status' => 1, 'message' => 'Please upload excel file (xlsx or xls) ...']);

        $fileTitle = $file->getClientOriginalNAme() . rand(1, 1000000);
        $filename = $file->storeAs('File/College/Marks/' . date('YF') . "/", $fileTitle . '.xlsx');

        $inputFileName = storage_path('app/') . $filename;
        $objPHPExcel = IOFactory::load($inputFileName);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $skippedRows = [];
        $newSchools = 0;

        $students = [];

        //  Loop through each row of the worksheet in turn
        for ($row = 3; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $student = $rowData[0][0];
            $marks = $rowData[0][2];
            if ($student == null || $marks == null) {
                $skippedRows[] = $row;
                continue;
            }
            $students[$student] = $marks;
        }

        $skiped = [];
        $max = (double)$category->max_marks;
        $saved = false;

        foreach ($students as $id => $m) {
            if ((double)$m > $max) {
                $skiped[] = $id . " has {$m} which is greater than maximum ( " . $max . " )";
                continue;
            }

            $marks = CatMarks::where('college_id', $category->college_id)
                ->where('student_id', $id)
                ->where('category_id', $category->id)
                ->where('academic_year', $category->academic_year)
                ->where('module_id', $category->module_id)
                ->where('semester', $category->semester)->first();

            if (!$marks)
                $marks = new CatMarks();

            $marks->college_id = $category->college_id;
            $marks->module_id = $category->module_id;
            $marks->student_id = $id;
            $marks->semester = $category->semester;
            $marks->category_id = $category->id;
            $marks->marks = $m;
            $marks->year_of_study = $category->year_of_study;
            $marks->academic_year = $category->academic_year;

            if ($marks->save())
                $saved = true;
        }

        if ($saved && count($skiped) <= 0)
            return back()->with(['status' => 1, 'message' => 'Marks uploaded successfully']);
        elseif (count($skiped) > 0)
            return back()->with(['status' => 3, 'message' => $skiped]);
        else
            return back()->with(['status' => 0, 'message' => 'Fail to upload']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */

    public function saveUploadedExamMarks(Request $request)
    {
        $course = LecturerAndCourse::find($request->input('course'));

        if (!$course)
            return back()->with(['status' => 0, 'message' => 'Oops something went wrong !!']);

        if (!$request->hasFile('marks'))
            return back()->with(['status' => 1, 'message' => 'File not find !!!']);

        $file = $request->file('marks');

        if ($file->getClientOriginalExtension() != 'xlsx' && $file->getClientOriginalExtension() != 'xls')
            return back()->with(['status' => 1, 'message' => 'Please upload excel file (xlsx or xls) ...']);

        $fileTitle = $file->getClientOriginalNAme() . rand(1, 1000000);
        $filename = $file->storeAs('File/College/Marks/' . date('YF') . "/", $fileTitle . '.xlsx');

        $inputFileName = storage_path('app/') . $filename;
        $objPHPExcel = IOFactory::load($inputFileName);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $skippedRows = [];
        $newSchools = 0;

        $students = [];

        //  Loop through each row of the worksheet in turn
        for ($row = 3; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $student = $rowData[0][0];
            $marks = $rowData[0][2];
            if ($student == null || $marks == null) {
                $skippedRows[] = $row;
                continue;
            }
            $students[$student] = $marks;
        }

        $skiped = [];
        $max = (double)40;
        $saved = false;

        foreach ($students as $id => $m) {
            if ((double)$m > $max) {
                $skiped[] = $id . " has {$m} which is greater than maximum ( " . $max . " )";
                continue;
            }

            $marks = RpExamMarks::where('college_id', $course->college_id)
                ->where('student_id', $id)
                ->where('academic_year', getCurrentAcademicYear())
                ->where('module_id', $course->course_id)
                ->where('semester', getCurrentSemester())->first();

            if (!$marks)
                $marks = new RpExamMarks();

            $marks->college_id = $course->college_id;
            $marks->module_id = $course->course_id;
            $marks->student_id = $id;
            $marks->semester = getCurrentSemester();
            $marks->marks = $m;
            $marks->academic_year = getCurrentAcademicYear();

            if ($marks->save())
                $saved = true;
        }

        if ($saved && count($skiped) <= 0)
            return back()->with(['status' => 1, 'message' => 'Marks uploaded successfully']);
        elseif (count($skiped) > 0)
            return back()->with(['status' => 3, 'message' => $skiped]);
        else
            return back()->with(['status' => 0, 'message' => 'Fail to upload']);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function submitMarks(Request $request)
    {
        $user_id = lecturer(null, 'id');
        $course = LecturerAndCourse::find($request->input('course'));

        if (!$course)
            return back()->with(['status' => 0, 'message' => 'Oops something went wrong !!']);

        if (!$request->has('students'))
            return back()->with(['status' => 0, 'message' => 'Please first provide marks for cat or exam']);

        $moduleCode = getLecturerCourse($course, 'code');

        $students = is_array($request->get('students')) ? $request->get('students') : [];

        $academic_year = getCurrentAcademicYear();
        $semester = getCurrentSemester();
        $college = lecturer(null, 'college_id');

        if ($course->year_of_study > 1)
            $module_model = CollegeModule::where('college_id', $college)
                ->where('module_code', $moduleCode)
                ->where('semester', $semester)
                ->where('academic_year', $academic_year)
                ->where('option_id', $course->program_id)
                ->where('year_of_study', $course->year_of_study)
                ->first();
        else {
            $semester_ = RpSemester::where('semester', $semester)->first();
            $semester_ = $semester_ ? $semester_->id : null;

            $module_model = RpCourseUnit::where('course_unit_code', $moduleCode)
                ->where('semester_id', $semester_)
                ->where('program_id', $course->program_id)
                ->where('level_of_study_id', $course->year_of_study)
                ->first();
        }

        $checkmarks_ = StudentMarks::where('academic_year', $academic_year)
            ->where('semester', $semester)
            ->where('module_id', $course->course_id);

        if ($module_model) {
            foreach ($students as $student => $marks) {
                $data = [
                    'registration_number' => $student,
                    'college_id' => $college,
                    'module_id' => $module_model->id,
                    'semester' => $semester,
                    'obtained_marks' => $marks,
                    'academic_year' => $academic_year,
                ];

                // check for update
                $check_marks = $checkmarks_->where('registration_number', $student)->first();
                $save = null;
                if ($check_marks)
                    $save = StudentMarks::where('id', $check_marks->id)->update($data);
                else {
                    $save = StudentMarks::create($data);
                    // check if module failed
                    if ($marks < 50)
                        FailedModule::create([
                            'college_id' => $college,
                            'student_id' => $student,
                            'academic_year' => $academic_year,
                            'module_id' => $course->course_id,
                            'semester' => $semester
                        ]);
                }
            }
            if ($save) {
                RpLecturerSubmitMarks::updateOrCreate([
                    'college_id' => $college,
                    'lecturer_id' => $user_id,
                    'academic_year' => $academic_year,
                    'module_id' => $course->course_id,
                    'semester' => $semester
                ], [
                    'college_id' => $college,
                    'lecturer_id' => $user_id,
                    'academic_year' => $academic_year,
                    'module_id' => $course->course_id,
                    'semester' => $semester
                ]);

                return back()->with(['status' => 1, 'message' => 'Marks submitted successfully !!']);
            }

        } else
            return back()->with(['status' => 0, 'message' => 'Module Code Not Matching !!!']);


        return back()->with(['status' => 0, 'message' => 'Oops, something went wrong ...']);

    }

}
