<?php

namespace App\Http\Controllers\LecturerAuth;

use App\Model\Rp\LecturerAndCourse;
use App\Model\Rp\MarksCategorizing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class GroupingController extends Controller
{

    public function home()
    {
        $users[] = Auth::user();
        $users[] = Auth::guard()->user();
        $users[] = Auth::guard('lecturer')->user();

        if (!auth()->guard('lecturer')->check())
            return abort(401);

        $modules = auth()->guard('lecturer')->user()->courses;

        return view('lecturer.home', compact('modules'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
        /*if (!auth()->guard('lecturer')->check())
            return abort(401);

        $modules = auth()->guard('lecturer')->user()->courses;
        $cModules = [];
        foreach ($modules as $module) {
            if (getLecturerCourse($module, 'semester') == getCurrentSemester())
                $cModules[$module->id] = getLecturerCourse($module, 'name') . " | YR: " . $module->year_of_study;
        }
        return view('lecturer.marks.categorizing', compact('cModules')); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->guard('lecturer')->check())
            return abort(401);

        $user = auth()->guard('lecturer')->user();

        $validation = Validator::make($request->all(), [
            'module' => 'required',
            'title' => 'required|min:3',
            'max_marks' => 'required|integer',
            'date_taken' => 'required'
        ]);

        if ($validation->fails())
            return back()->withInput()->with(['status' => '0', 'message' => $validation->errors()->all()]);

        $course = LecturerAndCourse::find($request->input('module'));

        if (!$course)
            return back()->withInput()->with(['status' => '0', 'message' => "Something is wrong !!"]);

        $data = [
            'college_id' => $user->college_id,
            'lecturer_id' => $user->id,
            'program_id' => $course->program_id,
            'module_id' => $course->course_id,
            'title' => $request->input('title'),
            'max_marks' => $request->input('max_marks'),
            'academic_year' => getCurrentAcademicYear(),
            'date_taken' => $request->input('date_taken'),
            'year_of_study' => $course->year_of_study,
            'semester' => getLecturerCourse($course, 'semester')
        ];

        $save = MarksCategorizing::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => "Saved Successfully"]);
        else
            return back()->withInput()->with(['status' => '0', 'message' => "Something is wrong !!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxMyCategories($module)
    {
        if (auth()->guard('lecturer')->check()) {
            $user = auth()->guard('lecturer')->user();
            $request = request();

            $course = LecturerAndCourse::where('id', $module)
                ->where('lecturer_id', $user->id)
                ->first();

            if ($course)
                $course = $course->course_id;
            else
                $course = null;

            $categories = $user->categorized()
                ->where('module_id', $course)
                ->where('academic_year', getCurrentAcademicYear())
                ->where('semester', getCurrentSemester())
                ->get();

            return view('lecturer.partials.my_course', compact('categories'));
        }

        return "No data";
    }
}
