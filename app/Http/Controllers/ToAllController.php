<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ToAllController extends Controller
{
    public function getlocation($where, $value)
    {
        switch ($where) {
            case "p":
                $value = getProvince();
                return json_encode($value);
                break;
            case "d":
                $b = html_entity_decode($value);
                $value = getDistrict($b);
                return json_encode($value);
                break;
            case "s":
                $b = html_entity_decode($value);
                $value = getSector($b);
                return json_encode($value);
                break;
            case "c":
                $b = html_entity_decode($value);
                $value = getCell($b);
                return json_encode($value);
                break;
            case "v":
                $b = html_entity_decode($value);
                $value = getVillage($b);
                return json_encode($value);
                break;
        }
    }
    public function getFilterlocation($where, $value)
    {
        switch ($where) {
            case "p":
                $value = getProvinceSchool();
                return json_encode($value);
                break;
            case "d":
                $b = html_entity_decode($value);
                $value = getDistrictSchool($b);
                return json_encode($value);
                break;
            case "s":
                $b = html_entity_decode($value);
                $value = getSectorSchool($b);
                return json_encode($value);
                break;
            case "c":
                $b = html_entity_decode($value);
                $value = getSchoolType($b);
                return json_encode($value);
                break;
        }
    }
}
