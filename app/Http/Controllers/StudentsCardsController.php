<?php

namespace App\Http\Controllers;

use App\Rp\CollegeStudent;
use Illuminate\Http\Request;

class StudentsCardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->year) {
            if ($request->year == 4)
                $students = CollegeStudent::where('college_id', college('college_id'))->whereIn('year_of_study', [1,2,3]);
            else
                $students = CollegeStudent::where('college_id', college('college_id'))->where('year_of_study', $request->year);

            if ($request->option_id)
                $students = $students->where('course_id', $request->option_id);

            $students = $students->paginate(50);
            $students ->withPath("?year=$request->year&program_id=$request->program_id&department_id=$request->department_id&option_id=$request->option_id");
            return view('college.students.cards', compact('students'));
        }
        return view('college.students.cards');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
