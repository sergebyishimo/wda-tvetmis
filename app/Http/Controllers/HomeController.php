<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use foo\bar;
use App\School;
use Illuminate\Support\Facades\DB;
use JavaScript;
use App\Student;
use App\Rp\Province;
use App\Rp\Applicant;
use App\Http\Requests;
use App\Qualification;
use App\Rp\Department;
use App\Rp\Polytechnic;
use App\AttachmentUpload;
use App\Model\Rp\RpProgram;
use App\Rp\AdmittedStudent;
use App\Model\Accr\District;
use Illuminate\Http\Request;
use App\Model\Accr\SchoolType;
use App\Model\Rp\RpDepartment;
use App\Mail\SendEmailToSchool;
use App\Model\Accr\SchoolRating;
use App\Model\Rp\CollegeProgram;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\TrainingTradesSource;
use App\Model\Accr\SourceSchoolOwnership;
use App\Model\Accr\CurriculumQualification;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function getlocation($where, $value)
    {
        switch ($where) {
            case "d":
                $b = html_entity_decode($value);
                $value = getDistrict($b);
                return json_encode($value);
                break;
            case "s":
                $b = html_entity_decode($value);
                $value = getSector($b);
                return json_encode($value);
                break;
            case "c":
                $b = html_entity_decode($value);
                $value = getCell($b);
                return json_encode($value);
                break;
            case "v":
                $b = html_entity_decode($value);
                $value = getVillage($b);
                return json_encode($value);
                break;
        }
    }

    public function ajaxGetSchools($where, $value, $col = null)
    {
        switch ($where) {
            case "d":
                $b = html_entity_decode(trim($value));
                $get = Polytechnic::find($b);
                $response = [];
                if ($get)
                    $response = $get->departments()
                        ->distinct('department_name')
                        ->get(['departments.id', 'department_name'])->toArray();
                return json_encode($response);
                break;
            case "c":
                $b = html_entity_decode(trim($value));
                $c = html_entity_decode(trim($col));
                $polytechnic = Polytechnic::find($c);
                $response = [];
                if ($polytechnic) {
                    $programs = $polytechnic->programs()->distinct('program_name');
                    foreach ($programs->get() as $program) {
                        if ($program->department_id == $b)
                            $response[] = [
                                'id' => $program->id,
                                'program_name' => $program->program_name
                            ];
                    }
                }
//                $get = RpDepartment::find($b);
//                if ($get)
//                    $response = $get->programs()->get(['programs.id', 'program_name'])->toArray();
                return json_encode($response);
                break;
            case "m":
                $b = html_entity_decode(trim($value));
                $get = RpProgram::find($b);
                $response = [];
                if ($get)
                    $response = $get->courseunits()
                        ->get()->toArray();
                return json_encode($response);
                break;
        }
    }

    public function indexNumber(Request $request)
    {
        $select = \DB::connection('admission')->table('ex_2017')->where("Index", $request->input('index'));
        if ($select->count() > 0) {
            return json_encode($select->get(['school_name', 'TOT'])->first());
        }
        return json_encode([]);
    }

    public function view(Request $request)
    {
        $this->validate($request, [
            'reg_no' => 'required'
        ]);
        $exist = Applicant::where('stdid', strtolower($request->reg_no))->first();
        $student = AdmittedStudent::where('std_id', strtolower($request->reg_no))->first();

        $feedback = ["Sorry you're not selected to any RP College.", "0"];
        if (!$exist)
            $feedback = [$request->reg_no . " doesn't exists in our system !!", "2"];

        if ($student) {
            $msg = $student->college ? "You're admitted to " . $student->college->name . "<br> Login using button bellow to continue registration and view more info." : '';
            $feedback = [$msg, "1"];
        }
        $reg = $request->reg_no;
        return view('welcome', compact("feedback", "reg"));
    }

    public function getDepartment($loc, $id = null, $pol)
    {
        $data = [];
        $pol = $pol;
        switch ($loc) {
            case 'd':
                $courses = Department::where('polytechnic', $pol)
                    ->get(['id', 'department'])
                    ->toJson();
                $data = $courses;
                break;
            case  'c':
                $courses = Course::where('polytechnic', $pol)
                    ->where('department', $id)
                    ->get(['id', 'choices'])
                    ->toJson();
                $data = $courses;
                break;
        }

        return json_encode($data);
    }

    public function getSubSectors($sector_id)
    {
        $sub_sectors = TrainingTradesSource::where('sector_id', $sector_id)->get();
        return $sub_sectors;
    }

    public function getCurrQualifications($sub_sector_id)
    {
        $qualifications = CurriculumQualification::select('id', 'qualification_title')->where('sub_sector_id', $sub_sector_id)->get();
        return $qualifications;
    }

    public function getPrograms($college_id, $program_id = null)
    {

        $programs = CollegeProgram::where('college_id', $college_id)->where('program_id', $program_id)->get();

        $pros = [];
        foreach ($programs as $program) {
            $pro = RpProgram::find($program->program_id);
            $pros[] = $pro;
        }

        return $pros;
    }

    public function schools(Request $request, $slugname = null)
    {
        if ($slugname != null) {
            $ownerships = SourceSchoolOwnership::all();
            $school = SchoolInformation::where('school_acronym', $slugname)->first();
            if ($school) {
                JavaScript::put([
                    'name' => $school->school_name,
                    'latitude' => $school->latitude,
                    'longitude' => $school->longitude,
                ]);
                return View::make('schoolinfo', compact('school', 'ownerships'));
            } else {
                $provinces = getProvince();
                $districts = getDistrict();
                $schooltypes = SchoolType::all();
                $schoolratings = SchoolRating::all();
                $qualifications = Qualification::all();

                return view('schools', compact("provinces", "districts", "schooltypes", "schoolratings", "qualifications"));
            }


        } else {
            $provinces = getProvince();
            $districts = getDistrict();
            $schooltypes = SchoolType::all();
            $schoolratings = SchoolRating::all();
            $qualifications = Qualification::all();

            return view('schools', compact("provinces", "districts", "schooltypes", "schoolratings", "qualifications"));
        }
    }

    public function dashboard()
    {

        $schools_nber = SchoolInformation::where('school_activity', 'Active')->count();
        $have_internet = SchoolInformation::where('school_activity', 'Active')->where('has_internet', 2)->count();
        $have_electricity = SchoolInformation::where('school_activity', 'Active')->where('has_electricity', 2)->count();
        $have_water = SchoolInformation::where('school_activity', 'Active')->where('has_water', 2)->count();
        $private = SchoolInformation::where('school_activity', 'Active')->where('school_status', 'Private')->count();

        $north_schools = SchoolInformation::where('school_activity', 'Active')->where('province', 'Northern Province')->count();
        $south_schools = SchoolInformation::where('school_activity', 'Active')->where('province', 'Southern Province')->count();
        $eastern_schools = SchoolInformation::where('school_activity', 'Active')->where('province', 'Eastern Province')->count();
        $western_schools = SchoolInformation::where('school_activity', 'Active')->where('province', 'Western Province')->count();
        $kigali_schools = SchoolInformation::where('school_activity', 'Active')->where('province', 'City of Kigali')->count();

        $schools_per_province_arr = [$north_schools, $south_schools, $eastern_schools, $western_schools, $kigali_schools];

        $north_students = Student::where('province', 'Northern Province')->count();
        $south_students = Student::where('province', 'Southern Province')->count();
        $eastern_students = Student::where('province', 'Eastern Province')->count();
        $western_students = Student::where('province', 'Western Province')->count();
        $kigali_students = Student::where('province', 'City of Kigali')->orWhere('province', 'Kigali')->count();

        $students_per_province = [$north_students, $south_students, $eastern_students, $western_students, $kigali_students];

        return view('dashboard', compact('schools_nber', 'have_internet', 'have_electricity', 'have_water', 'private', 'schools_per_province_arr', 'students_per_province'));

    }

    public function schoolsmap(Request $request)
    {
        if ($request->isMethod("POST")) {
            dd($request);
        }
        JavaScript::put([
            'mName' => $request->input(''),
            'mLatitude' => $request->input('latitude'),
            'mLongitude' => $request->input('longitude'),
        ]);
        return View::make('schoolsmap');
    }

    public function findaschool(Request $request)
    {
        if (isset($request->advanced_search)) {
//            dd(true);

            $selectedProvinces = $request->province;
            $selectedDistricts = $request->district;
            $selectedQualifications = $request->qualification;
            $selectedRating = $request->schoolrating;
            $selectedType = $request->schooltype;

            $schoolInQ = [];

            //pulling schools in province

            if (count($selectedProvinces)) {
                $pschools = SchoolInformation::whereIn('province', $selectedProvinces)->get();
                foreach ($pschools as $p) {
                    array_push($schoolInQ, $p);
                }
            }

            //pulling schools in districts
            if (count($selectedDistricts)) {
                $dschools = SchoolInformation::WhereIn('district', $selectedDistricts)->get();
                foreach ($dschools as $d) {
                    array_push($schoolInQ, $d);
                }
            }

            // school ratings
            if (count($selectedRating)) {
                $schoolRatings = SchoolInformation::whereIn('school_rating', $selectedRating)->get();
                foreach ($schoolRatings as $schoolRating) {
                    array_push($schoolInQ, $schoolRating);
                }
            }

            if (count($selectedType)) {
                $schoolTypes = SchoolInformation::whereIn('school_type', $selectedType)->get();
                foreach ($schoolTypes as $schoolType) {
                    array_push($schoolInQ, $schoolType);
                }
            }

            //depts related
            if (count($selectedQualifications)) {
                $departments = Department::all();
                foreach ($departments as $dept) {
                    if ($dept->qualification()->whereIn('id', $selectedQualifications)->count()) {
                        array_push($schoolInQ, $dept->school);
                    }
                }
            }
            //TODO: checks  Collection::merge function if can helps!!
            $schools = Collection::make($schoolInQ)->unique();
        } else {
            $_f_all = $request->input("show_all");
            $_f_one = $request->input("find_one");
            if (isset($_f_all)) {
                $schools = SchoolInformation::paginate(25);
            } else if (isset($_f_one)) {
                $_query = '%' . $request->query_field . '%';
                $schools = SchoolInformation::where('school_name', 'like', $_query)
                    ->orWhere('school_code', $request->query_field)
                    ->orWhere('province', $request->query_field)
                    ->orWhere('district', $request->query_field)
                    ->orWhere('sector', $request->query_field)
                    ->orWhere('manager_name', $request->query_field)
                    ->get();
            }
        }
        return view('searchResult', compact('schools'));
    }


    public function sendmessage(Request $request)
    {
        $this->validate($request, [
            'school_email' => 'required',
            'user_email' => 'required',
            'name' => 'required',
            'message' => 'required',

        ]);

        Mail::to($request->school_email)->send(new SendEmailToSchool($request->message, $request->name, $request->user_email));
        return back()->with(['status' => '1', 'message' => 'Message Sent Successful']);
    }

    public function requestAccount(Request $request)
    {
        if ($request->school) {

        }
        $schools = SchoolInformation::all();
        return view('request_account', compact('schools'));
    }

    public function contactUs(Request $request)
    {
        if ($request->message) {
            $message = $request->message." Phone number: ".$request->phone;
            Mail::raw($message, function ($message) use ($request) {
//                global $request;
                $message->to('cmugarura@gmail.com')
                    ->cc($request->email)
                    ->subject('TVET MIS - Contact Us Form');
                $message->from($request->email, $request->who);
            });

            return redirect(route('contact.us'));
        }
        $categories = [
            'School Manager' => 'School Manager',
            'Teacher' => 'Teacher',
            'Student' => 'Student',
            'Parent' => 'Parent',
            'Other' => 'Other'
        ];


        return view('contact', compact('categories'));
    }

    public function attachments()
    {
        $attachments = AttachmentUpload::orderBy('created_at', 'desc')->get();
        return view('attachments', compact('attachments'));
    }

    public function results(Request $request) {

        if (isset($request->index_number)) {
            $results = DB::table('senior_six_candidate_results')->where("index",$request->index_number)->first();
            return view('results', compact('results'));
        }

        return view('results');
    }

    public function resultsAll(Request $request) {

        if (isset($request->index_number)) {
            $results = DB::table('senior_six_results')->where("index_number",$request->index_number)->first();
            return view('resultsAll', compact('results'));
        }

        return view('resultsAll');
    }
}
