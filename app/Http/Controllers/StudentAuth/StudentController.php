<?php

namespace App\Http\Controllers\StudentAuth;

use App\Rp\AdmittedStudent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notification;

class StudentController extends Controller
{
    function __construct()
    {
        $this->middleware('student');
    }

    public function admissionLetter()
    {
        $stdid = auth()->guard('student')->user()->id;
        $pathX = '/storage/admissionLetter/' . date('Y') . '/';
        $path = public_path() . $pathX;
        \File::isDirectory($path) or \File::makeDirectory($path, 0777, true, true);

//        $pdf = \App::make('dompdf.wrapper');
//        $pdf->loadView('student.admission.letter');
        $functionalfees = \App\Model\Rp\FunctionalFees::all();
        $fees = $functionalfees->whereIn('paid_when', ['first_year', 'each_year']);

        $registration = \App\FeeCategory::where('category_name', "like", "%" . ucwords("registration") . "%")->first();
        $schoolRFees = \App\FeeCategory::where('category_name', "like", "%" . ucwords("school fees") . "%")->first();
        $rep = "";
        $totalRegFees = "";
        $schoolFees = "";

        if ($registration->fees) {
            $totalRegFees = $registration->fees->pluck('govt_sponsored_amount')->sum() . " Rwf";
            $x = 0;
            foreach ($registration->fees as $fee) {
                if ($x > 0)
                    $rep .= "; ";
                $rep .= $fee->fee_name . " " . number_format($fee->govt_sponsored_amount) . " Rwf";
                $x++;
            }
        }

        if ($schoolRFees->fees)
            $schoolFees = number_format($schoolRFees->fees->pluck('govt_sponsored_amount')->sum()) . " Rwf";

        $pdf = \PDF::loadView('student.admission.letters', compact('rep', 'totalRegFees', 'schoolFees'));

//        return view('student.admission.letters', compact('rep', 'totalRegFees', 'schoolFees'));
        return $pdf->stream($stdid . '.pdf');

    }


    public function print(Request $request)
    {
        $stdid = auth()->guard('student')->user()->id;
        $pathX = '/storage/admissionLette/' . date('Y') . '/';
        $path = public_path() . $pathX;
        \File::isDirectory($path) or \File::makeDirectory($path, 0777, true, true);

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pdf.applied');
        $file = $path . $stdid . '.pdf';
//        $pdf->save($file);
        return $pdf->download($stdid . '.pdf');
    }

    public function edit()
    {
        return view('student.auth.register');
    }

    public function home()
    {
        $user = Auth::guard('student')->user();

        $functionalfees = \App\Model\Rp\FunctionalFees::all();
        $dfs = "first_year";
        $col = "";
        $sponsor = get_my_info('sponsorship_status');
        if ($sponsor == "private")
            $col = "private_sponsored_amount";
        elseif ($sponsor == "government")
            $col = "govt_sponsored_amount";
        if (isContinuingStudent($user->id))
            $dfs = getMyYearOfStudy(true, get_my_info('year_of_study')) . "_year";

        $fees = $functionalfees->whereIn('paid_when', [$dfs, 'each_year']);

        $notifications = Notification::all();

        return view('student.home', compact('fees', 'col', 'notifications'));
    }

}
