<?php

namespace App\Http\Controllers\StudentAuth;

use App\Rp\Suspension;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuspensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suspensions = Suspension::where('student_reg', get_my_info())->get();
        return view('student.suspension.index', compact('suspensions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('letter')) {
            $validator = $request->validate([
                'letter' => 'required|max:5000'
            ]);

            Suspension::create([
                'college_id' => get_my_info('college_id', true),
                'student_reg' => get_my_info(),
                'letter' => $request->letter->store('uploads/suspensions', 'public'),
                'resuming_academic_year' => $request->academic_year,
                'resuming_semester' => $request->semester
            ]);

            return redirect()->back()->with(['status' => 1, 'message' => 'Suspenssion Submitted Successfully']);
        }
        return redirect()->back()->with(['status' => 2, 'message' => 'Suspenssion Not Submitted! Check the form!!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rp\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function show(Suspension $suspension)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rp\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function edit(Suspension $suspension)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rp\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suspension $suspension)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rp\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suspension $suspension)
    {
        //
    }
}
