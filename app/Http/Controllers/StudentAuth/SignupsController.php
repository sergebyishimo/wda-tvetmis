<?php

namespace App\Http\Controllers\StudentAuth;

use App\CollegeContinuingStudent;
use App\Http\Requests\RegisterContinuingStudentRequest;
use App\Http\Requests\RegisterStudentRequest;
use App\Http\Requests\SignupContinuingStudentRequest;
use App\Mail\RegisterMail;
use App\Mail\RegisterMailContinuing;
use App\Model\Rp\AdmissionPrivate;
use App\Model\Rp\RpProgram;
use App\Rp\AdmittedStudent;
use App\Rp\CollegeOption;
use App\Rp\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class SignupsController extends Controller
{
    function __construct()
    {
        $this->middleware('guest');
    }

    public function private()
    {
        return view('student.auth.signup.private_register');
    }

    public function postPrivate(RegisterStudentRequest $request)
    {
        $id = privateGenerateReg();
        $data = [
            'id' => $id,
            'name' => $request->input('first_name') . " " . $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'sponsorship' => 'private',
            'nation_id' => $request->input('nation_id')
        ];

        $add = Student::create($data);

        if ($add) {
            $data['admission'] = false;
            $data['std_id'] = $data['id'];
            $data['first_name'] = $request->input('first_name');
            $data['other_names'] = $request->input('last_name');
            $data['department_id'] = $request->input('department');
            $data['course_id'] = $request->input('program');
            $data['college_id'] = $request->input('college');
            $data['year_of_study'] = date('Y');
            $data['sponsorship_status'] = 'private';
            $data['national_id_number'] = $data['nation_id'];
            $adm = AdmissionPrivate::create($data);

            if ($adm)
                Mail::to($data['email'])->send(new RegisterMail($data['name'], $data['id'], null, null, [
                    'college' => getCollege($data['college_id'], 'polytechnic'),
                    'department' => getDepartment($data['department_id'], 'department_name'),
                    'program' => getCourse($data['course_id'], 'program_name')
                ]));

            return view('student.auth.signup.p_confirm');
        }


        return back()->with(['success' => 'Successfully added.']);
    }

    public function continuing()
    {
        return view('student.auth.signup.continuing_register');
    }

    public function poastContinuing(SignupContinuingStudentRequest $request)
    {

        $olR = $request->input('registration_number');
        $college = $request->input('college');
        $plp = CollegeContinuingStudent::where('registration_number', $olR)->where('college_id', $college)->first();
        if ($plp) {
            $pll = Student::where('id', $plp->student_reg)->first();
            if ($pll) {
                $plp->student_reg = continuingGenerateReg(0, $plp->year_of_study);
                $plp->save();
            }
            $id = $plp->student_reg;
//            $program = $plp->program_id;
            $program = $plp->option_id;
//            $rt = RpProgram::find($program);
            $rt = CollegeOption::find($program);
            $department = null;
            if ($rt)
                $department = $rt->department_id;

            $data = [
                'id' => $id,
                'name' => trim($plp->first_name . " " . $plp->last_name),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password'))
            ];

            $add = Student::create($data);

            if ($add) {
                $data['admission'] = false;
                $data['std_id'] = $data['id'];
                $data['first_name'] = $plp->first_name;
                $data['other_names'] = $plp->last_name;
                $data['department_id'] = $department;
                $data['course_id'] = $program;
                $data['college_id'] = $college;
                $data['year_of_study'] = $plp->repeat == 0 ? otherYears($plp->year_of_study) : '17';
                $data['sponsorship_status'] = $plp->sponsor_id;
                $data['academic_year'] = getCurrentAcademicYear();

                AdmittedStudent::create($data);

                $additional = [
                    'college' => getCollege($data['college_id'], 'polytechnic'),
                    'year_of_study' => $plp->year_of_study,
                    'student_id' => $data['id'],
                    'department' => getDepartment($department, 'department_name', true),
                    'option' => $rt ? $rt->option_name : "",
                    'academic_year' => date('Y') . '-' . date('Y', strtotime('+1 year'))
                ];
                Mail::to($data['email'])
                    ->send(new RegisterMailContinuing($data['name'], $data['id'], 'cont', null, $additional));
                return view('student.auth.signup.p_confirm');
            }
        } else {
            return back()
                ->withErrors(['registration_number' => 'Please the registration number provided, is not from the college provided'])
                ->withInput();
        }
    }

}
