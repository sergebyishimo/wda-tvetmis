<?php

namespace App\Http\Controllers\StudentAuth;

use App\FeeCategory;
use App\Mail\PendingSuccessfullyDoneEmail;
use App\Model\Rp\FunctionalFees;
use App\Rp\PaymentInvoice;
use App\Rp\PendingPaymentInvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Rp\CollegePayment;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['student']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function showFormCode()
    {
        $user = Auth::guard('student')->user();
        $feeCategoriesWithFees = [];
        $feeCategories = FeeCategory::all();
        $fees = [];
        $sponsor = get_my_info('sponsorship_status');
        $dfs = "first_year";
        $col = "private_sponsored_amount";

        if ($sponsor == "government")
            $col = "govt_sponsored_amount";

        if (isContinuingStudent($user->id))
            $dfs = getMyYearOfStudy(true) . "_year";
        $isAdmitted = isAdmitted($user->id);
        foreach ($feeCategories as $feeCategoy) {
            if ($isAdmitted && strtolower($feeCategoy->category_name) == strtolower('Application'))
                continue;
            if ($feeCategoy->fees()->count() > 0) {
                $functionalists = $feeCategoy->fees();
                $fees = $functionalists->whereIn('paid_when', [$dfs, 'each_year']);
            }
            $program = get_my_info('course_id', true);

            $yes = true;
            if ($feeCategoy->program_id != null && $feeCategoy->program_id != $program)
                $yes = false;

            if ($fees->count() > 0 && $yes)
                $feeCategoriesWithFees[] = [
                    'name' => $feeCategoy->category_name,
                    'fees' => $fees->get(),
                    'required' => $feeCategoy->mandatory == 1 ? '1' : '0',
                    'multiple_time' => $feeCategoy->multiple_time
                ];
        }

        return view('student.payment.showFormCode', compact('feeCategoriesWithFees', 'col'));
        // return view('student.showFormCode', compact('feeCategoriesWithFees', 'col'));
    }

    public function pending()
    {
        $invoices = auth()->guard('student')->user()->invoices;

        if ($invoices->count() > 0) {
            $invoices = auth()->guard('student')->user()->invoices();
            $invoices = $invoices->where('active', '1')->get();
        } else
            $invoices = [];

        return view('student.payment.pending', compact('invoices'));
        // return view('student.pending', compact('invoices'));
    }

    public function deletePending(Request $request, $code)
    {
        $code = $request->input('code');
        $invoice = auth()->guard('student')->user()->invoices()->where('code', $code);
        if ($invoice->count()) {
            foreach ($invoice->get() as $invoic) {
                if ($invoic->paid <= 0)
                    $invoic->delete();
            }
        }
        return redirect()->route('student.payment.pending')->with(['status' => '1', 'message' => 'Pending Invoice Canceled ...']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function generateCode(Request $request)
    {
        $this->validate($request, [
            'fees' => 'required|array|min:1'
        ]);

        if ($request->has('code_invoice')) {
            $code = $request->input('code_invoice');
            $fees = $request->fees;
            $category = $request->input('category');
            $mandatory = $request->input('mandatory');
            $std = auth()->guard('student')->user()->id;
            $created = Carbon::now()->toDateTimeString();
            if (count($fees) > 0) {
                $insert = [];
                $sum = 0;
                foreach ($fees as $fee => $amount) {
                    $insert[] = [
                        'student_id' => $std,
                        'function_fee' => $fee,
                        'amount' => $amount,
                        'code' => $code,
                        'created_at' => $created
                    ];
                    $sum += $amount;
                }
                if (!empty($insert)) {
                    if (!isInvoiceCodeExists($code)) {
                        PaymentInvoice::insert($insert);
                        $sponsor = sponsored(get_my_info(), $category, true, true);
                        if ($sponsor) {
                            $covered = ($sum * $sponsor->percentage) / 100;
                            $sum = $sum - $covered;
                        }

                        $pending = PendingPaymentInvoice::create([
                            'student_id' => $std,
                            'name' => $category,
                            'code' => $code,
                            'amount' => $sum,
                            'paid' => '0',
                            'active' => true,
                            'partial' => $mandatory == '1' ? false : true,
                            'academic_year' => getCurrentAcademicYear()
                        ]);

                        $student = auth()->guard('student')->user()->email;
                        if ($student) {
                            $data = [
                                'reg_no' => get_my_info(),
                                'college' => get_my_info('college_id'),
                                'department' => get_my_info('department_id'),
                                'program' => get_my_info('course_id'),
                                'names' => trim(get_my_info('names')),
                                'academic_year' => getCurrentAcademicYear(),
                                'year_of_study' => ucwords(getMyYearOfStudy(true, get_my_info('year_of_study'))),
                                'amount' => $sum ? number_format($sum) . " RWF" : "",
                                'date_invoice' => $pending->created_at,
                                'sponsor' => get_my_info('sponsorship_status') ? strtoupper(get_my_info('sponsorship_status')) : "",
                                'category' => $category,
                                'code' => $pending->code
                            ];
                            Mail::to($student)->send(new PendingSuccessfullyDoneEmail($data));
                        }

                    }
                }
            }
            return redirect()->route("student.payment.pending")->with(['status' => '1', 'message' => $code . ' now active for payment']);
        }

        $fees_ = $request->fees;
//        dd($fees_);
        $col = $request->col;
        $category = $request->input('category');
        $mandatory = $request->input('mandatory');
        $sum = 0;
        $code = strtoupper(generatePaymentCode());
        $fees = [];
        foreach ($fees_ as $fee) {
            $rt = FunctionalFees::find($fee);
            if ($rt) {
                $sum += $rt->{$col};
                $fees[] = $rt;
            }
        }
        $sponsor = sponsored(get_my_info(), $category, true, true);
        return view('student.payment.codeConfirm', compact('fees', 'col', 'code', 'sum', 'category', 'mandatory', 'sponsor'));
    }

    public function history()
    {
        $payments = CollegePayment::where('std_id', auth()->guard('student')->user()->id)->orderBy('op_date', 'desc')->paginate(30);
        return view('student.payment.history', compact('payments'));
        // return view('student.history', compact('payments'));
    }

}
