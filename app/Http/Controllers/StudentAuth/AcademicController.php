<?php

namespace App\Http\Controllers\StudentAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CollegeContinuingStudent;
use App\Model\Rp\StudentMarks;
use App\FailedModule;

class AcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = CollegeContinuingStudent::where('student_reg', get_my_info())->first();
        // FailedModule::where('student_reg', get_my)
        return view('student.academic.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = CollegeContinuingStudent::where('student_reg', get_my_info())->first();
        if($student) {
            $marks = StudentMarks::where('academic_year', $request->academic_year)->where('semester', 1)->where('registration_number', $student->student_reg)->get();
            return view('student.academic.index', compact('marks'));
        }

        return view('student.academic.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = CollegeContinuingStudent::where('student_reg', get_my_info())->first();
        if($student) {
            $failed = FailedModule::where('student_id', $student->student_reg)->get();
            return view('student.academic.failed', compact('failed'));
        }
        return view('student.academic.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function results(Request $request)
    {
        $student = CollegeContinuingStudent::where('student_reg', get_my_info())->first();
        // FailedModule::where('student_reg', get_my)
        return view('student.academic.index');
    }
}
