<?php

namespace App\Http\Controllers\StudentAuth;

use App\Rp\AccommodationApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rp\CollegeStudent;

class AccommodationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $check = CollegeStudent::where('student_reg', get_my_info())->count();
        return view('student.accommodation.index', compact('check'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'college_id' => get_my_info('college_id', true),
            'student_reg' => get_my_info(), 
            'academic_year' => $request->academic_year,
            'file' => $request->file->store('uploads/accommodations', 'public')
        ];
        AccommodationApplication::create($data);

        return redirect()->back()->with(['status' => 1, 'message' => 'You have successfully submitted your accommodation application! Wait for college feedback! ']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rp\AccommodationApplication  $accommodationApplication
     * @return \Illuminate\Http\Response
     */
    public function show(AccommodationApplication $accommodationApplication)
    {
        $check = CollegeStudent::where('student_reg', get_my_info())->count();
        $applications = AccommodationApplication::where('student_reg', get_my_info())->get();
        return view('student.accommodation.application', compact('applications', 'check'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rp\AccommodationApplication  $accommodationApplication
     * @return \Illuminate\Http\Response
     */
    public function edit(AccommodationApplication $accommodationApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rp\AccommodationApplication  $accommodationApplication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccommodationApplication $accommodationApplication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rp\AccommodationApplication  $accommodationApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccommodationApplication $accommodationApplication)
    {
        //
    }
}
