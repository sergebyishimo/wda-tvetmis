<?php

namespace App\Http\Controllers\StudentAuth;

use App\Http\Requests\SubmitRegistrationFormRequest;
use App\Http\Requests\UpdateRegistrationFormRequest;
use App\Mail\ConfirmRegistrationSuccess;
use App\Model\Rp\AdmissionPrivate;
use App\Model\Rp\StudentRegistered;
use App\Rp\CollegeStudent;
use App\Rp\PendingPaymentInvoice;
use App\Rp\SponsorshipStatus;
use App\Rp\TransferRequest;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Storage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/student/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('student');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:students',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return Student
     */
    protected function create(array $data)
    {
        return Student::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('student.auth.register');
        // return view('student.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('student');
    }

    /**
     * @param SubmitRegistrationFormRequest $request
     * @return void
     */
    public function registering(SubmitRegistrationFormRequest $request)
    {
        if ($request->country == '171')
            $rules = [
                "national_id_number" => 'regex:/1[0-9]{4}[8,7][0-9]{10}/',
                "province" => 'required',
                "district" => 'required',
                "sector" => 'required',
                "cell" => 'required',
                'village' => 'required'
            ];
        else
            $rules = [
                'address' => 'required'
            ];
        if (!applied() && !get_my_info('scan_national_id_passport', true) && !get_my_info('scan_of_diploma_or_certificate', true) && !get_my_info('photo', true))
            $rules = array_merge($rules, [
                'scan_national_id_passport' => 'required',
                'scan_of_diploma_or_certificate' => 'required',
                'photo' => 'required'
            ]);
        $this->validate($request, $rules);
        return $this->store($request);
    }

    public function updateRegistering(UpdateRegistrationFormRequest $request)
    {
        if ($request->country == '171') {
            $this->validate($request, [
                "national_id_number" => 'regex:/1[0-9]{4}[8,7][0-9]{10}/',
                "province" => 'required',
                "district" => 'required',
                "sector" => 'required',
                "cell" => 'required',
                'village' => 'required'
            ]);
        } else {
            $this->validate($request, [
                'address' => 'required'
            ]);
        }
        return $this->store($request);
    }

    protected function store($request)
    {
        if (isAdmitted(get_my_info()))
            $student = CollegeStudent::where('student_reg', get_my_info())->first();
        else
            $student = AdmissionPrivate::where('std_id', get_my_info())->first();

        $request->request->remove('_token');
        $input = $request->input();
        $files = $request->allFiles();
        if (admission()) {
            $input["photo"] = get_my_info('photo', true);
            $input["scan_national_id_passport"] = get_my_info('scan_national_id_passport', true);
            $input["scan_of_diploma_or_certificate"] = get_my_info('scan_of_diploma_or_certificate', true);
        } else {
            if (!empty($files)) {
                $path = 'applicatants/' . date('FY');
                if ($request->hasFile('photo')) {
                    $photo = $request->file('photo');
                    $photoP = $path;
                    $descF = Storage::disk('public')->put($photoP, $photo, 'public');
                    $input["photo"] = $descF;
                }

                if ($request->hasFile('scan_national_id_passport')) {
                    $scanpassport = $request->file("scan_national_id_passport");
                    $scanpassportP = $path;
                    $passF = Storage::disk('public')->put($scanpassportP, $scanpassport, 'public');
                    $input["scan_national_id_passport"] = $passF;
                }

                if ($request->hasFile('scan_of_diploma_or_certificate')) {
                    $scancertificate = $request->file("scan_of_diploma_or_certificate");
                    $scancertificateP = $path;
                    $certF = Storage::disk('public')->put($scancertificateP, $scancertificate, 'public');
                    $input["scan_of_diploma_or_certificate"] = $certF;
                }
            }
        }
        if ($student) {
            $input['college_id'] = get_my_info('college_id', true);
            $input['course_id'] = get_my_info('course_id', true);
            $input['department_id'] = get_my_info('department_id', true);
            $update = $student->update($input);
            if ($update)
                return back()->with('success', 'successfully updated !!');
            else
                return back() > with('error', 'Fail to update check what you entered and try again !!');
        } else {
            $input['college_id'] = get_my_info('college_id', true);
            $input['course_id'] = get_my_info('course_id', true);
            $input['department_id'] = get_my_info('department_id', true);
            $input['student_reg'] = get_my_info();

//            if ($input['year_of_study'] == date('Y'))
//                $input['year_of_study'] = '1';
            $input['year_of_study'] = getMyYearOfStudy(false, get_my_info('year_of_study'));

            if (strlen(get_my_info('sponsorship_status')) <= 1)
                $input['sponsorship_status'] = 'government';
            else
                $input['sponsorship_status'] = strtolower(get_my_info('sponsorship_status'));

            if (isAdmitted(get_my_info()))
                $save = CollegeStudent::create($input);
            else
                $save = false;

            if ($save == false)
                return back()->with('error', 'Fail to submit check what you entered and try again !!');
            else {
                $data = [
                    'reg_no' => $input['student_reg'],
                    'email' => $input['email'],
                    'college' => getCollege($input['college_id'], 'polytechnic'),
                    'department' => getDepartment($input['department_id'], 'department_name'),
                    'course' => getCourse($input['course_id'], 'program_name'),
                    'names' => trim(ucwords($input['first_name'] . " " . $input['other_names'])),
                    'academic_year' => getCurrentAcademicYear(),
                    'date' => $save ? $save->created_at : "",
                    'year_of_study' => ucwords(getMyYearOfStudy(true, get_my_info('year_of_study')))
                ];

                StudentRegistered::create([
                    'academic_year' => $data['academic_year'],
                    'college_id' => $input['college_id'],
                    'student_id' => $data['reg_no'],
                    'prof' => 0,
                    'year_of_study' => $input['year_of_study']
                ]);

                Mail::to($data['email'])->send(new ConfirmRegistrationSuccess($data));
            }
            return back()->with('success', 'Submitted Successfully !!');
        }
    }

    public function transferRequest()
    {
        return view("student.registration.transfer");
        // return view("student.transfer");
    }

    public function transferRequestSubmit(Request $request)
    {
        $this->validate($request, [
            'letter' => 'required|mimes:pdf',
            'college' => 'required|integer',
            'department' => 'required|integer',
            'course' => 'required|integer'
        ]);

        $path = 'transfer/' . date('FY');
        $letters = null;
        if ($request->hasFile('letter')) {
            $letter = $request->file('letter');
            $letterP = $path;
            $letters = Storage::disk('public')->put($letterP, $letter, 'public');
        }
        $create = [
            'student_id' => get_my_info(),
            'letter' => $letters,
            'college_id' => $request->college,
            'from' => get_my_info('college_id', true),
            'department_id' => $request->department,
            'course_id' => $request->course,
            'from_college_id' => get_my_info('college_id', true),
            'from_department_id' => get_my_info('department_id', true),
            'from_course_id' => get_my_info('course_id', true),
        ];
        TransferRequest::create($create);

        return redirect()->route('student.transfer')->with('success', "Transfer Request Submitted Successfully.");
    }

    public function cancelTransferRequest(Request $request)
    {
        $r = $request->student;

        $req = TransferRequest::where('read', '0')
            ->where('student_id', get_my_info())->first();

        if ($req) {
            Storage::disk('public')->delete($req->letter);
            $req->delete();
        }

        return redirect(route('student.transfer'))->with('success', "Request Successfully Canceled.");
    }

    public function prof()
    {
        if (isRegistered(get_my_info())) {

            $academicY = getCurrentAcademicYear();
            $name = $academicY . "_" . get_my_info();
            $reg = get_my_info();

            $amount = null;

            $payment = PendingPaymentInvoice::where('student_id', $reg)
                ->where('active', 0)
                ->where('name', 'LIKE', "Registration")
                ->whereYear('created_at', date('Y'))->first();
            if ($payment)
                $amount = $payment->amount;

            $data = [
                'reg_no' => $reg,
                'college' => get_my_info('college_id'),
                'department' => get_my_info('department_id'),
                'course' => get_my_info('course_id'),
                'names' => trim(ucwords(get_my_info('names'))),
                'academic_year' => $academicY,
                'date' => get_my_info('created_at'),
                'amount' => $amount ? number_format($amount) . " RWF" : "",
                'sponsor' => get_my_info('sponsorship_status'),
                'year_of_study' => ucwords(getMyYearOfStudy(true, get_my_info('year_of_study')) . " Year")
            ];

            $prof = StudentRegistered::where('student_id', $data['reg_no'])
                ->where('academic_year', $academicY)->first();
            if ($prof)
                $prof->update(['prof' => '1']);

            $pdf = \PDF::loadView('student.registration.prof', compact('data'));

            return $pdf->stream($name . '.pdf');

//            return view('student.registration.prof', compact('data'));

        } else
            return redirect()->route('student.registering')->with(['status' => '3', 'message' => 'What are you trying to do. Please register first !!!']);
    }

}
