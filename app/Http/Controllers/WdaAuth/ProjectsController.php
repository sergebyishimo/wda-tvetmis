<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\DevelopmentPartner;
use App\Model\Accr\Project;
use App\Model\Accr\ProjectFinanceTypeSource;
use App\Model\Accr\ProjectProgram;
use App\Model\Accr\ProjectResult;
use App\Model\Accr\ProjectsIndicator;
use App\Model\Accr\ProjectsOutput;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('wda');
    }

    public function basicInfo($project_id = null, Request $request)
    {

        if ($request->delete_project) {

            Project::find($request->delete_project)->delete();
            return redirect()
                ->route('wda.pr.info.index')
                ->with(['status' => '1', 'message' => 'Project Deleted Successfully!!']);
        }

        if ($request->project_name) {
            if ($request->project_id) {
                $project = Project::find($request->project_id);
            } else {
                $project = new Project();
            }

            $project->project_name = $request->project_name;
            $project->project_manager = $request->project_manager;
            $project->duration = $request->duration;
            $project->brief_description = $request->brief_description;

            if ($request->file('upload_file')) {
                $path = $request->file('upload_file')->store('projects/attachments', config('voyager.storage.disk'));
                $project->attachment_name = $path;
            }


            if ($request->finance_type) {
                $project->finance_type = implode(', ', $request->finance_type);
            }
            if ($request->development_partner) {
                $project->development_partner = implode(', ', $request->development_partner);
            }

            $project->budget_agency_name = $request->budget_agency_name;

            $project->save();

            return redirect()
                ->route('wda.pr.info.index')
                ->with(['status' => '1', 'message' => 'Project Saved Successfully!!']);
        }

        $devs = DevelopmentPartner::all();
        $fins = ProjectFinanceTypeSource::all();
        $projects = Project::all();

        if ($project_id != null) {
            $project_info = Project::find($project_id);

            return view('wda.projects.basic_info', compact('fins', 'devs', 'projects', 'project_info'));
        }

        return view('wda.projects.basic_info', compact('fins', 'devs', 'projects'));
    }

    public function developmentPartners($dev_id = null, Request $request)
    {

        if ($request->delete_dev) {
            DevelopmentPartner::find($request->delete_dev)->delete();
            return redirect()
                ->route('wda.pr.dev.partners.index')
                ->with(['status' => '1', 'message' => 'Development Partner Deleted Successfully!!']);
        }

        if ($request->development_partner) {
            if ($request->dev_id) {
                $dev = DevelopmentPartner::find($request->dev_id);
            } else {
                $dev = new DevelopmentPartner();
            }


            $dev->development_partner = $request->development_partner;
            $dev->area_of_operation = $request->area_of_operation;
            $dev->contact_person = $request->contact_person;
            $dev->address_details = $request->address_details;

            $dev->save();

            return redirect()
                ->route('wda.pr.dev.partners.index')
                ->with(['status' => '1', 'message' => 'Development Partner Saved Successfully!!']);
        }


        $devs = DevelopmentPartner::all();

        if ($dev_id != null) {

            $dev_info = DevelopmentPartner::find($dev_id);
            return view('wda.projects.dev_partners', compact('devs', 'dev_info'));
        }

        return view('wda.projects.dev_partners', compact('devs'));
    }

    public function financetypes(Request $request, $id = null)
    {
        if ($request->delete_fin) {
            ProjectFinanceTypeSource::find($request->delete_fin)->delete();
            return redirect()
                ->route('wda.pr.fin.types.index')
                ->with(['status' => '1', 'message' => 'Project Finance Type Deleted Successfully!!']);
        }

        if ($request->finance_type) {
            if ($request->id)
                $fin = ProjectFinanceTypeSource::findOrFail($request->id);
            else
                $fin = new ProjectFinanceTypeSource();

            $fin->finance_type = $request->finance_type;
            $fin->save();
        }

        $fins = ProjectFinanceTypeSource::all();

        $finstype = null;

        if ($id != null)
            $finstype = ProjectFinanceTypeSource::find($id);

        return view('wda.projects.financetypes', compact('fins', 'finstype'));
    }

    public function programs($program_id = null, Request $request)
    {

        if ($request->name) {
            $program = new ProjectProgram();
            $program->project_id = $request->project_id;
            $program->name = $request->name;
            $program->save();

            return redirect()
                ->route('wda.pr.programs.index')
                ->with(['status' => '1', 'message' => 'Program Created Successfully!!']);
        }

        if ($request->u_name) {
            $program = ProjectProgram::find($request->program_id);

            $program->name = $request->u_name;

            $program->save();

            return redirect()
                ->route('wda.pr.programs.index')
                ->with(['status' => '1', 'message' => 'Program Updated Successfully!!']);
        }

        if ($request->delete_pro) {

            ProjectProgram::find($request->delete_pro)->delete();
            return redirect()
                ->route('wda.pr.programs.index')
                ->with(['status' => '1', 'message' => 'Program Deleted Successfully!!']);
        }

        $projects = Project::all();
        $programs = ProjectProgram::all();

        if ($program_id != null) {
            $program_info = ProjectProgram::find($program_id);

            return view('wda.projects.programs', compact('projects', 'programs', 'program_info'));
        }

        return view('wda.projects.programs', compact('projects', 'programs'));
    }

    public function results(Request $request, $result_id = null)
    {
        if ($request->delete_result) {

            ProjectResult::find($request->delete_result)->delete();
            return redirect()
                ->route('wda.pr.results.index')
                ->with(['status' => '1', 'message' => 'Project Result Deleted Successfully!!']);
        }

        if ($request->name) {
            if ($request->result_id) {
                $result = ProjectResult::find($request->result_id);
            } else {
                $result = new ProjectResult();
            }

            $result->program_id = $request->program_id;
            $result->name = $request->name;
            $result->save();

            redirect()
                ->route('wda.pr.results.index')
                ->with(['status' => '1', 'message' => 'Result Saved Successfully!!']);
        }


        $projects = Project::all();
        $programs = ProjectProgram::all();
        $results = ProjectResult::all();

        if ($result_id != null) {
            $result_info = ProjectResult::find($result_id);

            return view('wda.projects.results', compact('projects', 'programs', 'results', 'result', 'result_info'));
        }

        return view('wda.projects.results', compact('projects', 'programs', 'results'));
    }

    public function indicators($indicator_id = null, Request $request)
    {
        $projects = Project::all();
        $programs = ProjectProgram::all();
        $indicators = ProjectsIndicator::all();

        if ($request->delete_indicator) {
            ProjectsIndicator::find($request->delete_indicator)->delete();
            redirect()
                ->route('wda.pr.indicators.index')
                ->with(['status' => '1', 'message' => 'Indicator Saved Successfully!!']);
        }

        if ($indicator_id != null) {
            $indicator_info = ProjectsIndicator::find($indicator_id);

            return view('wda.projects.indicators', compact('projects', 'programs', 'indicators', 'indicator_info'));
        }

        if ($request->indicator) {
            if ($request->indicator_id) {
                $indicator = ProjectsIndicator::find($request->indicator_id);

            } else {
                $indicator = new ProjectsIndicator();

                $indicator->result_id = $request->result_id;
            }

            $indicator->name = $request->name;
            $indicator->baseline = $request->baseline;

            $indicator->year_one = $request->year_one;
            $indicator->year_one_target = $request->year_one_target;
            $indicator->year_one_actual = $request->year_one_actual;
            $indicator->year_two = $request->year_two;
            $indicator->year_two_target = $request->year_two_target;
            $indicator->year_two_actual = $request->year_two_actual;
            $indicator->year_three = $request->year_three;
            $indicator->year_three_target = $request->year_three_target;
            $indicator->year_three_actual = $request->year_three_actual;
            $indicator->year_four = $request->year_four;
            $indicator->year_four_target = $request->year_four_target;
            $indicator->year_four_actual = $request->year_four_actual;
            $indicator->year_five = $request->year_five;
            $indicator->year_five_target = $request->year_five_target;
            $indicator->year_five_actual = $request->year_five_actual;
            $indicator->year_six = $request->year_six;
            $indicator->year_six_target = $request->year_six_target;
            $indicator->year_six_actual = $request->year_six_actual;
            $indicator->year_seven = $request->year_seven;
            $indicator->year_seven_target = $request->year_seven_target;
            $indicator->year_seven_actual = $request->year_seven_actual;

            $indicator->activities_to_deliver_output = $request->activities_to_deliver_output;
            $indicator->stakeholders = $request->stakeholders;
            $indicator->budget = $request->budget;
            $indicator->budget_spent = $request->budget_spent;
            $indicator->narrative_progress = $request->narrative_progress;

            $indicator->save();

            redirect()
                ->route('wda.pr.indicators.index')
                ->with(['status' => '1', 'message' => 'Strategic Plan Indicator Saved Successfully!!']);
        }

        $projects = Project::all();
        $indicators = ProjectsIndicator::all();

        return view('wda.projects.indicators', compact('projects', 'indicators', 'programs'));

    }

    public function report(Request $request, $project_id = null)
    {
        $projects = Project::all();
        $columns['indicators'] = DB::select("SHOW COLUMNS FROM `accr_projects_indicators`");
        $columns['programs'] = DB::select("SHOW COLUMNS FROM `accr_project_programs`");
        $columns['results'] = DB::select("SHOW COLUMNS FROM `accr_project_results`");
        $datas['indicators'] = DB::select("SELECT `id`,`name` FROM `accr_projects_indicators`");
        $datas['programs'] = DB::select("SELECT `id`,`name` FROM `accr_project_programs`");
        $datas['results'] = DB::select("SELECT `id`,`name` FROM `accr_project_results`");

        if (isset($request->project_id) || $project_id != null) {

            $project_id = ($project_id) ? $project_id : $request->project_id;

            $programs = DB::connection('accr_mysql')->table('project_programs')
                ->join('mande_minecofin_projects', 'mande_minecofin_projects.id', '=', 'project_programs.project_id')
                ->join('project_results', 'project_results.program_id', '=', 'project_programs.id')
                ->join('projects_indicators', 'projects_indicators.result_id', '=', 'project_results.id')
                ->select('project_programs.*', 'project_programs.id as proId', 'project_programs.name as programName', 'mande_minecofin_projects.*', 'projects_indicators.*', 'projects_indicators.name as indicatorName', 'project_results.*', 'project_results.name as resultName')
                ->where('project_programs.project_id', '=', $project_id)
                ->get();
//            $programs = ProjectProgram::where('project_id', $project_id)->get();
//                dd($programs);
            return view('wda.projects.report', compact('projects', 'columns', 'datas', 'programs'));
        }
        if (isset($request->mySelectId)) {
            $inputColumns = $request->columns_select;
            $project_id = $request->mySelectId;

            $Datacolumns = " ";
            $columnsDisplays = " ";
            $x = 1;
            if (is_array($inputColumns)) {
                foreach ($inputColumns as $inputColumn => $value) {
                    if (strpos($value, 'projects_indicators') !== false) {
                        $columnsDisplays .= str_replace('projects_indicators.', '', $value);
                    }
                    if (strpos($value, 'project_programs') !== false) {
                        $columnsDisplays .= str_replace('project_programs.', '', $value);
                    }
                    if (strpos($value, 'project_results') !== false) {
                        $columnsDisplays .= str_replace('project_results.', '', $value);
                    }
                    $Datacolumns .= $value;
                    if ($x < count($inputColumns)) {
                        $Datacolumns .= ', ';
                        $columnsDisplays .= ', ';
                    }
                    $x++;
                }
            } else {
                return redirect()->back();
            }

            $DataColumnsprograms = DB::connection('accr_mysql')->table('project_programs')
                ->join('mande_minecofin_projects', 'mande_minecofin_projects.id', '=', 'project_programs.project_id')
                ->join('project_results', 'project_results.program_id', '=', 'project_programs.id')
                ->join('projects_indicators', 'projects_indicators.result_id', '=', 'project_results.id')
                ->select('project_programs.id as programId', DB::raw($Datacolumns))
                ->where('project_programs.id', '=', $project_id)
                ->get();
            return view('wda.projects.report', compact('projects', 'columnsDisplays', 'DataColumnsprograms', 'columns', 'datas'));
        }

        if (isset($request->search_programid) || isset($request->search_result) || isset($request->search_indicator)) {
            $project_id = $request->search_programid;
            $result_id = $request->search_result;
            $indicator_id = $request->search_indicator;
            $search = " ";
            $searchId = " ";
            if (!empty($project_id)) {
                $search = 'project_programs';
                $searchId = $project_id;
            } elseif (!empty($result_id)) {
                $search = 'project_results';
                $searchId = $result_id;

            } else {
                $search = 'projects_indicators';
                $searchId = $indicator_id;

            }

            $searchprograms = DB::connection('accr_mysql')->table('project_programs')
                ->join('mande_minecofin_projects', 'mande_minecofin_projects.id', '=', 'project_programs.project_id')
                ->join('project_results', 'project_results.program_id', '=', 'project_programs.id')
                ->join('projects_indicators', 'projects_indicators.result_id', '=', 'project_results.id')
                ->select('project_programs.*', 'project_programs.id as proId', 'project_programs.name as programName', 'mande_minecofin_projects.*', 'projects_indicators.*', 'projects_indicators.name as indicatorName', 'project_results.*', 'project_results.name as resultName')
                ->where('' . $search . '.id', '=', $searchId)
                ->get();
            return view('wda.projects.report', compact('projects', 'columnsDisplays', 'searchprograms', 'columns', 'datas'));
        }

        return view('wda.projects.report', compact('projects', 'columns', 'datas'));
    }

    public function export(Request $request)
    {

//        $alphabet=range('A','W');
//         dd($alphabet);
        if ($request->project_id) {

            $spreadsheet = new Spreadsheet();

            $sheet = $spreadsheet->getActiveSheet();
            $styleArray = array(
                'font' => array('bold' => true),
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
                'borders' => array('top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'fill' => array(
                        'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startcolor' => array('argb' => 'FFA0A0A0',), 'endcolor' =>
                            array('argb' => 'FFFFFFFF'))));
            foreach (range('A', 'U') as $columnID) {
                $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $spreadsheet->getActiveSheet()->getStyle('A1:U1')->applyFromArray($styleArray);


            $sheet->setCellValue('A1', 'PROGRAM');
            $sheet->setCellValue('B1', 'OUTPUT');
            $sheet->setCellValue('C1', 'INDICATORS');
            $sheet->setCellValue('D1', 'BASELINE');
            $sheet->setCellValue('E1', 'PROJECT NAME');
            $sheet->setCellValue('F1', 'PROJECT MANAGER');
            $sheet->setCellValue('G1', 'DURATION');
            $sheet->setCellValue('H1', 'DESCRIPTION');
            $sheet->setCellValue('I1', 'BUDGET AGENCY NAME');
            $sheet->setCellValue('J1', 'DELIVER OUTPUT');
            $sheet->setCellValue('K1', 'STAKEHOLDERS');
            $sheet->setCellValue('L1', 'BUDGET');
            $sheet->setCellValue('M1', 'BUDGET SPENT');
            $sheet->setCellValue('N1', 'NARRATIVE REPORT');
            $sheet->setCellValue('O1', 'YEAR 1 /TARGET /ACTUAL');
            $sheet->setCellValue('P1', 'YEAR 2 /TARGET /ACTUAL');
            $sheet->setCellValue('Q1', 'YEAR 3 /TARGET /ACTUAL');
            $sheet->setCellValue('R1', 'YEAR 4 /TARGET /ACTUAL');
            $sheet->setCellValue('S1', 'YEAR 5 /TARGET /ACTUAL');
            $sheet->setCellValue('T1', 'YEAR 6 /TARGET /ACTUAL');
            $sheet->setCellValue('U1', 'YEAR 7 /TARGET /ACTUAL');
            $spreadsheet->getActiveSheet()->setTitle('Projects Plan Information');

            $programs = DB::connection('accr_mysql')->table('project_programs')
                ->join('mande_minecofin_projects', 'mande_minecofin_projects.id', '=', 'project_programs.project_id')
                ->join('project_results', 'project_results.program_id', '=', 'project_programs.id')
                ->join('projects_indicators', 'projects_indicators.result_id', '=', 'project_results.id')
                ->select('project_programs.*', 'project_programs.id as proId', 'project_programs.name as programName', 'mande_minecofin_projects.*', 'projects_indicators.*', 'projects_indicators.name as indicatorName', 'project_results.*', 'project_results.name as resultName')
                ->where('project_programs.id', '=', $request->project_id)
                ->get();

            $x = 2;
            foreach ($programs as $program) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A$x", $program->programName)
                    ->setCellValue("B$x", $program->resultName)
                    ->setCellValue("C$x", $program->indicatorName)
                    ->setCellValue("D$x", $program->baseline)
                    ->setCellValue("E$x", $program->project_name)
                    ->setCellValue("F$x", $program->project_manager)
                    ->setCellValue("G$x", $program->duration)
                    ->setCellValue("H$x", $program->brief_description)
                    ->setCellValue("I$x", $program->budget_agency_name)
                    ->setCellValue("J$x", $program->activities_to_deliver_output)
                    ->setCellValue("K$x", $program->stakeholders)
                    ->setCellValue("L$x", $program->budget)
                    ->setCellValue("M$x", $program->budget_spent)
                    ->setCellValue("N$x", $program->narrative_progress)
                    ->setCellValue("O$x", $program->year_one . ' / ' . $program->year_one_target . ' / ' . $program->year_one_actual)
                    ->setCellValue("P$x", $program->year_two . ' / ' . $program->year_two_target . ' / ' . $program->year_two_actual)
                    ->setCellValue("Q$x", $program->year_three . ' / ' . $program->year_three_target . ' / ' . $program->year_three_actual)
                    ->setCellValue("R$x", $program->year_four . ' / ' . $program->year_four_target . ' / ' . $program->year_four_actual)
                    ->setCellValue("S$x", $program->year_five . ' / ' . $program->year_five_target . ' / ' . $program->year_five_actual)
                    ->setCellValue("T$x", $program->year_six . ' / ' . $program->year_six_target . ' / ' . $program->year_six_actual)
                    ->setCellValue("U$x", $program->year_seven . ' / ' . $program->year_seven_target . ' / ' . $program->year_seven_actual);
                $x++;
            }
            $writer = new Xlsx($spreadsheet);

            try {
                $writer->save(storage_path('Projects.xlsx'));
            } catch (Exception $e) {
            }

            return response()->download(storage_path('Projects.xlsx'));


        } else {

            return redirect()->back();
        }

    }

    public function reportResult($result_id)
    {
        $result = ProjectResult::find($result_id);
        return view('wda.projects.report_result', compact('result'));
    }

    public function reportMore($output_id)
    {
        $output = ProjectsOutput::find($output_id);
        return view('projects.report_more', compact('output'));
    }

    public function reporttIndicator($indicator_id)
    {
        $indicator = ProjectsIndicator::find($indicator_id);
        return view('projects.indicator_details', compact('indicator'));
    }

    public function getResults($program_id)
    {
        $results = ProjectResult::where('program_id', $program_id)->get();
        return $results;
    }

    public function getProgram($project_id)
    {
        $results = ProjectProgram::where('project_id', $project_id)->get();
        return $results;
    }

    public function getOutputs($outcome_id)
    {
        $outputs = ProjectsOutput::where('outcome_id', $outcome_id)->get();
        return $outputs;
    }
}
