<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\AccrAttachmentsSource;
use App\Model\Accr\AccrCriteria;
use App\Model\Accr\AccrCriteriaSection;
use App\Model\Accr\AccrNotification;
use App\Model\Accr\AccrWelcomeMessage;
use App\Model\Accr\AuditIndicator;
use App\Model\Accr\AuditQuality;
use App\Model\Accr\Functions;
use App\Model\Accr\InfrastructuresSource;
use App\Model\Accr\QualityArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccrController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('wda');
    }

    public function attachmentsInput($att_id = null, Request $request)
    {
        if ($request->delete_att) {
            AccrAttachmentsSource::find($request->delete_att)->delete();
            return redirect()->route('wda.manage.attachments')->with(['status' => '1', 'message' => 'Attachment Deleted Successfully!!']);
        }

        if ($request->attachment) {
            if ($request->att_id) {
                $att = AccrAttachmentsSource::find($request->att_id);
            } else {
                $att = new AccrAttachmentsSource();
            }

            $att->attachment_name = $request->attachment;
            $att->save();

            return redirect()->route('wda.manage.attachments')->with(['status' => '1', 'message' => 'Attachment Saved Successfully!!']);
        }

        $atts = AccrAttachmentsSource::all();

        if ($att_id != null) {
            $att_info = AccrAttachmentsSource::find($att_id);
            return view('wda.schools.manage.attachments', compact('atts', 'att_info'));
        }

        return view('wda.schools.manage.attachments', compact('atts'));
    }

    public function infrastructuresInput($infra_id = null, Request $request)
    {
        if ($request->delete_att) {
            InfrastructuresSource::find($request->delete_att)->delete();
            return redirect()
                ->route('wda.manage.infrastructures')
                ->with(['status' => '1', 'message' => 'Infrastructure Deleted Successfully!!']);
        }

        if ($request->infrastructure) {
            if ($request->att_id) {
                $att = InfrastructuresSource::find($request->att_id);
            } else {
                $att = new InfrastructuresSource();
            }

            $att->name = $request->infrastructure;
            $att->save();

            return redirect()
                ->route('wda.manage.infrastructures')
                ->with(['status' => '1', 'message' => 'Infrastructure Saved Successfully!!']);
        }

        $atts = InfrastructuresSource::all();

        if ($infra_id != null) {
            $att_info = InfrastructuresSource::find($infra_id);
            return view('wda.schools.manage.infrastructures', compact('atts', 'att_info'));
        }

        return view('wda.schools.manage.infrastructures', compact('atts'));
    }

    public function criteriaSectionInput($id = null, Request $request)
    {
        if ($request->delete_att) {
            AccrCriteriaSection::find($request->delete_att)->delete();
            return redirect()->route('wda.manage.criteria')
                ->with(['status' => '1', 'message' => 'Criteria Section Deleted Successfully!!']);
        }

        if ($request->criteria) {
            if ($request->att_id) {
                $att = AccrCriteriaSection::find($request->att_id);
            } else {
                $att = new AccrCriteriaSection();
            }

            $att->quality_area_id = $request->quality_area_id;
            $att->criteria_section = $request->criteria;
            $att->save();

            return redirect()
                ->route('wda.manage.criteria')
                ->with(['status' => '1', 'message' => 'Criteria Section Saved Successfully!!']);
        }

        $atts = QualityArea::all();

        if ($id != null) {
            $att_info = AccrCriteriaSection::find($id);
            return view('wda.schools.manage.criteriasection', compact('atts', 'att_info'));
        }

        return view('wda.schools.manage.criteriasection', compact('atts'));
    }

    public function indicatorsInput($id = null, Request $request)
    {
        $rps = Functions::all();
        $programs = AccrCriteria::all();

        if ($id != null) {
            $program_info = AccrCriteria::find($id);

            return view('wda.schools.manage.indicators', compact('rps', 'programs', 'program_info'));
        }

        if ($request->name) {

            $this->validate($request, [
                'criteria_section_id' => 'required',
                'name' => 'required',
                'weight' => 'numeric'
            ]);

            if ($request->program_id) {
                $program = AccrCriteria::find($request->program_id);
            } else {
                $program = new AccrCriteria();
            }
            $program->criteria_section_id = $request->criteria_section_id;
            $program->criteria = $request->name;
            $program->weight = $request->weight;

            $program->save();

            return redirect()
                ->route('wda.manage.indicators')
                ->with(['status' => '1', 'message' => 'Indicator Saved Successfully!!']);
        }

        if ($request->delete_pro) {

            AccrCriteria::find($request->delete_pro)->delete();
            return redirect()
                ->route('wda.manage.indicators')
                ->with(['status' => '1', 'message' => 'Indicator Deleted Successfully!!']);
        }

        return view('wda.schools.manage.indicators', compact('rps', 'programs'));
    }

    public function qualityArea($id = null)
    {
        if ($id != null)
            $quality = QualityArea::findOrFail($id);

        $functions = Functions::all();
        return view('wda.schools.manage.areas.quality.index', compact('functions', 'quality'));
    }

    public function storeQualityArea(Request $request)
    {
        $this->validate($request, [
            'function_id' => 'required',
            'name' => 'required|min:2'
        ]);

        $check = QualityArea::where('function_id', $request->function_id)->where('name', $request->name);

        if ($check->count() > 0)
            return back()->withInput()->withErrors(['name' => 'Already Exists !!']);

        $data = $request->except('_token');

        QualityArea::create($data);

        return back()->with(['status' => '1', 'message' => "Quality Area Added Successfully."]);
    }

    public function updateQualityArea(Request $request, $id)
    {
        $this->validate($request, [
            'function_id' => 'required',
            'name' => 'required|min:2'
        ]);


        $data = $request->except('_token');
        $quality = QualityArea::findOrFail($id);
        $quality->update($data);

        return redirect()->route('wda.manage.quality.area')
            ->with(['status' => '1', 'message' => "Quality Area Updated Successfully."]);
    }

    public function destroyQualityArea(Request $request, $id)
    {
        $quality = QualityArea::findOrFail($id);

        $quality->delete();

        return redirect()->route('wda.manage.quality.area')
            ->with(['status' => '1', 'message' => "Quality Area Deleted Successfully."]);
    }

    public function generalSettings($noti_id = null, Request $request)
    {
        if (isset($request->form)) {
            switch ($request->form) {
                case 'welcome':
                    if ($request->welcome_id == "") {
                        $welcome = new AccrWelcomeMessage();
                        $welcome->message = $request->message;
                        $welcome->save();
                    } else {
                        $welcome = AccrWelcomeMessage::find($request->welcome_id);
                        $welcome->message = $request->message;
                        $welcome->save();
                    }


                    return redirect()
                        ->route('wda.accr.settings')
                        ->with(['status' => '1', 'message' => 'Welcome Message Saved Successfully']);

                    break;

                case 'notification':

                    if ($request->message) {
                        $notification = new AccrNotification();

                        $notification->message = $request->message;
                        $notification->save();
                    }

                    if ($request->u_message) {
                        $notification = AccrNotification::find($request->noti_id);
                        $notification->message = $request->u_message;
                        $notification->save();
                    }

                    if ($request->delete_noti) {
                        AccrNotification::find($request->delete_noti)->delete();

                        return redirect()
                            ->route('wda.accr.settings')
                            ->with(['status' => '1', 'message' => 'Notification Deleted Successfully!!']);
                    }

                    return redirect()
                        ->route('wda.accr.settings')
                        ->with(['status' => '1', 'message' => 'Notification Saved Successfully!!']);

                    break;

            }
        }

        $welcome = AccrWelcomeMessage::first();
        $notifications = AccrNotification::all();

        if ($noti_id != null) {
            $notification_info = AccrNotification::find($noti_id);
            return view('wda.schools.accr.settings', compact('welcome', 'notifications', 'notification_info'));
        }

        return view('wda.schools.accr.settings', compact('welcome', 'notifications'));
    }

    public function ajaxData()
    {
        $request = request();
        if ($request->input('f')) {
            $function = Functions::find($request->input('f'));
            if ($function) {
                $qa = $function->qualityAreas();
                if ($qa->count() > 0) {
                    return response()->json($qa->pluck('name', 'id')->toArray());
                }
            }
        } elseif ($request->input('q')) {
            $parent = QualityArea::find($request->input('q'));
            if ($parent) {
                $items = $parent->criteria();
                if ($items->count() > 0) {
                    return response()->json($items->pluck('criteria_section', 'id')->toArray());
                }
            }
        } elseif ($request->input('c')) {
            $parent = AccrCriteriaSection::find($request->input('c'));
            if ($parent) {
                $items = $parent->criterias();
                if ($items->count() > 0) {
                    return response()->json($items->pluck('criteria', 'id')->toArray());
                }
            }
        }

        return response()->json([]);
    }

    public function qualityAudit(Request $request, $id = null)
    {
        if ($request->has('_token')) {
            if ($request->has('destroy')) {
                $id = $request->input('destroy');
                $audit = AuditQuality::find($id);
                $audit->delete();
                return back()->with(['status' => '1', 'message' => 'Delete successfully ...']);
            }
            if ($request->has('quality')) {
                $audit = AuditQuality::find($request->input('quality'));
                if (!$audit)
                    return back()->with(['status' => '0', 'message' => 'Quality not find']);
            } else
                $audit = new AuditQuality();

            $audit->name = $request->input('name');
            if ($audit->save())
                return redirect()
                    ->route('wda.audit.manage.quality')
                    ->with(['status' => '1', 'message' => 'Saved successfully']);
            else
                return back()->with(['status' => '0', 'message' => 'Failed to save']);
        }
        $qualityAreas = AuditQuality::all();

        if ($id != null)
            $quality = AuditQuality::find($id);

        return view('wda.schools.manage.audit.quality', compact('qualityAreas', 'quality'));
    }

    public function indicatorsAudit(Request $request, $id = null)
    {
        if ($request->has('_token')) {
            if ($request->has('destroy')) {
                $id = $request->input('destroy');
                $audit = AuditIndicator::find($id);
                $audit->delete();
                return back()->with(['status' => '1', 'message' => 'Delete successfully ...']);
            }
            if ($request->has('indicator')) {
                $audit = AuditIndicator::find($request->input('indicator'));
                if (!$audit)
                    return back()->with(['status' => '0', 'message' => 'Indicator not find']);
            } else
                $audit = new AuditIndicator();
            $audit->quality_id = $request->input('quality_id');
            $audit->name = $request->input('name');
            if ($audit->save())
                return redirect()
                    ->route('wda.audit.manage.indicator')
                    ->with(['status' => '1', 'message' => 'Saved successfully']);
            else
                return back()->with(['status' => '0', 'message' => 'Failed to save']);
        }

        $qualityAreas = AuditQuality::all();
        if ($id != null)
            $cindicator = AuditIndicator::find($id);
        return view('wda.schools.manage.audit.indicators', compact('qualityAreas','cindicator'));
    }

}
