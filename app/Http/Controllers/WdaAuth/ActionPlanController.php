<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\ApIndicator;
use App\Model\Accr\ApOutput;
use App\Model\Accr\ApProgram;
use App\Model\Accr\ApReportingPeriod;
use App\Model\Accr\ApResult;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\PhpWord;

class ActionPlanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('wda');
    }

    public function reportingPeriod($rp_id = null, Request $request)
    {
        $rps = ApReportingPeriod::all();

        if ($rp_id != null) {

            $rp_info = ApReportingPeriod::find($rp_id);

            return view('wda.ap.reporting_period', compact('rps', 'rp_info'));
        }

        if ($request->reporting_period) {
            $rp = new ApReportingPeriod();

            $rp->period = $request->reporting_period;

            $rp->save();

            return redirect()
                ->route('wda.ap.rp.index')
                ->with(['status' => '1', 'message' => 'Reporting Period Created Successfully!!']);
        }

        if ($request->u_reporting_period) {
            $rp = ApReportingPeriod::find($request->rp_id);

            $rp->period = $request->u_reporting_period;

            $rp->save();

            return redirect()
                ->route('wda.ap.rp.index')
                ->with(['status' => '1', 'message' => 'Reporting Period Updated Successfully!!']);
        }

        if ($request->delete_rp) {
            ApReportingPeriod::find($request->delete_rp)->delete();
            return redirect()
                ->route('wda.ap.rp.index')
                ->with(['status' => '1', 'message' => 'Reporting Period Deleted Successfully!!']);
        }


        return view('wda.ap.reporting_period', compact('rps'));
    }

    public function programs($program_id = null, Request $request)
    {
        if ($request->delete_pro) {
            ApProgram::find($request->delete_pro)->delete();
            return redirect()
                ->route('wda.ap.programs.index')
                ->with(['status' => '1', 'message' => 'Program Deleted Successfully!!']);
        }

        if ($request->name) {
            if ($request->program_id) {
                $program = ApProgram::find($request->program_id);
            } else {
                $program = new ApProgram();
                $program->rp_id = $request->reporting_period;
            }


            $program->number = $request->number;
            $program->name = $request->name;

            $program->save();

            return redirect()
                ->route('wda.ap.programs.index')
                ->with(['status' => '1', 'message' => 'Program Saved Successfully!!']);
        }


        $rps = ApReportingPeriod::all();
        $programs = ApProgram::all();

        if ($program_id != null) {
            $program_info = ApProgram::find($program_id);

            return view('wda.ap.programs', compact('rps', 'programs', 'program_info'));
        }

        return view('wda.ap.programs', compact('rps', 'programs'));
    }

    public function results($id = null, Request $request)
    {
        if ($request->delete_result) {

            ApResult::find($request->delete_result)->delete();
            return redirect()
                ->route('wda.ap.results.index')
                ->with(['status' => '1', 'message' => 'Result Deleted Successfully!!']);
        }

        if ($request->name) {
            if ($request->result_id) {
                $result = ApResult::find($request->result_id);
            } else {
                $result = new ApResult();
            }

            $result->program_id = $request->program_id;
            $result->name = $request->name;

            $result->save();

            redirect()
                ->route('wda.ap.results.index')
                ->with(['status' => '1', 'message' => 'Result Saved Successfully!!']);
        }
        $rps = ApReportingPeriod::all();
        $programs = ApProgram::all();
        $results = ApResult::all();

        if ($id != null) {
            $result_info = ApResult::find($id);

            return view('wda.ap.results', compact('rps', 'programs', 'results', 'result_info'));
        }

        return view('wda.ap.results', compact('rps', 'programs', 'results'));
    }

    public function indicators($indicator_id = null, Request $request)
    {

        if ($request->delete_indicator) {

            ApIndicator::find($request->delete_indicator)->delete();
            return redirect()
                ->route('wda.ap.indicators.index')
                ->with(['status' => '1', 'message' => 'Indicator Deleted Successfully!!']);
        }

        if ($request->indicator) {
            if ($request->indicator_id) {
                $indicator = ApIndicator::find($request->indicator_id);
            } else {
                $indicator = new ApIndicator();
            }


            $indicator->result_id = $request->result_id;
            $indicator->name = $request->indicator;
            $indicator->baseline = $request->baseline;

            $indicator->q_one_target = $request->q_one_target;
            $indicator->q_one_actual = $request->q_one_actual;

            $indicator->q_two_target = $request->q_two_target;
            $indicator->q_two_actual = $request->q_two_actual;

            $indicator->q_three_target = $request->q_three_target;
            $indicator->q_three_actual = $request->q_three_actual;

            $indicator->q_four_target = $request->q_four_target;
            $indicator->q_four_actual = $request->q_four_actual;

            $indicator->activities_to_deliver_output = $request->activities_to_deliver_output;
            $indicator->stakeholders = $request->stakeholders;
            $indicator->available = $request->available;
            $indicator->amount_spent = $request->amount_spent;
            $indicator->balance = $request->balance;
            $indicator->narrative_support = $request->narrative_support;

            $indicator->save();

            redirect()
                ->route('wda.ap.indicators.index')
                ->with(['status' => '1', 'message' => 'Action Plan Indicator Saved Successfully!!']);
        }


        $rps = ApReportingPeriod::all();
        $programs = ApProgram::all();
        $indicators = ApIndicator::all();

        if ($indicator_id != null) {
            $indicator_info = ApIndicator::find($indicator_id);

            return view('wda.ap.indicators', compact('rps', 'programs', 'indicators', 'indicator_info'));
        }

        return view('wda.ap.indicators', compact('rps', 'indicators', 'programs'));
    }

    public function report($period_id = null, Request $request)
    {
        $periods = ApReportingPeriod::all();
        $columns['indicators'] = DB::select("SHOW COLUMNS FROM `accr_ap_indicators`");
        $columns['programs'] = DB::select("SHOW COLUMNS FROM `accr_ap_programs`");
        $columns['results'] = DB::select("SHOW COLUMNS FROM `accr_ap_results`");
        $datas['indicators'] = DB::select("SELECT `id`,`name` FROM `accr_ap_indicators`");
        $datas['programs'] = DB::select("SELECT `id`,`name` FROM `accr_ap_programs`");
        $datas['results'] = DB::select("SELECT `id`,`name` FROM `accr_ap_results`");

        if (isset($request->period_id) || $period_id != null) {

            $period_id = ($period_id) ? $period_id : $request->period_id;

//            $programs = ApProgram::where('rp_id', $period_id)->orderBy('number', 'asc')->get();
            $programs = DB::connection('accr_mysql')->table('ap_programs')
                ->join('ap_reporting_periods', 'ap_reporting_periods.id', '=', 'ap_programs.rp_id')
                ->join('ap_results', 'ap_results.program_id', '=', 'ap_programs.id')
                ->join('ap_indicators', 'ap_indicators.result_id', '=', 'ap_results.id')
                ->select('ap_programs.*', 'ap_programs.id as proId', 'ap_programs.name as programName', 'ap_reporting_periods.period', 'ap_indicators.*', 'ap_indicators.name as indicatorName', 'ap_results.*', 'ap_results.name as resultName')
                ->where('ap_reporting_periods.id', '=', $period_id)
                ->get();
//                dd($programs);
            return view('wda.ap.report', compact('periods', 'programs', 'datas', 'columns'));
        }
        //        search for select columns
        if (isset($request->mySelectId)) {
            $inputColumns = $request->columns_select;
            $period_id = $request->mySelectId;

            $Datacolumns = " ";
            $columnsDisplays = " ";
            $x = 1;
            if (is_array($inputColumns)) {
                foreach ($inputColumns as $inputColumn => $value) {
                    if (strpos($value, 'ap_indicators') !== false) {
                        $columnsDisplays .= str_replace('ap_indicators.', '', $value);
                    }
                    if (strpos($value, 'ap_results') !== false) {
                        $columnsDisplays .= str_replace('ap_results.', '', $value);
                    }
                    if (strpos($value, 'ap_programs') !== false) {
                        $columnsDisplays .= str_replace('ap_programs.', '', $value);
                    }
                    $Datacolumns .= $value;
                    if ($x < count($inputColumns)) {
                        $Datacolumns .= ', ';
                        $columnsDisplays .= ', ';
                    }
                    $x++;
                }
            } else {
                return redirect()->back();
            }

            $DataColumnsprograms = DB::connection('accr_mysql')->table('ap_programs')
                ->join('ap_reporting_periods', 'ap_reporting_periods.id', '=', 'ap_programs.rp_id')
                ->join('ap_results', 'ap_results.program_id', '=', 'ap_programs.id')
                ->join('ap_indicators', 'ap_indicators.result_id', '=', 'ap_results.id')
                ->select('ap_programs.id as programId', DB::raw($Datacolumns))
                ->where('ap_reporting_periods.id', '=', $period_id)
                ->get();


            return view('wda.ap.report', compact('periods', 'datas', 'columnsDisplays', 'DataColumnsprograms', 'columns'));
        }
        if (isset($request->search_programid) || isset($request->search_result) || isset($request->search_indicator)) {
            $reportingid = $request->search_programid;
            $result_id = $request->search_result;
            $indicator_id = $request->search_indicator;
            $search = " ";
            $searchId = " ";
            if (!empty($reportingid)) {
                $search = 'ap_programs';
                $searchId = $reportingid;
            } elseif (!empty($result_id)) {
                $search = 'ap_results';
                $searchId = $result_id;
            } else {
                $search = 'ap_indicators';
                $searchId = $indicator_id;
            }

            $searchprograms = DB::connection('accr_mysql')->table('ap_programs')
                ->join('ap_reporting_periods', 'ap_reporting_periods.id', '=', 'ap_programs.rp_id')
                ->join('ap_results', 'ap_results.program_id', '=', 'ap_programs.id')
                ->join('ap_indicators', 'ap_indicators.result_id', '=', 'ap_results.id')
                ->select('ap_programs.*', 'ap_programs.name as programName', 'ap_reporting_periods.period', 'ap_indicators.*', 'ap_indicators.name as indicatorName', 'ap_results.*', 'ap_results.name as resultName')
                ->where('' . $search . '.id', '=', $searchId)
                ->get();
            return view('wda.ap.report', compact('periods', 'datas', 'searchprograms', 'columns'));
        }

        return view('wda.ap.report', compact('periods', 'datas', 'columns'));
    }

    public function reportOlder($period_id = null, Request $request)
    {
        $periods = ApReportingPeriod::all();

        if (isset($request->period_id) || $period_id != null) {

            $period_id = ($period_id) ? $period_id : $request->period_id;

            $programs = ApProgram::where('rp_id', $period_id)->orderBy('number', 'asc')->get();

            return view('wda.ap.reportold', compact('periods', 'programs', 'datas', 'columns'));
        }


        return view('wda.ap.reportold', compact('periods', 'datas', 'columns'));
    }

    public function reportResult($result_id = '')
    {
        $result = ApResult::find($result_id);
        return view('wda.ap.report_result', compact('result'));
    }

    public function reportMore($output_id)
    {
        $output = ApOutput::find($output_id);
        return view('wda.ap.report_more', compact('output'));
    }

    public function export(Request $request)
    {

        if ($request->word) {
            return $this->word($request);
        }
        if ($request->excel) {

            return $this->excel($request);
        }
        if ($request->pdf) {

            $this->pdf($request);
        }

        return redirect()->back();

    }

    private function pdf($request)
    {
        if ($request->period_id) {
            $programs = DB::table('sp_programs')
                ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
                ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
                ->join('sp_indicators', 'sp_indicators.id', '=', 'sp_results.id')
                ->select('sp_programs.*', 'sp_programs.name as programName', 'sp_reporting_periods.period', 'sp_indicators.*', 'sp_indicators.name as indicatorName', 'sp_results.*', 'sp_results.name as resultName')
                ->where('sp_reporting_periods.id', '=', $request->period_id)
                ->get();

            $pdf = PDF::loadView('sp.print', compact('programs'));
            return $pdf->download('strategic.pdf');

//                return view('sp.print',compact('programs'));
        } else {
            return redirect()->back();
        }
    }

    private function word($request)
    {

        $wordTest = new PhpWord();
        if ($request->period_id) {
            // Create a new table style
            $table_style = new \PhpOffice\PhpWord\Style\Table;
            $table_style->setBorderColor('cccccc');
            $table_style->setBorderSize(1);
            $table_style->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);
            $table_style->setWidth(100 * 50);

            // Add a section to the document.
            $section = $wordTest->addSection();

            // Set up our table.
            $section->addTable($table_style);
            $desc1 = "The Portfolio $request->period_id is a very useful feature of the web pagu can establish your archived details and the works to the entire web community. It was outlined to bring in extra clients, get you selected based on this details.";

            $newSection = $wordTest->addSection();
            $newSection->addText($desc1, array('name' => 'Tahoma', 'size' => 15, 'color' => 'green'));

            $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'Word2007');
            try {
                $objectWriter->save(storage_path('StragicPlan.docx'));
            } catch (Exception $e) {
            }

            return response()->download(storage_path('StragicPlan.docx'));

        } else {
            return redirect()->back();
        }
    }

    private function excel($request)
    {
        $spreadsheet = new Spreadsheet();
        $alphabet = range('A', 'W');
//         dd($alphabet);
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
            'borders' => array('top' => array(
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startcolor' => array('argb' => 'FFA0A0A0',), 'endcolor' =>
                        array('argb' => 'FFFFFFFF'))));
        foreach (range('A', 'N') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle('A1:N1')->applyFromArray($styleArray);

        if ($request->period_id) {
            $sheet->setCellValue('A1', 'PROGRAM');
            $sheet->setCellValue('B1', 'OUTPUT');
            $sheet->setCellValue('C1', 'INDICATORS');
            $sheet->setCellValue('D1', 'BASELINE');
            $sheet->setCellValue('E1', ' Quarterly Targets');
            $sheet->mergeCells("E1:H1");
            $sheet->setCellValue('I1', 'Activities to Deliver output');
            $sheet->setCellValue('J1', 'Stakeholders');
            $sheet->setCellValue('K1', 'Available');
            $sheet->setCellValue('L1', 'Amount Spent');
            $sheet->setCellValue('M1', 'Balance');
            $sheet->setCellValue('N1', 'Narrative Report');
            $spreadsheet->getActiveSheet()->setTitle('Action Plan Information');

            $programs = DB::connection('accr_mysql')->table('ap_programs')
                ->join('ap_reporting_periods', 'ap_reporting_periods.id', '=', 'ap_programs.rp_id')
                ->join('ap_results', 'ap_results.program_id', '=', 'ap_programs.id')
                ->join('ap_indicators', 'ap_indicators.result_id', '=', 'ap_results.id')
                ->select('ap_programs.*', 'ap_programs.id as proId', 'ap_programs.name as programName', 'ap_reporting_periods.period', 'ap_indicators.*', 'ap_indicators.name as indicatorName', 'ap_results.*', 'ap_results.name as resultName')
                ->where('ap_reporting_periods.id', '=', $request->period_id)
                ->get();
            $x = 2;
            foreach ($programs as $program) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A$x", $program->programName)
                    ->setCellValue("B$x", $program->resultName)
                    ->setCellValue("C$x", $program->indicatorName)
                    ->setCellValue("D$x", $program->baseline)
                    ->setCellValue("E$x", ' Q1 :' . $program->q_one_target . ' /' . $program->q_one_actual)
                    ->setCellValue("F$x", ' Q2 :' . $program->q_two_target . ' /' . $program->q_two_actual)
                    ->setCellValue("G$x", ' Q3 :' . $program->q_three_target . ' /' . $program->q_three_actual)
                    ->setCellValue("H$x", ' Q4 :' . $program->q_four_target . ' /' . $program->q_four_actual)
                    ->setCellValue("I$x", $program->activities_to_deliver_output)
                    ->setCellValue("J$x", $program->stakeholders)
                    ->setCellValue("K$x", $program->available)
                    ->setCellValue("L$x", $program->amount_spent)
                    ->setCellValue("M$x", $program->balance)
                    ->setCellValue("N$x", $program->narrative_support);
                $x++;
            }
            $writer = new Xlsx($spreadsheet);

            try {
                $writer->save(storage_path('ActionPlan.xlsx'));
            } catch (Exception $e) {
            }

            return response()->download(storage_path('ActionPlan.xlsx'));

        } else {
            return redirect()->back();
        }
    }

    public function toPrint($period_id)
    {
        if ($period_id) {
            $programs = DB::connection('accr_mysql')->table('ap_programs')
                ->join('ap_reporting_periods', 'ap_reporting_periods.id', '=', 'ap_programs.rp_id')
                ->join('ap_results', 'ap_results.program_id', '=', 'ap_programs.id')
                ->join('ap_indicators', 'ap_indicators.result_id', '=', 'ap_results.id')
                ->select('ap_programs.*', 'ap_programs.id as proId', 'ap_programs.name as programName', 'ap_reporting_periods.period', 'ap_indicators.*', 'ap_indicators.name as indicatorName', 'ap_results.*', 'ap_results.name as resultName')
                ->where('ap_reporting_periods.id', '=', $period_id)
                ->get();
            return view('wda.ap.print', compact('programs'));
        } else {
            return redirect()->back();
        }
    }

    public function reporttIndicator($indicator_id)
    {
        $indicator = ApIndicator::find($indicator_id);
        return view('wda.ap.indicator_details', compact('indicator'));
    }

    public function graphs($output_id)
    {
        $output = ApOutput::find($output_id);
        return view('wda.ap.charts', compact('output'));
    }

    // Javascript API Section

    public function getPrograms($rp_id)
    {
        $programs = ApProgram::where('rp_id', $rp_id)->get();
        return $programs;
    }

    public function getResults($program_id)
    {
        $results = ApResult::where('program_id', $program_id)->get();
        return $results;
    }

}
