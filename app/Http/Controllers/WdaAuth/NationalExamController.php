<?php

namespace App\Http\Controllers\WdaAuth;

use App\Combination;
use App\ComputedAggregate;
use App\Http\Requests\WdaNationalExamMarksCreate;
use App\Level;
use App\Model\Accr\SchoolInformation;
use App\NationalExamResult;
use App\OldCourse;
use App\Rtqf;
use App\Student;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use Carbon\Carbon;
use function foo\func;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\DataTables;

class NationalExamController extends Controller
{
    protected $course_id = null;
    protected $combination_id = null;
    protected $academic_year = null;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $combinations = Combination::all();
        $courses = OldCourse::all();
        return view('wda.nationalexams.uploadexams', compact('combinations', 'courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(WdaNationalExamMarksCreate $request)
    {
        $this->course_id = $request->input('course_id');
        $this->combination_id = $request->input('combination_id');
        $filer = $request->file('attachment');
        $this->academic_year = $request->input('acad_year');
        try {
            $data = (new FastExcel)->import($filer->getRealPath());
            $imported = $data->map(function ($line) {
                return [
                    'school_id' => implode(array_slice(str_split($line['school']),0,6)),
                    'student_index' => $line['school'] . $line['option'] . $line['number'],
                    'section_id' => $this->combination_id,
                    'course_id' => $this->course_id,
                    'marks' => $line['marks'],
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ];
            });
            $chunked = $imported->chunk(100)->first();
//            dd($chunked);
            DB::table('national_exam_result')->insert($chunked->toArray());
        } catch (IOException $e) {
            return back()->with(['status' => '0', 'message' => $e->getMessage()]);
        } catch (UnsupportedTypeException $e) {
            return back()->with(['status' => '0', 'message' => $e->getMessage()]);
        } catch (ReaderNotOpenedException $e) {
            return back()->with(['status' => '0', 'message' => $e->getMessage()]);
        }
        return back()->with(['status' => '1', 'message' => 'Imported Successfully go check in Uploaded marks']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploaded()
    {
        $records = NationalExamResult::all();
        return view('wda.nationalexams.uploadedmarks', compact('records'));
    }

    public function getExaminationResults()
    {
        $datas = DB::table('national_exam_result')
            ->join('combinations', 'combinations.id', '=', 'national_exam_result.section_id')
            ->join('old_courses', 'old_courses.id', '=', 'national_exam_result.course_id')
            ->join('accr_schools_information', 'accr_schools_information.school_code', '=', 'national_exam_result.school_id');
        $select = [
            'national_exam_result.marks',
            'national_exam_result.id',
            'national_exam_result.student_index',
            'accr_schools_information.id AS school_id',
            'accr_schools_information.school_name',
            'accr_schools_information.province',
            'accr_schools_information.district',
            'accr_schools_information.sector',
            'accr_schools_information.cell',
            'accr_schools_information.village',
            'accr_schools_information.school_code',
            'combinations.id AS Cid',
            'combinations.combination_name',
            'combinations.combination_code',
            'old_courses.id AS CCid',
            'old_courses.combination_id',
            'old_courses.course_name',
            'old_courses.course_code',

        ];
        $datas->select($select);

        return DataTables::of($datas)
            ->addColumn('action', function ($data) {
                $route = route('wda.nationalexams.edit', $data->id);
                return "<a href='" . $route . "' class='btn btn-edit'>Edit</a>";
            })
            ->addColumn('calc_grade', function ($data) {
                return getNationalExamGrade($data->marks);
            })
            ->make(true);

    }

    public function getCourses(Request $request)
    {
        $id = $request->get('id');
        $d = OldCourse::where('combination_id', $id)->get();
        if ($d->count() > 0) {
            return json_encode($d);
        }
        return json_encode([]);
    }

    public function search(Request $request)
    {
        $result = null;
        if ($request->isMethod('POST')) {
            $student_index = $request->input('student_index');
            $result = NationalExamResult::where('student_index', $student_index)->get();
        }

        $courses = OldCourse::all();
        $combinations = Combination::all();
        if ($combinations)
            $combinations = $combinations->pluck('combination_name', 'id');
        else
            $combinations = [];
        $schools = SchoolInformation::all();
        if ($schools)
            $all_schools = $schools->pluck('school_name', 'school_code');
        else
            $all_schools = [];

        return view('wda.nationalexams.search', compact('result', 'combinations', 'all_schools'));
    }

    public function postQuery(Request $request)
    {
        $parms = ['n' => 1];
        $view = "wda.nationalexams.partial.national";
        if (($request->get('province') != null || $request->get('district') != null || $request->get('school_id') != null) && $request->get('course_id') == null && $request->get('combination_id') == null) {
            $request->request->add(['n' => 1]);
            $parms = $request->except(['_token', 'search']);
        } elseif ($request->get('combination_id') != null || $request->get('course_id') != null) {
            $combination = $request->combination_id;
//            $results = NationalExamResult::where('section_id', $combination);
            $results = ComputedAggregate::join('students', 'students.index_number', '=', 'nationa_exam_computed_aggregates.student_index')
                ->join('accr_schools_information', 'accr_schools_information.school_code', '=', 'nationa_exam_computed_aggregates.school_id')
                ->join('combinations', 'combinations.id', '=', 'nationa_exam_computed_aggregates.combination_id')
                ->where('nationa_exam_computed_aggregates.combination_id', $combination)
                ->whereYear('nationa_exam_computed_aggregates.created_at', date('Y'));
            $select = [
                'nationa_exam_computed_aggregates.*',
                'accr_schools_information.id AS school_id',
                'accr_schools_information.school_name',
                'accr_schools_information.province',
                'accr_schools_information.district',
                'accr_schools_information.sector',
                'accr_schools_information.cell',
                'accr_schools_information.village',
                'accr_schools_information.school_code',
                'accr_schools_information.school_status',
                'combinations.id AS Cid',
                'combinations.combination_name',
                'combinations.combination_code',
                'students.gender',
                'students.fname',
                'students.lname',
            ];

            if ($request->input('province') != null)
                $results->where('nationa_exam_computed_aggregates.province', '=', $request->get('province'));
            if ($request->input('district') != null)
                $results->where('nationa_exam_computed_aggregates.district', '=', $request->get('district'));
            if ($request->input('school_id') != null)
                $results->where('nationa_exam_computed_aggregates.school_id', '=', $request->get('school_id'));
            if ($request->input('combination_id') != null && $request->get('course_id') == null)
                $results->where('combinations.id', $request->get('combination_id'));
            if ($request->input('gender') != null)
                $results->where('students.gender', 'LIKE', $request->get('gender'));

            $results = $results->select($select);

            $studentR = $results;
            $students = $studentR->get();
            $mapping = $students->each(function ($key, $value) {
            });
            $students = $mapping->sortByDesc('total_marks');
//            $results = $results->get();
            $combination = Combination::find($combination);
            if ($combination) {
                $courses = $combination->courses;
            } else
                $courses = [];
            $view = "wda.nationalexams.partial.combination";
            $course = '';
            if ($request->has('course_id'))
                $course = $request->get('course_id');
            $url = true;
            return view('wda.nationalexams.post-query', compact('view', 'results', 'course', 'courses', 'students', 'url'));
        } elseif ($request->get('student_index') != null) {
            $request->request->add(['n' => 2]);
            $view = "wda.nationalexams.partial.student";
            $parms = $request->except(['_token', 'search']);
            $student = Student::where('acad_year', date('Y'))->where('index_number', $request->input('student_index'))->first();
            $results = NationalExamResult::where('student_index', $request->input('student_index'))->get();
            return view('wda.nationalexams.post-query', compact('view', 'student', 'results'));
        }
        $query = http_build_query($parms);
        $url = route('wda.nationalexams.get.search') . '?' . $query;
        return view('wda.nationalexams.post-query', compact('url', 'view'));
    }

    public function getResultsFromQueryGiven()
    {
        computingStudentsMarks();

        $request = request();
        if ($request->has('n') && $request->get('n') === '1') {
            $datas = DB::table('nationa_exam_computed_aggregates')
                ->join('students', 'students.index_number', '=', 'nationa_exam_computed_aggregates.student_index')
                ->join('combinations', 'combinations.id', '=', 'nationa_exam_computed_aggregates.combination_id')
//                ->join('old_courses', 'old_courses.id', '=', 'national_exam_result.course_id')
                ->join('accr_schools_information', 'accr_schools_information.school_code', '=', 'nationa_exam_computed_aggregates.school_id')
                ->whereYear('nationa_exam_computed_aggregates.created_at', date('Y'));
            $select = [
                'nationa_exam_computed_aggregates.*',
                'accr_schools_information.id AS school_id',
                'accr_schools_information.school_name',
                'accr_schools_information.province',
                'accr_schools_information.district',
                'accr_schools_information.sector',
                'accr_schools_information.cell',
                'accr_schools_information.village',
                'accr_schools_information.school_code',
                'accr_schools_information.school_status',
                'combinations.id AS Cid',
                'combinations.combination_name',
                'combinations.combination_code',
                'students.gender',
                'students.fname',
                'students.lname',
                'nationa_exam_computed_aggregates.total_aggregates AS aggregate',
                'nationa_exam_computed_aggregates.total_marks'
            ];
            if ($request->input('province') != null)
                $datas->where('nationa_exam_computed_aggregates.province', '=', $request->get('province'));
            if ($request->input('district') != null)
                $datas->where('nationa_exam_computed_aggregates.district', '=', $request->get('district'));
            if ($request->input('school_id') != null)
                $datas->where('nationa_exam_computed_aggregates.school_id', '=', $request->get('school_id'));
            if ($request->input('combination_id') != null && $request->get('course_id') == null)
                $datas->where('combinations.id', $request->get('combination_id'));
            if ($request->input('gender') != null)
                $datas->where('students.gender', 'LIKE', $request->get('gender'));
//            if ($request->get('course_id') != null)
//                $datas->where('old_courses.id', $request->get('course_id'));
            if ($request->input('student_index') != null)
                $datas->where('nationa_exam_computed_aggregates.student_index', $request->get('student_index'));
            $datas = $datas->select($select);
//            $datas->groupBy('national_exam_result.student_index');
            $datas = $datas->get();
            $mapping = $datas->each(function ($key, $value) {
            });
            $datas = $mapping->sortByDesc('total_marks');
            $dataTables = DataTables::of($datas)
                ->addColumn('names', function ($data) {
                    return ucwords($data->fname . " " . $data->lname);
                })
//                ->addColumn('aggregate', function ($data) {
//                    return getStudentAggregate($data->student_index);
//                })
                ->addColumn('decision', function ($data) {
                    $da = getStudentAggregate($data->student_index);
                    return calculateRemark($da);
                })
//                ->orderColumn('aggregate', '-aggregate $1')
                ->addIndexColumn()
                ->make(true);

            return $dataTables;
        }
    }

    public function overallReport()
    {
        //national level overallReport data
        $nnd = null;

        //per province and district
        $ppd = NationalExamResult::all();
        $overviews = ComputedAggregate::all();
        $basedDistrict = [];
        $fullData = [];
        if ($overviews) {
            $overviews = $overviews->each(function () {
            });
            $fullData = $overviews->sortByDesc('created_at');
            $basedDistrict = $overviews->unique('district');
        }
        return view('wda.nationalexams.overallreport', compact('ppd', 'nnd', 'basedDistrict', 'fullData'));
    }

    public  function  perProgram(Request $request){
        //national level stats data
        $nnd = null;

        //per province and district
        $ppd = NationalExamResult::all();
        $overviews = ComputedAggregate::all();
        $basedDistrict = [];
        $fullData = [];
        if ($overviews) {
            $overviews = $overviews->each(function () {
            });
            $fullData = $overviews->sortByDesc('created_at');
            $basedDistrict = $overviews->unique('district');
        }
        return view('wda.nationalexams.perprogram', compact('ppd', 'nnd', 'basedDistrict', 'fullData'));
    }

    public function registered(Request $request)
    {
        if ($request->details == true) {
            if ($request->has('level_id') && $request->has('school_id') && $request->has('program_code')) {
                $details = $request->details;
                $school_id = $request->school_id;
                $school = SchoolInformation::where('id', $school_id)->first();
                $school_name = $school->school_name;
                $program_code = $request->program_code;
                $_udata = Student::where('acad_year', date('Y'))->where('school', $request->school_id)->where('level_id', $request->level_id)->get();
                $data = $_udata->filter(function ($d) use ($program_code) {
                    return strposWrapper($d->index_number, $program_code, false);
                });
                return view('wda.nationalexams.registered', compact('data', 'details', 'school_id', 'program_code', 'school_name'));
            } elseif ($request->has('level_id') && $request->has('school_id')) {
                $details = $request->details;
                $school_id = $request->school_id;
                $school = SchoolInformation::where('id', $school_id)->first();
                $school_name = $school->school_name;
                $program_code = $request->program_code;
                $data = Student::where('acad_year', date('Y'))->where('school', $request->school_id)->where('level_id', $request->level_id)->get();
                return view('wda.nationalexams.registered', compact('details', 'school_id', 'program_code', 'data', 'school_name'));
            }
            abort(404);
        } else {
            $details = false;
            $columns = [];
            $comb = Combination::all();
            if ($comb)
                $columns = $comb->toArray();
//            dd($columns);
            $schools = SchoolInformation::all();
            $_l5_rtqf_id = Rtqf::where('level_name', 'Level Five')
                ->orwhere('level_name', 'Level 5')
                ->first();

            $rtqf_id = $_l5_rtqf_id ? $_l5_rtqf_id->id : null;

            $levels_5_id = [];
            if ($_l5_rtqf_id && $_l5_rtqf_id->levels)
                $levels_5_id = $_l5_rtqf_id->levels->pluck('id','school_id')->toArray();
//            $levels_5_id = $levels_5_id->toArray();
            $level_5_students = Student::whereIn('level_id', $levels_5_id)
                ->where('acad_year', date('Y'))->get();
//            $level_5_students = DB::table('students')
//                ->join('levels', 'levels.id', '=', 'students.level_id', 'inner')
//                ->join('rtqfs', 'rtqfs.id', '=', 'levels.rtqf_id', 'inner')
//                ->where('rtqfs.id', 'LIKE', $rtqf_id);
//
//            dd($level_5_students->get());
            return view('wda.nationalexams.registered', compact('details', 'columns', 'levels_5_id', 'schools', 'level_5_students'));
        }
    }

    public function getRegisteredData()
    {
        $columns = [];
        $comb = Combination::all();
        if ($comb)
            $columns = $comb->toArray();
//            dd($columns);
        $schools = DB::table('accr_schools_information')->select([
            'accr_schools_information.school_name',
            'accr_schools_information.province',
            'accr_schools_information.district',
            'accr_schools_information.school_code',
        ]);
        $_l5_rtqf_id = Rtqf::where('level_name', 'Level Five')
            ->orwhere('level_name', 'Level 5')
            ->first();
        $levels_5_id = collect();
        if ($_l5_rtqf_id && $_l5_rtqf_id->levels)
            $levels_5_id = $_l5_rtqf_id->levels->pluck('id','school_id')->toArray();

        dd($levels_5_id);
    }

}