<?php

namespace App\Http\Controllers\WdaAuth;

use App\District;
use App\RwandaBoundary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DistrictsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = District::all();
        $districts = RwandaBoundary::select('District')
            ->distinct()
            ->orderBy('District', "ASC")
            ->get();

        return view('wda.users.districts.index', compact('users', 'districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = District::findOrFail($id);
        $name = $user->name;
        $user->delete();

        return back()->with(['status' => '1', 'message' => $name.'\'s account deleted successfully . ']);
    }
}
