<?php

namespace App\Http\Controllers\WdaAuth;

use App\Http\Requests\CreateGrade;
use App\Http\Requests\UpdateGrade;
use App\WdaGrade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GradingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managegrade = null;
        $grade = null;
        $grades = WdaGrade::orderBy('grade_index','ASC')->get();
        return view('wda.grading.index',compact('grades','grade','managegrade'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGrade $request)
    {
        $check = WdaGrade::where('grade_index', $request->grade_index)->where('grade_name', $request->grade_name)->first();
        if ($check) {
            return redirect(route('wda.grading.index'))->with(['status' => '0', 'message' => "This Grade Already Available!."]);
        }

        $grade = new WdaGrade();
        $grade->grade_index = $request->grade_index;
        $grade->grade_name = $request->grade_name;
        $save = $grade->save();
        if ($save)
            return redirect(route('wda.grading.index'))->with(['status' => '1', 'message' => "Added Grade  Successfully."]);
        else
            return redirect(route('wda.grading.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Create Grade."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grades = WdaGrade::all();
        $managegrade = $grade = WdaGrade::where('id', $id)->first();
        if ($managegrade) {
            return view('wda.grading.index', compact('managegrade', 'grades', 'grade'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGrade $request, $id)
    {
        $managegrade = WdaGrade::where('id', $id)->first();
        if ($managegrade) {
            $managegrade->grade_index = $request->grade_index;
            $managegrade->grade_name = $request->grade_name;
            $save = $managegrade->save();
            if ($save)
                return redirect(route('wda.grading.index'))->with(['status' => '1', 'message' => "Grade Updated Successfully."]);
            else
                return redirect(route('wda.grading.index'))->withInput()
                    ->with(['status' => '0', 'message' => "Failed to update Grade."]);

        } else {
            return redirect(route('wda.grading.index'))->with(['status' => '0', 'message' => "Update Failed"]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grade = WdaGrade::where('id', $id)->first();
        if ($grade) {
            $grade->delete();
            return redirect(route('wda.grading.index'))->with(['status' => '1', 'message' => 'Grade removed successful']);
        } else
            abort(409);
    }
}
