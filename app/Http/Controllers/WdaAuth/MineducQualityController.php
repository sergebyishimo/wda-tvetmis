<?php

namespace App\Http\Controllers\WdaAuth;

use App\MineducQualityAuditIndicators;
use App\MineducQualityAuditReport;
use App\MineducQualityAuditSection;
use App\Model\Accr\AccrCriteria;
use App\Model\Accr\Functions;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SchoolRating;
use App\Model\Accr\SchoolType;
use App\Model\Accr\SourceSchoolOwnership;
use App\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class MineducQualityController extends Controller
{
    public function index()
    {
        return view('mineduc.viewschools');
    }

    public function createSchool()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schoolTypes = SchoolType::all();
        $schoolRating = SchoolRating::all();
        $schools = SchoolInformation::all();
        return view('mineduc.createschool', compact('schools', 'ownerships', 'schoolTypes', 'schoolRating'));
    }

    public function getAuditingReport()
    {
        $datas = DB::table('accr_schools_information')
            ->join('mineduc_quality_audit_reports', 'accr_schools_information.id', '=', 'mineduc_quality_audit_reports.school_id')
            ->join('accr_source_school_type','accr_schools_information.school_type','accr_source_school_type.id')
            ->groupBy('accr_schools_information.id');
        $select = [
            'accr_schools_information.id',
            'accr_schools_information.school_name',
            'accr_schools_information.district',
            'accr_source_school_type.school_type'
        ];

        $all_marks = MineducQualityAuditReport::all();
        $marks = [];
        foreach ($all_marks as $mark) {
            if (!isset($marks[$mark->school_id])) {
                $m = $mark->marks;
                $marks[$mark->school_id] = [$mark->section_id => (int)$m];
            } else {
                if (isset($marks[$mark->school_id][$mark->section_id]))
                    $marks[$mark->school_id][$mark->section_id] += $mark->marks;
                else
                    $marks[$mark->school_id][$mark->section_id] = (int)$mark->marks;

            }
        }
        $marks = collect($marks);
//        dd($marks);
        $datas = $datas->select($select);
//        return response()->json(['sql'=> $datas->toSql()]);
        $datas = $datas->get();

        $dataTables = DataTables::of($datas);
        $sections = MineducQualityAuditSection::all();
        foreach ($sections as $section) {
            $sectionID = $section->id;
            $dataTables->editColumn($section->id, function ($data) use($sectionID, $marks){
                if (isset($marks[$data->id][$sectionID]))
                    return $marks[$data->id][$sectionID];
                else
                    return '0';
            });
        }
        $dataTables->addColumn('overall',function($data) use($marks){
            if(isset($marks[$data->id])){
                $sum = 0;
                foreach ($marks[$data->id] as $sector => $mark){
                    $sum = $sum + $mark;
                }
                return $sum;
            }else
            {
                return 0;
            }
        });
        $dataTables->addColumn('ranking',function($data) use($marks){
            if(isset($marks[$data->id])){
                $sum = 0;
                foreach ($marks[$data->id] as $sector => $mark){
                    $sum = $sum + $mark;
                }
                switch ($sum){
                    case ($sum >=90 && $sum <= 100):
                        return "Excellent";
                    case ($sum >=80 && $sum <= 89.9):
                        return "Very Good";
                    case ($sum >=70 && $sum <= 79.9):
                        return "Good";
                    case ($sum >=50 && $sum <= 69.9):
                        return "Poor";
                    case ($sum >=0 && $sum <= 49.9):
                        return "Very Poor";
                    default:
                        return "Unknown";
                }
            }
        });

        $dataTables = $dataTables->addIndexColumn()->make(true);

        return $dataTables;
    }

    public function summary()
    {
        $sections = MineducQualityAuditSection::all();
        $url = route('wda.data.get.auditingdata');
        return view('mineduc.viewreport', compact('sections', 'url'));
    }

    public function qualitySection(Request $request)
    {
        $sections = MineducQualityAuditSection::all();
        if ($request->has('id'))
            $section = MineducQualityAuditSection::where('id', $request->id)->first();
        return view('mineduc.sections', compact('sections', 'section'));
    }

    public function storeQualitySection(Request $request)
    {
        $this->validate($request, [
            'section_name' => 'required|min:3'
        ]);
        $check = MineducQualityAuditSection::where('section_name', $request->section_name);
        if ($check->count() > 0)
            return back()->withInput()->withErrors(['name' => 'Already Exists !!']);
        $data = $request->except('_token');
        MineducQualityAuditSection::create($data);

        return back()->with(['status' => '1', 'message' => "Quality Audit Section Added Successfully."]);
    }

    public function updateQualitySection(Request $request, $id)
    {
        $this->validate($request, [
            'section_name' => 'required|min:3'
        ]);
        $data = $request->except('_token');
        $quality = MineducQualityAuditSection::findOrFail($id);
        $quality->update($data);

        return redirect()->route('wda.mineduc.quality.section.index')
            ->with(['status' => '1', 'message' => "Quality Section Updated Successfully."]);
    }

    public function destroyQualitySection(Request $request, $id)
    {
        $quality = MineducQualityAuditSection::findOrFail($id);
        $quality->delete();
        return redirect()->route('wda.mineduc.quality.section.index')
            ->with(['status' => '1', 'message' => "Quality Section Deleted Successfully."]);
    }

    #Quality Indicators

    public function qualityIndicator(Request $request)
    {
        $sections = MineducQualityAuditSection::all();
        $indicators = MineducQualityAuditIndicators::all();
        if ($request->has('id'))
            $indicator = MineducQualityAuditIndicators::where('id', $request->id)->first();
        return view('mineduc.indicators', compact('sections', 'indicators', 'indicator'));
    }

    public function storeQualityIndicator(Request $request)
    {
        $this->validate($request, [
            'section_id' => 'required',
            'indicator_name' => 'required|min:3',
            'grade1' => 'required',
            'grade2' => 'required',
            'grade3' => 'required',
            'grade4' => 'required',
            'grade5' => 'required',
            'evidences' => 'required'
        ]);
        $check = MineducQualityAuditIndicators::where('indicator_name', $request->indicator_name);
        if ($check->count() > 0)
            return back()->withInput()->withErrors(['name' => 'Already Exists !!']);
        $data = $request->except('_token');
        MineducQualityAuditIndicators::create($data);

        return back()->with(['status' => '1', 'message' => "Quality Audit Indicator Added Successfully."]);
    }

    public function updateQualityIndicator(Request $request, $id)
    {
        $this->validate($request, [
            'indicator_name' => 'required|min:3'
        ]);
        $data = $request->except('_token');
        $quality = MineducQualityAuditIndicators::findOrFail($id);
        $quality->update($data);

        return redirect()->route('wda.mineduc.quality.section.index')
            ->with(['status' => '1', 'message' => "Quality Indicator Updated Successfully."]);
    }

    public function destroyQualityIndicator(Request $request, $id)
    {
        $quality = MineducQualityAuditIndicators::findOrFail($id);
        $quality->delete();
        return redirect()->route('wda.mineduc.quality.section.index')
            ->with(['status' => '1', 'message' => "Quality Indicator Deleted Successfully."]);
    }


    public function qualityAudit(Request $request)
    {
        if ($request->isMethod("POST")) {
            foreach ($request->data as $sector => $arr) {
                foreach ($arr as $indicator => $value) {
                    MineducQualityAuditReport::create([
                        'school_id' => $request->school_id,
                        'section_id' => $sector,
                        'indicator_id' => $indicator,
                        'marks' => $value
                    ]);
                }
            }
            return redirect()->route('wda.mineduc.qualityaudit')
                ->with(['status' => '1', 'message' => 'Quality Audit Report Recorded Successful!']);
        }
        $sections = MineducQualityAuditSection::all();
        $schools = SchoolInformation::all();
        return view('mineduc.qualityaudit', compact('sections', 'schools'));
    }
}
