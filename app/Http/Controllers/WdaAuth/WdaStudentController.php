<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\RtqfSource;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\TrainingSectorsSource;
use App\Qualification;
use App\Rp\Province;
use App\StaffsInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use Illuminate\Support\Facades\DB;
use JavaScript;
use Yajra\DataTables\DataTables;

class WdaStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = false;
        $all_schools =  SchoolInformation::pluck('school_name','id');
        $qualifications = Qualification::pluck('qualification_title','id');
        $url = route('wda.data.get.studentdata');
        return view('wda.students.index',compact('url','search','total','students','all_schools','qualifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function  search(Request $request){
        $search = false;
        $all_schools =  SchoolInformation::pluck('school_name','id');
        $qualifications = Qualification::pluck('qualification_title','id');
        //build query params
        $parms = $request->except(['_token','search']);
        $query = http_build_query($parms);
        $url = route('wda.data.get.studentdata') . '?' . $query;
        return view('wda.students.index',compact('url','search','students','all_schools','qualifications'));
    }

    function  getStudentsData(){
        $datas = DB::table('students')
            ->join('accr_schools_information','students.school','=','accr_schools_information.id')
            ->join('levels','students.level_id','=','levels.id')
            ->join('rtqfs','levels.rtqf_id','=','rtqfs.id')
            ->join('departments','levels.department_id','=','departments.id')
            ->join('qualifications','departments.qualification_id','=','qualifications.id');
        $select = [
            'students.photo',
            'students.fname',
            'students.lname',
            'students.level_id',
            'students.gender',
            'students.reg_no',
            'students.acad_year',
            'qualifications.qualification_title',
            'rtqfs.level_name',
            'accr_schools_information.id',
            'accr_schools_information.school_name',
            'accr_schools_information.province',
            'accr_schools_information.district',
        ];

        if(request()->has('province')){
            $datas->where('students.province','=',request()->province);
        }
        if(request()->has('district')){
            $datas->where('students.district',request()->district);
        }
        if(request()->has('school_id')){
            //TODO checks if using accr_school_information can speed up proccess
            $datas->where('accr_schools_information.id',request()->school_id);
        }

        if(request()->has('combination_id')){
            //TODO skipped it for now
        }
        if(request()->has('course_id')){
            //TODO skipped it for now
        }
        if(request()->has('gender')){
            $datas->where('students.gender',request()->gender);
        }

        if(request()->has('acad_year')){
            $datas->where('students.acad_year',request()->acad_year);
        }else
        {
//            $datas->where('students.acad_year',date('Y'));
        }

        $datas = $datas->select($select);
//        return response()->json(['sql'=> $datas->toSql()]);
        $datas = $datas->get();
        $dataTables = DataTables::of($datas)
            ->editColumn('photo', function($data) {
                return '<img src="'.getStudentPhoto($data).'" alt="'.$data->fname.'" width="50px" height="50px">';
            })
            ->addColumn('names', function ($data) {
                return ucwords($data->fname . " " . $data->lname);
            })
            ->rawColumns(['photo'])
            ->addIndexColumn()
            ->make(true);
        return $dataTables;
    }

    public function summary(Request $request) {

        $chart_acad_year = date('Y');
        if ($request->chart_acad_year)
            $chart_acad_year = $request->chart_acad_year;

        $provinces = ['City of Kigali', 'Northern Province', 'Southern Province', 'Eastern Province', 'Western Province'];
        $males = [];
        $females = [];

        foreach ($provinces as $province) {
            $males[] = Student::join('accr_schools_information', 'accr_schools_information.id', 'students.school')->where('accr_schools_information.province', $province)->whereIn('students.gender', ['M', 'Male'])->where('students.acad_year', $chart_acad_year)->count();
            $females[] = Student::join('accr_schools_information', 'accr_schools_information.id', 'students.school')->where('accr_schools_information.province', $province)->whereIn('students.gender', ['F', 'Female'])->where('students.acad_year', $chart_acad_year)->count();
        }

        $provinces = ['Kigali', 'North', 'South', 'East', 'West'];

        JavaScript::put([
            'provinces' => $provinces,
            'provinces_males' => $males,
            'provinces_females' => $females,
        ]);

        $rtqfs = RtqfSource::all();
        $levels = [];
        $males = [];
        $females = [];

        foreach ($rtqfs as $level) {
            $levels[] = $level->level_name;
            $males[] = Student::join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.id', 'levels.rtqf_id')->where('accr_rtqf_source.level_name', $level->level_name)->whereIn('students.gender', ['M', 'Male'])->where('students.acad_year', $chart_acad_year)->count();
            $females[] = Student::join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.id', 'levels.rtqf_id')->where('accr_rtqf_source.level_name', $level->level_name)->whereIn('students.gender', ['F', 'Female'])->where('students.acad_year', $chart_acad_year)->count();
        }

        JavaScript::put([
            'levels' => $levels,
            'levels_males' => $males,
            'levels_females' => $females,
        ]);

        // Students per TVET sector per gender (for the current year / put year filter)

        $db_sectors = TrainingSectorsSource::all();
        $sectors = [];
        $males = [];
        $females = [];

        foreach ($db_sectors as $sector) {
            $sectors[] = $sector->field_code;
            $males[] = Student::join('levels', 'levels.id', 'students.level_id')->join('departments', 'departments.id', 'levels.department_id')->join('accr_curriculum_qualifications', 'accr_curriculum_qualifications.uuid', 'departments.qualification_id')->join('accr_training_sectors_source', 'accr_training_sectors_source.uuid','accr_curriculum_qualifications.sector_id')->where('accr_training_sectors_source.id', $sector->id)->whereIn('students.gender', ['M', 'Male'])->where('students.acad_year', $chart_acad_year)->toSql();
            $males[] = Student::join('levels', 'levels.id', 'students.level_id')->join('departments', 'departments.id', 'levels.department_id')->join('accr_curriculum_qualifications', 'accr_curriculum_qualifications.uuid', 'departments.qualification_id')->join('accr_training_sectors_source', 'accr_training_sectors_source.uuid','accr_curriculum_qualifications.sector_id')->where('accr_training_sectors_source.id', $sector->id)->whereIn('students.gender', ['F', 'Female'])->where('students.acad_year', $chart_acad_year)->count();
        }

        JavaScript::put([
            'sectors' => $sectors,
            'sectors_males' => $males,
            'sectors_females' => $females,
        ]);

        $acad_year = date('Y');
        if ($request->acad_year)
            $acad_year = $request->acad_year;

        $schools = SchoolInformation::select('id', 'school_name','school_type', 'province', 'district','school_status')->get();

        return view('wda.students.summary', compact('schools', 'acad_year', 'chart_acad_year'));
    }
}
