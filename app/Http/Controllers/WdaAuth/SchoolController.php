<?php

namespace App\Http\Controllers\WdaAuth;

use App\CostOfTraining;
use App\Model\Accr\Graduate;
use App\Model\Accr\QualificationLevel;
use App\Qualification;
use App\School;
use App\StaffsInfo;
use App\Student;
use App\Transformers\ListAllStaffTransformer;
use App\Transformers\ListAllSchoolTransformer;
use Illuminate\Support\Facades\DB;
use JavaScript;
use PhpOffice\PhpSpreadsheet\IOFactory;

use App\Department;
use App\Level;
use App\Mail\SchoolAccountSuccessfullyCreated;
use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SchoolRating;
use App\Model\Accr\SchoolType;
use App\Model\Accr\SourceSchoolOwnership;
use App\Model\Accr\TrainingSectorsSource;
use App\Model\Accr\TrainingTradesSource;
use App\SchoolSetting;
use App\SchoolUser;
use App\Sector;
use App\SubSector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Storage;
use Yajra\Datatables\Datatables;

class SchoolController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('wda');
    }

    public function view(Request $request)
    {
        $ownerships = SourceSchoolOwnership::all();
        $sectors = TrainingSectorsSource::all();
        $currs = CurriculumQualification::all();
        $sub_sectors = TrainingTradesSource::all();
        $schools = SchoolInformation::where('school_activity', 'Active')->get();
        $notactiveschools = SchoolInformation::where('school_activity', 'Not Active')->get();
        $suspendedschools = SchoolInformation::where('school_activity', 'Suspended')->get();
        $deps = Department::join('accr_schools_information', 'accr_schools_information.id', 'departments.school_id')->count();
        $parms = $request->except(['_token', 'search']);
        $query = http_build_query($parms);
        $url = route('wda.get.objectdata', ['type' => 'ss']) . "?" . $query;
        return view('wda.schools.view', compact('url', 'ownerships', 'sectors', 'sub_sectors', 'currs', 'schools', 'notactiveschools', 'suspendedschools', 'deps'));
    }

    public function archive(Request $request)
    {
        if ($request->has('restore')) {
            $id = $request->restore;
            SchoolInformation::withTrashed()->findOrFail($id)->restore();

            return back()->with(['status' => '1', 'message' => 'School Restored Successfully']);
        }

        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::onlyTrashed()->get();
        $sectors = TrainingSectorsSource::all();
        $sub_sectors = TrainingTradesSource::all();

        return view('wda.schools.archive', compact('schools', 'ownerships', 'sectors', 'sub_sectors'));
    }

    public function createSchool()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schoolTypes = SchoolType::all();
        $schoolRating = SchoolRating::all();
        $schools = SchoolInformation::all();
        $costoftraining = null;
        return view('wda.schools.create', compact('schools', 'ownerships', 'schoolTypes', 'schoolRating', 'costoftraining'));
    }

    public function uploadSchools(Request $request)
    {

        $file = $request->file;

        if ($file->getClientOriginalExtension() != 'xlsx' && $file->getClientOriginalExtension() != 'xls') {
            return redirect('/wda/school');
        }

        $fileTitle = $request->file->getClientOriginalNAme();
        $filename = $request->file->storeAs('schools_upload/', $fileTitle . ' ' . date('Ymd H:i') . '.xlsx');

        $inputFileName = storage_path('app/') . $filename;
        $objPHPExcel = IOFactory::load($inputFileName);


        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $skippedRows = [];
        $newSchools = 0;

        //  Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++) {

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $school_name = $rowData[0][0];
            $province = $rowData[0][1];
            $district = $rowData[0][2];
            $sector = $rowData[0][3];
            $cell = $rowData[0][4];
            $village = $rowData[0][5];
            $latitude = $rowData[0][6];
            $longitude = $rowData[0][7];
            $phone = $rowData[0][8];
            $email = $rowData[0][9];
            $school_status = $rowData[0][10];
            $manager_name = $rowData[0][11];
            $manager_phone = $rowData[0][12];
            $manager_email = $rowData[0][13];
            $accreditation_status = $rowData[0][14];
            $school_activity = $rowData[0][15];
            $school_code = $rowData[0][16];
            $school_type = $rowData[0][17];

            if ($school_name == null || $province == null || $district == null || $sector == null || $phone == null || $email == null || $school_status == null || $manager_name == null || $manager_phone == null || $manager_email == null || $accreditation_status == null || $school_activity == null || $school_code == null || $school_type == null) {
                $skippedRows[] = $row;
                continue;
            }

            $check = SchoolInformation::where('email', $email)->orWhere('phone', $phone)->count();
            if ($check > 0) {
                $skippedRows[] = $row;
                continue;
            }

            //get school type id
            $school_type_check = SchoolType::where('school_type', $school_type)->first();
            if ($school_type_check) {
                $school_type = $school_type_check->id;
            } else {
                $school_type = null;
            }

            $school = new SchoolInformation();

            $school->school_name = $school_name;
            $school->school_code = $school_code;
            $school->province = $province;
            $school->district = $district;
            $school->sector = $sector;
            $school->cell = $cell;
            $school->village = $village;
            $school->latitude = $latitude;
            $school->longitude = $longitude;
            $school->phone = $phone;
            $school->email = $email;
            $school->school_status = $school_status;
            $school->school_type = $school_type;
            $school->school_activity = $school_activity;
            $school->manager_name = $manager_name;
            $school->manager_phone = $manager_phone;
            $school->manager_email = $manager_email;
            $school->accreditation_status = $accreditation_status;

            $data = $school->save();
            if ($data) {
                if (!$request->school_id) {
                    $password = rand(100000, 1000000);
                    SchoolUser::create([
                        'name' => $school_name,
                        'email' => $email,
                        'password' => bcrypt($password),
                        'android_pass' => sha1($password),
                        'privilege' => 2,
                        'school_id' => $school->id
                    ]);

                    SchoolSetting::create([
                        'active_period' => 1,
                        'active_term' => 1,
                        'school' => $school->id
                    ]);

                    $data = [
                        'acronym' => $school_name,
                        'email' => $email,
                        'password' => $password
                    ];

                    Mail::to($data['email'])->send(new SchoolAccountSuccessfullyCreated($data));
                }
            }

            $newSchools++;
        }

        if (count($skippedRows) > 0) {
            return redirect()->route('wda.schools')
                ->with(['status' => '1', 'message' => 'New Schools :' . $newSchools . ' Skipped Rows : ' . implode(', ', $skippedRows)]);
        } else {
            return redirect()->route('wda.schools')
                ->with(['status' => '1', 'message' => 'New Schools : ' . $newSchools]);
        }

    }

    public function internet()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::where('has_internet', 2)->get();

        return view('wda.schools.view', compact('schools', 'ownerships'));
    }

    public function electricity()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::where('has_electricity', 2)->get();

        return view('wda.schools.view', compact('schools', 'ownerships'));
    }

    public function water()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::where('has_water', 2)->get();

        return view('wda.schools.view', compact('schools', 'ownerships'));
    }

    public function viewDetails($id)
    {
        $school = SchoolInformation::find($id);
        $students = SchoolInformation::find($id)->students()->paginate(50);
        $students->withPath('?tab=st');
        $staffs = getFromSchool($school->id, 'staffs', '2');
        return view('wda.schools.viewDetails', compact('school', 'students', 'staffs'));
    }

    public function deleteSchoolDepartments(Request $request)
    {
        $department = Department::where('school_id', $request->input('school'))
            ->where('id', $request->input('department'))->first();
        $countL = $department->levels->count();
        $countD = 0;
        foreach ($department->levels as $level) {
            if ($level->delete())
                $countD += 1;
        }
        if ($countL == $countD)
            $department->delete();

        return back()->with(['status' => 1, 'message' => 'Department deleted ...']);
    }

    public function uploadStaff()
    {

        $fileTitle = $request->attachment->getClientOriginalNAme();
        $filename = $request->attachment->storeAs('uploads/staffs', $fileTitle . ' ' . date('Ymd H:i') . '.xlsx');
        $inputFileName = storage_path('app/') . $filename;
        $objPHPExcel = IOFactory::load($inputFileName);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= 500; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $phone = trim($rowData[0][10]);
            $check = StaffsInfo::where('phone_number', $phone)->count();

            if ($check == 0) {
                $staff = new StaffsInfo();

                $staff->school_id = school(true)->id;
                $staff->first_name = $rowData[0][1];
                $staff->last_name = $rowData[0][2];
                $staff->gender = $rowData[0][3];
                $staff->staff_category = $rowData[0][4];
                $staff->sector = $rowData[0][5];
                $staff->sub_sector_taught = $rowData[0][6];
                $staff->modules_taught = $rowData[0][8];
                $staff->qualification_taught = $rowData[0][7];
                $staff->phone = $rowData[0][10];
                $staff->email = $rowData[0][11];
                $staff->nationality = $rowData[0][12];
                $staff->phone = $rowData[0][10];
                $staff->email = $rowData[0][11];
                $staff->nationality = $rowData[0][12];
                $staff->qualification = $rowData[0][13];
                // $staff->email = $rowData[0][14];
                $staff->institution = $rowData[0][15];
                $staff->emgraduated_yearail = $rowData[0][16];

                $staff->save();


            }

        }

        return redirect()->back()->with(['status' => 1, 'message' => 'Staff Uploaded Successfully!!']);
    }

    public function ViewStaffProfile($id, $staffid)
    {
        $school = SchoolInformation::find($id);
        $staffs = getFromSchool($school->id, 'staffs', '2');
        $staff = $staffs->where('id', $staffid)->first();
        if ($staff == null)
            return back();
        return view('wda.schools.viewstaffprofile', compact('school', 'staff'));
    }

    public function edit($id)
    {
        $ownerships = SourceSchoolOwnership::all();
        $school = SchoolInformation::findOrFail($id);
        $sectors = TrainingSectorsSource::all();
        $sub_sectors = TrainingTradesSource::all();
        $schools = SchoolInformation::all();
        $schoolTypes = SchoolType::all();
        $schoolRating = SchoolRating::all();
        $costoftraining = null;
        return view('wda.schools.edit', compact('school', 'ownerships', 'sectors', 'sub_sectors', 'schools', 'schoolTypes', 'schoolRating'));
    }

    public function save(Request $request)
    {

        if ($request->school_id) {
            $this->validate($request, [
                'email' => 'required|unique:accr_mysql.schools_information,email,' . $request->school_id,
                'school_code' => 'required|unique:accr_mysql.schools_information,school_code,' . $request->school_id
            ]);
            $school = SchoolInformation::find($request->school_id);
        } else {
            $this->validate($request, [
                'email' => 'required|unique:accr_mysql.schools_information',
                'school_code' => 'required|unique:accr_mysql.schools_information'
            ]);
            $school = new SchoolInformation();
        }

        $school->school_name = $request->school_name;
        $school->school_acronym = $request->school_acronym;
        $school->school_code = $request->school_code;
        $school->province = $request->province;
        $school->district = $request->district;
        $school->sector = $request->sector;
        $school->cell = $request->cell;
        $school->village = $request->village;
        $school->latitude = $request->latitude;
        $school->longitude = $request->longitude;
        $school->phone = $request->phone;
        $school->email = $request->email;
        $school->school_status = $request->school_status;
        $school->owner_name = $request->owner_name;
        $school->owner_phone = $request->owner_phone;
        $school->owner_type = $request->owner_type;
        $school->owner_email = $request->owner_email;
        $school->manager_name = $request->manager_name;
        $school->manager_phone = $request->manager_phone;
        $school->manager_email = $request->manager_email;

        $school->accreditation_status = $request->accreditation_status;
        $school->accreditation_number = $request->accreditation_number;
        $school->website = $request->website;
        $school->has_electricity = $request->has_electricity;
        $school->has_water = $request->has_water;
        $school->has_computer_lab = $request->has_computer_lab;
        $school->has_internet = $request->has_internet;
        $school->has_library = $request->has_library;
        $school->has_business_or_strategic_plan = $request->has_business_or_strategic_plan;
        $school->has_feeding_program = $request->has_feeding_program;
        $school->date_of_establishment = $request->date_of_establishment;
        $school->latitude = $request->latitude;
        $school->phone = $request->phone;
        $school->email = $request->email;
        $school->owner_name = $request->owner_name;
        $school->owner_phone = $request->owner_phone;
        $school->owner_type = $request->owner_type;
        $school->owner_email = $request->owner_email;
        $school->boarding_or_day = $request->boarding_or_day;
        $school->school_activity = $request->school_activity;
        //$school->school_type = $request->school_type;

        $path = '/schools/logos/';
        if ($request->file('school_logo')) {
            $photo = $request->file('school_logo');
            $photoP = $path;
            $logo = Storage::disk(config('voyager.storage.disk'))->put($photoP, $photo, 'public');
            $school->school_logo = $logo;
        }

        $school->school_rating = $request->school_rating;
        $school->school_type = $request->school_type;
        $school->number_of_desktops = $request->number_of_desktops;
        $school->number_of_classrooms = $request->number_of_classrooms;
        $school->students_males = $request->students_males;
        $school->staff_male = $request->staff_male;
        $school->staff_female = $request->staff_female;
        $school->number_of_generators = $request->number_of_generators;

        $school->number_of_computer_labs = $request->input('number_of_computer_labs');
        $school->number_of_smart_classrooms = $request->input('number_of_smart_classrooms');
        $school->number_of_laptops = $request->input('number_of_laptops');
        $school->number_of_positivo_laptops = $request->input('number_of_positivo_laptops');
        $school->has_three_phase_electricity = $request->input('has_three_phase_electricity');
        $school->number_of_incubation_centers = $request->input('number_of_incubation_centers');
        $school->type_of_internet_connection = $request->input('type_of_internet_connection');

        $data = $school->save();
        if ($data) {
            if (!$request->school_id) {
                $password = rand(100000, 1000000);
                SchoolUser::create([
                    'name' => $request->school_name,
                    'email' => $request->email,
                    'password' => bcrypt($password),
                    'android_pass' => sha1($password),
                    'user_type' => 'School Manager',
                    'privilege' => 2,
                    'school_id' => $school->id
                ]);

                SchoolSetting::create([
                    'active_period' => 1,
                    'active_term' => 1,
                    'school' => $school->id
                ]);

                $data = [
                    'acronym' => $request->school_name,
                    'email' => $request->email,
                    'password' => $password
                ];

                Mail::to($data['email'])->send(new SchoolAccountSuccessfullyCreated($data));
            }
            if ($request->cost_of_consumables_per_year || $request->budget_actually_available_for_salaries_per_year || $request->estimated_operational_budget_required_per_year || $request->estimated_total_salaries_year || $request->budget_available_for_consumables || $request->operational_funds_available_per_year) {
                $costoftraining = CostOfTraining::where('school_id', $school->id)
                    ->where('academic_year', $request->input('academic_year'))
                    ->first();
                if (!$costoftraining)
                    $costoftraining = new CostOfTraining();

                $costoftraining->school_id = $school->id;
                $costoftraining->trades_id = $request->input('trades_id');
                $costoftraining->cost_of_consumables_per_year = $request->input('cost_of_consumables_per_year');
                $costoftraining->estimated_total_salaries_year = $request->input('estimated_total_salaries_year');
                $costoftraining->budget_available_for_consumables = $request->input('budget_available_for_consumables');
                $costoftraining->operational_funds_available_per_year = $request->input('operational_funds_available_per_year');
                $costoftraining->academic_year = $request->input('academic_year');
                $costoftraining->save();
            }
        }

        return redirect()->route('wda.schools')
            ->with(['status' => '1', 'message' => 'School Saved Successfully!!']);
    }

    public function schoolQualification(Request $request)
    {

        $sector = Sector::findOrFail(getUUIDFrom('se', $request->r_i));
        $subsector = SubSector::findOrFail(getUUIDFrom('sb', $request->d_d));
        $school = SchoolInformation::findOrFail($request->o_o);
        $sectors = TrainingSectorsSource::all();
        $sub_sectors = TrainingTradesSource::all();
        if ($subsector) {
            $qualifications = $subsector->qualifications;

            return view("wda.schools.qualifications", compact('qualifications', 'subsector', 'sector', 'school', 'sectors', 'sub_sectors'));
        }
    }

    public function schoolDepartment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'qualification' => 'required'
        ], [
            'qualification' => 'Check at least one qualification'
        ]);

        if ($validator->fails())
            return back()->with(['status' => '0', 'message' => $validator->errors()->first()]);

        //        dd($request->all());

        $qualifications = $request->qualification;
        $rqtf = $request->rqtf;
        $school = $request->school;

        foreach ($qualifications as $qualification) {
            $check = Department::where('school_id', $school)->where('qualification_id', $qualification)->first();
            if (is_null($check)) {
                $data = [
                    'school_id' => $school,
                    'qualification_id' => $qualification,
                    'staffs_info_id' => null,
                    'status' => true,
                    'capacity_students' => 120,
                    'updateVersion' => updateVersionColumn('departments', false, $school)
                ];
                Department::create($data);
            }
            $dept = Department::where('school_id', $school)->where('qualification_id', $qualification)->first();
            if (isset($dept)) {
                if (isset($rqtf[$qualification])) {
                    foreach ($rqtf[$qualification] as $level) {
                        if ($level) {
                            $depID = $dept->id;
                            $check = Level::where('school_id', $school)->where('department_id', $depID)->where('rtqf_id', $level)->first();
                            if (is_null($check))
                                Level::create([
                                    'school_id' => $school,
                                    'department_id' => $depID,
                                    'rtqf_id' => trim($level),
                                    'updateVersion' => updateVersionColumn('levels', false, $school)
                                ]);
                        }
                    }
                }
            }
        }

        return back()->with(['status' => '1', 'message' => 'Save successfully.']);

    }

    public function delete(Request $request)
    {
        SchoolInformation::find($request->delete_school)->delete();
        $users = SchoolUser::where('school_id', $request->delete_school);
        if ($users->count() > 0) {
            foreach ($users->get() as $user) {
                $user->delete();
            }
        }
        return redirect()->route('wda.schools')->with(['status' => '1', 'message' => 'School Deleted Successfully!!']);
    }

    public function editAllSchools(Request $request)
    {

        if ($request->paginate) {
            $paginate = $request->paginate;
        } else {
            $paginate = 20;
        }

        $schoolTypes = SchoolType::all();
        $schools = SchoolInformation::orderBy('school_name', 'asc')->paginate($paginate);
        return view('wda.schools.editAll', compact('schools', 'schoolTypes', 'paginate'));
    }

    public function editAllSchoolsSave(Request $request)
    {

        $count = count($request->school_id);

        for ($i = 0; $i < $count; $i++) {

            $school = SchoolInformation::find($request->school_id[$i]);

            if ($school) {

                $school->school_code = $request->school_code[$i];
                $school->school_activity = $request->school_activity[$i];
                $school->school_name = $request->school_name[$i];
                $school->school_status = $request->school_status[$i];
                $school->manager_name = $request->manager_name[$i];
                $school->school_type = $request->school_type[$i];
                $school->phone = $request->phone[$i];
                $school->email = $request->email[$i];
                $school->accreditation_status = $request->accreditation_status[$i];
                $school->province = $request->province[$i];
                $school->district = $request->district[$i];
                $school->sector = $request->sector[$i];
                $school->latitude = $request->latitude[$i];
                $school->longitude = $request->longitude[$i];

                $school->save();

                $school_user = SchoolUser::where('school_id', $school->id)->where('privilege', 2)->first();
                if ($school_user) {
                    $school_user->email = $request->email[$i];
                    $school_user->save();
                }


            }
        }

        return back()->with(['status' => '1', 'message' => 'Saved successfully.']);

    }

    public function staffsList(Request $request)
    {
        $staffs = StaffsInfo::all();
        $search = false;
        $all_schools = SchoolInformation::pluck('school_name', 'id');
        $qualifications = Qualification::pluck('qualification_title', 'id');

        return view('wda.schools.staffs.browse', compact('staffs', 'search', 'all_schools', 'qualifications'));
    }

    public function staffsUpload(Request $request)
    {

        if ($request->id) {
            $school = School::where('id', $request->id)->first();
            if ($school)
                return view('wda.schools.staffs.upload', compact('school'));
        }

        return view('wda.schools.staffs.upload');
    }

    public function staffsUploadSave(Request $request)
    {

        if ($request->school_id) {
            $fileTitle = $request->uploadedFile->getClientOriginalNAme();
            $filename = $request->uploadedFile->storeAs('uploads/students', $fileTitle . ' ' . date('Ymd H:i') . '.xlsx');
            $inputFileName = storage_path('app/') . $filename;
            $objPHPExcel = IOFactory::load($inputFileName);

            $sheet = $objPHPExcel->getSheet(4);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $uploaded = 0;
            $skipped = 0;
            $skippedBoth = 0;
            for ($row = 4; $row <= 300; $row++) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                if ($rowData[0][2] == null && $rowData[0][3] == null) {
                    continue;
                }

                $first_name = $rowData[0][2];
                $last_name = $rowData[0][3];
                $gender = ($rowData[0][4] == 'Male' || $rowData[0][4] == 'Female') ? $rowData[0][4] : null;
                $category = $rowData[0][5];
                $phone = ($rowData[0][11] == null || $rowData[0][11] == '' || strlen($rowData[0][11]) < 9) ? null : '0' . substr($rowData[0][11], -9);
                $email = $rowData[0][12];

                $nationality = $rowData[0][13];
                $qualification = $rowData[0][14];
                $qualification_level = $rowData[0][15];
                $institution = $rowData[0][16];
                $graduated_year = $rowData[0][17];

                $qua_level = QualificationLevel::where('name', $qualification_level)->first();

                if ($email == null || $email == '')
                    $check = StaffsInfo::where('phone_number', $phone)->count();
                elseif ($phone == null || $phone == '')
                    $check = StaffsInfo::where('email', $email)->count();
                else
                    $check = StaffsInfo::where('phone_number', $phone)->orwhere('email', $email)->count();

                if (($phone == null || $phone == '') && ($email == null || $email == '')) {
                    $skippedBoth++;
                    continue;
                }

                if ($check == 0) {
                    StaffsInfo::create(
                        [
                            'school_id' => $request->school_id,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'staff_category' => $category,
                            'phone_number' => $phone,
                            'gender' => $gender,
                            'email' => $email,
                            'nationality' => $nationality,
                            'qualification_level' => $qua_level ? $qua_level->id : null,
                            'qualification' => $qualification,
                            'institution' => $institution,
                            'graduated_year' => $graduated_year
                        ]
                    );
                    $uploaded++;
                } else {
                    $skipped++;
                    continue;
                }

            }

            return redirect()->back()->with(['status' => 1, 'message' => 'Uploaded : ' . $uploaded . "<br>Already Exists : " . $skipped . "<br>Without Phone or E-mail :" . $skippedBoth]);
        }
        return redirect()->back();
    }

    public function staffsSummary()
    {
        $provinces = ['City of Kigali', 'Northern Province', 'Southern Province', 'Eastern Province', 'Western Province'];
        $statistics = [];

        foreach ($provinces as $province) {
            $statistics[] = StaffsInfo::join('accr_schools_information', 'accr_schools_information.id', 'staffs_info.school_id')->where('accr_schools_information.province', $province)->count();
        }

        $provinces = ['Kigali', 'North', 'South', 'East', 'West'];

        $administrative = StaffsInfo::where('staff_category', 'Administrative')->count();
        $academic = StaffsInfo::where('staff_category', 'Academic')->count();

        $qualification_levels = QualificationLevel::all();

        $level_stats = [];
        $levels = [];
        foreach ($qualification_levels as $level) {
            $levels[] = $level->name;
            $level_stats[] = StaffsInfo::where('qualification_level', $level->id)->count();
        }

        JavaScript::put([
            'provinces' => $provinces,
            'statistics' => $statistics,
            'category' => [$administrative, $academic],
            'levels' => $levels,
            'level_stats' => $level_stats
        ]);

        $schools = SchoolInformation::select('id', 'school_name', 'school_type', 'province', 'district', 'school_status')->get();

        return view('wda.schools.staffs.summary', compact('schools'));
    }

    /**
     * @param Datatables $datatables
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function data(Datatables $datatables, $type)
    {
        switch ($type) {
            case 's':
                // $jmodel = StaffsInfo::query()->with('school');
                $jmodel = StaffsInfo::join('accr_schools_information', 'accr_schools_information.id', 'staffs_info.school_id');

                $dd = $datatables->eloquent($jmodel)
                    ->setTransformer(new ListAllStaffTransformer())
                    ->rawColumns(['photo', 'names', 'actions'])
                    ->smart(false)
                    ->make(true);
//                dd($dd);
                return $dd;

                break;
            case 'ss' :

                $jmodel = SchoolInformation::query();

                $schools = DB::table('accr_schools_information')
                    ->join('accr_source_school_type', 'accr_schools_information.school_type', '=', 'accr_source_school_type.id')->where('accr_schools_information.school_activity', 'Active');
                if (request()->input('province') && !request()->input('district')) {
                    $schools->where('accr_schools_information.province', request()->input('province'));
                }
                if (request()->input('district') && !request()->input('sector')) {
                    $schools->where('accr_schools_information.district', request()->input('district'));
                }
                if (request()->input('sector')) {
                    $schools->where('accr_schools_information.sector', request()->input('sector'));
                }
                if (request()->input('schooltype')) {
                    $schools->where('accr_source_school_type.school_type', request()->input('schooltype'));
                }
                if (request()->input('accreditation')) {
                    $schools->where('accr_schools_information.accreditation_status', request()->accreditation);
                }

                $schools->select([
                    'accr_schools_information.id',
                    'accr_schools_information.school_name',
                    'accr_schools_information.school_acronym',
                    'accr_schools_information.province',
                    'accr_schools_information.district',
                    'accr_schools_information.accreditation_status',
                    'accr_schools_information.school_activity',
                    'accr_schools_information.school_status',
                    'accr_schools_information.phone',
                    'accr_schools_information.email',
                    'accr_source_school_type.school_type',
                ]);

//                $dd = $datatables->eloquent($jmodel)
//                    ->setTransformer(new ListAllSchoolTransformer())
//                    ->make(true);
//                dd($dd);
                return Datatables::of($schools)
                    ->addColumn('action', function ($school) {
                        $action = '<form method="POST">' . csrf_field() .
                            '<input type="hidden" name="_method" value="delete"> 
                            <input type="hidden" name="delete_school" value="' . $school->id . '"> 
                            <a href="' . route('wda.view.school', $school->id) . '" 
                            title="View More" 
                            class="btn btn-warning btn-sm btn-flat">
                            <i class="fa fa-list"></i>
                            </a>
                            <a title="Browsing" 
                            href="/schools/' . $school->school_acronym . '" target="_blank" style="color: #333"
                            class="btn btn-warning btn-sm btn-flat">
                            <i class="fa fa-server"></i>
                            </a> 
                            <a href="' . route('wda.edit.school', $school->id) . '" title="Edit" 
                            class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a> 
                            <button type="button" class="btn btn-primary btn-sm ml-0" data-school="' . $school->id . '" data-school-name="' . $school->school_name . '" data-toggle="modal" data-target="#assignQualifications"> <i class="fa fa-anchor"></i> </button> <button type="submit" class="btn btn-danger btn-sm pull-right btn-flat" title="Delete" onclick="return window.confirm("Are you sure you want to remove this school:\n ' . $school->school_name . '?)"> 
                            <i class="fa fa-trash"></i> </button>
                            <a href="' . route('wda.staff.upload', ['id' => $school->id]) . '" class="btn btn-success btn-sm btn-flat"><i class="fa fa-upload"></i></a>  
                            </form>'
                            . '<a title="View Certificate" 
                            href="' . route('wda.view.accreditation.certificate', $school->id) . '" target="_blank"
                            class="btn btn-dark btn-sm btn-flat">
                            <i class="fa fa-magic"></i>
                            </a> 
                            ';
                        return $action;
                    })
//                    ->editColumn('school_name', function ($school) {
//                        $name = '<a href="/schools/'.$school->school_acronym.'" target="_blank" style="color: #333">' . $school->school_name .'</a>';
//                        return $name;
//                    })
                    ->editColumn('id', 'ID: {{$id}}')
                    ->make(true);

                break;

            case 'schools_qualifications':
                $schools = DB::table('accr_schools_information')
                    ->leftjoin('accr_source_school_type', 'accr_schools_information.school_type', 'accr_source_school_type.id')
                    ->join('departments', 'accr_schools_information.id', 'departments.school_id')
                    ->join('accr_curriculum_qualifications', 'departments.qualification_id', 'accr_curriculum_qualifications.uuid')
                    ->join('accr_training_sectors_source', 'accr_curriculum_qualifications.sector_id', 'accr_training_sectors_source.id')
                    ->join('accr_training_trades_source', 'accr_curriculum_qualifications.sub_sector_id', 'accr_training_trades_source.id')
                    ->join('accr_rtqf_source', 'accr_curriculum_qualifications.rtqf_level_id', 'accr_rtqf_source.id');

                return Datatables::of($schools)
                    ->make(true);
                break;
        }
    }

    public function ajaxData()
    {
        $schools = SchoolInformation::orderBy('school_name', 'ASC')->get();
        if ($schools)
            return response()->json($schools->pluck('school_name', 'id'));

        return response()->json([]);
    }

    public function deleteStudents(Request $request)
    {

        if ($request->level_id) {
            Student::where('level_id', $request->level_id)->delete();
            return redirect()->back()->with(['status' => 1, 'message' => 'Students Deleted Successfully!']);
        }

        return redirect()->back()->with(['status' => 1, 'message' => 'Students Deleted Successfully!']);
    }

    public function deleteStaffs(Request $request)
    {
        if ($request->school_id) {
            StaffsInfo::where('school_id', $request->school_id)->delete();
            return redirect()->back()->with(['status' => 1, 'message' => 'Staffs Deleted Successfully!']);
        }

        return redirect()->back()->with(['status' => 1, 'message' => 'Students Deleted Successfully!']);
    }

    public function deleteAllStudents(Request $request)
    {
        if ($request->school_id) {
            Student::where('school', $request->school_id)->delete();
            return redirect()->back()->with(['status' => 1, 'message' => 'Deleted Students Successfully']);
        }
        return redirect()->back()->with(['status' => 0, 'message' => 'An error occurred']);

    }

    public function deleteAllGraduates(Request $request)
    {
        if ($request->school_id) {
            Graduate::where('school_id', $request->school_id)->delete();
            return redirect()->back()->with(['status' => 1, 'message' => 'Deleted Graduates Successfully']);
        }
        return redirect()->back()->with(['status' => 0, 'message' => 'An error occurred']);
    }

    public function assigned()
    {

        $errors = [];

        $check_quali = DB::select("SELECT s.* FROM staging_students_2 s WHERE (s.sector_studied, s.sub_sector_studied, s.rtqf_level_studied) NOT IN ( SELECT sector_id, sub_sector_id, rtqf_level_id FROM accr_curriculum_qualifications)");

        if (count($check_quali) > 0) {
            $message = "These Qualifications Doesn't Exists : ";
            //dd($check_quali);
            $errors = $check_quali;
            return view('college.errors', compact('errors', 'message'));
        }

        $students = DB::table('staging_students_2')->select()->get();


        foreach ($students as $student) {
            $qua = CurriculumQualification::where('sector_id', $student->sector_studied)->where('sub_sector_id', $student->sub_sector_studied)->where('rtqf_level_id', $student->rtqf_level_studied)->count();

            if ($qua > 1)
                dd('Qua ' . $student->sector_studied . ' ' . $student->sub_sector_studied . ' ' . $student->rtqf_level_studied);
            else
                $qua = $qua = CurriculumQualification::where('sector_id', $student->sector_studied)->where('sub_sector_id', $student->sub_sector_studied)->where('rtqf_level_id', $student->rtqf_level_studied)->first();

            if ($qua) {
                $department = Department::where('school_id', $student->school_name)->where('qualification_id', $qua->uuid)->first();
                if (!$department) {
                    $errors[] = $student;
                } else {
                    $level = Level::where('department_id', $department->id)->where('school_id', $student->school_name)->first();
                    if (!$level) {
                        $errors[] = $student;
                    }
                }
            } else {
                dd('Qua Down ' . $student->sector_studied . ' ' . $student->sub_sector_studied . ' ' . $student->rtqf_level_studied);
            }
        }

        $message = "These Qualifications are not assigned : ";
        //dd($errors);

        return view('college.errors', compact('errors', 'message'));
    }

    public function migrate()
    {
        $students = DB::select("SELECT staging_students_2.* FROM staging_students_2 WHERE merged = 0 OR merged IS null LIMIT 1000");

        $students_count = count($students);
        $migrated = 0;

        foreach ($students as $student) {

            $qua = CurriculumQualification::where('sector_id', $student->sector_studied)->where('sub_sector_id', $student->sub_sector_studied)->where('rtqf_level_id', $student->rtqf_level_studied)->first();

            if ($qua) {
                $dep = Department::where('school_id', $student->school_name)->where('qualification_id', $qua->uuid)->first();

                if ($dep) {

                    $level = Level::where('department_id', $dep->id)->where('school_id', $student->school_name)->first();

                    if ($level) {
                        Student::create([
                            'reg_no' => null,
                            'fname' => $student->fname,
                            'lname' => $student->lname,
                            'gender' => $student->gender,
                            'mode' => $student->mode,
                            'school' => $student->school_name,
                            'level_id' => $level->id,
                            'acad_year' => $student->academic_year,
                            'ft_name' => $student->ft_name,
                            'ft_phone' => $student->ft_phone,
                            'mt_name' => $student->mt_name,
                            'mt_phone' => $student->mt_phone,
                            'ubudehe' => $student->ubudehe,
                        ]);
                        DB::update('update staging_students_2 set merged = 1 where id = ?', [$student->id]);
                        $migrated++;
                    }
                }
            }

        }

        return compact('students_count', 'migrated');

    }
}
