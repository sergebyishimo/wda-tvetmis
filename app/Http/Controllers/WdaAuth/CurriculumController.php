<?php

namespace App\Http\Controllers\WdaAuth;

use App\Department;
use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\RtqfSource;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\TrainingSectorsSource;
use App\Model\Accr\TrainingTradesSource;
use App\Module;
use App\ModuleType;
use App\Qualification;
use App\QualificationRtqf;
use App\Rtqf;
use App\Sector;
use App\SubSector;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use TCG\Voyager\Models\Category;
use Yajra\DataTables\DataTables;

class CurriculumController extends Controller
{
    public function __construct()
    {

    }

    public function sectors($id = null, Request $request)
    {
        $this->middleware('wda');
        if ($request->delete_sector) {
            $sector = TrainingSectorsSource::findOrFail($request->delete_sector);
            $uuid = $sector->uuid;
            $sector->delete();

            $sSector = Sector::find($uuid);
            if ($sSector)
                $sSector->delete();

            return redirect()
                ->route('wda.sectors.index')
                ->with(['status' => '1', 'message' => 'Sector Deleted Successfully!!']);
        }

        if ($request->sector) {
            if ($request->sector_id) {
                $sector = TrainingSectorsSource::findOrFail($request->sector_id);
                $sSector = Sector::find($sector->uuid);
            } else {
                $sector = new TrainingSectorsSource();
                $sSector = Sector::withTrashed()->where('sector_name', 'LIKE', '%' . $request->sector . '%')->first();;
                if (!$sSector)
                    $sSector = new Sector();
                else {
                    if ($sSector->deleted_at != null)
                        $sSector->deleted_at = null;
                }
            }

            $sector->tvet_field = $request->sector;
            $sector->field_code = strtoupper(trim($request->code)) ?: null;
            $saveS = $sector->save();

            if ($saveS) {
                if ($request->sector_id) {
                    if (is_null($sSector))
                        $sSector = new Sector();
                }
                $sSector->sector_name = $sector->tvet_field;
                $sSector->acronym = trim($sector->field_code) ?: null;
                $Ssave = $sSector->save();
                if ($Ssave) {
                    $update = TrainingSectorsSource::find($sector->id);
                    if ($update) {
                        $update->uuid = $sSector->id;
                        $update->save();
                    }
                }
            }

            return redirect()
                ->route('wda.sectors.index')
                ->with(['status' => '1', 'message' => 'Sector Saved Successfully!!']);
        }

        $sectors = TrainingSectorsSource::all();

        if ($id != null) {
            $sector = TrainingSectorsSource::find($id);

            return view('wda.curricula.sectors', compact('sectors', 'sector'));
        }

        return view('wda.curricula.sectors', compact('sectors'));
    }

    public function trades($id = null, Request $request)
    {
        $this->middleware('wda');
        if ($request->delete_trade) {
            $subSector = TrainingTradesSource::find($request->delete_trade);
            $uuid = $subSector->uuid;
            $subSector->delete();

            $sSubSector = SubSector::find($uuid);
            if ($sSubSector)
                $sSubSector->delete();

            return redirect()
                ->route('wda.trades.index')
                ->with(['status' => '1', 'message' => 'Trade Deleted Successfully!!']);
        }

        if ($request->trade) {
            if ($request->trade_id) {
                $trade = TrainingTradesSource::findOrFail($request->trade_id);
                $subSector = SubSector::find($trade->uuid);
            } else {
                $trade = new TrainingTradesSource();
                $subSector = SubSector::withTrashed()->where('sub_sector_name', 'LIKE', '%' . $request->trade . '%')->first();
                if (!$subSector)
                    $subSector = new SubSector();
                else {
                    if ($subSector->deleted_at != null)
                        $subSector->deleted_at = null;
                }
            }

            $trade->sector_id = $request->sector_id;
            $trade->sub_field_name = $request->trade;
            $trade->sub_field_code = $request->code;
            $t = $trade->save();
            if ($t) {
                if ($request->sector_id) {
                    if (is_null($subSector))
                        $subSector = new SubSector();
                }
                $subSector->sector_id = getUUIDFrom('se', $trade->sector_id);
                $subSector->sub_sector_name = $trade->sub_field_name;
                $subSector->acronym = $trade->sub_field_code;

                $save = $subSector->save();

                if ($save) {
                    $update = TrainingTradesSource::find($trade->id);
                    if ($update) {
                        $update->uuid = $subSector->id;
                        $update->save();
                    }
                }
            }

            return redirect()
                ->route('wda.trades.index')
                ->with(['status' => '1', 'message' => 'Trade Saved Successfully!!']);
        }

        $sectors = TrainingSectorsSource::all();
        $trades = TrainingTradesSource::all();

        if ($id != null) {
            $trade = TrainingTradesSource::find($id);

            return view('wda.curricula.trades', compact('sectors', 'trades', 'trade'));
        }

        return view('wda.curricula.trades', compact('sectors', 'trades'));
    }

    public function rtqfs($id = null, Request $request)
    {
        $this->middleware('wda');
        if ($request->delete_rtqf) {
            $rtqf = RtqfSource::find($request->delete_rtqf);
            $uuid = $rtqf->uuid;
            $rtqf->delete();

            $sRtqf = Rtqf::find($uuid);
            if ($sRtqf)
                $sRtqf->delete();

            return redirect()
                ->route('wda.rtqfs.index')
                ->with(['status' => '1', 'message' => 'RTQF Deleted Successfully!!']);
        }

        if ($request->level) {
            if ($request->rtqf_id) {
                $rtqf = RtqfSource::find($request->rtqf_id);
                $sRtqf = Rtqf::find($rtqf->uuid);
            } else {
                $rtqf = new RtqfSource();
                $sRtqf = Rtqf::withTrashed()->where('level_name', 'LIKE', '%' . $request->level . '%')->first();
                if (!$sRtqf)
                    $sRtqf = new Rtqf();
                else {
                    if ($sRtqf->deleted_at != null)
                        $sRtqf->deleted_at = null;
                }
            }

            $rtqf->level_name = $request->level;
            $rtqf->level_description = $request->description;
            $rtqf->qualification_type = $request->qualification_type;
            $rSave = $rtqf->save();

            if ($rSave) {
                if ($request->rtqf_id) {
                    if (is_null($sRtqf))
                        $sRtqf = new Rtqf();
                }

                $sRtqf->level_name = $rtqf->level_name;
                $sRtqf->level_description = trim($rtqf->level_description) ?: null;

                $Ssave = $sRtqf->save();
                if ($Ssave) {
                    $update = RtqfSource::find($rtqf->id);
                    if ($update) {
                        $update->uuid = $sRtqf->id;
                        $update->save();
                    }
                }
            }

            return redirect()
                ->route('wda.rtqfs.index')
                ->with(['status' => '1', 'message' => 'RTQF Level Saved Successfully!!']);
        }


        $rtqfs = RtqfSource::all();

        if ($id != null) {
            $rtqf = RtqfSource::find($id);

            return view('wda.curricula.rtqfs', compact('rtqfs', 'rtqf'));
        }

        return view('wda.curricula.rtqfs', compact('rtqfs'));
    }

    public function curriculum($id = null, Request $request)
    {
        $this->middleware('wda');
        if ($request->delete_curriculum) {
            $qua = CurriculumQualification::find($request->delete_curriculum);
            $uuid = $qua->uuid;
            $qua->delete();

            $quaS = Qualification::find($uuid);
            if ($quaS)
                $quaS->delete();

            return redirect()
                ->route('wda.curriculum.index')
                ->with(['status' => '1', 'message' => 'Curriculum Deleted Successfully!']);
        }

        if ($request->trade_id) {
            $validator = Validator::make($request->all(), [
                'attachment' => 'nullable|mimes:jpeg,jpg,pdf'
            ]);
            if ($validator->fails())
                return back()->withInput()->with(['status' => '0', 'message' => $validator->errors()->all()]);

            if ($request->curriculum_id) {
                $curriculum = CurriculumQualification::findOrFail($request->curriculum_id);
                $quaS = Qualification::find($curriculum->uuid);
            } else {
                $curriculum = new CurriculumQualification();
                $quaS = Qualification::withTrashed()->where('qualification_title', 'LIKE', '%' . $request->title . '%')->first();
                if (!$quaS)
                    $quaS = new Qualification();
                else {
                    if ($quaS->deleted_at != null)
                        $quaS->deleted_at = null;
                }
            }

            $curriculum->qualification_code = $request->code;
            $curriculum->qualification_title = $request->title;
            $curriculum->sector_id = $request->sector_id;
            $curriculum->sub_sector_id = $request->trade_id;
            $curriculum->rtqf_level_id = $request->rtqf_level_id;
            $curriculum->credits = $request->credits;
            $curriculum->release_date = $request->release_date;
            $curriculum->status = $request->status;
            $curriculum->description = $request->description;
            $curriculum->occupation_profile = $request->occupation_profile;
            $curriculum->is_cbt = $request->is_cbt;

            if (count($request->jobs) > 0) {
                $curriculum->job_titles = implode(', ', $request->jobs);
            }


//            $curriculum->job_related_information        = $request->job_related_information;
//            $curriculum->entry_requirements             = $request->entry_requirements;
//            $curriculum->information_about_pathways     = $request->information_about_pathways;
//            $curriculum->employability_and_life_skills  = $request->employability_and_life_skills;
//            $curriculum->qualification_arrangement      = $request->qualification_arrangement;
//            $curriculum->competency_standards           = $request->competency_standards;
//            $curriculum->training_manual                = $request->training_manual;
//            $curriculum->trainees_manual                = $request->trainees_manual;
//            $curriculum->trainers_manual                = $request->trainers_manual;

            if ($request->file('attachment')) {
                $path = '/qualifications/attachments/' . date('FY');
                $file = $request->file('attachment');
                $fileP = $path;
                $file = Storage::disk(config('voyager.storage.disk'))->put($fileP, $file, 'public');
//                $path = $request->file('attachment')->store('curr_attachments');
                $curriculum->attachment = $file;
            }

            $save = $curriculum->save();

            if ($save) {
                if ($request->curriculum_id) {
                    if (is_null($quaS))
                        $quaS = new Qualification();
                }

                $quaS->qualification_code = $curriculum->qualification_code;
                $quaS->qualification_title = $curriculum->qualification_title;
                $quaS->sector_id = getUUIDFrom('se', $curriculum->sector_id);
                $quaS->sub_sector_id = getUUIDFrom('sb', $curriculum->sub_sector_id);
                $quaS->credits = $curriculum->credits;
                $quaS->release_date = $curriculum->release_date;
                $quaS->status = $curriculum->status;
                $quaS->description = $curriculum->description;

                $Ssave = $quaS->save();

                if ($Ssave) {
                    $curriculum->uuid = $quaS->id;
                    $curriculum->save();
                    if (is_array($request->rtqf_id) && !empty($request->rtqf_id)) {
                        $quaS->rtqfs()->detach();
                        foreach ($request->rtqf_id as $id) {
                            $pivot = new QualificationRtqf();
                            $pivot->qualification_id = $quaS->id;
                            $pivot->rtqf_id = getUUIDFrom('rt', $id);
                            $pivot->save();
                        }
                    }
                }

            }


            return redirect()
                ->route('wda.curriculum.index')
                ->with(['status' => '1', 'message' => 'Curriculum Saved Successfully!!']);
        }

        $sectors = TrainingSectorsSource::all();
        $Sub_sectors = TrainingTradesSource::all();
        $rtqfs = RtqfSource::all();

        $currs = new CurriculumQualification();
        if (request()->input('s_sector') && !request()->input('s_subsector')) {
            $currs = $currs->where('sector_id', request()->input('s_sector'));
        }
        if (request()->input('s_subsector') && !request()->input('s_rtqf')) {
            $currs = $currs->where('sub_sector_id', request()->input('s_subsector'));
        }
        if (request()->input('s_rtqf')) {
            $currs = $currs->where('rtqf_level_id', request()->input('s_rtqf'));
        }

        $currs = $currs->get();

        if ($id != null) {
            $curr_info = CurriculumQualification::find($id);
            $currs = [];
            return view('wda.curricula.curriculum', compact('sectors', 'rtqfs', 'currs', 'Sub_sectors', 'curr_info'));
        }

        if(request()->has(['s_sector','s_subsector','s_rtqf'])) {
            return view('wda.curricula.curriculum', compact('sectors', 'Sub_sectors', 'rtqfs', 'currs'));
        } else {
            $currs = [];
            return view('wda.curricula.curriculum', compact('sectors', 'Sub_sectors', 'rtqfs', 'currs'));
        }

    }

    public function SchoolsView($id)
    {
        $curr = CurriculumQualification::where('id', $id)->first();
        if (!$curr)
            return abort(404);
        $schools = [];
        $students = [];
        $despts = getQualificationSchools($id, false);
        if ($despts) {
            foreach ($despts as $despt) {
                if ($despt->school)
                    $schools[] = $despt->school;
            }
        }
        return view('wda.curricula.curricula_school', compact('schools', 'curr'));
    }

    public function StudentsView($id, $school = null)
    {
        $curr = CurriculumQualification::where('id', $id)->first();
        if (!$curr)
            return abort(404);
        $students = getQualificationStudents($id, false, $school);
        if ($students->count() <= 0)
            $students = [];
        if ($school != null) {
            $school = SchoolInformation::where('id', $school)->first();
            if (!$school)
                abort(404);
        }
        $url = route('wda.curricula.get.students', [$curr->uuid, $school ?: 'n']);
        return view('wda.curricula.curricula_students', compact('students', 'curr', 'school', 'url'));
    }

    public function getStudentsOfQualification($q, $s)
    {
        $qualification = $q;
        $school = $s != 'n' ? $s : null;
        $query = DB::table('students')
            ->join('levels', 'students.level_id', '=', 'levels.id')
            ->join('rtqfs', 'levels.rtqf_id', '=', 'rtqfs.id')
            ->join('departments', 'levels.department_id', '=', 'departments.id')
            ->join('accr_curriculum_qualifications', 'departments.qualification_id', '=', 'accr_curriculum_qualifications.uuid')
            ->join('accr_schools_information', 'departments.school_id', '=', 'accr_schools_information.id')
            ->where('accr_curriculum_qualifications.uuid', $qualification);
        if ($school)
            $query = $query->where('accr_schools_information.id', $school);
        $query = $query->select([
            'students.photo',
            'students.fname',
            'students.lname',
            'students.reg_no',
            'students.gender',
            'students.province',
            'students.district',
            'rtqfs.level_name',
            'accr_schools_information.school_name',
        ]);
        return DataTables::of($query)
            ->editColumn('photo', function ($student) {
                $photo = getStudentPhoto($student);
                $name = $student->fname;
                return '<img src="' . $photo . '" alt="' . $name . '" width="50px"
                                     height="50px" class="img img-rounded">';
            })
            ->addColumn('names', function ($student) {
                $name = $student->fname . " " . $student->lname;
                return ucfirst($name);
            })
            ->editColumn('reg_no', function ($student) {
                $reg = $student->reg_no;
                return '<label class="label label-primary"> ' . $reg . '</label>';
            })
            ->rawColumns(['photo', 'reg_no'])
            ->addIndexColumn()
            ->make(true);
    }

    public function modules(Request $request, $id = null)
    {
        $this->middleware('wda');
        if ($request->delete) {
            $module = Module::find($request->delete);
            $module->delete();
            return back()->with(['status' => '1', 'message' => 'Deleted Successfully.']);
        }

        if ($request->has('module_title')) {
            if ($request->has('module_id')) {
                $validator = Validator::make($request->all(), [
                    'qualification_id' => 'required',
                    'module_code' => 'required',
                    'module_title' => 'required',
                    'credits' => 'nullable|min:1|numeric',
                    'learning_hours' => 'nullable|min:1|numeric',
                    'competence_type' => 'required',
                    'competence_class' => 'required',
                    'attachment' => 'nullable|mimes:pdf'

                ]);
                $module = Module::findOrFail($request->module_id);
            } else {
                $validator = Validator::make($request->all(), [
                    'qualification_id' => 'required',
                    'module_code' => 'required',
                    'module_title' => 'required',
                    'credits' => 'nullable|min:1|numeric',
                    'learning_hours' => 'nullable|min:1|numeric',
                    'competence_type' => 'required',
                    'competence_class' => 'required',
                    'attachment' => 'nullable|mimes:pdf'

                ]);
                $module = new Module();
                $cmodule = Module::withTrashed()
                    ->where('qualification_id', $request->qualification_id)
                    ->where('module_code', $request->module_code)
                    ->where('module_title', $request->module_title)->first();

                if ($cmodule) {
                    $cmodule->restore();
                    return back()->with(['status' => '1', 'message' => 'Restored Successfully.']);
                }
            }

            if ($validator->fails())
                return back()->withInput()->with(['status' => '1', 'message' => $validator->errors()->first()]);

            $module->qualification_id = $request->qualification_id;
            $module->module_code = $request->module_code;
            $module->module_title = $request->module_title;
            $module->credits = $request->credits;
            $module->competence_type = $request->competence_type;
            $module->competence_class = $request->competence_class;
            $module->learning_hours = $request->learning_hours;
            if ($request->has('attachment')) {
                $path = '/modules/attachments/' . date('FY');
                $file = $request->file('attachment');
                $fileP = $path;
                $file = Storage::disk(config('voyager.storage.disk'))->put($fileP, $file, 'public');
                $attachment = $file;
            } else
                $attachment = $request->has('module_id') ? $module->attachment : null;
            $module->attachment = $attachment;
            $module->module_type_id = $request->module_type_id;

            $module->save();

            return redirect()->route('wda.modules.index')->with(['status' => '1', 'message' => 'Saved Successfully']);
        }
        $mod = null;
        if ($id != null)
            $mod = Module::findOrFail($id);

        $sectors = TrainingSectorsSource::all();
        $sub_sectors = TrainingTradesSource::all();
        $currs = CurriculumQualification::all();
        $categories = ModuleType::all();
        $modules = Module::all();
        return view('wda.curricula.modules', compact('sectors', 'sub_sectors', 'currs', 'categories', 'mod', 'modules'));
    }

    public function modulesUploading(Request $request)
    {
        if ($request->hasFile('modules')) {
            $this->validate($request, [
                'qualification_id' => 'required'
            ]);
            $validator = Validator::make(
                [
                    'file' => $request->modules,
                    'extension' => strtolower($request->modules->getClientOriginalExtension()),
                ],
                [
                    'file' => 'required',
                    'extension' => 'required|in:xlsx',
                ]
            );
            if ($validator->fails())
                return back()->withInput()->with(['status' => '0', 'message' => 'File is not valid !']);

            $cModules = [];
            $tModules = [];
            $errors = [];
            $insert = [];
            $currs = CurriculumQualification::where('uuid', $request->qualification_id)->first();
            if ($currs) {
                if ($currs->modules)
                    $cModules = $currs->modules()->pluck('module_code')->toArray();
                if ($currs->modules)
                    $tModules = $currs->modules()->pluck('module_title')->toArray();
            }

            $filer = $request->file('modules');
            try {
                $lines = (new FastExcel)->import($filer->getRealPath(), function ($line) {
                    if (!is_null($line)) {
                        if (strlen($line['competence_code']) > 0 && strlen($line['competence_name']) > 0 && strlen($line['credit']) && strlen($line['competence_type']) && strlen($line['competence_class']) && strlen($line['hours'])) {
                            return [
                                'competence_code' => $line['competence_code'],
                                'competence_name' => $line['competence_name'],
                                'credit' => $line['credit'],
                                'competence_type' => $line['competence_type'],
                                'competence_class' => $line['competence_class'],
                                'hours' => $line['hours']
                            ];
                        }
                    }
                });
                $lines = array_filter($lines->toArray(), function ($item) {
                    if (is_array($item))
                        return $item;
                });

                foreach ($lines as $line) {
                    if (!in_array($line['competence_code'], $cModules) && !in_array($line['competence_name'], $tModules)) {
                        $type = getModuleCompetenceType($line['competence_type']);
                        $class = getModuleCompetenceClass($line['competence_class']);
                        if ($type == "" || $class == "") {
                            $errors[] = $line['competence_code'] . "-" . $line['competence_name'] . " | Check Competence Class or Competence Type if is correct !";
                        } else {
                            Module::create([
                                'qualification_id' => $request->input('qualification_id'),
                                'module_code' => $line['competence_code'],
                                'module_title' => $line['competence_name'],
                                'credits' => $line['credit'],
                                'competence_type' => $line['competence_type'],
                                'competence_class' => $line['competence_class'],
                                'learning_hours' => $line['hours'],
                                'module_type_id' => $request->input('module_type_id'),
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ]);
                        }

                    } else
                        $errors[] = $line['competence_code'] . " - " . $line['competence_name'] . " | this competence seems to be already inserted !!!";
                }
            } catch (IOException $e) {
            } catch (UnsupportedTypeException $e) {
            } catch (ReaderNotOpenedException $e) {
            }

            if (count($errors) > 0)
                return redirect()->route('wda.modules.uploading')
                    ->withInput()
                    ->with(['status' => '0', 'message' => $errors]);

            return redirect()->route('wda.modules.index')->with(['status' => '1', 'message' => 'Modules Uploaded Successfully']);
        }
        $sectors = TrainingSectorsSource::all();
        $sub_sectors = TrainingTradesSource::all();
        $currs = CurriculumQualification::all();
        $categories = ModuleType::all();
        return view('wda.curricula.upload_modules', compact('sectors', 'sub_sectors', 'currs', 'categories'));
    }

    public function moduleCategories(Request $request, $id = null)
    {
        $this->middleware('wda');
        if ($request->has('delete')) {
            $id = $request->delete;
            ModuleType::find($id)->delete();
            return back()->with(['status' => '1', 'message' => 'Deleted Successfully.']);
        }
        if ($request->has('title')) {
            if ($request->category_id)
                $category = ModuleType::find($request->category_id);
            else {
                $simple = ModuleType::withTrashed()->where('title', $request->title)->whereNotNull('deleted_at')->first();
                if (!is_null($simple)) {
                    $simple->restore();
                    return redirect()->route('wda.modules.category.index')->with(['status' => '1', 'message' => 'Restored Successfully.']);
                } else {
                    $category = new ModuleType();
                }
            }

            $category->title = $request->title;
            $category->save();
            return redirect()->route('wda.modules.category.index')->with(['status' => '1', 'message' => 'Saved Successfully.']);
        }
        $categories = ModuleType::all();
        if ($id != null) {
            $cat = ModuleType::findOrFail($id);
        }
        return view('wda.curricula.modules_type', compact('categories', 'cat'));
    }

    public function curriculumView($id)
    {
        $this->middleware('wda');
        $curr = CurriculumQualification::find($id);
        if ($curr)
            $modules = $curr->modules;
        else
            $modules = [];
        return view('wda.curricula.curr_vew', compact('curr', 'modules'));
    }

    public function tradesView($id)
    {
        if ($id != null) {
            $sector_info = TrainingSectorsSource::find($id);
            if ($sector_info) {
                return view('wda.curricula.view_trades', compact('sector_info'));
            }
        }
        return abort(404);
    }

    public function view($id = null, $trade = null)
    {
        $this->middleware('wda');
        if ($id != null) {
            $sector_info = TrainingSectorsSource::find($id);
//            $subsector_info    = TrainingTradesSource::find($id);
            $rtqfs = RtqfSource::orderBy('id', 'asc')->get();
//            dd($sector_info->subsectors);
            $trad = $trade;
            return view('wda.curricula.details', compact('sector_info', 'rtqfs', 'trad'));
        }

        $sectors = TrainingSectorsSource::all();
        return view('wda.curricula.view', compact('sectors'));
    }

    public function getTrades($sector_id)
    {
        $trades = TrainingTradesSource::where('sector_id', $sector_id)->whereNotNull('uuid')->get();
        return $trades;
    }

    public function getQualifications($trade_id)
    {
        $trades = CurriculumQualification::where('sub_sector_id', $trade_id)->whereNotNull('uuid')->get();
        return $trades;
    }

    public function getDept(Request $request)
    {
        if ($request->has('school_id')) {
            $departments = Department::where('school_id', $request->input('school_id'))->get();
            $arrs = [];
            if ($departments->count() > 0) {
                foreach ($departments as $item) {
//                    $arrs[$item->id] = $item->qualification->qualification_title;
                    $arrs[] = [
                        'id' => $item->id,
                        'name' => $item->qualification->qualification_title
                    ];
                }
                return json_encode($arrs);
            }
        }
        return json_encode([]);
    }

    public function getLevels(Request $request)
    {
        $id = $request->get('id');
        if ($request->has('where') && $request->get('where') == 'id')
            if ($request->has('school_id')) {
                $d = getDepartmentsBySchoolId($request->get('school_id'))->where('id', $id);
            } else {
                $d = getDepartments()->where('id', $id);
            }
        else
            $d = getDepartments()->where('qualification_id', $id);

        if ($d->count() > 0) {
            $l = [];
            foreach ($d->get() as $dd) {
                if ($dd->levels->count() > 0) {
                    foreach ($dd->levels as $level) {
                        $l[] = [
                            'id' => $level->id,
                            'name' => $level->rtqf->level_name
                        ];
                    }
                }
            }
            return json_encode($l);
        }
        return json_encode([]);
    }


}
