<?php

namespace App\Http\Controllers\WdaAuth;

use App\Http\Controllers\Controller;
use App\Model\Accr\AccrTimeline;
use Illuminate\Http\Request;

class TimelinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timelines = AccrTimeline::all();
        return view('timelines.index', compact('timelines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('timelines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = [
            'activity' => 'required|min:2|unique:accr_timelines',
            'date_from' => 'required|date|after:today',
            'date_to' => 'required|date|after:date_from',
            'responsible' => 'required|min:2'
        ];
        if ($request->has('id'))
            $validation['activity'] = 'required|min:2|unique:accr_timelines,id,' . $request->has('id');

        $this->validate($request, $validation);

        if ($request->has('id'))
            $model = AccrTimeline::findOrFail($request->input('id'));
        else
            $model = new AccrTimeline;

        $model->activity = $request->input('activity');
        $model->date_from = $request->input('date_from');
        $model->date_to = $request->input('date_to');
        $model->responsible = $request->input('responsible');

        $model->save();

        if ( ! $request->has('id'))
            return back()->with(['status' => '1', 'message' => "Timeline Added Successfully."]);
        else
            return redirect()->route('wda.qm.manual')
                ->with(['status' => '1', 'message' => "Timeline Updated Successfully."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
