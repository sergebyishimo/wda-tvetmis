<?php

namespace App\Http\Controllers\WdaAuth;

use App\Http\Requests\CreateWeightScale;
use App\Http\Requests\UpdateWeightScale;
use App\WdaGrade;
use App\WeightScale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WeightScaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manageweightscale = null;
        $weightscales = WeightScale::all();
        $weightscale = null;
        $grades = WdaGrade::all();
        return view('wda.weightscale.index',compact('manageweightscale','weightscales','weightscale','grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateWeightScale $request)
    {
        $check = WeightScale::where('grade_id', $request->grade_id)->where('weightscale_from', $request->weightscale_from)->where('weightscale_to', $request->weightscale_to)->first();
        if ($check) {
            return redirect(route('wda.weightscale.index'))->with(['status' => '0', 'message' => "This Weight Scale Already Available!."]);
        }

        $weightscale = new WeightScale();
        $weightscale->grade_id = $request->grade_id;
        $weightscale->weightscale_from = $request->weightscale_from;
        $weightscale->weightscale_to = $request->weightscale_to;
        $save = $weightscale->save();
        if ($save)
            return redirect(route('wda.weightscale.index'))->with(['status' => '1', 'message' => "Added Weight Scale  Successfully."]);
        else
            return redirect(route('wda.weightscale.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Add Weight Scale."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $weightscales = WeightScale::all();
        $manageweightscale = $grade = WeightScale::where('id', $id)->first();
        if ($manageweightscale) {
            return view('wda.weightscale.index', compact('manageweightscale', 'weightscales'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWeightScale $request, $id)
    {
        $weightscale = WeightScale::where('id', $id)->first();
        if ($weightscale) {
            $weightscale->grade_id = $request->grade_id;
            $weightscale->weightscale_from = $request->weightscale_from;
            $weightscale->weightscale_to = $request->weightscale_to;
            $save = $weightscale->save();
            if ($save)
                return redirect(route('wda.weightscale.index'))->with(['status' => '1', 'message' => "Weight Scale Updated Successfully."]);
            else
                return redirect(route('wda.weightscale.index'))->withInput()
                    ->with(['status' => '0', 'message' => "Failed to update Weight Scale."]);

        } else {
            return redirect(route('wda.weightscale.index'))->with(['status' => '0', 'message' => "Update Weight Scale"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $weightscale = WeightScale::where('id', $id)->first();
        if ($weightscale) {
            $weightscale->delete();
            return redirect(route('wda.weightscale.index'))->with(['status' => '1', 'message' => 'Weight Scale removed successful']);
        } else
            abort(409);
    }
}
