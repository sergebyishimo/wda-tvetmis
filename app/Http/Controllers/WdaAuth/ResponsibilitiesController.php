<?php

namespace App\Http\Controllers\WdaAuth;

use App\Http\Controllers\Controller;
use App\Model\Accr\AccrResponsibilite;
use Illuminate\Http\Request;

class ResponsibilitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = AccrResponsibilite::all()->first();
        return view('responsibilities.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('responsibilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:10',
            'attachment' => 'nullable|mimes:pdf'
        ]);

        $path = null;
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $path = $file->storeAs(
                'qm-manual/attachment', time()
            );
        }

        $model = AccrResponsibilite::all()->first();
        if ($model) {
            if ($path == null)
                $path = $model->attachment;
        } else
            $model = new AccrResponsibilite;

        $model->description = $request->input('description');
        $model->attachment = $path;
        $model->save();

        return back()->with(['status' => '1', 'message' => "Responsibilities Updated Successfully."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
