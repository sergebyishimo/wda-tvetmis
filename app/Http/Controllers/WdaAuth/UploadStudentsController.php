<?php

namespace App\Http\Controllers\WdaAuth;

use App\Department;
use App\Http\Requests\WdaMassUploadStudentRequest;
use App\Level;
use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\Graduate;
use App\Model\Accr\QualificationLevel;
use App\Model\Accr\RtqfSource;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\TrainingSectorsSource;
use App\Model\Accr\TrainingTradesSource;
use App\OldTrades;
use App\Qualification;
use App\StaffsInfo;
use App\Student;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use function foo\func;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Rap2hpoutre\FastExcel\FastExcel;

class UploadStudentsController extends Controller
{
    protected $school_name = null;
    protected $school_id = null;
    protected $level_id = null;
    protected $acad_year = null;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $schools = SchoolInformation::all();
        $qualifications = Qualification::all();
        $departments = Department::all();
        $arrs = [];
        if ($departments->count() > 0) {
            foreach ($departments as $item) {
                if ($item->qualification)
                    $arrs[$item->id] = $item->qualification->qualification_title;
            }
        }

        $dep = $departments->where('status', '1');

        return view('wda.uploadstudents.index', compact('schools', 'qualifications', 'arrs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param WdaMassUploadStudentRequest $request
     * @return \Illuminate\Http\Response
     * @throws IOException
     * @throws ReaderNotOpenedException
     * @throws UnsupportedTypeException
     */
    public function store(WdaMassUploadStudentRequest $request)
    {
        $this->school_id = $request->input('school_id');
        $this->level_id = $request->input('level_id');
        $filer = $request->file('attachment');
        $this->acad_year = $request->input('academic_year');

        // Staff Sheet:5

        $lines = (new FastExcel)->sheet(5)->import($filer->getRealPath(), function ($line) {
            //dd($line);
            if ($line['first_name'] == null && $line['last_name'] == null)
                return;
            else
                return $line;
        });
        $lines = array_filter($lines->toArray());
        $staffErrors = [];
        $uploadedStaffs = 0;
        $skipped = 0;
        $skippedBoth = 0;

        $failed_sectors = [];
        $failed_trades = [];
        $failed_rtqf = [];
        $failed_quas = [];
        $failed_department = [];
        if (!empty($lines)) {
            foreach ($lines as $line) {
                if ($line == null)
                    break;

                $excel_sector = isset($line['sector']) ? $line['sector'] : null;
                $excel_trade =  isset($line['trade']) ? $line['trade'] : null;
                $excel_rtqf =  isset($line['rtqf']) ? $line['rtqf'] : null;

                $sector = TrainingSectorsSource::where('tvet_field', $excel_sector)->first();

                if (!$sector) {
                    $str = 'Staff Sheet : ' . $excel_sector;
                    if (!in_array($str, $failed_sectors)) {
                        $failed_sectors[] = $str;
                    }

                }

                if ($sector) {
                    $trade = TrainingTradesSource::where('sub_field_name', $excel_trade)->where('sector_id', $sector->id)->first();

                    if (!$trade) {
                        $str = 'Staff Sheet : ' . $excel_trade;
                        if (!in_array($str, $failed_trades)) {
                            $failed_trades[] = $str;
                        }
                        continue;
                    }

                    $rtqf = RtqfSource::where('level_name', $excel_rtqf)->first();
                    if (!$rtqf) {
                        $str = 'Staff Sheet : ' . $excel_rtqf;
                        if (!in_array($str, $failed_rtqf)) {
                            $failed_rtqf[] = $str;
                        }
                        continue;
                    }

                    $qua = CurriculumQualification::where('sector_id', $sector->id)->where('sub_sector_id', $trade->id)->where('rtqf_level_id', $rtqf->id)->whereNotNull('uuid')->first();

                    $str = $sector->tvet_field . ' - ' . $trade->sub_field_name .  ' - ' . $rtqf->level_name;
                    if (!$qua) {
                        $str = 'Staff Sheet : ' . $str;
                        if (!in_array($str , $failed_quas)) {
                            $failed_quas[] = $str;
                        }

                        continue;
                    }

                    $department = Department::where('school_id', $this->school_id)->where('qualification_id', $qua->uuid)->first();

                    if (!$department) {
                        $str = 'Staff Sheet : ' . $str;
                        if (!in_array($str, $failed_department)) {
                            $failed_department[] = $str;
                        }
                        continue;
                    }
                }


                $gender = null;
                if (isset($line['gender'])) {
                    if (ucfirst($line['gender']) == "M" || ucfirst($line['gender']) == "Male") {
                        $gender = 'Male';
                    } elseif (ucfirst($line['gender']) == "F" || ucfirst($line['gender']) == "Female") {
                        $gender = 'Female';
                    }
                }

                $level = isset($line['qualification_level']) ? $line['qualification_level'] : null;

                if ($level != null) {
                    $qua_level = QualificationLevel::where('name', $level)->first();
                    if ($qua_level)
                        $qua_level = $qua_level->id;
                    else
                        $qua_level = null;
                } else {
                    $qua_level = null;
                }

                $email = isset($line['email']) ? $line['email'] : null;
                $phone = isset($line['phone']) ? $line['phone'] : null;

                if ($email == null || $email == '')
                    $check = StaffsInfo::where('phone_number', $phone)->count();
                elseif ($phone == null || $phone == '')
                    $check = StaffsInfo::where('email', $email)->count();
                else
                    $check = StaffsInfo::where('phone_number', $phone)->orwhere('email', $email)->count();

                if (($phone == null || $phone == '') && ($email == null || $email == '')) {
                    $skippedBoth++;
                    continue;
                }

                if ($check > 0) {
                    $skipped++;
                    continue;
                }

                if (!isset($qua) && $line['category'] == 'Academic' ) {
                    $failed_category[] = implode('|', $line);
                    continue;
                }

                $data = [
                    'school_id' => $this->school_id,
                    'first_name' => isset($line['first_name']) ? $line['first_name'] : null,
                    'last_name' => isset($line['last_name']) ? $line['last_name'] : null,
                    'gender' => $gender,
                    'staff_category' => isset($line['category']) ? $line['category'] : null,
                    'sector_taught' => $sector ? $sector->id : null,
                    'sub_sector_taught' => isset($trade) ? $trade->id : null,
                    'qualification_taught' => isset($qua) ? $qua->id : null,

                    'nationality' => isset($line['nationality']) ? $line['nationality'] : null,

                    'graduated_year' => isset($line['graduated_year']) ? $line['graduated_year'] : null,
                    'institution' => isset($line['institution']) ? $line['institution'] : null,
                    'qualification' => isset($line['qualification']) ? $line['qualification'] : null,
                    'qualification_level' => $qua_level,

                    'email' => isset($line['email']) ? $line['email'] : null,
                    'phone_number' => isset($line['phone']) ? $line['phone'] : null,
                ];

                if ($data['first_name'] != null && $data['last_name'] != null) {
                    try {
                        StaffsInfo::create($data);
                        $uploadedStaffs++;
                    } catch (QueryException $e) {
                        //$error[] = $e->getCode() == 23000 ? $name . ' already uploaded !!' : ' Error occur while uploading ' . $name;
                        $staffErrors[] = $e->getMessage();
                        //Log::error($e->__toString());
                    }
                }
            }
        }


        // Students Sheet : 6

        $lines = (new FastExcel)->sheet(6)->import($filer->getRealPath(), function ($line) {
            //dd($line);
            if (count($line) == 0) {
                return;
            }

            if ($line['first_name'] == null && $line['last_name'] == null)
                return;
            else
                return $line;
        });
        $lines = array_filter($lines->toArray());
        $studentsError = [];
        $uploaded = 0;
        if (!empty($lines)) {
            foreach ($lines as $line) {
                if ($line == null)
                    break;

                $excel_sector =  $line['sector'];
                $excel_trade =  $line['trade'];
                $excel_rtqf =  $line['rtqf'];

                $sector = TrainingSectorsSource::where('tvet_field', $excel_sector)->first();

                if (!$sector) {
                    $str = 'Students Sheet : ' . $excel_sector;
                    if (!in_array($str, $failed_sectors)) {
                        $failed_sectors[] = $str;
                    }
                    continue;
                }


                $trade = TrainingTradesSource::where('sub_field_name', $excel_trade)->where('sector_id', $sector->id)->first();

                if (!$trade) {
                    $str = 'Students Sheet : ' . $excel_trade;
                    if (!in_array($str, $failed_trades)) {
                        $failed_trades[] = $str;
                    }
                    continue;
                }

                $rtqf = RtqfSource::where('level_name', $excel_rtqf)->first();
                if (!$rtqf) {
                    $str = 'Students Sheet : ' . $excel_rtqf;
                    if (!in_array($str, $failed_rtqf)) {
                        $failed_rtqf[] = $str;
                    }
                    continue;
                }

                $qua = CurriculumQualification::where('sector_id', $sector->id)->where('sub_sector_id', $trade->id)->where('rtqf_level_id', $rtqf->id)->whereNotNull('uuid')->first();

                $str = $sector->tvet_field . ' - ' . $trade->sub_field_name .  ' - ' . $rtqf->level_name;
                if (!$qua) {
                    $str = 'Students Sheet : ' . $str;
                    if (!in_array($str , $failed_quas)) {
                        $failed_quas[] = $str;
                    }

                    continue;
                }

                $department = Department::where('school_id', $this->school_id)->where('qualification_id', $qua->uuid)->first();

                if (!$department) {
                    $str = 'Students Sheet : ' . $str;
                    if (!in_array($str, $failed_department)) {
                        $failed_department[] = $str;
                    }
                    continue;
                }

                $level = Level::where('school_id', $this->school_id)->where('department_id', $department->id)->where('rtqf_id', $rtqf->uuid)->first();

                if (!$level)
                    continue;

                $level_id = $level->id;

                $gender = null;
                $mode = null;
                if (isset($line['gender'])) {
                    if (ucfirst($line['gender']) == "M" || ucfirst($line['gender']) == "Male") {
                        $gender = 'Male';
                    } elseif (ucfirst($line['gender']) == "F" || ucfirst($line['gender']) == "Female") {
                        $gender = 'Female';
                    }
                }

                if (isset($line['mode'])) {
                    if (ucfirst($line['mode']) == "Boarding") {
                        $mode = 'Boarding';
                    } elseif (ucfirst($line['mode']) == "Day") {
                        $mode = 'Day';
                    }
                }

                $data = [
                    'reg_no' => generateReg(),
                    'fname' => isset($line['first_name']) ? $line['first_name'] : null,
                    'lname' => isset($line['last_name']) ? $line['last_name'] : null,
                    'gender' => $gender,
                    'mode' => $mode,
                    'bdate' => isset($line['dob']) ? $line['dob'] : null,
                    'nationalID' => isset($line['idnumber']) ? $line['idnumber'] : null,
                    'school' => $this->school_id,
                    'level_id' => $level_id,
                    'acad_year' => $this->acad_year,
                    'ft_name' => isset($line['father']) ? $line['father'] : null,
                    'ft_phone' => isset($line['father_phone']) ? $line['father_phone'] : null,
                    'mt_name' => isset($line['mother']) ? $line['mother'] : null,
                    'mt_phone' => isset($line['mother_phone']) ? $line['mother_phone'] : null,
                    'ubudehe' => isset($line['ubudehe']) ? $line['ubudehe'] : null,
                    'acad_year' => 2018,
                    //'email' => isset($line['email']) ? $line['email'] : null,
                    //'telephone' => isset($line['phone']) ? $line['phone'] : null,
                    //'district' => isset($line['district']) ? $line['district'] : null,
                    //'sector' => isset($line['sector']) ? $line['sector'] : null
                ];

                if (isset($line['school_code']) && isset($line['trade_code']) && isset($line['number'])) {
                    if ($line['school_code'] != null && $line['trade_code'] != null && $line['number']) {
                        $data['index_number'] = $line['school_code'] . $line['trade_code'] . $line['number'];
                    }
                }

//            $internal = [];
//            array_walk($data,function($item) use(&$internal){
//                if($item == null){
//                    array_push($internal,$item);
//                }
//            });
//            if(count($data) != count($internal))
                if ($data['fname'] != null && $data['lname'] != null && $data['level_id'] != null) {
                    $name = $data['fname'] . " " . $data['lname'];
                    try {
                        Student::create($data);
                        $uploaded++;
                    } catch (QueryException $e) {
                        //$error[] = $e->getCode() == 23000 ? $name . ' already uploaded !!' : ' Error occur while uploading ' . $name;
                        $studentsError[] = $e->getMessage();
                        //Log::error($e->__toString());
                    }
                }
            }
        }

        // Graduates Sheet : 7
        $graduatesErrors = [];
        $uploadedGraduates = 0;

        $lines = (new FastExcel)->sheet(7)->import($filer->getRealPath(), function ($line) {
            //dd($line);
            if ($line['first_name'] == null && $line['last_name'] == null)
                return;
            else
                return $line;
        });
        $lines = array_filter($lines->toArray());

        if (!empty($lines)) {
            foreach ($lines as $line) {
                if ($line == null)
                    break;

                $gender = null;
                if (isset($line['gender'])) {
                    if (ucfirst($line['gender']) == "M" || ucfirst($line['gender']) == "Male") {
                        $gender = 'Male';
                    } elseif (ucfirst($line['gender']) == "F" || ucfirst($line['gender']) == "Female") {
                        $gender = 'Female';
                    }
                }

                $names = isset($line['first_name']) ? $line['first_name'] : null;
                $names .= isset($line['last_name']) ? $line['last_name'] : null;

                $data = [
                    'names' => $names,
                    'gender' => $gender,
                    'trade' => isset($line['option']) ? $line['option'] : null,
                    'year' => isset($line['year']) ? $line['year'] : null,
                    'phone_number' => isset($line['phone']) ? $line['phone'] : null,
                    'employment_status' => isset($line['status']) ? $line['status'] : null,
                ];

                if ($data['names'] != null) {
                    $name = $data['names'];
                    try {
                        Graduate::create($data);
                        $uploadedGraduates++;
                    } catch (QueryException $e) {
                        //$error[] = $e->getCode() == 23000 ? $name . ' already uploaded !!' : ' Error occur while uploading ' . $name;
                        $graduatesErrors[] = $e->getMessage();
                        //Log::error($e->__toString());
                    }
                }

            }
        }


        /*if (count($error) > 0) {
            return back()
                ->withInput()
                ->with(['status' => '0', 'message' => $error]);
        }*/
        return back()->with(['status' => '1', 'message' => 'Uploaded Staffs :' . $uploadedStaffs  . "<br>Already Exists : " . $skipped . "<br>Without Phone or E-mail :" . $skippedBoth . '<br>Uploaded Students: ' . $uploaded . '<br><br>Failed Sectors : ' . implode(', ' , $failed_sectors) . '<br><br>Failed Trades : ' . implode(', ', $failed_trades) . '<br><br>Failed RTQF : ' . implode(', ' , $failed_rtqf) . '<br><br>Failed Quali : ' . implode(', ', $failed_quas) . '<br><br>Quali. not Assigned : ' . implode(', ', $failed_department) . '<br><br>Uploaded Graduates : ' . $uploadedGraduates . '<br><br>Staffs Errors : ' . implode('<br>-' , $staffErrors) . '<br>Students Errors : ' . implode('<br>', $studentsError) . '<br>Graduates Errors : ' . implode('<br>', $graduatesErrors) ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
