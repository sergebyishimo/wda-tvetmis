<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\ApReportingPeriod;
use App\Model\Accr\Notifications;
use App\Model\Accr\Project;
use App\Model\Accr\SpIndicator;
use App\Model\Accr\SpProgram;
use App\Model\Accr\SpReportingPeriod;
use App\Model\Accr\SpResult;
use App\Model\Accr\WelcomeMessages;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\PhpWord;

class StrategicPlanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    public function monitoring()
    {
        $rps = ApReportingPeriod::count();
        $srps = SpReportingPeriod::count();
        $projects = Project::count();

        $welcomes = WelcomeMessages::all();
        $notifications = Notifications::all();

        return view('wda.sp.monitoring', compact('rps', 'srps', 'projects', 'welcomes', 'notifications'));
    }

    public function reportingPeriod($rp_id = null, Request $request)
    {
        $rps = SpReportingPeriod::all();

        if ($rp_id != null) {

            $rp_info = SpReportingPeriod::find($rp_id);

            return view('wda.sp.reporting_period', compact('rps', 'rp_info'));
        }

        if ($request->reporting_period) {
            $rp = new SpReportingPeriod();

            $rp->period = $request->reporting_period;

            $rp->save();

            return redirect()
                ->route('wda.sp.rp.index')
                ->with(['status' => '1', 'message' => 'Reporting Period Created Successfully!!']);
        }

        if ($request->u_reporting_period) {
            $rp = SpReportingPeriod::find($request->rp_id);

            $rp->period = $request->u_reporting_period;

            $rp->save();

            return redirect()
                ->route('wda.sp.rp.index')
                ->with(['status' => '1', 'message' => 'Reporting Period Updated Successfully!!']);
        }

        if ($request->delete_rp) {
            SpReportingPeriod::find($request->delete_rp)->delete();
            return redirect()
                ->route('wda.sp.rp.index')
                ->with(['status' => '1', 'message' => 'Reporting Period Deleted Successfully!!']);
        }


        return view('wda.sp.reporting_period', compact('rps'));
    }

    public function programs($program_id = null, Request $request)
    {
        $rps = SpReportingPeriod::all();
        $programs = SpProgram::all();

        if ($program_id != null) {
            $program_info = SpProgram::find($program_id);

            return view('wda.sp.programs', compact('rps', 'programs', 'program_info'));
        }

        if ($request->name) {
            $program = new SpProgram();
            $program->rp_id = $request->reporting_period;
            $program->number = $request->number;
            $program->name = $request->name;
            $program->save();

            return redirect()
                ->route('wda.sp.programs.index')
                ->with(['status' => '1', 'message' => 'Program Created Successfully!!']);
        }

        if ($request->u_name) {
            $program = SpProgram::find($request->program_id);
            $program->name = $request->u_name;
            $program->save();
            return redirect()
                ->route('wda.sp.programs.index')
                ->with(['status' => '1', 'message' => 'Program Updated Successfully!!']);
        }

        if ($request->delete_pro) {
            SpProgram::find($request->delete_pro)->delete();
            return redirect()
                ->route('wda.sp.programs.index')
                ->with(['status' => '1', 'message' => 'Program Deleted Successfully!!']);
        }

        return view('wda.sp.programs', compact('rps', 'programs'));
    }

    public function results($id = null, Request $request)
    {
        if ($request->delete_result) {

            SpResult::find($request->delete_result)->delete();
            return redirect()
                ->route('wda.sp.results.index')
                ->with(['status' => '1', 'message' => 'Result Deleted Successfully!!']);
        }

        if ($request->name) {
            if ($request->result_id) {
                $result = SpResult::find($request->result_id);
            } else {
                $result = new SpResult();
            }

            $result->program_id = $request->program_id;
            $result->name = $request->name;
            $result->verification_means = $request->verification_means;
            $result->assumptions = $request->assumptions;

            $result->save();

            redirect()
                ->route('wda.sp.results.index')
                ->with(['status' => '1', 'message' => 'Result Saved Successfully!!']);
        }

        $rps = SpReportingPeriod::all();
        $programs = SpProgram::all();
        $results = SpResult::all();

        if ($id != null) {
            $result_info = SpResult::find($id);

            return view('wda.sp.results', compact('rps', 'programs', 'results', 'result_info'));
        }

        return view('wda.sp.results', compact('rps', 'programs', 'results'));
    }

    public function indicators($id = null, Request $request)
    {

        if ($request->delete_indicator) {
            SpIndicator::find($request->delete_indicator)->delete();

            redirect()
                ->route('wda.sp.indicators.index')
                ->with(['status' => '1', 'message' => 'Strategic Plan Indicator Deleted Successfully!!']);
        }
        if ($request->name) {

            if ($request->indicator_id) {
                $indicator = SpIndicator::find($request->indicator_id);

            } else {
                $indicator = new SpIndicator();

                $indicator->result_id = $request->result_id;
            }

            $indicator->name = $request->name;
            $indicator->baseline = $request->baseline;

            $indicator->year_one = $request->year_one;
            $indicator->year_one_target = $request->year_one_target;
            $indicator->year_one_actual = $request->year_one_actual;
            $indicator->year_two = $request->year_two;
            $indicator->year_two_target = $request->year_two_target;
            $indicator->year_two_actual = $request->year_two_actual;
            $indicator->year_three = $request->year_three;
            $indicator->year_three_target = $request->year_three_target;
            $indicator->year_three_actual = $request->year_three_actual;
            $indicator->year_four = $request->year_four;
            $indicator->year_four_target = $request->year_four_target;
            $indicator->year_four_actual = $request->year_four_actual;
            $indicator->year_five = $request->year_five;
            $indicator->year_five_target = $request->year_five_target;
            $indicator->year_five_actual = $request->year_five_actual;
            $indicator->year_six = $request->year_six;
            $indicator->year_six_target = $request->year_six_target;
            $indicator->year_six_actual = $request->year_six_actual;
            $indicator->year_seven = $request->year_seven;
            $indicator->year_seven_target = $request->year_seven_target;
            $indicator->year_seven_actual = $request->year_seven_actual;

//            $indicator->activities_to_deliver_output = $request->activities_to_deliver_output;
//            $indicator->stakeholders        = $request->stakeholders;
//            $indicator->budget              = $request->budget;
//            $indicator->budget_spent        = $request->budget_spent;
//            $indicator->narrative_progress  = $request->narrative_progress;

            $indicator->save();

            redirect()
                    ->route('wda.sp.indicators.index')
                ->with(['status' => '1', 'message' => 'Strategic Plan Indicator Saved Successfully!!']);
        }

        $rps = SpReportingPeriod::all();
        $programs = SpProgram::all();
        $indicators = SpIndicator::all();

        if ($id != null) {
            $indicator_info = SpIndicator::find($id);

            return view('wda.sp.indicators', compact('rps', 'programs', 'indicators', 'indicator_info'));
        }

        return view('wda.sp.indicators', compact('rps', 'indicators', 'programs'));

    }

    public function report($period_id = null, Request $request)
    {
        $periods = SpReportingPeriod::all();
        $columns['indicators'] = DB::select("SHOW COLUMNS FROM `accr_sp_indicators`");
        $columns['programs'] = DB::select("SHOW COLUMNS FROM `accr_sp_programs`");
        $columns['results'] = DB::select("SHOW COLUMNS FROM `accr_sp_results`");
        $datas['indicators'] = DB::select("SELECT `id`,`name` FROM `accr_sp_indicators`");
        $datas['programs'] = DB::select("SELECT `id`,`name` FROM `accr_sp_programs`");
        $datas['results'] = DB::select("SELECT `id`,`name` FROM `accr_sp_results`");
        if (isset($request->period_id) || $period_id != null) {

            $period_id = ($period_id) ? $period_id : $request->period_id;

            $programs = DB::connection('accr_mysql')->table('sp_programs')
                ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
                ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
                ->join('sp_indicators', 'sp_indicators.result_id', '=', 'sp_results.id')
                ->select('sp_programs.*', 'sp_programs.name as programName', 'sp_reporting_periods.period', 'sp_indicators.*', 'sp_indicators.name as indicatorName', 'sp_results.*', 'sp_results.name as resultName')
                ->where('sp_reporting_periods.id', '=', $period_id)
                ->get();


            return view('wda.sp.report', compact('periods', 'datas', 'programs', 'columns'));
        }
//        search for select columns
        if (isset($request->mySelectId)) {
            $inputColumns = $request->columns_select;
            $period_id = $request->mySelectId;

            $Datacolumns = " ";
            $columnsDisplays = " ";
            $x = 1;
            if (is_array($inputColumns)) {
                foreach ($inputColumns as $inputColumn => $value) {
                    if (strpos($value, 'sp_indicators') !== false) {
                        $columnsDisplays .= str_replace('sp_indicators.', '', $value);
                    }
                    if (strpos($value, 'sp_results') !== false) {
                        $columnsDisplays .= str_replace('sp_results.', '', $value);
                    }
                    if (strpos($value, 'sp_programs') !== false) {
                        $columnsDisplays .= str_replace('sp_programs.', '', $value);
                    }
                    $Datacolumns .= $value;
                    if ($x < count($inputColumns)) {
                        $Datacolumns .= ', ';
                        $columnsDisplays .= ', ';
                    }
                    $x++;
                }
            } else {
                return redirect()->back();
            }

            $DataColumnsprograms = DB::connection('accr_mysql')->table('sp_programs')
                ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
                ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
                ->join('sp_indicators', 'sp_indicators.result_id', '=', 'sp_results.id')
                ->select('sp_programs.id as programId', DB::raw($Datacolumns))
                ->where('sp_reporting_periods.id', '=', $period_id)
                ->get();


            return view('wda.sp.report', compact('periods', 'datas', 'columnsDisplays', 'DataColumnsprograms', 'columns'));
        }
        if (isset($request->search_programid) || isset($request->search_result) || isset($request->search_indicator)) {
            $programid = $request->search_programid;

            $result_id = $request->search_result;
            $indicator_id = $request->search_indicator;
            $search = " ";
            $searchId = " ";
            if (!empty($programid)) {
                $search = 'sp_programs';
                $searchId = $programid;
            } elseif (!empty($result_id)) {
                $search = 'sp_results';
                $searchId = $result_id;

            } else {
                $search = 'sp_indicators';
                $searchId = $indicator_id;

            }
            $searchprograms = DB::connection('accr_mysql')->table('sp_programs')
                ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
                ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
                ->join('sp_indicators', 'sp_indicators.result_id', '=', 'sp_results.id')
                ->select('sp_programs.*', 'sp_programs.name as programName', 'sp_reporting_periods.period', 'sp_indicators.*', 'sp_indicators.name as indicatorName', 'sp_results.*', 'sp_results.name as resultName')
                ->where('' . $search . '.id', '=', $searchId)
                ->get();
            return view('wda.sp.report', compact('periods', 'datas', 'searchprograms', 'columns'));
        }

        return view('wda.sp.report', compact('periods', 'datas', 'columns'));
    }

    public function reportResult($result_id = '')
    {
        $result = SpResult::find($result_id);
        return view('wda.sp.report_result', compact('result'));
    }

    public function reportMore($output_id)
    {
        $output = SpOutput::find($output_id);
        return view('sp.report_more', compact('output'));
    }

    public function reporttIndicator($indicator_id)
    {
        $indicator = SpIndicator::find($indicator_id);
        return view('sp.indicator_details', compact('indicator'));
    }

    public function downloadIndicators($outcome_id)
    {

//        $outcome = SpOutcome::find($outcome_id);
        $outcome = DB::connection('accr_mysql')->table('sp_programs')
            ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
            ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
            ->join('sp_indicators', 'sp_indicators.id', '=', 'sp_results.id')
            ->select('sp_programs.*', 'sp_programs.name as programName', 'sp_reporting_periods.period', 'sp_indicators.*', 'sp_indicators.name as indicatorName', 'sp_results.*', 'sp_results.name as resultName')
            ->where('sp_results.id', '=', $outcome_id)
            ->first();
//        dd($outcome);
        if ($outcome) {
            $pdf = \PDF::loadView('sp.download_indicators', ['outcome' => $outcome])
                ->setPaper('a4', 'landscape')
                ->setOptions(['enable-javascript', true])
                ->setOptions(['javascript-delay', 13000])
                ->setOptions(['images', true])
                ->setOptions(['enable-smart-shrinking', true])
                ->setOptions(['no-stop-slow-scripts', true]);
            return $pdf->download('ownload_indicators.pdf');
        }
        return view('sp.download_indicators', compact('outcome'));

//        return $pdf->stream('sp.download_indicators.pdf');
    }

    public function getResults($program_id)
    {
        $results = SpResult::where('program_id', $program_id)->get();
        return $results;
    }

    public function export(Request $request)
    {

        if ($request->word) {
            return $this->word($request);
        }
        if ($request->excel) {
            return $this->excel($request);
        }
        if ($request->pdf) {

            $this->pdf($request);
        }

        return redirect()->back();

    }

    private function pdf($request)
    {
        if ($request->period_id) {
            $programs = DB::connection('accr_mysql')->table('sp_programs')
                ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
                ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
                ->join('sp_indicators', 'sp_indicators.id', '=', 'sp_results.id')
                ->select('sp_programs.*', 'sp_programs.name as programName', 'sp_reporting_periods.period', 'sp_indicators.*', 'sp_indicators.name as indicatorName', 'sp_results.*', 'sp_results.name as resultName')
                ->where('sp_reporting_periods.id', '=', $request->period_id)
                ->get();

            $pdf = PDF::loadView('sp.print', compact('programs'));
            return $pdf->download('strategic.pdf');

//                return view('sp.print',compact('programs'));
        } else {
            return redirect()->back();
        }
    }

    private function word($request)
    {

        $wordTest = new PhpWord();
        if ($request->period_id) {
            // Create a new table style
            $table_style = new \PhpOffice\PhpWord\Style\Table;
            $table_style->setBorderColor('cccccc');
            $table_style->setBorderSize(1);
            $table_style->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);
            $table_style->setWidth(100 * 50);

            // Add a section to the document.
            $section = $wordTest->addSection();

            // Set up our table.
            $section->addTable($table_style);
            $desc1 = "The Portfolio $request->period_id is a very useful feature of the web pagu can establish your archived details and the works to the entire web community. It was outlined to bring in extra clients, get you selected based on this details.";

            $newSection = $wordTest->addSection();
            $newSection->addText($desc1, array('name' => 'Tahoma', 'size' => 15, 'color' => 'green'));

            $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'Word2007');
            try {
                $objectWriter->save(storage_path('StragicPlan.docx'));
            } catch (Exception $e) {
            }

            return response()->download(storage_path('StragicPlan.docx'));

        } else {
            return redirect()->back();
        }
    }

    private function excel($request)
    {
        $spreadsheet = new Spreadsheet();
        $alphabet = range('A', 'W');
//         dd($alphabet);
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER),
            'borders' => array('top' => array(
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startcolor' => array('argb' => 'FFA0A0A0',), 'endcolor' =>
                        array('argb' => 'FFFFFFFF'))));
        foreach (range('A', 'M') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);

        if ($request->period_id) {
            $sheet->setCellValue('A1', 'STRATEGIC PRIORITIES');
            $sheet->setCellValue('B1', 'OUTCOMES');
            $sheet->setCellValue('C1', 'INDICATORS');
            $sheet->setCellValue('D1', 'BASELINE');
            $sheet->setCellValue('E1', 'Year / Target / Actual');
            $sheet->setCellValue('F1', 'Year / Target / Actual');
            $sheet->setCellValue('G1', 'Year / Target / Actual');
            $sheet->setCellValue('H1', 'Year / Target / Actual');
            $sheet->setCellValue('I1', 'Year / Target / Actual');
            $sheet->setCellValue('J1', 'Year / Target / Actual');
            $sheet->setCellValue('K1', 'Year / Target / Actual');
            $sheet->setCellValue('L1', 'MEANS OF VERIFICATION');
            $sheet->setCellValue('M1', 'ASSUMPTIONS');
            $spreadsheet->getActiveSheet()->setTitle('Strategic Plan Information');

            $programs = DB::table('sp_programs')
                ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
                ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
                ->join('sp_indicators', 'sp_indicators.id', '=', 'sp_results.id')
                ->select('sp_programs.*', 'sp_programs.name as programName', 'sp_reporting_periods.period', 'sp_indicators.*', 'sp_indicators.name as indicatorName', 'sp_results.*', 'sp_results.name as resultName')
                ->where('sp_reporting_periods.id', '=', $request->period_id)
                ->get();
            $x = 2;
            foreach ($programs as $program) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A$x", $program->programName)
                    ->setCellValue("B$x", $program->resultName)
                    ->setCellValue("C$x", $program->indicatorName)
                    ->setCellValue("D$x", $program->baseline)
                    ->setCellValue("E$x", $program->year_one . ' / ' . $program->year_one_target . ' / ' . $program->year_one_actual)
                    ->setCellValue("F$x", $program->year_two . ' / ' . $program->year_two_target . ' / ' . $program->year_two_actual)
                    ->setCellValue("G$x", $program->year_three . ' / ' . $program->year_three_target . ' / ' . $program->year_three_actual)
                    ->setCellValue("H$x", $program->year_four . ' / ' . $program->year_four_target . ' / ' . $program->year_four_actual)
                    ->setCellValue("J$x", $program->year_five . ' / ' . $program->year_five_target . ' / ' . $program->year_five_actual)
                    ->setCellValue("I$x", $program->year_six . ' / ' . $program->year_six_target . ' / ' . $program->year_six_actual)
                    ->setCellValue("K$x", $program->year_seven . ' / ' . $program->year_seven_target . ' / ' . $program->year_seven_actual)
                    ->setCellValue("L$x", $program->verification_means)
                    ->setCellValue("M$x", $program->assumptions);
                $x++;
            }
            $writer = new Xlsx($spreadsheet);

            try {
                $writer->save(storage_path('StragicPlan.xlsx'));
            } catch (Exception $e) {
            }

            return response()->download(storage_path('StragicPlan.xlsx'));

        } else {
            return redirect()->back();
        }
    }

    public function toPrint($period_id)
    {

        if ($period_id) {
            $programs = DB::table('sp_programs')
                ->join('sp_reporting_periods', 'sp_reporting_periods.id', '=', 'sp_programs.rp_id')
                ->join('sp_results', 'sp_results.program_id', '=', 'sp_programs.id')
                ->join('sp_indicators', 'sp_indicators.id', '=', 'sp_results.id')
                ->select('sp_programs.*', 'sp_programs.name as programName', 'sp_reporting_periods.period', 'sp_indicators.*', 'sp_indicators.name as indicatorName', 'sp_results.*', 'sp_results.name as resultName')
                ->where('sp_reporting_periods.id', '=', $period_id)
                ->get();
            return view('sp.print', compact('programs'));
        } else {
            return redirect()->back();
        }
    }

    // Javascript API Section

    public function getPrograms($sp_id)
    {
        $programs = SpProgram::where('rp_id', $sp_id)->get();
        return $programs;
    }

}
