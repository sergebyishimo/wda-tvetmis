<?php

namespace App\Http\Controllers\WdaAuth;

use App\Combination;
use App\Http\Requests\WdaOldCourseCreate;
use App\Http\Requests\WdaOldCourseUpdate;
use App\OldCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OldCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oldcourses = OldCourse::all();
        return view('wda.oldcourses.index',compact('oldcourses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $oldcourse = null;
        $combinations = Combination::all();
        return view('wda.oldcourses.create',compact('oldcourse','combinations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WdaOldCourseCreate $request)
    {
        $oldcourse = null;
        $cs = new OldCourse();
        $cs->course_name = $request->course_name;
        $cs->course_code = $request->course_code;
        $cs->combination_id = $request->combination_id;
        $save = $cs->save();
        if ($save)
            return redirect(route('wda.oldcourses.index'))->with(['status' => '1', 'message' => "Course Added Successfully."]);
        else
            return redirect(route('wda.oldcourses.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Course."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $combinations = Combination::all();
        $oldcourse = OldCourse::where('id', $id)->first();
        if ($oldcourse) {
            return view('wda.oldcourses.create', compact('oldcourse', 'combinations'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WdaOldCourseUpdate $request, $id)
    {
        $oldcourse = OldCourse::where('id', $id)->first();
        if (!$oldcourse)
            return abort(419);
        $oldcourse->course_name = $request->course_name;
        $oldcourse->course_code = $request->course_code;
        $oldcourse->combination_id = $request->combination_id;
        $save = $oldcourse->save();

        if ($save)
            return redirect(route('wda.oldcourses.index'))->with(['status' => '1', 'message' => "Course Updated Successfully."]);
        else
            return redirect(route('wda.oldcourses.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a Course."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldcourse = OldCourse::where('id', $id)->first();
        if ($oldcourse) {
            $oldcourse->delete();
            return redirect(route('wda.oldcourses.index'))->with(['status' => '1', 'message' => 'Course Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
