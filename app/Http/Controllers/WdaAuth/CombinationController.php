<?php

namespace App\Http\Controllers\WdaAuth;

use App\Combination;
use App\Http\Requests\WdaCombinationCreate;
use App\Http\Requests\WdaCombinationUpdate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CombinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $combinations = Combination::all();
        return view('wda.combinations.index',compact('combinations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $combination = null;
        return view('wda.combinations.create',compact('combination'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WdaCombinationCreate $request)
    {
        $combination = null;
        $comb = new Combination();
        $comb->combination_name = $request->combination_name;
        $comb->combination_code = $request->combination_code;
        $save = $comb->save();
        if ($save)
            return redirect(route('wda.combinations.index'))->with(['status' => '1', 'message' => "Combination Added Successfully."]);
        else
            return redirect(route('wda.combinations.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Combinations."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $combinations = Combination::all();
        $combination = Combination::where('id', $id)->first();
        if ($combination) {
            return view('wda.combinations.index', compact('combination', 'combinations'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WdaCombinationUpdate $request, $id)
    {
        $combination = Combination::where('id', $id)->first();
        if (!$combination)
            return abort(419);
        $combination->combination_name = $request->combination_name;
        $combination->combination_code = $request->combination_code;
        $save = $combination->save();

        if ($save)
            return redirect(route('wda.combinations.index'))->with(['status' => '1', 'message' => "Combination Updated Successfully."]);
        else
            return redirect(route('wda.combinations.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a Combination."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $combination = Combination::where('id', $id)->first();
        if ($combination) {
            $combination->delete();
            return redirect(route('wda.combinations.index'))->with(['status' => '1', 'message' => 'Combination Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
