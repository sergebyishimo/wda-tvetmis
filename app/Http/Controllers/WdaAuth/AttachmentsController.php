<?php

namespace App\Http\Controllers\WdaAuth;

use App\AttachmentUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class AttachmentsController extends Controller
{
    public function index()
    {
        $attachments = AttachmentUpload::all();
        
        return view('wda.attachments.index', compact('attachments'));
    }

    public function upload(Request $request) {
        
        if($request->title) {
            $path = '/attachments/uploaded/';
            if ($request->file('file')) {
                $photo = $request->file('file');
                $photoP = $path;
                $file = Storage::disk(config('voyager.storage.disk'))->put($photoP, $photo, 'public');
                $file = $request->file->storeAs($photoP, $request->title . ' ' . date('Y m d H:i').'.'.$request->file->getClientOriginalExtension(), 'public');
            }

            $data = [
                'title' => $request->title,
                'description' => $request->description,
                'file' => $file,
                'added_by' => auth()->guard('wda')->user()->id
            ];

            AttachmentUpload::create($data);

            return redirect(route('wda.attachments.index'));
        }

        return view('wda.attachments.upload');
    }

    public function remove($att_id) {
        AttachmentUpload::find($att_id)->delete();
        return redirect(route('wda.attachments.index'));
    }
}
