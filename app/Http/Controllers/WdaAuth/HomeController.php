<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\SchoolInformation;
use App\Model\Accr\TrainingSectorsSource;
use App\Model\Accr\TrainingTradesSource;
use App\RwandaBoundary;
use App\StaffsInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Student;
use JavaScript;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('wda');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schools_nber = SchoolInformation::where('school_activity', 'Active')->count();
        $have_internet = SchoolInformation::where('school_activity', 'Active')->where('has_internet', 2)->count();
        $have_electricity = SchoolInformation::where('school_activity', 'Active')->where('has_electricity', 2)->count();
        $have_water = SchoolInformation::where('school_activity', 'Active')->where('has_water', 2)->count();
        $private = SchoolInformation::where('school_activity', 'Active')->where('school_status', 'Private')->count();

        $provinces = ['Northern Province', 'Southern Province', 'Eastern Province', 'Western Province', 'City of Kigali'];
        //$schools_per_province_arr = [];
        $public = [];
        $gov = [];
        $privates = [];
        $staff_males = [];
        $staff_females = [];
        $students_male = [];
        $students_female = [];
        $students_per_province = [];
        foreach ($provinces as $province) {
            //$schools_per_province_arr[] = SchoolInformation::where('school_activity', 'Active')->where('province', $province)->count();
            $public[] = SchoolInformation::where('school_activity', 'Active')->where('province', $province)->where('school_status', 'Public')->count();
            $gov[] = SchoolInformation::where('school_activity', 'Active')->where('province', $province)->where('school_status', 'Government Aid')->count();
            $privates[] = SchoolInformation::where('school_activity', 'Active')->where('province', $province)->where('school_status', 'Private')->count();

            $staff_males[] = StaffsInfo::join('accr_schools_information', 'accr_schools_information.id', 'staffs_info.school_id')->where('accr_schools_information.province', $province)->whereIn('staffs_info.gender', ['Male' , 'M'])->count();
            $staff_females[] = StaffsInfo::join('accr_schools_information', 'accr_schools_information.id', 'staffs_info.school_id')->where('accr_schools_information.province', $province)->whereIn('staffs_info.gender', ['Female' , 'F'])->count();

            $students_male[] = Student::join('accr_schools_information', 'accr_schools_information.id', 'students.school')->where('accr_schools_information.province', $province)->whereIn('gender', ['Male', 'M'])->count();
            $students_female[] = Student::join('accr_schools_information', 'accr_schools_information.id', 'students.school')->where('accr_schools_information.province', $province)->whereIn('gender', ['Female', 'F'])->count();

        }

        $db_sectors = TrainingSectorsSource::all();
        $sectors = [];
        $sectors_statistics = [];
        foreach ($db_sectors as $sector) {
            $sectors[] = $sector->field_code;
            $sectors_statistics[] = Student::join('levels', 'levels.id', 'students.level_id')->join('departments', 'departments.id', 'levels.department_id')->join('accr_curriculum_qualifications', 'accr_curriculum_qualifications.uuid', 'departments.qualification_id')->where('accr_curriculum_qualifications.sector_id', $sector->id)->count();
        }

        // Schools Per District
        $districts = RwandaBoundary::distinct('district')->get(['district'])->pluck('district')->toArray();
        $districts_arr = [];
        $districts_stats  = [];
        $staff_districts_stats = [];
        foreach ($districts as $district) {
            $districts_stats[] = SchoolInformation::where('district', $district)->where('school_activity', 'Active')->count();
            $districts_arr[] = substr($district, 0, 5);

            $staff_districts_stats[] = StaffsInfo::join('accr_schools_information', 'accr_schools_information.id', 'staffs_info.school_id')->where('accr_schools_information.district', $district)->where('accr_schools_information.school_activity', 'Active')->count();
        }

        if ($request->sector_id) {
            $db_trades = TrainingTradesSource::where('sector_id', $request->sector_id)->get();
            $trades_arr = [];
            $staffs_trades_stats = [];
            $students_trades_stats = [];
            foreach ($db_trades as $trade) {
                $trades_arr[] = $trade->sub_field_name;
                $staffs_trades_stats[] = StaffsInfo::where('sub_sector_taught', $trade->id)->count();
                $students_trades_stats[] = Student::join('levels', 'levels.id', 'students.level_id')->join('departments', 'departments.id', 'levels.department_id')->join('accr_curriculum_qualifications', 'accr_curriculum_qualifications.uuid', 'departments.qualification_id')->where('accr_curriculum_qualifications.sub_sector_id', $trade->id)->count();
            }

            JavaScript::put([
               'trades_arr' => $trades_arr,
               'staffs_trades_stats' => $staffs_trades_stats,
               'students_trades_stats' => $students_trades_stats
            ]);
        }

        JavaScript::put([
            'provinces' => ['North', 'South', 'East', 'West', 'Kigali'],
            'publics' => $public,
            'gov' => $gov,
            'privates' => $privates,
            'staff_males' => $staff_males,
            'staff_females' => $staff_females,
            'students_male' => $students_male,
            'students_female' => $students_female,
            'sectors' => $sectors,
            'sectors_statistics' => $sectors_statistics,
            'districts' => $districts_arr,
            'districts_stats' => $districts_stats,
            'staff_districts_stats' => $staff_districts_stats
        ]);

//        $north_students = Student::where('province', 'Northern Province')->count();
//        $south_students = Student::where('province', 'Southern Province')->count();
//        $eastern_students = Student::where('province', 'Eastern Province')->count();
//        $western_students = Student::where('province', 'Western Province')->count();
//        $kigali_students = Student::where('province', 'City of Kigali')->orWhere('province', 'Kigali')->count();
//
//        $students_per_province = [$north_students, $south_students, $eastern_students, $western_students, $kigali_students];

        $users[] = Auth::user();
        $users[] = Auth::guard()->user();
        $users[] = Auth::guard('wda')->user();


        return view('wda.home', compact('schools_nber', 'have_internet', 'have_electricity', 'have_water', 'private', 'db_sectors'));
    }
}
