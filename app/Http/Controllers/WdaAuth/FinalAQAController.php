<?php

namespace App\Http\Controllers\WdaAuth;

use App\Department;
use App\Jobs\ScoresCalculateJob;
use App\Mail\SendSchoolApplicationFeedbackAccreditedMail;
use App\Mail\SendSchoolApplicationFeedbackNotAccreditedMail;
use App\Model\Accr\AccrAccreditationType;
use App\Model\Accr\AccrApplication;
use App\Model\Accr\AccrAttachment;
use App\Model\Accr\AccrBuildingsAndPlots;
use App\Model\Accr\AccrCriteria;
use App\Model\Accr\AccrCriteriaAnswer;
use App\Model\Accr\AccrCriteriaSection;
use App\Model\Accr\AccrProcedure;
use App\Model\Accr\AccrQualificationsApplied;
use App\Model\Accr\AccrResponsibilite;
use App\Model\Accr\AccrSchoolAssessmentComment;
use App\Model\Accr\AccrTimeline;
use App\Model\Accr\AuditQuality;
use App\Model\Accr\Functions;
use App\Model\Accr\Graduate;
use App\Model\Accr\ProgramsOffered;
use App\Model\Accr\QualityArea;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SchoolSelfAssessimentStatus;
use App\Model\Accr\SchoolType;
use App\Model\Accr\StaffAdministrator;
use App\Model\Accr\Trainer;
use App\Model\Accr\WdaProvisionalAQA;
use App\Repositories\Assessment\GetDataAssessmentScores;
use App\StaffsInfo;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Protection;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Level;
use Yajra\DataTables\DataTables;

class FinalAQAController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('wda');
    }

    public function view(Request $request)
    {
        $apps = AccrApplication::join('schools_information', 'schools_information.id', '=', 'accr_applications.school_id')
            ->select('schools_information.school_name', 'schools_information.district', 'accr_applications.*', 'accreditation_types.type')
            ->join('accreditation_types', 'accr_applications.accreditation_type_id', '=', 'accreditation_types.id')
            ->orderBy('accr_applications.created_at', 'desc')
            ->get();

        return view('wda.schools.application.view', compact('apps'));
    }

    public function viewMore($application_id, $school, Request $request)
    {
        if (isset($request->view_year)) {
            $graduates = Graduate::where('school_id', $school)->where('year', $request->view_year)->get();
        } else {
            $graduates = [];
        }

        $school_info = SchoolInformation::where('id', $school)->first();
        $attachments_data = AccrAttachment::where('application_id', $application_id)->get();
        $buildings_plots = AccrBuildingsAndPlots::where('school_id', $school)->get();
//        $programs_offered = ProgramsOffered::where('school_id', $school)->get(); #not applicable
        $programs_offered = Department::where('school_id', $school)->get();
//        $trainers = Trainer::where('school_id', $school)->get(); #not applicable
//        $staffs = StaffAdministrator::where('school_id', $school)->get(); #not applicable

        $trainers = StaffsInfo::where('school_id', $school_info->id)
            ->where('staff_category', 'Academic')->get();
        //        $trainers                  = Trainer::where('school_id', auth()->user()->school_id)->get();
        $staffs = StaffsInfo::where('school_id', $school_info->id)
            ->where('staff_category', 'Administrative')
            ->get();

        $application_data = AccrQualificationsApplied::where('application_id', $application_id)
            ->whereNull('decision')
            ->get();

        $criteria_sections = AccrCriteriaSection::get();

        $app = AccrApplication::find($application_id);

        return view('wda.schools.application.view_more', compact('school_info', 'attachments_data', 'buildings_plots', 'programs_offered', 'trainers', 'staffs', 'application_data', 'application_id', 'criteria_sections', 'graduates'));
    }

    public function makeDecision(Request $request)
    {
        $school = $request->school;
        $qualificaton = $request->qualification_id;
        $application = $request->application_id;
        $dataId = $request->application_qualification;
        $applicationData = AccrQualificationsApplied::where('id', $dataId)
            ->where('application_id', $application)
            ->where('curriculum_qualification_id', $qualificaton)
            ->whereNull('decision')
            ->first();
        if ($applicationData) {
            $app = $applicationData->application;
            if ($app) {
                if ($app->school_id == $school) {
                    if ($app->school) {
                        $skl = $app->school;
                        if ($request->input('reject')) {
                            $applicationData->decision = "Not Accredited";
                            $applicationData->save();
                            $ad = $applicationData;
                            $data = [
                                'school' => $skl->school_name,
                                'sector' => $ad->curr->subsector->sector->tvet_field,
                                'sub_sector' => $ad->curr->subsector->sub_field_name,
                                'qualification' => $ad->curr->qualification_title,
                                'rtqf' => $ad->rtqf->level_name,
                                'type' => $app->accreditationType->type,
                                'action' => "Not Accredited"
                            ];
                            if ($skl->email)
                                Mail::to($skl->email)->send(new SendSchoolApplicationFeedbackNotAccreditedMail($data));
                        } elseif ($request->input('accepted')) {
                            $applicationData->decision = "Accredited";
                            if ($applicationData->save()) {
                                $ad = $applicationData;
                                $qualification = $ad->curr ? $ad->curr->uuid : null;
                                $rqtf = $ad->rtqf ? $ad->rtqf->uuid : null;
                                $school = $skl->id;
                                if ($qualification && $rqtf && $school) {
                                    $check = Department::where('school_id', $school)
                                        ->where('qualification_id', $qualification)
                                        ->first();
                                    if (is_null($check)) {
                                        $data = [
                                            'school_id' => $school,
                                            'qualification_id' => $qualification,
                                            'staffs_info_id' => null,
                                            'status' => true,
                                            'capacity_students' => 120,
                                            'updateVersion' => updateVersionColumn('departments', false, $school)
                                        ];
                                        Department::create($data);
                                    }
                                    $dept = Department::where('school_id', $school)
                                        ->where('qualification_id', $qualification)
                                        ->first();
                                    if (isset($dept)) {
                                        if (isset($rqtf)) {
                                            $depID = $dept->id;
                                            $check = Level::where('school_id', $school)
                                                ->where('department_id', $depID)
                                                ->where('rtqf_id', $rqtf)
                                                ->first();
                                            if (is_null($check))
                                                Level::create([
                                                    'school_id' => $school,
                                                    'department_id' => $depID,
                                                    'rtqf_id' => trim($rqtf),
                                                    'updateVersion' => updateVersionColumn('levels', false, $school)
                                                ]);
                                        }
                                    }
                                    $data = [
                                        'school' => $skl->school_name,
                                        'sector' => $ad->curr->subsector->sector->tvet_field,
                                        'sub_sector' => $ad->curr->subsector->sub_field_name,
                                        'qualification' => $ad->curr->qualification_title,
                                        'rtqf' => $ad->rtqf->level_name,
                                        'type' => $app->accreditationType->type,
                                        'action' => "Accredited"
                                    ];
                                    if ($skl->email)
                                        Mail::to($skl->email)->send(new SendSchoolApplicationFeedbackAccreditedMail($data));
                                }
                            }
                        }
                        return back()->with(['status' => '1', 'message' => 'Decision Made Successfully ...']);
                    } else
                        return back()->with(['status' => '0', 'message' => 'Oops, School not available !!']);
                } else
                    return back()->with(['status' => '0', 'message' => 'Oops, School not available !!']);
            } else
                return back()->with(['status' => '0', 'message' => 'Oops, Application not available !!']);
        } else
            return back()->with(['status' => '0', 'message' => 'Applied Qualification not find !!!']);
    }

    public function saveData(Request $request)
    {
        if (isset($request->form)) {

            $app = AccrApplication::find($request->application_id);

            if (!$app)
                return abort(404);

            switch ($request->form) {
                case 'attachments':

                    if (isset($request->verified)) {
                        for ($i = 0; $i < count($request->verified); $i++) {
                            $att = $request->class::find($request->attachment_id[$i]);
                            $att->wda_verified = $request->verified[$i];
                            $att->save();
                        }
                    }

                    $check_update = WdaProvisionalAQA::where('application_id', $request->application_id)
                        ->where('section', $request->section)
                        ->where('step', $request->step)->first();

                    if ($check_update) {
                        $check = WdaProvisionalAQA::find($check_update->id);
                    } else {
                        $check = new WdaProvisionalAQA();
                        $check->application_id = $request->application_id;
                        $check->section = $request->section;
                        $check->step = $request->step;
                    }

                    $check->strength = $request->strength;
                    $check->weakness = $request->weakness;
                    $check->threat = $request->threat;
                    $check->opportunity = $request->opportunity;
                    $check->recommendation = $request->recommendation;
                    $check->marks = $request->marks;
                    $check->save();
//                    return redirect('/qm-manual/wda/view/1/#step-1-' . ($request->step + 1));
                    return redirect(route('wda.view.accr.application', [$request->application_id, $app->school_id]) . "?tab=" . ($request->step + 1));

                    break;

                case 'general_recommendation':

                    $check_update = WdaProvisionalAQA::where('application_id', $request->application_id)
                        ->where('section', 1)
                        ->where('step', 8)
                        ->first();

                    if ($check_update) {
                        $check = WdaProvisionalAQA::find($check_update->id);
                    } else {
                        $check = new WdaProvisionalAQA();
                        $check->application_id = $request->application_id;
                        $check->section = 1;
                        $check->step = 8;
                    }

                    $check->recommendation = $request->recommendation;
                    $check->marks = $request->marks;

                    $check->save();

//                    return redirect('/qm-manual/wda/view/1/#step-1-' . ($request->step + 1));
                    return redirect(route('wda.view.accr.application', [$request->application_id, $app->school_id]) . "?tab=8");

                    break;
            }
        }
    }

    public function assessmentView()
    {
        $functionInput = Functions::findOrFail(1);
        $functionProcess = Functions::findOrFail(2);
        $qualityInput = $functionInput->qualityAreas;
        $qualityProcess = $functionProcess->qualityAreas;

        $request = request()->input();
        $parms = null;
        if (count($request) >= 0) {
            $parms = http_build_query($request);
        }
        $tb = isset($request['tb']) ? $request['tb'] : null;
        if (count($request) <= 0 || $tb == 'i')
            $url = route('wda.get.school.assessment', 'i');
        elseif ($tb == 'p')
            $url = route('wda.get.school.assessment', 'p');
        elseif ($tb == 'm')
            $url = route('wda.ajax.get.assessment.status');
        if ($parms != null)
            $url = $url . "?" . $parms;

        $schoolTypes = SchoolType::all();
        $functions = Functions::all() ?: [];
        if ($functions)
            $functions = $functions->pluck('name', 'id');
        $ctb = null;
        if ($tb == 'i' || $tb == null)
            $ctb = '1';
        elseif ($tb == 'p')
            $ctb = '2';

        return view('wda.schools.application.assessment-view', compact('qualityInput', 'qualityProcess', 'url', 'schoolTypes', 'request', 'functions', 'ctb', 'parms', 'functionInput', 'functionProcess'));
    }

    public function exportingSchoolAssessments()
    {
        $request = request();
        $ex = [
            'id',
            'school_id',
            'function_id',
            "school_confirm",
            "district_confirm",
            "wda_confirm",
            "academic_year",
            "created_at",
            "updated_at",
            "submit_id",
            'rating',
            'stage',
            'accreditation',
            'button'
        ];
        $repo = new GetDataAssessmentScores(request(), $request->input('tb') ?: 'i', $ex, false);
        $export = $repo->getResults();
        $export = collect($export);
        if ($export->count() <= 0)
            return "<script>alert('The is no data to export');window.close();</script>";

        return (new FastExcel($export))->download('school_assessment_' . time() . '.xlsx');
    }

    public function assessmentViewAnswers()
    {
        $schoolTypes = SchoolType::all();
        $functions = Functions::all() ?: [];

        if ($functions)
            $functions = $functions->pluck('name', 'id');
        $inputs = request()->input();
        if (count($inputs)) {
            $schools = DB::table('accr_schools_information');

            if (request()->input('school'))
                $schools->where('id', request()->input('school'));
            if (request()->input('province') && !request()->input('district'))
                $schools->where('province', request()->input('province'));
            if (request()->input('district'))
                $schools->where('district', request()->input('district'));
            if (request()->input('school_type'))
                $schools->where('school_type', request()->input('school_type'));
            if (request()->input('school_status'))
                $schools->where('school_status', request()->input('school_status'));

            $schools = $schools->orderBy('school_name', 'ASC')->select(['id', 'school_name'])->get();

            $indicator = AccrCriteria::join('accr_criteria_sections', 'accr_criterias.criteria_section_id', '=', 'accr_criteria_sections.id')
                ->join('quality_areas', 'accr_criteria_sections.quality_area_id', '=', 'quality_areas.id')
                ->join('functions', 'quality_areas.function_id', '=', 'functions.id')
                ->where('functions.id', '=', request()->input('functions'));

            if (request()->input('quality_area') && !request()->input('criteria'))
                $indicator->where('quality_areas.id', '=', request()->input('quality_area'));

            if (request()->input('criteria') && !request()->input('indicator'))
                $indicator->where('accr_criteria_sections.id', '=', request()->input('criteria'));

            if (request()->input('indicator'))
                $indicator->where('accr_criterias.id', '=', request()->input('indicator'));

            $indicators = $indicator
                ->orderBy('quality_areas.name', 'ASC')
                ->orderBy('accr_criteria_sections.criteria_section', 'ASC')
                ->orderBy('accr_criterias.criteria', 'ASC')
                ->select([
                    'accr_criterias.id',
                    'accr_criterias.criteria',
                    'accr_criteria_sections.criteria_section',
                    'quality_areas.name AS quality_areas_name',
                    'functions.name AS function_name',
                ])->get();
            return view('wda.schools.application.assessment-answers', compact('schoolTypes', 'functions', 'schools', 'indicators'));
        }

        return view('wda.schools.application.assessment-answers', compact('schoolTypes', 'functions'));
    }

    public function getDataAssessments($type, $dataTables = true)
    {
        $request = request();
        $submitedInputs = DB::table('accr_school_self_assessiment_statuses')
            ->join('accr_schools_information', 'accr_school_self_assessiment_statuses.school_id', '=', 'accr_schools_information.id')
            ->join('accr_functions', 'accr_school_self_assessiment_statuses.function_id', '=', 'accr_functions.id')
            ->join('accr_source_school_type', 'accr_schools_information.school_type', '=', 'accr_source_school_type.id')
            ->where('accr_school_self_assessiment_statuses.academic_year', '=', date('Y'));
        if ($type == 'i') {
            $functionInput = Functions::findOrFail(1);
            $qualityInput = $functionInput->qualityAreas;
            $submitedInputs->where('accr_functions.id', '=', 1);
        } elseif ($type == 'p') {
            $functionInput = Functions::findOrFail(2);
            $qualityInput = $functionInput->qualityAreas;
            $submitedInputs->where('accr_functions.id', '=', 2);
        } else {
            return json_encode([]);
        }
        $submitedInputs->select([
            'accr_school_self_assessiment_statuses.*',
            'accr_schools_information.school_name',
            'accr_schools_information.province',
            'accr_schools_information.district',
            'accr_schools_information.school_status',
            'accr_source_school_type.school_type',
            'accr_source_school_type.id as school_type_id',
            'accr_functions.name',
        ]);
        if ($request->input('school'))
            $submitedInputs->where('accr_schools_information.id', 'LIKE', $request->input('school'));
        if ($request->input('school_type'))
            $submitedInputs->where('accr_source_school_type.id', '=', $request->input('school_type'));
        if ($request->input('school_status'))
            $submitedInputs->where('accr_schools_information.school_status', '=', $request->input('school_status'));
        if ($request->input('province') && !$request->input('district'))
            $submitedInputs->where('accr_schools_information.province', '=', $request->input('province'));
        if ($request->input('district'))
            $submitedInputs->where('accr_schools_information.district', '=', $request->input('district'));
        $submitedInputs
            ->orderBy('accr_school_self_assessiment_statuses.created_at', 'ASC');
        $dataTables = DataTables::of($submitedInputs);
        $pw = 0;
        $school = null;
//        $dataTables->addColumn('school_scores', function ($assessment) use ($qualityInput) {
//            $tt = getFunctionScores($qualityInput, $assessment->school_id, true, $assessment->submit_id);
//            $hv = getFunctionScores($qualityInput, $assessment->school_id, false, $assessment->submit_id);
//            $pp = ($hv * 100) / $tt;
//            $pp = ceil($pp);
//            return $pp;
//        });
        $dataTables->addColumn('wda_scores', function ($assessment) use ($qualityInput, $request) {
            $tt = getFunctionScores($qualityInput, $assessment->school_id, true, $assessment->submit_id, 'wda', $request->input('year'));
            $hv = getFunctionScores($qualityInput, $assessment->school_id, false, $assessment->submit_id, 'wda', $request->input('year'));
            $pp = ($hv * 100) / $tt;
            $pp = ceil($pp);
            return $pp;
        });
//        $dataTables->addColumn('district_scores', function ($assessment) use ($qualityInput) {
//            global $school;
//            $tt = getFunctionScores($qualityInput, $school, true, $assessment->submit_id, 'district');
//            $hv = getFunctionScores($qualityInput, $school, false, $assessment->submit_id, 'district');
//            $pp = ($hv * 100) / $tt;
//            $pp = ceil($pp);
//            return $pp;
//        });
//        $dataTables->addColumn('rating', function ($assessment) {
//            global $pw;
//            $class = getRattingScore($pw, 'c');
//            $rating = getRattingScore($pw);
//            return '<label class="label label-' . $class . ' text-sm p-1 rounded shadow-sm">' . $rating . '</label>';
//        });
//        $dataTables->addColumn('stage', function ($assessment) {
//            if ($assessment->district_confirm == null):
//                return '<label class="label label-info p-1 rounded shadow-sm" > At district </label>';
//            elseif ($assessment->wda_confirm == null):
//                return '<label class="label label-info p-1 rounded shadow-sm" > At WDA </label>';
//            else:
//                return '<label class="label label-info p-1 rounded shadow-sm" > Viewed</label>';
//            endif;
//        });
//        $dataTables->addColumn('accreditation', function ($assessment) {
//            if ($assessment->district_confirm == 1 && $assessment->wda_confirm == 1):
//                return '<label class="label label-success p-1 rounded shadow-sm">Done</label>';
//            else:
//                return '<label class="label label-warning p-1 rounded shadow-sm">In Process</label>';
//            endif;
//        });
        $dataTables->addColumn('button', function ($assessment) {
            $url = route('wda.sorting.school.assessment', [$assessment->function_id, $assessment->school_id, $assessment->submit_id]);
            $msg = "Are sure you want to delete ...\n" . $assessment->school_name;
            $urlD = route('wda.school.delete.assessment');
            $action = '<form method="POST" data-msg="' . $msg . '" id="formDeleteAssessment">' . '' .
                '<input type="hidden" name="_method" value="delete">
                <input type="hidden" name="_d_ass" value="' . $assessment->id . '">        
                <input type="hidden" name="_d_school" value="' . $assessment->school_id . '">        
                <input type="hidden" name="_d_submit" value="' . $assessment->submit_id . '">        
                <input type="hidden" name="_d_function" value="' . $assessment->function_id . '">        
                <button title="Delete ' . $assessment->school_name . '" 
                class="btn btn-danger btn-sm btn-block mt-1">
                Delete 
                </button></form>';
            return '<a href="' . $url . '" class="btn btn-sm btn-dark btn-block">View</a>' . $action;
        });
        if ($qualityInput) {
            foreach ($qualityInput as $quality) {
                if ($request->input('quality_area')) {
                    if ($request->input('quality_area') != $quality->id)
                        continue;
                }
                $dataTables->addColumn(strtolower(str_replace(' ', '_', $quality->name)), function ($assessment) use ($quality, $request) {
                    return getQualityAreaScores($quality->criteria, $assessment->school_id, $assessment->submit_id, $request->input('year'));
                });
                foreach ($quality->criteria as $criteria_section) {
                    if (request()->input('all_criteria') != 'y' && !request()->input('criteria')):
                        break;
                    endif;
                    if ($request->input('criteria')) {
                        if ($request->input('criteria') != $criteria_section->id)
                            continue;
                    }
                    $dataTables->addColumn(strtolower(str_replace(' ', '_', $criteria_section->criteria_section)), function ($assessment) use ($criteria_section, $request) {
                        return getCriteriaScores($criteria_section['criterias'], $assessment->school_id, $assessment->submit_id, $request->input('year'));
                    });
                }
            }
        }
        return $dataTables
            ->rawColumns(['button', 'accreditation', 'stage', 'rating'])
            ->make(true);
    }

    public function deleteSchoolAssessments(Request $request)
    {
        $id = $request->input('_d_ass');
        $school = $request->input('_d_school');
        $submit = $request->input('_d_submit');
        $function = $request->input('_d_function');
        $assements = SchoolSelfAssessimentStatus::where('id', $id)
            ->where('school_id', $school)
            ->where('submit_id', $submit)
            ->where('function_id', $function)->first();
        if ($assements) {
            $aswers = AccrCriteriaAnswer::where('submit_id', $assements->submit_id)
                ->where('school_id', $assements->school_id)->get();
            $ids = null;
            if ($aswers)
                $ids = $aswers->pluck('id')->toArray();
            if ($ids != null)
                AccrCriteriaAnswer::destroy($ids);
            $assements->delete();
            return back()->with(['status' => '1', 'message' => 'Deleted successfully and all related records ...']);
        } else
            return back()->with(['status' => '3', 'message' => 'Deleting failed !!']);
    }

    public function assessmentViewSchool($fun, $school, $sub = null)
    {
        $funct = Functions::findOrFail($fun);
        $school = SchoolInformation::findOrFail($school);
        if ($sub != null)
            $submitted = SchoolSelfAssessimentStatus::where('school_id', $school->id)
                ->where('submit_id', $sub)
                ->orderBy('created_at', 'DESC');
        else
            $submitted = SchoolSelfAssessimentStatus::where('school_id', $school->id)->orderBy('created_at', 'DESC');

        $selectDate = [];
        $firstNumberId = $submitted->first();
        if ($firstNumberId)
            $firstNumberId = $firstNumberId->submit_id;
        foreach ($submitted->get() as $item) {
            $selectDate[$item->submit_id] = date('M d, Y', strtotime($item->created_at));
        }
        $qualities = [];
        if ($funct->qualityAreas)
            $qualities = $funct->qualityAreas;

        return view('wda.schools.application.view-school-assessment', compact('school', 'funct', 'qualities', 'selectDate', 'firstNumberId', 'sub'));
    }

    public function storeAssessmentSchool(Request $request, $fun, $school)
    {
        $this->validate($request, [
            'school_id' => 'required',
            'function_id' => 'required',
            'quality_id' => 'required',
            'strength' => 'required | min:2',
            'weakness' => 'required | min:2',
            'recommendation' => 'required | min:2',
            'timeline' => 'required | min:2',
            'wda.*' => 'required'
        ]);

        $wda = $request->wda;
        $school = $request->school_id;

        $request->request->add(['from' => 'wda', 'academic_year' => date('Y')]);
        $dataComment = $request->except(['wda', '_token']);

        $vldD = 0;
        if (count($wda)) {
            foreach ($wda as $criteria => $decision) {
                $answer = AccrCriteriaAnswer::where('school_id', $school)
                    ->where('academic_year', date('Y'))
                    ->where('criteria_id', $criteria)
                    ->first();
                if ($answer) {
                    $answer->wda = $decision;
                    if ($answer->save())
                        $vldD++;
                }
            }
        }
        $comment = getCommentsWda($school, $request->function_id, $request->quality_id, null, true);
        if ($comment === false)
            AccrSchoolAssessmentComment::create($dataComment);
        else {
            if ($comment->update($dataComment))
                return back()->with(['successMessage' => 'Successfully updated . ']);
        }

        return back()->with(['successMessage' => 'Successfully saved . ']);
    }

    public function qmmanual($timeline = null)
    {
        $procedure = AccrProcedure::all()->first();
        $type = AccrAccreditationType::all()->first();
        $responsibility = AccrResponsibilite::all()->first();
        $timelines = AccrTimeline::all();
        $rekod = null;
        if (isset($timeline))
            $rekod = AccrTimeline::findOrFail($timeline);

        return view('wda.schools.application.qmmanual-create', compact('procedure', 'type', 'responsibility', 'timelines', 'rekod'));
    }

    public function toPrint($application_id)
    {
        $school_info = SchoolInformation::where('id', auth()->user()->school_id)->first();
        $attachments_data = AccrAttachment::where('application_id', $application_id)->get();
        $buildings_plots = AccrBuildingsAndPlots::where('school_id', auth()->user()->school_id)->get();
        $programs_offered = ProgramsOffered::where('school_id', auth()->user()->school_id)->get();
        $trainers = Trainer::where('school_id', auth()->user()->school_id)->get();
        $staffs = StaffAdministrator::where('school_id', auth()->user()->school_id)->get();
        $application_data = AccrQualificationsApplied::where('application_id', $application_id)->get();

        return view('wda.school.application.print', compact('school_info', 'attachments_data', 'buildings_plots', 'programs_offered', 'trainers', 'staffs', 'application_data', 'application_id'));
    }

    public function exportImport()
    {
        $schools = SchoolInformation::all();
        $functs = Functions::all();
//        dispatch(new ScoresCalculateJob());
        return view('wda.schools.accr.export_import', compact('schools', 'functs'));
    }

    public function formatToExport(Request $request)
    {
        $funct = Functions::find($request->input('function'))->name;
        $filename = seoUrl(strtolower($funct)) . "_" . time();
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        Excel::create($filename, function ($excel) use ($request) {
            $excel->sheet('sheet_1', function ($sheet) use ($request) {
                $funct = Functions::find($request->input('function'));
                $qualities = [];
                $count = 2000;
                if ($funct->qualityAreas)
                    $qualities = $funct->qualityAreas;
                $sheet->loadView('wda.schools.accr.export.exporting', compact('funct', 'qualities'));
                $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                $sheet->setAutoSize(array(
                    'B', 'C', 'D', 'E', 'F', 'G'
                ));
                $sheet->getProtection()->setPassword('wda@9012');
                $sheet->getProtection()->setSheet(true);
                $sheet->getStyle('D2:G' . $count)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
            });
        })->download('xlsx');

    }

    public function formatToImport(Request $request)
    {
        $filer = $request->file('file');

        $lines = (new FastExcel)->import($filer->getRealPath());

        $submitId = time();
        $clines = 0;
        $dlines = 0;

        $formatInvalid = false;
        foreach ($lines as $line) {
            if (isset($line['SCHOOL']) && isset($line["DISTRICT"]) && isset($line["WDA"])) {
                if (strlen($line['SCHOOL']) > 0 && strlen(trim($line["DISTRICT"])) > 0 && strlen(trim($line["WDA"])) > 0) {
                    $answers = AccrCriteriaAnswer::where('school_id', $request->input('school'))
                        ->where('criteria_id', $line['identifier'])
                        ->where('academic_year', date('Y'));
                    $answers = $answers->first();
                    if (!$answers)
                        $answers = new  AccrCriteriaAnswer();

                    $answers->school_id = $request->input('school');
                    $answers->criteria_id = $line['identifier'];
                    $answers->answer = trim($line['SCHOOL']);
                    $answers->submit_id = $submitId;
                    $answers->district = trim($line["DISTRICT"]);
                    $answers->wda = trim($line["WDA"]);
                    $answers->academic_year = date('Y');
                    $answers->save();
                    $clines++;
                } else
                    $dlines++;
            } else {
                $formatInvalid = true;
                break;
            }
        }

        if ($formatInvalid)
            return back()->with(['status' => '0', 'message' => 'Format is not valid !!']);

        if ($clines > 0)
            SchoolSelfAssessimentStatus::updateOrCreate([
                'school_id' => $request->input('school'),
                'function_id' => $request->input('function'),
                'academic_year' => date('Y')
            ], [
                'school_id' => $request->input('school'),
                'function_id' => $request->input('function'),
                'school_confirm' => 1,
                'district_confirm' => 1,
                'wda_confirm' => 1,
                'academic_year' => date('Y'),
                'submit_id' => $submitId
            ]);
        if ($dlines > 0)
            return back()->with(['status' => '0', 'message' => 'Please Check again if you did n\'t miss any necessary cell in these columns SCHOOL,DISTRICT,WDA']);
        else
            return back()->with(['status' => '1', 'message' => 'Uploaded Successfully !']);
    }

    public function viewCertificate(Request $request, $school)
    {
        $school = SchoolInformation::findOrFail($school);
        return view('wda.certificate',compact('school'));
    }

    public function ajaxDataQualityArea()
    {
        $q = QualityArea::all() ?: [];
        if (request()->input('f')) {
            $q = QualityArea::where('function_id', request()->input('f'))->get();
            if ($q->count() <= 0)
                $q = [];
        }
        if (!empty($q))
            return response()->json($q->pluck('name', 'id')->toArray());
        else
            return response()->json([]);
    }

    public function schoolAssessmentStatus()
    {
        $schools = DB::table('accr_schools_information')
            ->join('accr_source_school_type', 'accr_schools_information.school_type', '=', 'accr_source_school_type.id');

        if (request()->input('school'))
            $schools->where('accr_schools_information.id', request()->input('school'));
        if (request()->input('province') && !request()->input('district'))
            $schools->where('accr_schools_information.province', request()->input('province'));
        if (request()->input('district'))
            $schools->where('accr_schools_information.district', request()->input('district'));
        if (request()->input('school_type'))
            $schools->where('accr_schools_information.school_type', request()->input('school_type'));
        if (request()->input('school_status'))
            $schools->where('accr_schools_information.school_status', request()->input('school_status'));

        $schools = $schools->orderBy('school_name', 'ASC')
            ->select([
                'accr_schools_information.id',
                'accr_schools_information.school_name',
                'accr_schools_information.province',
                'accr_schools_information.district',
                'accr_schools_information.school_status',
                'accr_source_school_type.school_type'
            ]);
        $dataTable = DataTables::of($schools);
        $assessment = SchoolSelfAssessimentStatus::where('academic_year', date('Y'));
        $dataTable->addColumn('function_input', function ($school) {
            $assessment = SchoolSelfAssessimentStatus::where('academic_year', date('Y'))
                ->where('school_id', $school->id)
                ->where('function_id', 1)->get()->count();
            return $assessment > 0 ? 'YES' : 'NO';
        });
        $dataTable->addColumn('function_process', function ($school) use ($assessment) {
            $assessment = SchoolSelfAssessimentStatus::where('academic_year', date('Y'))
                ->where('school_id', $school->id)
                ->where('function_id', 2)->get()->count();
            return $assessment > 0 ? 'YES' : 'NO';
        });

        return $dataTable
            ->make(true);

    }

    public function summary(Request $request)
    {
        $school = $request->input('school');
        if ($school != null) {
            $qualities = AuditQuality::all();
            $school = SchoolInformation::find($school);
            if (!$school)
                return abort(404);
        }
        return view('wda.schools.manage.audit.summary', compact('qualities', 'school'));
    }

    public function uploadSummary(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'summary' => 'required|mimes:doc,pdf,docx'
        ]);

        if ($validator->fails())
            return back()->with(['status' => 0, 'message' => $validator->errors()->all()]);

        if ($request->input('school_id')) {
            $ext = $request->file('summary')->getClientOriginalExtension();
            $path = $request->file('summary')->storeAs(
                'audit/documents/' . $request->school_id . '/' . date('Y_F_d'), $request->school_id . '.' . $ext, 'public'
            );
        }

        $time = rand(2, 6);
        sleep($time);

        return back()->with(['status' => 1, 'message' => 'Uploaded successfully ...']);
    }

    public function overall(Request $request)
    {
        $functions = Functions::all() ?: [];

        if ($functions)
            $functions = $functions->pluck('name', 'id');

        if ($request->input('function'))
            $_function = Functions::find($request->input('function'));

        $provinces = DB::table('rwanda_boundaries');

        if ($request->input('province'))
            $provinces->where('Province', $request->input('province'));

        $provinces->distinct('District');
        $provinces = $provinces->select(['Province', 'District'])->get();
        $provinces = $provinces->groupBy('Province');

        $parms = "?" . http_build_query($request->input());
        return view('wda.schools.application.overall', compact('functions', '_function', 'provinces', 'parms'));
    }

    public function getOverall()
    {
        $dd = getPercentageForIndicator(
            request()->input('indicator'),
            request()->input('cond'),
            request()->input('needed'),
            request()->input('district')
        );
        echo $dd;
    }

    public function exportOverall()
    {
        $request = request();

        $provinces = DB::table('rwanda_boundaries');
        if ($request->input('province'))
            $provinces->where('Province', $request->input('province'));

        $provinces->distinct('District');
        $provinces = $provinces->select(['Province', 'District'])->get();
        $provinces = $provinces->groupBy('Province');

        if ($request->input('function'))
            $_function = Functions::find($request->input('function'));

        Excel::create('assessment-overall-report', function ($excel) use ($provinces, $_function) {
            $excel->sheet('sheet1', function ($sheet) use ($provinces, $_function) {
                $sheet->loadView('wda.schools.application.overall-export')->with('provinces', $provinces)
                    ->with('_function', $_function);
                $sheet->setOrientation('landscape');
            });
        })->download('xlsx');

//        return view('wda.schools.application.overall-export', compact('provinces', '_function'));
    }

}
