<?php

namespace App\Http\Controllers\WdaAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OwenIt\Auditing\Models\Audit;

class AuditController extends Controller
{
    public function index() {
        $audits = Audit::orderBy('created_at', 'desc')->paginate(100);

        return view('wda.audit.index', compact('audits'));
        
    }
}
