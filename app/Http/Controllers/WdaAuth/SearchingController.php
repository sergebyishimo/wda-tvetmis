<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\TrainingSectorsSource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchingController extends Controller
{
    function __construct()
    {
        $this->middleware('wda');
    }

    public function index(Request $request)
    {
        $results = null;
        $view = null;
        $colms = [];

        if ($request->has('model')) {
            $colms = $request->input('colms') ?: [];
            $model = $request->get('model');
            $column = $request->get('column');
            $condition = $request->get('condition');
            $value = $request->get('value');
            $qualification = $request->has('qualification') ? $request->get('qualification') : null;
            $process = $this->processingResults($model, $column, $condition, $value, $qualification);
            $results = $process['model'];
            $view = $process['view'];

        }
        $sectors = TrainingSectorsSource::all();
        $qualifications = CurriculumQualification::all();

        return view('wda.search.index', compact('sectors', 'results', 'view', 'colms', 'qualifications'));
    }
  public function filter(Request $request)
    {
        $results = null;
        $view = null;
        $colms = [];

        if ($request->has('model')) {
            $colms = $request->input('colms') ?: [];
            $model = $request->get('model');
            $column = $request->get('column');
            $condition = $request->get('condition');
            $value = $request->get('value');
            $qualification = $request->has('qualification') ? $request->get('qualification') : null;
            $process = $this->processingResults($model, $column, $condition, $value, $qualification);
            $results = $process['model'];
            $view = $process['view'];

        }
        $sectors = TrainingSectorsSource::all();
        $qualifications = CurriculumQualification::all();

        return view('wda.search.filter', compact('sectors', 'results', 'view', 'colms', 'qualifications'));
    }

    public function processingResults($model, $column, $condition, $value, $qualification = null)
    {
        $nameModel = $model;

        if ($condition === "0") {
            if ($qualification != null) {
                $q = getModelFormKey($nameModel)->qualification($qualification);
                $model = $q->whereNull($column);
            } else
                $model = getModelFormKey($model)->whereNull($column);

            $model = $model->get();
        } elseif ($condition === "00") {

            if ($qualification != null) {
                $q = getModelFormKey($nameModel)->qualification($qualification);
                $model = $q->whereNotNull($column);
            } else
                $model = getModelFormKey($model)->whereNotNull($column);

            $model = $model->get();
        } elseif ($condition == "#") {

            if ($qualification != null) {
                $q = getModelFormKey($nameModel)->qualification($qualification);
                $model = $q->where($column, $value);
            } else
                $model = getModelFormKey($model)->where($column, $value);
            $model = $model->count();
        } else {
            if ($qualification != null) {
                $q = getModelFormKey($nameModel)->qualification($qualification);
                $model = $q->where($column, $condition, $value);
            } else
                $model = getModelFormKey($model)->where($column, $condition, $value);
            $model = $model->get();
        }

        if ($nameModel == 'school') {
            $template = "wda.search.models.school.dataTable";
            if ($condition == "#")
                $template = "wda.search.models.school.count";
        }

        return ['model' => $model, 'view' => $template];
    }

    public function searchBased($model, $cols = null)
    {
        $mTmp = $model;
        $model = getModelFormKey($model);
        $table = $model != null ? $model->getTableColumns() : [];
        $related = $model != null ? $model->relationships() : [];

        switch ($mTmp) {
            case "school":
                if ($cols != null) {
                    $b = $model->groupBy($cols)->select($cols)->pluck($cols, $cols);
                    if ($b)
                        return $b->toJson();
                } else {
                    if (count($table) > 0)
                        $this->deleteElement(['id', 'latitude', 'longitude', 'created_at', 'updated_at', 'sms', 'deleted_at'], $table);
                    $table = [
                        'table' => $table,
                        'related' => $related
                    ];
                    return json_encode($table);
                }
                break;
            case "staffs":

                break;
            default:
                return [];
                break;
        }
    }

    /**
     * Remove an element from an array.
     *
     * @param $elements
     * @param array $array
     */
    private function deleteElement($elements, &$array)
    {
        if (is_array($elements)) {
            foreach ($elements as $element) {
                $index = array_search($element, $array);
                if ($index !== false) {
                    unset($array[$index]);
                }
            }
        }
    }

}
