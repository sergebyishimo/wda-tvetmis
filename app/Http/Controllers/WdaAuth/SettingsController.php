<?php

namespace App\Http\Controllers\WdaAuth;

use App\Model\Accr\Notifications;
use App\Model\Accr\WelcomeMessages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('wda');
    }

    public function generalSettings($noti_id = null, Request $request)
    {
        if (isset($request->form)) {
            switch ($request->form) {
                case 'welcome':
                    if ($request->welcome_id == "") {
                        $welcome = new WelcomeMessages();
                        $welcome->message = $request->message;
                        $welcome->save();
                    } else {
                        $welcome = WelcomeMessages::find($request->welcome_id);
                        $welcome->message = $request->message;
                        $welcome->save();
                    }


                    return redirect()
                        ->route('wda.settings')
                        ->with(['status' => '1', 'message' => 'Welcome Message Saved Successfully']);

                    break;

                case 'notification':

                    if ($request->message) {
                        $notification = new Notifications();

                        $notification->message = $request->message;
                        $notification->save();
                    }

                    if ($request->u_message) {
                        $notification = Notifications::find($request->noti_id);
                        $notification->message = $request->u_message;
                        $notification->save();
                    }

                    if ($request->delete_noti) {
                        Notifications::find($request->delete_noti)->delete();

                        return redirect()
                            ->route('wda.settings')
                            ->with(['status' => '1', 'message' => 'Notification Deleted Successfully!!']);
                    }

                    return redirect()
                        ->route('wda.settings')
                        ->with(['status' => '1', 'message' => 'Notification Saved Successfully!!']);

                    break;

            }
        }

        $welcome = WelcomeMessages::first();
        $notifications = Notifications::all();

        if ($noti_id != null) {
            $notification_info = Notifications::find($noti_id);
            return view('wda.settings', compact('welcome', 'notifications', 'notification_info'));
        }

        return view('wda.settings', compact('welcome', 'notifications'));
    }
}
