<?php

namespace App\Http\Controllers\Voyager;

use App\Mail\SchoolAccountSuccessfullyCreated;
use App\SchoolSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Voyager\VoyagerBaseController as Controller;

use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;

use App\SchoolUser;

class SchoolsController extends Controller
{

    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            event(new BreadDataAdded($dataType, $data));

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            SchoolUser::create([
                'name' => $data->acronym,
                'email' => $data->email,
                'password' => bcrypt($data->password),
                'android_pass' => sha1($data->password),
                'privilege' => 2,
                'school_id' => $data->id
            ]);

            SchoolSetting::create([
                'active_period' => 1,
                'active_term' => 1,
                'school' => $data->id
            ]);

            $data = [
                'acronym' => $data->acronym,
                'email' => $data->email,
                'password' => $data->password
            ];

            Mail::to($data['email'])->send(new SchoolAccountSuccessfullyCreated($data));

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

}
