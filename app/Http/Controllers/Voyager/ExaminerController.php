<?php

namespace App\Http\Controllers\Voyager;

use App\Mail\WDAExaminerAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Voyager\VoyagerBaseController as Controller;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;

class ExaminerController extends Controller
{
    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);
        $password = date('Y').'.'.rand(1,100);
        $request->request->add(['password' => bcrypt($password)]);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            event(new BreadDataAdded($dataType, $data));

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            $data = [
                'name' => $request->name,
                'email' => $data->email,
                'password' => $password
            ];

            Mail::to($data['email'])->send(new WDAExaminerAccount($data));

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }
}
