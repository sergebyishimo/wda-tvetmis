<?php

namespace App\Http\Controllers\RebAuth;

use App\Attachment;
use App\Http\Requests\RebUpdateMyInfoRequest;
use App\Http\Requests\UpdateMyInfoRequest;
use App\Http\Requests\UpdateStaffRequest;
use App\Mail\SendAssessorSubmittionMail;
use App\Mail\SendCreatedStaffInfo;
use App\Mail\SendMarkingSubmittionMail;
use App\Reb;
use App\RebStaffMarkerApplication;
use App\RebStaffMarkingBackground;
use App\RebStaffProfileAttachmentsFinal;
use App\RebStaffTeachingExperience;
use App\RebStaffWorkexperience;
use App\SchoolUser;
use App\StaffAssessorApplication;
use App\StaffAssessorBackground;
use App\StaffMarkerApplication;
use App\StaffMarkingBackground;
use App\StaffProfileAttachmentsFinal;
use App\StaffsInfo;
use App\StaffTeachingExperience;
use App\StaffWorkexperience;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStaffRequest;
use Mail;

class StaffController extends Controller
{
    protected $guard = "reb";

    function __construct()
    {
        $this->middleware("reb");
    }

    /**
     * @return string
     */
    protected function guard()
    {
        return $this->guard;
    }


    /**
     * @param RebUpdateMyInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateInfo(RebUpdateMyInfoRequest $request)
    {
        $staff = Reb::findOrFail($request->id);

        if (!$staff)
            return abort(419);

        $tmpEmail = $staff->email;

        $imagePath = 'staffs/reb/photos/'.date('Y');
        $name = rand(100,100000)."_".time()."_".$staff->id;
        $photo = $staff->photo;

        if ($request->has('image')) {
            $photoR = $request->file('image');
            $ext = $photoR->getClientOriginalExtension();
            $photo = $photoR->storeAs($imagePath, $name . '.' . $ext, config('voyager.storage.disk'));
        }

        if ($request->has('academic_document')) {
            $fileA = $request->file('academic_document');
            $ext = $fileA->getClientOriginalExtension();
            $fileA->storeAs("staffs/documents/academic", $name . '.' . $ext, config('voyager.storage.disk'));
        }

        if ($request->has('certificate_document')) {
            $fileC = $request->file('certificate_document');
            $ext = $fileC->getClientOriginalExtension();
            $fileC->storeAs("staffs/documents/others", $name . '.' . $ext, config('voyager.storage.disk'));
        }

        $staff->name = $request->input('name');
        $staff->gender = $request->input("gender");
        $staff->email = $request->input('email');
        $staff->civil_status = $request->input('civil_status');
        $staff->photo = $photo;
        $staff->phone_number = $request->input('phone_number');
        $staff->nationality = $request->input('nationality');
        $staff->national_id_number = $request->input('national_id_number');
        $staff->province = $request->input('Province');
        $staff->district = $request->input('District');
        $staff->sector = $request->input('Sector');
        $staff->bank_name = $request->input('bank_name');
        $staff->bank_account_number = $request->input('bank_account_number');
        $staff->rssb_number = $request->input('rssb_number');
        //New Ones
        $staff->qualification = $request->input('qualification');
        $staff->institution = $request->input('institution');
        $staff->graduated_year = $request->input('graduated_year');
        //New
        $staff->school = $request->school;
        $staff->school_province = $request->school_province;
        $staff->school_district = $request->school_district;
        $staff->school_master = $request->school_master;
        $staff->school_email = $request->school_email;
        $staff->school_phone = $request->school_phone;

//        $staff->updateVersion = 1;

        if ($staff->save()) {
//            $message = "Your information has been recently updated. From " . $request->school);
//                sendSMS($request->input('phone_number'), $message, "Update Staff", "Reb");
            return back()->with(['status' => '1', "message" => "Successfully Updated."]);
        }


        return redirect()->back()->withInput()->with(['status' => '0', "message" => "Updating failed !!"]);

    }

    public
    function myInfo()
    {
        if (isAllowed(1) || isAllowed())
            abort(419);

        $staff = auth()->guard('reb')->user();

        return redirect(route('reb.home'));
    }

    public
    function attachment()
    {
        $attachementDesc = getAttachmentDescriptionForSelect();
        $myAttachement = getInfoReb('attachement');

        return view('reb.staff.attachments', compact('myAttachement', 'attachementDesc'));
    }

    public
    function postAttachment(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:pdf,jpeg,jpg,png',
            'description' => 'required'
        ]);

        $path = 'staffs/reb/'.date('Y').'/attachments';
        $name = rand(1, 10000) . '_' . time() . '_' . trim(str_replace(' ', "_", strtolower(getInfoReb('names'))));
        $file = null;

        $fileR = $request->file('file');
        $ext = $fileR->getClientOriginalExtension();
        $file = $fileR->storeAs($path, $name . '.' . $ext, config('voyager.storage.disk'));

        $att = new RebStaffProfileAttachmentsFinal();
        $att->staff_id = getInfoReb('id');
        $att->attachment_description = $request->description;
        $att->attachment = $file;

        $att->save();

        return back()->with(['status' => '1', 'message' => 'Uploaded Successfully.']);
    }


    public
    function markerApplication()
    {
        $currentMarker = getInfoReb('currentMarking');
        return view("reb.staff.application.marker", compact('currentMarker'));
    }

    public
    function storeMarkerApplication(Request $request)
    {
        $this->validate($request, [
            'education_program_id' => 'required',
            'trade_marked_before' => 'required',
            'first_subject_applied_for' => 'required',
            'first_subject_marked_before' => 'required',
            'second_subject_applied_for' => 'required_with:third_subject_applied_for'
        ]);

        $data = $request->except(['Submit']);
        $save = RebStaffMarkerApplication::create($data);

        if ($save) {
            $data = [
                'names' => getInfoReb('names'),
                'academic_year' => $request->academic_year,
                'trade_applied' => getSelectTradeMarker($request->education_program_id),
                'assessed_before' => getYesNo($request->trade_marked_before),
                'first_subject' => $request->first_subject_applied_for,
                'first_subject_before' => getYesNo($request->first_subject_marked_before),
            ];

            if ($request->has('second_subject_applied_for')) {
                $data['second_subject'] = $request->second_subject_applied_for;
                $data['second_subject_before'] = getYesNo($request->second_subject_marked_before);
            }

            if ($request->has('third_subject_applied_for')) {
                $data['third_subject'] = $request->third_subject_applied_for;
                $data['third_subject_before'] = getYesNo($request->third_subject_marked_before);
            }

            Mail::to(getInfoReb('email'))->send(new SendMarkingSubmittionMail($data));

            return back()->with(['status' => '1', 'message' => 'Application Submitted Successfully !!!']);
        }

        return back()->with(['status' => '0', 'message' => 'Application failed ...']);

    }

    public
    function cancelApplication(Request $request)
    {
        $staff = $request->staff_id;
        $year = $request->academic_year;
        $where = $request->where;

        if ($where == 'm') {
            $as = RebStaffMarkerApplication::where('staff_id', $staff)
                ->where('academic_year', $year);

            if ($as->count() > 0)
                $as->first()->delete();
        }

        return back()->with(['status' => '1', 'message' => 'Application Canceled Successfully !!']);
    }

    public
    function workingExperience()
    {
        $backgrounds = getInfoReb("workingExperience");
        return view('reb.staff.application.working-experience', compact('backgrounds'));
    }

    public
    function postWorkingExperience(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255|min:3',
            'institution' => 'required|min:3|max:255',
            'status' => 'required|integer',
            'responsibility' => 'required|min:3|max:255',
            'period_from' => 'required|before:today',
            'period_to' => 'required|after:period_from'
        ]);

        $data = $request->except(['_token']);

        $save = RebStaffWorkexperience::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => 'Working experience added successfully.']);

        return back()->with(['status' => '0', 'message' => 'Adding experience failed.']);
    }

    public
    function teachingExperience()
    {
        $backgrounds = getInfoReb("teachingExperience");
        return view('reb.staff.application.teaching-experience', compact('backgrounds'));
    }

    public
    function postTeachingExperience(Request $request)
    {
        $this->validate($request, [
            'institution_taught' => 'required|max:255|min:3',
            'subject_taught' => 'required|min:3|max:255',
            'tvet_sub_field' => 'required',
            'class_or_level_taught' => 'required|integer',
            'from_date' => 'required|before:today',
            'to_date' => 'required|after:from_date'
        ]);

        $data = $request->except(['_token']);

        $save = RebStaffTeachingExperience::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => 'Teaching experience added successfully.']);

        return back()->with(['status' => '0', 'message' => 'Adding experience failed.']);
    }

    public
    function markingBackground()
    {
        $backgrounds = getInfoReb("markingBackgrounds");
        return view('reb.staff.application.marking-background', compact('backgrounds'));
    }

    public
    function postMarkingBackground(Request $request)
    {
        $this->validate($request, [
            'academic_year' => 'required',
            'marking_position' => 'required|integer',
            'marking_institution' => 'required',
            'subject_exam_marked' => 'required|integer',
            'education_program' => 'required|integer',
            'marking_center' => 'required|min:4|max:255'
        ]);

        $data = $request->except(['_token']);

        $save = RebStaffMarkingBackground::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => 'Marking background added successfully.']);

        return back()->with(['status' => '0', 'message' => 'Adding background failed.']);
    }

    public
    function assessorBackground()
    {
        $backgrounds = getInfoReb("assessorBackgrounds");
        return view('reb.staff.application.assessor-background', compact('backgrounds'));
    }

    public
    function destroyBackgrounds($id, $model)
    {
        switch ($model) {
            case 'mb':
                $model = RebStaffMarkingBackground::findOrFail($id);
                break;
            case 'te':
                $model = RebStaffTeachingExperience::findOrFail($id);
                break;
            case 'we':
                $model = RebStaffWorkexperience::findOrFail($id);
                break;
            case "atc":
                $model = RebStaffProfileAttachmentsFinal::findOrFail($id);
                break;
            default:
                return back();
                break;
        }
        $model->delete();
        return back()->with(['status' => '1', 'message' => 'Deleted successfully !!']);
    }

}
