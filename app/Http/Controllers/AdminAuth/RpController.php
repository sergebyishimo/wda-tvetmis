<?php

namespace App\Http\Controllers\AdminAuth;

use App\Rp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ext = "admin.layout.main";
        $edit = "admin.rp.edit";
        $destroy = "admin.rp.destroy";
        $store = "admin.rp.store";
        if (auth()->guard('rp')->check() === true) {
            $ext = "rp.layout.main";
            $edit = "rp.rps.edit";
            $destroy = "rp.rps.destroy";
            $store = "rp.rps.store";
        }

        $users = Rp::all();
        return view('admin.users.rp.index', compact('users', 'ext', 'edit', 'destroy', 'store'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ext = "admin.layout.main";
        $edit = "admin.rp.edit";
        $destroy = "admin.rp.destroy";
        $store = "admin.rp.store";
        $update = "admin.rp.update";
        if (auth()->guard('rp')->check() === true) {
            $ext = "rp.layout.main";
            $edit = "rp.rps.edit";
            $destroy = "rp.rps.destroy";
            $store = "rp.rps.store";
            $update = "rp.rps.update";
        }

        $users = Rp::all();
        $umuntu = Rp::findOrFail($id);
        return view('admin.users.rp.index', compact('users', 'ext', 'edit', 'destroy', 'store', 'umuntu', 'update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:rps,email,' . $id,
            'password' => 'nullable|min:6|confirmed',
            'permission' => 'required'
        ]);

        if ($validator->fails())
            return back()
                ->withErrors($validator->errors())
                ->withInput();

        $user = Rp::findOrFail($id);
        $data = $request->input();

        $user->name = $data['name'];
        $user->email = $data['email'];

        if ($request->input('password'))
            $user->password = bcrypt($data['password']);

        $user->permission = $data['permission'];
        $user->save();

        if (auth()->guard('admin')->check())
            $route = route('admin.rp.index');
        else
            $route = route('rp.rps.index');

        return redirect($route)
            ->with(['status' => '1', 'message' => 'Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Rp::findOrFail($id);

        $user->delete();

        if (auth()->guard('admin')->check())
            $route = route('admin.rp.index');
        else
            $route = route('rp.rps.index');

        return redirect($route)
            ->with(['status' => '1', 'message' => 'Deleted Successfully']);
    }
}
