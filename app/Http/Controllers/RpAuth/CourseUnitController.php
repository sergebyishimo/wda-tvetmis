<?php

namespace App\Http\Controllers\RpAuth;

use App\Http\Requests\CreateCourseUnitRequest;
use App\Http\Requests\UpdateCourseUnitRequest;
use App\Model\Rp\RpCourseUnit;
use App\Model\Rp\RpCourseUnitClass;
use App\Model\Rp\RpLevelOfStudy;
use App\Model\Rp\RpProgram;
use App\Model\Rp\RpSemester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $courseunits = RpCourseUnit::all();
//        $programs = RpProgram::all();
//        $level_of_study = RpLevelOfStudy::all();
//        $semester = RpSemester::all();
//        $unitclass = RpCourseUnitClass::all();
//        return view('rp.courseunits.index', compact('unitclass', 'semester', 'level_of_study', 'programs', 'courseunits'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $courseunit = [];
        $program = RpProgram::where('id', $request->p_id)->first();
        $level_of_study = RpLevelOfStudy::all();
        $semester = RpSemester::all();
        $unitclass = RpCourseUnitClass::all();
        return view('rp.courseunits.create', compact('unitclass', 'semester', 'level_of_study', 'program', 'courseunit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCourseUnitRequest $request)
    {
        $courseunit = new RpCourseUnit();
        $courseunit->program_id = $request->input('program_id');
        $courseunit->course_unit_code = $request->input('course_unit_code');
        $courseunit->course_unit_name = $request->input('course_unit_name');
        $courseunit->credit_unit = $request->input('credit_unit');
        $courseunit->level_of_study_id = $request->input('level_of_study_id');
        $courseunit->semester_id = $request->input('semester_id');
        $courseunit->unit_class_id = $request->input('unit_class_id');
        $save = $courseunit->save();
        if ($save)
            return redirect(route('rp.programs.index', ['o_o' => $request->program_id]))->with(['status' => '1', 'message' => "Course Units  added Successfully."]);
        else
            return redirect(route('rp.programs.index', ['o_o' => $request->program_id]))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds Course Units ."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courseunit = RpCourseUnit::where('id', $id)->first();
        $programs = RpProgram::all();
        $level_of_study = RpLevelOfStudy::all();
        $semester = RpSemester::all();
        $unitclass = RpCourseUnitClass::all();
        if ($courseunit)
            return view("rp.courseunits.create", compact('unitclass', 'semester', "courseunit", "programs", "level_of_study"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCourseUnitRequest $request, $id)
    {
        $courseunit = RpCourseUnit::where('id', $id)->first();
        $courseunit->program_id = $request->input('program_id');
        $courseunit->course_unit_code = $request->input('course_unit_code');
        $courseunit->course_unit_name = $request->input('course_unit_name');
        $courseunit->credit_unit = $request->input('credit_unit');
        $courseunit->level_of_study_id = $request->input('level_of_study_id');
        $courseunit->semester_id = $request->input('semester_id');
        $courseunit->unit_class_id = $request->input('unit_class_id');

        $save = $courseunit->save();

        if ($save)
            return redirect(route('rp.programs.index', ['o_o' => $request->input('program_id')]))->with(['status' => '1', 'message' => "Course Units Updated Successfully."]);
        else
            return back()
                ->withInput()
                ->with(['status' => '0', 'message' => "Failed to update  Course Units."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courseunit = RpCourseUnit::where('id', $id)->first();
        if (!$courseunit)
            return abort(419);
        $courseunit->delete();

        return redirect(route('rp.courseunits.index'));
    }
}
