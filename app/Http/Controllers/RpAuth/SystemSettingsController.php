<?php

namespace App\Http\Controllers\RpAuth;

use App\Http\Requests\SettingsActivationRequest;
use App\Rp\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class SystemSettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setTime()
    {
        $type = null;
        return view('rp.settings.set-time', compact('type'));
    }

    public function postSetTime(SettingsActivationRequest $request)
    {
        $settings = Settings::where('type', 'registration')
            ->orWhereNull('type')->first();
        if ($settings) {
            $settings->title = "Registration";
            $settings->academic_year = $request->academic_year;
            $settings->start = $request->start;
            $settings->ends = $request->ends;
            $settings->enabled = true;
            $settings->type = 'registration';
            $settings->save();
        } else {
            $settings = new Settings();
            $settings->title = "Regsitration";
            $settings->academic_year = $request->academic_year;
            $settings->start = $request->start;
            $settings->ends = $request->ends;
            $settings->enabled = true;
            $settings->type = 'registration';
            $settings->save();
        }

        return back()->with(['status' => 1, 'message' => 'Settings updated successfully !!']);
    }

    public function enrol()
    {
        $type = "enrol";
        return view('rp.settings.set-time', compact('type'));
    }

    public function postEnrol(SettingsActivationRequest $request)
    {
        $settings = Settings::where('type', 'enrol')->first();
        if ($settings) {
            $settings->title = "Enrol";
            $settings->academic_year = $request->academic_year;
            $settings->start = $request->start;
            $settings->ends = $request->ends;
            $settings->enabled = true;
            $settings->type = 'enrol';
            $settings->save();
        } else {
            $settings = new Settings();
            $settings->title = "Enrol";
            $settings->academic_year = $request->academic_year;
            $settings->start = $request->start;
            $settings->ends = $request->ends;
            $settings->enabled = true;
            $settings->type = 'enrol';
            $settings->save();
        }

        return back()->with(['status' => 1, 'message' => 'Settings updated successfully !!']);
    }

    public function semester(Request $request)
    {
        $type = null;
        return view('rp.settings.semester_academic', compact('type'));
    }

    public function postSemester(Request $request)
    {
        $key = config('mis');

        if ($request->input('semester'))
            $key['mis.semester'] = $request->input('semester');

        if ($request->input('academic_year'))
            $key['mis.academic_year'] = $request->input('academic_year');

        try {
            config($key);
            $fp = fopen(base_path() . '/config/mis.php', 'w');
            fwrite($fp, '<?php return ' . var_export(config('mis'), true) . ';');
            fclose($fp);

            return redirect()
                ->route('rp.settings.set.semester')
                ->withInput()
                ->with(['status' => 1, 'message' => 'Settings updated successfully !!']);
            } catch (\Exception $e) {
            return back()->with(['status' => 0, 'message' => $e->getMessage()]);
        }
    }
}
