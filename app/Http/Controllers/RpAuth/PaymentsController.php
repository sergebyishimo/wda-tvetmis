<?php

namespace App\Http\Controllers\RpAuth;

use App\FeeCategory;
use App\Model\Rp\CollegePayment;
use App\PaymentOutside;
use App\Rp\Payment;
use App\Rp\PaymentInvoice;
use App\Rp\PendingPaymentInvoice;
use App\Rp\Polytechnic;
use App\Rp\Student;
use App\Transformers\InvoicesTransformer;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function invoices()
    {
        $invoices = PendingPaymentInvoice::where('active', true)
            ->where('academic_year', getCurrentAcademicYear())
            ->orderBy('updated_at', "DESC")
            ->get();
        if ($invoices->count() > 1)
            $invoices = [];

        return view('rp.payments.invoices', compact('invoices'));
    }

    /**
     * Generate Data to propagate into InvoiceTransformer
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInvoices(Datatables $datatables)
    {
        $invoices = PendingPaymentInvoice::where('active', true)
            ->where('academic_year', getCurrentAcademicYear())
            ->orderBy('updated_at', "DESC");

        $dd = $datatables->eloquent($invoices)
            ->setTransformer(new InvoicesTransformer())
            ->smart(false)
            ->make(true);

        return $dd;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getMasterInvoices()
    {
        $invoices = PendingPaymentInvoice::where('active', true);
        $deleting = rpAllowed(5);
        $reading = rpAllowed(2);
        return DataTables::of($invoices)
            ->addColumn('details', function ($invoice) use ($reading) {
                if ($reading) {
                    $url = route('rp.payments.get.invoice.detail', $invoice->code);
                    return '<a href="' . $url . '"class="btn btn-xs btn-warning">
                        <i class="glyphicon glyphicon-list"></i> View Details</a>';
                }else
                    return null;
            })
            ->editColumn('partial', function ($invoice) {
                return $invoice->partial == 1 ? 'Yes' : 'No';
            })
            ->addColumn('action', function ($invoice) use ($deleting) {
                if ($invoice->paid <= 0 && $deleting) {
                    $url = route('rp.payments.get.invoice.destroy', $invoice->code);
                    $confirm = "return confirm('Do you really want to submit the form?');";
                    $form = "<form action='" . $url . "' method='post' onsubmit='" . $confirm . "'>
                            " . method_field('delete') . "
                            " . csrf_field() . "
                            <input type='hidden' name='student_id' value='" . $invoice->student_id . "'>
                            <button type='submit' class='btn btn-xs btn-danger'>
                                Delete
                            </button>
                        </form>";
                } else
                    $form = "";
                return $form;
            })
            ->setRowClass(function ($invoice) {
                return $invoice->partial == 1 ? 'bg-white' : 'bg-gray';
            })->rawColumns(['details', 'action'])
            ->make(true);
    }

    public function getDestroyInvoice(Request $request, $code)
    {
        $student = $request->input('student_id');
        $invoice = PendingPaymentInvoice::where('code', $code)
            ->where('student_id', $student)
            ->where('paid', '<', '1')
            ->where('active', true)->first();
        if ($invoice) {
            $invoiceItems = PaymentInvoice::where('code', $invoice->code)
                ->where('paid', 0)
                ->where('student_id', $invoice->student_id);
            if ($invoiceItems->count() > 0) {
                foreach ($invoiceItems->get() as $invoiceItem) {
                    $invoiceItem->delete();
                }
            }
            $invoice->delete();

            return back()->with(['status' => '1', 'message' => 'All Associated recorded has been deleted ...']);
        }
        return back()->with(['status' => '0', 'message' => 'Oops. there are a problem ...']);
    }

    /**
     * @param $code
     * @return mixed
     * @throws \Exception
     */

    public function getDetailsInvoice($code)
    {
        $fees = PaymentInvoice::join('functional_fees', "functional_fees.id", '=', 'payment_invoices.function_fee')
            ->where("payment_invoices.code", $code)
            ->select(["functional_fees.fee_name", "payment_invoices.amount", "payment_invoices.code"])
            ->orderBy('payment_invoices.amount', "DESC")->get();

        $payments = CollegePayment::where('invoice_code', $code)->orderBy('op_date', "DESC")->get();

        $invoice = PendingPaymentInvoice::where('code', $code)->first();

        return view("rp.payments.invoice_blade", compact('fees', 'code', 'payments', 'invoice'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function filtering()
    {
        $college = [];
        $categories = FeeCategory::all();
        if ($categories)
            $categories = $categories->pluck('category_name', 'category_name');

        if (auth()->guard('rp')->check()) {
            $colleges = Polytechnic::all();
            if ($colleges)
                $college = $colleges->pluck('short_name', 'id');
        } elseif (auth()->guard('college')->check())
            $college = auth()->guard('college')->user()->college_id;

        return view('rp.payments.filtering', compact('college', 'categories'));
    }

    public function makeFiltering(Request $request)
    {
        $this->validate($request, [
            'start' => 'nullable',
            'end' => 'nullable|after:start'
        ]);
        if (!auth()->guard('college')->check())
            $college = $request->input('college');
        else
            $college = college('college_id');

        $category = $request->input('category');
        $student = $request->input('student');
        $start = $request->input('start');
        $end = $request->input('end');
        $paymentType = $request->input('payment_type');
        $bank = $request->input('bank');
        $academicYear = $request->input('academic_year');

        $manually = $request->input('manually_payment');

        if ($paymentType == null && $bank == null) {
            $query = DB::table('rp_college_payments')
                ->join('rp_pending_payment_invoices', 'rp_college_payments.invoice_code', "=", "rp_pending_payment_invoices.code")
                ->join('adm_polytechnics', 'rp_college_payments.college_id', "=", "adm_polytechnics.id");

            $view = 'rp.payments.filter.index';

            if ($college)
                $query->where('rp_college_payments.college_id', $college);

            if ($student) {
                $query->where('rp_college_payments.std_id', $student);
                $view = 'rp.payments.filter.student';
            }

            if ($start && $end)
                $query->whereBetween('rp_college_payments.created_at', [$start, $end]);
            elseif ($start && !$end) {
                $date = strtotime($start);
                $query->whereDay('rp_college_payments.created_at', '=', date('d', $date));
                $query->whereMonth('rp_college_payments.created_at', '=', date('m', $date));
                $query->whereYear('rp_college_payments.created_at', '=', date('Y', $date));
            }

            if ($category)
                $query->where('rp_pending_payment_invoices.name', 'LIKE', '%' . $category . '%');

            if ($manually == 'y')
                $query->where('rp_college_payments.description', 'LIKE', '%Manually%');

            if ($academicYear)
                $query->where('rp_college_payments.academic_year', $academicYear);

            $payments = $query->get();
            $gPayments = $query->groupBy('rp_college_payments.invoice_code')->get();

            return view($view, compact('payments', 'gPayments'));
        } else {
            $query = PaymentOutside::orderBy('created_at', 'DESC');

            if (auth()->guard('college')->check())
                $query->where('college_id', college('college_id'));

            if ($college)
                $query->where('college_id', $college);

            if ($academicYear)
                $query->where('academic_year', $academicYear);

            if ($student)
                $query->where('student_id', $student);

            if ($start && $end)
                $query->whereBetween('date_paid', [$start, $end]);

            elseif ($start && !$end) {
                $date = strtotime($start);
                $query->whereDay('date_paid', '=', date('d', $date));
                $query->whereMonth('date_paid', '=', date('m', $date));
                $query->whereYear('date_paid', '=', date('Y', $date));
            }

            if ($paymentType && $paymentType != '000')
                $query->where('payment_type', $paymentType);

            if ($bank)
                $query->where('bank', $bank);

            $payments = $query->get();

            return view('rp.payments.filter.outside', compact('payments', 'category'));
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history()
    {
        return view('rp.payments.historic');
    }

    /**
     * Generate Data For Payments historic
     * @return mixed
     * @throws \Exception
     */
    public function getHistoricData()
    {
        $payments = DB::table('rp_college_payments')
            ->join('adm_polytechnics', 'rp_college_payments.college_id', '=', 'adm_polytechnics.id')
            ->join('rp_pending_payment_invoices', 'rp_college_payments.invoice_code', '=', 'rp_pending_payment_invoices.code')
            ->where('rp_college_payments.academic_year', 'LIKE', getCurrentAcademicYear())
            ->select(['rp_college_payments.*', 'adm_polytechnics.short_name', 'rp_pending_payment_invoices.name']);
//            ->orderByDesc("rp_college_payments.created_at");
        if (auth()->guard('college')->check())
            $payments->where('rp_college_payments.college_id', college('college_id'));

        return DataTables::of($payments)->make(true);

    }

    public function manually()
    {
        $payments = Payment::where('description', 'LIKE', '%Manually%')
            ->where('academic_year', getCurrentAcademicYear())
            ->get();
        return view('rp.payments.manually', compact('payments'));
    }

    public function revertManually(Request $request, $id)
    {
        $id = $request->input('delete');
        $student = $request->input('student_id');
        $invoice_code = $request->input('invoice_code');

        $payment = Payment::where('description', 'LIKE', '%Manually%')
            ->where('id', $id)
            ->where('std_id', $student);
        if ($invoice_code == null)
            $payment->whereNull('invoice_code');
        else
            $payment->where('invoice_code', $invoice_code);

        $payment = $payment->first();

        $student = Student::where('id', $request->input('student_id'))->first();

        if (!$student)
            return back()
                ->with(['status' => '0', 'message' => 'This student does not have user account ???']);

        if ($payment) {
            $invoice = $invoice_code ? $payment->invoice : true;
            if ($invoice) {
                if ($invoice_code) {
                    $invoice->paid -= $payment->paid_amount;
                    $invoice->active = 1;
                    $invoiceSave = $invoice->save();
                } else
                    $invoiceSave = true;
                if ($invoiceSave) {
                    $student->payments = 0;
                    if ($student->save()) {
                        $payment->delete();
                        return back()->with(['status' => '1', 'message' => 'All reverted successfully ...']);
                    }
                }
            }
        }

        return back()->with(['status' => '0', 'message' => 'Oops, something went wrong !!']);
    }

    public function manuallyStore(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'student_id' => 'required',
            'payment_category' => 'required',
            'academic_year' => 'required',
            'amount' => 'required',
            'date_paid' => 'required',
            'invoice_code' => 'required',
            'bank_slip_number' => 'required',
            'operator' => 'required',
            'attachment' => 'required|mimes:pdf'
        ]);

        if ($validation->fails())
            return back()->withInput()->with(['status' => '0', 'message' => $validation->errors()->all()]);

        $category = FeeCategory::where('id', $request->input('payment_category'))->first();

        $invoice = PendingPaymentInvoice::where('student_id', $request->input('student_id'))
            ->where('code', $request->input('invoice_code'))->first();
        $student = Student::where('id', $request->input('student_id'))->first();

        if (!$student)
            return back()
                ->withInput()
                ->with(['status' => '0', 'message' => 'This student does not have user account ???']);

        if ($category) {
            if ($invoice) {
                $icategory = strtolower($invoice->name);
                $ncategory = strtolower($category->category_name);
                if ($icategory == $ncategory) {
                    if ($request->input('amount') == $invoice->amount) {
                        if ($invoice->active == 1 || $invoice->active == '1') {
                            $slipN = Payment::where('bank_slipno', $request->input('bank_slip_number'));
                            if ($slipN->count() <= 0) {
                                $payment = new Payment();
                                $payment->college_id = getStudentInfo($request->input('student_id'), 'college_id', true);
                                $payment->std_id = $request->input('student_id');
                                $payment->year = $request->input('date_paid');
                                $payment->paid_amount = $request->input('amount');
                                $payment->bank_slipno = $request->input('bank_slip_number');
                                $payment->op_date = $request->input('date_paid');
                                $payment->bank_account = '00002-01390199483-52';
                                $payment->bank = $request->input('operator');
                                $payment->description = $request->input('invoice_code') . "-" . $request->input('student_id') . " + Manually Uploaded";
                                $payment->operator = "cogepay";
                                $payment->pay_mode = "COGEPAY";
                                $payment->pay_status = true;
                                $payment->trans_id = $request->reference ?: "Undefined";
                                $payment->event_id = $request->input('eventid') ?: 'Undefined';
                                $payment->invoice_code = $request->input('invoice_code');
                                $payment->academic_year = getCurrentAcademicYear();

                                if ($request->hasFile('attachment')) {
                                    $path = 'students/payments/' . date('F,Y') . "/manually";
                                    $file = $request->file('attachment');
                                    $ext = $file->getClientOriginalExtension();
                                    $attachment = $file->storeAs($path, $request->input('student_id') . '.' . $ext, config('voyager.storage.disk'));
                                    $payment->attachment = $attachment;
                                }
                                if ($payment->save()) {
                                    $expected = $invoice->amount;
                                    $paid = $invoice->paid + $request->input('amount');
                                    $active = 1;
                                    if ($paid >= $expected)
                                        $active = 0;
                                    $invoice->active = $active;
                                    $invoice->paid = $request->input('amount');
                                    $invoiceName = $invoice->name;
                                    if ($invoice->save()) {
                                        $nnn = str_slug(strtolower($invoiceName), "_");
                                        if ($nnn == "registration" OR $nnn == "registration_fees" OR $nnn = "application" OR $nnn = "application_fees") {
                                            $student->payments = 1;
                                            if (!$student->save())
                                                return back()
                                                    ->withInput()
                                                    ->with(['status' => '0', 'message' => 'Update Student Failed !!!']);
                                        }
                                        return back()
                                            ->with(['status' => '1', 'message' => 'Successfully uploaded.']);
                                    } else
                                        return back()
                                            ->withInput()
                                            ->with(['status' => '0', 'message' => 'Update Invoice Failed !!!']);
                                } else
                                    return back()
                                        ->withInput()
                                        ->with(['status' => '0', 'message' => 'Update Payment Failed !!!']);
                            } else
                                return back()
                                    ->withInput()
                                    ->with(['status' => '0', 'message' => 'Bank slip number already exists !!!']);
                        } else
                            return back()
                                ->withInput()
                                ->with(['status' => '0', 'message' => 'Invoice seem to be inactive ???']);

                    } else
                        return back()
                            ->withInput()
                            ->with(['status' => '0', 'message' => 'Amount is not equal to invoice generated amount ...']);
                } else
                    return back()
                        ->withInput()
                        ->with(['status' => '0', 'message' => 'Invoice is not corresponding to category ...']);
            } else
                return back()
                    ->withInput()
                    ->with(['status' => '0', 'message' => 'Invoice provided is not valid ...']);
        } else
            return back()
                ->withInput()
                ->with(['status' => '0', 'message' => 'Payment category provided is not valid ...']);
    }

}
