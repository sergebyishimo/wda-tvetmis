<?php

namespace App\Http\Controllers\RpAuth;

use App\Rp\Polytechnic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PolytechnicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $polytechnics = Polytechnic::all();
        $editting = null;

        return view('rp.polytechnics.index', compact('polytechnics', 'editting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('id'))
            $this->validate($request, [
                'polytechnic' => 'required|min:4|unique:admission_mysql.polytechnics,polytechnic,'.$request->id,
                'short_name' => 'required|min:2|unique:admission_mysql.polytechnics,short_name,'.$request->id,
            ]);
        else
            $this->validate($request, [
                'polytechnic' => 'required|min:4|unique:admission_mysql.polytechnics,polytechnic',
                'short_name' => 'required|min:2|unique:admission_mysql.polytechnics,short_name',
            ]);

        $poly = new Polytechnic();
        if ($request->has('id'))
            $poly = Polytechnic::find($request->id);

        $poly->polytechnic = $request->polytechnic;
        $poly->short_name = $request->short_name;
        $poly->save();

        return redirect()->route('rp.polytechnics.index')
            ->with(['status' => '1', 'message' => 'Save Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $polytechnics = Polytechnic::all();

        $editting = Polytechnic::findOrFail($id)->id;

        return view('rp.polytechnics.index', compact('polytechnics', 'editting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
