<?php

namespace App\Http\Controllers\RpAuth;

use App\FeeCategory;
use App\Model\Rp\AdmissionPrivate;
use App\Model\Rp\StudentRegistered;
use App\Rp\AdmittedStudent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function getStudentsShort()
    {
        $aStudents = AdmittedStudent::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $aStudents->where('college_id', college('college_id'));
        $aStudents = $aStudents->get();
        //
        $pStudents = AdmissionPrivate::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $pStudents->where('college_id', college('college_id'));
        $pStudents = $pStudents->get();
        $students = [];
        foreach ($aStudents as $aStudent) {
            if (!applied($aStudent->std_id)) #&& !isSponsored($aStudent->std_id)
                $students[$aStudent->std_id] = $aStudent->std_id . " - " . $aStudent->names;
        }
        foreach ($pStudents as $pStudent) {
            if (!isset($students[$pStudent->std_id]) && !isSponsored($pStudent->std_id))
                $students[$pStudent->std_id] = $pStudent->std_id . " - " . $pStudent->names;
        }

        return response()->json($students);
    }

    public function getStudentsShortExp()
    {
        $aStudents = AdmittedStudent::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $aStudents->where('college_id', college('college_id'));
        $aStudents = $aStudents->get();
        //
        $pStudents = AdmissionPrivate::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $pStudents->where('college_id', college('college_id'));
        $pStudents = $pStudents->get();
        $students = [];

        foreach ($aStudents as $aStudent) {
            if ($aStudent->registered()->where('academic_year', getCurrentAcademicYear())->count() <= 0)
                $students[$aStudent->std_id] = $aStudent->std_id . " - " . $aStudent->names;
        }
//        foreach ($pStudents as $pStudent) {
//            if (!isset($students[$pStudent->std_id]) && !isSponsored($pStudent->std_id))
//                $students[$pStudent->std_id] = $pStudent->std_id . " - " . $pStudent->names;
//        }

        return response()->json($students);
    }

    public function getPaymentCategories()
    {
        $categories = FeeCategory::all();
        if ($categories)
            $categories = $categories->pluck('category_name', 'id');
        else
            $categories = [];

        return response()->json($categories);
    }

}
