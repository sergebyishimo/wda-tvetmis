<?php

namespace App\Http\Controllers\RpAuth\Admission;

use App\Mail\SendAdmittedEmail;
use App\Mail\SendRejectedStudentEmail;
use App\Model\Rp\AdmissionPrivate;
use App\Rp\AdmittedStudent;
use App\Rp\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class PrivateStudentsController extends Controller
{
    /**
     * Display a listing of the resource | Signed Up Students.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = AdmissionPrivate::whereNull('rejected')
            ->whereNull("national_id_number")
            ->whereNull("gender")
            ->whereNull("scan_of_diploma_or_certificate")
            ->whereYear('created_at', '=', date('Y'))
            ->where('sponsorship_status', 'private');

        if (auth()->guard('college')->check())
            $students->where('college_id', college('college_id'));

        $students = $students->get();
        return view('rp.admission.private.index', compact('students'));
    }

    public function finished()
    {
        $students = AdmissionPrivate::whereNotNull("national_id_number")
            ->whereNotNull("gender")
            ->whereNotNull("scan_of_diploma_or_certificate")
            ->whereNull('rejected')
            ->whereYear('created_at', '=', date('Y'))
            ->where('sponsorship_status', 'private');

        if (auth()->guard('college')->check())
            $students->where('college_id', college('college_id'));

        $students = $students->get();
        $stds = [];
        if ($students->count() > 0) {
            foreach ($students as $student) {
                if (!isAdmitted($student)) {
                    $stds[] = $student;
                }
            }
            $students = $stds;
        }

        return view('rp.admission.private.finished', compact('students'));
    }

    public function rejected() {
        $students = AdmissionPrivate::whereNotNull("national_id_number")
            ->whereNotNull("gender")
            ->whereNotNull("scan_of_diploma_or_certificate")
            ->whereYear('created_at', '=', date('Y'))
            ->where('sponsorship_status', 'private')
            ->where('rejected', 1);

        if (auth()->guard('college')->check())
            $students->where('college_id', college('college_id'));

        $students = $students->get();
        $stds = [];
        if ($students->count() > 0) {
            foreach ($students as $student) {
                if (!isAdmitted($student)) {
                    $stds[] = $student;
                }
            }
            $students = $stds;
        }

        return view('rp.admission.private.rejected', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->middleware('college');
        $id = $request->input('std_id');
        $lstudent = AdmissionPrivate::where('std_id', $id)->first();
        if ($lstudent) {
            $student = $lstudent;
            $student = $student->toArray();
            if (isset($student['id']))
                unset($student['id']);
            if (isset($student['created_at']))
                unset($student['created_at']);
            if (isset($student['updated_at']))
                unset($student['updated_at']);
            if (isset($student['dob']))
                $student['dob'] = date('Y', strtotime($student['dob']));
            $student['academic_year'] = getCurrentAcademicYear();
            $admitted = false;
            if (!isAdmitted($lstudent->id))
                if ($request->department) {
                    $student['department_id'] = $request->department;
                }
                if ($request->program) {
                    $student['course_id'] = $request->program;
                }
                $admitted = AdmittedStudent::create($student);
            if ($admitted) {
                $lstudent->rejected = '0';
                $lstudent->save();
                $studentLogin = Student::where('id', $id)->first();
                if ($studentLogin)
                    $studentLogin->update(['payments' => '0']);
                $data = [
                    'reg_no' => $lstudent->std_id,
                    'college' => getCollege($lstudent->college_id, 'polytechnic'),
                    'department' => getDepartment($student['department_id'], 'department_name'),
                    'course' => getCourse($student['course_id'], 'program_name'),
                    'names' => trim(ucwords($lstudent->first_name . " " . $lstudent->other_names)),
                    'phone' => $lstudent->phone,
                    'email' => $lstudent->email
                ];
                Mail::to($data['email'])->send(new SendAdmittedEmail($data, true));
            }
            return redirect()->route('college.admission.privates.finished')
                ->with(['status' => '1', 'message' => 'Student Now Accepted successfully']);
        }
        return redirect()->route('college.admission.privates.finished')
            ->with(['status' => '0', 'message' => 'Student ID Not valid']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->middleware('college');
        $id = $request->input('std_id');
        $lStudent = Student::where('id', $id)->first();
        $student = AdmissionPrivate::where('std_id', $id)->first();
        $data = null;
        if ($student) {
            $data = [
                'reg_no' => $student->std_id,
                'college' => getCollege($student->college_id, 'polytechnic'),
                'course' => getCourse($student->course_id, 'program_name'),
                'names' => trim(ucwords($student->first_name . " " . $student->other_names)),
                'email' => $student->email,
                'reason' => $request->reason
            ];
            $student->rejected = '1';
            $student->save();
        }
        if ($lStudent) {
            $lStudent->delete();
        }
        if ($data)
            Mail::to($data['email'])->send(new SendRejectedStudentEmail($data));

        return redirect()->route('college.admission.privates.finished')
            ->with(['status' => '1', 'message' => 'Student Now Accepted successfully']);
    }
}
