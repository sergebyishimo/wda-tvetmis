<?php

namespace App\Http\Controllers\RpAuth;

use App\Jobs\SendAdmittedEmailJob;
use App\Mail\SendAdmittedEmail;
use App\Rp\AdmittedStudent;
use App\Rp\EmailSended;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class EmailsController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function portal()
    {
        $admittedStudents = AdmittedStudent::where('year_of_study', date('Y'))->get();
        return view('rp.mailing.portal', compact('admittedStudents'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send(Request $request)
    {
        $active = $request->input('on');

        switch ($active) {
            case 'a':
                $sendedSave = [];
                $students = AdmittedStudent::where('year_of_study', date('Y'))->get();
                $mmm = EmailSended::all()->pluck('email');
                if ($mmm->count() > 0)
                    $mmm = $mmm->toArray();
                else
                    $mmm = [];

                if ($students->count() > 0) {
                    $x = 1;
                    foreach ($students as $student) {
                        if (!in_array($student->email, $mmm)) {
                            $send = [
                                'reg_no' => $student->std_id,
                                'college' => getCollege($student->college_id, 'polytechnic'),
                                'department' => getDepartment($student->department_id, 'department_name'),
                                'course' => getCourse($student->course_id, 'program_name'),
                                'names' => trim(ucwords($student->first_name . " " . $student->other_names)),
                                'phone' => $student->phone,
                                'email' => $student->email
                            ];

//                            $emailJob = new SendAdmittedEmailJob($send);
//                            dispatch($emailJob);

                            $email = Mail::to($send['email'])->send(new SendAdmittedEmail($send));
                            if (is_null($email)) {
                                EmailSended::create([
                                    'from' => 'admitted',
                                    'email' => $send['email']
                                ]);
                                if ($x === 500)
                                    break;
                                else
                                    $x++;
                            }
                        }
                    }

                    $count = $x;
                    return redirect(route('rp.emails'))
                        ->with(['status' => '1', 'message' => 'All ' . $count . ' send successfully']);
                } else
                    return back()->with(['status' => '3', 'message' => 'Uhm, there is no admitted students !!']);
                break;
            case 's':
                break;
        }
    }
}
