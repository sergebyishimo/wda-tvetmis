<?php

namespace App\Http\Controllers\RpAuth;

use App\Http\Requests\UpdatePManageRequest;
use App\Model\Rp\CollegeProgram;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use App\Rp\Polytechnic;
use App\Rtqf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $editting = null;
        $program = null;
        $programs = RpProgram::all();
        $polytechnics = Polytechnic::all();
        $manageprogram = null;
        return view('rp.manageprograms.index', compact('manageprogram', "program", 'programs', 'polytechnics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdatePManageRequest $request)
    {
        if (!rpAllowed(4))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $check = CollegeProgram::where('college_id', $request->college_id)->where('program_id', $request->program_id)->first();
        if ($check) {
            return redirect(route('rp.manageprograms.index'))->with(['status' => '0', 'message' => "This Program Already Assigned To That College!."]);
        }
        $program = RpProgram::where('id', $request->program_id)->first();
        if ($program) {
            $collegeprogram = new CollegeProgram();
            $collegeprogram->college_id = $request->college_id;
            $collegeprogram->department_id = $program->department->id;
            $collegeprogram->program_id = $program->id;
            $save = $collegeprogram->save();
            if ($save)
                return redirect(route('rp.manageprograms.index'))->with(['status' => '1', 'message' => "Assigned Program To College  Successfully."]);
            else
                return redirect(route('rp.manageprograms.index'))->withInput()
                    ->with(['status' => '0', 'message' => "Failed to Assign Program to College."]);

        } else {
            abort(404);
        }
    }

    /*
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $program_id)
    {
        if (!rpAllowed(3))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $programs = RpProgram::all();
        $polytechnics = Polytechnic::all();
        $manageprogram = Polytechnic::where('id', $id)->first();
        $program = RpProgram::where('id', $program_id)->first();
        if ($manageprogram && $program) {
            return view('rp.manageprograms.index', compact('manageprogram', 'program', 'programs', 'polytechnics'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!rpAllowed(3))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $manageprogram = CollegeProgram::where('college_id', $id)->first();
        $program = RpProgram::where('id', $request->program_id)->first();
        if ($manageprogram) {
            $manageprogram->college_id = $request->college_id;
            $manageprogram->department_id = $program->department->id;
            $manageprogram->program_id = $program->id;
            $save = $manageprogram->save();
            if ($save)
                return redirect(route('rp.manageprograms.index'))->with(['status' => '1', 'message' => "Assigned Program To College  Successfully."]);
            else
                return redirect(route('rp.manageprograms.index'))->withInput()
                    ->with(['status' => '0', 'message' => "Failed to Assign Program to College."]);

        } else {
            return redirect(route('rp.manageprograms.index'))->with(['status' => '0', 'message' => "Update Failed"]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $program_id)
    {
        if (!rpAllowed(5))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $manageprogram = CollegeProgram::where('college_id', $id)->where('program_id', $program_id)->first();
        if ($manageprogram) {
            $manageprogram->delete();
            return redirect(route('rp.manageprograms.index'))->with(['status' => '1', 'message' => 'Program removed to specified college successful']);
        } else
            abort(409);
    }
}
