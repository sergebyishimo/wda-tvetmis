<?php

namespace App\Http\Controllers\RpAuth;

use App\Model\Rp\StudentInstructions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstructionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rp.instructions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'instructions' => 'required'
        ]);
        $type = $request->input('type');
        $instructions = $request->input('instructions');

        $model = StudentInstructions::where('type', $type)->first();
        if (!$model)
            $model = new StudentInstructions();

        $model->type = $type;
        $model->instructions = $instructions;
        $model->save();

        return back()->with(['status' => '1', 'message' => "Instruction Updated Successfully."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($type)
    {
        return view('student.instractions.show', compact('type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
