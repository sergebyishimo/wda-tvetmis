<?php

namespace App\Http\Controllers\RpAuth;

use App\Http\Requests\CreateRpDepartmentRequest;
use App\Http\Requests\UpdateRpDepartmentRequest;
use App\Model\Rp\RpDepartment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RpDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = null;
        $departments = RpDepartment::all();
        return view('rp.departments.index', compact('department', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = null;
        return view('school.departments.index', compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRpDepartmentRequest $request)
    {
        if (!rpAllowed(4))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $department = null;
        $departments = RpDepartment::all();
        $dept = new RpDepartment();
        $dept->department_name = $request->department_name;
        $dept->department_code = $request->department_code;
        $save = $dept->save();
        if ($save)
            return redirect(route('rp.departments.index'))->with(['status' => '1', 'message' => "Department Added Successfully."]);
        else
            return redirect(route('rp.departments.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Department."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!rpAllowed(3))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $departments = RpDepartment::all();
        $department = RpDepartment::where('id', $id)->first();
        if ($department) {
            return view('rp.departments.index', compact('department', 'departments'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRpDepartmentRequest $request, $id)
    {
        if (!rpAllowed(3))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $department = RpDepartment::where('id', $id)->first();
        if (!$department)
            return abort(419);
        $department->department_name = $request->department_name;
        $department->department_code = $request->department_code;
        $save = $department->save();

        if ($save)
            return redirect(route('rp.departments.index'))->with(['status' => '1', 'message' => "Department Updated Successfully."]);
        else
            return redirect(route('rp.departments.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a Department."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!rpAllowed(5))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $department = RpDepartment::where('id', $id)->first();
        if ($department) {
            $department->delete();
            return redirect(route('rp.departments.index'))->with(['status' => '1', 'message' => 'Department Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
