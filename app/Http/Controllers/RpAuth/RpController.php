<?php

namespace App\Http\Controllers\RpAuth;

use App\Http\Requests\UpdateRpProfileRequest;
use App\Rp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RpController extends Controller
{
    function __construct()
    {
        $this->middleware('rp');
    }

    public function updateProfile(UpdateRpProfileRequest $request)
    {
        $id = $request->id;
        $rp = Rp::findOrFail($id);
        $rp->email = $request->email;

        if ($request->has('password'))
            $rp->password = bcrypt($request->password);

        $rp->save();

        return back()->with(['status' => '1', 'message' => 'Updated Successfully !']);
    }
}
