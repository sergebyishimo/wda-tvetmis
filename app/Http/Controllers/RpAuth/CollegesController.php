<?php

namespace App\Http\Controllers\RpAuth;


use App\College;
use App\Http\Requests\CreateCollegeAccountsRequest;
use App\Http\Requests\UploadStudent;
use App\Mail\SendAdmittedEmail;
use App\Mail\SendTranseringFeedBackMail;
use App\Model\Rp\AdmissionPrivate;
use App\Model\Rp\StudentExp;
use App\Rp\AdmissionLetter;
use App\Rp\AdmittedStudent;
use App\Rp\Applicant;
use App\Rp\CollegeDepartment;
use App\Rp\CollegeOption;
use App\Rp\CollegeStudent;
use App\Rp\Polytechnic;
use App\Rp\Student;
use App\Rp\StudentNotification;
use App\Rp\TransferRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Rap2hpoutre\FastExcel\FastExcel;
use Yajra\DataTables\DataTables;

class CollegesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rp.colleges.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rp.colleges.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function store(Request $request)
    {
        /*if ($request->hasFile('file')) {
            $fileTitle = $request->file('file')->getClientOriginalNAme();
            $filename = $request->file('file')->storeAs('uploads/students', $fileTitle . ' ' . date('Ymd H:i') . '.xlsx');
            $inputFileName = storage_path('app/') . $filename;
            $objPHPExcel = IOFactory::load($inputFileName);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= 300; $row++) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                if ($rowData[0][0] == null) {
                    continue;
                }

                $reg_no = $rowData[0][0];
                $college_id = $rowData[0][2];
                $department_id = $rowData[0][3];
                $course_id = $rowData[0][4];
                $sponsor = strtolower($rowData[0][5]);

                $student = AdmittedStudent::where('std_id', $reg_no)->first();
                if ($student) {
                    $student->college_id = $college_id;
                    $student->department_id = $department_id;
                    $student->course_id = $course_id;
                    $student->save();
                }

                $student = CollegeStudent::where('student_reg', $reg_no)->first();
                if ($student) {
                    $student->college_id = $college_id;
                    $student->course_id = $course_id;
                    $student->department_id = $department_id;
                    $student->save();
                }
            }

            return back()->with(['status' => 1, 'message' => 'All imported successfully!!']);
        }*/


        if ($request->hasFile('file')) {
            $loc = "/admittedStudents/" . date("FY");
            $ext = $request->file('file')->getClientOriginalExtension();
            $name = Hash::make(time()) . "." . $ext;
            $path = $request->file('file')->storeAs($loc, $name, config('voyager.storage.disk'));

//            $path = Storage::disk(config('voyager.storage.disk'))->get($path);
            $path = storage_path("app/public/" . $path);
            $data = (new FastExcel)->configureCsv(',')->import($path, function ($line) {
                return $line;
            });

            if ($data->count() > 501)
                return back()->with(['status' => '0', "message" => 'Please your rows must not be more than 500.']);

            if (!empty($data) && $data->count()) {
                $insert = [];
                $leftover = [];
                $lo = 0;
                foreach ($data as $item) {
                    $insert[] = $item;
                }
//                $send = [];
                $error = [];

                if (!empty($insert)) {
                    foreach (array_chunk($insert, 100) as $students) {
                        $students = (object)$students;
                        foreach ($students as $student) {
                            $student = (object)$student;
                            $applicant = Applicant::where('stdid', $student->reg_no);
                            if ($applicant->count() > 0) {
                                $applicant = $applicant->first();
                                $data = [
                                    'college_id' => (int)$student->college,
                                    'department_id' => (int)$student->department,
                                    'course_id' => (int)$student->course,
                                    'std_id' => $student->reg_no,
                                    'first_name' => $applicant->first_name,
                                    'other_names' => $applicant->other_names,
                                    'email' => $applicant->email,
                                    'phone' => $applicant->phone,
                                    'gender' => $applicant->gender,
                                    'ubudehe' => $applicant->ubudehe,
                                    'dob' => $applicant->dob,
                                    'want_student_loan' => $applicant->want_student_loan,
                                    'parents_phone' => $applicant->parents_phone,
                                    'national_id_number' => $applicant->national_id_number,
                                    'province' => $applicant->province,
                                    'district' => $applicant->district,
                                    'sector' => $applicant->sector,
                                    'cell' => $applicant->cell,
                                    'village' => $applicant->village,
                                    'bank_slip_number' => $applicant->bank_slip_number,
                                    'scanned_bank_slip' => $applicant->scanned_bank_slip,
                                    'photo' => $applicant->photo,
                                    'scan_of_diploma_or_certificate' => $applicant->scan_of_diploma_or_certificate,
                                    'your_bank' => $applicant->your_bank,
                                    'your_bank_account' => $applicant->your_bank_account,
                                    'disability' => $applicant->disability,
                                    'examiner' => $applicant->examiner,
                                    'index_number' => $applicant->index_number,
                                    'school_attended' => $applicant->school_attended,
                                    'graduation_year' => $applicant->graduation_year,
                                    'option_offered' => $applicant->option_offered,
                                    'aggregates_obtained' => $applicant->aggregates_obtained,
                                    'payment_verified' => 0,
                                    'scan_national_id_passport' => $applicant->scan_national_id_passport,
                                    'year_of_study' => $applicant->year_of_study,
                                    'country' => $applicant->country
                                ];

                                if (isset($student->sponsor))
                                    $data['sponsorship_status'] = strlen($student->sponsor) > 1 ? $student->sponsor : "government";
                                else
                                    $data['sponsorship_status'] = strlen($applicant->sponsorship_status) > 1 ? $applicant->sponsorship_status : "government";

                                $a = AdmittedStudent::where('std_id', $data['std_id'])
                                    ->orWhere('email', $data['email']);
                                if ($a->count() == 0) {
                                    $save = AdmittedStudent::create($data);
                                    if ($save) {
                                        $userA = \DB::connection('admission_mysql')
                                            ->table('students')
                                            ->where('id', $data['std_id']);

                                        if ($userA->count() > 0) {
                                            $userA = $userA->first();
                                            $rpS = Student::where('id', $userA->id)->first();
                                            $emC = Student::where('email', $data['email'])->first();
                                            if (!$rpS) {
                                                if (!$emC) {
                                                    Student::create([
                                                        'id' => $userA->id,
                                                        'name' => $userA->name,
                                                        'email' => $userA->email,
                                                        'password' => $userA->password,
                                                        'telephone' => $data['phone']
                                                    ]);
                                                } else {
                                                    $error[] = [
                                                        'message' => $data['email'] . ' already Exists on this id : ' . $emC->id
                                                    ];
                                                }
                                            }
                                        }
//                                        $send[] = [
//                                            'reg_no' => $data['std_id'],
//                                            'college' => getCollege($data['college_id'], 'polytechnic'),
//                                            'department' => getDepartment($data['department_id'], 'department'),
//                                            'course' => getCourse($data['course_id'], 'choices'),
//                                            'names' => trim(ucwords($data['first_name'] . " " . $data['other_names'])),
//                                            'phone' => $data['phone'],
//                                            'email' => $data['email']
//                                        ];
                                    }
                                } else {
                                    $error[] = [
                                        'message' => $data['std_id'] . ' already Exists'
                                    ];
                                }
                            } else {
                                $error[] = [
                                    'message' => $student->reg_no . ' not find !!'
                                ];
                            }
                        }
                    }
//                    if (count($send) > 0) {
//                        foreach ($send as $item) {
//                            Mail::to($item['email'])->send(new SendAdmittedEmail($item));
//
////                            if (strlen($item['phone']) > 5)
////                                reg_sendSMS($item['phone'], "");
//                        }
//                    }
                    if (count($error) > 0) {
                        return back()->with(['status' => 3, 'message' => $error]);
                    }
                    return back()->with(['status' => 1, 'message' => 'All imported successfully']);
                }
            } else
                return back()->with(['status' => '3', 'message' => 'File is empty !!']);

        }

        return back()->with(['status' => '3', 'message' => 'Ooops file not set !!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $college = Polytechnic::findOrFail($id);
        return view('rp.colleges.show', compact('college'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function account($id)
    {
        $college = Polytechnic::findOrFail($id);

        return view('rp.colleges.account', compact('college'));
    }

    public function upadateAccount(CreateCollegeAccountsRequest $request, $id)
    {
        $college = Polytechnic::findOrFail($id);

        $username = $request->username;
        $password = $request->password;

        $id = $college->id;

        College::create([
            'name' => $college->short_name,
            'username' => $username,
            'password' => bcrypt($password),
            'college_id' => $id
        ]);

        return redirect(route('rp.colleges'))
            ->with(['status' => 1, 'message' => 'Account Created Successfully.']);
    }

    public function student($pol, $id)
    {
        $student = AdmittedStudent::where('college_id', $pol)
            ->where('std_id', $id);
        if ($student->count() <= 0)
            return abort(404);
        $college = college('college_id');
        $student = $student->first();
        $request = TransferRequest::where('student_id', $id)
            ->whereNull('released')
            ->first();
        $re = null;
        if ($request) {
            if ($request->from_college_id == $college)
                $re = "out";
            elseif ($request->college_id == $college)
                $re = "in";
        }
        return view('college.registration.show', compact('student', 'request', 're'));
    }

    public function transferStudents()
    {
        $requests = TransferRequest::where('read', '0')->get();

        return view("rp.colleges.transfer.transfer", compact('requests'));
    }

    public function transferGoingStudents()
    {
        $id = college('college_id');
        $requests = TransferRequest::whereNull('from_confirm')
            ->whereNull('to_confirm')
            ->where('from_college_id', $id)
            ->whereNull('released')->get();

        return view("rp.colleges.transfer.going", compact('requests'));
    }

    public function transferComingStudents()
    {
        $id = college('college_id');
        $requests = TransferRequest::where('from_confirm', 1)
            ->whereNull('to_confirm')
            ->where('college_id', $id)
            ->whereNull('released')->get();

        return view("rp.colleges.transfer.coming", compact('requests'));
    }

    public function transferDecision(Request $request)
    {
        $student = $request->student;
        $action = $request->decision;
        $re = $request->input('request');
        $college = college('college_id');

        $request = TransferRequest::whereNull('released')
            ->where('student_id', $student);

        if ($re == 'in')
            $request = $request->where('college_id', $college);
        elseif ($re == 'out')
            $request = $request->where('from_college_id', $college);
        else
            $request = $request->where('id', sha1(time()));

        $request = $request->first();

        if (!$request) {
            if ($re == 'in')
                return redirect()->route('college.student.transfer.coming')->with('error', "Student didn't request any transfer !!!");
            else
                return redirect()->route('college.student.transfer.going')->with('error', "Student didn't request any transfer !!!");
        }
        $data = [
            'reg_no' => $student,
            'names' => getStudentInfo($student, 'names'),
            'college' => strtoupper($request->collegeAcronym),
            'department' => getDepartment($request->department_id, 'department_name'),
            'course' => strtoupper($request->course->choices),
            'from_college' => getCollege($request->from_college_id, 'polytechnic'),
            'from_department' => getDepartment($request->from_department_id, 'department_name'),
            'from_course' => getCourse($request->from_course_id, 'program_name')
        ];
        switch ($action) {
            case "r":
                if ($re == 'in') {
                    $request->read = 1;
                    $request->to_confirm = false;
                    $request->released = false;
                }
                if ($re == 'out') {
                    $request->read = 1;
                    $request->released = false;
                    $request->from_confirm = false;
                }
                $request->save();
                StudentNotification::create([
                    'student_id' => $student,
                    'message' => 'Request of transferring you to ' . strtoupper($request->collegeAcronym) . " for " . strtoupper($request->course->choices) . " was not possible."
                ]);
                $data['email'] = getStudentInfo($data['reg_no'], 'email');
                $data['re'] = 'r';
                Mail::to($data['email'])->send(new SendTranseringFeedBackMail($data));
                break;
            case "a":
                $request->read = 1;
                if ($re == 'in') {
                    if (!applied($student))
                        $student = AdmittedStudent::where('std_id', $student)->first();
                    else
                        $student = CollegeStudent::where('student_reg', $student)->first();
                    if ($student) {
                        $student->college_id = $request->college_id;
                        $student->course_id = $request->course_id;
                        $student->department_id = $request->department_id;
                        $data['email'] = $student->email;
                        $data['re'] = 'a';
                        if ($student->save()) {
                            StudentNotification::create([
                                'student_id' => $student->student_reg,
                                'message' => 'Request of transferring you to ' . strtoupper($request->collegeAcronym) . " for " . strtoupper($request->course->choices) . " was accepted."
                            ]);
                            $request->to_confirm = true;
                            $request->released = true;
                            $request->save();
                            Mail::to($data['email'])->send(new SendTranseringFeedBackMail($data));
                        }
                    } else
                        redirect()->route('college.student.transfer.coming')->with('warning', 'Student not yet Registered !!');
                }
                if ($re == 'out') {
                    $request->from_confirm = true;
                    $request->save();
                }
                break;
        }
        if ($re == 'in')
            return redirect()->route('college.student.transfer.coming')->with('success', 'Decision taken successfully.');
        else
            return redirect()->route('college.student.transfer.going')->with('success', 'Decision taken successfully.');
    }

    public function letter()
    {
        return view('college.letter');
    }

    public function storeLetter(Request $request)
    {
        $letter = college()->letter;
        if ($letter) {
            $letter->letter = $request->input('letter');
            $letter->save();
        } else {
            AdmissionLetter::create([
                'college_id' => college('college_id'),
                'letter' => $request->input('letter')
            ]);
        }

        return back()->with('success', "Updated successfully.");
    }

    public function getDataAllStudents(Request $request)
    {
        $aStudents = AdmittedStudent::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $aStudents->where('college_id', college('college_id'));
        $aStudents = $aStudents->get();
        //
        $pStudents = AdmissionPrivate::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $pStudents->where('college_id', college('college_id'));
        $pStudents = $pStudents->get();
        $students = collect();
        foreach ($aStudents as $aStudent) {
            if (!applied($aStudent->std_id) && !isSponsored($aStudent->std_id))
                $students->push(['id' => $aStudent->std_id, 'name' => $aStudent->std_id . " - " . $aStudent->names]);
        }
        foreach ($pStudents as $pStudent) {
            if (!$students->has($pStudent->std_id) && !isSponsored($pStudent->std_id))
                $students->push(['id' => $pStudent->std_id, 'name' => $pStudent->std_id . " - " . $pStudent->names]);
        }

//        $request->request->add(['q' => '18RP000']);
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $students = $students->filter(function ($name, $id) use ($term) {
            if (isset($name['name']))
                return false !== stristr($name['name'], $term);
        });

        $formatted_students = [];

        foreach ($students as $item) {
            $formatted_students[] = ['id' => $item['id'], 'text' => $item['name']];
        }

        return \Response::json($formatted_students);
    }

    public function data($type, $id, $college, Datatables $datatables)
    {
        switch ($type) {
            case 'departments':

                $departments = CollegeDepartment::where('college_id', $college)->where('program_id', $id)->get();
                return $departments;
                break;

            case 'options':
                $options = CollegeOption::where('college_id', $college)->where('department_id', $id)->get();
                return $options;
                break;
        }
    }

    public function exceptionStudent(Request $request)
    {
        if ($request->input('_token')) {
            $validator = Validator::make($request->all(), [
                'student_id' => 'required',
                'academic_year' => 'required',
                'description' => 'nullable|min:5'
            ]);
            $student = $request->input('student_id');
            if (isRegistered($student))
                return back()->withInput()->with(['status' => '0', 'message' => $student . ' seems register']);

            $data = $request->except('_token');
            $data['college_id'] = getStudentInfo($student, 'college_id', true);
            $save = false;
            if (!empty($data))
                $save = StudentExp::updateOrCreate(
                    ['student_id' => $data['student_id'], 'academic_year' => $data['academic_year']],
                    $data
                );

            if ($save)
                return back()->with(['status' => '1', 'message' => 'Enabling ' . $student . ' successfully .']);
            else
                return back()->with(['status' => '0', 'message' => 'Enabling ' . $student . ' failed !!']);

        }
        $students = StudentExp::where('academic_year', getCurrentAcademicYear())->get();
        return view('rp.students.exceptional', compact('students'));
    }

    public function getStudentsShortExpDestroy(Request $request, $id)
    {
        $student = StudentExp::findOrFail($id);

        $student->delete();

        return back()->with(['status' => '1', 'message' => $request->input('student_id') . ' deleted successfully .']);
    }

}
