<?php

namespace App\Http\Controllers\RpAuth;

use App\Http\Requests\RpProgramRequest;
use App\Http\Requests\RpProgramUpdateRequest;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use App\Rtqf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RpProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('o_o')) {
            $program = RpProgram::where('id', $request->input('o_o'))->first();
            $details = true;
            return view('rp.programs.index', compact('program', 'details'));
        }
        $program = null;
        $programs = RpProgram::all();
        $departments = RpDepartment::all();
        $rtqf_level = Rtqf::all();
        return view('rp.programs.index', compact('program', 'programs', 'rtqf_level', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!rpAllowed(4))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $program = null;
        $programs = RpProgram::all();
        $departments = RpDepartment::all();
        $rtqf_level = Rtqf::all();
        return view('rp.programs.create', compact('program', 'programs', 'rtqf_level', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RpProgramRequest $request)
    {
        if (!rpAllowed(4))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $program = new RpProgram();
        $program->department_id = $request->department_id;
        $program->program_name = $request->program_name;
        $program->program_code = $request->program_code;
        $program->program_load = $request->program_load;
        $program->rtqf_level = $request->rtqf_level;
        $program->program_is_stem = $request->program_is_stem;
        $program->program_status = $request->program_status;
        $program->tuition_fees = $request->tuition_fees;
        $program->other_course_fees = $request->other_course_fees;
        $program->admission_requirements = $request->admission_requirements;
        $save = $program->save();
        if ($save) {
            return redirect(route('rp.programs.index'))->with(['status' => '1', 'message' => "Program  Added Successfully."]);
        } else {
            return redirect(route('rp.programs.index'))->with(['status' => '1', 'message' => "Failed to   Adds Program "]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!rpAllowed(3))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $programs = RpProgram::all();
        $departments = RpDepartment::all();
        $rtqf_level = Rtqf::all();

        $program = RpProgram::where('id', $id)->first();
        if ($program) {
            return view('rp.programs.create', compact('program', 'programs', 'rtqf_level', 'departments'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RpProgramUpdateRequest $request, $id)
    {
        if (!rpAllowed(3))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $program = RpProgram::where('id', $id)->first();
        $program->department_id = $request->department_id;
        $program->program_name = $request->program_name;
        $program->program_code = $request->program_code;
        $program->program_load = $request->program_load;
        $program->rtqf_level = $request->rtqf_level;
        $program->program_is_stem = $request->program_is_stem;
        $program->program_status = $request->program_status;
        $program->tuition_fees = $request->tuition_fees;
        $program->other_course_fees = $request->other_course_fees;
        $program->admission_requirements = $request->admission_requirements;
        $save = $program->save();
        if ($save) {
            return redirect(route('rp.programs.index'))->with(['status' => '1', 'message' => "Program  Updated Successfully."]);
        } else {
            return redirect(route('rp.programs.index'))->with(['status' => '1', 'message' => "Failed to   Updated Program "]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!rpAllowed(3))
            return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

        $program = RpProgram::where('id', $id)->first();
        if ($program) {
            $program->delete();
            return redirect(route('rp.programs.index'))->with(['status' => '1', 'message' => 'Program Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
