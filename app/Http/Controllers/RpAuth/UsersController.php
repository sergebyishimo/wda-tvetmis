<?php

namespace App\Http\Controllers\RpAuth;

use App\College;
use App\Mail\SendAccountInformationCollegeMail;
use App\Rp\Polytechnic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colleges = College::all();
        return view('rp.users.index', compact('colleges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colleges = Polytechnic::all();
        $college = null;
        return view('rp.users.create', compact('colleges', 'college'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|email|unique:colleges'
        ]);

        $id = $request->input('college');
        $college = Polytechnic::findOrFail($id);

        $username = $request->username;
        $password = $request->password;
        $conf_password = $request->password_confirmation;

        if ($password == $conf_password) {
            $id = $college->id;

            $perms = $request->input('permission');
            $role = $request->role;

            if ($request->input('permission') && is_array($request->input('permission')))
                $perms = implode('', $request->input('permission'));

            $check = College::where('college_id', $id)->where('roles', $role)->count();
            if ($check && $role != 'a' && $role != 'r')
                return back()->withInput()->with(['status' => '0', 'message' => 'This college already have ' . getCollegeUserRole($role)]);

            College::create([
                'name' => $request->name,
                'username' => $username,
                'password' => bcrypt($password),
                'college_id' => $id,
                'permission' => $perms,
                'roles' => $role
            ]);
            $data = [
                'name' => $request->name,
                'college' => getCollege($id, 'polytechnic'),
                'email' => $username,
                'password' => $password
            ];

            Mail::to($username)->send(new SendAccountInformationCollegeMail($data));
            return back()->with(['status' => 1, 'message' => 'Account Created Successfully.']);
        } else {
            return back()->with(['status' => '0', 'message' => 'Passwords Dont Match.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $colleges = Polytechnic::all();
        $college = College::find($id);
        return view('rp.users.create', compact('colleges', 'college'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'college' => 'required',
            'permission' => 'required',
            'role' => 'required',
            'username' => 'required|email|unique:colleges,username,' . $request->input('col'),
            'password' => 'nullable|min:6|confirmed'
        ]);

        $id = $request->input('college');
        $college = Polytechnic::findOrFail($id);

        $id = $college->id;

        $perms = $request->input('permission');
        $role = $request->role;

        if ($request->input('permission') && is_array($request->input('permission')))
            $perms = implode('', $request->input('permission'));

        $check = College::where('college_id', $id)->where('roles', $role)->count();

        $college = College::find($request->input('col'));

        if ($check && $role != 'a' && $role != 'r')
            return back()->withInput()->with(['status' => '0', 'message' => 'This college already have ' . getCollegeUserRole($role)]);

        $college->name = $request->name;
        $college->username = $request->username;

        if ($request->input('password'))
            $college->password = bcrypt($request->password);

        $college->permission = $perms;
        $college->roles = $role;
        $college->save();

        return redirect()->route('rp.users.index')
            ->with(['status' => 1, 'message' => 'Account Updated Successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        College::find($id)->delete();
        return redirect()->back();
    }
}
