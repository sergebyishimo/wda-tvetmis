<?php

namespace App\Http\Controllers\RpAuth;

use App\FeeCategory;
use App\Http\Requests\CreateManageFeeCategoryRequest;
use App\ManageFeeCategory;
use App\Model\Rp\CollegeProgram;
use App\Model\Rp\FunctionalFees;
use App\Model\Rp\RpProgram;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\FuncCall;

class ManageFeeCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managefcat = null;
        $managefcats = ManageFeeCategory::all();
        $assigned = [];
        if ($managefcats)
            $assigned = $managefcats->pluck('fee_id')->toArray();

        $fees = FunctionalFees::all();
        if (! empty($assigned))
            $fees = FunctionalFees::whereNotIn('id', $assigned)->get();

        $cats = FeeCategory::all();
        return view('rp.managecategory.index', compact('managefcats', 'managefcat', "cats", 'fees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateManageFeeCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateManageFeeCategoryRequest $request)
    {
        $check = ManageFeeCategory::where('fee_category_id', $request->category_id)->where('fee_id', $request->fee_id)->first();
        if ($check) {
            return redirect(route('rp.managefuncfeecategory.index'))->with(['status' => '0', 'message' => "The Fee Already Assigned To That Category!."]);
        }

        $feecat = new ManageFeeCategory();
        $feecat->fee_category_id = $request->category_id;
        $feecat->fee_id = $request->fee_id;
        $save = $feecat->save();
        if ($save)
            return redirect(route('rp.managefuncfeecategory.index'))->with(['status' => '1', 'message' => "Assigned Fee To Category  Successfully."]);
        else
            return redirect(route('rp.managefuncfeecategory.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Assign Fee to This Category."]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $fee_id)
    {
        $fees = FunctionalFees::all();
        $cats = FeeCategory::all();
        $managefcat = ManageFeeCategory::where('fee_category_id', $id)->where('fee_id', $fee_id)->first();
        $cat = FeeCategory::where('id', $id)->first();
        if ($managefcat && $cat) {
            return view('rp.managecategory.index', compact('managefcat', 'fees', 'cats', 'cat', 'fee_id'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $managecat = ManageFeeCategory::where('fee_category_id', $id)->first();
        if ($managecat) {
            $managecat->fee_category_id = $request->category_id;
            $managecat->fee_id = $request->fee_id;
            $save = $managecat->save();
            if ($save)
                return redirect(route('rp.managefuncfeecategory.index'))->with(['status' => '1', 'message' => "Assigned Fee To Category  Successfully."]);
            else
                return redirect(route('rp.managefuncfeecategory.index'))->withInput()
                    ->with(['status' => '0', 'message' => "Failed to Assign Fee to Category."]);

        } else {
            return redirect(route('rp.managefuncfeecategory.index'))->with(['status' => '0', 'message' => "Update Failed"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $fee_id)
    {
        $manageprogram = ManageFeeCategory::where('fee_category_id', $id)->where('fee_id', $fee_id)->first();
        if ($manageprogram) {
            $manageprogram->delete();
            return redirect(route('rp.managefuncfeecategory.index'))->with(['status' => '1', 'message' => 'Program removed to specified college successful']);
        } else
            abort(409);
    }
}
