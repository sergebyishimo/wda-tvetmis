<?php

namespace App\Http\Controllers\RpAuth;

use App\FeeCategory;
use App\Http\Requests\CreateFunctionFeeCategoryRequest;
use App\Http\Requests\UpdateFunctionFeeCategoryRequest;
use App\Model\Rp\RpProgram;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FunctionalFeeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functionalfeecategory = null;
        $functionalfeecategories = FeeCategory::all();
        $programs = RpProgram::all();
        if ($programs)
            $programs = $programs->pluck('program_name', 'id');
        return view('rp.feescategory.index', compact('functionalfeecategory', 'functionalfeecategories', 'programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $functionalfeecategory = null;
        $functionalfeecategories = FeeCategory::all();
        $programs = RpProgram::all();
        if ($programs)
            $programs = $programs->pluck('program_name', 'id');
        return view('rp.feescategory.index', compact('functionalfeecategory', 'functionalfeecategories','programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateFunctionFeeCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFunctionFeeCategoryRequest $request)
    {
        $functionalfeecategory = new FeeCategory();
        $functionalfeecategory->category_name = $request->category_name;
        $functionalfeecategory->mandatory = $request->mandatory ? : '0';
        $functionalfeecategory->multiple_time = $request->multiple_time ? : '0';
        $functionalfeecategory->program_id = $request->input('program')?:null;
        $save = $functionalfeecategory->save();
        if ($save)
            return redirect(route('rp.functionalfeescategory.index'))->with(['status' => '1', 'message' => "Functional fee  category Added Successfully."]);
        else
            return redirect(route('rp.functionalfeescategory.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Functional Fee Category."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functionalfeecategories = FeeCategory::all();
        $functionalfeecategory = FeeCategory::where('id', $id)->first();
        $programs = RpProgram::all();
        if ($programs)
            $programs = $programs->pluck('program_name', 'id');
        if ($functionalfeecategory) {
            return view('rp.feescategory.index', compact('functionalfeecategory', 'functionalfeecategories', 'programs'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFunctionFeeCategoryRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFunctionFeeCategoryRequest $request, $id)
    {
        $functionalfeecategory = FeeCategory::where('id', $id)->first();
        if (!$functionalfeecategory)
            return abort(419);

        $functionalfeecategory->category_name = $request->input('category_name');
        $functionalfeecategory->mandatory = $request->input('mandatory');
        $functionalfeecategory->multiple_time = $request->input('multiple_time');
        $functionalfeecategory->program_id = $request->input('program')?:null;

        $save = $functionalfeecategory->save();
        if ($save)
            return redirect(route('rp.functionalfeescategory.index'))->with(['status' => '1', 'message' => "Functional Fee Category Updated Successfully."]);
        else
            return redirect(route('rp.functionalfeescategory.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Functional Fee Category."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $functionalfeecategory = FeeCategory::where('id', $id)->first();
        if ($functionalfeecategory) {
            $functionalfeecategory->delete();
            return redirect(route('rp.functionalfeescategory.index'))->with(['status' => '1', 'message' => 'Functional Fee Category Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
