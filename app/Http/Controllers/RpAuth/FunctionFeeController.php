<?php

namespace App\Http\Controllers\RpAuth;

use App\Http\Requests\CreateFunctionFeeRequest;
use App\Http\Requests\UpdateFunctionFeeRequest;
use App\Model\Rp\FunctionalFees;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FunctionFeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functionalfee = null;
        $functionalfees = FunctionalFees::all();
        return view('rp.functionalfees.index', compact('functionalfee', 'functionalfees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $functionalfee = null;
        $functionalfees = FunctionalFees::all();
        return view('rp.functionalfees.index', compact('functionalfee', 'functionalfees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateFunctionFeeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFunctionFeeRequest $request)
    {
        $functionalfee = new FunctionalFees();
        $functionalfee->fee_name = $request->fee_name;
        $functionalfee->govt_sponsored_amount = $request->govt_sponsored_amount;
        $functionalfee->private_sponsored_amount = $request->private_sponsored_amount;
        $functionalfee->paid_when = $request->paid_when;

        $save = $functionalfee->save();
        if ($save)
            return redirect(route('rp.functionalfees.index'))->with(['status' => '1', 'message' => "Functional fee  Added Successfully."]);
        else
            return redirect(route('rp.functionalfees.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Functional Fee."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functionalfees = FunctionalFees::all();
        $functionalfee = FunctionalFees::where('id', $id)->first();
        if ($functionalfee) {
            return view('rp.functionalfees.index', compact('functionalfee', 'functionalfees'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFunctionFeeRequest $request, $id)
    {
        $functionalfee = FunctionalFees::where('id', $id)->first();
        if (!$functionalfee)
            return abort(419);

        $functionalfee->fee_name = $request->input('fee_name');
        $functionalfee->govt_sponsored_amount = $request->input('govt_sponsored_amount');
        $functionalfee->private_sponsored_amount = $request->input('private_sponsored_amount');
        $functionalfee->paid_when = $request->input('paid_when');
        $save = $functionalfee->save();
        if ($save)
            return redirect(route('rp.functionalfees.index'))->with(['status' => '1', 'message' => "Functional Fee Updated Successfully."]);
        else
            return redirect(route('rp.functionalfees.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Functional Fee."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!rpAllowed(5))
            return abort(401);

        $functionalfees = FunctionalFees::where('id', $id)->first();
        if ($functionalfees) {
            $functionalfees->delete();
            return redirect(route('rp.functionalfees.index'))->with(['status' => '1', 'message' => 'Functional Fee Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
