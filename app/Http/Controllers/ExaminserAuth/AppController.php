<?php

namespace App\Http\Controllers\ExaminserAuth;

use App\MarkingActivation;
use App\Reb;
use App\RebStaffMarkerApplication;
use App\School;
use App\StaffAssessorApplication;
use App\StaffMarkerApplication;
use App\StaffsInfo;
use App\Transformers\AssessingApplicantsTransformer;
use App\Transformers\MarkingApplicantsTransformer;
use App\Transformers\RebApplicantsTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Yajra\Datatables\Datatables;


class AppController extends Controller
{
    public function activation()
    {
        return view('examiner.application.activation');
    }

    public function updateActivation(Request $request)
    {
        $active = MarkingActivation::all();
        if ($active->count()) {
            $active = $active->first();
            $active->update($request->all());
            return back()->with(['status' => '1', 'message' => 'Setting successfully updated']);
        } else {
            $save = MarkingActivation::create($request->all());
            if ($save)
                return back()->with(['status' => '1', 'message' => 'Setting successfully saved']);
            else
                return back()->with(['status' => '0', 'message' => 'Problem occurs, try again later !!']);
        }
    }

    public function applicantMarking()
    {
        return view('examiner.application.marking');
    }

    public function applicantAssessing()
    {
        return view('examiner.application.assessor');
    }

    public function applicantReb()
    {
        return view('examiner.application.reb');
    }

    public function getApplicantMarking()
    {

    }

    public function data(Datatables $datatables, $type)
    {
        switch ($type) {
            case 'm':
                $jmodel = StaffMarkerApplication::select([
                    'staffs_info.*', 'staff_marker_applications.*', 'staffs_info.id as sid',
                    'source_tvet_sub_field.sub_field_name', 'schools.name'])
                    ->join('staffs_info', 'staff_marker_applications.staff_id', '=', 'staffs_info.id')
                    ->join('schools', 'schools.id', '=', 'staffs_info.school_id')
                    ->join('source_tvet_sub_field', 'source_tvet_sub_field.id', '=', 'staff_marker_applications.education_program_id')
                    ->where('staff_marker_applications.academic_year', date('Y'));

                $dd = $datatables->eloquent($jmodel->newQuery())
                    ->setTransformer(new MarkingApplicantsTransformer)
                    ->rawColumns(['photo', 'names', 'actions'])
                    ->smart(false)
                    ->make(true);

                return $dd;

                break;
            case 'r':

                $jmodel = Reb::select([
                    'rebs.*', 'reb_staff_marker_applications.*', 'rebs.id as sid',
                    'source_tvet_sub_field.sub_field_name'])
                    ->join('reb_staff_marker_applications', 'reb_staff_marker_applications.staff_id', '=', 'rebs.id')
                    ->join('source_tvet_sub_field', 'source_tvet_sub_field.id', '=', 'reb_staff_marker_applications.education_program_id')
                    ->where('reb_staff_marker_applications.academic_year', date('Y'));

                $dd = $datatables->eloquent($jmodel)
                    ->setTransformer(new RebApplicantsTransformer)
                    ->rawColumns(['photo', 'names', 'actions'])
                    ->make(true);

                return $dd;

                break;

            case 'a':
                $jmodel = StaffAssessorApplication::select([
                    'staffs_info.*', 'staff_assessor_applications.*', 'staffs_info.id as sid',
                    'source_tvet_sub_field.sub_field_name' /*, 'schools.name'*/])
                    ->join('staffs_info', 'staff_assessor_applications.staff_id', '=', 'staffs_info.id')
//                    ->join('schools', 'schools.id', '=', 'staffs_info.school_id')
                    ->join('source_tvet_sub_field', 'source_tvet_sub_field.id', '=', 'staff_assessor_applications.trade')
                    ->where('staff_assessor_applications.academic_year', date('Y'));

                $dd = $datatables->eloquent($jmodel)
                    ->setTransformer(new AssessingApplicantsTransformer)
                    ->rawColumns(['photo', 'names', 'actions'])
                    ->make(true);

                return $dd;

                break;
        }
    }

    public function readStaff($id)
    {
        $frm = 'nom';
        if (strlen($id) != 36) {
            $frm = 'reb';
            $staff = Reb::findOrFail($id);
            $myAttachement = $staff->attachement;
            $currentMarker = $staff->currentMarking;
            $currentAssessor = null;
            $backgroundsW = $staff->workingExperience;
            $backgroundsT = $staff->teachingExperience;
            $backgroundsM = $staff->markingBackgrounds;

        } else {
            $staff = StaffsInfo::findOrFail($id);
            $myAttachement = $staff->attachement;
            $currentMarker = $staff->currentMarking;
            $currentAssessor = $staff->currentAssessor;
            $backgroundsW = $staff->workingExperience;
            $backgroundsT = $staff->teachingExperience;
            $backgroundsM = $staff->markingBackgrounds;
            $backgroundsA = $staff->assessorBackgrounds;
        }

        return view('examiner.application.read', compact('staff', 'frm', 'myAttachement', 'backgroundsW', 'backgroundsT', 'backgroundsM', 'backgroundsA', 'currentMarker', 'currentAssessor'));
    }

}
