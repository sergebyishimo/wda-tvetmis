<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;

class NotificationController extends Controller
{
    function __construct()
    {
        if (auth()->guard('rp')->check())
            $this->middleware(['rp']);
        elseif (auth()->guard('wda')->check())
            $this->middleware(['wda']);
    }

    public function index()
    {
        $notifications = Notification::orderBy('created_at', "DESC")->get();

        return view('notifications', compact('notifications'));
    }

    public function create()
    {
        return view('notificationCreate');
    }

    public function store(Request $request)
    {
        $noti = new Notification();

        $user_id = null;
        $real_guard = null;
        $guards = array_keys(config('auth.guards'));
        foreach ($guards as $guard) {
            if (auth()->guard($guard)->check()) {
                $real_guard = $guard;
                $user_id = auth()->guard($guard)->user()->id;
            }
        }

        $noti->user_type = $real_guard;
        $noti->title = $request->title;
        $noti->message = $request->message;
        $noti->user_id = $user_id;

        $noti->save();

        return redirect(route('notifications.index'));
    }

    public function delete($id)
    {
        $not = Notification::find($id);
        if ($not)
            $not->delete();

        return back()->with(['status' => '1', 'message' => 'Notification Deleted']);
    }

}
