<?php

namespace App\Http\Controllers\CollegeAuth;

use App\CollegeContinuingStudent;
use App\Department;
use App\Level;
use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\RtqfSource;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\TrainingSectorsSource;
use App\Model\Accr\TrainingTradesSource;
use App\Model\Rp\RpCourseUnit;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use App\Rp\CollegeStudent;
use App\Student;
use App\Transformers\ListAllAdmittedStudentsTransformer;
use App\Rp\AdmissionLetter;
use App\College;
use App\Rp\AdmittedStudent;
use App\Rp\Polytechnic;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;
use App\Rp\CollegeDepartment;
use App\Rp\CollegeOption;
use App\Rp\CollegeModule;
use Yajra\DataTables\DataTables;

class CollegeController extends Controller
{
    function __construct()
    {

    }

    public function updateProfile(Request $request)
    {
        $rule['photo'] = "mimes:png";
        if (college("email"))
            $rule['email'] = "unique:colleges,email," . college("id");

        if (college("telephone"))
            $rule['telephone'] = "unique:colleges,telephone," . college("id");


        $this->validate($request, $rule);

        $request->request->remove('_token');
        $input = $request->input();
        $files = $request->allFiles();

        if ($request->input('password'))
            $input['password'] = bcrypt($input->password);
        else
            $input['password'] = college('password');

        $pol = Polytechnic::find(college('college_id'));
        if ($pol) {
            $pol->polytechnic = $input['polytechnic'];
            $pol->short_name = $input['name'];
            $pol->save();
        }
        $photoP = "/img/logos";
        if (!empty($files)) {
            if ($request->hasFile('photo')) {
                $photo = $request->file('photo');
                $descF = Storage::disk('public_uploads')->putFileAs($photoP, $photo, $input['username'] . "." . $photo->getClientOriginalExtension(), 'public');
                $input["logo"] = $descF;
            }
        }

        $college = College::findOrFail($input['id']);

        $college->name = $input['name'];
        $college->username = $input['username'];
        $college->password = $input['password'];
        $college->logo = isset($input['logo']) ? $input['logo'] : $this->renameLogo($input['username'], $photoP);
        $college->province = $input['province'];
        $college->district = $input['district'];
        $college->email = $input['email'];
        $college->telephone = $input['telephone'];
        $college->po_box = $input['po_box'];
        $college->website = $input['website'];
        $college->save();

        return back()->with(['status' => '1', 'message' => 'Updated Successfully !']);
    }

    private function renameLogo($p, $f)
    {
        $exp = explode("/", college('logo'));
        $name = end($exp);
        $cp = explode('.', $name);
        $ext = end($cp);
        $usr = $cp[0];

        if ($p == $usr)
            return college('logo');
        else {
            $nfile = $f . "/" . $p . "." . $ext;
            Storage::disk('public_uploads')->move(college('logo'), $nfile);
            return $nfile;
        }
    }

    public function letter()
    {
        return view('college.letter');
    }

    public function storeLetter(Request $request)
    {
        $letter = college()->letter;
        if ($letter) {
            $letter->letter = $request->input('letter');
            $letter->save();
        } else {
            AdmissionLetter::create([
                'college_id' => college('college_id'),
                'letter' => $request->input('letter')
            ]);
        }

        return back()->with('success', "Updated successfully.");
    }

    public function data($type, $id, Datatables $datatables)
    {
        $year = request()->input('y');
        switch ($type) {
            case 'departments':
                if ($year === 1 || $year === '1') {
                    $get = Polytechnic::find(college('college_id'));
                    $departments = collect([]);
                    if ($get)
                        $departments = $get->departments()
                            ->distinct('department_name')
                            ->get(['departments.id', 'department_name']);
                } else
                    $departments = CollegeDepartment::where('college_id', college('college')->id)->where('program_id', $id)->get();
                return $departments;
                break;

            case 'options':
                if ($year === 1 || $year === '1') {
                    $polytechnic = Polytechnic::find(college('college_id'));
                    $options = [];
                    if ($polytechnic) {
                        $programs = $polytechnic->programs()->distinct('program_name');
                        foreach ($programs->get() as $program) {
                            if ($program->department_id == $id)
                                $options[] = [
                                    'id' => $program->id,
                                    'program_name' => $program->program_name
                                ];
                        }
                    }
                    $options = json_encode($options);
                } else
                    $options = CollegeOption::where('college_id', college('college')->id)->where('department_id', $id)->get();

                return $options;
                break;

            case 'modules':
                if ((int)$year === 1) {
                    $modules = RpCourseUnit::join('source_level_of_study', 'source_level_of_study.id', '=', 'program_course_units.level_of_study_id')
                        ->join('source_semesters', 'source_semesters.id', '=', 'program_course_units.semester_id')
                        ->where('program_course_units.program_id', $id);

                    if (request()->input('y'))
                        $modules = $modules->where('source_level_of_study.level_of_study', request()->input('y'));

                    if (request()->input('semester'))
                        $modules = $modules->where('source_semesters.semester', request()->input('semester'));

                    $modules = $modules->select([
                        'program_course_units.id',
                        'program_course_units.course_unit_code as module_code',
                        'program_course_units.course_unit_name as module_name'
                    ]);
                } else {
                    $modules = CollegeModule::where('college_id', college('college_id'))->where('option_id', $id);

                    if (request()->input('semester'))
                        $modules = $modules->where('semester', request()->input('semester'));

                    if (request()->input('y'))
                        $modules = $modules->where('year_of_study', request()->input('y'));
                }

                $modules = $modules->get();
                return $modules;
                break;
            case "students":
                $option = $id;
                $college = college('college_id');
                $students = CollegeStudent::where('college_id', $college)
                    ->where('course_id', $option)
                    ->where('year_of_study', $year)
                    ->get();
                return $students;
                break;

            case 'admittedStudents':
                $jmodel = AdmittedStudent::where('college_id', college('college')->id)
                    ->where('year_of_study', date('Y'))
                    ->orWhere('year_of_study', '1')
                    ->whereYear('created_at', date('Y'))
                    ->select(['std_id', 'first_name', 'other_names', 'department_id', 'course_id', 'aggregates_obtained', 'phone', 'photo', 'sponsorship_status']);

                $dd = $datatables->eloquent($jmodel)
                    ->setTransformer(new ListAllAdmittedStudentsTransformer())
                    ->rawColumns(['photo', 'nber', 'actions'])
                    ->smart(false)
                    ->make(true);
//                dd($dd);
                return $dd;
                break;
            default:
                # code...
                break;
        }
    }

    public function correctOldReg()
    {

        for ($i = 1; $i <= 7; $i++) {
            for ($j = 2; $j <= 3; $j++) {
                $students = DB::select("SELECT COUNT(*) AS nber, student_reg, first_name, other_names, college_id, course_id FROM `rp_college_students` WHERE college_id = $i AND year_of_study = $j AND student_reg NOT IN ( SELECT student_reg FROM rp_continuing_students ) GROUP BY student_reg, first_name, other_names, college_id, course_id");

                foreach ($students as $student) {
                    try {
                        CollegeContinuingStudent::where('first_name', $student->first_name)->where('last_name', $student->other_names)->where('student_reg', '!=', $student->student_reg)->update(['student_reg' => $student->student_reg]);
                    } catch (QueryException $e) {
                        dd($e->getMessage());
                    }

                }
            }
        }

        return 'true';
    }

}
