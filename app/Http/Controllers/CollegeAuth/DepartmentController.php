<?php

namespace App\Http\Controllers\CollegeAuth;

use App\Rp\CollegeDepartment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = null;
        $departments = CollegeDepartment::where('college_id', college('college')->id)->get();
        return view('college.department.index',  compact('department', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $department = null;
        // $departments = CollegeDepartment::all();
        $dept = new CollegeDepartment();
        $dept->college_id = college('college')->id;
        $dept->program_id = $request->program_id;
        $dept->department_name = $request->department_name;
        $save = $dept->save();
        if ($save)
            return redirect(route('college.departments.index'))->with(['status' => '1', 'message' => "Department Added Successfully."]);
        else
            return redirect(route('college.departments.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Department."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rp\CollegeDepartment  $collegeDepartment
     * @return \Illuminate\Http\Response
     */
    public function show(CollegeDepartment $collegeDepartment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rp\CollegeDepartment  $collegeDepartment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = CollegeDepartment::where('college_id', college('college')->id)->get();
        $department = CollegeDepartment::where('id', $id)->first();
        if ($department) {
            return view('college.department.index', compact('department', 'departments'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rp\CollegeDepartment  $collegeDepartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CollegeDepartment $collegeDepartment,  $id)
    {
        $department = CollegeDepartment::find($id);
        if (!$department)
            return abort(419);
        
        $department->program_id = $request->program_id;
        $department->department_name = $request->department_name;
        $save = $department->save();

        if ($save)
            return redirect(route('college.departments.index'))->with(['status' => '1', 'message' => "Department Updated Successfully."]);
        else
            return redirect(route('college.departments.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a Department."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rp\CollegeDepartment  $collegeDepartment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = CollegeDepartment::find($id);
        if ($department) {
            $department->delete();
            return redirect(route('college.departments.index'))->with(['status' => '1', 'message' => 'Department Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
