<?php

namespace App\Http\Controllers\CollegeAuth;

use App\College;
use App\CollegeContinuingStudent;
use App\Model\Rp\AdmissionPrivate;
use App\Model\Rp\EnrolStudent;
use App\Model\Rp\StudentRegistered;
use App\Rp\CollegeStudent;
use App\Rp\AdmittedStudent;
use App\Rp\PendingPaymentInvoice;
use App\Rp\Student;
use Illuminate\Http\Request;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\CollegeProgram;
use App\Http\Controllers\Controller;
use App\Rp\Polytechnic;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $college = college('college');
        $students = null;
        if ($college)
            $students = $college->students()->paginate(50);

        $departments = RpDepartment::all();
        return view('college.students.index', compact('students', 'departments'));
    }

    public function indexRp(Request $request)
    {
        $colleges = Polytechnic::all();
        $departments = RpDepartment::all();

        if ($request->department) {
            $students = CollegeStudent::where('college_id', $request->college)
                ->where('department_id', $request->department)
                ->where('course_id', $request->program)
                ->where('year_of_study', $request->year)
                ->where('sponsorship_status', $request->sponsor)
                ->paginate(50);
            $programs = CollegeProgram::where('college_id', $request->college)->where('program_id', $request->program)->get();
            return view('college.students.index', compact('colleges', 'departments', 'students', 'programs'));
        }

        return view('college.students.index', compact('colleges', 'departments'));
    }

    public function admitted(Request $request)
    {
        $students = [];
        if ($request->option_id) {
            $students = AdmittedStudent::where('college_id', college('college')->id)
                ->whereIn('year_of_study', [1, date('Y')])
                ->where('course_id', $request->option_id)
                ->whereYear('created_at', date('Y'))
                ->get();
        }

        return view('college.students.admitted', compact('students'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function single(Request $request)
    {
        return view('college.students.queries.single');
    }

    public function queryExecuting(Request $request)
    {
        if (auth()->guard('college')->check())
            $route = "college.student.single";
        elseif (auth()->guard('rp')->check())
            $route = "rp.student.single";
        else
            return abort(404);

        if ($request->has('student') && !$request->has('execute')) {
            $student = $request->get('student');
            if (filter_var($student, FILTER_VALIDATE_EMAIL)) {
                $std = Student::where('email', $student)->first();
                if ($std)
                    $student = $std->id;
                else
                    return redirect()->route($route)->with(['status' => '0', 'message' => 'Email not find !!!']);
            } else {
                $std = Student::where('id', $student)->first();
                if (!$std)
                    return redirect()->route($route)->with(['status' => '0', 'message' => 'Id not find !!!']);
            }

            if (auth()->guard('college')->check()) {
                $college_id = getStudent($student, 'college_id');
                $college = college('college_id');
                if ($college_id != $college)
                    return redirect()->route($route)->with(['status' => '3', 'message' => 'Student not find !!!']);
            }

            if ($request->input('action') == 'e' && $request->input('model') != 'r') {
                if (auth()->guard('rp')->check())
                    return redirect()->route('rp.registration.edit', $student);
                elseif (auth()->guard('college')->check())
                    return redirect()->route('college.registration.edit', $student);
            }

            $action = $request->get('action');
            $model = $request->get('model');
            return view('college.students.queries.single', compact('student', 'action', 'model'));
        } elseif ($request->has('execute')) {
            $student = $request->get('student');
            if ($request->input('model') == 'u') {
                if ($request->input('execute') == 'd') {
                    if (isContinuingStudent($student))
                        return redirect()->route($route)->with(['status' => '3', 'message' => 'This is a continuing student use the appropriate link !!']);
                    $studentUserAccount = Student::where('id', $request->input('student'))->first();
                    if (isAdmitted($request->input('student')))
                        $studentInfo = AdmittedStudent::where('std_id', $request->input('student'))->first();
                    else
                        $studentInfo = AdmissionPrivate::where('std_id', $request->input('student'))->first();

                    $invoices = PendingPaymentInvoice::where('student_id', $student)
                        ->where('active', 1);

                    $disable = false;
                    if ($invoices->count() > 0) {
                        foreach ($invoices->get() as $invoice) {
                            if ($invoice->paid >= $invoice->amount) {
                                $disable = true;
                                break;
                            }
                        }
                    }
                    if ($disable || applied($student) || paidOutSide($student) || sponsored($student))
                        return redirect()->route($route)->with(['status' => '3', 'message' => 'Deleting this student is impossible (either have payments or already registered)']);
                    $de = false;
                    if ($studentUserAccount) {
                        if ($studentUserAccount->forceDelete())
                            $de = true;
                    }
                    if ($de) {
                        if ($studentInfo)
                            $studentInfo->delete();
                        else
                            return redirect()->route($route)->with(['status' => '3', 'message' => 'Deleted with some problems !!! (' . $studentUserAccount->id . ')']);
                    }
                    return redirect()->route($route)->with(['status' => '0', 'message' => 'Successfully Deleted']);
                }
            }

            if ($request->input('model') == 'r') {
                $new_reg = continuingGenerateReg(0, $request->year_of_study);

                Student::where('id', $request->student)->update(['id' => $new_reg]);
                $student = Student::where('id', $new_reg)->first();
                AdmittedStudent::where('std_id', $request->student)->update(['std_id' => $new_reg]);
                CollegeContinuingStudent::where('student_reg', $request->student)->update(['student_reg' => $new_reg]);

                $message = 'Hi ' . $student->name . ', your registration number has changed from ' . $request->student . ' to ' . $new_reg;
                Mail::raw($message, function ($message) use ($request, $student) {
//                global $request;
                    $message->to($student->email)
                        ->subject('Notification');
                });

                return redirect()->route($route)->with(['status' => '1', 'message' => 'Successfully Updated The Information']);

            }


        }

        return redirect()->route($route);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function enrol()
    {
        if (auth()->guard('college')->check())
            $students = EnrolStudent::where('college_id', college('college_id'))
                ->whereYear('created_at', date('Y'))
                ->paginate(25);
        else
            $students = EnrolStudent::whereYear('created_at', date('Y'))->paginate(25);

        return view('college.students.enrol', compact('students'));
    }

    public function admissionLetter($stdid = null)
    {
        $pathX = '/storage/admissionLetter/' . date('Y') . '/';
        $path = public_path() . $pathX;
        \File::isDirectory($path) or \File::makeDirectory($path, 0777, true, true);

//        $pdf = \App::make('dompdf.wrapper');
//        $pdf->loadView('student.admission.letter');
        $functionalfees = \App\Model\Rp\FunctionalFees::all();
        $fees = $functionalfees->whereIn('paid_when', ['first_year', 'each_year']);

        $registration = \App\FeeCategory::where('category_name', "like", "%" . ucwords("registration") . "%")->first();
        $schoolRFees = \App\FeeCategory::where('category_name', "like", "%" . ucwords("school fees") . "%")->first();
        $rep = "";
        $totalRegFees = "";
        $schoolFees = "";

        if ($registration->fees) {
            $totalRegFees = $registration->fees->pluck('govt_sponsored_amount')->sum() . " Rwf";
            $x = 0;
            foreach ($registration->fees as $fee) {
                if ($x > 0)
                    $rep .= "; ";
                $rep .= $fee->fee_name . " " . number_format($fee->govt_sponsored_amount) . " Rwf";
                $x++;
            }
        }

        if ($schoolRFees->fees)
            $schoolFees = number_format($schoolRFees->fees->pluck('govt_sponsored_amount')->sum()) . " Rwf";

        $pdf = \PDF::loadView('student.admission.letters', compact('rep', 'totalRegFees', 'schoolFees'));

//        return view('student.admission.letters', compact('rep', 'totalRegFees', 'schoolFees'));
        return $pdf->stream($stdid . '.pdf');

    }

    public function getStudentWithoutProf()
    {
        $academic_year = date('Y-') . date('Y', strtotime('+1 year'));
        $studentsProf = StudentRegistered::where('academic_year', 'LIKE', $academic_year);
        if ($studentsProf->count() > 0)
            $studentsProf = $studentsProf->pluck('student_id')->toArray();

        $studentsNotProf = CollegeStudent::whereNotIn('student_reg', $studentsProf)
            ->get(['college_id', 'student_reg', 'year_of_study']);
        if ($studentsNotProf->count()) {
            $studentsNotProf = collect($studentsNotProf->toArray());
            $studentsNotProf = $studentsNotProf->map(function ($data) use ($academic_year) {
                $data['student_id'] = $data['student_reg'];
                unset($data['student_reg']);
                $data['academic_year'] = $academic_year;
                $data['prof'] = 0;
                return $data;
            });
        } else
            $studentsNotProf = collect([]);

        if ($studentsNotProf->count() > 0) {
            $arrayLink = $studentsNotProf->toArray();
            $insert = StudentRegistered::insert($arrayLink);
            if ($insert)
                return "Done";
            else
                return "Oops";
        } else
            return "All has prof link ...";
    }


}
