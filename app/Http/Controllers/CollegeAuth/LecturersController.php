<?php

namespace App\Http\Controllers\CollegeAuth;

use App\Http\Requests\StoreLecturerRequest;
use App\Lecturer;
use App\Mail\LecturerAccountCreatedSuccessfullyMail;
use App\Model\Rp\LecturerAndCourse;
use App\Model\Rp\StaffPositionSource;
use App\Rp\CollegeDepartment;
use App\Rp\CollegeOption;
use App\Rp\Polytechnic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class LecturersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = "college.lecturers.partials.list";
        $ids = [];
        if (auth()->guard('lecturer')->check())
            $college = lecturer(null, 'college_id');
        else
            $college = college('college_id');

        $lecturers = Lecturer::where('college_id', $college);
        if (auth()->guard('lecturer')->check() && isHOD()) {
            $academicYear = getCurrentAcademicYear();
            $hod = lecturer(null, null);
            $id = LecturerAndCourse::where('college_id', $college)
                ->where('academic_year', $academicYear)
                ->where('department_id', $hod->department)
                ->where('year_of_study', $hod->year_of_study)
                ->distinct('lecturer_id')
                ->get();
            if ($id->count())
                $ids = $id->pluck('lecturer_id');

            $lecturers = $lecturers->whereIn('id', $ids);
        }

        $lecturers = $lecturers->get();
        return view('college.lecturers.index', compact('view', 'lecturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = StaffPositionSource::all();
        if ($positions)
            $positions = $positions->pluck('position_name', 'id');
        else
            $positions = [];

        return view('college.lecturers.create', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLecturerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreLecturerRequest $request)
    {
        $request->request->add([
            'college_id' => college('college_id'),
            'password' => bcrypt($request->input('telephone'))
        ]);
        $input = $request->except(['_token']);
        $path = null;
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $name = bcrypt(college('id') . time() . rand(1, 1000)) . '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('colleges/staff/' . date('Y_F'), $name, 'public');
        }
        $input['photo'] = $path;
        $save = Lecturer::create($input);

        $positions = StaffPositionSource::where('position_name', 'LIKE', '%lecturer%')
            ->where('id', $request->input('position'))
            ->first();

        if ($save) {
            if ($positions)
                Mail::to($save->email)->send(new LecturerAccountCreatedSuccessfullyMail($input));

            return redirect(route('college.lecturers.index'))
                ->with(['status' => '1', 'message' => 'Account Created Successfully']);
        }

        return back()->withInput()->with(['status' => '0', 'message' => "Oops, error occurs .. "]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function massUpload(Request $request)
    {

    }

    public function courseAssigning(Request $request)
    {
        if ($request->has('_token')) {
            $academicYear = getCurrentAcademicYear();
            $college = college('college_id');
            $exists = LecturerAndCourse::where('college_id', $college)
                ->where('academic_year', $academicYear)
                ->where('lecturer_id', $request->input('lecturer_id'))
                ->where('course_id', $request->input('course_id'))->get();
            if ($exists->count() > 0)
                return response()->json(['status' => 'fail', 'message' => 'Already assigned !!!']);
            else {
                $save = LecturerAndCourse::create([
                    'college_id' => $college,
                    'lecturer_id' => $request->input('lecturer_id'),
                    'course_id' => $request->input('course_id'),
                    'department_id' => $request->input('department_id'),
                    'program_id' => $request->input('program_id'),
                    'year_of_study' => $request->input('year_of_study'),
                    'academic_year' => $academicYear
                ]);
                if ($save)
                    return response()->json(['status' => 'ok', 'message' => 'Assigned Successfully']);
                else
                    return response()->json(['status' => 'fail', 'message' => 'Oops, error occurs !!']);
            }
        } else {
            $positions = StaffPositionSource::where('position_name', 'LIKE', '%lecturer%')->get();
            if ($positions)
                $positions = $positions->pluck('id')->toArray();
            else
                $positions = [];

            $college = college('college_id');
            if (lecturer(null, 'college_id'))
                $college = lecturer(null, 'college_id');

            $lecturers = Lecturer::where('college_id', $college)
                ->whereIn('position', $positions)
                ->get();
            if ($lecturers->count() > 0)
                $lecturers = $lecturers->pluck('name', 'id');
            else
                $lecturers = [];

            if (auth()->guard('lecturer')->check()) {
                if (isHOD()) {
                    $year = lecturer(null, 'year_of_study');
                    $department = lecturer(null, 'department');

                    if ($year === 1 || $year === '1') {
                        $polytechnic = Polytechnic::find($college);
                        $options = [];
                        if ($polytechnic) {
                            $programs = $polytechnic->programs()->distinct('program_name');
                            foreach ($programs->get() as $program) {
                                if ($program->department_id == $department)
                                    $options[] = [
                                        'id' => $program->id,
                                        'program_name' => $program->program_name
                                    ];
                            }
                        }
                        $options = $options;
                    } else
                        $options = CollegeOption::where('college_id', $college)
                            ->where('department_id', $department)
                            ->get();

                    if ($year === 1 || $year === '1') {
                        $get = Polytechnic::find($college);
                        $departments = collect([]);
                        if ($get)
                            $departments = $get->departments()
                                ->where('id', $department)
                                ->distinct('department_name')
                                ->get(['departments.id', 'department_name']);
                    } else
                        $departments = CollegeDepartment::where('college_id', $college)
                            ->where('id', $department)
                            ->get();
                }
            }

            return view('college.lecturers.assign', compact('lecturers', 'options', 'departments'));
        }
    }

    public function getAssignedCourse($lecturer)
    {
        $college = college('college_id');
        if (lecturer(null, 'college_id'))
            $college = lecturer(null, 'college_id');

        $lecturer = Lecturer::where('college_id', $college)
            ->where('id', $lecturer)->first();
        if ($lecturer) {
            $courses = $lecturer->courses;
            return view('college.lecturers.partials.my_course', compact('courses'));
        }
        return "Oops, something is wrong !!!";
    }

}
