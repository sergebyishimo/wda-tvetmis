<?php /** @noinspection PhpDocSignatureInspection */

namespace App\Http\Controllers\CollegeAuth;

use App\College;
use App\Model\Rp\AdmissionPrivate;
use App\Rp\Polytechnic;
use App\Rp\Student;
use App\Rp\Ubudehe;
use App\Rp\CollegeStudent;
use App\Rp\AdmittedStudent;
use Illuminate\Http\Request;
use App\Rp\SponsorshipStatus;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\CollegeProgram;
use Yajra\Datatables\Datatables;
use App\CollegeContinuingStudent;
use App\Http\Controllers\Controller;
use Rap2hpoutre\FastExcel\FastExcel;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use App\Transformers\ListAllContinuigStudentsTransformer;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Rp\CollegeDepartment;
use App\Rp\CollegeOption;

class RegistrationController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $colleges = Polytechnic::all();

        $sponsors = new CollegeStudent();

        if (auth()->guard('college')->check())
            $sponsors = $sponsors->where('college_id', college('college_id'));

        $sponsors = $sponsors->distinct('sponsorship_status')->get(['sponsorship_status']);

        if($request->year || $request->department_id || $request->gender || $request->sponsor) {
            if (auth()->guard('college')->check()) {
                $students = CollegeStudent::where('college_id', college('college_id'));

                 if ($request->option_id != null) {
                     $students = $students->where('course_id', $request->option_id);
                 }

                if ($request->year != null && $request->year != 4) {
                    $students = $students->where('year_of_study', $request->year);
                }

                if ($request->start != null && $request->end != null) {
                    $start = date('Y-m-d', strtotime($request->start));
                    $end = date('Y-m-d', strtotime($request->end));
                    $students = $students->whereBetween('updated_at', [$start, $end]);
                }

                if ($request->gender) {
                    $students = $students->where('gender', $request->gender);
                }

                if ($request->sponsor) {
                    $students = $students->where('sponsorship_status', $request->sponsor);
                }

                $students = $students->get();
            }

            else {

                $students = new CollegeStudent();

                if ($request->option_id != null) {
                    $students = $students->where('course_id', $request->option_id);
                }

                if ($request->year != null && $request->year != 4) {
                    $students = $students->where('year_of_study', $request->year);
                }

                if ($request->college != null) {
                    $students = $students->where('college_id', $request->college);
                }

                if ($request->start != null && $request->end != null) {
                    $start = date('Y-m-d', strtotime($request->start));
                    $end = date('Y-m-d', strtotime($request->end));
                    $students = $students->whereBetween('updated_at', [$start, $end]);
                }


                if ($request->gender != null) {
                    $students = $students->where('gender', $request->gender);
                }


                if ($request->sponsor) {
                    $students = $students->where('sponsorship_status', $request->sponsor);
                }

                $students = $students->get();

            }


            return view('college.registration.index', compact( 'colleges','students', 'sponsors'));
        }

        //if (auth()->guard('college')->check())
            //$students = college('myStudents');
        //elseif (auth()->guard('rp')->check())
            //$students = AdmittedStudent::where('year_of_study', date('Y'))->paginate(20);

        return view('college.registration.index', compact('colleges', 'sponsors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $college = null;
        if (auth()->guard('college')->check())
            $college = college('college_id');
        elseif (auth()->guard('rp')->check()) {
            if (!rpAllowed(2))
                return back()->with(['status' => '3', 'message' => 'Don\'t have permission']);

            $std = AdmittedStudent::where('std_id', $id)->first();
            if ($std)
                $college = $std->college_id;
            else {
                $std = AdmissionPrivate::where('std_id', $id)->first();
                if ($std)
                    $college = $std->college_id;
            }
        }
        if (applied($id))
            $student = CollegeStudent::where('college_id', $college)
                ->where('student_reg', $id);
        else
            $student = AdmittedStudent::where('college_id', $college)
                ->where('std_id', $id);
        if ($student->count() <= 0) {
            $student = AdmissionPrivate::where('college_id', $college)
                ->where('std_id', $id);
            if ($student->count() <= 0)
                return abort(404);
        }

        $student = $student->first();

        return view('college.registration.show', compact('student'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = AdmittedStudent::where('std_id', $id)->first();
        $departments = RpDepartment::all();
        $ubudehes = Ubudehe::all();
        return view('college.registration.edit', compact('student', 'departments', 'ubudehes'));
    }

    public function editSave(Request $request)
    {

        $student = AdmittedStudent::where('std_id', $request->student_id)->first();

        if ($student) {
            $student->first_name = $request->first_name;
            $student->other_names = $request->other_names;
            $student->email = $request->email;
            $student->phone = $request->phone;
            $student->department_id = $request->department_id;
            $student->course_id = $request->option_id;
            $student->gender = $request->gender;
            $student->ubudehe = $request->ubudehe;
            $student->dob = date('Y-m-d', strtotime($request->dob));
            $student->aggregates_obtained = $request->aggregate_obtained;
            $student->national_id_number = $request->national_id_number;
            $student->parents_phone = $request->parents_phone;
            $student->index_number = $request->index_number;
            $student->year_of_study = $request->year_of_study;

            if ($request->sponsor != null)
                $student->sponsorship_status = $request->sponsor;

            if($student->save()) {

                $student = CollegeStudent::where('student_reg', $student->std_id)->first();
                if ($student) {
                    $student->first_name = $request->first_name;
                    $student->other_names = $request->other_names;
                    $student->gender = $request->gender;
                    $student->email = $request->email;
                    $student->phone = $request->phone;
                    $student->department_id = $request->department_id;
                    $student->course_id = $request->option_id;
                    $student->ubudehe = $request->ubudehe;
                    $student->dob = date('Y-m-d', strtotime($request->dob));
                    $student->national_id_number = $request->national_id_number;
                    $student->aggregates_obtained = $request->aggregates_obtained;
                    $student->year_of_study = $request->year_of_study;

                    if ($request->sponsor != null)
                        $student->sponsorship_status = $request->sponsor;

                    $student->save();
                }
            }
        }

        $private = AdmissionPrivate::where('std_id', $request->student_id)->first();
        if ($private) {
            $private->first_name = $request->first_name;
            $private->other_names = $request->other_names;
            $private->email = $request->email;
            $private->phone = $request->phone;
            $private->department_id = $request->department_id;
            $private->course_id = $request->option_id;
            $private->gender = $request->gender;
            $private->ubudehe = $request->ubudehe;
            $private->dob = date('Y-m-d', strtotime($request->dob));
            $private->aggregates_obtained = $request->aggregate_obtained;
            $private->national_id_number = $request->national_id_number;
            $private->parents_phone = $request->parents_phone;
            $private->index_number = $request->index_number;
            $private->year_of_study = $request->year_of_study;

            $private->save();
        }

        $student_check = Student::where('email', $request->email)->first();

        if(!$student_check) {
            $student = Student::where('id', $request->student_id)->first();
            if($student) {
                $student->email = $request->email;
                $student->save();
            }
            
        }
        
        if(auth()->guard('college')->check()) {
            return redirect()->route('college.registration.index', ['show' => true])
                ->with(['status' => '1', 'message' => 'Saved Successfully.']);
        } else {
            return redirect()->route('rp.registration.index', ['show' => true])
                ->with(['status' => '1', 'message' => 'Saved Successfully.']);
        }
        

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = CollegeContinuingStudent::where('id', $id)->first();
        if (!$student)
            return abort(419);
        $check = AdmittedStudent::where('std_id', $student->student_reg)->count();
        if ($check == 0) {
            $student->delete();
            return redirect()->back()->with(['status' => '1', 'message' => 'Student Deleted Successfully']);
        } else {
            $student->delete();
            return redirect()->back()->with(['status' => '2', 'message' => 'Student Already Admitted!!']);
        }


    }


    public function registerStoreStudents(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:students',
            'std_id' => 'required',
            'password' => 'required|min:6',
            'department' => 'required',
            'course' => 'required'
        ]);

        $data = [
            'id' => $request->input('std_id'),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ];
        $add = Student::create($data);
        if ($add) {
            $data['admission'] = false;
            $data['std_id'] = $data['id'];
            $data['first_name'] = getNamesForName($data['name'])[0];
            $data['other_names'] = getNamesForName($data['name'])[1];
            $data['department_id'] = $request->input('department');
            $data['course_id'] = $request->input('course');
            $data['college_id'] = college('college_id');
            $data['year_of_study'] = date('Y');
            $data['academic_year'] = getCurrentAcademicYear();
            AdmittedStudent::create($data);
        }

        return back()->with(['success' => 'Successfully added.']);
    }

    public function finished()
    {
        $year = date('Y');

        if (auth()->guard('college')->check())
            $students = college('myStudents');
        else
            $students = CollegeStudent::whereYear('created_at', '=', $year)->get();

        return view('college.registration.finished', compact('students'));
    }

    public function notFinished(Request $request)
    {
        $students = [];

        if ($request->college || $request->year || $request->option_id) {
            $finished = new CollegeStudent();
            $students = new AdmittedStudent();

            if ($request->year == 4) {
                $finished = $finished->whereIn('year_of_study', [1,2,3,2018,17,16]);
                $students = $students->whereIn('year_of_study', [1,2,3,2018,17,16]);
            }
            else {
                if ($request->year == 1) {
                    $finished = $finished->whereIn('year_of_study', [1, 2018]);
                    $students = $students->whereIn('year_of_study', [1, 2018]);
                }

                if ($request->year == 2) {
                    $finished = $finished->whereIn('year_of_study', [2, 17]);
                    $students = $students->whereIn('year_of_study', [2, 17]);
                }

                if ($request->year == 3) {
                    $finished = $finished->whereIn('year_of_study', [3, 16]);
                    $students = $students->whereIn('year_of_study', [3, 16]);
                }
            }

            if ($request->college != null) {
                if(auth()->guard('rp')->check()) {
                    $finished = $finished->where('college_id', $request->college);
                    $students = $students->where('college_id', $request->college);
                } else {
                    $finished = $finished->where('college_id', college('college_id'));
                    $students = $students->where('college_id', college('college_id'));
                }

            }

            if ($request->option_id) {
                $finished = $finished->where('course_id', $request->option_id);
                $students = $students->where('course_id', $request->option_id);
            }

            $finished = $finished->get();

            if ($finished)
                $finished = $finished->pluck('student_reg');

            $students = $students->whereNotIn('std_id', $finished)
                ->orderBy('created_at', 'desc')
                ->get();
        }

        $colleges = Polytechnic::all();

        return view('college.registration.not-finished', compact('students', 'colleges'));
    }

    public function registerStudents()
    {
        return view("college.registration.regsiter");
    }

    public function uploadContinuingStudents(Request $request)
    {

        if ($request->attachment) {

            $fileTitle = $request->attachment->getClientOriginalNAme();
            $filename = $request->attachment->storeAs('uploads/students', $fileTitle . ' ' . date('Ymd H:i') . '.xlsx');
            $inputFileName = storage_path('app/') . $filename;
            $objPHPExcel = IOFactory::load($inputFileName);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= 500; $row++) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                if ($rowData[0][0] == null) {
                    continue;
                }

                $gen = continuingGenerateReg(0, $request->year == 1 ? 2 : $request->year , '');
                $data = [
                    'registration_number' => preg_replace('/\s+/', '', trim($rowData[0][0])),
                    'college_id' => college('college')->id,
                    'option_id' => $request->input('option_id'),
                    'sponsor_id' => $rowData[0][3],
                    'first_name' => $rowData[0][1],
                    'last_name' => $rowData[0][2],
                    'student_reg' => $gen,
                    'year_of_study' => $request->year,
                    'academic_year' => $request->academic_year,
                    'repeat' => (isset($rowData[0][4]) && $rowData[0][4] == true) ? 1 : 0
                ];

                $check = CollegeContinuingStudent::where('college_id', college('college')->id)->where('registration_number', $rowData[0][0])->first();

                if ($check) {
                    unset($data['student_reg']);
                    CollegeContinuingStudent::where('id', $check->id)->update($data);
                } else {
                    CollegeContinuingStudent::create($data);
                }
            }

            return redirect()->route('college.reg.uploads.continuing.student', ['show' => true])
                ->with(['status' => '1', 'message' => 'Uploading done successfully.']);
//            dd($collection);

        } else if($request->reg_number) {
            
            if($this->saveStudent($request) == true) {
                return redirect()->back()->with(['status' => 1, 'message' => 'Student Saved Successfully']);
            } else {
                return redirect()->back()->with(['status' => 2, 'error' => 'Student Not Saved! Check the form']);
            }
        } else {
            $show = false;
            $programs = college('college')->programs;
            $students = CollegeContinuingStudent::where('college_id', college('id'))->paginate(20);
            return view('college.registration.uploadcontinuing', compact('students', 'programs', 'show'));
        }
    }

    public function uploaded(Request $request)
    {
        $programs = CollegeProgram::where('college_id', college('college_id'))->get();
        if (isset($request->option_id)) {

            $students = CollegeContinuingStudent::where('college_id', college('college_id'))->where('option_id', $request->option_id)->where('academic_year', $request->academic_year);

            if ($request->sponsor != 0) {
                $students->where('sponsor_id', $request->sponsor);
            }

            if ($request->year != 0) {
                $students->where('year_of_study', $request->year);
            }

            if ($request->repeat != 0) {
                $students->where('repeat', $request->repeat);
            }

            $students = $students->get();

            $departments = CollegeDepartment::where('college_id', college('college_id'))->where('program_id', $request->program_id)->get();
            $options = CollegeOption::where('college_id', college('college_id'))->where('department_id', $request->department_id)->get();

            return view('college.registration.uploaded', compact('programs', 'students', 'departments', 'options'));
        }
        return view('college.registration.uploaded', compact('programs'));
    }

    public function data(Datatables $datatables, Request $request)
    {
        // $students = CollegeContinuingStudent::where('college_id', college('id'));

        $jmodel = CollegeContinuingStudent::where('college_id', college('college_id'));

        $dd = $datatables->eloquent($jmodel)
            ->setTransformer(new ListAllContinuigStudentsTransformer())
            ->make(true);
//                dd($dd);
        return $dd;

    }

    public function saveStudent($request) {
        
        if($request->first_name && $request->last_name && $request->sponsor) {
            
            
            $data = [
                'registration_number' => preg_replace('/\s+/', '', trim($request->reg_number)),
                'college_id' => college('college')->id,
                'option_id' => $request->input('option_id'),
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'year_of_study' => $request->year,
                'academic_year' => $request->academic_year,
                'repeat' => (isset($request->repeat) && $request->repeat == true) ? 1 : 0
            ];

            if ($request->sponsor != null)
                $data['sponsor_id'] = $request->sponsor;

            if($request->cont_id) {
                $check = CollegeContinuingStudent::find($cont_id);
            } else {
                $check = CollegeContinuingStudent::where('college_id', college('college_id'))->where('registration_number', $request->reg_number)->first();
            }
            
            
            if ($check) {
                CollegeContinuingStudent::where('id', $check->id)->update($data);
            } else {
                $gen = continuingGenerateReg(0, $request->year, '');
                $data['student_reg'] = $gen;
                CollegeContinuingStudent::create($data);
            }
            
            return true;
        } else {
            return false;
        }
    }

    public function editContinuing($cont_id = null, Request $request) {

        if($request->reg_number) {
            $student = CollegeContinuingStudent::find($request->cont_id);
            if($student) {
                $student->registration_number = $request->reg_number;
                $student->first_name = $request->first_name;
                $student->last_name = $request->last_name;
                $student->option_id = $request->option_id;
                $student->sponsor_id = $request->sponsor;
                $student->save();
            }
            
            $admitted = AdmittedStudent::where('std_id', $student->student_reg)->first();
            if($admitted) {
                $admitted->first_name = $request->first_name;
                $admitted->other_names = $request->last_name;
                $student->department_id = $request->department_id;
                $student->course_id = $request->option_id;
                $admitted->save();
            }

            $registered = CollegeStudent::where('student_reg', $student->student_reg)->first();
            if($registered) {
                $registered->first_name = $request->first_name;
                $registered->other_names = $request->last_name;
                $student->department_id = $request->department_id;
                $student->course_id = $request->option_id;
                $registered->save();
            }

            return redirect()->back()->with(['status' => 1, 'message' => 'Student Information Saved']);
        }

        $student = CollegeContinuingStudent::find($cont_id);
        if($student) {
            return view('college.registration.editContinuing', compact('student'));
        }
        
    }

    public function references(Request $request) {
        $fileTitle = $request->uploadedFile->getClientOriginalNAme();
        $filename = $request->uploadedFile->storeAs('uploads/students', $fileTitle . ' ' . date('Ymd H:i') . '.xlsx');
        $inputFileName = storage_path('app/') . $filename;
        $objPHPExcel = IOFactory::load($inputFileName);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if ($rowData[0][0] == null) {
                continue;
            }

            $old_reg = $rowData[0][0];

            // check if the reg exists

            $student = CollegeContinuingStudent::where('registration_number', $old_reg)->first();

            if ($student) {
                $student->reference_number = $rowData[0][3];
                $student->save();
            }
        }

        return redirect()->back()->with(['status' => 1, 'message' => 'Reference Numbers Uploaded Successfully']);
    }
}
