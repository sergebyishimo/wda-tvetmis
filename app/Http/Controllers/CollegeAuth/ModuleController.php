<?php

namespace App\Http\Controllers\CollegeAuth;

use App\Rp\CollegeModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rp\CollegeOption;
use App\Rp\CollegeDepartment;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $modules = CollegeModule::where('college_id', college('college')->id)->get();

        if(isset($request->option_id)) {
            $modules = CollegeModule::where('college_id', college('college_id'))->where('option_id', $request->option_id)->where('year_of_study', $request->year)->where('academic_year', $request->academic_year)->where('semester', $request->semester)->get();
            $departments = CollegeDepartment::where('college_id', college('college')->id)->get();
            $options = CollegeOption::where('college_id', college('college')->id)->where('department_id', $request->department_id)->get();
            // dd('Semester : ' . $request->semester . );
            return view('college.modules.index',compact('modules', 'departments', 'options'));
        }

        return view('college.modules.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $module = null;
        return view('college.modules.create', compact('module'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = null;

        for ($i=0; $i < count($request->module_code); $i++) { 
            $dept = new CollegeModule();
            $dept->college_id = college('college')->id;
            $dept->option_id = $request->option_id;
            $dept->year_of_study = $request->year;
            $dept->semester = $request->semester[$i];
            $dept->module_code = $request->module_code[$i];
            $dept->module_name = $request->module_name[$i];
            $dept->credits = $request->credits[$i];
            $dept->academic_year = $request->academic_year[$i];
            $save = $dept->save();
        }
        
        if ($save)
            return redirect(route('college.modules.index'))->with(['status' => '1', 'message' => "Module Added Successfully."]);
        else
            return redirect(route('college.modules.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Module."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rp\CollegeModule  $collegeModule
     * @return \Illuminate\Http\Response
     */
    public function show(CollegeModule $collegeModule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rp\CollegeModule  $collegeModule
     * @return \Illuminate\Http\Response
     */
    public function edit(CollegeModule $collegeModule,$id)
    {
        $module = CollegeModule::find($id);
        $options = CollegeOption::where('college_id', college('college')->id)->get();
        return view('college.modules.create', compact('options', 'module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rp\CollegeModule  $collegeModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CollegeModule $collegeModule,$id)
    {
        $department = null;
        $dept = CollegeModule::find($id);
        $dept->college_id = college('college')->id;
        $dept->option_id = $request->option_id;
        $dept->year_of_study = $request->year;
        $dept->semester = $request->semester[0];
        $dept->module_code = $request->module_code[0];
        $dept->module_name = $request->module_name[0];
        $dept->credits = $request->credits[0];
        $dept->academic_year = $request->academic_year[0];
        $save = $dept->save();
        if ($save)
            return redirect(route('college.modules.index'))->with(['status' => '1', 'message' => "Module Added Successfully."]);
        else
            return redirect(route('college.modules.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds a Module."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rp\CollegeModule  $collegeModule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = CollegeModule::find($id);
        if ($department) {
            $department->delete();
            return redirect(route('college.modules.index'))->with(['status' => '1', 'message' => 'Department Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
