<?php

namespace App\Http\Controllers\CollegeAuth;

use App\FeeCategory;
use App\Model\Rp\AdmissionPrivate;
use App\Model\Rp\FeesOverride;
use App\PaymentOutside;
use App\Rp\AdmittedStudent;
use App\Rp\CollegePaymentDetail;
use App\Http\Requests\AddCustomCategoryRequest;
use App\Rp\CollegeStudent;
use App\Rp\PaymentCategory;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Rp\CollegePayment;
use Illuminate\Support\Facades\Validator;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $globalCategories = PaymentCategory::whereNull('college_id')->get();
        $collegeCategories = college()->paymentCategory;
        return view('college.payments.index', compact('globalCategories', 'collegeCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function outside(Request $request, $student = null)
    {
        if ($request->has('delete') && !$request->has('attachment')) {
            $regNo = $request->input('student_id');
            $id = $request->input('delete');
            $payment = PaymentOutside::where('id', $id)->where('student_id', $regNo)->first();
            if ($payment) {
                $payment->delete();
                return back()->with(['status' => '1', 'message' => 'Deleted Successfully']);
            } else
                return back()->with(['status' => '0', 'message' => 'Oops Failed ...']);
        }
        if (!$request->has('delete') && $request->has('attachment')) {
            $validator = Validator::make($request->all(), [
                'student_id' => 'required',
                'payment_category' => 'required',
                'attachment' => 'required|mimes:pdf',
                'academic_year' => 'required',
                'amount' => 'required',
                'date_paid' => 'required',
                'bank' => 'required',
                'payment_type' => 'required'
            ]);

            if ($validator->fails())
                return back()->withInput()->with(['status' => '0', 'message' => $validator->errors()->all()]);

            $data = $request->except(['_token']);
            $regNo = $request->input('student_id');
            if ($request->hasFile('attachment')) {
                $path = 'students/payments/' . date('F,Y') . "/outside";
                $file = $request->file('attachment');
                $ext = $file->getClientOriginalExtension();
                $attachment = $file->storeAs($path, $regNo . '.' . $ext, config('voyager.storage.disk'));
                $data['attachment'] = $attachment;
            }
            $college = getStudentInfo($regNo, 'college_id', true);
            if (strlen($college) >= 1 && is_integer($college)) {
                $data['college_id'] = $college;
                if (!paidOutSide($regNo, null, $data['payment_category'])) {
                    $save = PaymentOutside::create($data);
                    if ($save)
                        return back()->with(['status' => '1', 'message' => 'Student Assigned Successfully.']);
                    else
                        return back()->with(['status' => '0', 'message' => 'Oops. Saving failed, try again later ...']);
                } else
                    return back()->with(['status' => '0', 'message' => 'Student already assigned ...']);
            }
            return back()->with(['status' => '1', 'message' => 'Oops, Something wrong happened, call system administrator ... ']);
        }

        $aStudents = AdmittedStudent::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $aStudents->where('college_id', college('college_id'));
        $aStudents = $aStudents->get();
        //
        $pStudents = AdmissionPrivate::where('academic_year', getCurrentAcademicYear());
        if (auth()->guard('college')->check())
            $pStudents->where('college_id', college('college_id'));
        $pStudents = $pStudents->get();
        $students = [];
        foreach ($aStudents as $aStudent) {
            if (!applied($aStudent->std_id) && !isSponsored($aStudent->std_id))
                $students[$aStudent->std_id] = $aStudent->std_id . " - " . $aStudent->names;
        }
        foreach ($pStudents as $pStudent) {
            if (!isset($students[$pStudent->std_id]) && !isSponsored($pStudent->std_id))
                $students[$pStudent->std_id] = $pStudent->std_id . " - " . $pStudent->names;
        }

        $payments = PaymentOutside::orderBy('created_at', 'DESC');
        if (auth()->guard('college')->check())
            $payments->where('college_id', college('college_id'));
        $payments = $payments->get();

        $categories = FeeCategory::all();
        if ($categories)
            $categories = $categories->pluck('category_name', 'id');
        else
            $categories = [];
        return view('college.payments.outside', compact('students', 'payments', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $college = college('id');
        $insert = [];
        if (is_array($request->input('amount'))) {
            $amounts = $request->input('amount');
            $active = $request->input('active');

            foreach ($amounts as $category => $amount) {
                $details = CollegePaymentDetail::where('college_id', $college)
                    ->where('category_id', $category)
                    ->where('academic_year', date('Y'));
                if ($amounts > 0) {
                    if ($details->count() > 0) {
                        $details = $details->first();
                        $details->amount = $amount;
                        $details->active = isset($active[$category]) ? $active[$category] : false;
                        $details->save();
                    } else {
                        $insert[] = [
                            'college_id' => $college,
                            'category_id' => $category,
                            'amount' => $amount,
                            'active' => isset($active[$category]) ? $active[$category] : false,
                            'academic_year' => date('Y')
                        ];
                    }
                }
            }
            if (count($insert) > 0) {
                CollegePaymentDetail::insert($insert);
            }
        }

        return back()->with(['status' => '1', 'message' => 'Updated Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function finished()
    {
        if (auth()->guard('rp')) {
            $payments = CollegePayment::orderBy('op_date', 'desc')->paginate(30);
        } else {
            $payments = CollegePayment::where('college_id', college('id'))->orderBy('op_date', 'desc')->paginate(30);
        }

        return view('college.payments.finished', compact('payments'));
    }

    public function history()
    {
        return view('college.payments.history');
    }

    public function category()
    {
        $id = college('id');
        $categories = PaymentCategory::where('college_id', $id)->get();

        return view('college.payments.category', compact('categories'));
    }

    public function storeCategory(AddCustomCategoryRequest $request)
    {
        $pay = new PaymentCategory();

        $pay->college_id = college('id');
        $pay->name = $request->name;
        $pay->save();

        return back()->with(['status' => 1, 'message' => 'Category Added Successfully']);
    }

    public function categoryDo($id, $action)
    {
        $category = PaymentCategory::findOrFail($id);

        if ($category->college_id != college('id'))
            return abort(404);
        switch ($action) {
            case "DELETE":
                $category->delete();
                break;
            case "DISABLE":
                $category->active = false;
                $category->save();
                break;
            case "ENABLE":
                $category->active = true;
                $category->save();
                break;
            default:
                break;
        }

        return redirect()->route('college.payments.category');
    }

    public function override()
    {
        $cid = college('college_id');
        $categories = FeeCategory::all();
        $students = CollegeStudent::where('college_id', $cid);

        if ($students->count() > 0)
            $students = $students->get()->pluck('names', 'student_reg');
        else
            $students = [];

        if ($categories)
            $categories = $categories->pluck('category_name', 'id');
        else
            $categories = [];
        $installments = FeesOverride::where('college_id', $cid)->orderBy('created_at', "DESC")->get();
        if ($installments->count() <= 0)
            $installments = [];

        return view('college.override.index', compact('categories', 'students', 'installments'));
    }

    public function overrideStore(Request $request)
    {
        $data = $request->except('_token', 'student_id');
        $data['college_id'] = college('college_id');
        if ($request->has('student_id')) {
            $students = $request->input('student_id');
            if (is_array($students)) {
                foreach ($students as $student) {
                    $data['student_id'] = $student;
                    $check = FeesOverride::where('installment_due_date', $data['installment_due_date'])
                        ->where('fee_category', $data['fee_category'])
                        ->where('installment_amount', $data['installment_amount'])
                        ->where('academic_year', $data['academic_year'])
                        ->where('student_id', $student);
                    if ($check->count() <= 0)
                        FeesOverride::create($data);
                }
            }
        } else {
            $check = FeesOverride::where('installment_due_date', $data['installment_due_date'])
                ->where('fee_category', $data['fee_category'])
                ->where('installment_amount', $data['installment_amount'])
                ->where('academic_year', $data['academic_year']);

            if ($check->count() <= 0)
                FeesOverride::create($data);
            else
                return back()->with(['status' => '0', 'message' => 'Fail, already saved !!!']);
        }

        return back()->with(['status' => '1', 'message' => 'Successfully Saved']);
    }

    public function overrideDestroy(Request $request, $id)
    {
        $plan = FeesOverride::findOrFail($id);
        $plan->delete();

        return back()->with(['status' => '1', 'message' => 'Deleted Successfully.']);
    }
}
