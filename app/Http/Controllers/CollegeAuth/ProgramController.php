<?php

namespace App\Http\Controllers\CollegeAuth;

use App\Rp\CollegeProgram;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rp\CollegeDepartment;
use App\Rtqf;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('o_o')) {
            $program = CollegeProgram::where('id', $request->input('o_o'))->first();
            $details = true;
            return view('college.program.index', compact('program', 'details'));
        }
        $program = null;
        $programs = CollegeProgram::where('college_id', college('id'))->get();
        $departments = CollegeDepartment::where('college_id', college('id'))->get();
        $rtqf_level = Rtqf::all();
        return view('college.program.index', compact('program', 'programs', 'rtqf_level', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $program = null;
        $programs = CollegeProgram::where('college_id', college('id'))->get();
        $departments = CollegeDepartment::where('college_id', college('id'))->get();
        $rtqf_level = Rtqf::all();
        return view('college.program.create', compact('program', 'programs', 'rtqf_level', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $program = new CollegeProgram();
        $program->college_id = college('id');
        $program->department_id = $request->department_id;
        $program->program_name = $request->program_name;
        $program->program_code = $request->program_code;
        $program->program_load = $request->program_load;
        $program->rtqf_level = $request->rtqf_level;
        $program->program_is_stem = $request->program_is_stem;
        $program->program_status = $request->program_status;
        $program->tuition_fees = $request->tuition_fees;
        $program->other_course_fees = $request->other_course_fees;
        $program->admission_requirements = $request->admission_requirements;
        $save = $program->save();
        if ($save) {
            return redirect(route('college.programs.index'))->with(['status' => '1', 'message' => "Program  Added Successfully."]);
        } else {
            return redirect(route('college.programs.index'))->with(['status' => '1', 'message' => "Failed to   Adds Program "]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rp\CollegeProgram  $collegeProgram
     * @return \Illuminate\Http\Response
     */
    public function show(CollegeProgram $collegeProgram)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rp\CollegeProgram  $collegeProgram
     * @return \Illuminate\Http\Response
     */
    public function edit(CollegeProgram $collegeProgram)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rp\CollegeProgram  $collegeProgram
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CollegeProgram $collegeProgram)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rp\CollegeProgram  $collegeProgram
     * @return \Illuminate\Http\Response
     */
    public function destroy(CollegeProgram $collegeProgram)
    {
        //
    }
}
