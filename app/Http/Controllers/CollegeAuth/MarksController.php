<?php

namespace App\Http\Controllers\CollegeAuth;

use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use App\Rp\CollegeStudent;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style;


use App\CollegeContinuingStudent;
use App\Http\Requests\CollegeStudentsMarksUploadRequest;
use App\Model\Rp\StudentMarks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Rp\CollegeModule;
use App\Rp\CollegeOption;
use App\FailedModule;
use App\Rp\CollegeDepartment;


class MarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $college = college('college');
        $departments = [];
        if ($college)
            $departments = $college->departments()->distinct('id')->get();
        if ($departments->count() > 0) {
            $departmentss = [];
            foreach ($departments as $department) {
                $departmentss[$department->id] = $department->department_name;
            }
            $departments = $departmentss;
        }


        if (isset($request->option_id)) {

            $modules = CollegeModule::where('option_id', $request->option_id)->where('year_of_study', $request->year)->pluck('id')->toArray();

            $one = StudentMarks::where('college_id', college('college')->id)->where('academic_year', $request->academic_year)->where('semester', 1)->where('retake', null)->whereIn('module_id', $modules)->distinct('module_id')->get(['module_id']);
            $two = StudentMarks::where('college_id', college('college')->id)->where('academic_year', $request->academic_year)->where('semester', 2)->where('retake', null)->whereIn('module_id', $modules)->distinct('module_id')->get(['module_id']);
            $students = StudentMarks::where('college_id', college('college')->id)->where('academic_year', $request->academic_year)->where('retake', null)->whereIn('module_id', $modules)->distinct('registration_number')->paginate(40, ['registration_number']);

            $students->withPath("/college/marks?program_id=$request->program_id&department_id=$request->department_id&option_id=$request->option_id&year=$request->year&academic_year=$request->academic_year");

            $departments = CollegeDepartment::where('college_id', college('college_id'))->where('program_id', $request->program_id)->get();
            $options = CollegeOption::where('college_id', college('college_id'))->where('department_id', $request->department_id)->get();

            return view('college.students.marks.index', compact('departments', 'options', 'one', 'two', 'students'));
        }


        return view('college.students.marks.index');
    }

    /**
     * Upload a listing of the marks.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function upload()
    {
        $college = college('college');
        $departments = [];
        if ($college)
            $departments = $college->departments()->distinct('id')->get();
        if ($departments->count() > 0) {
            $departmentss = [];
            foreach ($departments as $department) {
                $departmentss[$department->id] = $department->department_name;
            }
            $departments = $departmentss;
        }
        return view('college.students.marks.upload', compact('departments'));
    }

    public function otherUpload()
    {
        $college = college('college');
        $departments = [];
        if ($college)
            $departments = $college->departments()->distinct('id')->get();
        if ($departments->count() > 0) {
            $departmentss = [];
            foreach ($departments as $department) {
                $departmentss[$department->id] = $department->department_name;
            }
            $departments = $departmentss;
        }
        return view('college.students.marks.upload_other', compact('departments'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function storeUpload(Request $request)
    {
        if ($request->download == "Download Template") {
            $this->download($request);
            exit;
        }

        if (!$request->hasFile('file')) {
            return back()->with(['status' => '0', 'message' => "You have to choose the file"]);
        }

        $fileTitle = $request->file->getClientOriginalNAme();
        $filename = $request->file->storeAs('uploads/marks', $fileTitle . ' ' . date('Ymd H:i') . '.xlsx');
        $inputFileName = storage_path('app/') . $filename;
        $objPHPExcel = IOFactory::load($inputFileName);

        $sheet_upload_nber = [];
        $sheet_upload_nber_update = [];

        $modules_errors = [];
        $skippedRegs = [];

        for ($id = 0; $id < $objPHPExcel->getSheetCount(); $id++) {

            $sheet = $objPHPExcel->getSheet($id);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $sheet_upload_nber[$id] = 0;
            $sheet_upload_nber_update[$id] = 0;

            if ($id <= 1) {
                $semester = 1;
            } else {
                $semester = 2;
            }

            if ($sheet->getTitle() == 'Worksheet') {
                continue;
            }

            $modules_arr = [];

            $rowData = $sheet->rangeToArray('C' . 8 . ':' . $highestColumn . 8, NULL, TRUE, FALSE);

            foreach ($rowData[0] as $row) {
                if ($row != null) {
                    $row = preg_replace('/\s+/', '', trim($row));
                    $modules_arr[] = $row;

                    $module_check = CollegeModule::where('college_id', college('college')->id)->where('module_code', $row)->where('semester', $semester)->where('academic_year', $request->academic_year)->where('option_id', $request->option_id)->where('year_of_study', $request->year)->count();

                    if ($module_check == null) {
                        $modules_errors[] = $row . ' in Option : ' . CollegeOption::find($request->option_id)->option_name . ' in Semester : ' . $semester . ' in Academic Year : ' . $request->academic_year . ' in Year of Study : ' . $request->year;
                    }
                }
            }

            for ($row = 12; $row <= 200; $row++) {
                //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $std_reg_no = trim($rowData[0][0]);
                $std_reg_no = preg_replace('/\s+/', '', $std_reg_no);

                if ($std_reg_no == null) {
                    continue;
                }

                //check reg no

                $check_std = CollegeContinuingStudent::where('registration_number', $std_reg_no)->first();

                if (!$check_std) {
                    $skippedRegs[] = $std_reg_no;
                    continue;
                }

                $b = 1;
                for ($n = 0; $n < count($modules_arr); $n++) {
                    if ($rowData[0][$b] == null || filter_var($rowData[0][$b], FILTER_VALIDATE_FLOAT) === false) {
                        $b++;
                        continue;
                        // $marks = 0;
                    } else {
                        $marks = $rowData[0][$b];
                    }

                    $module_model = CollegeModule::where('college_id', college('college')->id)->where('module_code', $modules_arr[$n])->where('semester', $semester)->where('academic_year', $request->academic_year)->where('option_id', $request->option_id)->where('year_of_study', $request->year)->first();

                    if ($module_model) {
                        $data = [
                            'registration_number' => $check_std->student_reg,
                            'college_id' => college('college')->id,
                            'module_id' => $module_model->id,
                            'semester' => $semester,
                            'obtained_marks' => $marks,
                            'academic_year' => $request->academic_year,
                            'retake' => ($id == 1 || $id == 3) ? 1 : null,
                        ];

                        // check for update
                        $check_marks = StudentMarks::where('registration_number', $check_std->student_reg)->where('academic_year', $request->academic_year)->where('semester', $semester)->where('module_id', $module_model->id)->first();

                        // dd($check_marks);

                        if ($check_marks) {
                            StudentMarks::where('id', $check_marks->id)->update($data);
                            $sheet_upload_nber_update[($semester - 1)] += 1;
                        } else {
                            StudentMarks::create($data);
                            $sheet_upload_nber[($semester - 1)] += 1;

                            // check if module failed

                            if ($marks < 50) {
                                FailedModule::create(['college_id' => college('college_id'), 'student_id' => $check_std->student_reg, 'academic_year' => $request->academic_year, 'module_id' => $module_model->id]);
                            }
                        }


                    }


                    $b++;
                }


            }

        }


        // $lines = (new FastExcel)->import($filer->getRealPath());

        // if ($lines->count() > 501)
        //     return back()->with(['status' => '3', 'message' => 'Please, 500 students is the maximum to upload at once.']);
        // $error = [];
        // $uploaded = null;
        // foreach ($lines as $line) {
        //     $check = CollegeContinuingStudent::where('registration_number', $line['registration_number'])->first();
        //     if ($check) {
        //         $cc = StudentMarks::where('registration_number', $check->student_reg)
        //             ->where('program_id', $request->input('program'))
        //             ->where('module_id', $request->input('module'))
        //             ->where('academic_year', $request->input('academic_year'));
        //         if ($cc->count() <= 0) {
        //             StudentMarks::create([
        //                 'registration_number' => $check->student_reg,
        //                 'program_id' => $request->input('program'),
        //                 'module_id' => $request->input('module'),
        //                 'module_result' => $line['marks'],
        //                 'academic_year' => $request->input('academic_year')
        //             ]);
        //             $uploaded[] = ['registration_number' => $check->student_reg, 'marks' => $line['marks'], 'academic_year' => $request->input('academic_year')];
        //         } else
        //             $error[] = $line['registration_number'] . " already have marks for " . $request->input('academic_year');
        //     } else
        //         $error[] = $line['registration_number'] . " not in system !!";

        // }

        $msg = 'Uploaded Successfully! </br> New Marks Semester 1 : ' . $sheet_upload_nber[0] . ' </br> New Marks Semester 2 : ' . $sheet_upload_nber[1];

        if (count($skippedRegs) > 0) {
            $msg .= '<br><br> Skipped Students. These registration numbers are not in the system : ' . implode(', ', $skippedRegs);
        }

        if (count($modules_errors) > 0) {
            $msg .= '</br><br> Skipped Modules: ' . implode(', </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ', $modules_errors);
        }

        if ($sheet_upload_nber_update[0] > 0 || $sheet_upload_nber_update[1] > 0) {
            $msg .= '<br><br> Updated Marks Semester 1 :' . $sheet_upload_nber_update[0] . ' <br> Updated Marks Semester 2 : ' . $sheet_upload_nber_update[1];
        }

        return back()->with(['status' => '1', 'message' => $msg]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($request)
    {

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('TVET Information System')
            ->setLastModifiedBy('TVET Information System')
            ->setTitle('TVET Information System')
            ->setSubject('TVET Information System')
            ->setDescription('TVET Information System')
            ->setKeywords('TVET Information System')
            ->setCategory('TVET Information System');

        for ($w = 0; $w < 4; $w++) {

            if ($w <= 1) {
                $semester = 1;
            } else {
                $semester = 2;
            }

            if ($w == 1 || $w == 3) {
                $retake = ' Retake';
            } else {
                $retake = '';
            }

            $modules = CollegeModule::where('college_id', college('college')->id)
                ->where('option_id', $request->option_id)
                ->where('year_of_study', $request->year)
                ->where('academic_year', $request->academic_year)
                ->where('semester', $semester)->get();

            $p = 1;
            $tittle_to = 'C';
            foreach ($modules as $module) {
                if ($p > 1)
                    $tittle_to++;
                $p++;
            }

            $myWorkSheet = new Worksheet($spreadsheet, 'Semester ' . $semester . $retake);
            // Attach the “My Data” worksheet as the first worksheet in the PHPExcel object
            $spreadsheet->addSheet($myWorkSheet, $w);

            if ($request->year == '1') {
                $get = RpDepartment::find($request->department_id);
                $department_name = $get ? $get->department_name : "";
                $get = RpProgram::find($request->option_id);
                $option_name = $get ? $get->option_name : "";
            } else {
                $department_name = CollegeDepartment::find($request->department_id)->department_name;
                $option_name = CollegeOption::find($request->option_id)->option_name;
            }

            // Add some data
            $spreadsheet->setActiveSheetIndex($w)
                ->setCellValue('B2', 'Program')
                ->setCellValue('C2', $request->program_id == 1 ? 'Dipl.' : 'Adv. Dip.')
                ->setCellValue('B3', 'Department')
                ->setCellValue('C3', $department_name)
                ->setCellValue('B4', 'Option')
                ->setCellValue('C4', $option_name)
                ->setCellValue('B5', 'Academic Year')
                ->setCellValue('C5', $request->academic_year)
                ->mergeCells('C7:' . $tittle_to . '7')
                ->setCellValue('C7', 'Semester ' . $semester . $retake);

            $spreadsheet->getActiveSheet()->getStyle('C7')->getAlignment()->setHorizontal(Style\Alignment::HORIZONTAL_CENTER);

            $fontColorArray = array(
                'font' => array(
                    'color' => array('rgb' => 'ffffff'),
                )
            );

            $spreadsheet->getActiveSheet()->getStyle('C7')->getFont()->setBold(true);

            $styleArray = array(
                'borders' => array(
                    'outline' => array(
                        'borderStyle' => Style\Border::BORDER_THIN,
                        'color' => array('argb' => '000'),
                    ),
                ),
            );

            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            // $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $spreadsheet->getActiveSheet()->getStyle('C7:' . $tittle_to . '7')->applyFromArray($styleArray);

            $spreadsheet->getActiveSheet()->setCellValue('B8', 'Module Codes');
            $spreadsheet->getActiveSheet()->getStyle('B8:B8')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->setCellValue('B9', 'Max.');
            $spreadsheet->getActiveSheet()->getStyle('B9:B9')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->setCellValue('B10', 'Pass.');
            $spreadsheet->getActiveSheet()->getStyle('B10:B10')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->setCellValue('B11', '#Credits');
            $spreadsheet->getActiveSheet()->getStyle('B11:B11')->applyFromArray($styleArray);


            $al = 'C';
            foreach ($modules as $module) {
                $spreadsheet->getActiveSheet()->setCellValue($al . '8', $module->module_code);
                $spreadsheet->getActiveSheet()->getStyle($al . '8:' . $al . '8')->applyFromArray($styleArray);
                $spreadsheet->getActiveSheet()->getStyle($al . '8:' . $al . '8')->getAlignment()->setHorizontal(Style\Alignment::HORIZONTAL_RIGHT);

                $spreadsheet->getActiveSheet()->setCellValue($al . '9', '100');
                $spreadsheet->getActiveSheet()->getStyle($al . '9:' . $al . '9')->applyFromArray($styleArray);
                $spreadsheet->getActiveSheet()->getStyle($al . '9:' . $al . '9')->getAlignment()->setHorizontal(Style\Alignment::HORIZONTAL_RIGHT);

                $spreadsheet->getActiveSheet()->setCellValue($al . '10', '50');
                $spreadsheet->getActiveSheet()->getStyle($al . '10:' . $al . '10')->applyFromArray($styleArray);
                $spreadsheet->getActiveSheet()->getStyle($al . '10:' . $al . '10')->getAlignment()->setHorizontal(Style\Alignment::HORIZONTAL_RIGHT);

                $spreadsheet->getActiveSheet()->setCellValue($al . '11', $module->credits);
                $spreadsheet->getActiveSheet()->getStyle($al . '11:' . $al++ . '11')->applyFromArray($styleArray);
                $spreadsheet->getActiveSheet()->getStyle($al . '11:' . $al . '11')->getAlignment()->setHorizontal(Style\Alignment::HORIZONTAL_RIGHT);
            }
        }

        if ($request->year == '1')
            $option = RpProgram::find($request->option_id);
        else
            $option = CollegeOption::find($request->option_id);

        if ($option) {
            $name = college('college')->short_name . ' ' . $option->option_name . ' ' . $request->academic_year . ' Marks Upload';
        } else {
            $name = college('college')->short_name . ' Marks Upload';
        }


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $name . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function getTranscript()
    {
        return view('college.students.marks.transcript');
    }

    public function postTranscript(Request $request)
    {
        $college = college('college_id');
        if ($request->input('year') > 1)
            $yearOfStudy = (int)$request->input('year') - 1;
        else
            $yearOfStudy = $request->input('year');
        $academicYear = $request->input('academic_year');

        $getMarks = DB::table('rp_student_marks')
            ->join('college_modules', 'rp_student_marks.module_id', '=', 'college_modules.id')
            ->where('college_modules.option_id', '=', $request->input('option_id'))
            ->where('rp_student_marks.college_id', '=', $college)
            ->where('rp_student_marks.academic_year', 'LIKE', $request->input('academic_year'))
            ->whereNotNull('rp_student_marks.obtained_marks');

        $modules = CollegeModule::where('college_id', college('college_id'))
            ->where('option_id', $request->input('option_id'))
            ->where('year_of_study', $yearOfStudy)
            ->where('academic_year', $request->input('academic_year'));

        if ($request->input('students')) {
            $getMarks->where('rp_student_marks.registration_number', 'LIKE', $request->input('students'));
            $view = "college.students.marks.views.transcript_single_year";
            $students = CollegeStudent::where('student_reg', $request->input('students'))->first();
        } else {
            $students = CollegeStudent::where('college_id', $college)
                ->where('department_id', $request->input('department_id'))
                ->where('course_id', $request->input('option_id'))
                ->where('year_of_study', $request->input('year'))
                ->get();
        }

        if ($request->input('semester') && $request->input('semester') != 3) {
            $getMarks->where('rp_student_marks.semester', 'LIKE', $request->input('semester'));
            $modules->where('semester', 'LIKE', $request->input('semester'));
            $semester = $request->input('semester');
        }
        $modules = $modules->orderBy('semester', 'ASC')
            ->orderBy('module_name', 'ASC')->get();
        $getMarks = $getMarks
            ->select(['rp_student_marks.*'])
            ->get();

        return view('college.students.marks.transcript', compact('modules', 'getMarks', 'students', 'yearOfStudy', 'academicYear', 'view', 'semester'));
    }
}
