<?php

namespace App\Http\Controllers\CollegeAuth;

use App\FeeCategory;
use App\Http\Requests\SocialStudentRegisterRequest;
use App\Mail\RegisterMail;
use App\Mail\SendEmailSuccessfullySocialStudent;
use App\Model\Rp\AdmissionPrivate;
use App\Model\Rp\RpSocialStudent;
use App\Rp\AdmittedStudent;
use App\Rp\Polytechnic;
use App\Rp\Student;
use App\SourceSponsor;
use App\SponsorStudent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $college = college('college_id');
//        $ids = SponsorStudent::where('college_id', $college)->get();
//        if ($ids->count() > 0)
//            $ids = $ids->pluck('student_id')->toArray();
//        else
//            $ids = [];
//
//        $students = AdmittedStudent::where('college_id', $college)
//            ->whereIn("std_id", $ids)
//            ->get();
        if ($request->option_id) {
            $socials = SponsorStudent::join('admitted_students', 'admitted_students.std_id', 'sponsor_students.student_id')->where('admitted_students.course_id', $request->option_id)->where('admitted_students.year_of_study', $request->year)->where('sponsor_students.college_id', $college)->get();
        } else {
            $socials = SponsorStudent::where('college_id', $college)->get();
        }


        return view('college.students.social.index', compact('socials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $get = Polytechnic::find(college('college_id'));
        $response = [];
        if ($get)
            $response = $get->departments()
                ->distinct('department_name')->get();


        $departments = $response;

        $feesCategories = FeeCategory::all();
        $students = [];
        $aStudents = AdmittedStudent::where('college_id', college('college_id'))->get();
        $pStudents = AdmissionPrivate::where('college_id', college('college_id'))->get();

        foreach ($aStudents as $aStudent) {
            if (!applied($aStudent->std_id))
                $students[$aStudent->std_id] = $aStudent->std_id . " - " . $aStudent->names;
        }
        foreach ($pStudents as $pStudent) {
            if (!isset($students[$pStudent->std_id]))
                $students[$pStudent->std_id] = $pStudent->std_id . " - " . $pStudent->names;
        }
        $sponsors = SourceSponsor::all();
        if ($sponsors) {
            $sponsors = $sponsors->pluck('sponsor', 'sponsor');
            $mp = [];
            foreach ($sponsors as $key => $sponsor) {
                $mp[$key] = strtoupper($sponsor);
            }
            $sponsors = $mp;
        } else
            $sponsors = [];

        return view('college.students.social.create', compact('departments', 'feesCategories', 'sponsors', 'students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SocialStudentRegisterRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SocialStudentRegisterRequest $request)
    {
//        $id = privateGenerateReg();

//        $data = [
//            'id' => $id,
//            'name' => $request->input('first_name') . " " . $request->input('last_name'),
//            'email' => $request->input('email'),
//            'password' => bcrypt($request->input('password')),
//            'sponsorship' => 'social'
//        ];
//
//        $add = Student::create($data);

//        if ($add) {
//            $data['admission'] = false;
//            $data['std_id'] = $data['id'];
//            $data['first_name'] = $request->input('first_name');
//            $data['other_names'] = $request->input('last_name');
//            $data['department_id'] = $request->input('department');
//            $data['course_id'] = $request->input('program');
//            $data['college_id'] = college('college_id');
//            $data['year_of_study'] = date('Y');
//            $data['sponsorship_status'] = $request->input('sponsor');
//            $adm = AdmittedStudent::create($data);

//            $sponsorStudent = SponsorStudent::where('student_id', $data['id'])->first();
        $id = $request->input('student');
        $sponsorStudent = SponsorStudent::where('student_id', $id)->first();
        if (!$sponsorStudent) {
            $fees = $request->input('fees');
            $percentage = $request->input('percentage');
            $insert = [];
            $data = null;
            if ($fees) {
                foreach ($fees as $key => $value) {
                    $insert[] = [
                        'college_id' => college('college_id'),
                        'student_id' => $id,
                        'category_id' => $value,
                        'sponsor' => $request->input('sponsor'),
                        'percentage' => isset($percentage[$key]) ? $percentage[$key] : '',
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];
                }
                SponsorStudent::insert($insert);
                if ($insert) {
                    if (getStudentInfo($id)) {
                        $data['email'] = getStudentInfo($id, 'email');
                        $data['name'] = getStudentInfo($id, 'names');
                        $data['id'] = $id;
                        $data['college'] = getStudentInfo($id, 'college_id');
                        $data['department'] = getStudentInfo($id, 'department_id');
                        $data['program'] = getStudentInfo($id, 'course_id');
                        $data['sponsor'] = strtoupper($request->input('sponsor'));
                        foreach ($insert as $item) {
                            $category = FeeCategory::find($item['category_id']);
                            $data['categories'][] = [
                                'category' => $category ? $category->category_name : '',
                                'percentage' => $item['percentage']
                            ];
                        }
                    }
                }
            }
            if ($data) {
                Mail::to($data['email'])->send(new SendEmailSuccessfullySocialStudent($data));
            }
            $sponsorStudent = SponsorStudent::where('student_id', $id)->first();
            if ($sponsorStudent->admitted)
                $sponsorStudent->admitted()->update(['sponsorship_status' => $request->input('sponsor')]);
            if ($sponsorStudent->student)
                $sponsorStudent->student()->update(['sponsorship_status' => $request->input('sponsor')]);
        }
//            if ($adm)
//                Mail::to($data['email'])->send(new SendEmailSuccessfullySocialStudent($data['name'], $data['id'], $data['email'], $request->input('password')));
//        }


        return back()->with(['success' => 'Successfully added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sponsorStudent = SponsorStudent::where('student_id', $id)->first();
        if ($sponsorStudent)
            $sponsorStudent->delete();
        return back()->with(['status' => 1, 'message' => 'Deleted successfully !!']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getSponsoredStudents()
    {
        $datas = DB::table('rp_sponsor_students')
            ->join('rp_admitted_students', 'rp_sponsor_students.student_id', '=', 'rp_admitted_students.std_id')
            ->join('rp_source_fees_category', 'rp_sponsor_students.category_id', '=', 'rp_source_fees_category.id')
            ->join('rp_departments', 'rp_admitted_students.department_id', '=', 'rp_departments.id')
            ->join('rp_programs', 'rp_admitted_students.course_id', '=', 'rp_programs.id');

        if (auth()->guard('rp')->check())
            $datas->join('adm_polytechnics', 'rp_sponsor_students.college_id', '=', 'adm_polytechnics.id');

        if (auth()->guard('college')->check())
            $datas->where('rp_sponsor_students.college_id', auth()->guard('college')->user()->college_id);

        $selected = [
            'rp_source_fees_category.category_name',
            'rp_departments.department_name',
            'rp_programs.program_name',
            'rp_admitted_students.std_id',
            'rp_admitted_students.first_name',
            'rp_admitted_students.other_names',
            'rp_admitted_students.gender',
            'rp_admitted_students.photo',
            'rp_sponsor_students.*'
        ];
        if (auth()->guard('rp')->check())
            $selected[] = 'adm_polytechnics.short_name';

        $datas->select($selected);

        return DataTables::of($datas)
            ->addColumn('action', function ($data) {
                $route = route('college.social.destroy', $data->std_id);
                $form = "";
                if (!applied($data->std_id)) {
                    $form = '<form method="POST" action="' . $route . '">' . csrf_field() .
                        '<input type="hidden" name="_method" value="delete"> 
                            <input type="hidden" name="id" value="' . $data->std_id . '"> 
                             <button class="btn btn-sm btn-danger">
                             <i class="fa fa-trash"></i>
                             DELETE
                             </button>
                            </form>';
                }
                return $form;
            })
            ->make(true);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getSponsoredOtherStudents()
    {
        $datas = DB::table('rp_sponsor_students')
            ->join('rp_admission_privates', 'rp_sponsor_students.student_id', '=', 'rp_admission_privates.std_id')
            ->join('rp_source_fees_category', 'rp_sponsor_students.category_id', '=', 'rp_source_fees_category.id')
            ->leftJoin('rp_departments', 'rp_admission_privates.department_id', '=', 'rp_departments.id')
            ->leftJoin('rp_programs', 'rp_admission_privates.course_id', '=', 'rp_programs.id');

        if (auth()->guard('rp')->check())
            $datas->join('adm_polytechnics', 'rp_sponsor_students.college_id', '=', 'adm_polytechnics.id');

        if (auth()->guard('college')->check())
            $datas->where('rp_sponsor_students.college_id', auth()->guard('college')->user()->college_id);

        $selected = [
            'rp_source_fees_category.category_name',
            'rp_departments.department_name',
            'rp_programs.program_name',
            'rp_admission_privates.std_id',
            'rp_admission_privates.first_name',
            'rp_admission_privates.other_names',
            'rp_admission_privates.gender',
            'rp_admission_privates.photo',
            'rp_sponsor_students.*'
        ];
        if (auth()->guard('rp')->check())
            $selected[] = 'adm_polytechnics.short_name';

        $datas->select($selected);

        return DataTables::of($datas)
            ->addColumn('action', function ($data) {
                $route = route('college.social.destroy', $data->std_id);
                $form = "";
                if (!applied($data->std_id)) {
                    $form = '<form method="POST" action="' . $route . '">' . csrf_field() .
                        '<input type="hidden" name="_method" value="delete"> 
                            <input type="hidden" name="id" value="' . $data->std_id . '"> 
                             <button class="btn btn-sm btn-danger">
                             <i class="fa fa-trash"></i>
                             DELETE
                             </button>
                            </form>';
                }
                return $form;
            })
            ->make(true);
    }
}
