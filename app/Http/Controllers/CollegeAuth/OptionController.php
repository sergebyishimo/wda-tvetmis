<?php

namespace App\Http\Controllers\CollegeAuth;

use App\Rp\CollegeOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rp\CollegeDepartment;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $option = null;
        $departments = CollegeDepartment::where('college_id', college('college')->id)->get();
        $options = CollegeOption::where('college_id', college('college')->id)->get();
        return view('college.options.index',  compact('departments', 'option', 'options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = null;
        $dept = new CollegeOption();
        $dept->college_id = college('college')->id;
        $dept->department_id = $request->department_id;
        $dept->option_name = $request->option_name;
        $save = $dept->save();
        if ($save)
            return redirect(route('college.options.index'))->with(['status' => '1', 'message' => "Option Added Successfully."]);
        else
            return redirect(route('college.options.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Adds an Option."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rp\CollegeOption  $collegeOption
     * @return \Illuminate\Http\Response
     */
    public function show(CollegeOption $collegeOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rp\CollegeOption  $collegeOption
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = CollegeDepartment::where('college_id', college('college')->id)->get();
        $options = CollegeOption::where('college_id', college('college')->id)->get();
        $option = CollegeOption::where('id', $id)->first();
        if ($option) {
            return view('college.options.index', compact('option', 'departments', 'options'));
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rp\CollegeOption  $collegeOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CollegeOption $collegeOption, $id)
    {
        $department = CollegeOption::find($id);
        if (!$department)
            return abort(419);
        
        $department->department_id = $request->department_id;
        $department->option_name = $request->option_name;
        $save = $department->save();

        if ($save)
            return redirect(route('college.options.index'))->with(['status' => '1', 'message' => "Option Updated Successfully."]);
        else
            return redirect(route('college.options.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update an Option."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rp\CollegeOption  $collegeOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(CollegeOption $collegeOption, $id)
    {
        $department = CollegeOption::find($id);
        if ($department) {
            $department->delete();
            return redirect(route('college.options.index'))->with(['status' => '1', 'message' => 'Department Deleted Successful']);
        } else {
            abort(409);
        }
    }
}
