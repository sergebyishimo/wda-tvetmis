<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Level;
use App\SchoolPermission;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('school.permissions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $term = $request->input('term');
        $year = $request->input('acad_year');
        $level = $request->input('level');
        $permissions = school(true)
            ->studentPermissions()
            ->where('term', $term)
            ->where('acad_year', $year)
            ->whereNull('returned');

        $level = Level::findOrFail($level);
        return view('school.permissions.verify', compact("permissions", "level"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = school(true)->settings;

        if (count($request->input('std')) > 0 && is_array($request->input('std'))) {
            $level_id = $request->input('level');
            $students = $request->input('std');
            $sms = [];
            foreach ($students as $student) {
                $data = [
                    'school_id' => school('id'),
                    'student_id' => $student,
                    'acad_year' => date('Y'),
                    'term' => $settings->active_term,
                    'reason' => $request->input('reason'),
                    'destination' => $request->input('destination'),
                    'observation' => $request->input('observation'),
                    'issued_by' => \Auth::user()->name,
                    'leaving_datetime' => date('Y-m-d H:i:s', strtotime($request->input('leave_datetime'))),
                    'returning_datetime' => date('Y-m-d H:i:s', strtotime($request->input('return_datetime'))),
                    'leaving_sms' => $request->input('informParents') ? true : false
                ];

                $c = SchoolPermission::create($data);
                if ($c)
                    $sms[] = [
                        'student' => $student,
                        'reason' => $data['reason'],
                        'destination' => $data['destination'],
                        'leaving' => $data['leaving_datetime'],
                        'sms' => $request->input('informParents') ? true : false
                    ];
            }

            if (count($sms) > 0) {
                foreach ($sms as $key => $value) {
                    $std = Student::find($value['student']);
                    if ($value['sms'] == true) {
                        $message = "Mubyeyi dufatanyije kurera umwana wawe asohotse ikigo" . $value['leaving'] .
                            " kubera" . $value['reason'] . ", agiye " . $value['destination'];
                        $number = getStudentGPhone($std);
                        if ($number != null)
                            sendSMS($number, $message, 'permission');
                    }
                }
            }
        }

        return back()->with(['status' => '1', 'message' => 'Permission Given Successfuly!! ']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permissions = school(true)
            ->studentPermissions()
            ->where('id', $id)->first();
        if ($permissions) {
            $permissions->returned = date("Y-m-d");
            $permissions->save();
            return redirect()->route('school.permission.index')->with(['status' => '1', 'message' => 'Permission Verified .']);
        }
        return abort(404);
    }
}
