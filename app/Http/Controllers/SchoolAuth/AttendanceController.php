<?php

namespace App\Http\Controllers\SchoolAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttendanceController extends Controller
{
    /**
     * Display Course Attendance Form
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('school.attendance.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function postCourse(Request $request)
    {
        $month = $request->month;
        $level = $request->level_id;
        $course = $request->course_id;
        $students = school(true)->students()
            ->where("level_id", $level)
            ->where("status", "active");
        $class = school(true)->levels()->where('id', $level)->first();
        $course = school(true)->courses()->where('id', $course)->first();

        $expode = explode('-', $month);
        $year = $expode[0];
        $mm = $expode[1];
        $number = cal_days_in_month(CAL_GREGORIAN, $mm, $year);
        $level = $class;
        return view("school.attendance.view.course", compact("month", "class", "course", "level", "students", "number"));
    }

    /**
     * Display Event Attendance Form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function event()
    {
        return view('school.attendance.event');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function postEvent(Request $request)
    {
        $month = $request->month;
        $level = $request->level_id;
        $students = school(true)->students()
            ->where("level_id", $level)
            ->where("status", "active");
        $class = school(true)->levels()->where('id', $level)->first();

        $expode = explode('-', $month);
        $year = $expode[0];
        $mm = $expode[1];
        $number = cal_days_in_month(CAL_GREGORIAN, $mm, $year);
        $level = $class;
        $type = getAttendanceEvent($request->input('event'));
        $event = $request->input('event');
        return view("school.attendance.view.event", compact("month", "class", "level", "students", "number", "type", "event"));
    }

    /**
     * Display In & Out Attendance Form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function inout()
    {
        return view('school.attendance.in-out');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function postInOut(Request $request)
    {
        $begins = $request->date_start;
        $ends = $request->date_end;
        $class = $request->level_id;
        $attendance = inOutAttendance($begins, $ends, $class);
        $daterange = $attendance->getDateRange();
        $students = $attendance->getStudents();

        $level = school(true)->levels()->where("id", $class)->first();

        return view("school.attendance.view.in-out", compact("begins", "ends", "level", "attendance", "daterange", "students"));
    }

    /**
     * Display Visting form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function visiting()
    {
        return view('school.attendance.visiting');
    }

    /**
     * @param Request $request
     */

    public function postVisiting(Request $request)
    {

    }

    /**
     * Display Staff Form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function staff()
    {
        return view('school.attendance.staff');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function postStaff(Request $request)
    {
        $start = $request->input('startdate');
        $end = $request->input('enddate');
        $staffs = school(true)->staffs()
            ->where('status', 'active')
            ->orderBy('first_name', "ASC")
            ->orderBy('last_name', "ASC")
            ->get();

        $attendance = staffAttendance($start, $end);

        return view('school.attendance.view.staff', compact('attendance', 'staffs'));
    }
}
