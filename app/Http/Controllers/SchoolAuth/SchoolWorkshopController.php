<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateWorkshopRequest;
use App\Http\Requests\UpdateWorkshopRequest;
use App\SchoolWorkshop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolWorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workshops = school(true)->workshops;
        return view('school.workshops.index',compact('workshops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schoolworkshop = null;
        return view('school.workshops.create',compact('schoolworkshop'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateWorkshopRequest $request)
    {
        $schoolworkshop = new SchoolWorkshop();
        $schoolworkshop->school_id = school(true)->id;
        $schoolworkshop->workshop_name = $request->input('workshop_name');
        $schoolworkshop->sector = $request->input('sector');
        $schoolworkshop->sub_sector = $request->input('sub_sector');
        $save = $schoolworkshop->save();

        if ($save)
            return redirect(route('school.workshops.index'))->with(['status' => '1', 'message' => "Workshop  Added Successfully."]);
        else
            return redirect(route('school.workshops.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Add Workshop."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $schoolworkshop = school(true)->workshops()
            ->where('id', $id)->first();
        if ($schoolworkshop)
            return view("school.workshops.create", compact("schoolworkshop"));
        else
            return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $schoolworkshop = school(true)->workshops()
            ->where('id', $id)->first();
        if ($schoolworkshop)
            return view("school.workshops.create", compact("schoolworkshop"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorkshopRequest $request, $id)
    {
        $schoolworkshop = school(true)->workshops()
            ->where('id', $id)->first();
        if (!$schoolworkshop)
            return abort(419);

        $schoolworkshop->school_id = school(true)->id;
        $schoolworkshop->workshop_name = $request->input('workshop_name');
        $schoolworkshop->sector = $request->input('sector');
        $schoolworkshop->sub_sector = $request->input('sub_sector');
        $save = $schoolworkshop->save();

        if ($save)
            return redirect(route('school.workshops.index'))->with(['status' => '1', 'message' => "Workshop Updated Successfully."]);
        else
            return redirect(route('school.workshops.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Workshop."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = school(true)->workshops()->where('id', $id)->first();
        if (!$request)
            return abort(419);
        $request->delete();

        return redirect(route('school.workshops.index'));
    }
}
