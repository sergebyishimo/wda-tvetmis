<?php

namespace App\Http\Controllers\SchoolAuth;

use App\ExamEntry;
use App\ModuleType;
use App\PeriodicEntry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use Excel;

//use QrCode;

class MarksController extends Controller
{
    public function index()
    {
        return view('school.marks.index');
    }

    public function courses($level_id, $term = null)
    {
        $settings = school(true)->settings;
        $school = school(true);

        if (!isset($term)) {
            $term = $settings->active_term;
        }
        $courses = [];

        if (isAllowed(2) || isAllowed(5)) {
            $c = $school->courses()->whereIn('term', [$term, 4])
                ->where('level_id', $level_id)->where('status', 1)->get();
            if ($c) {
                foreach ($c as $item) {
                    $courses[] = ['id' => $item->id, 'name' => $item->module->module_title];
                }
            }
        } else {
            $c = $school->courses()->where('staffs_info_id', getStaff('id'))
                ->whereIn('term', [$term, 4])
                ->where('level_id', $level_id)->where('status', 1)->get();
            if ($c) {
                foreach ($c as $item) {
                    $courses[] = ['id' => $item->id, 'name' => $item->module->module_title];
                }
            }
        }
        return $courses;
    }

    public function allCourses($level_id)
    {
        $courses = Course::where('level_id', $level_id)->where('status', '!=', 'deleted')->get();
        return $courses;
    }

    public function category($category)
    {
        $category_values = Student::select($category)->distinct($category)->get();

        return $category_values;
    }

    public function period(Request $request)
    {
        $levels = school(true)->levels()->where('status', 1)->get();

        $settings = settings();

        if (isset($request->acad_year))
            $acad_year = $request->acad_year;
        else
            $acad_year = date('Y');

        if (isset($request->term))
            $term = $request->term;
        else
            $term = $settings->active_term;

        if (isset($request->period))
            $period = $request->period;
        else
            $period = $settings->active_period;

        if (isset($request->marks_type)) {

            $courses = school(true)->courses()->where('staffs_info_id', getStaff('id'))
                ->whereIn('term', [$term, 4])
                ->where('level_id', $request->input('level'))->where('status', 1)->get();

            // check if it is an update
            $check = school(true)->periodicEntry()
                ->where('course_id', $request->input('course_id'))
                ->where('marks_type', $request->input('marks_type'))
                ->where('term', $term)
                ->where('period', $period)
                ->where('acad_year', date('Y'))
                ->take(1)->get();

            if (count($check) == 0) {
                $students = school(true)->students()->where('level_id', $request->level_id)
                    ->where('status', 'active')
                    ->orderBy('fname', 'asc')
                    ->orderBy('lname', 'asc')
                    ->get();
//                dd($request->level_id);
                return view('school.marks.periodMarksEntry', [
                    'disable_nav' => true,
                    'levels' => $levels,
                    'students' => $students,
                    'courses' => $courses,
                    'level_id' => $request->level_id,
                    'marks_type' => $request->marks_type,
                    'course_id' => $request->course_id,
                    'acad_year' => $acad_year,
                    'term' => $term,
                    'period' => $period]);
            } else {

                $diff = time() - strtotime($check->first()->created_at);

                if ($diff > 86400 && isAllowed(5)) { // Auth::user()->staff['privilege'] != 5
                    return view('school.marks.periodMarksEntry', [
                        'disable_nav' => true,
                        'levels' => $levels,
                        'courses' => $courses,
                        'level_id' => $request->level_id,
                        'marks_type' => $request->marks_type,
                        'course_id' => $request->course_id,
                        'acad_year' => $acad_year,
                        'term' => $term,
                        'period' => $period,
                        'timeout' => true]);

                } else {

                    $u_students = school(true)->students()->where('level_id', $request->level_id)
                        ->where('status', 'active')
                        ->orderBy('fname', 'ASC')
                        ->orderBy('lname', 'ASC')
                        ->get();

                    return view('school.marks.periodMarksEntry', [
                        'disable_nav' => true,
                        'levels' => $levels,
                        'u_students' => $u_students,
                        'courses' => $courses,
                        'level_id' => $request->level_id,
                        'marks_type' => $request->marks_type,
                        'course_id' => $request->course_id,
                        'acad_year' => $acad_year,
                        'term' => $term,
                        'period' => $period,
                        'check' => $check->first()]);
                }


            }
        } else
            return view('school.marks.periodMarksEntry', [
                'disable_nav' => true,
                'levels' => $levels,
                'acad_year' => $acad_year,
                'term' => $term,
                'period' => $period
            ]);

    }

    public function periodSave(Request $request)
    {
        $settings = settings();

        if ($request->marks_type == 'Whole') {

            $term = $settings->active_term;
            $acad_year = date('Y');

            $count = count($request->reg_no);


            foreach ($request->reg_no as $key => $reg_no) {
                // check if its an update
                $i = $key;
                $check_period = school(true)->periodicEntry()
                    ->where('marks_type', 'Whole')
                    ->where('term', $term)
                    ->where('acad_year', $acad_year)
                    ->where('std_reg_no', $request->reg_no[$i])
                    ->select('id')
                    ->get();
                if (count($check_period) == 0) {
                    $marks = new PeriodicEntry();

                    $marks->school_id = school('id');
                    $marks->std_reg_no = $request->reg_no[$i];
                    $marks->term = $term;
                    $marks->period = 1;
                    $marks->marks_type = 'Whole';
                    $marks->course_id = $request->course_id;
                    $marks->obt_marks = ($request->my_new_cat[$i]) ? $request->my_new_cat[$i] : 0;
                    $marks->total = $request->total;
                    $marks->acad_year = date('Y');
                    $marks->given_date = date('Y-m-d');

                    $marks->save();
                } else {
//                    $marks = PeriodicEntry::find($check_period[0]['id']);
                    $marks = school(true)->periodicEntry()
                        ->where('id', $check_period[0]['id'])->first();
                    $marks->std_reg_no = $request->reg_no[$i];
                    $marks->term = $term;
                    $marks->period = 1;
                    $marks->marks_type = $request->marks_type;
                    $marks->course_id = $request->course_id;
                    $marks->obt_marks = ($request->my_new_cat[$i]) ? $request->my_new_cat[$i] : 0;
                    $marks->total = $request->total;
                    $marks->acad_year = date('Y');
                    $marks->given_date = date('Y-m-d');

                    $marks->save();
                }

                $check_exam = school(true)->examEntry()
                    ->where('std_reg_no', $request->reg_no[$i])
                    ->where('term', $term)
                    ->where('acad_year', $acad_year)
                    ->select('id')
                    ->get();

                if (count($check_exam) == 0) {
                    $marks = new ExamEntry();

                    $marks->school_id = school('id');
                    $marks->std_reg_no = $request->reg_no[$i];
                    $marks->term = $term;
                    $marks->course_id = $request->course_id;
                    $marks->obt_marks = ($request->my_new_ex[$i]) ? $request->my_new_ex[$i] : 0;
                    $marks->total = $request->total;
                    $marks->acad_year = date('Y');
                    $marks->given_date = date('Y-m-d');

                    $marks->save();
                } else {
                    $marks = school(true)->examEntry()->where('id', $check_exam[0]['id'])->first();

                    $marks->std_reg_no = $request->reg_no[$i];
                    $marks->term = $term;
                    $marks->course_id = $request->course_id;
                    $marks->obt_marks = ($request->my_new_ex[$i]) ? $request->my_new_ex[$i] : 0;
                    $marks->total = $request->total;
                    $marks->acad_year = date('Y');
                    $marks->given_date = date('Y-m-d');

                    $marks->save();
                }


            }

            return redirect()->route('school.marks.index')->with(['status' => '1', 'message' => 'Marks Saved Successfully!!']);
        } else {
            // check if the current user is the one who teaches that course

            $course_info = school(true)->courses()->where('id', $request->course_id)->first();

            if ($course_info->staffs_info_id == getStaff('id') || isAllowed()) {

                $count = count($request->reg_no);

                for ($i = 0; $i < $count; $i++) {
                    $marks = new PeriodicEntry();

                    $marks->school_id = school('id');
                    $marks->std_reg_no = $request->reg_no[$i];
                    $marks->term = settings()->active_term;
                    $marks->period = settings()->active_period;
                    $marks->marks_type = $request->marks_type;
                    $marks->course_id = $request->course_id;
                    $marks->obt_marks = ($request->my_new_marks[$i]) ? $request->my_new_marks[$i] : 0;
                    $marks->total = $request->total;
                    $marks->acad_year = date('Y');
                    $marks->given_date = date('Y-m-d', strtotime($request->date));

                    $marks->save();
                }

                return redirect(route('school.marks.index'))->with(['status' => '1', 'message' => 'Periodic Marks Saved Successfully!!']);
            } else {
                return redirect(route('school.marks.index'))->withErrors(['errorMessage' => 'Only the teacher of that course is allowed to make that action!!']);
            }
        }
    }

    public function periodicMarksUpdate(Request $request)
    {
        // check if the current user is the one who teaches that course or director or head teacher

        $course_info = school(true)->courses()->where('id', $request->course_id)->first();

        if ($course_info->staffs_info_id == getStaff('id') || isAllowed(2) || isAllowed(5)) {
            $settings = settings();
            $count = count($request->u_reg_no);

            for ($i = 0; $i < $count; $i++) {
                $marks = school(true)->periodicEntry()->where('id', $request->u_marks_id[$i])->first();

                $marks->std_reg_no = $request->u_reg_no[$i];
                $marks->term = $settings->active_term;
                $marks->period = $settings->active_period;
                $marks->marks_type = $request->marks_type;
                $marks->course_id = $request->course_id;
                $marks->obt_marks = ($request->my_marks[$i]) ? $request->my_marks[$i] : 0;
                $marks->total = $request->u_total;
                $marks->acad_year = date('Y');
                $marks->given_date = date('Y-m-d', strtotime($request->date));

                $marks->save();
            }

            return redirect(route('school.marks.index'))->with(['status' => '1', 'message' => 'Periodic Marks Updated Successfully!!']);
        } else {
            return redirect(route('school.marks.index'))->withErrors(['errorMessage' => 'You are not allowed to make that action!!']);
        }
    }

    public function examMarks(Request $request)
    {
        $levels = school(true)->levels()->where('status', 1)->get();
        $settings = settings();

        if ($settings->active_period != 6 && !isAllowed() && !isAllowed(5)) {
            return redirect(route('school.marks.index'))->withErrors(['errorMessage' => 'Exam Period Not Started!!']);
        }

        if (isset($request->acad_year)) {
            $acad_year = $request->acad_year;
        } else {
            $acad_year = date('Y');
        }

        if (isset($request->term)) {
            $term = $request->term;
        } else {
            $term = $settings[0]->active_term;
        }

        if (isset($request->level_id)) {
            $courses = school(true)->courses()
                ->where('level_id', $request->level_id)
                ->where('status', 1);

            if (!isAllowed())
                $courses->where('staffs_info_id', getStaff('id'));

            $courses = $courses->get();

            // check if it is an update

            $check = school(true)->examEntry()
                ->where('course_id', $request->course_id)
                ->where('term', $term)
                ->where('acad_year', date('Y'))
                ->take(1)
                ->get();

            if (count($check) == 0) {
                $students = school(true)->students()
                    ->where('level_id', $request->level_id)
                    ->where('status', 'active')
                    ->orderBy('fname', 'asc')
                    ->orderBy('lname', 'asc')
                    ->get();

                return view('school.marks.examMarks', [
                    'disable_nav' => true,
                    'levels' => $levels,
                    'students' => $students,
                    'courses' => $courses,
                    'level_id' => $request->level_id,
                    'course_id' => $request->course_id,
                    'term' => $term,
                    'acad_year' => $acad_year
                ]);
            } else {
                $u_students = school(true)->students()
                    ->where('level_id', $request->level_id)
                    ->where('status', 'active')
                    ->orderBy('fname', 'asc')
                    ->orderBy('lname', 'asc')
                    ->get();

                return view('school.marks.examMarks', [
                    'disable_nav' => true,
                    'levels' => $levels,
                    'u_students' => $u_students,
                    'courses' => $courses,
                    'level_id' => $request->level_id,
                    'course_id' => $request->course_id,
                    'term' => $term,
                    'acad_year' => $acad_year,
                    'check' => $check[0]
                ]);
            }
        } else {
            return view('school.marks.examMarks', [
                'disable_nav' => true,
                'levels' => $levels,
                'acad_year' => $acad_year,
                'term' => $term
            ]);
        }


    }

    /**
     * Exam Marks Entry
     */

    public function examMarksEntry(Request $request)
    {
        $settings = settings();

        $count = count($request->reg_no);

        for ($i = 0; $i < $count; $i++) {
            $marks = new ExamEntry();

            $marks->school_id = school('id');
            $marks->std_reg_no = $request->reg_no[$i];
            $marks->term = $settings->active_term;
            $marks->course_id = $request->course_id;
            $marks->obt_marks = ($request->my_new_marks[$i]) ? $request->my_new_marks[$i] : 0;
            $marks->total = $request->total;
            $marks->acad_year = date('Y');
            $marks->given_date = date('Y-m-d', strtotime($request->date));

            $marks->save();
        }

        return redirect(route('school.marks.index'))->with(['status' => '1', 'message' => 'Periodic Marks Saved Successfully!!']);
    }


    /**
     * Exam Marks Update
     */

    public function examMarksUpdate(Request $request)
    {
        $settings = settings();

        $count = count($request->u_reg_no);

        for ($i = 0; $i < $count; $i++) {

            $marks = school(true)->examEntry()->where('id', $request->u_marks_id[$i])->first();

            if ($marks) {
                $marks->std_reg_no = $request->u_reg_no[$i];
                $marks->term = $settings->active_term;
                $marks->course_id = $request->course_id;
                $marks->obt_marks = ($request->my_marks[$i]) ? $request->my_marks[$i] : 0;
                $marks->total = $request->u_total;
                $marks->acad_year = date('Y');
                $marks->given_date = date('Y-m-d', strtotime($request->date));

                $marks->save();
            }

        }

        return redirect(route('school.marks.index'))->with(['status' => '1', 'message' => 'Periodic Marks Saved Successfully!!']);
    }

    /**
     * Term Marks Entry
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function termEntry(Request $request)
    {

        $levels = school(true)->levels()->where('status', '1')->get();
        $settings = settings();

        if (isset($request->level_id)) {
            $students = school(true)->students()
                ->where('level_id', $request->level_id)
                ->where('status', 'active')
                ->orderBy('fname', 'asc')
                ->orderBy('lname', 'asc')
                ->get();

            $courses = school(true)->courses()
                ->where('level_id', $request->level_id)
                ->where('status', 1)
                ->get();

            $course_info = school(true)->courses()->where('id', $request->course_id)->first();

            // check if it is an update

            $periods = school(true)->periodicEntry()
                ->join('students', 'periodic_marks.std_reg_no', 'students.reg_no')
                ->where('marks_type', 'Whole')
                ->where('term', $settings->active_term)
                ->where('periodic_marks.acad_year', date('Y'))
                ->where('students.level_id', $request->level_id)
                ->where('periodic_marks.course_id', $request->course_id)
                ->get();

            if (count($periods) > 0) {
                $exams = school(true)->examEntry()
                    ->join('students', 'exam_marks.std_reg_no', 'students.reg_no')
                    ->where('term', $settings->active_term)
                    ->where('exam_marks.acad_year', date('Y'))
                    ->where('students.level_id', $request->level_id)
                    ->where('exam_marks.course_id', $request->course_id)
                    ->get();

                return view('school.marks.exam-period-marks', [
                    'disable_nav' => true,
                    'levels' => $levels,
                    'term' => $settings->active_term,
                    'students' => $students,
                    'courses' => $courses,
                    'course_info' => $course_info,
                    'level_id' => $request->level_id,
                    'course_id' => $request->course_id,
                    'acad_year' => date('Y'),
                    'periods' => $periods,
                    'exams' => $exams
                ]);
            } else {
                return view('school.marks.exam-period-marks', [
                    'disable_nav' => true,
                    'levels' => $levels,
                    'term' => $settings->active_term,
                    'students' => $students,
                    'courses' => $courses,
                    'course_info' => $course_info,
                    'level_id' => $request->level_id
                ]);
            }
        }
    }

    /**
     * Download Excel
     */

    public function uploadView()
    {
        return view('school.marks.upload-marks');
    }

    public function download(Request $request)
    {
        $level_info = school(true)->levels()->where('id', $request->level_id)->first();
        $course_info = school(true)->courses()->where('id', $request->course_id)->first();

        $ddo = $level_info->department->qualification->qualification_code;
        $nm = $level_info->rtqf->level_name;

        Excel::create($nm . '_' . $ddo . '_' . str_replace(' ', '_', $course_info->module->module_title) . '_Term_' . $request->term . '_Acad_Year_' . $request->acad_year, function ($excel) {

            global $request, $course_info;

            if ($request->course_id == 0) {
                $courses = school(true)->courses()
                    ->where('level_id', $request->level_id)
                    ->where('status', 1)->get();

                foreach ($courses as $course) {

                    $course_info = $course;

                    $excel->sheet($course->module->module_code . '', function ($sheet) {
                        global $request, $course_info;

                        $students = school(true)->students()
                            ->where('level_id', $request->level_id)
                            ->orderBy('fname', 'asc')
                            ->orderBy('lname', 'asc')
                            ->where('status', 'active')
                            ->get();

                        $sheet->row(1, array('Reg. No', 'First Name', 'Last Name', str_replace(' ', '_', $course_info->module->module_title) . ' CAT / ' . $course_info['max_point'] . ' Pts', $course_info->module->module_title . ' Exam / ' . $course_info['max_point'] . ' Pts', 'Term', 'Acad Year'));

                        $sheet->row(1, function ($row) {

                            // call cell manipulation methods
                            $row->setBackground('#337ab7');

                        });

                        $sheet->setHeight(1, 20);

                        $sheet->cells('A1:G1', function ($cells) {

                            // manipulate the range of cells
                            $cells->setFontColor('#ffffff');
                        });

                        $sheet->setBorder('A1:G1', 'thin');

                        $row = 2;
                        foreach ($students as $student) {
                            // Sheet manipulation
                            $sheet->row($row, array(
                                $student['reg_no'], $student['fname'], $student['lname'], 0, 0, $request->term, $request->acad_year
                            ));

                            $sheet->setHeight($row++, 20);
                        }
                    });
                }
            } else {
                $excel->sheet(substr($request->course_id, 0, 7), function ($sheet) {
                    global $request;

                    $level_info = school(true)->levels()->where('id', $request->level_id)->first();
                    $course_info = school(true)->courses()->where('id', $request->course_id)->first();
                    $students = school(true)->students()->where('level_id', $request->level_id)
                        ->orderBy('fname', 'asc')
                        ->orderBy('lname', 'asc')
                        ->where('status', 'active')
                        ->get();

                    $sheet->row(1, array('Reg. No', 'First Name', 'Last Name', $course_info->module->module_title . ' CAT / ' . $course_info['max_point'] . ' Pts', $course_info->module->module_title . ' Exam / ' . $course_info['max_point'] . ' Pts', 'Term', 'Acad Year'));

                    $sheet->row(1, function ($row) {

                        // call cell manipulation methods
                        $row->setBackground('#337ab7');

                    });

                    $sheet->setHeight(1, 20);

                    $sheet->cells('A1:G1', function ($cells) {

                        // manipulate the range of cells
                        $cells->setFontColor('#ffffff');
                    });

                    $sheet->setBorder('A1:G1', 'thin');

                    $row = 2;
                    foreach ($students as $student) {
                        // Sheet manipulation
                        $sheet->row($row, array(
                            $student['reg_no'], $student['fname'], $student['lname'], 0, 0, $request->term, $request->acad_year
                        ));

                        $sheet->setHeight($row++, 20);
                    }
                });

                $excel->sheet('sheet2', function ($sheet) {
                });
            }
        })->export('xls');

        return redirect(route('school.marks.file.exam'))
            ->with(['status' => '1', 'message' => 'File Generated Successfully!! Open and fill it.']);
    }

    public function upload(Request $request)
    {
        global $sheetTitle, $error;


        $fileTitle = $request->file->getClientOriginalNAme();

        $filename = $request->file->storeAs('uploads', $fileTitle . ' ' . date('Ymd H:i') . '.xls', config('voyager.storage.disk'));

        Excel::load('storage/' . $filename, function ($reader) {
            global $sheetTitle, $error;

            $reader->each(function ($sheet) {
                global $sheetTitle, $skippedStudents, $error;
                $sheetTitle = $sheet->getTitle();
                $sheet->each(function ($row) {
                    global $sheetTitle, $skippedStudents, $error;

                    $check_course = school(true)->courses()->where('id', "LIKE", "$sheetTitle%")->first();
                    if ($check_course) {

                        $reg_no = $row['reg._no'];

                        // check if the reg_no exists

                        $check_reg_no = school(true)->students()
                            ->where('reg_no', $reg_no)->get();


                        if (count($check_reg_no) > 0) {

                            $cat_row_name = strtolower(str_replace(' ', '_', $check_course['module']['module_title'])) . '_cat_' . $check_course['max_point'] . '_pts';
                            $ex_row_name = strtolower(str_replace(' ', '_', $check_course['module']['module_title'])) . '_exam_' . $check_course['max_point'] . '_pts';

                            $cat_marks = $row[$cat_row_name];
                            $ex_marks = $row[$ex_row_name];


                            if ($cat_marks > $check_course['max_point']) {
                                $error .= $check_reg_no[0]['fname'] . ' ' . $check_reg_no[0]['lname'] . ' in ' . $check_reg_no[0]['level']['rtqf']['level_name'] . ' ' . $check_reg_no[0]['level']['department']['qualification']['qualification_title'] . ' in ' . $check_course['module']['module_title'] . ' CAT is greater than the total : ' . $cat_marks . ' > ' . $check_course['max_point'] . "<br>";
                                $cat_marks = 0;
                            }

                            if ($ex_marks > $check_course['max_point']) {
                                $error .= $check_reg_no[0]['fname'] . ' ' . $check_reg_no[0]['lname'] . ' in ' . $check_reg_no[0]['level']['rtqf']['level_name'] . ' ' . $check_reg_no[0]['level']['department']['qualification']['qualification_title'] . ' in ' . $check_course['module']['module_title'] . ' EXAM Marks is greater than the total : ' . $ex_marks . ' > ' . $check_course['max_point'] . "<br>";

                                $ex_marks = 0;
                            }

                            if ($cat_marks == '') {
                                $cat_marks = 0;
                            }

                            if ($ex_marks == '') {
                                $ex_marks = 0;
                            }

                            // check if its an update

                            $check_period = school(true)->periodicEntry()
                                ->where('marks_type', 'Whole')
                                ->where('course_id', $check_course->id)
                                ->where('term', $row['term'])
                                ->where('acad_year', $row['acad_year'])
                                ->where('std_reg_no', $reg_no)
                                ->select('id')
                                ->get();

                            if (count($check_period) == 0) {
                                $marks = new PeriodicEntry();

                                $marks->school_id = school('id');
                                $marks->std_reg_no = $reg_no;
                                $marks->term = $row['term'];
                                $marks->period = 1;
                                $marks->marks_type = 'Whole';
                                $marks->course_id = $check_course->id;
                                $marks->obt_marks = $cat_marks;
                                $marks->total = $check_course['max_point'];
                                $marks->acad_year = date('Y');
                                $marks->given_date = date('Y-m-d');

                                $marks->save();

                            } else {
                                $marks = school(true)->periodicEntry()
                                    ->where('id', $check_period[0]['id'])
                                    ->first();

                                $marks->std_reg_no = $reg_no;
                                $marks->term = $row['term'];
                                $marks->period = 1;
                                $marks->marks_type = 'Whole';
                                $marks->course_id = $check_course->id;
                                $marks->obt_marks = $cat_marks;
                                $marks->total = $check_course['max_point'];
                                $marks->acad_year = date('Y');
                                $marks->given_date = date('Y-m-d');

                                $marks->save();
                            }

                            $check_exam = school(true)->examEntry()
                                ->where('std_reg_no', $reg_no)
                                ->where('course_id', $check_course->id)
                                ->where('term', $row['term'])
                                ->where('acad_year', $row['acad_year'])
                                ->select('id')
                                ->get();

                            if (count($check_exam) == 0) {
                                $marks = new ExamEntry();

                                $marks->school_id = school('id');
                                $marks->std_reg_no = $reg_no;
                                $marks->term = $row['term'];
                                $marks->course_id = $check_course->id;
                                $marks->obt_marks = $ex_marks;
                                $marks->total = $check_course['max_point'];
                                $marks->acad_year = date('Y');
                                $marks->given_date = date('Y-m-d');

                                $marks->save();
                            } else {
                                $marks = school(true)->examEntry()->where('id', $check_exam[0]['id'])->first();

                                $marks->std_reg_no = $reg_no;
                                $marks->term = $row['term'];
                                $marks->course_id = $check_course->id;
                                $marks->obt_marks = $ex_marks;
                                $marks->total = $check_course['max_point'];
                                $marks->acad_year = date('Y');
                                $marks->given_date = date('Y-m-d');

                                $marks->save();
                            }
                        } else {
                            $skippedStudents++;
                        }
                    }
                });
            });
        });

        return redirect(route('school.marks.file.exam'))->with(['status' => '1', 'message' => 'Marks Uploaded Successfully!!'])
            ->with('uploadErrors', $error);
    }

    /**
     * Periodic Results Display
     */

    public function periodicResults(Request $request)
    {
        $levels = school(true)->levels()->where('status', 1)->get();
        $settings = settings();

        if (isset($request->acad_year)) {

            if ($request->course_id == 0 || $request->download == 1) {

                $client = new Client([
                    // Base URI is used with relative requests
                    'base_uri' => 'https://www.intouchsms.co.rw/api/sendsms/.json',
                    // You can set any number of default request options.
                    'timeout' => 500.0,
                ]);

                $courses = school(true)->courses()
                    ->where('level_id', $request->level_id)
                    ->whereIn('term', [$request->term, 4])
                    ->where('status', 1)
                    ->get();

                $courses_total = school(true)->courses()
                    ->where('level_id', $request->level_id)
                    ->whereIn('term', [$request->term, 4])
                    ->where('status', 1)
                    ->sum('max_point');

                if ($request->period == 7) {
                    $courses_total = $courses_total * 2;
                }

                $students = school(true)->students()
                    ->where('level_id', $request->level_id)
                    ->get();

                $students = $students->each(function ($item, $key) {
                    global $request;
                    $item->acad_year = $request->acad_year;
                    $item->term = $request->term;
                    $item->period = $request->period;
                });

                if ($request->period == 7) {
                    $students = $students->sortByDesc('term_perc');
                } else {
                    $students = $students->sortByDesc('total_obt_marks_sort');
                }

                if (isset($request->download) && $request->download == 1) {

                    $level_info = school(true)->levels()->where('id', $request->level_id)->first();


                    return view('school.marks.print.period-results', [
                        'levels' => $levels,
                        'acad_year' => $request->acad_year,
                        'term' => $request->term,
                        'period' => $request->period,
                        'level_id' => $request->level_id,
                        'course_id' => $request->course_id,
                        'courses' => $courses,
                        'courses_total' => $courses_total,
                        'students' => $students,
                        'send_sms' => $request->send_sms,
                        'client' => $client,
                        'title' => $level_info['rtqf']['level_name'] . ' ' . $level_info['department']['qualification']['qualification_title'] . ' Acad Year ' . $request->acad_year . ' Term ' . $request->term . ' Period ' . $request->period
                    ]);
                } else {
                    return view('school.marks.period-results', [
                        'disable_nav' => true,
                        'levels' => $levels,
                        'acad_year' => $request->acad_year,
                        'term' => $request->term,
                        'period' => $request->period,
                        'level_id' => $request->level_id,
                        'course_id' => $request->course_id,
                        'courses' => $courses,
                        'courses_total' => $courses_total,
                        'students' => $students,
                        'send_sms' => $request->send_sms,
                        'client' => $client
                    ]);
                }

            } else {
                $courses = school(true)->courses()
                    ->where('level_id', $request->level_id)
                    ->whereIn('term', [$request->term, 4])
                    ->where('status', 1)
                    ->get();

                if ($request->period == 7) {
                    return view('school.marks.period-results', [
                        'disable_nav' => true,
                        'levels' => $levels,
                        'acad_year' => $request->acad_year,
                        'term' => $request->term,
                        'period' => $request->period,
                        'level_id' => $request->level_id,
                        'course_id' => $request->course_id,
                        'courses' => $courses,
                        'error' => true
                    ]);
                } else {
                    $periods = school(true)->periodicEntry()
                        ->where('course_id', $request->course_id)
                        ->where('acad_year', $request->acad_year)
                        ->where('term', $request->term)
                        ->where('period', $request->period)
                        ->select('marks_type', 'total')
                        ->groupBy('marks_type', 'total')
                        ->get();

                    $periods_total = school(true)->periodicEntry()
                        ->where('course_id', $request->course_id)
                        ->where('acad_year', $request->acad_year)
                        ->where('term', $request->term)
                        ->where('period', $request->period)
                        ->distinct('marks_type')
                        ->sum('total');
                }

                $course = school(true)->courses()
                    ->where('id', $request->course_id)
                    ->get();

                $students = school(true)->students()
                    ->where('level_id', $request->level_id)
                    ->get();

                $students = $students->each(function ($item, $key) {
                    global $request;
                    $item->acad_year = $request->acad_year;
                    $item->term = $request->term;
                    $item->period = $request->period;
                });

                $students = $students->sortByDesc('total_obt_marks');

                return view('school.marks.period-results', [
                    'disable_nav' => true,
                    'levels' => $levels,
                    'acad_year' => $request->acad_year,
                    'term' => $request->term,
                    'period' => $request->period,
                    'level_id' => $request->level_id,
                    'course_id' => $request->course_id,
                    'periods' => $periods,
                    'course_info' => $course[0],
                    'courses' => $courses,
                    'periods_total' => $periods_total,
                    'students' => $students
                ]);
            }

        }
    }


    /**
     * Reports
     */

    public function report(Request $request)
    {

        $levels = school(true)->levels()->where('status', 1)->get();
        $settings = settings();


        if (isset($request->level_id)) {
            $students = school(true)->students()
                ->where('level_id', $request->level_id)
                ->where('status', 'active')
                ->orderBy('fname', 'asc')
                ->orderBy('lname', 'asc')
                ->get();

            $students = $students->each(function ($item, $key) {
                global $request;
                $item->acad_year = $request->acad_year;
                $item->term = $request->term;
            });

            $students = $students->sortByDesc('term_perc');

            if (isset($request->download) && $request->download == 1) {
                $level_info = school(true)->levels()->where('id', $request->level_id)->first();

                return view('school.marks.print.reportsDownload', [
                    'acad_year' => $request->acad_year,
                    'term' => $request->term,
                    'students' => $students,
                    'level_id' => $request->level_id,
                    'level_info' => $level_info,
                    'title' => $level_info['rtqf']['level_name'] . ' ' . $level_info['department']['qualification']['qualification_title'] . ' Term ' . $request->term . ' Acad Year' . $request->acad_year . ' Proclamation Report '
                ]);

            } else {
                return view('school.marks.reporting', [
                    'disable_nav' => true,
                    'levels' => $levels,
                    'acad_year' => $request->acad_year,
                    'term' => $request->term,
                    'students' => $students,
                    'level_id' => $request->level_id
                ]);
            }


        } else {
            return view('school.marks.reporting', [
                'disable_nav' => true,
                'levels' => $levels,
                'term' => $settings->active_term,
                'period' => $settings->active_period
            ]);
        }
    }

    /**
     * Student Report
     */

    public function studentReport(Request $request)
    {
        $reg_no = $request->reg_no;
        $acad_year = $request->year;
        $term = $request->term;
        $qed = $request->qed;

        $download = $request->download;

        $student = school(true)->students()->where('reg_no', $reg_no)->get();

        $all_students = school(true)->students()
            ->where('level_id', $student[0]['level_id'])
            ->orderBy('fname', 'asc')
            ->orderBy('lname', 'asc')
            ->where('status', 'active')
            ->count();


        if ($term == 4) {

            $moduleType = ModuleType::all();
            $type_cource = [];
            if ($moduleType->count() > 0) {
                foreach ($moduleType as $key => $type) {
                    if ($type->modules()->count() > 0) {
                        foreach ($type->modules as $module) {
                            $m = $module->courses()
                                ->where('school_id', school('id'))
                                ->where('status', 1)
                                ->where('level_id', $student[0]['level_id']);
                            if ($m->count() > 0) {
                                $type_cource[$type->title][] = [$m->get()];
                            }
                        }
//                        $type_cource[$key] = $type->modules();
                    }
                }
            }
//
//            $first_courses = school(true)->courses()
//                ->where('category', 'GENERAL SUBJECT')
//                ->where('status', 1)
//                ->where('level_id', $student[0]['level_id'])
//                ->get();
//
//            $second_courses = school(true)->courses()
//                ->where('category', 'CORE SUBJECT')
//                ->where('status', 1)
//                ->where('level_id', $student[0]['level_id'])
//                ->get();

        } else {

            $moduleType = ModuleType::all();
            $type_cource = [];
            if ($moduleType->count() > 0) {
                foreach ($moduleType as $key => $type) {
                    if ($type->modules()->count() > 0) {
                        foreach ($type->modules as $module) {
                            $m = $module->courses()
                                ->whereIn('term', [$term, 4])
                                ->where('school_id', school('id'))
                                ->where('status', 1)
                                ->where('level_id', $student[0]['level_id']);
                            if ($m->count() > 0) {
                                $type_cource[$type->title] = [$m->first()];
                            }
                        }
//                        $type_cource[$key] = $type->modules();
                    }
                }
            }

//            $first_courses = school(true)->courses()
//                ->whereIn('term', [$term, 4])
//                ->where('category', 'GENERAL SUBJECT')
//                ->where('status', '!=', 'deleted')
//                ->where('level_id', $student[0]['level_id'])
//                ->orderBy('term', 'asc')->get();
//
//            $second_courses = school(true)->courses()
//                ->where('category', 'CORE SUBJECT')
//                ->whereIn('term', [$term, 4])
//                ->where('status', '!=', 'deleted')
//                ->where('level_id', $student[0]['level_id'])
//                ->orderBy('term', 'asc')
//                ->get();

        }

        $level_info = school(true)->levels()->where('id', $student[0]['level_id'])->first();
        $class_teacher = school(true)->staffs()->where('id', $level_info['staff_info_id'])->first();
        $director = school(true)->staffs()->where('privilege', 2)->get();


        if ($term == 4) {

            if ($download == 1) {
                return view('school.marks.yearReportViewDownload', [
                    'students' => $student,
                    'acad_year' => $acad_year,
                    'term' => $term,
                    'type_courses' => $type_cource,
//                    'second_courses' => $second_courses,
                    'all_students_nber' => $all_students,
                    'position' => $qed,
                    'director' => $director[0],
                    'title' => $student[0]['fname'] . ' ' . $student[0]['lname'] . ' ' . $level_info['department']['qualification']['qualification_title'] . ' Acad Year ' . $acad_year
                ]);
            } else {
                return view('school.marks.yearReportView', [
                    'student' => $student[0],
                    'acad_year' => $acad_year,
                    'term' => $term,
                    'type_courses' => $type_cource,
//                    'second_courses' => $second_courses,
                    'all_students_nber' => $all_students,
                    'position' => $qed,
                    'director' => $director[0]
                ]);
            }

        } else {

            if ($download == 1) {
                return view('school.marks.yearReportViewDownload', [
                    'students' => $student,
                    'acad_year' => $acad_year,
                    'term' => $term,
                    'type_courses' => $type_cource,
//                    'second_courses' => $second_courses,
                    'all_students_nber' => $all_students,
                    'position' => $qed,
                    'class_teacher' => $class_teacher,
                    'title' => $student[0]['fname'] . ' ' . $student[0]['lname'] . ' ' . $level_info['department']['qualification']['qualification_title'] . ' Term ' . $term . ' Acad Year ' . $acad_year, 'director' => $director[0]
                ]);
            } else {

                return view('school.marks.yearReportView', [
                    'student' => $student[0],
                    'acad_year' => $acad_year,
                    'term' => $term,
                    'type_courses' => $type_cource,
//                    'second_courses' => $second_courses,
                    'all_students_nber' => $all_students,
                    'position' => $qed,
                    'class_teacher' => $class_teacher,
                    'director' => isset($director[0]) ? $director[0] : "",
                ]);
            }
        }
    }


    /**
     *
     */

    public function levelReport(Request $request)
    {
        $acad_year = $request->year;
        $term = $request->term;
        $level_id = $request->level_id;

        $students = school(true)->students()
            ->where('level_id', $level_id)
            ->where('status', 'active')
            ->orderBy('fname', 'asc')
            ->orderBy('lname', 'asc')
            ->get();

        $students = $students->each(function ($item, $key) {
            global $request;
            $item->acad_year = $request->year;
            $item->term = $request->term;
        });

        $students = $students->sortByDesc('term_perc');

        if ($term == 4) {

            $moduleType = ModuleType::all();
            $type_cource = [];
            if ($moduleType->count() > 0) {

                foreach ($moduleType as $key => $type) {
                    if ($type->modules()->count() > 0) {
                        foreach ($type->modules as $module) {
                            $m = $module->courses()
                                ->where('school_id', school('id'))
                                ->where('status', 1)
                                ->where('level_id', $level_id);
                            if ($m->count() > 0) {
                                $type_cource[$type->title][] = [$m->get()];
                            }
                        }
//                        $type_cource[$key] = $type->modules();
                    }
                }
            }
//            $first_courses = school(true)->courses()
//                ->where('category', 'GENERAL SUBJECT')
//                ->where('status', 1)
//                ->where('level_id', $level_id)
//                ->get();
//
//            $second_courses = school(true)->courses()
//                ->where('category', 'CORE SUBJECT')
//                ->where('status', 1)
//                ->where('level_id', $level_id)
//                ->get();

        } else {
            $moduleType = ModuleType::all();
            $type_cource = [];
            if ($moduleType->count() > 0) {
                foreach ($moduleType as $key => $type) {
                    if ($type->modules()->count() > 0) {
                        foreach ($type->modules as $module) {
                            $m = $module->courses()
                                ->whereIn('term', [$term, 4])
                                ->where('school_id', school('id'))
                                ->where('status', 1)
                                ->where('level_id', $level_id);
                            if ($m->count() > 0) {
                                $type_cource[$type->title] = [$m->first()];
                            }
                        }
//                        $type_cource[$key] = $type->modules();
                    }
                }
            }
//            $first_courses = school(true)->courses()
//                ->whereIn('term', [$term, 4])
//                ->where('category', 'GENERAL SUBJECT')
//                ->where('status', 1)
//                ->where('level_id', $level_id)
//                ->orderBy('term', 'asc')
//                ->get();
//
//            $second_courses = school(true)->courses()
//                ->where('category', 'CORE SUBJECT')
//                ->whereIn('term', [$term, 4])
//                ->where('status', 1)
//                ->where('level_id', $level_id)
//                ->orderBy('term', 'asc')
//                ->get();
        }

        $level_info = school(true)->levels()->where('id', $level_id)->first();
        $class_teacher = school(true)->staffs()->where('id', $level_info['staff_info_id'])->first();
        $director = school(true)->staffs()->where('privilege', 2)->get();

        if ($term == 4) {
            return view('school.marks.yearReportViewDownload', [
                'students' => $students,
                'acad_year' => $acad_year,
                'term' => $term,
                'type_courses' => $type_cource,
//                    'second_courses' => $second_courses,
                'all_students_nber' => count($students),
                'class_teacher' => $class_teacher,
                'title' => $level_info['rtqf']['level_name'] . ' ' . $level_info['department']['qualification']['qualification_title'] . ' Acad Year ' . $acad_year,
                'director' => isset($director[0]) ? $director[0] : school(true)
            ]);
        } else {
            return view('school.marks.yearReportViewDownload', [
                'students' => $students,
                'acad_year' => $acad_year,
                'term' => $term,
                'type_courses' => $type_cource,
//                    'second_courses' => $second_courses,
                'all_students_nber' => count($students),
                'class_teacher' => $class_teacher,
                'title' => $level_info['rtqf']['level_name'] . ' ' . $level_info['department']['qualification']['qualification_title'] . ' Term ' . $term . ' Acad Year ' . $acad_year,
                'director' => isset($director[0]) ? $director[0] : school(true)
            ]);
        }


    }


}
