<?php

namespace App\Http\Controllers\SchoolAuth;

use App\CostOfTraining;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CostOfTrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->guard('wda')->check())
            $view = 'school.costoftrainings.indexWda';
        else {
            $costoftrainings = school(true)->costoftraining;
            $view = 'school.costoftrainings.index';
        }

        return view($view, compact('costoftrainings'));
    }

    public function getData()
    {
        $datas = DB::table('cost_of_training')
            ->join('accr_schools_information', 'cost_of_training.school_id', '=', 'accr_schools_information.id')
            ->select('cost_of_training.*', 'accr_schools_information.school_name');

        return DataTables::of($datas)
            ->addColumn('total', function ($data) {
                $sum = 0;
                if ($data->cost_of_consumables_per_year)
                    $sum += (double) str_replace(',', "", $data->cost_of_consumables_per_year);
                if ($data->estimated_total_salaries_year)
                    $sum += (double) str_replace(',', "", $data->cost_of_consumables_per_year);
                if ($data->cost_of_consumables_per_year)
                    $sum += (double) str_replace(',', "", $data->cost_of_consumables_per_year);
                if ($data->budget_actually_available_for_salaries_per_year)
                    $sum += (double) str_replace(',', "", $data->cost_of_consumables_per_year);
                if ($data->estimated_operational_budget_required_per_year)
                    $sum += (double) str_replace(',', "", $data->cost_of_consumables_per_year);
                if ($data->operational_funds_available_per_year)
                    $sum += (double) str_replace(',', "", $data->cost_of_consumables_per_year);

                return number_format($sum);
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $costoftraining = null;
        return view('school.costoftrainings.create', compact('costoftraining'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $costoftraining = new CostOfTraining();
        $costoftraining->school_id = school(true)->id;
//        $costoftraining->trades_id = $request->input('trades_id');
        $costoftraining->cost_of_consumables_per_year = $request->input('cost_of_consumables_per_year');
        $costoftraining->estimated_total_salaries_year = $request->input('estimated_total_salaries_year');
        $costoftraining->budget_available_for_consumables = $request->input('budget_available_for_consumables');
        $costoftraining->operational_funds_available_per_year = $request->input('operational_funds_available_per_year');

        $costoftraining->budget_actually_available_for_salaries_per_year = $request->input('budget_actually_available_for_salaries_per_year');
        $costoftraining->estimated_operational_budget_required_per_year = $request->input('estimated_operational_budget_required_per_year');
        $costoftraining->academic_year = $request->input('academic_year');
        $save = $costoftraining->save();

        if ($save)
            return redirect(route('school.costoftrainings.index'))->with(['status' => '1', 'message' => "Cost of training  Added Successfully."]);
        else
            return redirect(route('school.costoftrainings.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Add Cost of training."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $costoftraining = school(true)->costoftraining()
            ->where('id', $id)->first();
        if ($costoftraining)
            return view("school.costoftrainings.create", compact("costoftraining"));
        else
            return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $costoftraining = school(true)->costoftraining()
            ->where('id', $id)->first();
        if ($costoftraining)
            return view("school.costoftrainings.create", compact("costoftraining"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $costoftraining = school(true)->costoftraining()
            ->where('id', $id)->first();
        if (!$costoftraining)
            return abort(419);

        $costoftraining->school_id = school(true)->id;
        $costoftraining->trades_id = $request->input('trades_id');
        $costoftraining->cost_of_consumables_per_year = $request->input('cost_of_consumables_per_year');
        $costoftraining->estimated_total_salaries_year = $request->input('estimated_total_salaries_year');
        $costoftraining->budget_available_for_consumables = $request->input('budget_available_for_consumables');
        $costoftraining->operational_funds_available_per_year = $request->input('operational_funds_available_per_year');
        $costoftraining->academic_year = $request->input('academic_year');
        $save = $costoftraining->save();

        if ($save)
            return redirect(route('school.costoftrainings.index'))->with(['status' => '1', 'message' => "cost of trainings Updated Successfully."]);
        else
            return redirect(route('school.costoftrainings.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update cost of trainings."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
