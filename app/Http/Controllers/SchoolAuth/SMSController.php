<?php

namespace App\Http\Controllers\SchoolAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Stmt\If_;

class SMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outbox = school(true)->outbox()
            ->orderBy('created_at', "DESC")->get();

        return view('school.sms.index', compact('outbox'));
    }

    public function send($to)
    {
        $school = school(true);
        $sending = false;
        $arr = ['s', 'f', 'd', 'l'];
        if (in_array($to, $arr))
            $sending = $to;
        else
            return abort(404);

        return view('school.sms.index', compact('school', 'sending'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school = school(true);
        $number = null;
        $type = "Informing";

        switch ($request->on) {
            case "s":
                $id = $request->student;
                $student = $school->students()->where('id', $id)->first();
                if ($student)
                    $number = getStudentGPhone($student);
                break;
            case "f":
                $id = $request->staff;
                $staff = $school->staffs()->where('id', $id)->first();
                $type = "Staff";
                if ($staff)
                    $number = $staff->phone_number;
                break;
            case "d":
                $id = $request->qualification;
                $students = $school->students()->where('status', "active");
                if ($students->count() > 0) {
                    foreach ($students->get() as $student) {
                        if ($student->level->department->qualification_id == $id) {
                            if (getStudentGPhone($student))
                                $number[] = getStudentGPhone($student);
                        }
                    }
                }
                break;
            case "l":
                $id = $request->level;
                $students = $school->students()->where('status', "active");
                if ($students->count() > 0) {
                    foreach ($students->get() as $student) {
                        if ($student->level_id == $id) {
                            if (getStudentGPhone($student))
                                $number[] = getStudentGPhone($student);
                        }
                    }
                }
                break;
            default:
                return abort(404);
                break;
        }
        $message = $request->message;

        if ($number) {
            if (is_array($number)) {
                foreach ($number as $item) {
                    sendSMS($item, $message, $type);
                }
            } else
                sendSMS($number, $message, $type);

            return redirect()->route('school.sms.index')->with(['status' => '1', "message" => 'Message send successfully']);
        }

        return back()->with(['status' => '3', "message" => 'No numbers find !!']);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if (isset($request->level_id)) {

            $outbox = DB::table('outbox')
                ->join('students', 'outbox.std_reg_no', 'students.reg_no')
                ->where('school_id', school('id'))
                ->where('outbox.acad_year', $request->acad_year)
                ->where('term', $request->term)
                ->where('period', $request->period)
                ->get();

            return view('school.sms.index', ['showOutbox' => true,
                'outbox' => $outbox,
                'level_id' => $request->level_id
            ]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
