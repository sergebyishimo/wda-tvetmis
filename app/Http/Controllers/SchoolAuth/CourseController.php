<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Course;
use App\Department;
use App\Http\Requests\CreateCourseRequest;
use App\Qualification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('school.course.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCourseRequest $request)
    {
        $modules = $request->input('modules');
        $marks = $request->input('max_marks');
        $teacher = $request->input('teacher');
        $level = $request->input('level');
        $term = $request->input('term');
        $response = [];
        foreach ($modules as $key => $value) {
            $cID = getMyCourseInfo($value, $level, $term, false, 'id');
            if ($cID == "") {
                $response[] = Course::create([
                    'school_id' => school(true)->id,
                    'level_id' => $level,
                    'module_id' => $value,
                    'max_point' => $marks[$key],
                    'staffs_info_id' => $teacher[$key],
                    'term' => $term,
                    'updateVersion' => updateVersionColumn('courses')
                ]);
            } else {
                $response[] = $this->update($cID, $marks[$key], $teacher[$key]);
            }
        }

        return redirect()->route('school.course.index')->with(['status' => '1', "message" => 'Course successfully add']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $qualification = Qualification::findOrFail($request->input('qualification'));
        return view('school.course.index', compact('request', 'qualification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($cid, $marks, $teacher)
    {
        $course = Course::find($cid);
        $y = 0;
        if ($course->max_point != $marks) {
            $course->max_point = $marks;
            $y += 1;
        }
        if ($course->staffs_info_id != $teacher) {
            $course->staffs_info_id = $teacher;
            $y += 1;
        }

        if ($y != 0) {
            $course->updateVersion = updateVersionColumn('courses');
            return $course->save();
        }

        return false;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getLevels(Request $request)
    {

        $id = $request->get('id');
        if ($request->has('where') && $request->get('where') == 'id')
            if ($request->has('school_id')) {
                $d = getDepartmentsBySchoolId($request->school_id)->where('id', $id);
            } else {
                $d = getDepartments()->where('id', $id);
            }
        else
            $d = getDepartments()->where('qualification_id', $id);

        if ($d->count() > 0) {
            $l = [];
            foreach ($d->get() as $dd) {
                if ($dd->levels->count() > 0) {
                    foreach ($dd->levels as $level) {
                        $l[] = [
                            'id' => $level->id,
                            'name' => $level->rtqf->level_name
                        ];
                    }
                }
            }
            return json_encode($l);
        }
        return json_encode([]);
    }
}
