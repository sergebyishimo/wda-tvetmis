<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateSchoolAdmissionRequest;
use App\Http\Requests\UpdateSchoolAdmissionRequest;
use App\SchoolAdmission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolAdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schooladmission = school(true)->admissionrequirement;
        return view('school.admission.index', compact('schooladmission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schooladmission = null;
        return view('school.admission.create', compact('schooladmission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSchoolAdmissionRequest $request)
    {
        $filePath = 'admissionsrequirements';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachment')) {
            $fileR = $request->file('attachment');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id . Date('Y_m_d_H_m_s') . '.' . $ext, config('voyager.storage.disk'));
        }

        $schooladmission = new SchoolAdmission();
        $schooladmission->school_id = school(true)->id;
        $schooladmission->admission_requirements = $request->input('admission_requirements');
        $schooladmission->attachment = $file;
        $save = $schooladmission->save();
        if ($save)
            return redirect(route('school.admissions.index'))->with(['status' => '1', 'message' => "Admission Requirement Added Successfully."]);
        else
            return redirect(route('school.admissions.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Add Admission Requirements."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schooladmission = school(true)->admissionrequirement()
            ->where('id', $id)->first();
        if ($schooladmission)
            return view("school.admission.create", compact("schooladmission"));
        else
            return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schooladmission = school(true)->admissionrequirement()
            ->where('id', $id)->first();
        if ($schooladmission)
            return view("school.admission.create", compact("schooladmission"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSchoolAdmissionRequest $request, $id)
    {
        $schooladmission = school(true)->admissionrequirement()
            ->where('id', $id)->first();
        if (!$schooladmission)
            return abort(419);


        $filePath = 'admissionsrequirements';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachment')) {
            $fileR = $request->file('attachment');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id . Date('Y_m_d_H_m_s') . '.' . $ext, config('voyager.storage.disk'));
        }
        $schooladmission->school_id = school(true)->id;
        $schooladmission->admission_requirements = $request->input('admission_requirements');
        if ($request->has('attachment')) {
            $schooladmission->attachment = $file;
        }
        $save = $schooladmission->save();

        if ($save)
            return redirect(route('school.admissions.index'))->with(['status' => '1', 'message' => "Admission  Requirements Updated Successfully."]);
        else
            return redirect(route('school.admissions.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Admission Requirement."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schooladmission = school(true)->admissionrequirement()->where('id', $id)->first();
        if (!$schooladmission)
            return abort(419);
        $schooladmission->delete();

        return redirect(route('school.admissions.index'));
    }
}
