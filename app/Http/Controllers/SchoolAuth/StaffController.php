<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Attachment;
use App\Http\Requests\UpdateMyInfoRequest;
use App\Http\Requests\UpdateStaffRequest;
use App\Mail\SendAssessorSubmittionMail;
use App\Mail\SendCreatedStaffInfo;
use App\Mail\SendMarkingSubmittionMail;
use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\TrainingSectorsSource;
use App\Model\Accr\TrainingTradesSource;
use App\Module;
use App\Qualification;
use App\Rp\Country;
use App\SchoolUser;
use App\StaffAssessorApplication;
use App\StaffAssessorBackground;
use App\StaffComputerSkills;
use App\StaffIap;
use App\StaffLanguage;
use App\StaffMarkerApplication;
use App\StaffMarkingBackground;
use App\StaffMedicalHistory;
use App\StaffProfileAttachmentsFinal;
use App\StaffQualification;
use App\StaffRttiDetails;
use App\StaffRttiTrainingReference;
use App\StaffRttiTrainingReferenceSource;
use App\StaffsInfo;
use App\StaffTeachingExperience;
use App\StaffWorkexperience;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStaffRequest;
use Mail;
use App\TrainersModulesTaught;
use Illuminate\Support\Facades\Gate;

class StaffController extends Controller
{
    protected $guard = "school";

    function __construct()
    {
        $this->middleware("school");
    }

    protected function guard()
    {
        return $this->guard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('isSchoolManager') || Gate::allows('isTeachOrStaff')){
            $settings = school(true)->settings;
            $staffs = school(true)->staffs;

            return view('school.staff.index', compact('settings', 'staffs'));
        }else{
            abort(404,'error page');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $staff = college([]);
        $departments = school(true)->departments();
        if ($departments->count() > 0)
            $departments = $departments->get()->pluck('qualification_id')->toArray();
        else
            $departments = [];

        $courses = Module::whereIn('qualification_id', $departments);
        if ($courses->count() > 0)
            $courses = $courses->get()->pluck('module_title', 'id');
        else
            $courses = college([]);

        $sectors = TrainingSectorsSource::all();
        $currs = CurriculumQualification::all();
        $sub_sectors = TrainingTradesSource::all();

        return view('school.staff.create', compact('staff', 'courses', 'sectors', 'currs', 'sub_sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateStaffRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStaffRequest $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $staff = new StaffsInfo();

        $staff->school_id = school(true)->id;
        $staff->first_name = $request->input('first_name');
        $staff->last_name = $request->input("last_name");
        $staff->gender = $request->input("gender");
        $staff->privilege = $request->input('privilege');
        $staff->phone_number = $request->input('phone_number');
        $staff->email = $request->input('email');
        $staff->attended_rtti = $request->input('attended_rtti');
        $staff->staff_category = $request->input('staff_category');

        $staff->sector_taught = $request->r_i;
        $staff->sub_sector_taught = $request->d_d;



        $staff->updateVersion = updateVersionColumn('staffs_info');
        $photo = null;
        $imagePath = 'staffs/photo';

        $name = sha1(time().$imagePath);
        if ($request->has('image')) {
            $photoR = $request->file('image');
            $ext = $photoR->getClientOriginalExtension();
            $photo = $photoR->storeAs($imagePath, $name . '.' . $ext, config('voyager.storage.disk'));
        }
        $staff->photo = $photo;
        $staff->save();
        if ($request->has('modules_taught')) {
            $check = TrainersModulesTaught::where('staff_id', $staff->id)->delete();
            for ($i = 0; $i < count($request->input('modules_taught')); $i++) {
                $course = Module::find($request->modules_taught[$i]);
                if ($course) {
                    // $staff->modules_taught = $request->modules_taught[$i];
                    TrainersModulesTaught::create(
                        [
                            'staff_id' => $staff->id,
                            'sector_id' => $course->qualification->subSector->sector_id,
                            'sub_sector_id' => $course->qualification->subSector->id,
                            'qualification_id' => $course->qualification_id,
                            'module_id' => $course->id
                        ]
                    );

                }
            }

        }
        if ($staff->id) {

            \DB::table('staff_privileges')->insert([
                'school_id' => school(true)->id,
                'staff_info_id' => $staff->id,
                'school_user_privilege_id' => $request->input('privilege'),
                'authorized' => true
            ]);

            $allowedStuff = ['1', '2', '3', '4', '5', '6', '7'];

            if (in_array($request->input('privilege'), $allowedStuff)) {
                SchoolUser::create([
                    'name' => trim($request->input('first_name') . " " . $request->input('last_name')),
                    'email' => trim($request->input('email')),
                    'password' => bcrypt(trim($request->input('phone_number'))),
                    'android_pass' => sha1(trim($request->input('phone_number'))),
                    'privilege' => $request->input('privilege'),
                    'school_id' => school(true)->id,
                    'updateVersion' => updateVersionColumn('school_users')
                ]);

                $data = [
                    'name' => trim($request->input('first_name') . " " . $request->input('last_name')),
                    'password' => $request->input('phone_number'),
                    'email' => trim($request->input('email')),
                    'school' => school(true)->name
                ];

                Mail::to(trim($request->input('email')))->send(new SendCreatedStaffInfo($data));
                $message = "Welcome to " . school('acronym') . " your 'll be using your number as password (" . $request->input('phone_number') . ").";
                sendSMS($request->input('phone_number'), $message, "New Staff");
            }


            return redirect()->route('school.staff.index')
                ->with(['status' => '1', "message" => "Successfully Added to this school.<br>Check email for farther info ..."]);

        } else
            return redirect()->back()->withInput()->with(['status' => '0', "message" => "Creating new staff failed !!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $settings = school(true)->settings;
        return view('school.staff.index', compact('settings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = school(true)->staffs()->where('id', $id)->first();
        if (!$staff)
            return abort(419);

        $departments = school(true)->departments();
        if ($departments->count() > 0)
            $departments = $departments->get()->pluck('qualification_id')->toArray();
        else
            $departments = [];

        $courses = Module::whereIn('qualification_id', $departments);
        if ($courses->count() > 0)
            // $courses = $courses->get()->pluck('module_title', 'id');
            $courses = $courses->get();

        else
            $courses = [];
        $sectors = TrainingSectorsSource::all();
        $currs = CurriculumQualification::all();
        $sub_sectors = TrainingTradesSource::all();

        return view('school.staff.create', compact('staff', 'courses', 'sectors', 'currs', 'sub_sectors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStaffRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateStaffRequest $request, $id)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $staff = school(true)->staffs()->where('id', $id)->first();
        if (!$staff)
            return abort(419);

        if ($staff->email != $request->email)
            $this->validate($request, [
                'email' => 'unique:staffs_info|unique:school_users'
            ]);

        if (validSMSNumber($staff->phone_number) != validSMSNumber($request->phone_number))
            $this->validate($request, [
                'phone_number' => 'unique:staffs_info,phone_number'
            ]);

        $tmpEmail = $staff->email;

        $staff->first_name = $request->input('first_name');
        $staff->last_name = $request->input("last_name");
        $staff->gender = $request->input("gender");
        $staff->privilege = $request->input('privilege');
        $staff->phone_number = $request->input('phone_number');
        $staff->email = $request->input('email');
        $staff->attended_rtti = $request->input('attended_rtti');
        $staff->staff_category = $request->input('staff_category');
        $staff->updateVersion = updateVersionColumn('staffs_info');
        $imagePath = 'staffs/photo';
        $name = $staff->id;
        $photo = $staff->photo;
        if ($request->has('image')) {
            $photoR = $request->file('image');
            $ext = $photoR->getClientOriginalExtension();
            $photo = $photoR->storeAs($imagePath, $name . '.' . $ext, config('voyager.storage.disk'));
        }
        $staff->photo = $photo;
        if ($request->has('modules_taught')) {
            $check = TrainersModulesTaught::where('staff_id', $staff->id)->delete();
            for ($i = 0; $i < count($request->input('modules_taught')); $i++) {
                $course = Module::find($request->modules_taught[$i]);
                if ($course) {
                    // $staff->modules_taught = $request->modules_taught[$i];

                    TrainersModulesTaught::create(
                        [
                            'staff_id' => $staff->id,
                            'sector_id' => $course->qualification->subSector->sector_id,
                            'sub_sector_id' => $course->qualification->subSector->id,
                            'qualification_id' => $course->qualification_id,
                            'module_id' => $course->id
                        ]
                    );

                }
            }

        }

        if ($staff->save()) {
            $staffU = school(true)->schoolUser()->where('email', $tmpEmail);
            if ($staffU->count() > 0) {
                $staffUser = $staffU->first();
                if ($staffUser->email != $request->input('email'))
                    $staffUser->email = $request->input('email');
                $staffUser->privilege = $request->input('privilege');
                $staffUser->updateVersion = updateVersionColumn('school_users');
                $staffUser->name = trim($request->input('first_name') . " " . $request->input('last_name'));

                $message = "Your information has been recently updated. From " . strtoupper(school('acronym'));
                if ($staffUser->save())
                    sendSMS($request->input('phone_number'), $message, "Update Staff");

            }

            return redirect()->route('school.staff.index')
                ->with(['status' => '1', "message" => "Successfully Updated."]);

        } else
            return redirect()->back()->withInput()->with(['status' => '0', "message" => "Updating failed !!"]);

    }

    /**
     * @param UpdateMyInfoRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|void
     */

    public function updateInfo(UpdateMyInfoRequest $request)
    {
        $id = $request->id;
        $staff = school(true)->staffs()->where('id', $id)->first();
        if (!$staff)
            return abort(419);

        $tmpEmail = $staff->email;

        if ($staff->email != $request->email) {
            $checkInSchoolUsers = school(true)->schoolUsers()->where('email', $request->email)->count();
            if ($checkInSchoolUsers > 0)
                return back()->withInput()->with(['status' => '0', 'message' => 'Email already exists, try another']);
        }

        $imagePath = 'staffs/photo';
        $name = $staff->id;
        $photo = $staff->photo;

        if ($request->has('image')) {
            $photoR = $request->file('image');
            $ext = $photoR->getClientOriginalExtension();
            $photo = $photoR->storeAs($imagePath, $name . '.' . $ext, config('voyager.storage.disk'));
        }

        if ($request->has('academic_document')) {
            $fileA = $request->file('academic_document');
            $ext = $fileA->getClientOriginalExtension();
            $fileA->storeAs("staffs/documents/academic", $name . '.' . $ext, config('voyager.storage.disk'));
        }

        if ($request->has('certificate_document')) {
            $fileC = $request->file('certificate_document');
            $ext = $fileC->getClientOriginalExtension();
            $fileC->storeAs("staffs/documents/others", $name . '.' . $ext, config('voyager.storage.disk'));
        }

        $staff->first_name = $request->input('first_name');
        $staff->last_name = $request->input("last_name");
        $staff->gender = $request->input("gender");
        $staff->email = $request->input('email');
        $staff->civil_status = $request->input('civil_status');
        $staff->photo = $photo;
        $staff->phone_number = $request->input('phone_number');
        $staff->nationality = $request->input('nationality');
        $staff->national_id_number = $request->input('national_id_number');
        $staff->province = $request->input('Province');
        $staff->district = $request->input('District');
        $staff->sector = $request->input('Sector');
        $staff->bank_name = $request->input('bank_name');
        $staff->bank_account_number = $request->input('bank_account_number');
        $staff->rssb_number = $request->input('rssb_number');
        //New Ones
        $staff->qualification = $request->input('qualification');
        $staff->institution = $request->input('institution');
        $staff->graduated_year = $request->input('graduated_year');
        $staff->attended_rtti = $request->input('attended_rtti') or "";
        $staff->updateVersion = updateVersionColumn('staffs_info');

        if ($staff->save()) {
            $staffU = school(true)->schoolUser()->where('email', $tmpEmail);
            if ($staffU->count() > 0) {
                $staffUser = $staffU->first();
                if ($staffUser->email != $request->input('email'))
                    $staffUser->email = $request->input('email');
                $staffUser->updateVersion = updateVersionColumn('school_users');
                $staffUser->name = trim($request->input('first_name') . " " . $request->input('last_name'));

                $message = "Your information has been recently updated. From " . strtoupper(school('acronym'));
                if ($staffUser->save())
                    sendSMS($request->input('phone_number'), $message, "Update Staff");

            }

            return back()->with(['status' => '1', "message" => "Successfully Updated."]);

        } else
            return redirect()->back()->withInput()->with(['status' => '0', "message" => "Updating failed !!"]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $staff = school(true)->staffs()->where('id', $id)->first();
        if (!$staff)
            return abort(419);

        $email = $staff->email;
        $kop = $staff;
        $staff->status = "deleted";
        $staff->updateVersion = updateVersionColumn('staffs_info');

        if ($staff->save()) {
            $kop->delete();
            $staffU = school(true)->schoolUser()->where('email', $email);
            if ($staffU->count() > 0) {
                $staffUser = $staffU->first();
                if ($staffUser->privilege != 2)
                    $staffUser->delete();
            }
        }

        return redirect()->route('school.staff.index')->with(['status' => '1', "message" => 'Deleted successfully .']);
    }

    public function myInfo()
    {
        if (isAllowed(1) || isAllowed())
            abort(419);

        $staff = school(true)->staffs()->where("email", auth()->user()->email)->first();
        $countries = Country::all()->pluck('country_name', 'country_name');

        return view("school.staff.my-info", compact("staff", "countries"));
    }

    public function profile(Request $request)
    {
        $school = school(true);
        if ($request->has("profile")) {

            $staff = school(true)->staffs()->where("id", $request->profile)->first();
        } else {
            $staff = school(true)->staffs()->where("email", auth()->user()->email)->first();
        }

        return view("school.staff.profile", compact("staff", "school"));
    }

    public function attachment()
    {
        $attachementDesc = getAttachmentDescriptionForSelect();
        $myAttachement = getStaff('attachement');

        return view('school.staff.attachments', compact('myAttachement', 'attachementDesc'));
    }

    public function postAttachment(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:pdf,jpeg,jpg,png',
            'description' => 'required'
        ]);

        $path = 'staffs/attachments';
        $name = rand(1, 10000) . '_' . time() . '_' . trim(str_replace(' ', "_", strtolower(getStaff('names'))));
        $file = null;

        $fileR = $request->file('file');
        $ext = $fileR->getClientOriginalExtension();
        $file = $fileR->storeAs($path, $name . '.' . $ext, config('voyager.storage.disk'));

        $att = new StaffProfileAttachmentsFinal();
        $att->school_id = getStaff('school_id');
        $att->staff_id = getStaff('id');
        $att->attachment_description = $request->description;
        $att->attachment = $file;

        $att->save();

        return back()->with(['status' => '1', 'message' => 'Uploaded Successfully.']);
    }

    public function markerApplication()
    {
        $currentMarker = getStaff('currentMarking');
        return view("school.staff.application.marker", compact('currentMarker'));
    }

    public function storeMarkerApplication(Request $request)
    {
        $this->validate($request, [
            'education_program_id' => 'required',
            'trade_marked_before' => 'required',
            'first_subject_applied_for' => 'required',
            'first_subject_marked_before' => 'required',
            'second_subject_applied_for' => 'required_with:third_subject_applied_for'
        ]);

        $data = $request->except(['Submit']);
        $save = StaffMarkerApplication::create($data);

        if ($save) {
            $data = [
                'names' => getStaff('names'),
                'school' => getStaff('schoolName'),
                'academic_year' => $request->academic_year,
                'trade_applied' => getSelectTradeMarker($request->education_program_id),
                'assessed_before' => getYesNo($request->trade_marked_before),
                'first_subject' => $request->first_subject_applied_for,
                'first_subject_before' => getYesNo($request->first_subject_marked_before),
            ];

            if ($request->has('second_subject_applied_for')) {
                $data['second_subject'] = $request->second_subject_applied_for;
                $data['second_subject_before'] = getYesNo($request->second_subject_marked_before);
            }

            if ($request->has('third_subject_applied_for')) {
                $data['third_subject'] = $request->third_subject_applied_for;
                $data['third_subject_before'] = getYesNo($request->third_subject_marked_before);
            }

            Mail::to(getStaff('email'))->send(new SendMarkingSubmittionMail($data));

            return back()->with(['status' => '1', 'message' => 'Application Submitted Successfully !!!']);
        }

        return back()->with(['status' => '0', 'message' => 'Application failed ...']);

    }

    public function assessorApplication()
    {
        $currentAssessor = getStaff('currentAssessor');
        return view("school.staff.application.assessor", compact('currentAssessor'));
    }

    public function storeAssessorApplication(Request $request)
    {
        $this->validate($request, [
            'trade' => 'required',
            'trade_assessed_before' => 'required',
            'academic_year' => 'required',
            'school_id' => 'required',
            'staff_id' => 'required'
        ]);

        $data = $request->except(['Submit']);
        $save = StaffAssessorApplication::create($data);
        if ($save) {
            $data = [
                'names' => getStaff('names'),
                'school' => getStaff('schoolName'),
                'academic_year' => $request->academic_year,
                'trade' => getSelectTradeMarker($request->trade),
                'trade_assessed_before' => getYesNo($request->trade_assessed_before)
            ];

            Mail::to(getStaff('email'))->send(new SendAssessorSubmittionMail($data));

            return back()->with(['status' => '1', 'message' => 'Application Submitted Successfully !!!']);
        }

        return back()->with(['status' => '0', 'message' => 'Application failed ...']);
    }

    public function cancelApplication(Request $request)
    {
        $school = $request->school_id;
        $staff = $request->staff_id;
        $year = $request->academic_year;
        $where = $request->where;

        if ($where == 'm') {
            $as = StaffMarkerApplication::where('school_id', $school)
                ->where('staff_id', $staff)
                ->where('academic_year', $year);

            if ($as->count() > 0)
                $as->first()->delete();
        }

        if ($where == 'a') {
            $as = StaffAssessorApplication::where('school_id', $school)
                ->where('staff_id', $staff)
                ->where('academic_year', $year);

            if ($as->count() > 0)
                $as->first()->delete();
        }

        return back()->with(['status' => '1', 'message' => 'Application Canceled Successfully !!']);
    }

    public function workingExperience()
    {
        $backgrounds = getStaff("workingExperience");
        return view('school.staff.application.working-experience', compact('backgrounds'));
    }

    public function postWorkingExperience(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255|min:3',
            'institution' => 'required|min:3|max:255',
            'status' => 'required|integer',
            'responsibility' => 'required|min:3|max:255',
            'period_from' => 'required|before:today',
            'period_to' => 'required|after:period_from'
        ]);

        $data = $request->except(['_token']);

        $save = StaffWorkexperience::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => 'Working experience added successfully.']);

        return back()->with(['status' => '0', 'message' => 'Adding experience failed.']);
    }

    public function teachingExperience()
    {
        $backgrounds = getStaff("teachingExperience");
        return view('school.staff.application.teaching-experience', compact('backgrounds'));
    }

    public function postTeachingExperience(Request $request)
    {
        $this->validate($request, [
            'institution_taught' => 'required|max:255|min:3',
            'subject_taught' => 'required|min:3|max:255',
            'tvet_sub_field' => 'required',
            'class_or_level_taught' => 'required|integer',
            'from_date' => 'required|before:today',
            'to_date' => 'required|after:from_date'
        ]);

        $data = $request->except(['_token']);

        $save = StaffTeachingExperience::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => 'Teaching experience added successfully.']);

        return back()->with(['status' => '0', 'message' => 'Adding experience failed.']);
    }

    public function markingBackground()
    {
        $backgrounds = getStaff("markingBackgrounds");
        return view('school.staff.application.marking-background', compact('backgrounds'));
    }

    public function postMarkingBackground(Request $request)
    {
        $this->validate($request, [
            'academic_year' => 'required',
            'marking_position' => 'required|integer',
            'marking_institution' => 'required',
            'subject_exam_marked' => 'required|integer',
            'education_program' => 'required|integer',
            'marking_center' => 'required|min:4|max:255'
        ]);

        $data = $request->except(['_token']);

        $save = StaffMarkingBackground::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => 'Marking background added successfully.']);

        return back()->with(['status' => '0', 'message' => 'Adding background failed.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assessorBackground()
    {
        $backgrounds = getStaff("assessorBackgrounds");
        return view('school.staff.application.assessor-background', compact('backgrounds'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAssessorBackground(Request $request)
    {
        $this->validate($request, [
            'academic_year' => 'required',
            'education_program_assessed' => 'required|integer',
            'assessement_center' => 'required|min:4|max:255'
        ]);

        $data = $request->except(['_token']);

        $save = StaffAssessorBackground::create($data);

        if ($save)
            return back()->with(['status' => '1', 'message' => 'Assessor background added successfully.']);

        return back()->with(['status' => '0', 'message' => 'Adding background failed.']);
    }

    /**
     * @param $id
     * @param $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyBackgrounds($id, $model)
    {
        switch ($model) {
            case 'ab':
                $model = StaffAssessorBackground::findOrFail($id);
                break;
            case 'mb':
                $model = StaffMarkingBackground::findOrFail($id);
                break;
            case 'te':
                $model = StaffTeachingExperience::findOrFail($id);
                break;
            case 'we':
                $model = StaffWorkexperience::findOrFail($id);
                break;
            case "atc":
                $model = StaffProfileAttachmentsFinal::findOrFail($id);
                break;
            case "qua":
                $model = StaffQualification::findOrFail($id);
                break;
            case "inter":
                $model = StaffIap::findOrFail($id);
                break;
            default:
                return back();
                break;
        }
        $model->delete();
        return back()->with(['status' => '1', 'message' => 'Deleted successfully !!']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function familyInfo(Request $request)
    {
        if ($request->input('_token')) {
            $id = getStaff('id');
            $model = StaffRttiDetails::where('staff_id', $id)->first();
            if (is_null($model)) {
                $model = new StaffRttiDetails();
                $model->staff_id = $id;
            }

            $model->fathers_names = $request->fathers_names;
            $model->mothers_names = $request->mothers_names;
            $model->family_province = $request->family_province;
            $model->family_district = $request->family_district;
            $model->family_sector = $request->family_sector;
            $model->number_brothers = $request->number_brothers;
            $model->number_sisters = $request->number_sisters;
            $model->about_family = $request->about_family;
            $model->your_family_responsibilities = $request->your_family_responsibilities;
            $model->your_family_responsibilities = $request->your_family_responsibilities;

            if (getStaff('civil_status') != 'single') {
                $model->spouse_names = $request->spouse_names;
                $model->number_of_children = $request->number_of_children;
                if (is_array($request->age_range_children))
                    $ranges = implode(',', $request->age_range_children);
                else
                    $ranges = $request->age_range_children;

                $model->age_range_children = $ranges;
                $model->whom_you_stay_with = $request->whom_you_stay_with;
            }

            $model->save();

            redirect()->back()->with(['status' => '1', 'message' => 'Saved Successfully']);
        }

        return view("school.staff.application.family");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function staffQualification(Request $request)
    {
        $id = getStaff('id');
        if ($request->input('_token')) {

            $model = new StaffQualification();
            $model->school = getStaff('school_id');
            $model->staff_id = $id;
            $model->qualification_name = $request->qualification_name;
            $model->institution_of_study = $request->institution_of_study;
            $model->date_of_award = $request->date_of_award;
            $model->qualification_level = $request->qualification_level;
            $model->save();

            return back()->with(['status' => '1', 'message' => 'Saved Successfully.']);
        }

        $qualifications = StaffQualification::where("staff_id", $id)->get();
        return view('school.staff.application.qualification', compact('qualifications'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function languageAndComputer(Request $request)
    {
        if ($request->input('_token')) {
            $id = getStaff('id');
            if ($request->input('type') == "comp") {
                $model = StaffComputerSkills::where('staff_id', $id)->first();
                if (is_null($model)) {
                    $model = new StaffComputerSkills();
                    $model->staff_id = $id;
                }

                $model->internet = $request->internet;
                $model->word = $request->word;
                $model->excel = $request->excel;
                $model->powerpoint = $request->powerpoint;

            } elseif ($request->input('type') == "lang") {
                $model = StaffLanguage::where('staff_id', $id)->first();
                if (is_null($model)) {
                    $model = new StaffLanguage();
                    $model->staff_id = $id;
                }
                $dModel = StaffRttiDetails::where('staff_id', $id)->first();
                if (!is_null($dModel)) {
                    $dModel->language_spoken = $request->language_spoken;
                    $dModel->language_prefered = $request->language_prefered;
                    $dModel->save();
                }
                //English
                $model->english_r = $request->english_r;
                $model->english_s = $request->english_s;
                $model->english_w = $request->english_w;
                //French
                $model->french_r = $request->french_r;
                $model->french_s = $request->french_s;
                $model->french_w = $request->french_w;
                //Kinyarwanda
                $model->kinyarwanda_r = $request->kinyarwanda_r;
                $model->kinyarwanda_s = $request->kinyarwanda_s;
                $model->kinyarwanda_w = $request->kinyarwanda_w;
                //Swahili
                $model->swahili_r = $request->swahili_r;
                $model->swahili_s = $request->swahili_s;
                $model->swahili_w = $request->swahili_w;
            }

            $model->save();

            return back()->with(['status' => '1', 'message' => 'Saved Successfully !']);
        }
        return view('school.staff.application.lang-comp');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function internshipProgram(Request $request)
    {
        $id = getStaff('id');
        if ($request->input('_token')) {

            $model = new StaffIap();
            $model->staff_id = $id;
            $model->company_name = $request->company_name;
            $model->from_date = $request->from_date;
            $model->to_date = $request->to_date;
            $model->place = $request->place;
            $model->days = $request->days;

            $model->save();

            return back()->with(['status' => '1', 'message' => 'Saved Successfully.']);
        }
        $interns = StaffIap::where("staff_id", $id)->get();

        return view('school.staff.application.intern', compact('interns'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function staffMedical(Request $request)
    {
        if ($request->input('_token')) {
            $id = getStaff('id');
            $model = StaffMedicalHistory::where('staff_id', $id)->first();
            if (is_null($model)) {
                $model = new StaffMedicalHistory();
                $model->staff_id = $id;
            }
            $model->allergies = $request->allergies;
            $model->disabilities_or_special_needs = $request->disabilities_or_special_needs;
            $model->last_date_hospitalized = $request->last_date_hospitalized;
            $model->last_time_sick = $request->last_time_sick;
            $model->cause_of_sickness_latest_time = "sickness";
            $model->medical_history_background = $request->medical_history_background;
            $model->detailed_medical_information_or_dr_Instructions = $request->detailed_medical_information_or_dr_Instructions;

            $model->save();

            return redirect()->back()->with(['status' => '1', 'message' => 'Saved Successfully']);
        }
        return view('school.staff.application.medical');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trainingReference(Request $request)
    {
        if ($request->input('_token')) {
            $id = getStaff('id');
            if (is_array($request->response)) {
                foreach ($request->response as $key => $response) {
                    if (strlen(trim($response)) > 0) {
                        $model = StaffRttiTrainingReference::where('staff_id', $id)
                            ->where('training_reference_id', $key)
                            ->first();
                        if (is_null($model)) {
                            $model = new StaffRttiTrainingReference();
                            $model->staff_id = $id;
                        }
                        $model->training_reference_id = $key;
                        $model->response = $response;
                        $model->save();
                    }
                }
            }
            return back()->with(['status' => '1', 'message' => 'Saved Successfully']);
        }
        $traings = StaffRttiTrainingReferenceSource::all();
        return view('school.staff.application.training', compact('traings'));
    }

}
