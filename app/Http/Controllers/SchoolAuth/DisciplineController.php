<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Discipline;
use App\DisciplineHistory;
use App\Http\Requests\StoreDisciplineInfoRequest;
use App\Level;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DisciplineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('school.discipline.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sms = [];
        if (is_array($request->input('student'))) {
            foreach ($request->input('student') as $key => $value) {
                $r = [
                    'student' => $value,
                    'marks' => $request->input('marks'),
                    'fault' => $request->input('fault'),
                    'comment' => $request->input('comment'),
                    'send_sms' => $request->input('send_sms')[$value]
                ];
                $sms[] = $this->savingStdMarks($r);
            }
        } else {
            $r = [
                'student' => $request->input('student'),
                'marks' => $request->input('marks'),
                'fault' => $request->input('fault'),
                'comment' => $request->input('comment'),
                'send_sms' => $request->input('send_sms')
            ];
            $sms[] = $this->savingStdMarks($r);
        }

        foreach ($sms as $key => $value) {
            $std = Student::find($value['student']);
            if ($value['sms'] == true) {
                $message = "Mubyeyi dufatanyije kurera umwana wawe yakoze irikosa " . $value['fault'] . " akurwaho amanota" . $value['marks'];
                $number = getStudentGPhone($std);
                if ($number != null)
                    sendSMS($number, $message, 'discipline');
            }
        }

        return redirect()->route('school.discipline.index')->with(['status' => '1', 'message' => 'Successfully reduced']);

    }

    public function savingStdMarks($r)
    {
        $term = school(true)->settings->active_term;
        $year = date('Y');
        $student = $r['student'];
        $rMarks = $r['marks'];
        $fault = $r['fault'];
        $comment = $r['comment'];
        $sms = $r['send_sms'] ? true : false;

        $cm = school('discipline_max') ?: 40;
        $check = school(true)->discipline()
            ->where('student_id', $student)
            ->where('acad_year', $year)
            ->where('term', $term);


        if ($check->count() > 0)
            $cm = $check->first()->marks;

        if ($cm > $rMarks)
            $uc = $cm - $rMarks;
        else
            $uc = $cm;

//        dd($uc);

        if ($check->count() > 0) {
            $dis = $check->first();
            $dis->marks = $uc;
            $dis->save();
        } else {
            Discipline::create([
                'school_id' => school('id'),
                'student_id' => $student,
                'term' => $term,
                'acad_year' => $year,
                'marks' => $uc
            ]);
        }

        DisciplineHistory::create([
            'school_id' => school('id'),
            'student_id' => $student,
            'term' => $term,
            'acad_year' => $year,
            'marks_had' => $cm,
            'marks' => $rMarks,
            'fault' => $fault,
            'comment' => $comment,
            'sms' => $sms,
            'done_by' => auth()->user()->name
        ]);

        return ['student' => $student, 'sms' => $sms, 'fault' => $fault, 'message' => $comment, 'marks' => $rMarks];
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $action = $request->input('action');
        $based = $request->input('based');
        $term = $request->input('term');
        $year = $request->input('acad_year');

        switch ($action) {
            case 'c':
                $level = $request->input('level');
                $students = school(true)->students()->where('status', "active")
                    ->where('level_id', $level)
                    ->orderBy("fname", "ASC")
                    ->orderBy("lname", "ASC");
                $level = Level::findOrFail($level);

                return view('school.discipline.reducing', compact('students', 'based', 'term', 'year', 'level'));
                break;
            case 'h':
                $level = $request->input('level');
                $students = school(true)->students()->where('status', "active")
                    ->where('level_id', $level)
                    ->orderBy("fname", "ASC")
                    ->orderBy("lname", "ASC");
                $level = Level::findOrFail($level);
                return view('school.discipline.history', compact('students', 'based', 'term', 'year', 'level'));
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
