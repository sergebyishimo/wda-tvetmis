<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Mail\SendMailAccredition;
use App\Model\Accr\AccrApplication;
use App\Model\Accr\AccrAttachment;
use App\Model\Accr\AccrAttachmentsSource;
use App\Model\Accr\AccrBuildingPlotInfrastructure;
use App\Model\Accr\AccrBuildingsAndPlots;
use App\Model\Accr\AccrCriteriaAnswer;
use App\Model\Accr\AccrCriteriaSection;
use App\Model\Accr\AccreditationType;
use App\Model\Accr\AccrQualificationsApplied;
use App\Model\Accr\CurriculumQualification;
use App\District;
use App\Model\Accr\Functions;
use App\Model\Accr\Graduate;
use App\Model\Accr\InfrastructuresSource;
use App\Model\Accr\ProgramsOffered;
use App\Model\Accr\QualificationLevel;
use App\Model\Accr\RtqfSource;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SchoolSelfAssessimentStatus;
use App\Model\Accr\StaffAdministrator;
use App\Model\Accr\Trainer;
use App\Model\Accr\TrainingSectorsSource;
use App\Model\Accr\TrainingTradesSource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\StaffsInfo;
use App\Department;

class AccrController extends Controller
{
    function __construct()
    {
        $this->middleware('school');
    }

    public function save(Request $request)
    {

        if (isset($request->form)) {

            $step = $request->input('step');

            if ($request->form == 'attachments' || $request->form == 'apply') {

                $check_app = AccrApplication::where('school_id', auth()->user()->school_id)->where('status', 'In Process')->first();
                if ($check_app) {
                    $application_id = $check_app->id;
                } else {
                    $application = new AccrApplication();

                    $application->school_id = auth()->user()->school_id;
                    $application->status = 'In Process';

                    $application->save();

                    $application_id = $application->id;
                }
            }


            switch ($request->form) {
                case 'school_info':

                    SchoolInformation::where('id', auth()->user()->school_id)->update(
                        [
                            'school_name' => $request->name,
                            'province' => $request->province,
                            'district' => $request->district,
                            'sector' => $request->sector,
                            'cell' => $request->cell,
                            'village' => $request->village,
                            'latitude' => $request->latitude,
                            'longitude' => $request->longitude,
                            'phone' => $request->phone,
                            'email' => $request->email,
                            'school_status' => $request->school_status,
                            'owner_name' => $request->owner_name,
                            'owner_phone' => $request->owner_phone,
                            'owner_type' => $request->owner_type,
                            'owner_email' => $request->owner_email,
                            'manager_name' => $request->manager_name,
                            'manager_phone' => $request->manager_phone,
                            'manager_email' => $request->manager_email,

                        ]
                    );

                    return redirect(route('school.accr.application') . '?step=' . ($step + 1));

                    break;

                case 'attachments':

                    if ($request->delete_attachment) {
                        $att = AccrAttachment::find($request->delete_attachment);
                        \Storage::disk('public')->delete($att->attachment);
                        $att->delete();

                        return redirect(route('school.accr.application') . '?step=' . $step);
                    }

                    $validatedData = $request->validate([
                        'upload_file' => 'required|mimes:pdf,jpeg,bmp,png',
                        'attachment_name' => 'required',
                    ]);

                    // Upload a file
                    $path = $request->file('upload_file')->store('attachments', config('voyager.storage.disk'));

                    $attachment = new AccrAttachment();

                    $attachment->application_id = $application_id;
                    $attachment->attachment_id = $validatedData['attachment_name'];
                    $attachment->attachment = $path;

                    $attachment->save();

                    return redirect(route('school.accr.application') . '?step=' . ($step + 1));

                    break;

                case 'buildings_plots':

                    $building = new AccrBuildingsAndPlots();

                    $building->school_id = auth()->user()->school_id;
                    $building->name = $request->name;
                    $building->class = $request->class;
                    $building->purpose = $request->purpose;
                    $building->size = $request->size;
                    $building->capacity = $request->capacity;
                    $building->construction_materials = $request->construction_materials;
                    $building->roofing_materials = $request->roofing_materials;
                    $building->harvests_rain_water = $request->harvests_rain_water;
                    $building->has_water = $request->has_water;
                    $building->has_electricity = $request->has_electricity;
                    $building->has_internet = $request->has_internet;
                    $building->has_extinguisher = $request->has_extinguisher;
                    $building->has_lightening_arrestor = $request->has_lightening_arrestor;
                    $building->has_external_lighting = $request->has_external_lighting;
                    $building->furniture = $request->furniture;
                    $building->equipment = $request->equipment;

                    $building->save();

                    if (isset($request->infrastructure)) {
                        for ($i = 0; $i < count($request->infrastructure); $i++) {
                            if ($request->infrastructure[$i] != '') {

                                AccrBuildingPlotInfrastructure::insert(
                                    [
                                        'building_id' => $building->id,
                                        'infrastructure_id' => $request->infrastructure[$i],
                                        'quantity' => $request->quantity[$i],
                                    ]
                                );
                            }

                        }
                    }

                    return redirect(route('school.accr.application') . '?step=' . ($step + 1));

                    break;

                case 'edit_buildings_plots':

                    AccrBuildingsAndPlots::where('id', $request->bp_id)->update(
                        [
                            'name' => $request->name,
                            'class' => $request->class,
                            'purpose' => $request->purpose,
                            'size' => $request->size,
                            'capacity' => $request->capacity,
                            'construction_materials' => $request->construction_materials,
                            'roofing_materials' => $request->roofing_materials,
                            'harvests_rain_water' => $request->harvests_rain_water,
                            'has_water' => $request->has_water,
                            'has_electricity' => $request->has_electricity,
                            'has_internet' => $request->has_internet,
                            'has_extinguisher' => $request->has_extinguisher,
                            'has_lightening_arrestor' => $request->has_lightening_arrestor,
                            'has_external_lighting' => $request->has_external_lighting,
                            'furniture' => $request->furniture,
                            'equipment' => $request->equipment
                        ]
                    );

                    if (isset($request->infrastructure)) {
                        AccrBuildingPlotInfrastructure::where('building_id', $request->bp_id)->delete();
                        for ($i = 0; $i < count($request->infrastructure); $i++) {
                            if ($request->infrastructure[$i] != '') {
                                AccrBuildingPlotInfrastructure::insert(
                                    [
                                        'building_id' => $request->bp_id,
                                        'name' => $request->infrastructure[$i],
                                        'quantity' => $request->quantity[$i],
                                    ]
                                );
                            }

                        }
                    }

                    return redirect(route('school.accr.application') . '?step=' . $step);

                    break;

                case 'programs':

                    if ($request->delete_program) {
                        ProgramsOffered::find($request->delete_program)->delete();

                        return redirect(route('school.accr.application') . '?step=' . ($step));
                    }

                    if ($request->po_id) {
                        $program = ProgramsOffered::find($request->po_id);
                    } else {
                        $program = new ProgramsOffered();
                        $program->school_id = auth()->user()->school_id;
                    }

                    $program->sector_id = $request->sector_id;
                    $program->sub_sector_id = $request->sub_sector_id;
                    $program->qualification_id = $request->qualification_id;
                    $program->rtqf_level_id = $request->rtqf_level_id;
                    $program->male_trainees = $request->male_trainees;
                    $program->female_trainees = $request->female_trainees;
                    $program->curriculum = $request->curriculum;

                    $program->save();

                    return redirect(route('school.accr.application') . '?step=' . ($step + 1));

                    break;

                case 'teachers':

                    if ($request->delete_trainer) {
                        Trainer::find($request->delete_trainer)->delete();
                        return redirect(route('school.accr.application') . '?step=' . ($step));
                    }

                    if ($request->trainer_id) {
                        $trainer = Trainer::find($request->trainer_id);
                    } else {
                        $trainer = new Trainer();

                        $trainer->school_id = auth()->user()->school_id;
                    }


                    $trainer->names = $request->names;
                    $trainer->gender = $request->gender;
                    $trainer->qualification_level_id = $request->qualification_level;
                    $trainer->qualification = $request->qualification;
                    $trainer->certificationId = $request->certification;
                    $trainer->experience = $request->experience;
                    $trainer->modules_taught = $request->modules_taught;

                    $trainer->save();

                    return redirect(route('school.accr.application') . '?step=' . ($step + 1));

                    break;

                case 'staff_members':

                    if ($request->delete_staff) {

                        StaffAdministrator::find($request->delete_staff)->delete();

                        return redirect(route('school.accr.application') . '?step=' . ($step));
                    }

                    if (isset($request->staff_id)) {
                        $staff = StaffAdministrator::find($request->staff_id);
                    } else {
                        $staff = new StaffAdministrator();

                        $staff->school_id = auth()->user()->school_id;
                    }


                    $staff->names = $request->names;
                    $staff->gender = $request->gender;
                    $staff->position = $request->position;
                    $staff->academic_qualification = $request->academic_qualification;
                    $staff->institution_studied = $request->institution_studied;
                    $staff->qualification_level_id = $request->qualification_level;
                    $staff->teaching_experience = $request->teaching_experience;
                    $staff->assessor_qualification = $request->assessor_qualification;

                    $staff->save();

                    return redirect(route('school.accr.application') . '?step=' . ($step + 1));

                    break;

                case 'apply':

                    if ($request->delete_application) {
                        AccrQualificationsApplied::find($request->delete_application)->delete();

                        return redirect(route('school.accr.application') . '?step=' . ($step + 1));
                    }

                    $application = new AccrQualificationsApplied();

                    $application->application_id = $application_id;
                    $application->sector_id = $request->sector_id;
                    $application->sub_sector_id = $request->sub_sector_id;
                    $application->rtqf_level_id = $request->rtqf_level_id;
                    $application->curriculum_qualification_id = $request->curriculum_qualification_id;
                    $application->save();

                    return redirect(route('school.accr.application') . '?step=' . ($step));

                    break;
            }
        }


    }

    public function schoolInput(Request $request, $object = null, $obj_id = null)
    {
//        $check_app = AccrApplication::where('school_id', auth()->user()->school_id)->get();
////        $check_app = AccrApplication::where('school_id', auth()->user()->school_id)->where('status', 'In Process')->first();
//
//        dd($check_app);
//        if ($check_app) {
//            $application_id = $check_app->id;
//        } else {
//            $application_id = '';
//        }

        $step = $request->get('step') ?: "1";

        $kigali = ['Gasabo', 'Kicukiro', 'Nyarugenge'];
        $north = ['Burera', 'Gakenke', 'Gicumbi', 'Musanze', 'Rulindo'];
        $south = ['Gisagara', 'Huye', 'Kamonyi', 'Muhanga', 'Nyamagabe', 'Nyanza', 'Nyaruguru', 'Ruhango'];
        $east = ['Bugesera', 'Gatsibo', 'Kayonza', 'Kirehe', 'Ngoma', 'Nyagatare', 'Rwamagana'];
        $west = ['Karongi', 'Ngororero', 'Nyabihu', 'Nyamasheke', 'Rubavu', 'Rusizi', 'Rutsiro'];

        $provinces = ['west' => $kigali, 'north' => $north, 'south' => $south, 'east' => $east, 'west' => $west];

        $school_info = SchoolInformation::where('id', auth()->guard()->user()->school_id)->first();
        $attachments_source = AccrAttachmentsSource::get();
//        $attachments_data = AccrAttachment::where('application_id', auth()->user()->school_id)->get();
        $attachments_data = [];
        if (auth()->user()->school) {
            $uoi = auth()->user()->school;
            if ($uoi->accrApplication)
                $attachments_data = $uoi->accrApplication;
        }
        $infrastructures_source = InfrastructuresSource::all();
        $buildings_plots = AccrBuildingsAndPlots::where('school_id', auth()->user()->school_id)->get();

        $sectors = TrainingSectorsSource::get();
        $sub_sectors = TrainingTradesSource::all();
        $rtqfs = RtqfSource::get();

        $programs_offered = Department::where('school_id', auth()->user()->school_id)->get();

        $curriculum_qualifications = CurriculumQualification::all();
        $qualification_levels = QualificationLevel::all();
        $certications = DB::connection('accr_mysql')->table('source_traines_certification')
            ->select('source_traines_certification.*')
            ->get();
        $trainers = StaffsInfo::where('school_id', $school_info->id)->where('staff_category', 'Academic')->get();
        //        $trainers                  = Trainer::where('school_id', auth()->user()->school_id)->get();
        $staffs = StaffsInfo::where('school_id', $school_info->id)->where('staff_category', 'Administrative')->get();
        $data_source_owner_types = DB::connection('accr_mysql')->table('source_school_ownership')->get();

//        $application_data = AccrQualificationsApplied::where('application_id', auth()->user()->school_id)->get();
        $application_data = [];
        if (auth()->user()->school) {
            $uoi = auth()->user()->school;
            if ($uoi->accrApplication)
                $application_data = $uoi->accrApplication;
        }
        $accreditation_types = AccreditationType::all();

        if ($object != null) {

            if ($object == 'programs') {
                $program = ProgramsOffered::where('school_id', auth()->user()->school_id)->where('id', $obj_id)->first();

                return view('school.accr.input', compact('school_info', 'provinces', 'certications', 'attachments_data', 'attachments_source', 'infrastructures_source', 'buildings_plots', 'programs_data', 'sectors', 'sub_sectors', 'rtqfs', 'programs_offered', 'qualification_levels', 'trainers', 'staffs', 'application_data', 'program', 'curriculum_qualifications', 'data_source_owner_types', 'accreditation_types', 'step'));
            }

            if ($object == 'trainers') {
                $trainer = Trainer::where('school_id', auth()->user()->school_id)->where('id', $obj_id)->first();

                return view('school.accr.input', compact('school_info', 'provinces', 'certications', 'attachments_data', 'attachments_source', 'infrastructures_source', 'buildings_plots', 'programs_data', 'sectors', 'sub_sectors', 'rtqfs', 'programs_offered', 'qualification_levels', 'trainers', 'staffs', 'application_data', 'trainer', 'curriculum_qualifications', 'data_source_owner_types', 'accreditation_types', 'step'));
            }

            if ($object == 'staffs') {
                $staff = StaffAdministrator::where('school_id', auth()->user()->school_id)->where('id', $obj_id)->first();

                return view('school.accr.input', compact('school_info', 'provinces', 'certications', 'attachments_data', 'attachments_source', 'infrastructures_source', 'buildings_plots', 'programs_data', 'sectors', 'sub_sectors', 'rtqfs', 'programs_offered', 'qualification_levels', 'trainers', 'staffs', 'application_data', 'staff', 'curriculum_qualifications', 'data_source_owner_types', 'accreditation_types', 'step'));
            }


        }

        $criteria_sections = AccrCriteriaSection::get();
        $data_source_rating = DB::connection('accr_mysql')->table('source_rating')
            ->select('source_rating.*')
            ->get();
        $data_source_academic_year = DB::connection('mysql')->table('source_academic_years')
            ->select('source_academic_years.*')
            ->get();
        $data_source_graduate_emp_statuses = DB::connection('accr_mysql')->table('source_graduate_emp_statuses')
            ->select('source_graduate_emp_statuses.*')
            ->get();
        $data_source_graduate_emp_timings = DB::connection('accr_mysql')->table('source_graduate_emp_timings')
            ->select('source_graduate_emp_timings.*')
            ->get();

        return view('school.accr.input', compact('school_info', 'criteria_sections', 'provinces', 'attachments_data', 'certications', 'attachments_source', 'infrastructures_source', 'buildings_plots', 'programs_data', 'sectors', 'sub_sectors', 'rtqfs', 'programs_offered', 'qualification_levels', 'trainers', 'staffs', 'application_data', 'curriculum_qualifications', 'data_source_rating', 'data_source_academic_year', 'data_source_graduate_emp_statuses', 'data_source_graduate_emp_timings', 'data_source_owner_types', 'accreditation_types', 'step'));

    }

    public function confirm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'accreditation_type_id' => 'required'
        ], [
            'accreditation_type_id' => 'The accreditation type field is required.'
        ]);
        $step = $request->input('step');
        if ($validator->fails()) {
            return redirect('/school/accreditation/school-applications?step=' . $step)
                ->withErrors($validator)
                ->withInput();
        }

        $check_app = AccrApplication::where('school_id', auth()->user()->school_id)
            ->where('status', 'In Process')
            ->first();

        //Object To send
        if ($check_app) {

            $data = array();
//        $data['attachments_data'] = AccrAttachment::where('application_id', auth()->user()->school_id)->get();

            $data['attachments_data'] = AccrAttachment::where('application_id', $check_app->id)->get();
            $data['buildings_plots'] = AccrBuildingsAndPlots::where('school_id', auth()->user()->school_id)->get();
            $data['programs_offered'] = ProgramsOffered::where('school_id', auth()->user()->school_id)->get();
            $data['trainers'] = Trainer::where('school_id', auth()->user()->school_id)->get();
            $data['staffs'] = StaffAdministrator::where('school_id', auth()->user()->school_id)->get();
//        $data['application_data'] = AccrQualificationsApplied::where('application_id', auth()->user()->school_id)->get();
            $data['application_data'] = AccrQualificationsApplied::where('application_id', $check_app->id)->get();
            $data['accreditation_type'] = $check_app->accreditationType ? $check_app->accreditationType->type : "";
            $district = school('district');
            $district = District::where('name', $district)->first();
            $email = $district->email;
            //        return view('email.mailAccredit',compact('data'));
            if ($check_app) {

                $application_id = $check_app->id;
                $application = AccrApplication::find($application_id);
                $application->status = 'Submitted';
                $application->accreditation_type_id = $request->accreditation_type_id;

                $application->save();

            }
            if ($request->next) {

                if ($email) {
                    //send email
                    Mail::to($email)->send(new SendMailAccredition($data));
                    return redirect('/school/accreditation/process');
                } else {
                    return redirect()->back();
                }

            } else {
                if ($email) {
                    // send email
                    Mail::to($email)->send(new SendMailAccredition($data));
                    return redirect('/school/accreditation/school-applications/view');
                } else {
                    return redirect()->back();
                }
            }
        }
        return back();

    }

    public function input(Request $request, $object = null, $obj_id = null)
    {
        $criteria_sections = AccrCriteriaSection::get();
        $getFunctionInput = Functions::where('name', 'Input')->first();
        $school_id = auth()->user()->school_id;
        $qualities = [];
        $function_id = null;

        $today = date('F d, 2018', strtotime(Carbon::today()));
        $submitId = rand(1000, 9999) . '-' . strtotime(Carbon::now()) . '-' . rand(1000, 9999);

        if ($getFunctionInput != null) {
            $qualities = $getFunctionInput->qualityAreas;
            $function_id = $getFunctionInput->id;
        }

        $step = $request->get('step') ?: '1';
        $istep = $request->get('istep') ?: '1';

        return view('school.accr.areas.input', compact('criteria_sections', 'qualities', 'school_id', 'function_id', 'today', 'submitId', 'step', 'istep'));
    }

    public function process(Request $request)
    {

        if ($request->answers) {
            for ($i = 0; $i < count($request->answers); $i++) {
                $current = AccrCriteriaAnswer::where('school_id', auth()->user()->school_id)
                    ->where('criteria_id', $request->criteria_id[$i])
                    ->whereDate('created_at', Carbon::today())
                    ->first();

                if ($current) {
                    $answer = $current;
                } else {
                    $answer = new AccrCriteriaAnswer();
                }
                $answer->school_id = auth()->user()->school_id;
                $answer->criteria_id = $request->criteria_id[$i];
                $answer->answer = $request->answers[$i];
                $answer->academic_year = date('Y');
                $answer->save();
            }
            $url = $request->url;
            if ($request->input('lstep') == $request->input('istep')) {
                $nxtA = $request->step + 1;
                $nxtS = 1;
            } else {
                $nxtA = $request->step;
                $nxtS = $request->istep + 1;
            }


            return redirect($url . "?step=" . $nxtA . '&istep=' . $nxtS);
        }

        $today = date('F d, 2018', strtotime(Carbon::today()));
        $submitId = rand(1000, 9999) . '-' . strtotime(Carbon::now()) . '-' . rand(1000, 9999);
        $getFunctionInput = Functions::where('name', 'Process')->first();
        $school_id = auth()->user()->school_id;
        $qualities = [];
        $function_id = null;

        if ($getFunctionInput != null) {
            $qualities = $getFunctionInput->qualityAreas;
            $function_id = $getFunctionInput->id;
        }
        $criteria_sections = AccrCriteriaSection::get();

        $step = $request->get('step') ?: '1';
        $istep = $request->get('istep') ?: '1';

        return view('school.accr.areas.process', compact('criteria_sections', 'qualities', 'school_id', 'function_id', 'today', 'submitId', 'step', 'istep'));
    }

    public function confirmationSubmit(Request $request)
    {
        $this->validate($request, [
            'function_id' => 'required',
            'school_id' => 'required',
            'academic_year' => 'required'
        ]);
        $submitId = rand(1000, 9999) . '-' . strtotime(Carbon::now()) . '-' . rand(1000, 9999);
        $request->request->add(['submit_id' => $submitId]);
        $data = $request->except(['_token', '_method']);

        $function = Functions::find($request->input('function_id'));
        $qualities = $function->qualityAreas;

        $update = false;

        foreach ($qualities as $quality) {
            foreach ($quality->criteria as $criteria_section) {
                foreach ($criteria_section['criterias'] as $criteria) {
                    $creteria = AccrCriteriaAnswer::where('school_id', auth()->user()->school_id)
                        ->where('criteria_id', $criteria->id)
                        ->whereDate('created_at', Carbon::today());

                    if ($creteria->count() > 0)
                        $update = $creteria->update(['submit_id' => $submitId]);
                }
            }
        }

        if ($update)
            SchoolSelfAssessimentStatus::create($data);
        else
            return back();

        return redirect()
            ->route('school.myAssessment')
            ->with(['successMessage' => 'Self Assessment submitted successfully.']);
    }

    public function output(Request $request)
    {

        $data_source_rating = DB::connection('accr_mysql')->table('source_rating')
            ->select('source_rating.*')
            ->get();
        $data_source_academic_year = DB::table('source_academic_years')
            ->select('source_academic_years.*')
            ->orderBy('year', "DESC")
            ->get();
        $data_source_graduate_emp_statuses = DB::connection('accr_mysql')->table('source_graduate_emp_statuses')
            ->select('source_graduate_emp_statuses.*')
            ->get();
        $data_source_graduate_emp_timings = DB::connection('accr_mysql')->table('source_graduate_emp_timings')
            ->select('source_graduate_emp_timings.*')
            ->get();
        if (isset($request->names)) {
            $graduate = new Graduate();

            $graduate->school_id = auth()->user()->school_id;
            $graduate->year = $request->year;
            $graduate->names = $request->names;
            $graduate->gender = $request->gender;
            $graduate->trade = $request->trade;
            $graduate->rtqf_achieved = $request->rtqf_achieved;
            $graduate->phone_number = $request->phone_number;
            $graduate->employment_status = $request->employment_status;
            $graduate->employment_timing = $request->employment_timing;
            $graduate->monthly_salary = $request->monthly_salary;
            $graduate->name_of_employment_company = $request->name_of_employment_company;
            $graduate->sector = $request->sector;
            $graduate->sub_sector = $request->sub_sector;
            $graduate->employment_contact = $request->employment_contact;
            $graduate->knowledge_level = $request->knowledge_level;
            $graduate->attitude_level = $request->attitude_level;
            $graduate->skill_level = $request->skill_level;

            $graduate->save();

            $graduates = Graduate::where('school_id', auth()->user()->school_id)->where('year', $request->year)->get();
            return view('school.accr.output', compact('graduates', 'data_source_rating', 'data_source_academic_year', 'data_source_graduate_emp_statuses', 'data_source_graduate_emp_timings'));
        }

        $academic_year = $data_source_academic_year;

        if ($request->year) {
            $graduates = Graduate::where('school_id', auth()->user()->school_id)->where('year', $request->year)->get();
            $cyear = $request->year;
            return view('school.accr.output', compact('graduates', 'data_source_rating', 'data_source_academic_year', 'data_source_graduate_emp_statuses', 'data_source_graduate_emp_timings', 'academic_year', 'cyear'));
        }

        return view('school.accr.output', compact('data_source_rating', 'data_source_academic_year', 'data_source_graduate_emp_statuses', 'data_source_graduate_emp_timings', 'academic_year'));

    }

    /**
     *  handling applications
     * */

    public function view()
    {
        $check_app = AccrApplication::where('school_id', auth()->user()->school_id)
            ->where('status', 'Submitted')
            ->first();
        if ($check_app) {

            $application_id = $check_app->id;
            $application = AccrApplication::find($application_id);

            $apps = AccrQualificationsApplied::where('application_id', $application_id)->get();

            return view('school.accr.view', compact('apps', 'application_id'));
        }
        return view('school.accr.view');
    }

    public function viewMore($application_id)
    {
        $school_info = SchoolInformation::where('id', auth()->user()->school_id)->first();
        $attachments_data = AccrAttachment::where('application_id', $application_id)->get();
        $buildings_plots = AccrBuildingsAndPlots::where('school_id', auth()->user()->school_id)->get();
        $programs_offered = ProgramsOffered::where('school_id', auth()->user()->school_id)->get();
        $trainers = Trainer::where('school_id', auth()->user()->school_id)->get();
        $staffs = StaffAdministrator::where('school_id', auth()->user()->school_id)->get();
        $application_data = AccrQualificationsApplied::where('application_id', $application_id)->get();

        return view('school.accr.view_more', compact('school_info', 'attachments_data', 'buildings_plots', 'programs_offered', 'trainers', 'staffs', 'application_data', 'application_id'));
    }

    public function download($application_id)
    {
        $school_info = SchoolInformation::where('id', auth()->user()->school_id)->first();
        $attachments_data = AccrAttachment::where('application_id', $application_id)->get();
        $buildings_plots = AccrBuildingsAndPlots::where('school_id', auth()->user()->school_id)->get();
        $programs_offered = ProgramsOffered::where('school_id', auth()->user()->school_id)->get();
        $trainers = Trainer::where('school_id', auth()->user()->school_id)->get();
        $staffs = StaffAdministrator::where('school_id', auth()->user()->school_id)->get();
        $application_data = AccrQualificationsApplied::where('application_id', $application_id)->get();


        $pdf = PDF::loadView('school.accr.download', compact('school_info', 'attachments_data', 'buildings_plots', 'programs_offered', 'trainers', 'staffs', 'application_data', 'application_id'))->setPaper('a4', 'landscape');

        return $pdf->stream('Download.pdf');
    }

    public function toPrint($application_id)
    {
        $school_info = SchoolInformation::where('id', auth()->user()->school_id)->first();
        $attachments_data = AccrAttachment::where('application_id', $application_id)->get();
        $buildings_plots = AccrBuildingsAndPlots::where('school_id', auth()->user()->school_id)->get();
        $programs_offered = ProgramsOffered::where('school_id', auth()->user()->school_id)->get();
        $trainers = Trainer::where('school_id', auth()->user()->school_id)->get();
        $staffs = StaffAdministrator::where('school_id', auth()->user()->school_id)->get();
        $application_data = AccrQualificationsApplied::where('application_id', $application_id)->get();

        return view('school.accr.print', compact('school_info', 'attachments_data', 'buildings_plots', 'programs_offered', 'trainers', 'staffs', 'application_data', 'application_id'));
    }

    /**
     *  School Self Assessment
     * */

    public function assessmentView()
    {
        $school = auth()->user()->school_id;

        $submited = SchoolSelfAssessimentStatus::where('academic_year', date('Y'))
            ->where('school_id', $school)
            ->orderBy('created_at', 'ASC')
            ->get();

        return view('school.accr.assessment-view', compact('submited'));
    }

    public function assessmentViewSchool($fun)
    {
        $school = auth()->user()->school_id;

        $funct = Functions::findOrFail($fun);
        $school = SchoolInformation::findOrFail($school);
        $qualities = [];
        if ($funct->qualityAreas)
            $qualities = $funct->qualityAreas;

        return view('school.accr.view-school-assessment', compact('school', 'funct', 'qualities'));
    }

    public function printAssessmentViewSchool(Request $request, $fun)
    {
        $school = auth()->user()->school_id;

        $funct = Functions::findOrFail($fun);
        $school = SchoolInformation::findOrFail($school);
        $qualities = [];
        if ($funct->qualityAreas)
            $qualities = $funct->qualityAreas;

//        $pdf = \App::make('dompdf.wrapper');
//        $pdf->loadView('school.print-view-school-assessment', compact('school', 'funct', 'qualities'));
//
//        $pathX = '/pdfs/ '. date('FY') . '/';
//        $path = public_path() . $pathX;
//        \File::isDirectory($path) or \File::makeDirectory($path, 0777, true, true);
//        $file = $path.time() . '-' . $school->id.'.pdf';

        return view('school.accr.print-view-school-assessment', compact('school', 'funct', 'qualities'));
    }
}
