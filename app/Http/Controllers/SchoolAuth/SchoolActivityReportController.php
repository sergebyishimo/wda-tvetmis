<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateActivityReportRequest;
use App\Http\Requests\UpdateActivityReportRequest;
use App\SchoolActivityReport;
use App\Http\Controllers\Controller;

class SchoolActivityReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = school(true)->activityreports;

        return view('school.activityreport.index',compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $report = null;
        return view('school.activityreport.create',compact('report'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateActivityReportRequest $request)
    {
        $schoolactivityreport = new SchoolActivityReport();
        $schoolactivityreport->school_id = school(true)->id;
        $schoolactivityreport->activity_description = $request->input('activity_description');
        $schoolactivityreport->implemented_from = $request->input('implemented_from');
        $schoolactivityreport->implemented_to = $request->input('implemented_to');
        $schoolactivityreport->impact_of_activity = $request->input('impact_of_activity');
        $schoolactivityreport->activity_completed = $request->input('activity_completed');
        $schoolactivityreport->activity_progress_details = $request->input('activity_progress_details');
        $save = $schoolactivityreport->save();

        if ($save)
            return redirect(route('school.activityreports.index'))->with(['status' => '1', 'message' => "Activity Report  Sent Successfully."]);
        else
            return redirect(route('school.activityreports.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Send Activity Report."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = school(true)->activityreports()
            ->where('id', $id)->first();
        if ($report)
            return view("school.activityreport.create", compact("report"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateActivityReportRequest $request, $id)
    {
        $schoolactivityreport = school(true)->activityreports()
            ->where('id', $id)->first();
        if (!$schoolactivityreport)
            return abort(419);

        $schoolactivityreport->school_id = school(true)->id;
        $schoolactivityreport->activity_description = $request->input('activity_description');
        $schoolactivityreport->implemented_from = $request->input('implemented_from');
        $schoolactivityreport->implemented_to = $request->input('implemented_to');
        $schoolactivityreport->impact_of_activity = $request->input('impact_of_activity');
        $schoolactivityreport->activity_completed = $request->input('activity_completed');
        $schoolactivityreport->activity_progress_details = $request->input('activity_progress_details');
        $save = $schoolactivityreport->save();

        if ($save)
            return redirect(route('school.activityreports.index'))->with(['status' => '1', 'message' => "Activity Reports Updated Successfully."]);
        else
            return redirect(route('school.activityreports.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Activity Reports."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = school(true)->activityreports()->where('id', $id)->first();
        if (!$request)
            return abort(419);
        $request->delete();

        return redirect(route('school.activityreports.index'));
    }
}
