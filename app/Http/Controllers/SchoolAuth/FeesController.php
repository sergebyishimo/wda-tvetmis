<?php

namespace App\Http\Controllers\SchoolAuth;

use App\AdditionalFees;
use App\FeesExpected;
use App\Http\Requests\AddPaymentRequest;
use App\Http\Requests\SearchPaymentRequest;
use App\Http\Requests\StoreAdditionalFeesRequest;
use App\Http\Requests\UpdateFeesRequest;
use App\Level;
use App\Payment;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeesController extends Controller
{
    function __construct()
    {
        $this->middleware('school');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = school(true)->levels()->where("status", "1");
        $departments = school(true)->departments()->where('status', 1);

        return view('school.fees.index', compact('levels', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('school.fees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateFeesRequest $request)
    {
        $count = count($request->level_id);

        for ($i = 0; $i < $count; $i++) {

            $term1Bo[$i] = filter_var($request->term1Bo[$i], FILTER_SANITIZE_NUMBER_INT);
            $term1Day[$i] = filter_var($request->term1Day[$i], FILTER_SANITIZE_NUMBER_INT);

            $term2Bo[$i] = filter_var($request->term2Bo[$i], FILTER_SANITIZE_NUMBER_INT);
            $term2Day[$i] = filter_var($request->term2Day[$i], FILTER_SANITIZE_NUMBER_INT);

            $term3Bo[$i] = filter_var($request->term3Bo[$i], FILTER_SANITIZE_NUMBER_INT);
            $term3Day[$i] = filter_var($request->term3Day[$i], FILTER_SANITIZE_NUMBER_INT);


            if ($term1Bo[$i] > 0 || $term1Day[$i] > 0) {
                $check = FeesExpected::where('level_id', $request->level_id[$i])->where('term', 1)->count();
                if ($check == 0)
                    FeesExpected::create(['school_id' => school('id'), 'term' => 1, 'level_id' => $request->level_id[$i], 'boarding' => $term1Bo[$i], 'day' => $term1Day[$i]]);
                else
                    FeesExpected::where('term', 1)->where('level_id', $request->level_id[$i])
                        ->where('school_id', school('id'))
                        ->update(['boarding' => $term1Bo[$i], 'day' => $term1Day[$i]]);
            }


            if ($term2Bo[$i] > 0 || $term2Day[$i] > 0) {
                $check = FeesExpected::where('level_id', $request->level_id[$i])->where('term', 2)->count();
                if ($check == 0) {
                    FeesExpected::create(['school_id' => school('id'), 'term' => 2, 'level_id' => $request->level_id[$i], 'boarding' => $term2Bo[$i], 'day' => $term2Day[$i]]);
                } else {
                    FeesExpected::where('term', 2)->where('level_id', $request->level_id[$i])
                        ->where('school_id', school('id'))
                        ->update(['boarding' => $term2Bo[$i], 'day' => $term2Day[$i]]);
                }
            }

            if ($term3Bo[$i] > 0 || $term3Day[$i] > 0) {
                $check = FeesExpected::where('level_id', $request->level_id[$i])->where('term', 3)->count();
                if ($check == 0) {
                    FeesExpected::create(['school_id' => school('id'), 'term' => 3, 'level_id' => $request->level_id[$i], 'boarding' => $term3Bo[$i], 'day' => $term3Day[$i]]);
                } else {
                    FeesExpected::where('term', 3)->where('level_id', $request->level_id[$i])
                        ->where('school_id', school('id'))
                        ->update(['boarding' => $term3Bo[$i], 'day' => $term3Day[$i]]);
                }
            }
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AddPaymentRequest $request
     * @return void
     */
    public
    function update(AddPaymentRequest $request)
    {
        $data = [
            'school_id' => school('id'),
            'student_id' => $request->input('student'),
            'term' => $request->input('term'),
            'amount' => $request->input('amount'),
            'operator' => $request->input('bank_name'),
            'bank_number' => $request->input('slip_number'),
        ];
        if ($request->hasFile('bank_slip')) {
            $file = $request->file('bank_slip');
            $n = $file->store("payments", config('voyager.storage.disk'));
            $data['bank_slip'] = $n;
        }

        if ($request->has('year'))
            $data['created_at'] = date("Y-m-d H:j:s", strtotime($request->input('year')));

        $create = Payment::create($data);

        if ($create) {
            if ($request->has('year'))
                return redirect()->route('school.fees.index')->with(['status' => '1', 'message' => 'Payment Record added Successfully.']);

            return back()->with(['status' => '1', 'message' => 'Payment Record added Successfully.']);
        } else
            return back()->withInput()->with(['status' => '0', 'message' => 'Adding Record Failed !!!']);
    }

    public
    function updating(Request $request)
    {
        $create = [];
        $student = $request->input('student');
        if (is_array($student)) {
            foreach ($request->input('student') as $id => $amout) {
                if ($amout > 0) {
                    $data = [
                        'school_id' => school('id'),
                        'student_id' => $id,
                        'term' => $request->input('term'),
                        'amount' => $amout
                    ];
                    if ($request->has('year'))
                        $data['created_at'] = date("Y-m-d H:j:s", strtotime($request->input('year')));
                    $c = Payment::create($data);
                    if ($c)
                        $create[] = $c;
                }
            }
        }


        if (count($create) == count($student))
            return redirect()->route('school.fees.index')
                ->with(['status' => '1', 'message' => 'All Payment Record added Successfully.']);
        else
            return redirect()->route('school.fees.index')
                ->withInput()->with(['status' => '3', 'message' => 'Only ' . count($create) . ' records added !']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }

    public
    function getPayments(SearchPaymentRequest $request)
    {
        $based = $request->input('based');
        $term = $request->input('term');
        $year = $request->input('acad_year');

        switch ($based) {
            case "c":
                $level = $request->input('level');
                $students = school(true)->students()->where('status', "active")
                    ->where('level_id', $level)
                    ->orderBy("fname", "ASC")
                    ->orderBy("lname", "ASC");
                $level = Level::findOrFail($level);

                return view('school.fees.payments', compact('students', 'based', 'term', 'year', 'level'));
                break;
            case "s":
                $id = $request->input('student');
                $student = Student::findOrFail($id);
                $additionalFees = getStudentAdditionalFees($term, $year, $student);
                return view('school.fees.payments', compact('student', 'based', 'term', 'year', 'additionalFees'));
                break;
        }
        return null;
    }

    /**
     * @param StoreAdditionalFeesRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function feesAdditional(StoreAdditionalFeesRequest $request)
    {
        $based = $request->input('based');
        $term = $request->input('term');
        $year = $request->input('acad_year');
        $department = $request->input('qualification');
        $label = $request->input('label');
        $amount = $request->input('amount');
        $data = [
            'school_id' => school('id'),
            'label' => $label,
            'amount' => $amount,
            'acad_year' => $year,
            'term' => $term
        ];
        switch ($based) {
            case "c":
                if ($request->has('level')) {
                    $level = $request->input('level');
                    $additional = getAdditionalFees($term, $year, $label, null, $level);
                    if ($additional == null)
                        $data['level_id'] = $level;
                    else
                        return redirect()->route('school.fees.index')
                            ->with(['status' => '3', 'message' => 'Duplicating Additional Fees']);
                } else {
                    $additional = getAdditionalFees($term, $year, $label, $department, null);
                    if ($additional == null)
                        $data['department_id'] = $department;
                    else
                        return redirect()->route('school.fees.index')
                            ->with(['status' => '3', 'message' => 'Duplicating Additional Fees']);
                }
                AdditionalFees::create($data);
                break;
            case "s":
                $id = $request->input('student');
                $additional = getAdditionalFees($term, $year, $label, null, null, $id);
                if ($additional == null){
                    $data['student_id'] = $id;
                    AdditionalFees::create($data);
                }
                else
                    return redirect()->route('school.fees.index')
                        ->with(['status' => '3', 'message' => 'Duplicating Additional Fees']);
                break;
        }
        return redirect()->route('school.fees.index')
            ->with(['status' => '1', 'message' => 'Additional Fees add successfully .']);
    }
}
