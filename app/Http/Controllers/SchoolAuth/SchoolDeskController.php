<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateSchoolDeskRequest;
use App\Http\Requests\UpdateSchoolDeskRequest;
use App\Http\Controllers\Controller;
use App\SchoolDesk;

class SchoolDeskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schooldesks = school(true)->desks;
        return view('school.desks.index',compact('schooldesks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schooldesk = null;
        return view('school.desks.create',compact('schooldesk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSchoolDeskRequest $request)
    {
        $schooldesk = new SchoolDesk();
        $schooldesk->school_id = school(true)->id;
        $schooldesk->rtqf_level = $request->input('rtqf_level');
        $schooldesk->classrooms = $request->input('classrooms');
        $schooldesk->desks = $request->input('desks');
        $save = $schooldesk->save();

        if ($save)
            return redirect(route('school.desks.index'))->with(['status' => '1', 'message' => "Desks & Classrooms  Saved Successfully."]);
        else
            return redirect(route('school.desks.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Save Desks & Classrooms."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schooldesk = school(true)->desks()
            ->where('id', $id)->first();
        if ($schooldesk)
            return view("school.desks.create", compact("schooldesk"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSchoolDeskRequest $request, $id)
    {
        $schooldesk = school(true)->desks()
            ->where('id', $id)->first();
        if (!$schooldesk)
            return abort(419);

        $schooldesk->school_id = school(true)->id;
        if($request->has('rtqf_level')){
            $schooldesk->rtqf_level = $request->input('rtqf_level');
        }
        if($request->has('classrooms')){
            $schooldesk->classrooms = $request->input('classrooms');
        }

        if($request->has('desks')){
            $schooldesk->desks = $request->input('desks');
        }

        $save = $schooldesk->save();

        if ($save)
            return redirect(route('school.desks.index'))->with(['status' => '1', 'message' => "Desks Updated Successfully."]);
        else
            return redirect(route('school.desks.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a Desks."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $desk = school(true)->desks()->where('id', $id)->first();
        if (!$desk)
            return abort(419);
        $desk->delete();

        return redirect(route('school.desks.index'));
    }
}
