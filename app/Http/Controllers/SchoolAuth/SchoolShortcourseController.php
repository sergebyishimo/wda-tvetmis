<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateShortCourse;
use App\Http\Requests\UpdateShortCourse;
use App\ShortCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolShortcourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shortcourses = school(true)->shortcourses;
        return view('school.shortcourses.index', compact('shortcourses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shortcourse = null;
        return view('school.shortcourses.create', compact('shortcourse'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateShortCourse $request)
    {
        $filePath = 'shortcourses';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachment')) {
            $fileR = $request->file('attachment');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id . Date('Y_m_d_H_m_s') . '.' . $ext, config('voyager.storage.disk'));
        }

        $shortcourse = new ShortCourse();
        $shortcourse->school_id = school(true)->id;
        $shortcourse->course_name = $request->input('course_name');
        $shortcourse->course_details = $request->input('course_details');
        $shortcourse->attachment = $file;
        $save = $shortcourse->save();

        if ($save)
            return redirect(route('school.shortcourses.index'))->with(['status' => '1', 'message' => "Shortcourse  Save Successfully."]);
        else
            return redirect(route('school.shortcourses.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Save a Shortcourse."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shortcourse = school(true)->shortcourses()
            ->where('id', $id)->first();
        if ($shortcourse)
            return view("school.shortcourses.create", compact("shortcourse"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShortCourse $request, $id)
    {
        $shortcourse = school(true)->shortcourses()
            ->where('id', $id)->first();
        if (!$shortcourse)
            return abort(419);

        $filePath = 'shortcourses';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachment')) {
            $fileR = $request->file('attachment');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id . Date('Y_m_d_H_m_s') . '.' . $ext, config('voyager.storage.disk'));
        }

        $shortcourse->school_id = school(true)->id;
        $shortcourse->course_name = $request->input('course_name');
        $shortcourse->course_details = $request->input('course_details');
        if ($file) {
            $shortcourse->attachment = $file;
        }
        $save = $shortcourse->save();

        if ($save)
            return redirect(route('school.shortcourses.index'))->with(['status' => '1', 'message' => "ShortCourse Updated Successfully."]);
        else
            return redirect(route('school.shortcourses.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a ShortCourse."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shortcourse = school(true)->shortcourses()->where('id', $id)->first();
        if (!$shortcourse)
            return abort(419);
        $shortcourse->delete();

        return redirect(route('school.shortcourses.index'));
    }
}
