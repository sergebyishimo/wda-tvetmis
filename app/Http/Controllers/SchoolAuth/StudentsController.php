<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Requests\UploadStudentsRequest;
use App\Student;
use Composer\Repository\PathRepository;
use FileUpload\FileUploadFactory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Storage;
use Excel;
class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('isSchoolManager') || Gate::allows('isTeachOrStaff')){
           $student = null;
            return view('school.students.index', compact('student'));
        }else{
            abort(404,'error page');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $student = null;
        return view('school.students.create', compact("student"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStudentRequest $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $imagePath = 'students';
        $regNo = $request->input('reg_no');
        $photo = null;
        if ($request->has('image')) {
            $photoR = $request->file('image');
            $ext = $photoR->getClientOriginalExtension();
            $rPath = $imagePath . $regNo . "." . $ext;
            $photo = $photoR->storeAs($imagePath, $regNo . '.' . $ext, config('voyager.storage.disk'));
        }

        $student = new Student();
        $student->school = school(true)->id;
        $student->reg_no = $regNo;
        $student->fname = $request->input('fname');
        $student->lname = $request->input('lname');
        $student->level_id = $request->input('level_id');
        $student->gender = $request->input('gender');
        $student->mode = $request->input('mode');
        $student->bdate = $request->input('bdate');
        $student->nationality = $request->input('nationality');
        $student->province = $request->input('Province');
        $student->district = $request->input('District');
        $student->sector = $request->input('Sector');
        $student->cell = $request->input('Cell');
        $student->village = $request->input('Village');
        $student->ft_name = $request->input('ft_name');
        $student->ft_phone = $request->input('ft_phone');
        $student->mt_name = $request->input('mt_name');
        $student->mt_phone = $request->input('mt_phone');
        $student->gd_name = $request->input('gd_name');
        $student->gd_phone = $request->input('gd_phone');
        $student->orphan = $request->input('orphan');
        $student->sponsor = $request->input('sponsor');
        $student->insurance = $request->input('insurance');
        $student->insurance_number = $request->input('insurance_number');
        $student->ubudehe = $request->input('ubudehe');
        $student->status = $request->input('status') ?: "active";
        $student->acad_year = $request->input('acad_year') ?: date('Y');
        $student->email = $request->input('email');
        $student->telephone = $request->input('telephone');
        $student->index_number = $request->input('index_number');
        $student->password = bcrypt($regNo);
        $student->updateVersion = updateVersionColumn('students');

        $save = $student->save();

        if ($save)
            return redirect(route('school.students.index'))->with(['status' => '1', 'message' => ucwords($request->input('fname')) . " Added Successfully."]);
        else
            return redirect(route('school.students.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to add a student."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $student = school(true)->students()
            ->where('id', $id)->first();
        if ($student)
            return view("school.students.create", compact("student"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStudentRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, $id)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $student = school(true)->students()
            ->where('id', $id)->first();
        if (!$student)
            return abort(419);

        $imagePath = 'students';
        $regNo = $student->reg_no;
        $photo = null;

        if ($request->has('image')) {
            $photoR = $request->file('image');
            $ext = $photoR->getClientOriginalExtension();
            $rPath = $imagePath . $regNo . "." . $ext;
            $photo = $photoR->storeAs($imagePath, $regNo . '.' . $ext, config('voyager.storage.disk'));
        }

        $student->fname = $request->input('fname');
        $student->lname = $request->input('lname');
        $student->level_id = $request->input('level_id');
        $student->gender = $request->input('gender');
        $student->mode = $request->input('mode');
        $student->bdate = $request->input('bdate');
        $student->nationality = $request->input('nationality');
        $student->province = $request->input('Province');
        $student->district = $request->input('District');
        $student->sector = $request->input('Sector');
        $student->cell = $request->input('Cell');
        $student->village = $request->input('Village');
        $student->ft_name = $request->input('ft_name');
        $student->ft_phone = $request->input('ft_phone');
        $student->mt_name = $request->input('mt_name');
        $student->mt_phone = $request->input('mt_phone');
        $student->gd_name = $request->input('gd_name');
        $student->gd_phone = $request->input('gd_phone');
        $student->orphan = $request->input('orphan');
        $student->sponsor = $request->input('sponsor');
        $student->insurance = $request->input('insurance');
        $student->insurance_number = $request->input('insurance_number');
        $student->ubudehe = $request->input('ubudehe');
        $student->status = $request->input('status') ?: "active";
        $student->acad_year = $request->input('acad_year') ?: date('Y');
        $student->email = $request->input('email');
        $student->telephone = $request->input('telephone');
        $student->index_number = $request->input('index_number');
        $student->updateVersion = updateVersionColumn('students');

        $save = $student->save();

        if ($save)
            return redirect(route('school.students.index'))->with(['status' => '1', 'message' => ucwords($request->input('fname')) . " Updated Successfully."]);
        else
            return redirect(route('school.students.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a student."]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $student = school(true)->students()->where('id', $id)->first();
        if (!$student)
            return abort(419);

        $student->status = "deleted";
        $student->updateVersion = updateVersionColumn('students');
        if ($student->save())
            $student->delete();


        return redirect(route('school.students.index'));
    }

    /**
     * View to Upload Students
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */

    public function uploadStudents()
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        return view("school.students.uploads.students");
    }

    /**
     * Saving Uploaded Students
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function uploadStudentsStore(UploadStudentsRequest $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

//        dd($request->file());
        $level_id = $request->input('level_id');
        $acadYear = $request->input('acad_year');
        $status = $request->input('status');

        if ($request->hasFile('file')) {
            $path = $request->file('file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                $insert = [];
                $leftover = [];
                $lo = 0;
                foreach ($data as $key => $value) {
                    $lo = $key + 2;
                    $insert[$lo] = [
                        'school' => school('id'),
                        'password' => bcrypt(generateReg($lo)),
                        'reg_no' => generateReg($lo++),
                        'lname' => $value->last_name,
                        'fname' => $value->first_name,
                        'email' => $value->email,
                        'level_id' => $level_id,
                        'gender' => in_array(ucwords($value->gender), ['Male', 'Female']) ? ucwords($value->gender) : null,
                        'mode' => in_array(ucfirst($value->mode), ['Boarding', 'Day']) ? ucwords($value->mode) : null,
                        'bdate' => date("Y-m-d", strtotime($value->bday)),
                        'nationality' => $value->nationality,
                        'province' => $value->province,
                        'district' => $value->district,
                        'sector' => $value->sector,
                        'cell' => $value->cell,
                        'village' => $value->village,
                        'ft_name' => $value->father_name,
                        'ft_phone' => $value->father_phone,
                        'mt_name' => $value->mother_name,
                        'mt_phone' => $value->mother_phone,
                        'gd_name' => $value->guardian_name,
                        'gd_phone' => $value->guardian_phone,
                        'sponsor' => $value->sponsor,
                        'acad_year' => $acadYear,
                        'status' => $status,
                        'updateVersion' => updateVersionColumn('students')
                    ];
                }

                if (!empty($insert)) {

                    foreach ($insert as $key => $std) {
                        $opt = 0;
                        if(strlen(trim($std['email'])) > 0 )
                            $opt = Student::where('email', $std['email'])->count();
                        if ($opt <= 0) {
                            Student::create($std);
                        } else {
                            $leftover[$key] = ['name' => $std['fname'] . " " . $std['lname'],
                                'gender' => $std['gender'],
                                'mode' => $std['mode'],
                                'email' => $std['email']];
                        }
                    }
                    if (empty($leftover)) {
                        return back()->with([
                            'status' => 1,
                            'message' => "All " . count($insert) . " Students were successfully imported.",
                            'students' => $insert
                        ]);
                    } else {
                        return back()->with([
                            'status' => 3,
                            'message' => count($insert) . " Students imported. " . count($leftover) . " Students not because of ready existing emails !",
                            'students' => $insert,
                            'leftovers' => $leftover
                        ]);
                    }
                }
            } else
                return back()->withInput()
                    ->with(['status' => '0', 'message' => 'What are you doing, uploading empty file for what purpose ?!']);


        }

    }

    /**
     * View to Upload Students Pictures
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */

    public function uploadPictures()
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        return view("school.students.uploads.picture");
    }

    /**
     * Saving Uploaded Pictures
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */

    public function uploadPicturesStore(Request $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $input = $request->all();
        $rules = array(
            'file' => 'image|max:3000',
        );

        $validation = \Validator::make($input, $rules);

        if ($validation->fails()) {
            return \Response::make($validation->errors->first(), 400);
        }

        $imagePath = 'students';
        $photoR = $request->file('file');
        $ext = $photoR->getClientOriginalExtension();
        $name = explode('.', $photoR->getClientOriginalName());
        $name = $name[0];

        if (Student::where('reg_no', $name)->count() <= 0)
            return \Response::make("No Student registered with $name !!", 400);
        else {
            $photo = $photoR->storeAs($imagePath, $name . '.' . $ext, config('voyager.storage.disk'));
            $upload_success = $photo;

            if ($upload_success) {
                return \Response::json('success', 200);
            } else {
                return \Response::json('error', 400);
            }
        }
    }

    public function studentList(Request $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $level = $request->level;
        $students = school(true)->students()->where('level_id', $level)->get();

        return view('school.students.student-list', compact('students', 'level'));
    }

    public function studentListP(Request $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $level = $request->level;
        $students = school(true)->students()->where('level_id', $level)->get();

        return view('school.students.student-parent-list', compact('students', 'level'));
    }

    public function assignCard(Request $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $student = school(true)->students()->where('id', $request->student);
        if ($student->count() <= 0)
            return redirect(route('school.students.index'))->with(['status' => '0', 'message' => "Student not find !!"]);

        $student = $student->first();
        if ($request->who == 's')
            $student->card = $request->card;
        elseif ($request->who == 'p')
            $student->pt_card = $request->card;

        $student->updateVersion = updateVersionColumn('students');

        if ($student->save())
            return redirect(route('school.students.index'))->with(['status' => '1', 'message' => "Card successfully added."]);
        else
            return redirect(route('school.students.index'))->with(['status' => '0', 'message' => "Fail to add card !!"]);
    }

    public function truncateTempCard(Request $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $who = $request->who;
        switch ($who) {
            case "s":
                $temp = school(true)->tempCards()->where('type', "st");
                if ($temp->count() > 0) {
                    foreach ($temp->get() as $item) {
                        $item->delete();
                    }
                }
                break;
            case "p":
                $temp = school(true)->tempCards()->where('type', "pa");
                if ($temp->count() > 0) {
                    foreach ($temp->get() as $item) {
                        $item->delete();
                    }
                }
                break;
        }
    }

    public function removeCard($id, $who)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $student = school(true)->students()->where('id', $id);
        if ($student->count() <= 0)
            return redirect(route('school.students.index'))->with(['status' => '0', 'message' => "Student not find !!"]);

        $student = $student->first();
        if ($who == 's')
            $student->card = null;
        elseif ($who == 'p')
            $student->pt_card = null;

        $student->updateVersion = updateVersionColumn('students');

        if ($student->save())
            return redirect(route('school.students.index'))->with(['status' => '1', 'message' => "Card successfully removed."]);
        else
            return redirect(route('school.students.index'))->with(['status' => '0', 'message' => "Fail to remove card !!"]);
    }

    public function getNewCard(Request $request)
    {
        if (!Gate::allows('isSchoolManager')){
            abort(404,'error page');

        }

        $who = $request->who;
        switch ($who) {
            case "s":
            case "st":
                $temp = school(true)->tempCards()
                    ->where('type', "st")
                    ->orderBy("created_at", "DESC");
                if ($temp->count() > 0)
                    return $temp->first()->card;
                break;
            case "p":
            case "pt":
                $temp = school(true)->tempCards()
                    ->where('type', "pt")
                    ->orderBy("created_at", "DESC");
                if ($temp->count() > 0)
                    return $temp->first()->card;
                break;
        }
        return "";
    }


}
