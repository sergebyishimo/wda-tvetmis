<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Department;
use App\Http\Requests\StoreClassRequest;
use App\Http\Requests\StoreDepartmentRequest;
use App\Http\Requests\UpdateStudentsCapRequest;
use App\Level;
use App\Qualification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('school.class.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClassRequest $request)
    {
        $school = school(true);
        $levels = $request->input('levels');
        $dep = $school->departments()->where('qualification_id', $request->input('qualification'));
        $depID = $dep->first()->id;
        if (count($levels) > 0) {
            foreach ($levels as $level) {
                Level::create([
                    'school_id' => school(true)->id,
                    'department_id' => $depID,
                    'rtqf_id' => trim($level),
                    'updateVersion' => updateVersionColumn('levels')
                ]);
            }
        }
        return redirect(route('school.class.index'))->with(['status' => '1', 'message' => 'Adding Levels Done Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level = Level::findOrFail($id);
        if ($level->school_id == school(true)->id)
            $level->delete();
        else
            return abort(403);

        return redirect(route('school.class.index'))->with(['status' => '3', 'message' => ucwords($level->rtqf->level_name) . ' successfully removed !!']);
    }


    public function createDepartment(StoreDepartmentRequest $request)
    {
        $data = [
            'school_id' => school(true)->id,
            'qualification_id' => $request->input('qualification'),
            'staffs_info_id' => $request->input('head'),
            'status' => true,
            'updateVersion' => updateVersionColumn('departments')
        ];

        $save = Department::create($data);

        if ($save)
            return redirect(route('school.class.index'))->with(['status' => '1', 'message' => 'Successfully Saved.']);

        return redirect(route('school.class.index'))->with(['status' => '0', 'message' => 'Saving Failed !']);

    }

    public function getRtqf(Request $request)
    {
        $id = $request->get('id');
        $q = Qualification::find($id);

        if ($q->rtqfs->count() > 0) {
            $l = [];
            foreach ($q->rtqfs()->orderBy('level_name', "ASC")->get() as $r) {
                $l[] = [
                    'id' => $r->id,
                    'name' => $r->level_name,
                    'checked' => checkIfHaveLevel($r->id, $id)
                ];
            }
            return json_encode($l);
        }
        return json_encode([]);
    }

    public function destroyDepartment(Request $request, $id)
    {
        $dept = Department::findOrFail($id);
        if ($dept->school_id == school(true)->id) {
            $dept->status = 0;
            $dept->updateVersion = updateVersionColumn('departments');
            $dept->save();
        } else
            return abort(403);

        return redirect(route('school.class.index'))->with(['status' => '3', 'message' => 'Successfully disabled !!']);
    }

    public function updateRoomsCapacity(Request $request, $id)
    {
        $dept = Department::findOrFail($id);
        if ($dept->school_id == school(true)->id) {
            $dept->capacity_rooms = $request->capacity_rooms;
            $dept->updateVersion = updateVersionColumn('departments');
            $dept->save();
        } else
            return abort(403);

        return redirect(route('school.class.index'))->with(['status' => '3', 'message' => 'Rooms Capacity Updated Successfully !!']);
    }

    public function updateStudentsCapacity(UpdateStudentsCapRequest $request, $id)
    {
        $dept = Department::findOrFail($id);
        if ($dept->school_id == school(true)->id) {
            $dept->capacity_students = $request->capacity_students;
            $dept->updateVersion = updateVersionColumn('departments');
            $dept->save();
        } else
            return abort(403);

        return redirect(route('school.class.index'))->with(['status' => '3', 'message' => 'Students Capacity Updated Successfully !!']);
    }

    public function getStudents($level)
    {
        $students = school(true)->students()
            ->where('level_id', $level)
            ->orderBy('fname', 'asc')->orderBy('lname', 'asc')
            ->get();
        return $students;
    }

}
