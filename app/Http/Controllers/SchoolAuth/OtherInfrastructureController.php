<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateOtherInfrastructureRequest;
use App\Http\Requests\UpdateOtherInfrastructureRequest;
use App\OtherInfrastructure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OtherInfrastructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $otherinfrastructures = school(true)->otherinfrastructures;
        return view('school.otherinfrastructure.index',compact('otherinfrastructures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $otherinfrastructure = null;
        return view('school.otherinfrastructure.create',compact('otherinfrastructure'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOtherInfrastructureRequest $request)
    {
        $otherinfrastructure = new OtherInfrastructure();
        $otherinfrastructure->school_id = school(true)->id;
        $otherinfrastructure->infrastructure_id = $request->input('infrastructure_id');
        $otherinfrastructure->quantity = $request->input('quantity');
        $otherinfrastructure->capacity = $request->input('capacity');
        $otherinfrastructure->construction_materials = $request->input('construction_materials');
        $otherinfrastructure->comment = $request->input('comment');
        $save = $otherinfrastructure->save();

        if ($save)
            return redirect(route('school.otherinfrastructure.index'))->with(['status' => '1', 'message' => "Other Infrastructure Added Successfully."]);
        else
            return redirect(route('school.otherinfrastructure.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Add Other Infrastructure."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $otherinfrastructure = school(true)->otherinfrastructures()
            ->where('id', $id)->first();
        if ($otherinfrastructure)
            return view("school.otherinfrastructure.create", compact("otherinfrastructure"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOtherInfrastructureRequest $request, $id)
    {
        $otherinfrastructure = school(true)->otherinfrastructures();
        if (!$otherinfrastructure)
            return abort(419);
        $otherinfrastructure->school_id = school(true)->id;
        $otherinfrastructure->infrastructure_id = $request->input('infrastructure_id');
        $otherinfrastructure->quantity = $request->input('quantity');
        $otherinfrastructure->capacity = $request->input('capacity');
        $otherinfrastructure->construction_materials = $request->input('construction_materials');
        $otherinfrastructure->comment = $request->input('comment');
        $save = $otherinfrastructure->save();

        if ($save)
            return redirect(route('school.otherinfrastructure.index'))->with(['status' => '1', 'message' => "Other Infrastructure Updated Successfully."]);
        else
            return redirect(route('school.otherinfrastructure.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Other Infrastructure."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $otherinfrastructure = school(true)->otherinfrastructures()->where('id', $id)->first();
        if (!$otherinfrastructure)
            return abort(419);
        $otherinfrastructure->delete();

        return redirect(route('school.otherinfrastructure.index'));
    }
}
