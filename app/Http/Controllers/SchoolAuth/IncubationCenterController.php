<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateIncubationCenterRequest;
use App\Http\Requests\UpdateIncubationCenterRequest;
use App\IncubationCenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncubationCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incubationcenters = school(true)->incubationcenters;
        return view('school.incubationcenter.index',compact('incubationcenters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $incubationcenter = null;
        return view('school.incubationcenter.create',compact('incubationcenter'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateIncubationCenterRequest $request)
    {
        $filePath = 'incubations';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachments')) {
            $fileR = $request->file('attachments');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id.'_'.Date('Y_m_d_H_m_s'). '.' . $ext, config('voyager.storage.disk'));
        }

        $incubationcenter = new IncubationCenter();
        $incubationcenter->school_id = school(true)->id;
        $incubationcenter->club_name = $request->input('club_name');
        $incubationcenter->club_facilitator = $request->input('club_facilitator');
        $incubationcenter->incubatees_male = $request->input('incubatees_male');
        $incubationcenter->incubatees_female = $request->input('incubatees_female');
        $incubationcenter->average_age = $request->input('average_age');
        $incubationcenter->business_areas = $request->input('business_areas');
        $incubationcenter->annual_turn_over_francs = $request->input('annual_turn_over_francs');
        $incubationcenter->funds_source = $request->input('funds_source');
        $incubationcenter->rdb_registration_number = $request->input('rdb_registration_number');
        $incubationcenter->number_incubated_clubs = $request->input('number_incubated_clubs');
        $incubationcenter->attachments = $file;
        $save = $incubationcenter->save();

        if ($save)
            return redirect(route('school.incubationcenters.index'))->with(['status' => '1', 'message' => "Incubation Center  Added Successfully."]);
        else
            return redirect(route('school.incubationcenters.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Add Incubation Center."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $incubationcenter = school(true)->incubationcenters()
            ->where('id', $id)->first();
        if ($incubationcenter)
            return view("school.incubationcenter.create", compact("incubationcenter"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateIncubationCenterRequest $request, $id)
    {
        $incubationcenter = school(true)->incubationcenters()
            ->where('id', $id)->first();
        if (!$incubationcenter)
            return abort(419);

        $filePath = 'incubations';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachments')) {
            $fileR = $request->file('attachments');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id.'_'.Date('Y_m_d_H_m_s'). '.' . $ext, config('voyager.storage.disk'));
        }

        $incubationcenter->school_id = school(true)->id;
        $incubationcenter->club_name = $request->input('club_name');
        $incubationcenter->club_facilitator = $request->input('club_facilitator');
        $incubationcenter->incubatees_male = $request->input('incubatees_male');
        $incubationcenter->incubatees_female = $request->input('incubatees_female');
        $incubationcenter->average_age = $request->input('average_age');
        $incubationcenter->business_areas = $request->input('business_areas');
        $incubationcenter->annual_turn_over_francs = $request->input('annual_turn_over_francs');
        $incubationcenter->funds_source = $request->input('funds_source');
        $incubationcenter->rdb_registration_number = $request->input('rdb_registration_number');
        $incubationcenter->number_incubated_clubs = $request->input('number_incubated_clubs');
        $incubationcenter->attachments = $file;

        if($file){
            $incubationcenter->request_attachment = $file;
        }
        $save = $incubationcenter->save();

        if ($save)
            return redirect(route('school.incubationcenters.index'))->with(['status' => '1', 'message' => "Incubation Center Updated Successfully."]);
        else
            return redirect(route('school.incubationcenters.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Incubation Center."]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $incubationcenter = school(true)->incubationcenters()->where('id', $id)->first();
        if (!$incubationcenter)
            return abort(419);
        $incubationcenter->delete();

        return redirect(route('school.incubationcenter.index'));
    }
}
