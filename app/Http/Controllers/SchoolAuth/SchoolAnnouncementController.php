<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateSchoolAnnouncementRequest;
use App\Http\Requests\UpdateSchoolAnnouncementRequest;
use App\SchoolAnnouncement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolAnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements = school(true)->announcements;
        return view('school.announcements.index', compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $announcement = null;
        return view('school.announcements.create', compact('announcement'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSchoolAnnouncementRequest $request)
    {

        $announcement = new SchoolAnnouncement();
        $announcement->school_id = school(true)->id;
        $announcement->announcement = $request->input('announcement');
        $save = $announcement->save();

        if ($save)
            return redirect(route('school.announcements.index'))->with(['status' => '1', 'message' => "Announcements  Sent Successfully."]);
        else
            return redirect(route('school.announcements.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Send a Announcements."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement = school(true)->announcements()
            ->where('id', $id)->first();
        if ($announcement)
            return view("school.announcements.create", compact("announcement"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSchoolAnnouncementRequest $request, $id)
    {
        $announcement = school(true)->announcements()
            ->where('id', $id)->first();
        if (!$announcement)
            return abort(419);
        $announcement->school_id = school(true)->id;
        $announcement->announcement = $request->input('announcement');
        $save = $announcement->save();

        if ($save)
            return redirect(route('school.announcements.index'))->with(['status' => '1', 'message' => "Announcement Updated Successfully."]);
        else
            return redirect(route('school.announcements.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update  Announcement."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcement = school(true)->announcements()->where('id', $id)->first();
        if (!$announcement)
            return abort(419);
        $announcement->delete();

        return redirect(route('school.announcements.index'));
    }
}
