<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateSchoolRequestsRequest;
use App\Http\Requests\UpdateSchoolRequestsRequest;
use App\SchoolRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class
SchoolRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schoolrequests = school(true)->requests;
        return view('school.requests.index',compact('schoolrequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schoolrequest = null;
        return view('school.requests.create',compact('schoolrequest'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSchoolRequestsRequest $request)
    {
        $filePath = 'requests';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('request_attachment')) {
            $fileR = $request->file('request_attachment');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id .Date('Y_m_d_H_m_s'). '.' . $ext, config('voyager.storage.disk'));
        }

        $schoolrequest = new SchoolRequest();
        $schoolrequest->school_id = school(true)->id;
        $schoolrequest->request_class = $request->input('request_class');
        $schoolrequest->required_units = $request->input('required_units');
        $schoolrequest->unit_cost = $request->input('unit_cost');
        $schoolrequest->request_description = $request->input('required_by_date');
        $schoolrequest->required_by_date = $request->input('required_by_date');
        $schoolrequest->request_attachment = $file;
        $save = $schoolrequest->save();

        if ($save)
            return redirect(route('school.requests.index'))->with(['status' => '1', 'message' => "Request  Sent Successfully."]);
        else
            return redirect(route('school.requests.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Send a request."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schoolrequest = school(true)->requests()
            ->where('id', $id)->first();
        if ($schoolrequest)
            return view("school.requests.create", compact("schoolrequest"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSchoolRequestsRequest $request, $id)
    {

        $schoolrequest = school(true)->requests()
            ->where('id', $id)->first();
        if (!$schoolrequest)
            return abort(419);

        $filePath = 'requests';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('request_attachment')) {
            $fileR = $request->file('request_attachment');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id .Date('Y_m_d_H_m_s'). '.' . $ext, config('voyager.storage.disk'));
        }

        $schoolrequest->school_id = school(true)->id;
        $schoolrequest->request_class = $request->input('request_class');
        $schoolrequest->required_units = $request->input('required_units');
        $schoolrequest->unit_cost = $request->input('unit_cost');
        $schoolrequest->request_description = $request->input('required_by_date');
        $schoolrequest->required_by_date = $request->input('required_by_date');
        if($file){
            $schoolrequest->request_attachment = $file;
        }
        $save = $schoolrequest->save();

        if ($save)
            return redirect(route('school.requests.index'))->with(['status' => '1', 'message' => "Request Updated Successfully."]);
        else
            return redirect(route('school.requests.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update a Request."]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = school(true)->requests()->where('id', $id)->first();
        if (!$request)
            return abort(419);
        $request->delete();

        return redirect(route('school.requests.index'));
    }
}
