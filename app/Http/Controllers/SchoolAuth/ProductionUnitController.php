<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateProductionUnitRequest;
use App\Http\Requests\UpdateProductionUnitRequest;
use App\ProductionUnit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductionUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productionunits = school(true)->productionunits;

        return view('school.productionunit.index',compact('productionunits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productionunit = null;
        return view('school.productionunit.create',compact('productionunit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductionUnitRequest $request)
    {
        $filePath = 'productionunit';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachments')) {
            $fileR = $request->file('attachments');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id.'_'.Date('Y_m_d_H_m_s'). '.' . $ext, config('voyager.storage.disk'));
        }

        $productionunit = new ProductionUnit();
        $productionunit->school_id = school(true)->id;
        $productionunit->production_unit_name = $request->input('production_unit_name');
        $productionunit->operation_field = $request->input('operation_field');
        $productionunit->production_unit_capital_francs = $request->input('production_unit_capital_francs');
        $productionunit->annual_turnover_francs = $request->input('annual_turnover_francs');
        $productionunit->monthly_revenues_francs = $request->input('monthly_revenues_francs');
        $productionunit->employees_male = $request->input('employees_male');
        $productionunit->employees_female = $request->input('employees_female');
        $productionunit->funds_source = $request->input('funds_source');
        $productionunit->rdb_registration_number = $request->input('rdb_registration_number');
        $productionunit->community_impact = $request->input('community_impact');
        $productionunit->attachments = $file;
        $save = $productionunit->save();

        if ($save)
            return redirect(route('school.productionunit.index'))->with(['status' => '1', 'message' => "Production Unit Added Successfully."]);
        else
            return redirect(route('school.productionunit.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to Add Production Unit."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productionunit = school(true)->productionunits()
            ->where('id', $id)->first();
        if ($productionunit)
            return view("school.productionunit.create", compact("productionunit"));
        else
            return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductionUnitRequest $request, $id)
    {
        $productionunit = school(true)->productionunits()
            ->where('id', $id)->first();
        if (!$productionunit)
            return abort(419);

        $filePath = 'productionunit';
        $school_id = $request->input('school_id');
        $file = null;
        if ($request->has('attachments')) {
            $fileR = $request->file('attachments');
            $ext = $fileR->getClientOriginalExtension();
            $file = $fileR->storeAs($filePath, $school_id.'_'.Date('Y_m_d_H_m_s'). '.' . $ext, config('voyager.storage.disk'));
        }

        $productionunit->school_id = school(true)->id;
        $productionunit->production_unit_name = $request->input('production_unit_name');
        $productionunit->operation_field = $request->input('operation_field');
        $productionunit->production_unit_capital_francs = $request->input('production_unit_capital_francs');
        $productionunit->annual_turnover_francs = $request->input('annual_turnover_francs');
        $productionunit->monthly_revenues_francs = $request->input('monthly_revenues_francs');
        $productionunit->employees_male = $request->input('employees_male');
        $productionunit->employees_female = $request->input('employees_female');
        $productionunit->funds_source = $request->input('funds_source');
        $productionunit->rdb_registration_number = $request->input('rdb_registration_number');
        $productionunit->community_impact = $request->input('community_impact');

        if($file){
            $productionunit->attachments = $file;
        }
        $save = $productionunit->save();

        if ($save)
            return redirect(route('school.productionunit.index'))->with(['status' => '1', 'message' => "Production Unit Updated Successfully."]);
        else
            return redirect(route('school.productionunit.index'))->withInput()
                ->with(['status' => '0', 'message' => "Failed to update Production Unit."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productionunit = school(true)->productionunits()->where('id', $id)->first();
        if (!$productionunit)
            return abort(419);
        $productionunit->delete();

        return redirect(route('school.productionunit.index'));
    }
}
