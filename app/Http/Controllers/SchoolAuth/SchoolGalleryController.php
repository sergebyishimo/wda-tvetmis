<?php

namespace App\Http\Controllers\SchoolAuth;

use App\Model\Accr\SchoolInformation;
use App\SchoolGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schools = SchoolInformation::all()->pluck('school_name', 'id');

        $galleries = [];
        if ($request->school_id) {
            $school_id = $request->school_id;
            $galleries = SchoolGallery::where('school_id', $request->school_id)->select(['id', 'description' , 'created_at'])->groupBy('description')->get();
            return view('school.gallery.index', compact('galleries', 'schools', 'school_id'));
        }
        return view('school.gallery.index', compact('schools'));





    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->photos as $photo) {
            $fileName = str_replace(' ', '_', $photo->getClientOriginalName());
            $fileName = str_replace(',', '', $fileName);
            $fileName = time() . $fileName;
            $imagename = $photo->storeAs('gallery', $fileName, 'public');
            SchoolGallery::create([
                'school_id' => $request->school_id,
                'photo' => $imagename,
                'description' => $request->description
            ]);
        }

        return redirect()->back()->with(['status' => 1, 'message' => 'Photos Uploaded Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param $description
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show($description, Request $request)
    {
        $photos = SchoolGallery::where('school_id', $request->school_id)->where('description', $description)->get();
        return view('school.gallery.show', compact('photos', 'description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($description)
    {
        SchoolGallery::where('description', $description)->delete();
        return redirect()->back();
    }

    public function deleteOne($id) {
        SchoolGallery::where('id', $id)->delete();
        return redirect()->back();
    }
}
