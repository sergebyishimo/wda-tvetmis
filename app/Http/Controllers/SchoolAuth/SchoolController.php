<?php
/**
 * Created by PhpStorm.
 * User: hirwaf
 * Date: 5/13/18
 * Time: 4:14 AM
 */

namespace App\Http\Controllers\SchoolAuth;

use App\Http\Requests\CreateNewSchoolRequest;
use App\Http\Requests\UpdateExistingSchoolRequest;
use App\Http\Requests\UpdateSchoolInfo;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SchoolRating;
use App\Model\Accr\SchoolType;
use App\Model\Accr\SourceSchoolOwnership;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SchoolUser;
use Auth;
use DB;
use Hash;
use File;
use Storage;

class SchoolController extends Controller
{
    protected $guard = "school";

    function __construct()
    {
        $this->middleware('school');
    }

    public function home()
    {
        $users[] = Auth::user();
        $users[] = Auth::guard()->user();
        $users[] = Auth::guard('school')->user();

        $settings = school(true)->settings;
        trigerSchoolUpdateVersion();
//        $students_nber = Student::where('acad_year', date('Y'))->count() + 1;
//        $reg_no        = date('Y') . '-' . $students_nber;
//        $next_staff_id = User::all()->count() + 1;
//
//        $deps       = Department::where('status', '!=', 'deleted')->orderBy('name', 'asc')->get();
//        $levels     = Level::where('status', '!=', 'deleted')->orderBy('name', 'asc')->get();
//        $faults     = Fault::all();
//
//        $school = School::all();


//        return view('home', ['privileges' => $GLOBALS['privileges'], 'departments' => $deps, 'levels' => $levels, 'reg_no' => $reg_no, 'next_staff_id' => $next_staff_id, 'settings' => $settings[0], 'faults' => $faults, 'school' => $school[0]]);
        return view('school.home', compact('settings'));
    }

    /**
     * Settings Update
     */

    public function settingsUpdate(Request $request)
    {
        $save = school(true)->settings->update([
            'active_term' => $request->term,
            'active_period' => $request->period,
            'updateVersion' => updateVersionColumn('school_settings')
        ]);
        if ($save)
            return redirect()->intended(route('school.home'))->with(['message' => 'Settings Updated Successfully!!', 'status' => '1']);
        else
            return back()->with(['message' => 'Updating Settings Failed !!', 'status' => '0']);
    }

    /**
     * Updating Password
     */

    public function passwordCheck($pass)
    {

        if (Hash::check($pass, Auth::user()->password)) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function passwordChange(Request $request)
    {
        if (Hash::check($request->old_pass, Auth::user()->password)) {
            if ($request->new_pass == $request->conf_pass) {

                $user = SchoolUser::findOrFail(Auth::guard('school')->user()->id);
                if ($user->privilege == 2) {
                    $school = school(true);
                    $school->password = $request->new_pass;
//                    $school->updateVersion = updateVersionColumn('schools');
                    $school->save();
                }
                $user->password = bcrypt($request->new_pass);
                $user->android_pass = sha1(trim($request->new_pass));
                $user->updateVersion = updateVersionColumn('school_users');
                $user->save();

                return back();
            } else {
                return redirect()->intended('/home')->withErrors(['status' => '0', 'message' => 'The passwords you entered doesn\'t match!!']);
            }
        } else {
            return redirect()->intended('/home')->withErrors(['status' => '0', 'message' => 'The old password you entered is invalid!! ']);
        }
    }

    /**
     * Updating School Info
     */

    public function getinfo()
    {
        $school = school(true);
        $ownerships = SourceSchoolOwnership::all();
        $schoolTypes = SchoolType::all();
        $schoolRating = SchoolRating::all();
        $provinces = getProvince();
//        dd($provinces);
//        dd($settings);
        return view('school.settings.info', compact('school', 'ownerships', 'schoolTypes', 'schoolRating', 'provinces'));
    }

    public function updateInfo(UpdateExistingSchoolRequest $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:accr_mysql.schools_information,email,' . school(true)->id,
            'school_code' => 'required|unique:accr_mysql.schools_information,school_code,' . school(true)->id
        ]);
        $school = SchoolInformation::find(school(true)->id);

//        $school->name = $request->name;
//        $school->acronym = $request->acronym;
//        $school->phone_number = $request->phone_number;
//        $school->email = $request->email;
//        $school->website = $request->website;
//        $school->discipline_max = $request->discipline_max;
//        $school->province = $request->province;
//        $school->district = $request->district;
//        $school->sector = $request->sector;
//        $school->logo = $logo;
//        $school->mid = $request->mid;
        $school->school_name = $request->school_name;
        $school->school_acronym = $request->school_acronym;
        $school->school_code = $request->school_code;

        $school->province = $request->province;
        $school->district = $request->district;
        $school->sector = $request->sector;
        $school->cell = $request->cell;
        $school->village = $request->village;
        $school->latitude = $request->latitude;
        $school->longitude = $request->longitude;
        $school->phone = $request->phone;
        $school->email = $request->email;

        $school->owner_name = $request->owner_name;
        $school->owner_phone = $request->owner_phone;
        $school->owner_type = $request->owner_type;
        $school->owner_email = $request->owner_email;
        $school->manager_name = $request->manager_name;
        $school->manager_phone = $request->manager_phone;
        $school->manager_email = $request->manager_email;

        $school->website = $request->website;

        $school->has_electricity = $request->has_electricity;
        $school->has_water = $request->has_water;
        $school->has_computer_lab = $request->has_computer_lab;
        $school->has_internet = $request->has_internet;
        $school->has_library = $request->has_library;
        $school->has_business_or_strategic_plan = $request->has_business_or_strategic_plan;
        $school->has_feeding_program = $request->has_feeding_program;

        $school->date_of_establishment = $request->date_of_establishment;
        $school->latitude = $request->latitude;
        $school->phone = $request->phone;
        $school->email = $request->email;
        $school->owner_name = $request->owner_name;
        $school->owner_phone = $request->owner_phone;
        $school->owner_type = $request->owner_type;
        $school->owner_email = $request->owner_email;
        $school->boarding_or_day = $request->boarding_or_day;

        $path = '/schools/logos/';
        if ($request->file('school_logo')) {
            $photo = $request->file('school_logo');
            $photoP = $path;
            $logo = Storage::disk(config('voyager.storage.disk'))->put($photoP, $photo, 'public');
            $school->school_logo = $logo;
        }

        $school->number_of_desktops = $request->number_of_desktops;
        $school->number_of_classrooms = $request->number_of_classrooms;
        $school->students_males = $request->students_males;
        $school->staff_male = $request->staff_male;
        $school->staff_female = $request->staff_female;
        $school->number_of_generators = $request->number_of_generators;

        $school->save();

        return redirect()->route('school.get.info')
            ->with(['status' => '1', 'message' => 'School Information Updated Successfully!!']);
    }

}