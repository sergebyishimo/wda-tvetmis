<?php

namespace App\Http\Controllers;

use App\Combination;
use App\Model\Accr\SchoolInformation;
use App\NationalExamResult;
use App\OldCourse;
use App\Student;
use App\WdaGrade;
use App\WeightScale;
use Illuminate\Http\Request;

class ExperimentalExamResultController extends Controller
{
    public function province(){
        $result = NationalExamResult::all();
        $result->groupBy('student_index');
        return response()->json($result);
    }
    public function schools(){
        $result = SchoolInformation::all();
        return response()->json($result);
    }
    public function students(){
        $result = Student::all();
        return response()->json($result);
    }
    public function courses(){
        $result = OldCourse::all();
        return response()->json($result);
    }
    public function programs(){
        $result = Combination::all();
        return response()->json($result);
    }
    public function grades(){
        $result = WdaGrade::all();
        return response()->json($result);
    }
    public function ws(){
        $result = WeightScale::all();
        return response()->json($result);
    }
}
