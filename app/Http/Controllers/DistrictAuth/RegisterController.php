<?php

namespace App\Http\Controllers\DistrictAuth;

use App\District;
use App\Mail\sendDistrictAccountInformation;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/wda/district';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['wda']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:districts',
            'password' => 'required|min:6|confirmed',
            'district' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return District
     */
    protected function create(array $data)
    {
        return  $user = District::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'district' => $data['district'],
            'password' => bcrypt($data['password']),
        ]);
//        $user->assignRole('District');
//
//        return $user ;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()
                ->route('wda.district.index')
                ->withErrors($validator)
                ->withInput();
        }


        event(new Registered($user = $this->create($request->all())));
        $user->assignRole('District');

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'district' => $request->district,
            'password' => $request->password,
        ];

        if ($user)
            Mail::to($request->email)->send(new sendDistrictAccountInformation($data));


//        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: back()->with(['status' => '1', 'message' => $request->district.'\'s account created successfully . ']);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('district.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('district');
    }
}
