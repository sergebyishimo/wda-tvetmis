<?php

namespace App\Http\Controllers\DistrictAuth;

use App\Model\Accr\SchoolInformation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('district');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Auth::guard('district')->user()->district;
        $schools_nber = SchoolInformation::where('district', $users)->count();
        $have_internet = SchoolInformation::where('has_internet', 2)->where('district', $users)->count();
        $have_electricity = SchoolInformation::where('has_electricity', 2)->where('district', $users)->count();
        $have_water = SchoolInformation::where('has_water', 2)->where('district', $users)->count();


        return view('district.home', compact('schools_nber', 'have_internet', 'have_electricity', 'have_water'));
    }
}
