<?php

namespace App\Http\Controllers\DistrictAuth;

use App\Model\Accr\AccrApplication;
use App\Model\Accr\AccrAttachment;
use App\Model\Accr\AccrBuildingsAndPlots;
use App\Model\Accr\AccrCriteriaAnswer;
use App\Model\Accr\AccrCriteriaSection;
use App\Model\Accr\AccrQualificationsApplied;
use App\Model\Accr\AccrSchoolAssessmentComment;
use App\Model\Accr\DistrictProvisionalAQA;
use App\Model\Accr\Functions;
use App\Model\Accr\Graduate;
use App\Model\Accr\ProgramsOffered;
use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SchoolSelfAssessimentStatus;
use App\Model\Accr\StaffAdministrator;
use App\Model\Accr\Trainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProvisionalAQAController extends Controller
{
    private  $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('district');
    }

    private function user($col=null)
    {
        $user = auth()->guard('district')->user();

        if (isset($user->{$col}) && $col != null)
            return $user->{$col};

        return $user;
    }

    public function view(Request $request)
    {
        $apps = AccrApplication::join('schools_information', 'schools_information.id', '=', 'accr_applications.school_id')
            ->where('schools_information.district', '=', $this->user('district'))
            ->select('schools_information.school_name', 'schools_information.district', 'accr_applications.*')
            ->get();

        return view('district.view', compact('apps'));
    }

    public function viewMore($application_id, Request $request)
    {
        if (isset($request->view_year)) {
            $graduates = Graduate::where('school_id', auth()->user()->school_id)->where('year', $request->view_year)->get();
        } else {
            $graduates = [];
        }

        $school_info = SchoolInformation::where('id', auth()->user()->school_id)->first();
        $attachments_data = AccrAttachment::where('application_id', $application_id)->get();
        $buildings_plots = AccrBuildingsAndPlots::where('school_id', auth()->user()->school_id)->get();
        $programs_offered = ProgramsOffered::where('school_id', auth()->user()->school_id)->get();
        $trainers = Trainer::where('school_id', auth()->user()->school_id)->get();
        $staffs = StaffAdministrator::where('school_id', auth()->user()->school_id)->get();
        $application_data = AccrQualificationsApplied::where('application_id', $application_id)->get();

        $criteria_sections = AccrCriteriaSection::get();

        $app = AccrApplication::find($application_id);

        return view('district.view_more', compact('school_info', 'attachments_data', 'buildings_plots', 'programs_offered', 'trainers', 'staffs', 'application_data', 'application_id', 'criteria_sections', 'graduates'));
    }

    public function saveData(Request $request)
    {
        if (isset($request->form)) {
            switch ($request->form) {
                case 'attachments':

                    if (isset($request->verified)) {
                        for ($i = 0; $i < count($request->verified); $i++) {
                            $att = $request->class::find($request->attachment_id[$i]);
                            $att->district_verified = $request->verified[$i];
                            $att->save();
                        }
                    }

                    $check_update = DistrictProvisionalAQA::where('application_id', $request->application_id)->where('section', $request->section)->where('step', $request->step)->first();


                    if ($check_update) {
                        $check = DistrictProvisionalAQA::find($check_update->id);
                    } else {
                        $check = new DistrictProvisionalAQA();
                        $check->application_id = $request->application_id;
                        $check->section = $request->section;
                        $check->step = $request->step;
                    }

                    $check->strength = $request->strength;
                    $check->weakness = $request->weakness;
                    $check->threat = $request->threat;
                    $check->opportunity = $request->opportunity;
                    $check->recommendation = $request->recommendation;
                    $check->marks = $request->marks;

                    $check->save();

                    return redirect('/district/accreditation/view/1/#step-' . ($request->step + 1));

                    break;

                case 'general_recommendation':

                    $check_update = DistrictProvisionalAQA::where('application_id', $request->application_id)->where('section', 1)->where('step', 8)->first();

                    if ($check_update) {
                        $check = DistrictProvisionalAQA::find($check_update->id);
                    } else {
                        $check = new DistrictProvisionalAQA();
                        $check->application_id = $request->application_id;
                        $check->section = 1;
                        $check->step = 8;
                    }

                    $check->recommendation = $request->recommendation;

                    $check->save();

                    return redirect('/district/accreditation/view/1/#step-' . ($request->step + 1));

                    break;
            }
        }
    }

    public function assessmentView()
    {
        $submitedInputs = SchoolSelfAssessimentStatus::where('academic_year', date('Y'))
            ->where('function_id', 1)
            ->orderBy('created_at', 'ASC')
            ->get();

        $submitedProcess = SchoolSelfAssessimentStatus::where('academic_year', date('Y'))
            ->where('function_id', 2)
            ->orderBy('created_at', 'ASC')
            ->get();
        $functionInput = Functions::findOrFail(1);
        $functionProcess = Functions::findOrFail(2);

        $qualityInput = $functionInput->qualityAreas;
        $qualityProcess = $functionProcess->qualityAreas;

        $submited = SchoolSelfAssessimentStatus::where('academic_year', date('Y'))
            ->orderBy('created_at', 'ASC')
            ->get();

        return view('district.assessment-view', compact('submited','submitedInputs', 'submitedProcess', 'qualityInput', 'qualityProcess'));
    }

    public function assessmentViewSchool($fun, $school)
    {
        $funct = Functions::findOrFail($fun);
        $school = SchoolInformation::findOrFail($school);
        $qualities = [];
        if ($funct->qualityAreas)
            $qualities = $funct->qualityAreas;

        return view('district.view-school-assessment', compact('school', 'funct', 'qualities'));
    }

    public function storeAssessmentSchool(Request $request, $fun, $school)
    {
        $this->validate($request, [
            'school_id' => 'required',
            'function_id' => 'required',
            'quality_id' => 'required',
            'strength' => 'required|min:2',
            'weakness' => 'required|min:2',
            'recommendation' => 'required|min:2',
            'timeline' => 'required|min:2',
            'district.*' => 'required'
        ]);

        $district = $request->district;
        $school = $request->school_id;

        $request->request->add(['from' => 'district', 'academic_year' => date('Y')]);
        $dataComment = $request->except(['district', '_token']);

        $vldD = 0;
        if (count($district)) {
            foreach ($district as $criteria => $decision) {
                $answer = AccrCriteriaAnswer::where('school_id', $school)
                    ->where('academic_year', date('Y'))
                    ->where('criteria_id', $criteria)
                    ->first();
                if ($answer) {
                    $answer->district = $decision;
                    if ($answer->save())
                        $vldD++;
                }
            }
        }
        $comment = getCommentsDistrict($school, $request->function_id, $request->quality_id, null, true);
        if ($comment === false)
            AccrSchoolAssessmentComment::create($dataComment);
        else {
            if ($comment->update($dataComment))
                return back()->with(['successMessage' => 'Successfully updated .']);
        }

        return back()->with(['successMessage' => 'Successfully saved .']);
    }
}
