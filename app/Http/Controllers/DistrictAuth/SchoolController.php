<?php

namespace App\Http\Controllers\DistrictAuth;

use App\Model\Accr\SchoolInformation;
use App\Model\Accr\SourceSchoolOwnership;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SchoolController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = Auth::guard('district')->user();
    }

    public function view()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::all();

        return view('district.schools.view', compact('schools', 'ownerships'));
    }

    public function internet()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::where('has_internet', 2)->get();

        return view('district.schools.view', compact('schools', 'ownerships'));
    }

    public function electricity()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::where('has_electricity', 2)->get();

        return view('district.schools.view', compact('schools', 'ownerships'));
    }

    public function water()
    {
        $ownerships = SourceSchoolOwnership::all();
        $schools = SchoolInformation::where('has_water', 2)->get();

        return view('district.schools.view', compact('schools', 'ownerships'));
    }

    public function viewDetails($id)
    {
        $school = SchoolInformation::find($id);
        return view('district.schools.viewDetails', compact('school'));
    }
}
