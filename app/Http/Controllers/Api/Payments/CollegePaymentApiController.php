<?php

namespace App\Http\Controllers\Api\Payments;

use App\Model\Rp\AdmissionPrivate;
use App\Rp\AdmittedStudent;
use App\Rp\CollegeStudent;
use App\Rp\Payment;
use App\Rp\PaymentInvoice;
use App\Rp\PendingPaymentInvoice;
use App\Rp\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollegePaymentApiController extends Controller
{
    protected $api_token = "Nz99qQ-Bkx9dYiSbXoA32zOzs12cDk";

    public function __construct()
    {

    }

    private function authorized($token)
    {
        $_token = $this->api_token;

        if ($token !== $_token)
            return false;

        return true;
    }

    private function activeModel($student)
    {
        $padm = AdmissionPrivate::where('std_id', $student)->first();
        $adm = AdmittedStudent::where('std_id', $student)->first();
        $cst = CollegeStudent::where('student_reg', $student)->first();

        if ($padm)
            return $padm;
        elseif ($adm)
            return $adm;
        elseif ($cst)
            return $cst;
        else
            return null;
    }

    private function checkStudent($student)
    {

        $padm = AdmissionPrivate::where('std_id', $student)->first();
        $adm = AdmittedStudent::where('std_id', $student)->first();
        $cst = CollegeStudent::where('student_reg', $student)->first();

        if (!$padm) {
            if (!$adm) {
                if (!$cst)
                    return false;
            }
        }

        return true;
    }

    private function getCollege($student)
    {
        if (!$this->checkStudent($student))
            return "";
        $model = $this->activeModel($student);
        $college = getCollege($model->college_id, 'short_name');

        return $college;
    }

    private function getPendingInvoices($student, $code = null)
    {
        if (!is_null($code))
            $invoices = PendingPaymentInvoice::where('code', '=', $code)
                ->where('active', '=', '1');
        else
            $invoices = PendingPaymentInvoice::where('student_id', '=', $student)
                ->where('active', '=', '1');
        $rInvoices = [];
        if ($invoices->count() > 0) {
            foreach ($invoices->get() as $invoice) {
                $amount = $invoice->amount - $invoice->paid;
                $rInvoices[] = [
                    'invoice_id' => $invoice->code,
                    'category' => $invoice->name,
                    'partial' => $invoice->partial == false ? 0 : 1,
                    'amount' => $amount,
                ];
            }
        }
        return $rInvoices;
    }

    public function getParticularInvoice($code, $student = null, $col = "")
    {
        $invoice = PendingPaymentInvoice::where('student_id', '=', $student)
            ->where('code', $code)->first();

        if ($student == null)
            $invoice = PendingPaymentInvoice::where('code', $code)->first();

        if ($invoice)
            return $col != "" ? $invoice->{$col} : $invoice;
        else
            return null;
    }

    private function studentNames($student)
    {
        return $this->activeModel($student)->names;
    }

    public function invoices($token, $student)
    {
        if ($this->authorized($token)) {
            if ($this->checkStudent($student)) {
                $invoices = $this->getPendingInvoices($student);
                if (!empty($invoices)) {
                    $response = [];
                    $response['status'] = 200;
                    $response['student'] = $student;
                    $response['college'] = $this->getCollege($student);
                    $response['college_id'] = $this->activeModel($student)->college_id;
                    $response['name'] = $this->studentNames($student);
                    $response['invoices'] = $invoices;
                    return json_encode($response);
                } else
                    return json_encode(['status' => '403', 'message' => "There is no payment submitted !"]);
            } else
                return json_encode(['status' => '404', 'message' => 'StudentID is not valid !!']); #Payments is not yet open
        }
        return response(json_encode("unauthorized access"), 401);
    }

    public function invoice($token, $code)
    {
        if ($this->authorized($token)) {
            $student = $this->getParticularInvoice($code, null, 'student_id');
            if ($this->checkStudent($student)) {
                $invoices = $this->getPendingInvoices($student, $code);
                if (!empty($invoices)) {
                    $response = [];
                    $response['status'] = 200;
                    $response['college'] = $this->getCollege($student);
                    $response['college_id'] = $this->activeModel($student)->college_id;
                    $response['name'] = $this->studentNames($student);
                    $response['invoice'] = $invoices;
                    return json_encode($response);
                } else
                    return json_encode(['status' => '403', 'message' => "There is no payment submitted !"]);
            } else
                return json_encode(['status' => '404', 'message' => 'StudentID is not valid !']); # Registration is not yet open
        }
        return response(json_encode("unauthorized access"), 401);
    }

    public function feedback(Request $request)
    {
        if ($this->authorized($request->token)) {
            $student = $request->customerid;
            $invoice_id = $request->invoice_id;
            $status = $request->status;
            $amount = $request->amountcredit;
//            if ($status == 1) {

            if ($this->forRegistration($invoice_id, $student))
                $this->updateStudentIfRegistration($student);

            $this->updatePendingInvoice($student, $invoice_id, $amount);
            $this->recordHistory($request);

            return json_encode(['status' => 200, 'message' => "Payment Done Successfully"]);
//            }
        }
        return response(json_encode("unauthorized access"), 401);
    }

    private function forRegistration($invoice, $student)
    {
        $nnn = "";
        $invoiceName = $this->getParticularInvoice($invoice, $student, 'name');
        if ($invoiceName)
            $nnn = str_slug(strtolower($invoiceName), "_");

        if ($nnn == "registration" OR $nnn == "registration_fees" OR $nnn ="application" OR $nnn ="application_fees")
            return true;

        return false;
    }

    private function updateStudentIfRegistration($student)
    {
        $std = Student::where('id', $student)->first();
        if ($std) {
            $std->payments = 1;
            $std->save();
        }
    }

    private function updatePendingInvoice($student, $code, $paid)
    {
        $invoice = $this->getParticularInvoice($code, $student);
        if ($invoice) {
            $expected = $invoice->amount;
            $paid = $invoice->paid + $paid;
            $active = 1;
            if ($paid >= $expected)
                $active = 0;

            $closed = $active == 1 ? 0 : 1;

            $invoice->active = $active;
            $invoice->paid = $paid;
            $invoice->save();
            $this->updatePaymentInvoice($code, $student, $closed);
        }
    }

    private function recordHistory(Request $request)
    {
        $student = $request->customerid;
        $invoice_id = $request->invoice_id;
        $amount = $request->amountcredit;
        $year = $request->valuedate;
        $college = $this->activeModel($student)->college_id;
        $op_date = $request->operationdate;
        $bank = $request->branch;
        $operator = $request->username;
        $mode = $request->pay_mode;
        $description = $request->description;

        $payment = new Payment();
        $payment->college_id = $college;
        $payment->std_id = $student;
        $payment->year = $year;
        $payment->paid_amount = $amount;
        $payment->bank_slipno = $request->account ?: "Undefined";
        $payment->op_date = $op_date;
        $payment->bank_account = $request->account ?: "Undefined";
        $payment->bank = $bank;
        $payment->description = $description;
        $payment->operator = $operator;
        $payment->pay_mode = $request->terminal;
        $payment->pay_status = true;
        $payment->trans_id = $request->reference ?: "Undefined";
        $payment->event_id = $request->input('eventid');
        $payment->academic_year = getCurrentAcademicYear();

        if ($this->forRegistration($invoice_id, $student))
            $payment->admission = true;

        $payment->invoice_code = $invoice_id;

        $payment->save();

    }

    private function updatePaymentInvoice($code, $student, $closed = 0)
    {
        $invoices = PaymentInvoice::where('code', $code)->where('student_id', $student);
        if ($invoices->count() > 0) {
            $invoices->update(['paid' => $closed]);
//            foreach ($invoices->get() as $invoice) {
//                $invoice->paid = $closed;
//                $invoice->save();
//            }
        }
    }

}
