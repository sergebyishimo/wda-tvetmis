<?php

namespace App\Http\Controllers\Api;

use App\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function sendDisciplineSMS($school, $fault, $marks, $student, $token = null)
    {
        $value = [
            'fault' => $fault,
            'marks' => $marks
        ];
        $message = "Mubyeyi dufatanyije kurera umwana wawe yakoze irikosa " . $value['fault'] . " akurwaho amanota " . $value['marks'];

        $feedback = 'School Don\'t exists !!';

        $school = School::find($school);
        if ($school) {
            $student = $school->students()->where('id', $student)->first();
            if ($student) {
                $number = getStudentGPhone($student);
                if ($number != null)
                    sendSMS($number, $message, 'discipline', $school);
                return json_encode(['status' => '1', 'message' => "SMS send !"]);
            } else
                $feedback = 'Student Does n\'t exists !!';
        }

        return json_encode(['status' => '0', 'message' => $feedback]);
    }

    public function sendPermissionSMS($school, $destination, $reason, $leaving, $student, $token = null)
    {
        $value = [
            'reason' => $reason,
            'destination' => $destination,
            'leaving' => $leaving
        ];
        $message = "Mubyeyi dufatanyije kurera umwana wawe asohotse ikigo " . $value['leaving'] .
            " kubera " . $value['reason'] . ", agiye " . $value['destination'];

        $feedback = 'School Does n\'t exists !!';

        $school = School::find($school);
        if ($school) {
            $student = $school->students()->where('id', $student)->first();
            if ($student) {
                $number = getStudentGPhone($student);
                if ($number != null)
                    sendSMS($number, $message, 'permission', $school);
                return json_encode(['status' => '1', 'message' => "SMS send !"]);
            } else
                $feedback = 'Student Does n\'t exists !!';
        }

        return json_encode(['status' => '0', 'message' => $feedback]);
    }

    public function checkPayment($school, $student, $term, $year)
    {
        $feedback = 'School Does n\'t exists !!';

        $school = School::find($school);
        if ($school) {
            $student = $school->students()->where('id', $student)->first();
            if ($student) {
                $ex = getExpectedFees($student->level->id, $term, strtolower($student->mode), $school);
                $pa = getPaidFees($student->id, $term, $year, "amount", $school);
                $re = getRemainPayment($ex, $pa);
                return json_encode([
                    'status' => '1',
                    'expected' => $ex,
                    'paid' => $pa,
                    'remain' => $re,
                    'message' => ''
                ]);
            } else
                $feedback = 'Student Does n\'t exists !!';
        }

        return json_encode(['status' => '0', 'message' => $feedback]);
    }

}
