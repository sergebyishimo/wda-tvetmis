<?php

namespace App\Http\Controllers\Api;

use App\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApiResultController extends Controller
{
    protected $api_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";

    public function __construct()
    {

    }

    private function authorized($token)
    {
        $_token = $this->api_token;

        if ($token !== $_token)
            return false;

        return true;
    }


    public function resultsAll($token, $index_number) {

        if($this->authorized($token)) {
            $results = DB::table('senior_six_results')->where("index_number",$index_number)->first();
            if ($results) {
                return json_encode([ 'status' => true, 'results' => $results ]);
            } else {
                return json_encode([ 'status' => false, 'message' => 'Student Not Found' ]);
            }
        } else {
            return json_encode([ 'status' => false, 'message' => 'Invalid Token']);
        }

    }



}
