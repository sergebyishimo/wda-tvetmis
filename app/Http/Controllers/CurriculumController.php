<?php

namespace App\Http\Controllers;

use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\RtqfSource;
use App\Model\Accr\TrainingSectorsSource;
use Illuminate\Http\Request;

class CurriculumController extends Controller
{
    public function curriculumView($id)
    {
        $curr = CurriculumQualification::find($id);
        $curr = CurriculumQualification::find($id);
        if ($curr)
            $modules = $curr->modules;
        else
            $modules = [];

        return view('curriculum.curr_view', compact('curr', 'modules'));
    }

    public function tradesView($id)
    {
        if ($id != null) {
            $sector_info = TrainingSectorsSource::find($id);
            if ($sector_info) {
                return view('curriculum.trades', compact('sector_info'));
            }
        }
        return abort(404);
    }

    public function view(Request $request, $id = null, $trade = null)
    {

        if ($id != null) {
            $sector_info = TrainingSectorsSource::find($id);
//            $subsector_info    = TrainingTradesSource::find($id);
            $rtqfs = RtqfSource::where('level_name', '!=', 'Senior Six')->orderBy('id', 'asc')->get();
            //    dd($rtqfs);
            $trad = $trade;
            if (isset($request->hide)) {

                $hide_arr = [];
                for ($i = 0; $i < count($request->hide); $i++) {
                    if ($request->hide[$i] != null) {
                        $hide_arr[] = $request->hide[$i];
                    }
                }

                return view('curriculum.details', compact('sector_info', 'rtqfs', 'hide_arr', 'trad'));
            } else {
                return view('curriculum.details', compact('sector_info', 'rtqfs', 'trad'));
            }

        }

        $sectors = TrainingSectorsSource::all();
        return view('curriculum.view', compact('sectors'));
    }
}
