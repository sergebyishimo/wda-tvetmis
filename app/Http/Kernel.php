<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'lecturer' => \App\Http\Middleware\RedirectIfNotLecturer::class,
        'lecturer.guest' => \App\Http\Middleware\RedirectIfLecturer::class,
        'district' => \App\Http\Middleware\RedirectIfNotDistrict::class,
        'district.guest' => \App\Http\Middleware\RedirectIfDistrict::class,
        'admin_and_rp' => \App\Http\Middleware\AdminAndRpMiddleware::class,
        'admin' => \App\Http\Middleware\RedirectIfNotAdmin::class,
        'admin.guest' => \App\Http\Middleware\RedirectIfAdmin::class,
        'college' => \App\Http\Middleware\RedirectIfNotCollege::class,
        'college.guest' => \App\Http\Middleware\RedirectIfCollege::class,
        'rp' => \App\Http\Middleware\RedirectIfNotRp::class,
        'rp.guest' => \App\Http\Middleware\RedirectIfRp::class,
        'wda' => \App\Http\Middleware\RedirectIfNotWda::class,
        'wda.guest' => \App\Http\Middleware\RedirectIfWda::class,
        'examiner' => \App\Http\Middleware\RedirectIfNotExaminer::class,
        'examiner.guest' => \App\Http\Middleware\RedirectIfExaminer::class,
        'reb' => \App\Http\Middleware\RedirectIfNotReb::class,
        'reb.guest' => \App\Http\Middleware\RedirectIfReb::class,
        'student' => \App\Http\Middleware\RedirectIfNotStudent::class,
        'student.guest' => \App\Http\Middleware\RedirectIfStudent::class,
        'school' => \App\Http\Middleware\RedirectIfNotSchool::class,
        'school.guest' => \App\Http\Middleware\RedirectIfSchool::class,
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
//        'isEmailVerified' => \Lunaweb\EmailVerification\Middleware\IsEmailVerified::class,
        'payed' =>  \App\Http\Middleware\StudentHasPayed::class,
        'system_active' => \App\Http\Middleware\IsSystemEnabled::class,
        'enrol_active' => \App\Http\Middleware\IsEnrolEnabled::class,
        'applied'   =>  \App\Http\Middleware\RedirectIfApplied::class,
        'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
        'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
    ];
}
