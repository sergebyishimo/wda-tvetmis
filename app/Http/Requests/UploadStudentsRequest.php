<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UploadStudentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 2 || Auth::guard('school')->user()->privilege == 5;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qualification' =>  'required',
            'level_id'  =>  'required',
            'acad_year' => 'required|size:4',
            'status' => 'required',
            'file' => 'required|mimes:csv,txt'
        ];
    }
}
