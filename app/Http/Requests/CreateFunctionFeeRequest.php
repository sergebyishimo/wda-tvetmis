<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFunctionFeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::guard('rp')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fee_name' => 'required',
            'govt_sponsored_amount' => 'required|numeric',
            'private_sponsored_amount' => 'required|numeric',
            'paid_when' => 'required'
        ];
    }
}
