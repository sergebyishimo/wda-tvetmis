<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreDisciplineInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 6 || Auth::guard('school')->user()->privilege == 7 || Auth::guard('school')->user()->privilege == 2;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student' => 'required',
            'marks' => 'required|integer|min:1|max:20',
            'fault' => 'required|min:4|max:15',
            'comment' => 'required|min:8|max:90'
        ];
    }
}
