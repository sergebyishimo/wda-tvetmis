<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupContinuingStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registration_number' => 'required|exists:rp_mysql.continuing_students',
            'college' => 'required',
            'email' => 'required|email|unique:rp_mysql.students',
            'password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'password.regex' => 'Password must be a mix of lowercase and uppercase characters, numbers and symbols.'
        ];
    }
}
