<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLecturerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guard('college')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:lecturers,email',
            'telephone' => 'required|min:10|unique:lecturers,telephone',
            'nationID' => 'required|unique:lecturers,nationID',
            'gender' => 'required',
            'nationality' => 'required|string',
            'position' => 'required',
            'category' => 'required',
            'photo' => 'nullable|image',
        ];

        if ($this->get('is_head_of_department') == '1') {
            $rules['year_of_study'] = 'required';
            $rules['department'] = 'required';
        }

        return $rules;
    }
}
