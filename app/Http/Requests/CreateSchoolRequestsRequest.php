<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateSchoolRequestsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 1 || Auth::guard('school')->user()->privilege == 2 || Auth::guard('school')->user()->privilege == 3 || Auth::guard('school')->user()->privilege == 5;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'request_class' => 'required|in:Consumables,Trainers,Trainings',
            'required_units' => 'required|numeric',
            'unit_cost' => 'required|numeric',
            'request_description' =>'required',
            'required_by_date' =>'required|date',
            'school_id' => 'required',
            'request_attachment' => 'required|file',
        ];
    }

}
