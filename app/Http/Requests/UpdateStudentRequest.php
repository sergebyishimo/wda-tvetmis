<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isAllowed() || isAllowed(3);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reg_no' => 'required|unique:students,id,' . $this->get('id'),
            'qualification' => 'required',
            'level_id' => 'required',
            'fname' => 'required|min:3|regex:/^[a-zA-Z]+$/u',
            'lname' => 'required|min:3|regex:/^[a-zA-Z]+$/u',
            'email' => 'required|email|unique:students,id,' . $this->get('id'),
            'gender' => 'required|in:Male,Female',
            'bdate' => 'required|date',
            'image' => 'nullable|mimes:jpg,png,jpeg|max:5120',
            'orphan' => 'required|in:Father,Mother,Both,None',
            'ft_name' => 'nullable|required_if:orphan,None,Mother',
            'mt_name' => 'nullable|required_if:orphan,None,Father',
            'sponsor' => 'required',
            'insurance' => 'nullable',
            'insurance_number' => 'nullable|required_with:insurance',
            'gd_name' => 'nullable|required_if:orphan,Both',
            'gd_phone' => 'nullable|required_with:gd_name|regex:/07[8,2,3]{1}[0-9]{7}/',
            'ft_phone' => 'nullable|required_with:ft_name|regex:/07[8,2,3]{1}[0-9]{7}/',
            'mt_phone' => 'nullable|required_with:mt_name|regex:/07[8,2,3]{1}[0-9]{7}/',
            'mode' => 'required|in:Boarding,Day',
            'ubudehe' => 'nullable',
            'telephone' => 'nullable|regex:/07[8,2,3]{1}[0-9]{7}/|unique:students,id,'.$this->get('id'),
            'nationality' => 'nullable|min:4|max:10',
            'nationalID' => 'nullable|regex:/1[0-9]{4}[8,7][0-9]{10}/|unique:students,id,'.$this->get('id'),
            'Province' => 'required',
            'District' => 'required',
            'Sector' => 'required',
            'Cell' => 'required',
            'Village' => 'required',
            'index_number' => 'required|unique:students,id,'.$this->get('id')
        ];
    }

    public function messages()
    {
        return [
            'nationalID.regex' => 'The :attribute must be a valid Rwanda Format.',
            'telephone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
            'ft_phone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
            'gd_phone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
            'mt_phone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
            'ft_name.required_if' => 'The Father Name required if not orphan',
            'mt_name.required_if' => 'The Father Name required if not orphan'
        ];
    }
}
