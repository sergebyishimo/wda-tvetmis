<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 2 || Auth::guard('school')->user()->privilege == 1;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|regex:/^[a-zA-Z]+$/u',
            'last_name' => 'required|min:3',
            'privilege' => 'required',
            'gender' => 'required|in:Male,Female',
            'email' => 'required|email|unique:staffs_info|unique:school_users|unique:schools',
            'phone_number' => 'required|regex:/07[8,2,3]{1}[0-9]{7}/|unique:staffs_info',
            'attended_rtti' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
//            'national_id_number.regex' => 'The :attribute must be a valid Rwanda Format.',
            'first_name.regex' => 'The :attribute must not have any space.',
            'phone_number.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
//            '*.max' => "The :attribute may not be greater than 1MB.",
//            'scan_of_diploma_or_certificate.required_unless'    =>  'The certificate is required when your not graduated in 2017 and your examiner is not WDA.'
        ];
    }
}
