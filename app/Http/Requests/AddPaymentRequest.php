<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AddPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 4 || Auth::guard('school')->user()->privilege == 2;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student' => 'required',
            'term' => 'required|integer|min:1|max:3',
            'amount' => 'required|integer|min:1',
            'slip_number' => 'nullable|min:4',
            'bank_name' => 'nullable|min:2',
            'bank_slip' => 'nullable|mimes:pdf'
        ];
    }
}
