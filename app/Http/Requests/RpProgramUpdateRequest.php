<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RpProgramUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::guard('rp')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_id' => 'required',
            'program_name' => 'required',
            'program_code' => 'required',
            'program_load' => 'required',
            'rtqf_level' => 'required',
            'program_is_stem' => 'required',
            'program_status' => 'required',
            'tuition_fees' => 'required|numeric',
            'other_course_fees' => 'required|numeric',
        ];
    }
}
