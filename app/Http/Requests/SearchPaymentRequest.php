<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SearchPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 4 || Auth::guard('school')->user()->privilege == 2;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'term' => 'required',
            'acad_year' => 'required',
            'based' => 'required',
            'qualification' => 'required_if:based,c',
            'level' => 'required_if:based,c',
            'student' => 'required_if:based,s'
        ];
    }

    public function messages()
    {
        return [
            'based.required' => 'Check option please !!',
            'qualification.required_if' => 'Yeah, this one is required.',
            'level.required_if' => 'Yeah, Level too is required also.',
            'student.required_if' => 'Yeah, this one is required.'
        ];
    }
}
