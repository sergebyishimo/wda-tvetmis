<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WdaNationalExamMarksUpate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guard('wda')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attachment' => 'required|file',
            'combination_id' => 'required',
            'course_id' => 'required'
        ];
    }
}
