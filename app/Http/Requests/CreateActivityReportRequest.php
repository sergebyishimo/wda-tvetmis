<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateActivityReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isAllowed(1) || isAllowed() || isAllowed(3) || isAllowed(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required',
            'activity_description' => 'required',
            'implemented_from' => 'required|date',
            'implemented_to' => 'required|date',
            'impact_of_activity' =>'required',
            'activity_completed' =>'required',
            'activity_progress_details' => 'required',
        ];
    }
}
