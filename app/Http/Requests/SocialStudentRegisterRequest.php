<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SocialStudentRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'student' => 'required',
                'sponsor' => 'required',
//            'first_name' => 'required|max:255',
//            'last_name' => 'required|max:255',
//            'college' => 'required',
//            'department' => 'required',
//            'program' => 'required',
//            'email' => 'required|email|max:255|unique:rp_mysql.students,email|unique:rp_mysql.college_students,email|unique:rp_mysql.admitted_students,phone',
//            'telephone' => 'regex:/07[8,2,3]{1}[0-9]{7}/|unique:rp_mysql.students,telephone|unique:rp_mysql.college_students,phone|unique:rp_mysql.admitted_students,phone',
//            'password' => 'required|min:6|confirmed',
        ];
    }
}
