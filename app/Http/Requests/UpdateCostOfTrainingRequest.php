<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCostOfTrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isAllowed(1) || isAllowed() || isAllowed(3) || isAllowed(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required',
            'trades_id' => 'nullable',
            'cost_of_consumables_per_year' => 'required|numeric',
            'estimated_total_salaries_year' => 'required|numeric',
            'budget_available_for_consumables' => 'required|numeric',
            'operational_funds_available_per_year' => 'required|numeric',
            'budget_actually_available_for_salaries_per_year' => 'required|numeric',
            'estimated_operational_budget_required_per_year' => 'required|numeric',
            'academic_year' => 'required',
        ];
    }
}
