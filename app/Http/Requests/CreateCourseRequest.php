<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Request;

class CreateCourseRequest extends FormRequest
{
    protected $redirect = "/school/course";

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 2 || Auth::guard('school')->user()->privilege == 1 || Auth::guard('school')->user()->privilege == 5;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (Request::ajax())
            $rules = $this->rulesForAjax();
        else
            $rules = $this->rulesForHttp();

        return $rules;
    }

    private function rulesForAjax()
    {
        return [
            'modules' => 'array|min:1',
            'max_marks.*' => 'nullable|integer|min:1|max:100',
            'teacher' => 'required|array|min:1'
        ];
    }

    private function rulesForHttp()
    {
        $rules = ['modules' => 'required|array|min:1'];
        if (is_array($this->request->get('modules'))) {
            foreach ($this->request->get('modules') as $key => $val) {
                if ($val != null || $this->request->has('modules' . $key)) {
                    $rules['max_marks.' . $key] = 'required|integer|min:1|max:100';
                    $rules['teacher.' . $key] = 'required';
                }
            }
        }

        return $rules;
    }
}
