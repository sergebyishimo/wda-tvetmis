<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdditionalFeesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isAllowed() || isAllowed(4);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'term' => 'required',
            'acad_year' => 'required',
            'based' => 'required',
            'qualification' => 'required_if:based,c',
            'level' => 'nullable',
            'student' => 'required_if:based,s',
            'label' => 'required|min:4',
            'amount' => 'required|integer|min:1'
        ];
    }

    public function messages()
    {
        return [
            'based.required' => 'Check option please !!',
            'qualification.required_if' => 'Yeah, this one is required.',
            'level.required_if' => 'Yeah, Level too is required also.',
            'student.required_if' => 'Yeah, this one is required.'
        ];
    }
}
