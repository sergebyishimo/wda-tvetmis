<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CollegeStudentsMarksUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guard('college')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department' => 'required|numeric',
            'program' => 'required|numeric',
            'module' => 'required|numeric',
            'academic_year' => 'required|digits:4|integer|min:2009|max:'.(date('Y', strtotime('-1 year'))),
            'file' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'file.mimes' => 'File must be excel with xlsx format !'
        ];
    }
}
