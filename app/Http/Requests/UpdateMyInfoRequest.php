<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMyInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !isAllowed() || !isAllowed(3);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|regex:/^[a-zA-Z]+$/u',
            'last_name' => 'required|min:3',
            'gender' => 'required|in:Male,Female',
            'email' => 'required|email|unique:staffs_info,id,' . $this->get('id'),
            'phone_number' => 'required|regex:/07[8,2,3]{1}[0-9]{7}/|unique:staffs_info,id,' . $this->get('id'),
            'image' => 'required_without:image_old|mimes:jpg,png,jpeg',
            'nationality' => 'required|min:4',
            'national_id_number' => 'required|regex:/1[0-9]{4}[8,7][0-9]{10}/|unique:staffs_info,id,' . $this->get('id'),
            'Province' => 'required',
            'District' => 'required',
            'Sector' => 'required',
            'Cell' => 'nullable',
            'Village' => 'nullable',
            'civil_status' => 'required',
            'bank_name' => 'nullable|min:2',
            'bank_account_number' => 'nullable|min:2|unique:staffs_info,id,' . $this->get('id'),
            'rssb_number' => 'nullable|min:3|unique:staffs_info,id,' . $this->get('id'),
            'academic_document' => 'nullable|mimes:pdf',
            'certificate_document' => 'nullable|mimes:pdf'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
//            'national_id_number.regex' => 'The :attribute must be a valid Rwanda Format.',
            'first_name.regex' => 'The :attribute must not have any space.',
            'phone_number.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
//            '*.max' => "The :attribute may not be greater than 1MB.",
//            'scan_of_diploma_or_certificate.required_unless'    =>  'The certificate is required when your not graduated in 2017 and your examiner is not WDA.'
        ];
    }
}
