<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSchoolRequestsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isAllowed(1) || isAllowed() || isAllowed(3) || isAllowed(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'request_class' => 'required|in:Consumables,Trainers,Trainings',
            'required_units' => 'required|numeric',
            'unit_cost' => 'required|numeric',
            'request_description' =>'required',
            'required_by_date' =>'required|date',
            'school_id' => 'required',
            'request_attachment' => 'file',
        ];
    }
}
