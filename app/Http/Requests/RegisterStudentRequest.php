<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'college' => 'required',
            'department' => 'required',
            'program' => 'required',
            'email' => 'required|email|max:255|unique:rp_mysql.students|unique:rp_mysql.college_students',
            'telephone' => 'regex:/07[8,2,3]{1}[0-9]{7}/|unique:rp_mysql.students|unique:rp_mysql.college_students,phone',
            'password' => 'required|min:6|confirmed',
            'nation_id' => 'required|unique:rp_mysql.students'
        ];
    }

    public function messages()
    {
        return [
            'nation_id.regex' => 'The :attribute must be a valid Rwanda Format.'
        ];
    }
}
