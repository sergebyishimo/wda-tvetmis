<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateSchoolInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::guard('school')->check())
            return Auth::guard('school')->user()->privilege == 2;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|max:50|min:9",
            'acronym' => "required|max:8|min:2",
            'phone_number' => "required|regex:/07[8,2,3]{1}[0-9]{7}/|unique:schools,id,".school('id'),
            'email' => "required|email|unique:schools,id,".school('id'),
            'website' => "nullable|url",
            'discipline_max' => 'required|integer',
            'province' => 'required',
            'district' => 'required',
            'sector' => 'required',
            'logo' => 'mimes:png'.(strlen(school('logo')) > 0 ? "" : "|required"),
            'mid' => 'required|unique:schools,id,'.school('id')
        ];
    }
}
