<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCourseUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::guard('rp')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'program_id' => 'required|numeric',
            'course_unit_code' => 'required',
            'course_unit_name' => 'required',
            'credit_unit' => 'required|numeric',
            'level_of_study_id' => 'required|numeric',
            'semester_id' => 'required|numeric',
            'unit_class_id' => 'required|numeric',
        ];
    }
}
