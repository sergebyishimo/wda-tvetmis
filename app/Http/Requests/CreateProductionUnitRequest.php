<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductionUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isAllowed(1) || isAllowed() || isAllowed(3) || isAllowed(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required',
            'production_unit_name' => 'required',
            'operation_field' => 'required',
            'production_unit_capital_francs' => 'required|numeric',
            'annual_turnover_francs' =>'required|numeric',
            'monthly_revenues_francs' =>'required|numeric',
            'employees_male' => 'required|numeric',
            'employees_female' => 'required|numeric',
            'funds_source' => 'required',
            'rdb_registration_number' => 'required',
            'community_impact' => 'required',
            'attachments' => 'required',
        ];
    }
}
