<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitRegistrationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guard('student')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dat = date('y') - 16;
        $dat = $dat . "-12-01";
        return [
            'first_name' => 'required|string|max:255',
            'other_names' => 'required|string|max:255',
            'phone' => 'required|regex:/07[8,2,3]{1}[0-9]{7}/|unique:rp_mysql.college_students,phone',
            'gender' => 'required',
            'ubudehe' => 'required',
            'dob' => 'required',
            'want_student_loan' => 'nullable',
            'parents_phone' => 'nullable|regex:/07[8,2,3]{1}[0-9]{7}/|different:phone',
            "national_id_number" => 'required|unique:rp_mysql.college_students,national_id_number',
            "photo" => 'nullable|mimes:jpeg,jpg,png|max:1024',
            "scan_of_diploma_or_certificate" => 'nullable|mimes:jpeg,jpg,png,pdf|max:1024',
            "your_bank" => 'required',
            "your_bank_account" => 'required',
            "disability" => 'nullable',
            "examiner" => 'required',
            "index_number" => 'required',
            "school_attended" => 'required',
            "graduation_year" => 'required|date_format:Y|before:today',
            "option_offered" => 'nullable',
            "aggregates_obtained" => 'required',
//            "payment_verified" => 'required',
//            "admission" => 'required',
            "scan_national_id_passport" => "mimes:jpeg,jpg,png,pdf|max:1024",
            'email' => 'required|string|email|max:255|unique:rp_mysql.college_students,email',

            'father_nation_id' => 'nullable|regex:/1[0-9]{4}[8,7][0-9]{10}/',
            'mother_nation_id' => 'nullable|regex:/1[0-9]{4}[8,7][0-9]{10}/',
            'guardian_nation_id' => 'nullable|regex:/1[0-9]{4}[8,7][0-9]{10}/',

            'father_phone' => 'nullable|regex:/07[8,2,3]{1}[0-9]{7}/',
            'mother_phone' => 'nullable|regex:/07[8,2,3]{1}[0-9]{7}/',
            'guardian_phone' => 'nullable|regex:/07[8,2,3]{1}[0-9]{7}/',

            'father_name' => 'nullable',
            'mother_name' => 'nullable',
            'guardian_name' => 'nullable'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'national_id_number.regex' => 'The :attribute must be a valid Rwanda Format.',
            'phone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
            '*.max' => "The :attribute may not be greater than 1MB."
        ];
    }

}
