<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateExistingSchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guard('wda')->check() or auth()->guard('school')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_code' => 'required|unique:accr_mysql.schools_information,school_code,' . $this->get('school_id'),
            'school_name' => 'required|min:3',
            'province' => 'required',
            'district' => 'required',
            'sector' => 'required',
            'cell' => 'required',
            'village' => 'required',
            'manager_name' => 'required|min:2',
            'manager_phone' => 'required|regex:/07[8,2,3]{1}[0-9]{7}/|unique:accr_mysql.schools_information,manager_phone,' . $this->get('school_id'),
            'manager_email' => 'required|email|unique:accr_mysql.schools_information,manager_email,' . $this->get('school_id'),
            'accreditation_status' => 'required',
            'website' => 'nullable|url',
            'has_electricity' => 'required',
            'has_water' => 'required',
            'has_computer_lab' => 'required',
            'has_internet' => 'required',
            'has_library' => 'required',
            'has_business_or_strategic_plan' => 'required',
            'has_feeding_program' => 'required',
            'date_of_establishment' => 'nullable|date_format:Y|before:' . date('Y'),
            'latitude' => 'nullable',
            'longitude' => 'nullable',
            'phone' => 'nullable|regex:/07[8,2,3]{1}[0-9]{7}/|unique:accr_mysql.schools_information,phone,' . $this->get('school_id'),
            'email' => 'required|email|unique:accr_mysql.schools_information,email,' . $this->get('school_id'),
            'school_status' => 'required',
            'owner_name' => 'nullable|min:2',
            'owner_phone' => 'nullable|regex:/07[8,2,3]{1}[0-9]{7}/|unique:accr_mysql.schools_information,owner_phone,' . $this->get('school_id'),
            'owner_type' => 'nullable',
            'owner_email' => 'nullable|email|unique:accr_mysql.schools_information,owner_email,' . $this->get('school_id'),
            'boarding_or_day' => 'required',
            'school_activity' => 'required',
            'school_logo' => 'nullable|image',
            'number_of_desktops' => 'nullable|integer',
            'number_of_classrooms' => 'nullable|integer',
            'students_males' => 'nullable|integer',
            'students_female' => 'nullable|integer',
            'staff_male' => 'nullable|integer',
            'staff_female' => 'nullable|integer',
            'number_of_generators' => 'nullable|integer'
        ];
    }

    public function messages()
    {
        return [
            'manager_phone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
            'phone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.',
            'owner_phone.regex' => 'The :attribute must be either MTN, TIGO or AIRTEL.'
        ];
    }
}
