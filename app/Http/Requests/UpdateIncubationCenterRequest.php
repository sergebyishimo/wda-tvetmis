<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateIncubationCenterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isAllowed(1) || isAllowed() || isAllowed(3) || isAllowed(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required',
            'club_name' => 'required',
            'club_facilitator' => 'required',
            'incubatees_male' => 'required|numeric',
            'incubatees_female' =>'required|numeric',
            'average_age' =>'required|numeric',
            'business_areas' => 'required',
            'annual_turn_over_francs' => 'required',
            'funds_source' => 'required',
            'rdb_registration_number' => 'required',
            'number_incubated_clubs' => 'required|numeric',
            'attachments' => 'required',
        ];
    }
}
