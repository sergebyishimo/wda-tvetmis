<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualificationRtqf extends Model
{
    protected $connection = "mysql";
    protected $table = "qualification_rtqfs";

    public function qualification()
    {
        return $this->belongsToMany(Qualification::class,'qualification_rtqfs', 'qualification_id');
    }

    public function rtqf()
    {
        return $this->belongsToMany(Rtqf::class,'qualification_rtqfs', 'rtqf_id');
    }

}
