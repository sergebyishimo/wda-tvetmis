<?php

namespace App;

use App\Model\Rp\FunctionalFees;
use Illuminate\Database\Eloquent\Model;

class ManageFeeCategory extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "source_functional_fees_category";

}
