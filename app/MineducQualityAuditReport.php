<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class MineducQualityAuditReport extends Model
{
    protected  $fillable = ['school_id','section_id','indicator_id','marks'];

    public  function  school(){
        return $this->belongsTo(SchoolInformation::class,'school_id');
    }
}
