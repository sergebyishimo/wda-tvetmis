<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Visit extends Model
{
    use Uuid;

    public $incrementing = false;

    protected $fillable = [
        'school_id', 'visits_date', 'student_id'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
