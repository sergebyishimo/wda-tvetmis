<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RebStaffWorkexperience extends Model
{
    public $timestamps = false;

    protected $connection = 'reb_mysql';

    protected $table = "staff_workexperience";

    protected $fillable = [
        'school_id', 'staff_id', 'title', 'institution', 'status', 'period_from', 'period_to', 'responsibility',
        'field_of_expertise_sector', 'field_of_expertise_trade', 'supporting_doc', 'attachment_name', '	attachment_size'
    ];

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }
}
