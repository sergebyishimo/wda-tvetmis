<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class SchoolDesk extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_school_desks";

    public function school()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id');
    }

    public function level()
    {
        return $this->belongsTo(Rtqf::class, 'rtqf_level');
    }
}