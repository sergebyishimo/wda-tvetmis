<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RebStaffMarkingBackground extends Model
{
    public $timestamps = false;

    protected $connection = 'reb_mysql';

    protected $table = "staff_marking_backgrounds";

    protected $fillable = [
        'school_id', 'staff_id', 'academic_year', 'marking_position', 'marking_institution', 'education_program',
        'marking_center', 'subject_exam_marked'
    ];

    public function staff()
    {
        $this->belongsTo(Reb::class, 'staff_id', 'id');
    }

}
