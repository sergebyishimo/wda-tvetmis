<?php

namespace App;

use App\Model\Rp\RpProgram;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class Rtqf extends Model
{
    use Uuid;
    use SoftDeletes;

    protected $connection = "mysql";

    protected $fillable = [
        'level_name', 'level_description'
    ];

    public function levels()
    {
        return $this->hasMany(Level::class);
    }

    public function qualifications()
    {
        return $this->belongsToMany(Qualification::class, "qualification_rtqfs");
    }

    public function programs()
    {
        return $this->hasMany(RpProgram::class, 'rtqf_level', 'id');
    }
}
