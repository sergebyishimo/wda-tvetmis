<?php

namespace App;

use App\Notifications\CollegeResetPassword;
use App\Rp\AdmissionLetter;
use App\Rp\AdmittedStudent;
use App\Rp\CollegeStudent;
use App\Rp\PaymentCategory;
use App\Rp\Polytechnic;
use App\Rp\Department;
use App\Model\Rp\RpDepartment;
use App\Model\Rp\RpProgram;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class College extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $connection = "mysql";

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'college_id', 'permission', 'roles'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CollegeResetPassword($token));
    }

    public function college()
    {
        return $this->belongsTo(Polytechnic::class, 'college_id', 'id');
    }

    public function letter()
    {
        return $this->hasOne(AdmissionLetter::class, 'college_id', 'college_id');
    }

    public function collegeStudents()
    {
        return $this->hasMany(CollegeStudent::class,'college_id', 'college_id');
    }

    public function paymentCategory()
    {
        return $this->hasMany(PaymentCategory::class, 'college_id', 'id');
    }

    public function getStudentsAttribute()
    {
        $students = AdmittedStudent::where('college_id', $this->college_id)
            ->where('admission', true)
            ->where('year_of_study', date('Y'));

        return $students->get();
    }

    public function getAllStudentsAttribute()
    {
        $students = AdmittedStudent::where('college_id', $this->college_id)
            ->where('year_of_study', date('Y'));

        return $students->paginate(800);
    }

    public function getMyLetterAttribute()
    {
        if ($this->letter)
            return $this->letter->letter;
        else
            return "";
    }

    public function getMyStudentsAttribute()
    {
        $students = CollegeStudent::where('college_id', $this->college_id)
            ->get();

        return $students;
    }

    public function getMyFirstYearStudentsAttribute()
    {
        $students = CollegeStudent::where('college_id', $this->college_id)
            ->where('year_of_study', '1')
            ->orWhere('year_of_study', date('y'))
            ->get();

        return $students;
    }

    public function getMySecondYearStudentsAttribute()
    {
        $students = CollegeStudent::where('college_id', $this->college_id)
            ->where('year_of_study', '2')
            ->get();

        return $students;
    }

    public function getMyThirdYearStudentsAttribute()
    {
        $students = CollegeStudent::where('college_id', $this->college_id)
            ->where('year_of_study', '3')
            ->get();

        return $students;
    }

    public function getTodayStudentAttribute()
    {
        $students = CollegeStudent::where('college_id', $this->college_id)
            ->whereDate('created_at', Carbon::today());
//            ->where('year_of_study', date('Y'))

        return $students->count();
    }

    public function getRecentStudentsAttribute()
    {
        $students = CollegeStudent::where('college_id', $this->college_id)
            ->orderBy("created_at", "DESC")
            ->take(8)
            ->get();
//            ->where('year_of_study', date('Y'))

        return $students;
    }

    public function getMyDepartmentAttribute()
    {
        $department = Department::where('polytechnic', $this->college_id)->orderBy('department', 'ASC')->get(['id', 'department']);

        return $department;

    }

    public function getEmailAttribute()
    {
        return $this->username . "@" . str_replace(" ", "", strtolower($this->collegeName)) . ".edu";
    }

    public function getCollegeNameAttribute()
    {
        if ($this->college)
            return $this->college->short_name;

        return "None";
    }

    public function departments() {
        return $this->belongsToMany(RpDepartment::class, 'college_programs','department_id', 'college_id');
    }

    public function programs() {
        return $this->belongsToMany(RpProgram::class, 'college_programs','program_id', 'college_id');
    }

}
