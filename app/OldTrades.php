<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldTrades extends Model
{
    protected $table = "old_trades";
}
