<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkingActivation extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'marking', 'assessor', 'reb'
    ];


}
