<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class QualificationStatus extends Model
{
    use Uuid;
    protected $connection = "mysql";
//    use SoftDeletes;

    protected $fillable = [
        'school_id', 'qualification_id', 'status'
    ];
}
