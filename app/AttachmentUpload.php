<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AttachmentUpload extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'attachments_upload';
    protected $fillable = ['title', 'description', 'file', 'added_by'];
}
