<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class FeesExpected extends Model
{
    use Uuid;
    protected $connection = "mysql";
    protected $table = "fees_expected";

    public $incrementing = false;

    protected $fillable = [
        'school_id', 'term', 'level_id', 'boarding', 'day', 'last_update'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::clas);
    }
}
