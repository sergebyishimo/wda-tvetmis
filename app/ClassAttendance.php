<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class ClassAttendance extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = [
        'school_id', 'tapDate', 'tapTime', 'level_id', 'course_id', 'absent'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
