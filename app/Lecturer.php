<?php

namespace App;

use App\Model\Rp\LecturerAndCourse;
use App\Model\Rp\MarksCategorizing;
use App\Model\Rp\RpLecturerSubmitMarks;
use App\Model\Rp\StaffPositionSource;
use App\Notifications\LecturerResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Lecturer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'college_id', 'name', 'email', 'password',
        'telephone', 'gender', 'address', 'nationID', 'position_id',
        'category', 'nationality', 'position',
        'photo', 'is_head_of_department', 'year_of_study', 'department'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new LecturerResetPassword($token));
    }

    public function courses()
    {
        return $this->hasMany(LecturerAndCourse::class,'lecturer_id','id');
    }

    public function categorized()
    {
        return $this->hasMany(MarksCategorizing::class, 'lecturer_id', 'id');
    }

    public function submittedMarks()
    {
        return $this->hasMany(RpLecturerSubmitMarks::class, 'lecturer_id');
    }

    public function _position()
    {
        return $this->belongsTo(StaffPositionSource::class,'position', 'id');
    }

    public function getPositionNameAttribute()
    {
        return $this->_position ? $this->_position->position_name : "";
    }

}
