<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAssessorApplication extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'school_id', 'staff_id', 'trade', 'trade_assessed_before', 'subject', 'subject_assessed_before', 'academic_year',
        'application_date', 'first_subject_applied_for', 'first_subject_assssed_before', 'second_subject_applied_for',
        'second_subject_assssed_before', 'third_subject_applied_for', 'third_subject_assssed_before', 'wda_confirmation',
        'wda_selection', 'score'
    ];

    public function staff()
    {
        return $this->belongsTo(StaffsInfo::class, 'staff_id', 'id');
    }

    public function tvetSubField()
    {
        return $this->belongsTo(SourceTvetSubField::class,'trade', 'id');
    }

    public function getNamesAttribute()
    {
        if ($this->staff)
            return $this->staff->names;
        return "";
    }

    public function getGenderAttribute()
    {
        if ($this->staff)
            return $this->staff->gender;
        return "";
    }

    public function getPhotoAttribute()
    {
        if ($this->staff)
            return $this->staff->photo;
        return "";
    }

    public function getSchoolAttribute()
    {
        if ($this->staff)
            return $this->staff->school->name;
        return "";
    }

    public function getPhoneNumberAttribute()
    {
        if ($this->staff)
            return $this->staff->phone_number;
        return "";
    }

    public function getNationalIdNumberAttribute()
    {
        if ($this->staff)
            return $this->staff->national_id_number;
        return "";
    }

    public function getEmailAttribute()
    {
        if ($this->staff)
            return $this->staff->email;
        return "";
    }

    public function getQualificationAttribute()
    {
        if ($this->staff)
            return $this->staff->qualification;
        return "";
    }

    public function getInstitutionAttribute()
    {
        if ($this->staff)
            return $this->staff->institution;
        return "";
    }

    public function getGraduatedYearAttribute()
    {
        if ($this->staff)
            return $this->staff->graduated_year;
        return "";
    }

    public function getNationalityAttribute()
    {
        if ($this->staff)
            return $this->staff->nationality;
        return "";
    }

    public function getProvinceAttribute()
    {
        if ($this->staff)
            return $this->staff->province;
        return "";
    }

    public function getDistrictAttribute()
    {
        if ($this->staff)
            return $this->staff->district;
        return "";
    }

    public function getSectorAttribute()
    {
        if ($this->staff)
            return $this->staff->sector;
        return "";
    }

}
