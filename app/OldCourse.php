<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldCourse extends Model
{
    //

    public function option(){
        return $this->belongsTo(Combination::class,'combination_id');
    }
}
