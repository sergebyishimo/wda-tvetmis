<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combination extends Model
{
    protected  $table= "combinations";

    public function courses()
    {
        return $this->hasMany(OldCourse::class,'combination_id');
    }
}
