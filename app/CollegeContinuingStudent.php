<?php

namespace App;

use App\College;
use App\Model\Rp\RpProgram;
use App\Rp\AdmittedStudent;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Rp\CollegeOption;

class CollegeContinuingStudent extends Model  implements Auditable
{

    use \OwenIt\Auditing\Auditable;    
    protected $connection = "rp_mysql";
    protected $table = "continuing_students";
    protected $fillable = ['registration_number', 'college_id',
        'option_id', 'sponsor_id',
        'year_of_study', 'academic_year', 'student_reg', 'first_name', 'last_name'];

    public function program()
    {
        return $this->belongsTo(RpProgram::class, "program_id");
    }

    public function option()
    {
        return $this->belongsTo(CollegeOption::class, "option_id");
    }

    public function college()
    {
        return $this->belongsTo(College::class, "college_id");
    }

    public function admissionInfo()
    {
        return $this->belongsTo(AdmittedStudent::class, 'student_reg', 'std_id');
    }
}
