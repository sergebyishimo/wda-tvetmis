<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class SubSector extends Model
{
    use Uuid;
    use SoftDeletes;
    protected $connection = "mysql";

    protected $fillable = [
        'sector_id', 'sub_sector_name', 'acronym'
    ];


    public function workshops(){
        return $this->hasMany(SchoolWorkshop::class,'sub_sector','id');
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class,'sub_sector_id');
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

}
