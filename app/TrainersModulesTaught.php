<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainersModulesTaught extends Model
{
    protected $table = 'school_trainers_modules_taught';

    protected $fillable = ['staff_id', 'sector_id', 'sub_sector_id', 'qualification_id', 'module_id'];
}
