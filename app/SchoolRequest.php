<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class SchoolRequest extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_school_requests";


    public function school()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id');
    }
}
