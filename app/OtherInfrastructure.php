<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;

class OtherInfrastructure extends Model
{
    protected $connection = "accr_mysql";
    protected $table = "source_other_infrastructure";

    public  function school(){
        return $this->belongsTo(SchoolInformation::class,'school_id');
    }

}
