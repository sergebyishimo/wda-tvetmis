<?php

namespace App;

use App\Model\Accr\SchoolInformation;
use App\Notifications\StudentResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class Student extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;
    use Uuid;

    protected $connection = "mysql";
    protected $dates = ['deleted_at'];

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'school',
        'reg_no',
        'lname',
        'fname',
        'level_id',
        'gender',
        'mode',
        'bdate',
        'nationality',
        'province',
        'district',
        'sector',
        'cell',
        'village',
        'ft_name',
        'ft_phone',
        'mt_name',
        'mt_phone',
        'gd_name',
        'gd_phone',
        'sponsor',
        'acad_year',
        'status',
        'nationalID',
        'index_number',
        'updateVersion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['names'];

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new StudentResetPassword($token));
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school', 'id');
    }
    public  function  mschool(){
        return $this->belongsTo(SchoolInformation::class, 'school', 'id');
    }
    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function discipline()
    {
        return $this->hasMany(Discipline::class);
    }

    public function disciplineHistory()
    {
        return $this->hasMany(DisciplineHistory::class);
    }

    public function permissions()
    {
        return $this->hasMany(SchoolPermission::class, 'student_id');
    }

    public function periodicMarks()
    {
        return $this->hasMany(PeriodicEntry::class, "std_reg_no", 'reg_no');
    }

    public function examMarks()
    {
        return $this->hasMany(ExamEntry::class, "std_reg_no", 'reg_no');
    }

    public function position()
    {
        return $this->hasMany(Position::class, 'std_reg_no', 'reg_no');
    }

    public function attendance_in_out()
    {
        return $this->hasMany(AttendanceInOut::class, 'std_id', 'id');
    }

    public function visit()
    {
        return $this->hasMany(Visit::class);
    }

    public function getNamesAttribute()
    {
        return ucwords(trim($this->fname . " " . $this->lname));
    }

    public function getDiscMarksAttribute()
    {
        $marks = $this->discipline()
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->get();

        if (count($marks) == 0) {
            return 0;
        } else {
            return $marks[0]->marks;
        }
    }

    public function getDiscMarksOneAttribute()
    {
        $marks = $this->discipline()
            ->where('term', 1)
            ->where('acad_year', $this->acad_year)->get();

        if (count($marks) == 0) {
            return 0;
        } else {
            return $marks[0]->marks;
        }
    }

    public function getDiscMarksTwoAttribute()
    {
        $marks = $this->discipline()
            ->where('term', 2)
            ->where('acad_year', $this->acad_year)
            ->get();

        if (count($marks) == 0) {
            return 0;
        } else {
            return $marks[0]->marks;
        }
    }

    public function getDiscMarksThreeAttribute()
    {
        $marks = $this->discipline()
            ->where('term', 3)
            ->where('acad_year', $this->acad_year)
            ->get();

        if (count($marks) == 0) {
            return 0;
        } else {
            return $marks[0]->marks;
        }
    }

    public function setPositionAttribute($value)
    {

        $check = school(true)->positions()
            ->where('acad_year', $this->acad_year)
            ->where('term', $this->term)
            ->where('std_reg_no', $this->reg_no)
            ->get();

        if (count($check) == 0) {
            Position::create([
                'school_id' => school('id'),
                'acad_year' => $this->acad_year,
                'term' => $this->term,
                'std_reg_no' => $this->reg_no,
                'position' => $value
            ]);
        } else {
            school(true)->positions()
                ->where('acad_year', $this->acad_year)
                ->where('term', $this->term)
                ->where('std_reg_no', $this->reg_no)
                ->update(['position' => $value]);
        }

        $this->attributes['position'] = $value;
    }

    public function setAcadYearAttribute($value)
    {
        $this->attributes['acad_year'] = $value;
    }

    public function setTermAttribute($value)
    {
        $this->attributes['term'] = $value;
    }

    public function setPeriodAttribute($value)
    {
        $this->attributes['period'] = $value;
    }

    // used in calculating in periodicReport.blade.php

    public function setCourseIdAttribute($value)
    {
        $this->attributes['course_id'] = $value;
    }

    public function setCourseMaxAttribute($value)
    {
        $this->attributes['course_max'] = $value;
    }

    public function setCoursesTotalAttribute($value)
    {
        $this->attributes['courses_total'] = $value;
    }

    public function setMarksTypeAttribute($value)
    {
        $this->attributes['marks_type'] = strtolower($value);
    }

    public function getMarksAttribute()
    {

        $settings = settings();
        $marks = school(true)->periodicEntry()
            ->where('course_id', $this->course_id)
            ->where('marks_type', $this->marks_type)
            ->where('term', $settings->active_term)
            ->where('period', $settings->active_period)
            ->where('acad_year', date('Y'))
            ->where('std_reg_no', $this->reg_no)
            ->get();

        if (count($marks) == 0) {
            return 0;
        } else {
            return $marks[0]->obt_marks;
        }
    }

    public function getExamMarksAttribute()
    {

        $marks = school(true)->examEntry()
            ->where('course_id', $this->course_id)
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->where('std_reg_no', $this->reg_no)
            ->get();

        if (count($marks) == 0) {
            return 0;
        } else {
            return $marks[0]->obt_marks;
        }

    }

    public function getMarkIdAttribute()
    {

        $settings = settings();
        $marks = school(true)->periodicEntry()
            ->where('course_id', $this->course_id)
            ->where('marks_type', $this->marks_type)
            ->where('term', $settings->active_term)
            ->where('period', $settings->active_period)
            ->where('acad_year', date('Y'))
            ->where('std_reg_no', $this->reg_no)
            ->get();

        return $marks[0]->id;
    }

    public function getExamMarkIdAttribute()
    {
        $marks = school(true)->examEntry()
            ->where('course_id', $this->course_id)
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->where('std_reg_no', $this->reg_no)
            ->get();

        return $marks[0]->id;
    }

    public function getSumCourseObtMarksAttribute()
    {
        if ($this->period == 6) {
            $marks = school(true)->examEntry()
                ->where('std_reg_no', $this->reg_no)
                ->where('course_id', $this->course_id)
                ->where('term', $this->term)
                ->where('acad_year', $this->acad_year)
                ->sum('obt_marks');
        } else {
            $marks = school(true)->periodicEntry()
                ->where('std_reg_no', $this->reg_no)
                ->where('course_id', $this->course_id)
                ->where('term', $this->term)
                ->where('period', $this->period)
                ->where('acad_year', $this->acad_year)
                ->sum('obt_marks');
        }

        return $marks;
    }

    public function getSumCourseTermObtMarksAttribute()
    {
        $marks = school(true)->periodicEntry()
            ->where('std_reg_no', $this->reg_no)
            ->where('course_id', $this->course_id)
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->sum('obt_marks');
        return $marks;
    }

    // used in periodResults.blade.php in all courses periodic report to get student quizes total marks in all courses

    public function getTotalObtMarksAttribute()
    {
        if ($this->period == 6) {
            $marks = school(true)->examEntry()
                ->where('std_reg_no', $this->reg_no)
                ->where('term', $this->term)
                ->where('acad_year', $this->acad_year)
                ->sum('obt_marks');
        } else {
            $marks = school(true)->periodicEntry()
                ->where('std_reg_no', $this->reg_no)
                ->where('term', $this->term)
                ->where('period', $this->period)
                ->where('acad_year', $this->acad_year)
                ->sum('obt_marks');
        }

        return $marks;
    }

    public function getTotalObtMarksSortAttribute()
    {
        $courses = Course::where('level_id', $this->level_id)
            ->whereIn('term', [$this->term, 4])
            ->where('status', 1)
            ->get();

        if ($this->period == 6) {
            $total = school(true)->examEntry()
                ->where('std_reg_no', $this->reg_no)
                ->where('term', $this->term)
                ->where('acad_year', $this->acad_year)
                ->sum('obt_marks');

        } else {

            $total = 0;
            foreach ($courses as $course) {
                $sum_course_obt_marks = school(true)->periodicEntry()
                    ->where('std_reg_no', $this->reg_no)
                    ->where('course_id', $course['id'])
                    ->where('term', $this->term)
                    ->where('period', $this->period)
                    ->where('acad_year', $this->acad_year)
                    ->sum('obt_marks');

                $period_course_total = school(true)->periodicEntry()
                    ->where('course_id', $course['id'])
                    ->where('term', $this->term)
                    ->where('period', $this->period)
                    ->where('acad_year', $this->acad_year)
                    ->distinct('marks_type', $course['marks_type'])
                    ->sum('total');

                if ($period_course_total == 0) {
                    $real_marks = 0;
                } else {
                    $real_marks = ($sum_course_obt_marks * $course['max_point']) / $period_course_total;
                }

                $total += $real_marks;
            }

        }


        return $total;
    }

    // used in periodResults.blade.php in single course periodic report to get quiz marks

    public function getPeriodObtMarksAttribute()
    {
        $marks = school(true)->periodicEntry()
            ->where('std_reg_no', $this->reg_no)
            ->where('term', $this->term)
            ->where('period', $this->period)
            ->where('acad_year', $this->acad_year)
            ->where('course_id', $this->course_id)
            ->where('marks_type', $this->marks_type)
            ->get();

        if (count($marks) == 0) {
            return 0;
        } else {
            return $marks[0]->obt_marks;
        }
    }

    public function getPeriodCourseTotalAttribute()
    {
        if ($this->period == 6) {
            $marks = school(true)->examEntry()
                ->where('course_id', $this->course_id)
                ->where('term', $this->term)
                ->where('acad_year', $this->acad_year)
                ->distinct('std_reg_no')
                ->sum('total');
        } else {
            $marks = school(true)->periodicEntry()
                ->where('course_id', $this->course_id)
                ->where('term', $this->term)
                ->where('period', $this->period)
                ->where('acad_year', $this->acad_year)
                ->distinct('marks_type', $this->marks_type)
                ->sum('total');
        }


        return $marks;
    }

    public function getTermCourseTotalAttribute()
    {
        $marks = school(true)->periodicEntry()
            ->where('course_id', $this->course_id)
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->distinct('marks_type', $this->marks_type)
            ->sum('total');

        return $marks;
    }

    // used in periodResults.blade.php in single courses periodic report to get student total marks in all quizez in a course

    public function getTotalObtMarksInCourseAttribute()
    {
        $marks = school(true)->periodicEntry()
            ->where('std_reg_no', $this->reg_no)
            ->where('term', $this->term)
            ->where('period', $this->period)
            ->where('acad_year', $this->acad_year)
            ->where('course_id', $this->course_id)
            ->sum('obt_marks');

        return $marks;
    }

    public function getTermPercAttribute()
    {
        if ($this->term == 4) {

            $courses = Course::where('level_id', $this->level_id)
                ->where('status', 1)
                ->get();

            $total_obt = 0;
            $ex_total_obt = 0;
            $total_max = 0;
            $all_real_total = 0;

            $i = 1;
            foreach ($courses as $course) {
                $sum_course_obt_marks = school(true)->periodicEntry()
                    ->where('std_reg_no', $this->reg_no)
                    ->where('course_id', $course['id'])
                    ->where('acad_year', $this->acad_year)
                    ->sum('obt_marks');

                $period_course_total = school(true)->periodicEntry()
                    ->where('course_id', $course['id'])
                    ->where('acad_year', $this->acad_year)
                    ->distinct('marks_type', $course['marks_type'])
                    ->sum('total');

                //error_log($this->reg_no . ' period obt : ' . $sum_course_obt_marks . ' peiord total  ' . $period_course_total);


                if ($period_course_total == 0) {
                    $real_marks = 0;
                } else {
                    $real_marks = ($sum_course_obt_marks * $course['max_point'] * 3) / $period_course_total;
                }

                $total_obt += $real_marks;


                $ex_real_marks = school(true)->examEntry()
                    ->where('std_reg_no', $this->reg_no)
                    ->where('course_id', $course['id'])
                    ->where('acad_year', $this->acad_year)
                    ->take(1)
                    ->sum('obt_marks');

                $tot = $real_marks + $ex_real_marks;
                $tot2 = $course['max_point'] * 6;

                // error_log($this->reg_no . ' ' . $i++);

                //error_log($this->reg_no . ' ' . $course['name'] .  ' Total ' . $tot . ' / ' . $tot2 );

                $ex_total_obt += $ex_real_marks;

                $total_max += $course['max_point'] * 3;
            }

            $total_total = $total_obt + $ex_total_obt;

            $perc = ($total_total * 100) / ($total_max * 2);


        } else {

            $courses = Course::where('level_id', $this->level_id)
                ->whereIn('term', [$this->term, 4])
                ->where('status', 1)
                ->get();

            $total_obt = 0;
            $ex_total_obt = 0;
            $total_max = 0;
            foreach ($courses as $course) {
                $sum_course_obt_marks = school(true)->periodicEntry()
                    ->where('std_reg_no', $this->reg_no)
                    ->where('course_id', $course['id'])
                    ->where('term', $this->term)
                    ->where('acad_year', $this->acad_year)
                    ->sum('obt_marks');

                $period_course_total = school(true)->periodicEntry()
                    ->where('course_id', $course['id'])
                    ->where('term', $this->term)
                    ->where('acad_year', $this->acad_year)
                    ->distinct('marks_type', $course['marks_type'])
                    ->sum('total');

                if ($period_course_total == 0) {
                    $real_marks = 0;
                } else {
                    $real_marks = ($sum_course_obt_marks * $course['max_point']) / $period_course_total;
                }

                $total_obt += $real_marks;
                $total_max += $course['max_point'];

                $ex_sum_course_obt_marks = school(true)->examEntry()
                    ->where('std_reg_no', $this->reg_no)
                    ->where('course_id', $course['id'])
                    ->where('term', $this->term)
                    ->where('acad_year', $this->acad_year)
                    ->sum('obt_marks');

                $ex_course_total = school(true)->examEntry()
                    ->where('course_id', $course['id'])
                    ->where('term', $this->term)
                    ->where('acad_year', $this->acad_year)
                    ->distinct('marks_type', $course['marks_type'])
                    ->sum('total');


                if ($ex_course_total == 0) {
                    $ex_real_marks = 0;
                } else {
                    $ex_real_marks = ($ex_sum_course_obt_marks * $course['max_point']) / $ex_course_total;
                }

                $ex_total_obt += $ex_real_marks;


            }

            $total_total = $total_obt + $ex_total_obt;


            $perc = ($total_total * 100) / ($total_max * 2);

        }

        return $perc;
    }

    public function getTermCatAttribute()
    {
        $period_obt_marks = school(true)->periodicEntry()
            ->where('std_reg_no', $this->reg_no)
            ->where('course_id', $this->course_id)
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->sum('obt_marks');

        $period_total = school(true)->periodicEntry()
            ->where('std_reg_no', $this->reg_no)
            ->where('course_id', $this->course_id)
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->sum('total');

        if ($period_total == 0) {
            $period_course_total = 0;
        } else {
            $period_course_total = ($period_obt_marks * $this->course_max) / $period_total;
        }

        return $period_course_total;
    }

    public function getTermExAttribute()
    {
        $obt_marks = school(true)->periodicEntry()
            ->where('std_reg_no', $this->reg_no)
            ->where('course_id', $this->course_id)
            ->where('term', $this->term)
            ->where('acad_year', $this->acad_year)
            ->get();

        if (count($obt_marks) == 0) {
            return 0;
        } else {
            return $obt_marks[0]['obt_marks'];
        }
    }

    public function getToPayAttribute()
    {
        $fees = school(true)->fees()
            ->where('level_id', $this->level_id)
            ->where('term', $this->term)
            ->get();

        $mode = strtolower($this->mode);
        return $fees[0]->$mode;
    }

    public function getPaidAttribute()
    {
        $fees = school(true)->payments()
            ->where('student_id', $this->id)
            ->where('acad_year', $this->acad_year)
            ->where('term', $this->term)
            ->sum('amount');
        return $fees;
    }

    public function getTermPosAttribute()
    {
        $i = school(true)->positions()
            ->where('acad_year', $this->acad_year)
            ->where('term', $this->term)
            ->where('std_reg_no', $this->reg_no)
            ->get();

        if (count($i) == 0) {
            $i = 0;
        } else {
            $i = $i[0]->position;
        }


        return $i;
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
