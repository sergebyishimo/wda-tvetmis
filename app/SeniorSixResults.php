<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeniorSixResults extends Model
{
    protected $table = 'senior_six_results';
}
