<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Level extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = [
        'school_id', 'department_id', 'rtqf_id', 'staffs_info_id', 'status', 'updateVersion'
    ];


    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function rtqf()
    {
        return $this->belongsTo(Rtqf::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function teacher()
    {
        return $this->belongsTo(StaffsInfo::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function fees()
    {
        return $this->hasMany(FeesExpected::class);
    }

    public function class_attendance()
    {
        return $this->hasMany(ClassAttendance::class);
    }
}
