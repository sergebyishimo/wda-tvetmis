<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class QueryCacher extends Model
{
    use Uuid;
    public $incrementing = false;

    protected $fillable =[
        'id', 'query', 'results'
    ];

}
