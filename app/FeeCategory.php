<?php

namespace App;

use App\Model\Rp\FunctionalFees;
use App\Model\Rp\RpProgram;
use Illuminate\Database\Eloquent\Model;

class FeeCategory extends Model
{
    protected $connection = "rp_mysql";
    protected $table = "source_fees_category";


    public function fees()
    {
        return $this->belongsToMany(FunctionalFees::class, 'source_functional_fees_category', 'fee_category_id', "fee_id");
    }

    public function program()
    {
        return $this->belongsTo(RpProgram::class, 'program_id', 'id');
    }

    public function feesCategory()
    {
        return $this->hasMany(ManageFeeCategory::class, 'fee_category_id', 'id');
    }

    /**
     * @return bool|null
     * @throws \Exception
     */
    public function delete()
    {
        // delete all related function fees
        $this->feesCategory()->delete();

        // delete the category
        return parent::delete();
    }
}
