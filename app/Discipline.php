<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Discipline extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = ['school_id', 'student_id', 'term', 'acad_year', 'marks'];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

}
