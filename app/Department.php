<?php

namespace App;

use App\Model\Accr\CurriculumQualification;
use App\Model\Accr\SchoolInformation;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Department extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = [
        'school_id', 'qualification_id', 'staffs_info_id', 'status', 'updateVersion'
    ];


    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function school2()
    {
        return $this->belongsTo(SchoolInformation::class, 'school_id');
    }

    public function qualification()
    {
        return $this->belongsTo(CurriculumQualification::class,'qualification_id', 'uuid');
    }

    public function teacher()
    {
        return $this->belongsTo(StaffsInfo::class, 'staffs_info_id');
    }

    public function levels()
    {
        return $this->hasMany( Level::class);
    }

}
