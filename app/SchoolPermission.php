<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class SchoolPermission extends Model
{
    use Uuid;
    protected $connection = "mysql";
    public $incrementing = false;

    protected $fillable = [
        'school_id', 'student_id', 'acad_year','term', 'reason','destination',
        'observation','issued_by', 'leaving_datetime','returning_datetime', 'leaving_sms'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
