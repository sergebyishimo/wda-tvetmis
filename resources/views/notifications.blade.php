@extends('adminlte::layouts.app')
@section('contentheader_title')
    <div class="container-fluid">
        Notifications
    </div>
@endsection

@section('htmlheader_title', "All Notifications")

@section('l-style') @parent
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
      integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
<link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
<style>
    .jFiler-theme-default .jFiler-input {
        width: inherit !important;
    }
</style>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
            @include('feedback.feedback')
            <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ route('notifications.create') }}" class="btn btn-primary pull-right"> Add
                            Notification </a>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped table-hover" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User Type</th>
                                <th>Title</th>
                                <th>Notification</th>
                                <th>User Names</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1)
                            @foreach ($notifications as $notification)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ ucwords($notification->user_type) }}</td>
                                    <td>{!! $notification->title !!}</td>
                                    <td>{!! $notification->message !!}</td>
                                    <td>{{  $notification->user_type ? ucwords("App\\" .ucwords($notification->user_type))::find($notification->user_id)->name : "" }}</td>
                                    <td><a href="{{ route('delete.notification', $notification->id) }}"
                                           class="btn btn-danger"
                                           onclick="return window.confirm('Are you sure you want to remove this attachment: ?')"><i
                                                    class="fa fa-trash"></i> Remove</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
