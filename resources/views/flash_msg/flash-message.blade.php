@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block text-white">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <span style="font-size:14px">{{ $message }}</span>
    </div>
@endif


@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block text-white">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <span style="font-size:14px">{{ $message }}</span>
    </div>
@endif


@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block text-white">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <span style="font-size:14px">{{ $message }}</span>
    </div>
@endif


@if ($message = Session::get('info'))
    <div class="alert alert-info alert-block text-white">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <span style="font-size:14px">{{ $message }}</span>
    </div>
@endif


@if ($errors->any())
    <div class="alert alert-danger text-white">
        <button type="button" class="close" data-dismiss="alert">×</button>
        Please check the form below for errors
    </div>
@endif