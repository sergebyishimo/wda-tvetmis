@if(auth()->guard('examiner')->check())
    <li class="{{ activeMenu('examiner.home') }}">
        <a href="{{ route('examiner.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>

    <li class="{{ activeMenu('examiner.activation') }}">
        <a href="{{ route('examiner.activation') }}"><i
                    class='fa fa-scribd'></i>&nbsp;<span>Applications Activation</span></a>
    </li>

    <li class="{{ activeMenu('examiner.applicant.marking') }}">
        <a href="{{ route('examiner.applicant.marking') }}"><i
                    class='fa fa-adn'></i>&nbsp;<span>Marking Applicants</span></a>
    </li>

    <li class="{{ activeMenu('examiner.applicant.assessing') }}">
        <a href="{{ route('examiner.applicant.assessing') }}"><i
                    class='fa fa-arrows'></i>&nbsp;<span>Assessing Applicants.</span></a>
    </li>

    <li class="{{ activeMenu('examiner.applicant.reb') }}">
        <a href="{{ route('examiner.applicant.reb') }}"><i
                    class='fa fa-at'></i>&nbsp;<span>REB Applicants.</span></a>
    </li>

@endif