@extends('examiner.layout.main')

@section('panel-title', "Dashboard")

@section('htmlheader_title', "Examiners")

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{ \App\Student::where('status','active')->count() }}</h3>
                                <p>Students</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            {{--<a href="{{ '#' }}" class="small-box-footer">--}}
                                {{--View all <i class="fa fa-arrow-circle-right"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{ \App\StaffsInfo::where('status','active')->where('privilege', '7')->count() }}</h3>

                                <p>Teachers</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-knife"></i>
                            </div>

                            {{--<a href="{{ '#' }}" class="small-box-footer">View all <i--}}
                                        {{--class="fa fa-arrow-circle-right"></i></a>--}}
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{ \App\School::all()->count() }}</h3>

                                <p>Schools</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            {{--<a href="{{ '#' }}" class="small-box-footer">View all <i--}}
                                        {{--class="fa fa-arrow-circle-right"></i></a>--}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-olive-active">
                            <div class="inner">
                                <h3>{{ \App\StaffMarkerApplication::where('academic_year', date('Y'))->count() }}</h3>

                                <p>Total Marking Application</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-done"></i>
                            </div>
                            <a href="{{ route('examiner.applicant.marking') }}" class="small-box-footer">View all <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-purple-active">
                            <div class="inner">
                                <h3>{{ \App\StaffAssessorApplication::where('academic_year', date('Y'))->count() }}</h3>

                                <p>Total Assessor Application</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pizza"></i>
                            </div>
                            <a href="{{ route('examiner.applicant.assessing') }}" class="small-box-footer">View all <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-black-gradient">
                            <div class="inner">
                                <h3>{{ \App\RebStaffMarkerApplication::where('academic_year', date('Y'))->count() }}</h3>

                                <p>Total REB Marking Applicants</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-disc"></i>
                            </div>
                            <a href="{{ route('examiner.applicant.reb') }}" class="small-box-footer">View all <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
