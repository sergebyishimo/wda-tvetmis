@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Assessing Applicants
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap.min.css"/>
    {{--<link rel="stylesheet"--}}
    {{--href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>--}}
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css" rel="stylesheet"/>

    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('contentheader_title')
    <div class="container-fluid">
        Assessing Applicants
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body" style="overflow-x: auto !important;">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed" id="dataTable">
                            <thead>
                            <th>Photo</th>
                            <th>Names</th>
                            <th>Gender</th>
                            <th>NationID</th>
                            <th>Phone Number</th>
                            <th>School</th>
                            <th>Education Program</th>
                            <th>Trade Before</th>
                            <th>Province</th>
                            <th>District</th>
                            <th>Sector</th>
                            <th>Qualification</th>
                            <th>Graduated Year</th>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                            <th>Photo</th>
                            <th>Names</th>
                            <th>Gender</th>
                            <th>NationID</th>
                            <th>Phone Number</th>
                            <th>School</th>
                            <th>Education Program</th>
                            <th>Trade Before</th>
                            <th>Province</th>
                            <th>District</th>
                            <th>Sector</th>
                            <th>Qualification</th>
                            <th>Graduated Year</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>

    <script>
        $(function () {
            'use strict';
            var table = $("#dataTable").dataTable({
                serverSide: true,
                processing: true,
                searching: true,
                colReorder: true,
                fixedHeader: true,
                responsive: true,
                ajax: 'datatable/object-data/a',
                lengthMenu: [[50, 100, 1000, -1], [50, 100, 1000, "All"]],
                columns: [
                    {data: 'photo', orderable: false, searchable: false},
                    {data: 'names', name: 'staffs_info.first_name'},
                    {data: 'gender', name: 'staffs_info.gender'},
                    {data: 'nationID', name: 'staffs_info.national_id_number'},
                    {data: 'phone_number', name: 'staffs_info.phone_number'},
                    {data: 'school', name: 'schools.name', orderable: false, searchable: false},
                    {data: 'trade'},
                    {data: 'trade_assessed_before'},
                    {data: 'province', name: 'staffs_info.province'},
                    {data: 'district', name: 'staffs_info.district'},
                    {data: 'sector', name: 'staffs_info.sector'},
                    {data: 'qualification', name: 'staffs_info.qualification'},
                    {data: 'graduated_year', name: 'staffs_info.graduated_year'},
                    // {data: 'actions', orderable: false, searchable: false}
                ],
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            // jQuery selector to add a border
                            $('row c[r*="10"]', sheet).attr('s', '25');
                        },
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        postfixButtons: ['colvisRestore'],
                        collectionLayout: 'fixed two-column'
                    },


                    // 'columnsToggle'
                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select class="select2"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });

            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
@endsection