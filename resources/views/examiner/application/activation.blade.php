@extends('examiner.layout.main')

@section('panel-title', "Applying System Activation")

@section('htmlheader_title', "Applying System Activation")
@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">
                {!! Form::open(['route' => 'examiner.update.activation']) !!}
                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Switch box</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div>
                            <h3 class="text-blue p-1">Marking Applications</h3>
                            <div class="pretty p-switch">
                                <input type="radio" name="marking"
                                       value="1" {{ getMarkingActivation('marking') == 1 ? 'checked' : '' }}/>
                                <div class="state p-success">
                                    <label>Active</label>
                                </div>
                            </div>

                            <div class="pretty p-switch p-fill">
                                <input type="radio" name="marking"
                                       value="0" {{ getMarkingActivation('marking') == 0 ? 'checked' : '' }} />
                                <div class="state p-danger">
                                    <label>Inactive</label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div>
                            <h3 class="text-blue p-1">Assessor Applications</h3>
                            <div class="pretty p-switch">
                                <input type="radio" name="assessor"
                                       value="1" {{ getMarkingActivation('assessor') == 1 ? 'checked' : '' }}/>
                                <div class="state p-success">
                                    <label>Active</label>
                                </div>
                            </div>

                            <div class="pretty p-switch p-fill">
                                <input type="radio" name="assessor"
                                       value="0" {{ getMarkingActivation('assessor') == 0 ? 'checked' : '' }}/>
                                <div class="state p-danger">
                                    <label>Inactive</label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div>
                            <h3 class="text-blue p-1">REB Marking Applications</h3>
                            <div class="pretty p-switch">
                                <input type="radio" name="reb"
                                       value="1" {{ getMarkingActivation('reb') == 1 ? 'checked' : '' }} />
                                <div class="state p-success">
                                    <label>Active</label>
                                </div>
                            </div>

                            <div class="pretty p-switch p-fill">
                                <input type="radio" name="reb"
                                       value="0" {{ getMarkingActivation('reb') == 0 ? 'checked' : '' }} />
                                <div class="state p-danger">
                                    <label>Inactive</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clear-fix">&nbsp;</div>

                    <div class="box-footer">
                        <button type="su" class="btn btn-primary btn-success">Update</button>
                    </div>

                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
