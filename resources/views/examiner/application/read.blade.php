@extends('adminlte::layouts.app')

@section('htmlheader_title')
    More on {!! $staff->names !!}
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap.min.css"/>
    {{--<link rel="stylesheet"--}}
    {{--href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>--}}
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css" rel="stylesheet"/>

    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('contentheader_title')
    <div class="container-fluid">
        {!! $staff->names !!}
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body" style="overflow-x: auto !important;">
                <div class="row">
                    <div class="col-md-5">
                        <table class="table">
                            <tr>
                                <td colspan="2" align="center">
                                    <img src="{{ getStaffPhoto($staff->photo) }}" alt=""
                                         class="img-responsive img-rounded" style="width: 200px;">
                                </td>
                            </tr>
                            <tr>
                                <td>Names</td>
                                <td><b>{!! $frm == 'nom'? $staff->names : $staff->name !!}</b></td>
                            </tr>
                            <tr>
                                <td>School</td>
                                <td><b>{!! $frm == 'nom'? $staff->school->name : $staff->school !!}</b></td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td><b>{!! $staff->gender !!}</b></td>
                            </tr>
                            <tr>
                                <td>NationalId Number</td>
                                <td><b>{!! $staff->national_id_number !!}</b></td>
                            </tr>
                            @if($frm == 'nom')
                                <tr>
                                    <td>Job Title</td>
                                    <td><b>{!! getPrivilege($staff->privilege) !!}</b></td>
                                </tr>
                            @endif
                            <tr>
                                <td>Qualification</td>
                                <td><b>{!! $staff->qualification !!}</b></td>
                            </tr>
                            <tr>
                                <td>Institution</td>
                                <td><b>{!! $staff->institution !!}</b></td>
                            </tr>
                            <tr>
                                <td>Graduated Year</td>
                                <td><b>{!! $staff->graduated_year !!}</b></td>
                            </tr>
                            <tr>
                                <td>Phone Number</td>
                                <td><b>{!! $staff->phone_number !!}</b></td>
                            </tr>
                            <tr>
                                <td>Nationality</td>
                                <td><b>{!! $staff->nationality !!}</b></td>
                            </tr>
                            <tr>
                                <td>Province</td>
                                <td><b>{!! $staff->province !!}</b></td>
                            </tr>
                            <tr>
                                <td>District</td>
                                <td><b>{!! $staff->district !!}</b></td>
                            </tr>
                            <tr>
                                <td>Sector</td>
                                <td><b></b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-6" style="border-right: solid rgba(0,0,0,.1) 1px;">
                                <h3>Marking Application</h3>
                                @if($currentMarker)
                                    <div>Academic Year: <b>{{ $currentMarker->academic_year }}</b></div>
                                    <div>Trade applied for:
                                        <b>{{ getSelectTradeMarker($currentMarker->education_program_id) }}</b></div>
                                    <div>Assessed Before: <b>{{ getYesNo($currentMarker->trade_marked_before) }}</b>
                                    </div>
                                    <div>First Subject applied for:
                                        <b>{{ $currentMarker->first_subject_applied_for }}</b></div>
                                    <div>First Subject Assessed Before:
                                        <b>{{ getYesNo($currentMarker->first_subject_marked_before) }}</b>
                                    </div>
                                    @isset($currentMarker->second_subject_applied_for)
                                        <div>Second Subject applied for:
                                            <b>{{ $currentMarker->second_subject_applied_for }}</b></div>
                                        <div>Second Subject Assessed Before:
                                            <b>{{ getYesNo($currentMarker->second_subject_marked_before) }}</b></div>
                                    @endisset
                                    @isset($currentMarker->third_subject_applied_for)
                                        <div>Third Subject applied for:
                                            <b>{{ $currentMarker->third_subject_applied_for }}</b></div>
                                        <div>Third Subject Assessed Before:
                                            <b>{{ getYesNo($currentMarker->third_subject_marked_before) }}</b></div>
                                    @endisset
                                @else
                                    <div>Not Applied !!</div>
                                @endif
                            </div>
                            @if($frm == 'nom')
                                <div class="col-md-6">
                                    <h3>Assessor Application</h3>
                                    @if($currentAssessor)
                                        <div>Academic Year: <b>{{ $currentAssessor->academic_year }}</b></div>
                                        <div>Trade applied for:
                                            <b>{{ getSelectTradeMarker($currentAssessor->trade) }}</b>
                                        </div>
                                        <div>Assessed Before:
                                            <b>{{ getYesNo($currentAssessor->trade_assessed_before) }}</b>
                                        </div>
                                    @else
                                        <div>Not Applied !!</div>
                                    @endif
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div class="box box-primary">
                            <div class="box-header">Attachments</div>
                            <div class="box-body">
                                <table class="table" id="dataTable">
                                    <thead>
                                    <th>#</th>
                                    <th>Attachment Description</th>
                                    <th>Link</th>
                                    </thead>
                                    <tbody>
                                    @if($myAttachement)
                                        @php($x=1)
                                        @foreach($myAttachement as $attachement)
                                            <tr>
                                                <td width="30px">{{ $x++ }}</td>
                                                <td>{{ ucwords($attachement->name) }}</td>
                                                <td><a href="{{ asset('storage/'.$attachement->attachment) }}"
                                                       target="_blank"
                                                       class="btn btn-link">Download</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="">None</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header">Teaching Experience</div>
                            <div class="box-body">
                                <table class="table" id="dataTable">
                                    <thead>
                                    <th>#</th>
                                    <th>Institution Taught</th>
                                    <th>Subject Taught</th>
                                    <th>Option / Trade</th>
                                    <th>Level Taught</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    </thead>
                                    <tbody>
                                    @if($backgroundsT->count() > 0)
                                        @php($x=1)
                                        @foreach($backgroundsT as $background)
                                            <tr>
                                                <td width="30px">{{ $x++ }}</td>
                                                <td>{{ $background->institution_taught }}</td>
                                                <td>{{ $background->subject_taught }}</td>
                                                <td>{{ getSelectTradeMarker($background->tvet_sub_field) }}</td>
                                                <td>{{ getLevelTaught($background->class_or_level_taught) }}</td>
                                                <td>{{ $background->from_date }}</td>
                                                <td>{{ $background->to_date }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="">None</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header">Working Experience</div>
                            <div class="box-body">
                                <table class="table" id="dataTable">
                                    <thead>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Institution</th>
                                    <th>Status</th>
                                    <th>Period From</th>
                                    <th>Period To</th>
                                    <th>Responsibility</th>
                                    </thead>
                                    <tbody>
                                    @if($backgroundsW->count() > 0)
                                        @php($x=1)
                                        @foreach($backgroundsW as $background)
                                            <tr>
                                                <td width="30px">{{ $x++ }}</td>
                                                <td>{{ $background->title }}</td>
                                                <td>{{ $background->institution }}</td>
                                                <td>{{ getStatusWorkExperience($background->status) }}</td>
                                                <td>{{ $background->period_from }}</td>
                                                <td>{{ $background->period_to }}</td>
                                                <td>{{ $background->responsibility }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="">None</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header">Marking Experience</div>
                            <div class="box-body">
                                <table class="table" id="dataTable">
                                    <thead>
                                    <th>#</th>
                                    <th>Academic Year</th>
                                    <th>Marking Position</th>
                                    <th>Marking Institution</th>
                                    <th>Marking Center</th>
                                    <th>Subject Exam Marked</th>
                                    <th>Education Program</th>
                                    </thead>
                                    <tbody>
                                    @if($backgroundsM->count() > 0)
                                        @php($x=1)
                                        @foreach($backgroundsM as $background)
                                            <tr>
                                                <td width="30px">{{ $x++ }}</td>
                                                <td>{{ $background->academic_year }}</td>
                                                <td>{{ getPosition($background->marking_position) }}</td>
                                                <td>{{ $background->marking_institution }}</td>
                                                <td>{{ $background->marking_center }}</td>
                                                <td>{{ getSubjectExamMarked($background->subject_exam_marked) }}</td>
                                                <td>{{ getSelectTradeMarker($background->education_program) }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="">None</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if($frm == 'nom')
                            <div class="box box-primary">
                                <div class="box-header">Assessing Experience</div>
                                <div class="box-body">
                                    <table class="table" id="dataTable">
                                        <thead>
                                        <th width="30px">#</th>
                                        <th>Academic Year</th>
                                        <th>Program Assessed</th>
                                        <th>Assessement Center</th>
                                        </thead>
                                        <tbody>
                                        @if($backgroundsA->count() > 0)
                                            @php($x=1)
                                            @foreach($backgroundsA as $background)
                                                <tr>
                                                    <td width="30px">{{ $x++ }}</td>
                                                    <td>{{ $background->academic_year }}</td>
                                                    <td>{{ getSelectTradeMarker($background->education_program_assessed) }}</td>
                                                    <td>{{ ucwords($background->assessement_center) }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="">None</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
@endsection