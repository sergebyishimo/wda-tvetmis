<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
</head>
<body>

<nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144"
     style="background-color: rgb(255, 255, 255);">
    <div class="container">
        <div class="row bg-white p-2" style="width: inherit !important;">
            <div class="col-md-9">
                
                <div class="navbar-toggleable-sm">
                    <span class="navbar-logo"><a href="/"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                    <span><a class="navbar-brand" href="/">{{ config('app.name') }}</a></span>
                    <!-- Example single danger button -->
                    <a class="btn btn-sm btn-primary shadow-sm" href="{{ route('home.dashboard') }}"> <span>Dashboard</span> </a>
                    <a class="btn btn-sm btn-dark shadow-sm"
                       href="{{ route('curricula.index') }}">
                        <span>Curriculum</span>
                    </a>

                    <a class="btn btn-sm btn-info shadow-sm"
                    href="{{ route('home.schools') }}">
                     <span>Schools</span>
                    </a>

                    <span class="dropdown">
                        <button class="btn btn-sm btn-warning shadow-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Support
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               href="{{ route('school.request.account') }}">Schools Account Request</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item"
                               href="{{ route('contact.us') }}">Contact Us</a>
                        </div>
                    </span>

                    <a class="btn btn-sm btn-success shadow-sm" href="{{ route('home.attachments') }}">
                    <span>Downloads</span>
                    </a>
                    

                </div>
            </div>
            <div class="col-md-3">
                <div class="btn-group" style="float: right !important;">
                    <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        General Login
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('school.login') }}">Schools Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('college.login') }}">Colleges
                            Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('rp.login') }}">RP Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('district.login') }}">District Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('wda.login') }}">WDA Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/register') }}">REB | Create Account</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/login') }}">REB | Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
         style="background-image: url({{asset('img/bg_wood.png')}});background-attachment: fixed;">
    <div class="mbr-table-cell">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{--##################################################################################--}}
                    <form name="aspnetForm" method="post" action="{{ route('home.schools.findaschool') }}"
                          id="aspnetForm">
                        {{ csrf_field() }}
                        <div>

                            <script src="js/jquery-ui-1.8.custom.min.js" type="text/javascript"></script>
                            <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
                                  rel="stylesheet" type="text/css"/>

                            <script type="text/javascript">
                                $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                                    item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                                    return $("<li></li>")
                                        .data("item.autocomplete", item)
                                        .append("<a>" + item.label + "</a>")
                                        .appendTo(ul);
                                };
                                $(document).ready(function () {
                                    $("input#ctl00_ContentPlaceHolder1_txtSchoolOrZip").autocomplete(
                                        {
                                            source: ["ACERO - BRIGHTON PARK", "ACERO - CISNEROS", "ACERO - CLEMENTE", "ACERO - DE LA CRUZ", "ACERO - DE LAS CASAS", "ACERO - FUENTES", "ACERO - GARCIA HS", "ACERO - IDAR", "ACERO - MARQUEZ", "ACERO - PAZ", "ACERO - SANTIAGO", "ACERO - SOTO HS", "ACERO - TAMAYO", "ACERO - TORRES", "ACERO - ZIZUMBO", "ADDAMS", "AGASSIZ", "AHS - PASSAGES", "AIR FORCE HS", "ALBANY PARK", "ALCOTT ES", "ALCOTT HS", "ALDRIDGE", "AMUNDSEN HS", "ARIEL", "ARMOUR", "ARMSTRONG G", "ASHBURN", "ASHE", "ASPIRA - BUSINESS & FINANCE HS", "ASPIRA - EARLY COLLEGE HS", "ASPIRA - HAUGAN", "AUDUBON", "AUSTIN CCA HS", "AVALON PARK", "AZUELA", "BACK OF THE YARDS HS", "BARNARD", "BARRY", "BARTON", "BASS", "BATEMAN", "BEARD", "BEASLEY", "BEAUBIEN", "BEETHOVEN", "BEIDLER", "BELDING", "BELL", "BELMONT-CRAGIN", "BENNETT", "BLACK", "BLAINE", "BLAIR", "BOGAN HS", "BOND", "BOONE", "BOUCHET", "BOWEN HS", "BRADWELL", "BRENNEMANN", "BRENTANO", "BRIDGE", "BRIDGESCAPE - BRAINERD HS", "BRIDGESCAPE - HUMBOLDT PARK HS", "BRIDGESCAPE - LAWNDALE HS", "BRIDGESCAPE - ROSELAND HS", "BRIGHT", "BRIGHTON PARK", "BRONZEVILLE CLASSICAL", "BRONZEVILLE HS", "BROOKS HS", "BROWN R", "BROWN W", "BROWNELL", "BRUNSON", "BUDLONG", "BURBANK", "BURKE", "BURLEY", "BURNHAM", "BURNSIDE", "BURR", "BURROUGHS", "BYRNE", "CALDWELL", "CALMECA", "CAMELOT - EXCEL ENGLEWOOD HS", "CAMELOT - EXCEL HS", "CAMELOT - EXCEL SOUTHSHORE HS", "CAMELOT - EXCEL SOUTHWEST HS", "CAMELOT - SAFE ES", "CAMELOT - SAFE HS", "CAMERON", "CAMRAS", "CANTY", "CARDENAS", "CARNEGIE", "CARROLL", "CARSON", "CARTER", "CARVER G", "CARVER MILITARY HS", "CASALS", "CASSELL", "CATALYST - CIRCLE ROCK", "CATALYST - MARIA", "CATHER", "CHALMERS", "CHAPPELL", "CHASE", "CHAVEZ", "CHIARTS HS", "CHICAGO ACADEMY ES", "CHICAGO ACADEMY HS", "CHICAGO AGRICULTURE HS", "CHICAGO COLLEGIATE", "CHICAGO MATH & SCIENCE HS", "CHICAGO MILITARY HS", "CHICAGO TECH HS", "CHICAGO VIRTUAL", "CHICAGO VOCATIONAL HS", "CHOPIN", "CHRISTOPHER", "CHRISTOPHER HOUSE", "CICS - AVALON/SOUTH SHORE", "CICS - BASIL", "CICS - BOND", "CICS - BUCKTOWN", "CICS - CHICAGOQUEST HS", "CICS - ELLISON HS", "CICS - IRVING PARK", "CICS - LONGWOOD", "CICS - LOOMIS", "CICS - NORTHTOWN HS", "CICS - PRAIRIE", "CICS - WASHINGTON PARK", "CICS - WEST BELDEN", "CICS - WRIGHTWOOD", "CLAREMONT", "CLARK ES", "CLARK HS", "CLAY", "CLEMENTE HS", "CLEVELAND", "CLINTON", "CLISSOLD", "COLEMON", "COLES", "COLLINS HS", "COLUMBIA EXPLORERS", "COLUMBUS", "COOK", "COONLEY", "COOPER", "CORKERY", "CORLISS HS", "COURTENAY", "CRANE MEDICAL HS", "CROWN", "CUFFE", "CULLEN", "CURIE HS", "CURTIS", "DALEY", "DARWIN", "DAVIS M", "DAVIS N", "DAWES", "DE DIEGO", "DECATUR", "DENEEN", "DEPRIEST", "DETT", "DEVER", "DEVRY HS", "DEWEY", "DIRKSEN", "DISNEY", "DISNEY II ES", "DISNEY II HS", "DIXON", "DOOLITTLE", "DORE", "DOUGLASS HS", "DRAKE", "DRUMMOND", "DUBOIS", "DULLES", "DUNBAR HS", "DUNNE", "DURKIN PARK", "DVORAK", "DYETT ARTS HS", "EARHART", "EARLE", "EBERHART", "EBINGER", "EDGEBROOK", "EDISON", "EDISON PARK", "EDWARDS", "ELLINGTON", "EPIC HS", "ERICSON", "ERIE", "ESMOND", "EVERETT", "EVERGREEN", "EVERS", "FAIRFIELD", "FALCONER", "FARADAY", "FARNSWORTH", "FARRAGUT HS", "FENGER HS", "FERNWOOD", "FIELD", "FINKL", "FISKE", "FOREMAN HS", "FORT DEARBORN", "FOSTER PARK", "FOUNDATIONS", "FRANKLIN", "FRAZIER CHARTER", "FRAZIER PROSPECTIVE", "FULLER", "FULTON", "FUNSTON", "GAGE PARK HS", "GALE", "GALILEO", "GALLISTEL", "GARVEY", "GARVY", "GARY", "GILLESPIE", "GLOBAL CITIZENSHIP", "GOETHE", "GOODE HS", "GOUDY", "GRAHAM ES", "GRAHAM HS", "GRAY", "GREAT LAKES", "GREELEY", "GREEN", "GREENE", "GREGORY", "GRESHAM", "GRIMES", "GRISSOM", "GUNSAULUS", "HAINES", "HALE", "HALEY", "HAMILTON", "HAMLINE", "HAMMOND", "HAMPTON", "HANCOCK HS", "HANSON PARK", "HARLAN HS", "HARPER HS", "HARTE", "HARVARD", "HAUGAN", "HAWTHORNE", "HAY", "HAYT", "HEALY", "HEARST", "HEDGES", "HEFFERAN", "HENDERSON", "HENDRICKS", "HENRY", "HERNANDEZ", "HERZL", "HIBBARD", "HIGGINS", "HIRSCH HS", "HITCH", "HOLDEN", "HOLMES", "HOPE HS", "HOPE LEARNING ACADEMY", "HORIZON - SOUTHWEST", "HOWE", "HOYNE", "HUBBARD HS", "HUGHES C", "HUGHES L", "HURLEY", "HYDE PARK HS", "INFINITY HS", "INSTITUTO - HEALTH", "INSTITUTO - LOZANO HS", "INTER-AMERICAN", "INTRINSIC HS", "IRVING", "JACKSON A", "JACKSON M", "JAHN", "JAMIESON", "JEFFERSON HS", "JENSEN", "JOHNSON", "JONES HS", "JOPLIN", "JORDAN", "JUAREZ HS", "JULIAN HS", "JUNGMAN", "KANOON", "KELLER", "KELLMAN", "KELLOGG", "KELLY HS", "KELVYN PARK HS", "KENNEDY HS", "KENWOOD HS", "KERSHAW", "KILMER", "KING ES", "KING HS", "KINZIE", "KIPLING", "KIPP - ACADEMY", "KIPP - ASCEND", "KIPP - BLOOM", "KIPP - ONE", "KOZMINSKI", "LAKE VIEW HS", "LANE TECH HS", "LANGFORD", "LARA", "LASALLE", "LASALLE II", "LAVIZZO", "LAWNDALE", "LEARN - 7", "LEARN - BUTLER", "LEARN - CAMPBELL", "LEARN - EXCEL", "LEARN - MIDDLE", "LEARN - PERKINS", "LEARN - SOUTH CHICAGO", "LEE", "LEGACY", "LEGAL PREP HS", "LELAND", "LENART", "LEWIS", "LIBBY", "LINCOLN", "LINCOLN PARK HS", "LINDBLOM HS", "LITTLE BLACK PEARL HS", "LITTLE VILLAGE", "LLOYD", "LOCKE A", "LOCKE J", "LOGANDALE", "LORCA", "LOVETT", "LOWELL", "LOZANO", "LYON", "MADERO", "MADISON", "MANIERRE", "MANLEY HS", "MANN", "MARINE LEADERSHIP AT AMES HS", "MARQUETTE", "MARSH", "MARSHALL HS", "MASON", "MATHER HS", "MAYER", "MAYS", "MCAULIFFE", "MCCLELLAN", "MCCORMICK", "MCCUTCHEON", "MCDADE", "MCDOWELL", "MCKAY", "MCNAIR", "MCPHERSON", "MELODY", "METCALFE", "MIRELES", "MITCHELL", "MOLLISON", "MONROE", "MONTESSORI ENGLEWOOD", "MOOS", "MORGAN PARK HS", "MORRILL", "MORTON", "MOUNT GREENWOOD", "MOUNT VERNON", "MOVING EVEREST", "MOZART", "MULTICULTURAL HS", "MURPHY", "MURRAY", "NAMASTE", "NASH", "NATIONAL TEACHERS", "NEIL", "NETTELHORST", "NEW FIELD", "NEW SULLIVAN", "NEWBERRY", "NICHOLSON", "NIGHTINGALE", "NINOS HEROES", "NIXON", "NKRUMAH", "NOBEL", "NOBLE - ACADEMY HS", "NOBLE - BAKER HS", "NOBLE - BULLS HS", "NOBLE - BUTLER HS", "NOBLE - COMER", "NOBLE - DRW HS", "NOBLE - GOLDER HS", "NOBLE - HANSBERRY HS", "NOBLE - ITW SPEER HS", "NOBLE - JOHNSON HS", "NOBLE - MANSUETO HS", "NOBLE - MUCHIN HS", "NOBLE - NOBLE HS", "NOBLE - PRITZKER HS", "NOBLE - RAUNER HS", "NOBLE - ROWE CLARK HS", "NOBLE - UIC HS", "NORTH LAWNDALE - CHRISTIANA HS", "NORTH LAWNDALE - COLLINS HS", "NORTH RIVER", "NORTH-GRAND HS", "NORTHSIDE LEARNING HS", "NORTHSIDE PREP HS", "NORTHWEST", "NORWOOD PARK", "OGDEN ES", "OGDEN HS", "OGLESBY", "OKEEFFE", "OMBUDSMAN - NORTHWEST HS", "OMBUDSMAN - SOUTH HS", "OMBUDSMAN - WEST HS", "ONAHAN", "ORIOLE PARK", "OROZCO", "ORR HS", "ORTIZ DE DOMINGUEZ", "OTIS", "OTOOLE", "OWEN", "OWENS", "PALMER", "PARK MANOR", "PARKER", "PARKSIDE", "PASTEUR", "PATHWAYS - ASHBURN HS", "PATHWAYS - AVONDALE HS", "PATHWAYS - BRIGHTON PARK HS", "PAYTON HS", "PEACE AND EDUCATION HS", "PECK", "PEIRCE", "PENN", "PEREZ", "PERSHING", "PERSPECTIVES - JOSLIN HS", "PERSPECTIVES - LEADERSHIP HS", "PERSPECTIVES - MATH & SCI HS", "PERSPECTIVES - TECH HS", "PETERSON", "PHILLIPS HS", "PHOENIX MILITARY HS", "PICCOLO", "PICKARD", "PILSEN", "PIRIE", "PLAMONDON", "PLATO", "POE", "POLARIS", "PORTAGE PARK", "POWELL", "PRESCOTT", "PRIETO", "PRITZKER", "PROSSER HS", "PROVIDENCE ENGLEWOOD", "PRUSSING", "PULASKI", "PULLMAN", "RABY HS", "RANDOLPH", "RAVENSWOOD", "RAY", "REAVIS", "REILLY", "REINBERG", "REVERE", "RICHARDS HS", "RICHARDSON", "RICKOVER MILITARY HS", "ROBINSON", "ROGERS", "ROOSEVELT HS", "ROWE", "RUDOLPH", "RUGGLES", "RUIZ", "RYDER", "SABIN", "SALAZAR", "SANDOVAL", "SAUCEDO", "SAUGANASH", "SAWYER", "SAYRE", "SCAMMON", "SCHMID", "SCHUBERT", "SCHURZ HS", "SENN HS", "SEWARD", "SHERIDAN", "SHERMAN", "SHERWOOD", "SHIELDS", "SHIELDS MIDDLE", "SHOESMITH", "SHOOP", "SIMEON HS", "SIMPSON HS", "SKINNER", "SKINNER NORTH", "SMITH", "SMYSER", "SMYTH", "SOCIAL JUSTICE HS", "SOLOMON", "SOLORIO HS", "SOR JUANA", "SOUTH LOOP", "SOUTH SHORE ES", "SOUTH SHORE INTL HS", "SOUTHEAST", "SOUTHSIDE HS", "SPENCER", "SPRY ES", "SPRY HS", "STAGG", "STEINMETZ HS", "STEM", "STEVENSON", "STOCK", "STONE", "STOWE", "SUDER", "SULLIVAN HS", "SUMNER", "SUTHERLAND", "SWIFT", "TAFT HS", "TALCOTT", "TALMAN", "TANNER", "TARKINGTON", "TAYLOR", "TEAM HS", "TELPOCHCALLI", "THOMAS", "THORP J", "THORP O", "TILDEN HS", "TILL", "TILTON", "TONTI", "TURNER-DREW", "TWAIN", "U OF C - DONOGHUE", "U OF C - NKO", "U OF C - WOODLAWN HS", "U OF C - WOODSON", "UPLIFT HS", "URBAN PREP - BRONZEVILLE HS", "URBAN PREP - ENGLEWOOD HS", "URBAN PREP - WEST HS", "VANDERPOEL", "VAUGHN HS", "VICK", "VOLTA", "VON LINNE", "VON STEUBEN HS", "WACKER", "WADSWORTH", "WALSH", "WARD J", "WARD L", "WARREN", "WASHINGTON G ES", "WASHINGTON H ES", "WASHINGTON HS", "WATERS", "WEBSTER", "WELLS ES", "WELLS HS", "WENTWORTH", "WEST PARK", "WEST RIDGE", "WESTCOTT", "WESTINGHOUSE HS", "WHISTLER", "WHITE", "WHITNEY", "WHITTIER", "WILDWOOD", "WILLIAMS HS", "WOODLAWN", "WOODSON", "WORLD LANGUAGE HS", "YATES", "YCCS - ADDAMS", "YCCS - ASPIRA PANTOJA", "YCCS - ASSOCIATION HOUSE", "YCCS - AUSTIN CAREER", "YCCS - CAMPOS", "YCCS - CCA ACADEMY", "YCCS - CHATHAM", "YCCS - INNOVATIONS", "YCCS - LATINO YOUTH", "YCCS - MCKINLEY", "YCCS - OLIVE HARVEY", "YCCS - PROGRESSIVE LEADERSHIP", "YCCS - SCHOLASTIC ACHIEVEMENT", "YCCS - SULLIVAN", "YCCS - TRUMAN", "YCCS - VIRTUAL", "YCCS - WEST TOWN", "YCCS - WESTSIDE HOLISTIC", "YCCS - YOUTH CONNECTION", "YCCS - YOUTH DEVELOPMENT", "YORK HS", "YOUNG ES", "YOUNG HS", "YOUNG WOMENS HS", "ZAPATA", "60602", "60605", "60607", "60608", "60608-4408", "60608-4817", "60609", "60609-2931", "60610", "60610-1007", "60612", "60612-2613", "60613", "60614", "60614-1573", "60615", "60615-2452", "60616", "60617", "60617-6335", "60617-6726", "60618", "60618-7347", "60618-8208", "60619", "60620", "60620-4952", "60621", "60622", "60622-2854", "60623", "60623-2649", "60624", "60624-1063", "60624-2232", "60625", "60626", "60627", "60628", "60629", "60629-4822", "60629-4929", "60630", "60631", "60632", "60632-1906", "60632-1937", "60633", "60634", "60636", "60636-3307", "60636-3333", "60637", "60637-2710", "60637-3210", "60638", "60639", "60639-3009", "60640", "60641", "60642", "60642-4011", "60643", "60643-6318", "60644", "60644-1720", "60644-1906", "60645", "60646", "60647", "60649", "60649-4817", "60651", "60652", "60653", "60655", "60656", "60656-4011", "60657", "60659", "60660", "60661", "60707", "60827", "60827-1427"],
                                            select: function () {
                                                $("input#ctl00_ContentPlaceHolder1_btnFind").trigger('click');
                                            }
                                        });
                                });


                            </script>

                            <div class="backgroundimage">
                                <div class="container" style="margin-bottom:300px;">
                                    <div class="row" style="padding-bottom: 20px; padding-top: 20px;">
                                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                                            <h1 class="text-primary">Search Schools</h1>
                                            <div class="input-group">
                                                <input name="query_field" type="text"
                                                       id="ctl00_ContentPlaceHolder1_txtSchoolOrZip"
                                                       class="input-lg form-control" title="Enter School Name"
                                                       placeholder="Enter School Name or School Code"
                                                       style="width:100%;"/>
                                                <span class="input-group-btn"><input type="submit" name="find_one"
                                                                                     value="Find"
                                                                                     id="ctl00_ContentPlaceHolder1_btnFind"
                                                                                     class="btn btn-primary btn-lg"/></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center" style="padding-bottom: 40px;">
                                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="collapse"
                                                    data-target="#advancedOptions" aria-expanded="false"
                                                    aria-controls="collapseExample">Advanced Search
                                            </button>
                                            <input type="submit" name="show_all" value="Show All Schools"
                                                   id="ctl00_ContentPlaceHolder1_btnShowAll"
                                                   class="btn btn-primary btn-lg"/>
                                        </div>
                                    </div>
                                    <div class="collapse" id="advancedOptions" style="font-size:16px;">
                                        <div class="well" style="opacity:0.85;">
                                            <div class="row" style="padding-bottom: 20px;">
                                                <div class="col-sm-3 col-sm-offset-1">
                                                    <h4 class="text-primary">Province</h4>
                                                    @foreach($provinces as $province)
                                                        <span class="checkbox h5 grey-dark fasInput"><input
                                                                    id="{{ $province->Province }}" type="checkbox"
                                                                    name="province[]"
                                                                    value="{{ $province->Province }}"/><label
                                                                    for="{{ $province->Province }}">{{ $province->Province }}</label></span>
                                                    @endforeach
                                                    <h4 class="text-primary">School Types</h4>
                                                    @foreach($schooltypes as $schooltype)
                                                        <span class="checkbox h5 grey-dark fasInput"><input
                                                                    id="{{ $schooltype->school_type }}" type="checkbox"
                                                                    name="schooltype[]"
                                                                    value="{{ $schooltype->id }}"/><label
                                                                    for="{{ $schooltype->school_type }}">{{ $schooltype->school_type }}</label></span>
                                                    @endforeach
                                                    <h4 class="text-primary" style="margin-top:5px;">School Rating</h4>
                                                    @foreach($schoolratings as $schoolrating)
                                                        <span class="checkbox h5 grey-dark fasInput"><input
                                                                    id="{{ $schoolrating->rating }}" type="checkbox"
                                                                    name="schoolrating[]"
                                                                    value="{{ $schoolrating->id }}"/><label
                                                                    for="{{ $schoolrating->rating }}">{{ $schoolrating->rating }}</label></span>
                                                    @endforeach

                                                </div>
                                                <div class="col-sm-4">
                                                    <h4 class="text-primary">
                                                        School Qualifications</h4>
                                                    <table id="ctl00_ContentPlaceHolder1_cblSchoolProgramTypes"
                                                           class="checkbox h5 grey-dark fasInput"
                                                           title="School Programs" border="0">
                                                        @foreach($qualifications as $qualification)
                                                        <tr>
                                                            <td>
                                                                <input id="{{ $qualification->qualification_title }}"
                                                                       type="checkbox" name="qualification[]"
                                                                       value="{{ $qualification->id }}"/><label
                                                                        for="{{ $qualification->qualification_title }}">{{ $qualification->qualification_title }}</label>
                                                            </td>
                                                        </tr>
                                                        @endforeach

                                                    </table>
                                                </div>
                                                <div class="col-sm-3">
                                                    <h4 class="text-primary">Districts</h4>
                                                    @foreach($districts as $district)

                                                        <span class="checkbox h5 grey-dark fasInput"><input
                                                                    id="{{ $district->District }}" type="checkbox"
                                                                    name="district[]"
                                                                    value="{{ $district->District }}"/><label
                                                                    for="{{ $district->District }}">{{ $district->District }}</label></span>
                                                        <h4 class="text-primary" style="margin-top:5px;"></h4>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="row text-center">
                                                <div class="col-sm-4 col-sm-offset-4">
                                                    <input type="submit" name="advanced_search" value="Find"
                                                           id="ctl00_ContentPlaceHolder1_Button1"
                                                           class="btn btn-primary btn-lg btn-block"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-bottom: 20px;">
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                            <div class="well well-lg text-center">
                                                <h4>Looking for a school near your home?</h4>
                                                <a href="{{ route('home.schools.map') }}" target="_blank"
                                                   class="btn btn-primary btn-lg">Go to
                                                    School Locator Tool</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="CPS_error">
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('.bubbleInfo').each(function () {
                                        var distance = 10;
                                        var time = 10;
                                        var hideDelay = 20;

                                        var hideDelayTimer = null;

                                        var beingShown = false;
                                        var shown = false;
                                        var trigger = $('.trigger', this);
                                        var info = $('.popup', this).css('opacity', 0);

                                        $([trigger.get(0), info.get(0)]).mouseover(function () {
                                            if (hideDelayTimer) clearTimeout(hideDelayTimer);
                                            if (beingShown || shown) {
                                                // don't trigger the animation again
                                                return;
                                            } else {
                                                // reset position of info box
                                                beingShown = true;

                                                info.css({
                                                    top: -110,
                                                    left: 20,
                                                    display: 'block'
                                                }).animate({
                                                    top: '-=' + distance + 'px',
                                                    opacity: 1
                                                }, time, 'swing', function () {
                                                    beingShown = false;
                                                    shown = true;
                                                });
                                            }

                                            return false;
                                        }).mouseout(function () {
                                            if (hideDelayTimer) clearTimeout(hideDelayTimer);
                                            hideDelayTimer = setTimeout(function () {
                                                hideDelayTimer = null;
                                                info.animate({
                                                    top: '-=' + distance + 'px',
                                                    opacity: 0
                                                }, time, 'swing', function () {
                                                    shown = false;
                                                    info.css('display', 'none');
                                                });

                                            }, hideDelay);

                                            return false;
                                        });
                                    });

                                });

                            </script>

                        </div>
                    </form>
                    {{--##################################################################################--}}
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="mbr-section mbr-section-small mbr-footer" id="contacts1-m" data-rv-view="178"
         style="background-color: rgb(55, 56, 62); padding-top: 4.5rem; padding-bottom: 4.5rem;">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="img-responsive">
                    <img src="{{ asset('img/app_logo.jpg') }}" width="200px">
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>
                    <strong>Address</strong><br>
                    Kicukiro Niboye<br>
                    KK 15 Rd, Kigali
                </p>
            </div>
            <div class="col-xs-12 col-md-3">
                <p><strong>Contacts</strong><br>
                    Email: rp@gmail.com<br>
                    Phone: +250 788 4564<br>
                </p>
            </div>
            <div class="col-xs-12 col-md-3"><strong>Links</strong>
                <ul>
                    <li><a href="#">RP Website</a></li>
                    <li><a href="#">WDA Website</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"--}}
{{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>--}}

{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"--}}
{{--integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--}}
{{--crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
        integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
        integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
{{--Datables--}}
<script src="{{ asset('js/main.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
{{--Datatable Exporting--}}
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.js"
        integrity="sha256-vtAyL1164tJW23BstoGrdXyOXvBR47n1PoKtlT0CEdE=" crossorigin="anonymous"></script>
<script>
    $(function () {
        $(".select2, select").select2();
        $("#dataTable").dataTable();
        $("#dataTableBtn").dataTable({
            dom: 'Blfrtip',
            pager: true,
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed two-column'
                }
            ],
            columnDefs: [
                {
                    targets: [],
                    visible: false,
                }
            ]
        });
        $('#rootwizard').bootstrapWizard();
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>

</body>
</html>
