@php
    $status = session()->get('status');
    $message = Session()->get('message');
@endphp
@if ($status == '1')
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @if(is_string($message))
            <strong>{!!  $message !!}</strong>
        @elseif(is_array($message))
            <ul>
                @foreach ($message as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
@elseif ($status == '0')
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @if(is_string($message))
            <strong>{!!  $message !!}</strong>
        @elseif(is_array($message))
            <ul>
                @foreach ($message as $error)
                    <li>{!! isset($error['message']) ? $error['message'] : $error !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
@elseif ($status == '3')
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @if(is_string($message))
            <strong>{!!  $message !!}</strong>
        @elseif(is_array($message))
            <ul>
                @foreach ($message as $error)
                    <li>{!! isset($error['message']) ? $error['message'] : $error !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
@elseif($status == '4')
    <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @if(is_string($message))
            <strong>{!!  $message !!}</strong>
        @elseif(is_array($message))
            <ul>
                @foreach ($message as $error)
                    <li>{!! isset($error['message']) ? $error['message'] : $error !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif