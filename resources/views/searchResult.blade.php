<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
</head>
<body>

<nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144"
     style="background-color: rgb(255, 255, 255);">
    <div class="container">
        <div class="row bg-white p-2" style="width: inherit !important;">
            <div class="col-md-9">
                <div class="navbar-toggleable-sm">
                    <span class="navbar-logo"><a href="#"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                    <span><a class="navbar-brand" href="#">{{ config('app.name') }}</a></span>
                    <!-- Example single danger button -->

                    <a class="btn btn-sm btn-dark shadow-sm"
                       href="{{ route('curricula.index') }}">
                        <span>Curriculum</span>
                    </a>

                    <a class="btn btn-sm btn-info shadow-sm"
                       href="{{ route('home.schools') }}">
                        <span>Schools</span>
                    </a>

                    <span class="dropdown">
                        <button class="btn btn-sm btn-warning shadow-sm btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Support
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               href="{{ route('school.request.account') }}">Schools Account Request</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item"
                               href="{{ route('contact.us') }}">Contact Us</a>
                        </div>
                    </span>

                </div>
            </div>
            <div class="col-md-3">
                <div class="btn-group" style="float: right !important;">
                    <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        General Login
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('school.login') }}">Schools Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('college.login') }}">Colleges
                            Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('rp.login') }}">RP Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('district.login') }}">District Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('wda.login') }}">WDA Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/register') }}">REB | Create Account</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/login') }}">REB | Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
         style="background-image: url({{asset('img/bg_wood.png')}});background-attachment: fixed;">
    <div class="mbr-table-cell">
        <div class="container">
            <div class="row">
                <div>
                    <style>
                        a.customPhoneLink:link,
                        a.customPhoneLink:visited,
                        a.customPhoneLink:active,
                        a.customPhoneLink:hover {
                            color: currentColor;
                            cursor: inherit;
                            text-decoration: none;
                        }
                    </style>
                    <script type="text/javascript" src="SearchResults.js"></script>
                    <script type="text/javascript" language="javascript">
                        var ESYS = {};
                        $(document).ready(function () {
                            ESYS.Glbl =
                                {
                                    UNITS_SELECTED_DLM: ':::',
                                    QUERY_UNIT_PARM: 'SchoolId',
                                    DIV_TOP_MSG: document.getElementById("divMsg"),
                                    DIV_BOTTOM_MSG: document.getElementById("divMsgBottom"),
                                    NAMES_SELECTED_LBL: document.getElementById('ctl00_ContentPlaceHolder1_lblNamesSelected'),
                                    UNITS_SELECTED_OBJ: document.getElementById('ctl00_ContentPlaceHolder1_hUnitsSelected'),
                                    NAMES_SELECTED_OBJ: document.getElementById('ctl00_ContentPlaceHolder1_hNamesSelected'),
                                    unitsSelectedArray: {},
                                    namesSelectedArray: {},
                                    unitsSelectedCount: {}
                                }
                            ESYS.Glbl.unitsSelectedArray = ESYS.Glbl.UNITS_SELECTED_OBJ.value.split(ESYS.Glbl.UNITS_SELECTED_DLM);
                            ESYS.Glbl.namesSelectedArray = ESYS.Glbl.NAMES_SELECTED_OBJ.value.split(ESYS.Glbl.UNITS_SELECTED_DLM);
                            ESYS.Glbl.unitsSelectedCount = CountNonBlankEntries(ESYS.Glbl.unitsSelectedArray);
                            if (ESYS.Glbl.DIV_TOP_MSG) //Existence check. When no search results, grid is not in HTML.
                            {
                                SetSelectionMessages();
                                $('#divGridContents a[id$="hyplnkSource"]').each(function (n) {
                                    var isSelected = IsInUnitsArray(ESYS.Glbl.unitsSelectedArray, GetUnitFromUrl(this.href))
                                    $(this).parents('tr:first').children('td').find('input:checkbox[id$="chkSelect"]')[0].checked = isSelected;
                                });
                            }

                            $(".btnFindAnotherSchool").click(function () {
                                var pageurl = top.location.toString();
                                var x = pageurl.split('?')[0]; //get rid of last part of url
                                //var y = x.substring(x.lastIndexOf("/") + 1); //gets last element
                                var to = x.lastIndexOf('/');
                                to = to == -1 ? x.length : to + 1;
                                x = x.substring(0, to);
                                x += "FindaSchool.aspx";
                                window.top.location.href = x;
                            });
                        });
                    </script>

                    <div class="container bg-white">
                        <h4>Search Results</h4>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" id="tblControls">
                            <tr>
                                <td valign="top">
                                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hUnitsSelected"
                                           id="ctl00_ContentPlaceHolder1_hUnitsSelected"/>
                                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hNamesSelected"
                                           id="ctl00_ContentPlaceHolder1_hNamesSelected"/>
                                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hCheckboxUnits"
                                           id="ctl00_ContentPlaceHolder1_hCheckboxUnits"/>
                                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hCheckboxIds"
                                           id="ctl00_ContentPlaceHolder1_hCheckboxIds"/>

                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <div id="ctl00_ContentPlaceHolder1_pnlControls">

                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td valign="top" class="label label-primary"
                                                    style="font-size: 100%; font-weight: normal;">
                                                    <span id="ctl00_ContentPlaceHolder1_lblInfo">1- of {{ $schools->count() }}</span>
                                                    <span id="ctl00_ContentPlaceHolder1_lblFilter"></span>
                                                    <div class="pull-right" style="margin-right: 10px;">
                                                        <a id="ctl00_ContentPlaceHolder1_lnkMapSelections"
                                                           title="Show map of schools on this page"
                                                           class="btn btn-primary btn-xs "
                                                           href="{{ route('home.schools.map') }}"
                                                           target="_blank">View these schools on map</a>
                                                        <a href="{{ route('home.schools') }}"
                                                           class="btn btn-primary btn-xs btnFindAnotherSchool">Search
                                                            Again</a></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>

                                                            <td>
                                                                To compare the features of <strong>up to 4
                                                                    schools</strong>, select at least 2 checkboxes, then
                                                                click "Compare".
                                                                <div id="divComparisonSelections" style="display:none">
                                                                    <br/><span
                                                                            id="divMsg">You have selected<strong> 0 </strong>of 4 schools to compare:</span>
                                                                    <br/><span
                                                                            id="ctl00_ContentPlaceHolder1_lblNamesSelected"></span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px; padding-bottom: 10px;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="99%">
                                                        <tr>
                                                            <td class="" valign="top" style="width: 150px;">

                                                                <input type="submit"
                                                                       name="ctl00$ContentPlaceHolder1$btnCompareTop"
                                                                       value="Compare"
                                                                       onclick="return ValidateSelection();"
                                                                       id="ctl00_ContentPlaceHolder1_btnCompareTop"
                                                                       class="btn btn-primary btn-sm"/>
                                                            </td>
                                                            <td style="text-align: center;" valign="top">

                                                            </td>
                                                            <td style="text-align: right;" valign="top">


                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <div id="divGridContents">
                                                        <div>
                                                            <table class="table table-condensed" cellspacing="0"
                                                                   cellpadding="0" border="0"
                                                                   id="ctl00_ContentPlaceHolder1_gvSchoolSearchResults"
                                                                   style="width:100%;border-collapse:collapse;"
                                                                   id="dataTable">
                                                                <tr>
                                                                    <th scope="col">&nbsp;</th>
                                                                    <th class="h5 text-bold text-left" align="left"
                                                                        scope="col">
                                                                        <a href="#">School▲</a>
                                                                    </th>
                                                                    <th class="h5 text-bold text-center" scope="col">
                                                                        <a href="#">School Type</a>
                                                                    </th>
                                                                    <th class="h5 text-bold text-center" scope="col">
                                                                        Grades
                                                                    </th>
                                                                    <th class="h5 text-bold text-center" scope="col">
                                                                        <a href="#">Students</a>
                                                                    </th>
                                                                    <th class="h5 text-bold text-center" scope="col">
                                                                        <a href="#">Rating</a>
                                                                    </th>
                                                                </tr>

                                                                @foreach($schools as $school)
                                                                    <tr>
                                                                        <td valign="top" style="width:30px;">
                                                                            <div class="text-center">
                                                                                <input name="ctl00$ContentPlaceHolder1$gvSchoolSearchResults$ctl03$chkSelect"
                                                                                       type="checkbox"
                                                                                       id="ctl00_ContentPlaceHolder1_gvSchoolSearchResults_ctl03_chkSelect"
                                                                                       title="Select"
                                                                                       onclick="ModifySelectedUnits(this,'400153','ACERO - BRIGHTON PARK');"/>
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <div class="text-left">
                                                                                <a id="ctl00_ContentPlaceHolder1_gvSchoolSearchResults_ctl03_hyplnkSource"
                                                                                   class="h5 blue-dark"
                                                                                   href="{{ route('home.schools',['slugname'=> $school->school_acronym]) }}">{{ $school->school_name }}</a>
                                                                                <br/>
                                                                                {{$school->province}}
                                                                                , {{$school->district}}
                                                                                , {{$school->sector }}
                                                                                <br/>
                                                                                <a id="ctl00_ContentPlaceHolder1_gvSchoolSearchResults_ctl03_hyplnkPhone"
                                                                                   class="customPhoneLink"
                                                                                   href="tel:312-455-5434">{{$school->school_code}}</a>
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <div class="text-center">
                                                                                <span id="ctl00_ContentPlaceHolder1_gvSchoolSearchResults_ctl03_lblType">
                                                                                    {{ @$school->schooltype->school_type }}
                                                                                </span>
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <div class="text-center">
                                                                                <span id="ctl00_ContentPlaceHolder1_gvSchoolSearchResults_ctl03_lblGrades">
                                                                                     {{-- @foreach(@givemeLevels($school) as $grade)
                                                                                        {{ @$grade  }},
                                                                                    @endforeach --}}
                                                                                    {{-- @if(count(givemeLevels($school)) > 0)
                                                                                        @foreach(@givemeLevels($school) as $grade)
                                                                                            {{ @$grade  }},
                                                                                        @endforeach
                                                                                    @endif --}}
                                                                                </span>
                                                                            </div>
                                                                        </td>
                                                                        <td align="center" valign="top">
                                                                            <div class="text-center">
                                                                                <span id="ctl00_ContentPlaceHolder1_gvSchoolSearchResults_ctl03_lblStudents">
                                                                                    {{ @$school->students->count() }}
                                                                                </span>
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <div class="text-center">
                                                                                {{ @$school->schoolrating->rating }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">


                                                </td>
                                            </tr>

                                        </table>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>

                <input type="hidden" name="ctl00$lblVersion" id="ctl00_lblVersion" value="2.0.2.0(04)"/>
                <!-- iframeResizer JS -->
                <script type="text/javascript" src="js/iframeResizer.min.js"></script>
                <script type="text/javascript" src="js/iframeResizer.contentWindow.min.js"></script>
                <script type="text/javascript">
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-33983846-42', 'cps.edu');
                    ga('send', 'pageview');

                </script>

                </form>

                {{--##################################################################################--}}
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>

    </div>
</section>

<section class="mbr-section mbr-section-small mbr-footer" id="contacts1-m" data-rv-view="178"
         style="background-color: rgb(55, 56, 62); padding-top: 4.5rem; padding-bottom: 4.5rem;">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="img-responsive">
                    <img src="{{ asset('img/app_logo.jpg') }}" width="200px">
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>
                    <strong>Address</strong><br>
                    Kicukiro Niboye<br>
                    KK 15 Rd, Kigali
                </p>
            </div>
            <div class="col-xs-12 col-md-3">
                <p><strong>Contacts</strong><br>
                    Email: rp@gmail.com<br>
                    Phone: +250 788 4564<br>
                </p>
            </div>
            <div class="col-xs-12 col-md-3"><strong>Links</strong>
                <ul>
                    <li><a href="#">RP Website</a></li>
                    <li><a href="#">WDA Website</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"--}}
{{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>--}}

{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"--}}
{{--integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--}}
{{--crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
        integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
        integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
{{--Datables--}}
<script src="{{ asset('js/main.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
{{--Datatable Exporting--}}
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.js"
        integrity="sha256-vtAyL1164tJW23BstoGrdXyOXvBR47n1PoKtlT0CEdE=" crossorigin="anonymous"></script>
<script>
    $(function () {
        $(".select2, select").select2();
        $("#dataTable").dataTable();
        $("#dataTableBtn").dataTable({
            dom: 'Blfrtip',
            pager: true,
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed two-column'
                }
            ],
            columnDefs: [
                {
                    targets: [],
                    visible: false,
                }
            ]
        });
        $('#rootwizard').bootstrapWizard();
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>

</body>
</html>
