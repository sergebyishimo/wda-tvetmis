<table class="table table-condensed table-bordered">
    <thead>
    <tr>
        <th class="text-center">#</th>
        <th class="text-center">Title</th>
        <th class="text-center">Max Marks</th>
        <th class="text-center">Semester</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @php
        $x = 1;
    @endphp
    @forelse($categories as $category)
        <tr>
            <td class="text-center">{{ $x++ }}</td>
            <td class="text-center">{{ $category->title }}</td>
            <td class="text-center">{{ $category->max_marks }}</td>
            <td class="text-center">{{ $category->semester }}</td>
            <td class="text-center">
                <button data-row="{{ $category->id }}" class="btn btn-danger delete-course">Delete</button>
            </td>
        </tr>
    @empty
        <tr>
            <td class="text-center" colspan="5">No data !!</td>
        </tr>
    @endforelse
    </tbody>
</table>