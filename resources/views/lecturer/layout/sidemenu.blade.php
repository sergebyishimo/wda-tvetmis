@if(auth()->guard('lecturer')->check())
    <li class="{{ activeMenu('lecturer.home') }}">
        <a href="{{ route('lecturer.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>

    @if (isHOD())
        <li class="treeview {{ activeMenu([
        'lecturer.assign',
        ]) }}">
            <a href="{{ route('lecturer.assign') }}">
                <i class='fa fa-legal'></i>&nbsp;
                <span>Manage Lecturers</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['lecturer.assign']) }}">
                    <a href="{{ route('lecturer.assign') }}">
                        <i class='fa fa-link'></i>
                        <span>Geo Location</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <li class="treeview {{ activeMenu([
            'lecturer.grouping.index',
            'lecturer.marks.select.class', 'lecturer.marks.post.select.class',
            'lecturer.marks.view.class', 'lecturer.marks.post.view.class',
            'lecturer.marks.select.class.exam', 'lecturer.marks.view.exam.class'
        ]) }}">
        <a href="{{ '#' }}">
            <i class='fa fa-sort-numeric-desc'></i>&nbsp;
            <span>Manage Marks</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            {{--<li class="{{ activeMenu(['lecturer.grouping.index']) }}">--}}
                {{--<a href="{{ route('lecturer.grouping.index') }}">--}}
                    {{--<i class='fa fa-link'></i>--}}
                    {{--<span>Categorizing</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="treeview {{ activeMenu([
                'lecturer.marks.select.class', 'lecturer.marks.post.select.class',
                'lecturer.marks.view.class', 'lecturer.marks.post.view.class',
                'lecturer.marks.select.class.exam', 'lecturer.marks.view.exam.class',
                ]) }}">
                <a href="#">
                    <i class='fa fa-hand-grab-o'></i>
                    <span>Marks Entry & View</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview {{ activeMenu([
                        'lecturer.marks.select.class', 'lecturer.marks.post.select.class',
                        'lecturer.marks.view.class', 'lecturer.marks.post.view.class',
                        ]) }}">
                        <a href="#">
                            <i class='fa fa-pencil'></i>
                            <span>Period</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ activeMenu('lecturer.marks.select.class') }}">
                                <a href="{{ route('lecturer.marks.select.class') }}"><i
                                            class='fa fa-link'></i>&nbsp;<span>Marks Entry</span></a>
                            </li>
                            <li class="{{ activeMenu('lecturer.marks.view.class') }}">
                                <a href="{{ route('lecturer.marks.view.class') }}"><i
                                            class='fa fa-link'></i>&nbsp;<span>Marks View</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview {{ activeMenu([
                        'lecturer.marks.select.class.exam', 'lecturer.marks.view.exam.class'
                        ]) }}">
                        <a href="#">
                            <i class='fa fa-link'></i>
                            <span>Exams</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ activeMenu('lecturer.marks.select.class.exam') }}">
                                <a href="{{ route('lecturer.marks.select.class.exam') }}"><i
                                            class='fa fa-link'></i>&nbsp;<span>Marks Entry</span></a>
                            </li>
                            <li class="{{ activeMenu('lecturer.marks.view.exam.class') }}">
                                <a href="{{ route('lecturer.marks.view.exam.class') }}"><i
                                            class='fa fa-link'></i>&nbsp;<span>Marks View</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
@endif