@extends('lecturer.layout.main')

@section('panel-title', "Dashboard")

@section('htmlheader_title', "Lecturer Dashboard")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>Module</th>
                            <th>Program</th>
                            <th class="text-center">YearOfStudy</th>
                            <th class="text-center">Semester</th>
                            <th class="text-center">Student</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $count = [1 => 0, 2 => 0, 3 => 0];
                        @endphp
                        @foreach ($modules as $module)
                            @php
                                $course = App\Model\Rp\LecturerAndCourse::find($module->id);
                                if ($course)
                                    $count[$module->year_of_study] += getStudentFromLecturer($course)->count();
                            @endphp
                            <tr @if (getLecturerCourse($module, 'semester') == getCurrentSemester()) class="bg-green-active"
                                @else class="bg-gray-light" @endif>
                                <td>{{ getLecturerCourse($module, 'name') }}</td>
                                <td>{{ getLecturerCourse($module, 'program') }}</td>
                                <td class="text-center">{{ $module->year_of_study }}</td>
                                <td class="text-center">{{ getLecturerCourse($module, 'semester') }}</td>
                                <td class="text-center">{{ getStudentFromLecturer($course)->count() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green-active">
                <div class="inner">
                    <h3>{{ getCurrentSemester() }}
                        <sub class="pull-right"><small class="text-white">{{ getCurrentAcademicYear() }}</small></sub>
                    </h3>
                    <p>Active Semester</p>
                </div>
                <div class="icon pt-2">
                    <i class="fa fa-hand-o-up" style="font-size: 70px"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $count[1] }}</h3>
                    <p>First Year</p>
                </div>
                <div class="icon">
                    <i class="ion ion-disc"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-gray">
                <div class="inner">
                    <h3>{{ $count[2] }}</h3>

                    <p>Second Year</p>
                </div>
                <div class="icon">
                    <i class="ion ion-knife"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue-active">
                <div class="inner">
                    <h3>{{ $count[3] }}</h3>
                    <p>Third Year</p>
                </div>
                <div class="icon">
                    <i class="ion ion-icecream"></i>
                </div>
            </div>
        </div>
    </div>
@endsection
