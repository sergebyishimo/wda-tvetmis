@extends('college.layout.main')

@section('panel-title', "Class List With Exam and Periodic Marks")

@section('htmlheader_title', "Class List With Exam and Periodic Marks")

@section('l-style')
    @parent
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <style>
        .number_ex {
            border: 2px solid #ff0000;
        }

        .popover-danger, .bg-danger-light {
            background-color: #d9534f;
            border-color: #d43f3a;
            color: white;
        }

        .popover-danger.right .arrow:after {
            border-right-color: #d9534f;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['id' => 'selectModule','method' => 'GET']) !!}
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {!! Form::label('module', "Modules", ['class' => 'control-label']) !!}
                            {!! Form::select('module', $cModules, request()->input('module'), ['class' =>'form-control select2', 'placeholder' => 'select ...']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @isset($students)
            <div class="box">
                <div class="box-header with-border">
                    <table class="table table-condensed">
                        <tr>
                            <td class="bg-gray-light">Program</td>
                            <td>: {{ strtoupper(getLecturerCourse($course, 'program')) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Course</td>
                            <td>: {{ strtoupper(getLecturerCourse($course, 'name')) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Semester</td>
                            <td>: {{ strtoupper(getLecturerCourse($course, 'semester')) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Academic Year</td>
                            <td>: {{ strtoupper($course->academic_year) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Year of Study</td>
                            <td>: {{ $course->year_of_study }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Total Students</td>
                            <td>: {{ $students->count() }}</td>
                        </tr>
                    </table>
                </div>
                <div class="box-body" style="overflow-x: auto;">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open(['route' => 'lecturer.marks.submit.marks', 'id' => 'submitFinalMarks']) !!}
                            {!! Form::hidden('course', $course->id) !!}
                            <table class="table table-striped table-bordered" id="dataTableBtnMarks">
                                <thead class="bg-gray-light">
                                <tr>
                                    <th>StudentID</th>
                                    <th>Names</th>
                                    @php
                                        $total = 0;
                                    @endphp
                                    <th class="text-center">Cat /60</th>
                                    <th class="text-center">Exam /40</th>
                                    <th class="text-center">Total /100</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($students as $student)
                                    @php
                                        $ccat = null;
                                        $cat = getStudentModuleMarks($student->getModuleMarks($course->course_id), null, "sum");
                                        if ($cat)
                                            $ccat = $totalCat > 0 ? round((($cat * 60) / $totalCat), 1) : 0;
                                        $exam = getStudentExamModuleMarks($student->getExamModuleMarks($course->course_id), 'marks');
                                        $total = $ccat + $exam;
                                    @endphp
                                    <tr>
                                        <td>{{ $student->student_reg }}</td>
                                        <td>{{ $student->names }}</td>
                                        <td class="text-center {{ $ccat < 30 ? 'bg-danger-light' : '' }}">{{ $ccat ?:'-' }}</td>
                                        <td class="text-center {{ $exam < 20 ? 'bg-danger-light' : '' }}">{{ $exam ?:'-' }}</td>
                                        <td class="text-center {{ $total < 50 ? 'bg-danger-light' : '' }}">
                                            {{ $total }}
                                            @if($ccat && $exam)
                                                {!! Form::hidden('students['.$student->student_reg.']', $total) !!}
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">No Students available</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <div id="holderInput" style="opacity: 0;"></div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $("#module").on('change', function () {
                let v = $(this).val();
                if (v)
                    $("#selectModule").submit();
            });
        });
    </script>
    @isset($students)
        <script type="text/javascript">
            $(function () {
                var table = $("#dataTableBtnMarks").dataTable({
                    dom: 'Blfrtip',
                    page: true,
                    "ordering": true,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            },
                            title: "{{ str_replace(' ', '', strtolower($course->academic_year)) }}_{{ str_replace(' ', '_', strtolower(getLecturerCourse($course, 'name'))) }}_exam_and_cat"
                        }, {
                            text: 'Submit Processed Marks',
                            action: function (e, dt, node, config) {
                                let l = $("#holderInput");
                                var data = table.$('input, select').each(function (e, d) {
                                    l.append(d);
                                });
                                $("#submitFinalMarks").submit();
                                return false;
                            }
                        }
                    ],
                    columnDefs: [
                        {
                            targets: [],
                            visible: false,
                        }
                    ]
                });
            });
        </script>
    @endisset
@show