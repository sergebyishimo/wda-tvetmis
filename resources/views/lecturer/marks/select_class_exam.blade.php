@extends('college.layout.main')

@section('panel-title', "Exam Entry")

@section('htmlheader_title', "Exam Entry")

@section('l-style')
    @parent
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <style>
        .number_ex {
            border: 2px solid #ff0000;
        }

        .popover-danger {
            background-color: #d9534f;
            border-color: #d43f3a;
            color: white;
        }

        .popover-danger.right .arrow:after {
            border-right-color: #d9534f;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body">
                @if(isExamTime())
                    {!! Form::open(['id' => 'selectModule','method' => 'GET']) !!}
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group">
                                {!! Form::label('module', "Modules", ['class' => 'control-label']) !!}
                                {!! Form::select('module', $cModules, request()->input('module'), ['class' =>'form-control select2', 'placeholder' => 'select ...']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                @else
                    <div class="alert alert-danger text-center">
                        Not in Exam Period !!
                    </div>
                @endif
            </div>
        </div>
        @isset($students)
            <div class="box">
                <div class="box-header with-border">
                    <table class="table table-condensed">
                        <tr>
                            <td class="bg-gray-light">Program</td>
                            <td>: {{ strtoupper(getLecturerCourse($course, 'program')) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Course</td>
                            <td>: {{ strtoupper(getLecturerCourse($course, 'name')) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Semester</td>
                            <td>: {{ strtoupper(getLecturerCourse($course, 'semester')) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Academic Year</td>
                            <td>: {{ strtoupper($course->academic_year) }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Year of Study</td>
                            <td>: {{ $course->year_of_study }}</td>
                        </tr>
                        <tr>
                            <td class="bg-gray-light">Total Students</td>
                            <td>: {{ $students->count() }}</td>
                        </tr>
                    </table>
                    <h2 class="text-center">Exam</h2>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            {!! Form::open(['files' => true, 'route' => 'lecturer.marks.save.exam.uploaded']) !!}
                            {!! Form::label('Marks Files (excel)') !!}
                            {!! Form::file('marks') !!}
                            {!! Form::submit('Upload', ['class' => 'btn btn-primary mt-5']) !!}
                            {!! Form::hidden('course', $course->id, ['id' => 'courseID']) !!}
                            {!! Form::close() !!}
                            <p class="alert alert-warning" style="color: #0c0c0c !important; font-weight: bold;">
                                Use the list or download the excel file ...
                            </p>
                        </div>
                        <div class="col-md-6">

                            <table class="table table-striped table-bordered" id="dataTableBtnMarks">
                                <thead class="bg-gray-light">
                                <tr>
                                    <td>StudentID</td>
                                    <td>Names</td>
                                    <td width="50px">Marks / 40</td>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($students as $student)
                                    @php
                                        $marks = getStudentExamModuleMarks($student->getExamModuleMarks($course->course_id), 'marks');
                                    @endphp
                                    <tr>
                                        <th>{{ $student->student_reg }}</th>
                                        <th>{{ $student->names }}</th>
                                        <th width="85px">
                                            {!! Form::number('marks['.$student->student_reg.']', $marks, ['class' => 'form-control entry', 'data-max' => 40, 'data-content' => 'Please, enter max less than '. 40 .' !!', 'data-placement' => 'top' ]) !!}
                                        </th>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">No Students available</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>

                        </div>
                        <div class="col-md-3" id="feedbackSection">
                            <div id="formMarksSavingHidding" style="opacity: 0;">
                                {!! Form::open(['route' => 'lecturer.marks.save.exam', 'id' => 'formMarksSaving']) !!}
                                {!! Form::hidden('course', $course->id, ['id' => 'course_id']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $("#module").on('change', function () {
                let v = $(this).val();
                if (v)
                    $("#selectModule").submit();
            });
            $("* .entry").keyup(function (e) {
                let o = $(this).val();
                let mx = $(this).data('max');
                if (o >= 0 && o <= mx) {
                    $(this).removeClass('number_ex');
                    $(this).popover('hide');
                }
                else {
                    $(this).addClass('number_ex');
                    $(this).popover('show');
                    $(this).next('.popover').addClass('popover-danger');
                }
            });
        });
    </script>
    @isset($students)
        <script type="text/javascript">
            $(function () {
                var table = $("#dataTableBtnMarks").dataTable({
                    dom: 'B<"toolbar">frtip',
                    "ordering": false,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            },
                            title: "{{ str_replace(' ', '', strtolower($course->academic_year)) }}_exam_{{ str_replace(' ', '_', strtolower(getLecturerCourse($course, 'name'))) }}"
                        }, {
                            text: 'Save Marks',
                            action: function (e, dt, node, config) {
                                let l = $("#formMarksSaving");
                                var data = table.$('input, select').each(function (e, d) {
                                    l.append(d);
                                });
                                $("#formMarksSaving").submit();
                                return false;
                            }
                        }
                    ],
                    columnDefs: [
                        {
                            targets: [],
                            visible: false,
                        }
                    ]
                });
            });
        </script>
    @endisset
@show