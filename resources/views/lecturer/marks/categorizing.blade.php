@extends('college.layout.main')

@section('panel-title', "Categorizing")

@section('htmlheader_title', "Categorizing")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box">

            <div class="box-body">
                {!! Form::open([]) !!}
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {!! Form::label('module', "Modules", ['class' => 'control-label']) !!}
                            {!! Form::select('module', $cModules, null, ['class' =>'form-control select2', 'placeholder' => 'select ...']) !!}
                        </div>
                    </div>
                </div>
                <div class="row lecturer-proper">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('title', "Title", ['class' => 'control-label']) !!}
                                {!! Form::text('title', null,['class' => 'form-control', 'placeholder' => 'ex: Quiz 2 or Cat 1 ...', 'required' => true]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('max_marks', "Total Marks", ['class' => 'control-label']) !!}
                                {!! Form::number('max_marks', null,['class' => 'form-control', 'placeholder' => '...', 'required' => true]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('date_taken', "Due Date", ['class' => 'control-label']) !!}
                                {!! Form::date('date_taken', null,['class' => 'form-control', 'placeholder' => '...', 'required' => true]) !!}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-left">
                                    <i class="fa fa-save"></i>
                                    <span>Save</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-8" style="overflow-y: auto;">
                        <h4 class="text-center bg-primary">Available Marks Groups</h4>
                        <div id="content-course">
                            <center>
                                <img src="{{ asset('img/ajaxload.gif') }}" class="mt-2"/>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script type="text/javascript">
        $(function () {
            $(".lecturer-proper").hide();
            $("#module").on("change", function () {
                let v = $(this).val();
                if (v) {
                    $("#content-course").load("/lecturer/data/category/" + v);
                    $(".lecturer-proper").fadeIn(555);
                } else {
                    $(".lecturer-proper").fadeOut();
                }
            });
        });
    </script>
@show