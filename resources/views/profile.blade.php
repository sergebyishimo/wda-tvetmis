@extends('wda.layout.main')

@section('htmlheader_title')
    User Profile
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        User Profile
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-4">
                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Profile Picture</h3>
                    </div>
                    <div class="box-body">
                        <img src="{{Gravatar::get(auth()->guard('wda')->user()->email)}}" class="user-image" alt="User Profile">
                    </div>
                    <div class="box-footer">

                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
            <div class="col-md-8">
                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Information</h3>
                    </div>
                    <div class="box-body">
                        <label>NAMES: </label> {{auth()->guard('wda')->user()->name}} <br/>
                        <label>E-mail: </label> {{auth()->guard('wda')->user()->email}}<br/>
                        <label>Created: </label> {{auth()->guard('wda')->user()->created_at->diffForHumans()}}

                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@show