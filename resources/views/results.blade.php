<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">


</head>
<body>
<nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144"
     style="background-color: rgb(255, 255, 255);">
    <div class="container">
        <div class="row bg-white p-2" style="width: inherit !important;">
            <div class="col-md-10">
                <div class="navbar-toggleable-sm">
                    <span class="navbar-logo"><a href="/"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                    <span><a class="navbar-brand" href="/">{{ config('app.name') }}</a></span>
                    <!-- Example single danger button -->
                    <a class="btn btn-sm btn-primary shadow-sm" href="{{ route('home.dashboard') }}">
                        <span>Dashboard</span> </a>
                    <a class="btn btn-sm btn-dark shadow-sm"
                       href="{{ route('curricula.index') }}">
                        <span>Curriculum</span>
                    </a>

                    <a class="btn btn-sm btn-info shadow-sm"
                       href="{{ route('home.schools') }}">
                        <span>Schools</span>
                    </a>

                    <span class="dropdown">
                        <button class="btn btn-sm btn-warning shadow-sm btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Support
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               target="_blank"
                               href="{{ url('https://solutions.rp.ac.rw') }}">Help Desk</a>
                            <a class="dropdown-item"
                               href="{{ route('contact.us') }}">Contact Us</a>
                        </div>
                    </span>

                    <a class="btn btn-sm btn-success shadow-sm" href="{{ route('home.attachments') }}">
                        <span>Downloads</span>
                    </a>

                    <a class="btn btn-sm btn-success shadow-sm" href="{{ url('https://gis.rp.ac.rw/') }}">
                        <span>GIS</span>
                    </a>

                    <a class="btn btn-sm btn-primary shadow-sm" href="{{ url('results') }}">
                        <span>Senior Six Results</span>
                    </a>

                </div>
            </div>
        </div>
    </div>
</nav>
<section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
         style="background-image: url({{ asset('img/wlc_bg_n_4.jpg') }});background-attachment: fixed;">
    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="mbr-section-title display-5 text-dark bg-white shadow-md rounded p-2 pt-0">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10 col-sm-9">
                                        Check Senior Six Results
                                    </div>
                                    <div class="col-md-2 col-sm-3">
                                        <button type="button" class="btn btn-outline-secondary btn-block" id="print">Print</button>
                                    </div>
                                </div>


                            </div>
                            <div class="card-body">
                                {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => "results.view"]) !!}
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            {!! Form::label('Index Number') !!}
                                            {!! Form::text('index_number', request('index_number'), ['class' => 'form-control', 'required' => true]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">&nbsp;</label>
                                        {!! Form::submit('Verify', ['class' => 'btn btn-sm btn-primary btn-block']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                @if(isset($results))
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-hover" id="resultsTable">
                                            <thead style="display: none">
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Year of Exam</td>
                                                    <td>: {{ $results->exam_year  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Family Name</td>
                                                    <td>: {{ $results->family_name  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Other Names</td>
                                                    <td>: {{ $results->other_names  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender</td>
                                                    <td>: {{ $results->SEX  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Index Number</td>
                                                    <td>: {{ $results->index  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>School Name</td>
                                                    <td>: {{ $results->school_name  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Obtained Marks</td>
                                                    <td>: {{ $results->TOT  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Mention</td>
                                                    <td>: {{ $results->MENTION  }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script>
    $(document).ready( function () {
        var table = $("#resultsTable").DataTable({
            paging: false,
            searching: false,
            info: false,
            ordering: false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    className: 'fade',
                    text: 'Print in PDF',
                    filename: 'Senior Six Results',
                    title: 'Senior Six Results',
                    customize: function(doc) {
                        doc.styles.tableBodyEven = {
                            background: '#fff',
                        };

                        doc.styles.tableBodyOdd = {
                            background: '#fff',
                        };

                        doc.content[1].table.widths = [ '30%', '50%'];

                        var objLayout = {};
                        objLayout['paddingTop'] = function(i) { return 6; };
                        objLayout['paddingBottom'] = function(i) { return 6; };
                        objLayout['paddingLeft'] = function(i) { return 6; };
                        objLayout['paddingRight'] = function(i) { return 6; };
                        doc.content[1].layout = objLayout;
                    }
                }
            ]
        });

        $("#print").click(function() {
            table.button(1-1).trigger();
        })
    } );
</script>
</body>
</html>