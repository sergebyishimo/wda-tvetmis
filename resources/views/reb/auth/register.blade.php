@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Register
@endsection

@section('content')
    <body class="hold-transition login-page" style="background-image: url({{ asset('img/bg_wood.png') }}); background-attachment: fixed;">
    <div id="app" v-cloak>
        <div class="register-box" style="margin-top: 10px;">
            <div class="row">
                <div class="col-md-6 col-md-offset-4 img-responsive">
                    <img src="{{ asset('img/Coat_of_arms_of_Rwanda.png') }}" alt="{{ config('app.name') }}"
                         class="img-responsive" width="100px">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center" style="color: #37752f;">
                        <b>TVET Information System</b>
                    </h1>
                </div>
            </div>
            <div class="login-logo">
                <div class="row">
                    <div class="col-md-12" style="font-size: 20px">
                        <a href="{{ url('/school/login') }}"><b>REB</b>&nbsp;registration</a>
                    </div>
                </div>
            </div><!-- /.login-logo -->
            <div class="row">
                <div class="col-md-12 alert alert-warning">
                    <p>
                        <b>
                            Applicants who teach in REB schools can Click on the button Create Account to create An
                            Account. You will be asked to verify that your email is correct and then you can proceed to
                            login and complete the forms to submit an application. Please fill in all the required
                            information.
                        </b>
                    </p>
                </div>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="register-box-body">
                <p class="login-box-msg">Register a new account</p>
                <form action="{{ url('/reb/register') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control"
                               placeholder="{{ trans('adminlte_lang::message.fullname') }}" name="name"
                               value="{{ old('name') }}" autofocus/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    @if (config('auth.providers.users.field','email') === 'username')
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control"
                                   placeholder="{{ trans('adminlte_lang::message.username') }}" name="username"
                                   autofocus/>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                    @endif

                    <div class="form-group has-feedback">
                        <input type="email" class="form-control"
                               placeholder="{{ trans('adminlte_lang::message.email') }}" name="email"
                               value="{{ old('email') }}"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control"
                               placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password Confirmation"
                               name="password_confirmation"/>
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button type="submit"
                                    class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.register') }}</button>
                        </div><!-- /.col -->
                    </div>
                </form>
                <a href="{{ url('/reb/login') }}" class="text-center">I already have an account</a>
            </div><!-- /.form-box -->
        </div><!-- /.register-box -->
    </div>

    @include('adminlte::layouts.partials.scripts_auth')

    @include('adminlte::auth.terms')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"
            integrity="sha256-8HGN1EdmKWVH4hU3Zr3FbTHoqsUcfteLZJnVmqD/rC8=" crossorigin="anonymous"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

    </body>

@endsection