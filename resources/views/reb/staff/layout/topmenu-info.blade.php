<div class="list-group list-group-horizontal">
    <a href="{{ route('reb.home') }}"
       class="list-group-item {{ activeMenu('reb.home') }}">My Information</a>
    <a href="{{ route('reb.staff.get.experience') }}"
       class="list-group-item {{ activeMenu('reb.staff.get.experience') }}">Work Experience</a>
    <a href="{{ route('reb.staff.get.teaching') }}"
       class="list-group-item {{ activeMenu('reb.staff.get.teaching') }}">Teaching Experience</a>
    <a href="{{ route('reb.staff.background.marking') }}"
       class="list-group-item {{ activeMenu('reb.staff.background.marking') }}">Marking Background</a>
    <a href="{{ route('reb.staff.get.attachment') }}"
       class="list-group-item {{ activeMenu('reb.staff.get.attachment') }}">Attachment</a>
</div>