@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Unregistered Students 2018 - 2019
    </div>
@endsection

@section('l-style')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        @if(auth()->guard('rp')->check())
                            <form method="GET" action="{{ route('rp.reg.not-finished')  }}">
                                @else
                                    <form method="GET" action="{{ route('college.reg.not-finished')  }}">
                                        @endif
                                <div class="row">
                                    @if (auth()->guard('rp')->check())
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">College</label>
                                                <select name="college" id="college" class="form-control">
                                                    <option value="">All Colleges</option>
                                                    @foreach ($colleges as $college)
                                                        <option value="{{ $college->id }}" @if(request('college') == $college->id) selected @endif>{{ $college->short_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Year Of Study</label>
                                            <select name="year" id="year" class="form-control" required>
                                                <option value="4">All Years</option>
                                                <option value="1" @if(request('year') == 1) selected @endif>First Year</option>
                                                <option value="2" @if(request('year') == 2) selected @endif>Second Year</option>
                                                <option value="3" @if(request('year') == 3) selected @endif>Third Year</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Program</label>
                                            @if(auth()->guard('college')->check())
                                                <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                            @endif
                                            <select name="program_id" id="nProgram" class="form-control">
                                                <option value="">Choose Program</option>
                                                <option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>
                                                <option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Department</label>
                                            <select name="department_id" id="nDepartment" class="form-control">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Option</label>
                                            <select name="option_id" id="nOption" class="form-control">
                                                @if (isset($options))
                                                    <option value="">Choose Option</option>
                                                    @foreach ($options as $option)
                                                        <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-1">
                                        <br>
                                        <button type="submit" class="btn btn-md btn-primary pull-left">
                                            View
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <div class="box-body" style="overflow-x: auto;">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="dataTableListingP">
                            <thead>
                            <tr>
                                <th width="30px" class="text-center">#</th>
                                <th width="100px" class="text-center">Photo</th>
                                <th>Student Reg Number</th>
                                <th>Student Name</th>
                                <th>College</th>
                                <th>Department</th>
                                <th>Course</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($students as $student)
                                <tr>
                                    <td class="text-center">{{ $i++ }}</td>
                                    <td align="center">
                                        <img src="{{ regGetStudentPhoto($student) }}" alt="{{ $student->names }}"
                                             style="width: 5em;height: 5em;box-shadow: 0px 0px 10px #8d9499"
                                             class="img-circle img-responsive">
                                    </td>
                                    <td>{{ $student->std_id }}</td>
                                    <td>{{ $student->names }}</td>
                                    <td>{{ $student->college ? $student->college->college->short_name : '' }}</td>
                                    <td>{{ getDepartment($student->department_id,'department_name',isContinuingStudent($student->std_id)) }}</td>
                                    <td>{{ getCourse($student->course_id, 'program_name', isContinuingStudent($student->std_id)) }}</td>
                                    <td>
                                        @if(auth()->guard('college')->check())
                                            <a href="{{ route('college.registration.show', strtolower($student->std_id)) }}"
                                               class="btn btn-primary btn-sm pull-right">See More</a>
                                        @else
                                            @if (rpAllowed(2))
                                                <a href="{{ route('rp.registration.show', strtolower($student->std_id)) }}"
                                                   class="btn btn-primary btn-sm pull-right">See More</a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th width="30px" class="text-center">#</th>
                                <th width="100px" class="text-center">Photo</th>
                                <th>Student Reg Number</th>
                                <th>Student Name</th>
                                <th>College</th>
                                <th>Department</th>
                                <th>Course</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTableListingP').DataTable({
                dom: 'Blfrtip',
                "oSearch": {"bSmart": false},
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore'],
                        exportOptions: {

                            modifier: {
                                page: 'all',
                                search: 'none'
                            },
                        },
                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
                        },
                    },
                    'copy', 'csv', 'pdf', 'print'
                ],
                "columnDefs" : [
                    {
                        "targets" : [2],
                        "visible" : false

                    }
                ],
                "lengthMenu": [ [10, 50, 100, 200, -1], [10, 50, 100, 200, "All"] ],
                pager: true,
                initComplete: function () {
                    var x = 0;
                    var lenC = this.api().columns().length;
                    this.api().columns().every(function () {
                        if (x > 1 ){
                            var column = this;
                            var select = $('<select class="select2"><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        x++;
                    });
                },
            });
        });
    </script>
    @if (auth()->guard('rp')->check())
    <script>
        $(function() {

            $("#college").change(function() {
                $("#year").val('4');
                $("#nProgram").val('');
                $("#nDepartment").html('');
                $("#nOption").html('');
            });

            $("#year").change(function() {
                if($("#year").val() === '1') {
                    var id = $("#college").val();
                    getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
                } else {
                    $("#nDepartment").html('');
                }
            });


            $("#nProgram").change(function() {
                if ($("#year").val() !== '1') {
                    var program = $(this).val();
                    var college = $("#college").val();
                    $("#nDepartment").html('');
                    $.get('/rp/data/departments/' + program + '/' + college, function(data,status) {
                        $("#nDepartment").html('<option value="">Choose Department</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                        }
                    });
                }
            });

            $("#nDepartment").change(function() {
                if ($("#year").val() === '1') {
                    var id = $(this).val();
                    var cl = $("#college").val();
                    getingOPtions(cl, "/poly/c/"+id+"/", $("#nOption"), 'c');
                } else {
                    var program = $(this).val();
                    var college = $("#college").val();
                    $("#nOption").html('');
                    $.get('/rp/data/options/' + program + '/' + college, function(data,status) {
                        $("#nOption").html('<option value="">Choose Option</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                        }
                    });
                }

            });

            function getingOPtions(id, url, elem, val, cl) {
                $.get(url + id, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select ...</option>";
                    $.each(obj, function (key, value) {
                        var v = "";
                        var a = "";
                        if (val == 'd')
                            v = value.department_name;
                        if (val == 'c') {
                            v = value.program_name;
                            var cd = value.choice_code;
                            var aa = value.study_area;
                            if (cd != undefined){
                                if (cd.length > 0)
                                    a = " ( " + cd + " ) ";
                            }
                        }
                        if (val == "m")
                            v = value.course_unit_name;
                        html += "<option value='" + value.id + "'>" + v + a + "</option>";
                    });
                    elem.html(html);
                });
            }
        });
    </script>
    @else
        <script>
            $(function() {

                $("#year").change(function() {
                    if($("#year").val() === '1') {
                        var id = $("#aCollege").val();
                        getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
                    } else {
                        $("#nDepartment").html('');
                    }
                });


                $("#nProgram").change(function() {
                    if ($("#year").val() !== '1') {
                        var program = $(this).val();
                        $("#nDepartment").html('');
                        $.get('/college/data/departments/' + program, function(data,status) {
                            $("#nDepartment").html('<option value="">Choose Department</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                            }
                        });
                    }
                });

                $("#nDepartment").change(function() {
                    if ($("#year").val() === '1') {
                        var id = $(this).val();
                        var cl = $("#aCollege").val();
                        getingOPtions(cl, "/poly/c/"+id+"/", $("#nOption"), 'c');
                    } else {
                        var program = $(this).val();
                        $("#nOption").html('');
                        $.get('/college/data/options/' + program, function(data,status) {
                            $("#nOption").html('<option value="">Choose Option</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                            }
                        });
                    }

                });

                function getingOPtions(id, url, elem, val, cl) {
                    $.get(url + id, {}, function (data) {
                        var obj = jQuery.parseJSON(data);
                        var html = "<option value='' selected disabled>Select ...</option>";
                        $.each(obj, function (key, value) {
                            var v = "";
                            var a = "";
                            if (val == 'd')
                                v = value.department_name;
                            if (val == 'c') {
                                v = value.program_name;
                                var cd = value.choice_code;
                                var aa = value.study_area;
                                if (cd != undefined){
                                    if (cd.length > 0)
                                        a = " ( " + cd + " ) ";
                                }
                            }
                            if (val == "m")
                                v = value.course_unit_name;
                            html += "<option value='" + value.id + "'>" + v + a + "</option>";
                        });
                        elem.html(html);
                    });
                }
            });
        </script>
    @endif
@show