@extends('college.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('panel-title')
    <span>Uploads Continuing Students</span>
@endsection

@section('panel-body')
    <div class="box">
        @php
            $schoolrequest = null;
        @endphp
        @if($schoolrequest)
            @php($route = ['school.requests.update', $schoolrequest->id])
            @php($id = "updateSchoolRequests")
        @else
            @php($route = 'college.reg.uploads.continuing.student')
            @php($id = "createSchoolRequests")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
        @if($schoolrequest)
            @method("PATCH")
            {!! Form::hidden('id', $schoolrequest->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <a href="{{ asset('files/Continuing Students Format.xlsx') }}">Download Format</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Program</label>
                        <select name="option_id" id="nProgram" class="form-control" required>
                            <option value="">Choose Program</option>
                            <option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>
                            <option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                        </select>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Department</label>
                        <select name="option_id" id="nDepartment" class="form-control" required>
                            {{-- <option value="">Choose Option</option>
                            @foreach ($options as $option)
                                <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                            @endforeach --}}
                        </select>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Option</label>
                        <select name="option_id" id="nOption" class="form-control" required>
                            @if (isset($options))
                                <option value="">Choose Option</option>
                                @foreach ($options as $option)
                                    <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                @endforeach    
                            @endif
                        </select>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Year Of Study</label>
                        <select name="year" id="" class="form-control" required>
                            <option value="1" @if(isset($module) &&$module->year_of_study == 1) selected @endif >First Year</option>
                            <option value="2" @if(isset($module) &&$module->year_of_study == 2) selected @endif >Second Year</option>
                            <option value="3" @if(isset($module) &&$module->year_of_study == 3) selected @endif >Third Year</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('academic_year', "Academic Year", ['class' => 'control-label']) !!}
                        {!! Form::select('academic_year', academicYear(), null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4 pull-right">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('attachment', "Attachment") !!}
                            {!! Form::file('attachment', ['class' => 'filer_docs_input form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Registration Number</label>
                    <input type="text" class="form-control" name="reg_number">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">First Name</label>
                    <input type="text" class="form-control" name="first_name">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Last Name</label>
                    <input type="text" class="form-control" name="last_name">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Sponsor</label>
                    <select id="" class="form-control" name="sponsor">
                        @php($sponsors = \App\SourceSponsor::all())
                        @foreach ($sponsors as $sponsor)
                            <option>{{ $sponsor->sponsor }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <br clear="left">
                    <label for="repeat">Repeat</label>
                    <input type="checkbox" id="repeat" name="repeat">
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $schoolrequest ? "Update Request" : "Uploads Students" }}
                    </button>
                    <a href="{{ route('school.requests.index') }}"
                       class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        <br>
        <div class="box">
            {!! Form::open(['route' => 'college.reg.uploads.references', 'files' => true, 'method' => 'post', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
            <div class="box-header">
                <h4>Upload Students' Reference Numbers : <a href="/files/Reference Numbers.xlsx">Download Format Here</a></h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Choose File</label>
                            <input type="file" name="uploadedFile">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <br>
                        <div class="form-group">
                            <button class="btn btn-primary">Upload</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
    
    @if($show == true)
    <div class="box">
        <div class="box-body">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="text-bold">STUDENTS LIST</a> @if($show = true) <a
                                href="{{ route('college.reg.uploads.uploaded') }}"
                                class="btn btn-md btn-success pull-right"><span class="fa fa-check-circle"
                                                                                style="margin-right: 5px;"></span>finish</a> @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($students))
                        <table class="table table-responsive">
                            <tr>
                                <th>Student Names</th>
                                <th>Program Name</th>
                                <th>Old Reg Number</th>
                                <th>New Reg Number</th>
                                <th>Sponsor</th>
                                <th>Year Of Study</th>
                                <th>Delete</th>
                            </tr>
                            @foreach($students as $student)
                                <tr>
                                    <td>{{ $student->first_name. " " . $student->last_name }}</td>
                                    <td>{{ $student->program->program_name or "" }}</td>
                                    <td>{{ $student->registration_number }}</td>
                                    <td>{{ $student->student_reg }}</td>
                                    <td>{{ strtoupper($student->sponsor_id ) }}</td>
                                    <td>{{ $student->year_of_study }}</td>
                                    <td>
                                        <form action="{{route('college.reg.delete',['id'=>$student->id])}}"
                                              method="post">
                                            @method("DELETE")
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger"><span class="fa fa-warning"
                                                                                               style="margin-right: 5px;"></span>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $students->links()}}
                    @endif


                </div>
            </div>
        </div>
    </div>
    @endif
    @include('school.requests.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($schoolrequest)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateSchoolRequestsRequest', '#updateSchoRequests'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateSchoolRequestsRequest', '#createSchoRequests'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: docx, xlsx,pdf or ppt");
        });
    </script>
    <script>

        $(function() {
            $("#nProgram").change(function() {
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program, function(data,status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                    }
                });
            });
        
            $("#nDepartment").change(function() {
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program, function(data,status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                    }
                });
            });
        });
        
    </script>
@endsection