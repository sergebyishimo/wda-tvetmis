@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Register New Students
    </div>
@endsection

@section('head_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          integrity="sha256-nbyata2PJRjImhByQzik2ot6gSHSU4Cqdz5bNYL2zcU=" crossorigin="anonymous"/>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box">
                {!! Form::open(['class' => 'form-horizontal', 'id' => 'register', 'method' => 'POST']) !!}
                {{--<form class="form-horizontal" id="register" role="form" method="POST"--}}
                {{--action="{{ url('/student/register') }}">--}}
                <div class="box-body">
                    @include('flash_msg.flash-message')
                    @php($reg=privateGenerateReg())
                    <div class="form-group">
                        <label for="std_id" class="col-md-4 control-label">Registration Number</label>
                        <div class="col-md-6">
                            {!! Form::text('std_id', $reg, ['class' => 'form-control', 'readonly' => true]) !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Full Names</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                   autofocus required>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="text" class="form-control" name="password" value="{{ $reg }}"
                                   required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="department" class="col-md-4 control-label">Department</label>
                    <div class="col-md-6">
                        {!! Form::select('department', selectMyDepartment(),null, ['class' => 'form-control select2 department', 'placeholder' => 'Department', 'required' => true]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="std_id" class="col-md-4 control-label">Course</label>
                    <div class="col-md-6">
                        {!! Form::select('course', [],null , ['class' => 'form-control select2 course', 'required' => true]) !!}
                    </div>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Register
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2").select2();
            $(".department").on("change", function () {
                let d = $(this).val();

                $.get("/college/department/" + d, {}, function (data) {
                    let obj = $.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    console.log(data);
                    $.each(JSON.parse(obj), function (key, value) {
                        html += "<option value='" + value.id + "'>" + value.choices + "</option>";
                    });
                    $(".course").html(html);
                });
            });
        });
    </script>
@show