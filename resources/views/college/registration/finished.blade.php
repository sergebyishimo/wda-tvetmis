@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        List Of Student Who Finished Registration
    </div>
@endsection

@section('head_css')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th width="30px" class="text-center">#</th>
                                <th width="100px" class="text-center">Photo</th>
                                <th width="160px">Student Reg Number</th>
                                <th>Student Name</th>
                                <th>Department</th>
                                <th>Course</th>
                                <th>Done Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($students as $student)
                                <tr>
                                    <td class="text-center">{{ $i++ }}</td>
                                    <td align="center">
                                        <img src="{{ regGetStudentPhoto($student) }}" alt="{{ $student->names }}"
                                             style="width: 5em;height: 5em;box-shadow: 0px 0px 10px #8d9499"
                                             class="img-circle img-responsive">
                                    </td>
                                    <td>{{ $student->student_reg }}</td>
                                    <td>{{ $student->names }}</td>
                                    <td>{{  getDepartment($student->department_id, 'department_name', isContinuingStudent($student->student_reg)) }}</td>
                                    <td>{{ getCourse($student->course_id, 'program_name', isContinuingStudent($student->student_reg)) }}</td>
                                    <td>{{ $student->created_at->diffForHumans() }}</td>
                                    <td>
                                        @if(auth()->guard('college')->check())
                                            <a href="{{ route('college.registration.show', strtolower($student->student_reg)) }}"
                                               class="btn btn-primary btn-sm pull-right">See More</a>
                                        @elseif(auth()->guard('rp')->check())
                                            <a href="{{ route('rp.registration.show', strtolower($student->student_reg)) }}"
                                               class="btn btn-primary btn-sm pull-right">See More</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show