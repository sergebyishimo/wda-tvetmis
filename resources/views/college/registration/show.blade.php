@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Show More
    </div>
@endsection

@section('head_css')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8">
                @if(auth()->guard('rp')->check() || auth()->guard('college')->check())
                    @isset($request)
                        @if($request)
                            @if($re == 'out')
                                <div class="box box-warning">
                                    <div class="box-header">
                                        <h6 class="box-title">Transfer Out Request Details</h6>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th style="vertical-align: middle;">Requested College</th>
                                                        <th style="vertical-align: middle;">Requested Department</th>
                                                        <th style="vertical-align: middle;">Requested Course</th>
                                                        <th style="vertical-align: middle;">Current Department</th>
                                                        <th style="vertical-align: middle;">Current Course</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{!! $request->college !!}</td>
                                                        <td>{!! $request->department->department_name !!}</td>
                                                        <td>{!! $request->course->program_name !!}</td>
                                                        <td>{!! getDepartment($request->from_department_id,'department_name') !!}</td>
                                                        <td>{!! getCourse($request->from_course_id, 'program_name') !!}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <a href="{{ asset('storage/'.$request->letter) }}" target="_blank"
                                                   class="btn btn-md btn-primary">Read Requesting Letter</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::open(['route' => 'college.student.decision.transfer']) !!}
                                                {!! Form::hidden('decision', 'a') !!}
                                                {!! Form::hidden('request', $re) !!}
                                                {!! Form::hidden('student', $request->student_id) !!}
                                                <button type="submit" class="btn btn-sm btn-success pull-left">Accept
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::open(['route' => 'college.student.decision.transfer']) !!}
                                                {!! Form::hidden('decision', 'r') !!}
                                                {!! Form::hidden('request', $re) !!}
                                                {!! Form::hidden('student', $request->student_id) !!}
                                                <button type="submit" class="btn btn-sm btn-danger pull-right">Reject
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif($re == 'in')
                                <div class="box box-warning">
                                    <div class="box-header">
                                        <h6 class="box-title">Transfer In Request Details</h6>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th style="vertical-align: middle;">Requested Department</th>
                                                        <th style="vertical-align: middle;">Requested Course</th>
                                                        <th style="vertical-align: middle;">From College</th>
                                                        <th style="vertical-align: middle;">From Department</th>
                                                        <th style="vertical-align: middle;">From Course</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{!! $request->department->department_name !!}</td>
                                                        <td>{!! $request->course->program_name !!}</td>
                                                        <td>{!! getCollege($request->from_college_id, 'polytechnic') !!}</td>
                                                        <td>{!! getDepartment($request->from_department_id, 'department_name') !!}</td>
                                                        <td>{!! getCourse($request->from_course_id, 'program_name') !!}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <a href="{{ asset('storage/'.$request->letter) }}" target="_blank"
                                                   class="btn btn-md btn-primary">Read Requesting Letter</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::open(['route' => 'college.student.decision.transfer']) !!}
                                                {!! Form::hidden('decision', 'a') !!}
                                                {!! Form::hidden('request', $re) !!}
                                                {!! Form::hidden('student', $request->student_id) !!}
                                                <button type="submit" class="btn btn-sm btn-success pull-left">Accept
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::open(['route' => 'college.student.decision.transfer']) !!}
                                                {!! Form::hidden('decision', 'r') !!}
                                                {!! Form::hidden('request', $re) !!}
                                                {!! Form::hidden('student', $request->student_id) !!}
                                                <button type="submit" class="btn btn-sm btn-danger pull-right">Reject
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endif
                    @endif
                @endisset
            @endif
            <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <tr>
                                        <td>Names</td>
                                        <td>
                                            <b>{{ ucwords($student->names) }}</b>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>E-Mail</td>
                                        <td><b>{{ $student->email }}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Telephone:</td>
                                        <td><b>{{ ucwords(validSMSNumber($student->phone, true)) }}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Gender:</td>
                                        <td><b>{{ ucwords($student->gender) }}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Ubudehe:</td>
                                        <td><b>{{ ucwords(get_from('ubudehe', $student->ubudehe)) }}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>DOB:</td>
                                        <td><b>{{ ucwords(get_from('dob', $student->dob)) }}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Want Student Loan:</td>
                                        <td>
                                            <b>{{ ucwords(get_from('want_student_loan', $student->want_student_loan)) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Parents Phone:</td>
                                        <td><b>{{ ucwords(get_from('parents_phone', $student->parents_phone)) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>National ID:</td>
                                        <td>
                                            <b>{{ ucwords(get_from('national_id_number', $student->national_id_number)) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Province:</td>
                                        <td><b>{{ ucwords(get_from('province', $student->province)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>District:</td>
                                        <td><b>{{ ucwords(get_from('district', $student->district)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Sector:</td>
                                        <td><b>{{ ucwords(get_from('sector', $student->sector)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Cell:</td>
                                        <td><b>{{ ucwords(get_from('cell', $student->cell)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Village:</td>
                                        <td><b>{{ ucwords(get_from('village', $student->village)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Your Bank:</td>
                                        <td><b>{{ ucwords(get_from('your_bank', $student->your_bank)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Your Bank Account:</td>
                                        <td>
                                            <b>{{ ucwords(get_from('your_bank_account', $student->your_bank_account)) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Disability:</td>
                                        <td><b>{{ ucwords(get_from('disability', $student->disability)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Examiner:</td>
                                        <td><b>{{ ucwords(get_from('examiner', $student->examiner)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Index Number:</td>
                                        <td><b>{{ ucwords(get_from('index_number', $student->index_number)) }}</b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>School Attended:</td>
                                        <td><b>{{ ucwords(get_from('school_attended', $student->school_attended)) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Graduation Year:</td>
                                        <td><b>{{ ucwords(get_from('graduation_year', $student->graduation_year)) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Option Offered:</td>
                                        <td><b>{{ ucwords(get_from('option_offered', $student->option_offered)) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Aggregates Obtained:</td>
                                        <td><b>{{ get_from('aggregates_obtained', $student->aggregates_obtained) }}</b>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    @if(!isAdmitted($student->std_id))
                                        @if(applied($student->std_id, true))
                                            <tr>
                                                <td>Study Mode:</td>
                                                <td><b>{{ $student->student_category }}</b>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Diploma / Certificate:</td>
                                                <td>
                                                    @if(json_decode($student->scan_of_diploma_or_certificate))
                                                        @foreach(json_decode($student->scan_of_diploma_or_certificate) as $file)
                                                            <a href="{{ Storage::disk('public')->url($file->download_link) ? regGetStudentFile($file->download_link) : '' }}"
                                                               target="_blank">
                                                                <b>{{ $file->original_name ?: '' }}</b>
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        @if($student->scan_of_diploma_or_certificate)
                                                            <a href="{{ regGetStudentFile($student->scan_of_diploma_or_certificate) }}"
                                                               target="_blank">
                                                                <b>Download</b>
                                                            </a>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Diploma / Certificate:</td>
                                                <td>
                                                    @if(json_decode($student->scan_national_id_passport))
                                                        @foreach(json_decode($student->scan_national_id_passport) as $file)
                                                            <a href="{{ Storage::disk('public')->url($file->download_link) ? regGetStudentFile($file->download_link) : '' }}"
                                                               target="_blank">
                                                                <b>{{ $file->original_name ?: '' }}</b>
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        @if($student->scan_national_id_passport)
                                                            <a href="{{ regGetStudentFile($student->scan_national_id_passport) }}"
                                                               target="_blank">
                                                                <b>Download</b>
                                                            </a>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endif
                                    @endif
                                </table>
                                @if(!isAdmitted($student->std_id))
                                    @if(applied($student->std_id, true))
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::open(['route' => 'college.privates.store', 'id' => 'myForm']) !!}
                                                {!! Form::hidden('std_id', $student->std_id) !!}
                                                {!! Form::submit('Accept', ['class' => 'btn btn-primary']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::open(['route' => ['college.privates.destroy', $student->std_id]]) !!}
                                                @method('DELETE')
                                                {!! Form::hidden('std_id', $student->std_id) !!}
                                                <textarea name="reason" class="form-control" placeholder="Reject Reason" required></textarea>
                                                <br>
                                                {!! Form::submit('Reject', ['class' => 'btn btn-danger pull-right']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-4">
                <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2" align="center">
                                            <img src="{{ regGetStudentPhoto($student) }}"
                                                 class="img-responsive img-circle"
                                                 style="width: 10em;height: 10em;box-shadow: 0px 0px 10px #8d9499"
                                                 alt="{{ $student->std_id }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Department</td>
                                        <td>
{{--                                            <b>{{ $student->department? $student->department->department_name : "" }}</b>--}}
                                            <b>{{ getDepartment($student->department_id, 'department_name', isContinuingStudent($student->std_id)) }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Course</td>
                                        <td><b>{{ ucwords($student->course? $student->course->program_name : "") }}</b>
                                        </td>
                                    </tr>
                                    @if(!isAdmitted($student->std_id))
                                        @if(applied($student->std_id, true))
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td>Give Department</td>
                                            <td>
                                                <input type="hidden" id="aCollege" value="{{ college('college_id')  }}">
                                                <select name="department" form="myForm" id="aDepartment" class="form-control"></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Give Program</td>
                                            <td>
                                                <select name="program" form="myForm" id="aProgram" class="form-control"></select>
                                            </td>
                                        </tr>
                                        @endif
                                    @endif
                                </table>
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(isAdmitted($student->std_id))
                                            <a href="{{ route('college.admission.letter', $student->std_id) }}"
                                               target="_blank"
                                               class="btn btn-success">
                                                Admission Letter
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script>
        var id = $("#aCollege").val();
        getingOPtions(id, "/poly/d/", $("#aDepartment"), 'd');

        $("#aDepartment").change(function () {
            var id = $(this).val();
            var cl = $("#aCollege").val();
            getingOPtions(cl, "/poly/c/"+id+"/", $("#aProgram"), 'c');
        });(cl, "/poly/c/"+id+"/", $("#aProgram"), 'c');



        function getingOPtions(id, url, elem, val, cl) {
            $.get(url + id, {}, function (data) {
                var obj = jQuery.parseJSON(data);
                var html = "<option value='' selected disabled>Select ...</option>";
                $.each(obj, function (key, value) {
                    var v = "";
                    var a = "";
                    if (val == 'd')
                        v = value.department_name;
                    if (val == 'c') {
                        v = value.program_name;
                        var cd = value.choice_code;
                        var aa = value.study_area;
                        if (cd != undefined){
                            if (cd.length > 0)
                                a = " ( " + cd + " ) ";
                        }
                    }
                    if (val == "m")
                        v = value.course_unit_name;
                    html += "<option value='" + value.id + "'>" + v + a + "</option>";
                });
                elem.html(html);
                console.log(html);
            });
        }
    </script>
@show