@extends('college.layout.main')

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('panel-title')
    <span>Uploaded Students : {{ isset($students) ? count($students) : \App\CollegeContinuingStudent::where('college_id', college('college_id'))->count() }} </span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-body">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="text-bold">STUDENTS LIST</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <form method="POST" action="{{ route('college.reg.uploads.uploaded.save') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Program</label>
                                    <select name="program_id" id="nProgram" class="form-control" required>
                                        <option value="">Choose Program</option>
                                        <option value="1" @if(request()->program_id == 1) selected @endif >
                                            Diploma (Level 6)
                                        </option>
                                        <option value="2" @if(request()->program_id == 2) selected @endif >
                                            Advanced Diploma (Level 7)
                                        </option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Department</label>
                                    <select name="department_id" id="nDepartment" class="form-control" required>
                                        @if (isset($departments))
                                            @foreach ($departments as $department)
                                                <option value="{{ $department->id }}"
                                                        @if( request()->department_id == $department->id) selected @endif >{{ $department->department_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Option</label>
                                    <select name="option_id" id="nOption" class="form-control" required>
                                        @if (isset($options))
                                            <option value="">Choose Option</option>
                                            @foreach ($options as $option)
                                                <option value="{{ $option->id }}"
                                                        @if( request()->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="sponsor" class="control-label">Sponsor</label>
                                    <select class="form-control" id="sponsor" required="" name="sponsor">
                                        <option value="0">All</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Year Of Study</label>
                                    <select name="year" id="" class="form-control" required>
                                        <option value="1" @if(request()->year == 1) selected @endif >
                                            First Year
                                        </option>
                                        <option value="2" @if(request()->year == 2) selected @endif >
                                            Second Year
                                        </option>
                                        <option value="3" @if(request()->year == 3) selected @endif >
                                            Third Year
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('academic_year', "Academic Year", ['class' => 'control-label']) !!}
                                    {!! Form::select('academic_year', academicYear(), request()->academic_year, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Repeat</label>
                                    <select name="repeat" id="" class="form-control" required>
                                        <option value="0" @if(request()->repeat == 0) selected @endif >All</option>
                                        <option value="1" @if(request()->repeat == 1) selected @endif >Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <br>
                                <button type="submit" class="btn btn-primary btn-block">View</button>
                            </div>
                        </div>

                    </form>
                    <div class="box">
                        <div class="box-body" style="overflow-x: scroll;">
                            @if (isset($students))
                                <table class="table table-bordered table-striped table-hover table-responsive"
                                       id="qwerty123">
                                    <thead>
                                    <tr>
                                        <th>Student Names</th>
                                        <th>Option Name</th>
                                        <th>Old Reg Number</th>
                                        <th>New Reg Number</th>
                                        <th>Sponsor</th>
                                        <th>Year Of Study</th>
                                        <th>Uploaded At</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($students as $student)
                                        <tr>
                                            <td>{{ $student->first_name. " " . $student->last_name }}</td>
                                            <td>{{ $student->option->option_name or "" }}</td>
                                            <td>{{ $student->registration_number }}</td>
                                            <td>{{ $student->student_reg }}</td>
                                            <td>{{ strtoupper($student->sponsor_id ) }}</td>
                                            <td>{{ $student->year_of_study }}</td>
                                            <td>{{ $student->created_at}}</td>
                                            <td>
                                                <a href="/college/registration/uploads/uploaded/edit/{{ $student->id }}" class="btn btn-primary"> Edit </a>
                                                @if($student->admissionInfo == null)
                                                    <form action="{{route('college.reg.delete',['id'=>$student->id])}}"
                                                          method="post">
                                                        @method("DELETE")
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger"
                                                                onclick="return window.confirm('Are you sure you want to remove this student: \n {{$student->first_name . ' ' . $student->last_name}} ?')">
                                                            Delete
                                                        </button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            @else
                                <table class="table table-bordered table-striped table-hover table-responsive"
                                       id="qwerty123">
                                    <thead>
                                    <tr>
                                        <th>Student Names</th>
                                        <th>Option Name</th>
                                        <th>Old Reg Number</th>
                                        <th>New Reg Number</th>
                                        <th>Sponsor</th>
                                        <th>Year Of Study</th>
                                        <th>Uploaded At</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    {{-- <tbody>
                                    @if (isset($students))
                                        @foreach($students as $student)
                                            <tr>
                                                <td>{{ $student->first_name. " " . $student->last_name }}</td>
                                                <td>{{ $student->option->option_name or "" }}</td>
                                                <td>{{ $student->registration_number }}</td>
                                                <td>{{ $student->student_reg }}</td>
                                                <td>{{ strtoupper($student->sponsor_id ) }}</td>
                                                <td>{{ $student->year_of_study }}</td>
                                                <td>{{ $student->created_at}}</td>
                                                <td>
                                                    @if($student->admissionInfo == null)
                                                        <form action="{{route('college.reg.delete',['id'=>$student->id])}}"
                                                            method="post">
                                                            @method("DELETE")
                                                            {{ csrf_field() }}
                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    @else
                                                        Already Registered
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif --}}

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Student Names</th>
                                        <th>Program Name</th>
                                        <th>Old Reg Number</th>
                                        <th>New Reg Number</th>
                                        <th>Sponsor</th>
                                        <th>Year Of Study</th>
                                        <th>Uploaded At</th>
                                        <th>Delete</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('school.requests.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{-- <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script> --}}

    {{-- <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script> --}}
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    @if (!isset($students))
        <script>
            $('#qwerty123').DataTable({
                dom: 'Blfrtip',
                ordering: true,
                retrieve: true,
                serverSide: true,
                processing: true,
                searching: true,
                pager: true,
                colReorder: true,
                // "columnDefs": [
                //     { "width": "200px", "targets": 0 },
                //     { "width": "90px", "targets": 8 }
                // ],
                ajax: '/college/datatable/object-data/uploadedStudents',
                "oSearch": {"bSmart": false},
                columns: [
                    {data: 'student_names', name: 'first_name'},
                    {data: 'option_name', name: 'college_options.option_name', searchable: false, orderable: false},
                    {data: 'old_reg_nber', name: 'registration_number'},
                    {data: 'new_reg_nber', name: 'student_reg'},
                    {data: 'sponsor', name: 'sponsor_id'},
                    {data: 'year_of_study', name: 'year_of_study', orderable: true},
                    {data: 'uploaded_at', name: 'created_at'},
                    {data: 'action', orderable: false, searchable: false}
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select class="select2"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });

        </script>
    @else
        <script>
            $('#qwerty123').DataTable({
                dom: 'Blfrtip',
                ordering: false,
                retrieve: true,
                // serverSide: true,
                // processing: true,
                searching: true,
                pager: true,
                colReorder: true,
                responsive: true,
                "scrollX": true,
                // "columnDefs": [
                //     { "width": "200px", "targets": 0 },
                //     { "width": "90px", "targets": 8 }
                // ],

                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select class="select2"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });
        </script>
    @endif

    <script>
        $(function () {


            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: xlsx, ppt, or pdf");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
    <script>

        $(function () {
            $("#nProgram").change(function () {
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program, function (data, status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value=' + data[i].id + '>' + data[i].department_name + '</option>');
                    }
                });
            });

            $("#nDepartment").change(function () {
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program, function (data, status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nOption").append('<option value=' + data[i].id + '>' + data[i].option_name + '</option>');
                    }
                });
            });
        });

    </script>
@show