@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Edit Student : {{ $student->first_name . ' ' . $student->other_names }}
    </div>
@endsection

@section('head_css')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Student Information</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>

                        <div class="row">
                            @if(auth()->guard('college')->check())
                                <form method="POST" action="{{ route('college.registration.edit.save') }}">
                            @endif
                            @if(auth()->guard('rp')->check())
                                <form method="POST" action="{{ route('rp.registration.edit.save') }}">
                            @endif
                                @csrf
                                <input type="hidden" name="student_id" value="{{ $student->std_id }}">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Reg. Nber</label>
                                        <input type="text" class="form-control flat" readonly name="school_code"
                                            placeholder="REg. Nber" value="{{ $student->std_id }}">
                                    </div>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control flat" name="first_name"
                                            placeholder="First Name" value="{{ $student->first_name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Other Names</label>
                                        <input type="text" class="form-control flat" name="other_names"
                                            placeholder="Other Names" value="{{ $student->other_names }}">
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input type="text" class="form-control flat" name="email"
                                            placeholder="E-mail" value="{{ $student->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control flat" name="phone"
                                            placeholder="Phone" value="{{ $student->phone }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="gender" class="form-control">
                                            <option value="male" @if($student->gender == 'male') selected @endif>Male</option>
                                            <option value="female" @if($student->gender == 'female') selected @endif>Female</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Ubudehe</label>
                                        <select name="ubudehe" class="form-control">
                                            @foreach ($ubudehes as $ubudehe)
                                                <option value="{{ $ubudehe->id }}" @if($student->ubudehe == $ubudehe->id ) selected @endif >{{ $ubudehe->ubudehe }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Year Of Study </label>
                                        <select name="year_of_study" class="form-control" required>
                                            <option value="1" @if($student->year_of_study  == 1 || $student->year_of_study  == 2018) selected @endif>First Year</option>
                                            <option value="2" @if($student->year_of_study  == 2 || $student->year_of_study  == 17) selected @endif>Second Year</option>
                                            <option value="3" @if($student->year_of_study  == 3 || $student->year_of_study  == 16) selected @endif>Third Year</option>
                                        </select>
                                    </div>
                                    @if(auth()->guard('college')->check())
                                        <input type="hidden" id="aCollege" value="{{ getStudentInfo($student->std_id, 'college_id', true)  }}">
                                    @endif

                                    @if(isContinuingStudent($student->std_id))
                                        <div class="form-group">
                                            <label for="">Program</label>
                                            <select name="option_id" id="nProgram" class="form-control" required>
                                                <option value="">Choose Program</option>
                                                <option value="1" @if(getContinuingProgram($student->std_id) == 1) selected @endif >Diploma (Level 6)</option>
                                                <option value="2" @if(getContinuingProgram($student->std_id) == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                            </select>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="">Department</label>
                                        @if(isContinuingStudent($student->std_id))
                                            @php($departments = \App\Rp\CollegeDepartment::where('college_id', getStudentInfo($student->std_id, 'college_id', true))->get())
                                            <select name="department_id" id="nDepartment" class="form-control" required>
                                                <option value="">Choose Department</option>
                                                @foreach ($departments as $dep)
                                                    <option value="{{$dep->id}}" @if($dep->id == $student->department_id) selected @endif >{{ $dep->department_name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            @php($departments = \App\Model\Rp\RpDepartment::all())
                                            <select name="department_id" id="aDepartment" class="form-control" required>
                                                @foreach ($departments as $dep)
                                                    <option value="{{$dep->id}}" @if($dep->id == $student->department_id) selected @endif >{{ $dep->department_name }}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Option</label>
                                        @if(isContinuingStudent($student->std_id))
                                            @php($options = \App\Rp\CollegeOption::where('college_id', getStudentInfo($student->std_id, 'college_id', true))->where('department_id', $student->department_id)->get())
                                            <select name="option_id" id="nOption" class="form-control" required>
                                                <option value="">Choose Department</option>
                                                @foreach ($options as $option)
                                                    <option value="{{$option->id}}" @if($option->id == $student->course_id) selected @endif>{{ $option->option_name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            @php($options = \App\Model\Rp\RpProgram::where('department_id', getStudentInfo($student->std_id, 'department_id', true))->get())    
                                            <select name="option_id" id="aProgram" class="form-control">
                                                @foreach ($options as $option)
                                                    <option value="{{$option->id}}" @if($option->id == $student->course_id) selected @endif>{{ $option->program_name }}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Birth Date</label>
                                        <input type="text" class="form-control flat" name="dob"
                                            placeholder="Birth Date" value="{{ date('d/m/Y', strtotime($student->dob)) }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Aggregated Obtained</label>
                                        <input type="text" class="form-control flat" name="aggregate_obtained" placeholder="Birth Date" value="{{$student->aggregates_obtained }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Sponsorship</label>
                                        <select name="sponsor" class="form-control">
                                            <option value="">Choose Here</option>
                                            <option value="government" @if($student->sponsorship_status == 'government') selected @endif >Government</option>
                                            <option value="private" @if($student->sponsorship_status == 'private') selected @endif >Private</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>National ID</label>
                                        <input type="text" class="form-control flat" name="national_id_number" placeholder="National ID" value="{{$student->national_id_number }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Parents Phone</label>
                                        <input type="text" class="form-control flat" name="parents_phone" placeholder="Parents Phone" value="{{$student->parents_phone }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Index Number</label>
                                        <input type="text" class="form-control flat" name="index_number" placeholder="Parents Phone" value="{{$student->index_number }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">Save Information</button>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
    <script>

        $(function() {
            $("#nProgram").change(function() {
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program, function(data,status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                    }
                });
            });
        
            $("#nDepartment").change(function() {
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program, function(data,status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                    }
                });
            });
        });

        $("#aDepartment").change(function () {
            var id = $(this).val();
            var cl = $("#aCollege").val();
            getingOPtions(cl, "/poly/c/"+id+"/", $("#aProgram"), 'c');
        });(cl, "/poly/c/"+id+"/", $("#aProgram"), 'c');

        $("#aProgram").change(function () {
            var id = $(this).val();
            getingOPtions(id, "/poly/m/", $("#aModule"), 'm');
        });

        function getingOPtions(id, url, elem, val, cl) {
            $.get(url + id, {}, function (data) {
                var obj = jQuery.parseJSON(data);
                var html = "<option value='' selected disabled>Select ...</option>";
                $.each(obj, function (key, value) {
                    var v = "";
                    var a = "";
                    if (val == 'd')
                        v = value.department_name;
                    if (val == 'c') {
                        v = value.program_name;
                        var cd = value.choice_code;
                        var aa = value.study_area;
                        if (cd != undefined){
                            if (cd.length > 0)
                                a = " ( " + cd + " ) ";
                        }
                    }
                    if (val == "m")
                        v = value.course_unit_name;
                    html += "<option value='" + value.id + "'>" + v + a + "</option>";
                });
                elem.html(html);
                console.log(html);
            });
        }
    </script>
@show