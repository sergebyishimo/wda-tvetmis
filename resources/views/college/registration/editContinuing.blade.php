@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Edit Student : {{ $student->first_name . ' ' . $student->last_name }}
    </div>
@endsection

@section('head_css')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-6">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Student Information</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('feedback.feedback')
                        </div>
                    </div>

                    <div class="row">
                        <form method="POST" action="/college/registration/uploads/uploaded/edit">
                            @csrf
                            <input type="hidden" name="cont_id" value="{{$student->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Reg. Nber</label>
                                    <input type="text"ready class="form-control flat" name="reg_number" placeholder="REg. Nber" value="{{ $student->registration_number }}" required>
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control flat" name="first_name" placeholder="First Name" value="{{ $student->first_name }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Last Names</label>
                                    <input type="text" class="form-control flat" name="last_name" placeholder="Other Names" value="{{ $student->last_name }}" required>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="">Department</label>
                                        @if(isContinuingStudent($student->id))
                                            <select name="department_id" id="" class="form-control">
                                                @php($departments = \App\Rp\CollegeDepartment::where('college_id', college('college_id'))->get())
                                                @foreach ($departments as $dep)
                                                    <option value="{{$dep->id}}" @if($dep->id == $student->department_id) selected @endif >{{ $dep->department_name }}</option>
                                                @endforeach
                                            </select>
                                        @else

                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Option</label>
                                        @if(isContinuingStudent($student->id))
                                            <select name="option_id" id="" class="form-control">
                                                @php($options = \App\Rp\CollegeOption::where('college_id', college('college_id'))->get())
                                                @foreach ($options as $option)
                                                    <option value="{{$option->id}}" @if($option->id == $student->course_id) selected @endif>{{ $option->option_name }}</option>
                                                @endforeach
                                            </select>
                                        @else

                                        @endif
                                    </div>
                                <div class="form-group">
                                    <label for="">Sponsor</label>
                                    <select id="" class="form-control" name="sponsor">
                                        @php($sponsors = \App\SourceSponsor::all())
                                        @foreach ($sponsors as $sponsor)
                                            <option @if($student->sponsor_id == $sponsor->sponsor) selected @endif>{{ $sponsor->sponsor }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Repeat</label>
                                    <select id="" class="form-control" name="repeat">
                                        <option value="0" @if($student->repeat == 0) selected @endif >No</option>
                                        <option value="1" @if($student->repeat == 1) selected @endif >Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">Save Information</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection