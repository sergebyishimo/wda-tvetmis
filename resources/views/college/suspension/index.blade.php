@extends('college.layout.main')

@section('panel-title', "Suspensions Submitted")

@section('htmlheader_title', "Suspensions Submitted")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Student Names</th>
                                <th>Department</th>
                                <th>Option</th>
                                <th>Letter</th>
                                <th>Resuming Academic Year</th>
                                <th>Resuming Semester</th>
                                <th>Decision</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php( $i = 1 )
                                @forelse($suspensions as $sus)
                                    <tr>
                                        <td>{{ $i++  }}</td>
                                        <td>{{ $sus->student ?  $sus->student->first_name . ' ' . $sus->student->other_names  : '' }}</td>
                                        <td>{{ getDepartment($sus->student_reg, 'department_name', isContinuingStudent($sus->student_reg))  }}</td>
                                        <td>{{ getCourse($sus->student_reg, 'option_name', isContinuingStudent($sus->student_reg))  }}</td>
                                        <td><a href="/storage/{{ $sus->letter  }}" target="_blank">View Letter</a></td>
                                        <td>{{ $sus->resuming_academic_year  }}</td>
                                        <td>{{ $sus->resuming_semester  }}</td>
                                        <td>
                                            <a href="#" class="btn btn-success">Approve</a>
                                            <a href="#" class="btn btn-danger">Deny</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="8" class="text-center">No Suspensions Found.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection