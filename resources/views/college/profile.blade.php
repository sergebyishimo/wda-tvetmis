@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Profile
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Profile</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="post" action="{{ route('rp.update.profile') }}">
                            @csrf()
                            <div class="row">
                                <div class="col-md-12" style="padding: 15px 25px 0px 25px;">
                                    @include('rp.layout.feedback')
                                </div>
                            </div>
                            {!! Form::hidden('id', auth()->guard('college')->user()->id) !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="username" class="form-control" id="inputEmail3"
                                               value="{{ auth()->guard('college')->user()->email }}"
                                               placeholder="Username">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password" class="form-control" id="inputPassword3"
                                               placeholder="Password">
                                        <span class="help-block text-aqua text-sm">
                                            Leave it if no changes.
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword4" class="col-sm-2 control-label">Password Confirm</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="inputPassword4"
                                               name="password_confirmation"
                                               placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info">Update</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection