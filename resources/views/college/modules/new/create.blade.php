@extends('college.layout.main')

@section('panel-title', "Create College Module")

@section('htmlheader_title', "Create College Module")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if(isset($module))
                    @php($route = ['college.modules.update', $module->id])
                    @php($id = "updateProgram")
                @else
                    @php($route = 'college.modules.store')
                    @php($id = "createProgram")
                @endif
                {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                @if(isset($module))
                    @method("PATCH")
                    {!! Form::hidden('id', $module->id) !!}
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ route('college.modules.index') }}"
                                   class="btn btn-warning btn-md pull-right">Go Back</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Year Of Study</label>
                                    <select name="year" id="nYearOfStudy" class="form-control select2" required>
                                        <option value="">Select ...</option>
                                        <option value="1"
                                                @if(isset($module) &&$module->year_of_study == 1) selected @endif >First
                                            Year
                                        </option>
                                        <option value="2"
                                                @if(isset($module) &&$module->year_of_study == 2) selected @endif >
                                            Second Year
                                        </option>
                                        <option value="3"
                                                @if(isset($module) &&$module->year_of_study == 3) selected @endif >Third
                                            Year
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="keypoints">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Program</label>
                                        <select name="option_id" id="nProgram" class="form-control select2"
                                                style="width: 100%;">
                                            <option value="">Choose Program</option>
                                            <option value="1"
                                                    @if(isset($department) && $department->program_id == 1) selected @endif >
                                                Diploma (Level 6)
                                            </option>
                                            <option value="2"
                                                    @if(isset($department) && $department->program_id == 2) selected @endif >
                                                Advanced Diploma (Level 7)
                                            </option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Department</label>
                                        <select name="option_id" id="nDepartment" class="form-control select2"
                                                style="width: 100%;">
                                            {{-- <option value="">Choose Option</option>
                                            @foreach ($options as $option)
                                                <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                            @endforeach --}}
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Option</label>
                                        <select name="option_id" id="nOption" class="form-control select2" required
                                                style="width: 100%;">
                                            @if (isset($options))
                                                <option value="">Choose Option</option>
                                                @foreach ($options as $option)
                                                    <option value="{{ $option->id }}"
                                                            @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="module_row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Semester</label>
                                    <select name="semester[]" id="" class="form-control select2" required>
                                        <option value="">Choose Semester</option>
                                        <option value="1" @if(isset($module) &&$module->semester == 1) selected @endif >
                                            Semester 1
                                        </option>
                                        <option value="2" @if(isset($module) &&$module->semester == 2) selected @endif >
                                            Semester 2
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Module Code</label>
                                    <input type="text" name="module_code[]" class="form-control"
                                           placeholder="Module Code" value="{{ $module->module_code or '' }}" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Module Name</label>
                                    <input type="text" name="module_name[]" class="form-control"
                                           placeholder="Module Name" value="{{ $module->module_name or '' }}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Credits</label>
                                    <input type="text" name="credits[]" class="form-control" placeholder="Credits"
                                           value="{{ $module->credits or '' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Academic Year</label>
                                    <select name="academic_year[]" id="" class="form-control" required>
                                        @php($academic_arr = '')
                                        @for ($i = (date('Y') + 1); $i >= 2015; $i--)
                                            @php($acad = $i - 1 . ' - ' . $i)
                                            <option value="{{ $acad }}">{{ $acad }}</option>
                                            @php($academic_arr .= "<option value='".$acad."'>". $acad ."</option>")
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            @if (!isset($module))
                                <div class="col-md-1">
                                    <br clear="left">
                                    <button type="button" class="btn btn-primary" style="margin-top: 10px"
                                            onclick="addModule()"><i class="fa fa-plus"></i></button>
                                </div>
                            @endif

                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-md btn-primary pull-left">
                            {{ $module ? "Update Module" : "Save Module" }}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    {{-- <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($module)
        {!! JsValidator::formRequest('App\Http\Requests\RpProgramUpdateRequest', '#updateProgram'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\RpProgramRequest', '#createProgram'); !!}
    @endif --}}
    @if(isset($academic_arr))
        <script type="text/javascript"> var academic_arr = "<?= $academic_arr;  ?>"; </script>
    @endif
    <script>
        $(function () {
            $(".keypoints").hide();
            $("#nYearOfStudy").on("change", function () {
                $(".keypoints").fadeIn(555);
                $("*.keypoints select").val("").trigger('change');
            });
            $("#nProgram").change(function () {
                let year = $("#nYearOfStudy").val();
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program + "?y=" + year, function (data, status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value=' + data[i].id + '>' + data[i].department_name + '</option>');
                    }
                });
            });

            $("#nDepartment").change(function () {
                let year = $("#nYearOfStudy").val();
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program + "?y=" + year, function (data, status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    if (year === '1') {
                        let o = $.parseJSON(data);
                        $.each(o, function (i, item) {
                            $("#nOption").append('<option value=' + item.id + '>' + item.program_name + '</option>');
                        });
                    } else {
                        for (i = 0; i < data.length; i++) {
                            $("#nOption").append('<option value=' + data[i].id + '>' + data[i].option_name + '</option>');
                        }
                    }
                });
            });
        });

        function addModule() {
            $("#module_row").append('<div class="col-md-2"> <div class="form-group"> <label for="">Semester</label> <select name="semester[]" id="" class="form-control" required> <option value="">Choose Semester</option> <option value="1">Semester 1</option> <option value="2">Semester 2</option> </select> </div> </div><div class="col-md-2"> <div class="form-group"> <label for="">Module Code</label> <input type="text" name="module_code[]" class="form-control" placeholder="Module Code" value="{{ $module->module_code or '' }}" required> </div> </div> <div class="col-md-3"> <div class="form-group"> <label for="">Module Name</label> <input type="text" name="module_name[]" class="form-control" placeholder="Module Name" value="{{ $module->module_name or '' }}" required> </div> </div> <div class="col-md-2"> <div class="form-group"> <label for="">Credits</label> <input type="text" name="credits[]" class="form-control" placeholder="Credits" value="{{ $module->credits or '' }}"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label for="">Academic Year</label> <select name="academic_year[]" class="form-control" required> ' + academic_arr + ' </select> </div>  </div> <div class="col-md-1"> <br clear="left"> <button type="button" class="btn btn-primary" style="margin-top: 10px" onclick="addModule()"><i class="fa fa-plus"></i></button> </div>');
        }

    </script>
@show