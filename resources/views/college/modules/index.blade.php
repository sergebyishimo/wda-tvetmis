@extends('college.layout.main')

@section('panel-title', "College Modules" . request()->option_id . ' ' . request()->year. ' ' . request()->academic_year . ' ' . request()->semester)

@section('htmlheader_title', "College Modules")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@if(isset($details) && $details == true)
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ route('college.modules.index') }}" class="btn btn-primary btn-sm">back</a>
                        <center><h3 class="box-title text-primary">Details Information of program</h3></center>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <tr>
                                <th class="text-green col-md-4">Program Name</th>
                                <td style="font-weight: bold;" class="col-md-8">{{ $module->program_name }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Code</th>
                                <td>{{ $module->program_code }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Load</th>
                                <td>{{ $module->program_load }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Rtqf Level</th>
                                <td>{{ $module->rtqflevel->rtqf_level  or ""}}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Is It STEM ?</th>
                                <td>{{ $module->program_is_stem }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Status</th>
                                <td>{{ $module->program_status }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Level Applied</th>
                                <td>{{ $module->level_applied }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Tuition Fees</th>
                                <td>{{ $module->tuition_fees }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Other Course Fees</th>
                                <td>{{ $module->other_course_fees }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Admission Requirements</th>
                                <td>{{ $module->admission_requirements }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Attachment</th>
                                <td><a href="{{ asset('storage/'.$module->attachment) }}">View Attachment</a></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <center><h3 class="box-title text-primary">Course Units In <span
                                        class="text-green">{{$module->program_name}}</span></h3></center>
                        <div class="list-group list-group-horizontal pull-right">
                            <a href="#"
                               class="list-group-item active">
                                Create a Course Unit</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <tr>
                                <th>Course Unit Code</th>
                                <th>Course Unit Name</th>
                                <th>Credit Unit</th>
                                <th>Level Of Study</th>
                                <th>Semester Id</th>
                                <th>Unit Class Id</th>
                            </tr>
                            @if ($module->courseunits)
                                @foreach($module->courseunits as $courseunit)
                                    <tr>
                                        <td>
                                            {{ $courseunit->course_unit_code }}
                                        </td>
                                        <td>
                                            <h5>
                                                {{ $courseunit->course_unit_name }}
                                            </h5>
                                        </td>
                                        <td>
                                            {{ $courseunit->credit_unit }}
                                        </td>
                                        <td>
                                            {{ $courseunit->levelofstudy->level_of_study or "" }}
                                        </td>
                                        <td>
                                            {{ $courseunit->semester->semester  or ""}}
                                        </td>
                                        <td>
                                            {{ $courseunit->unitclass->course_unit_class or ""}}
                                        </td>
                                        <td>
                                        </td>
                                        <th>
                                            <a href="{{ route('college.courseunits.edit', $courseunit->id) }}"
                                               class="btn btn-info btn-sm">Edit</a>
                                            <button type="button"
                                                    data-url="{{ route('college.courseunits.destroy', $courseunit->id) }}"
                                                    data-names="{{ $courseunit->id }}"
                                                    class="btn btn-danger btn-sm btn-delete">
                                                Delete
                                            </button>
                                        </th>
                                    </tr>
                                @endforeach
                            @endif

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@else
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    @if (collegeAllowed(4, 'd') || collegeAllowed(4, 'a'))
                        <div class="box-header with-border">
                            <span class="pull-right"><a href="{{ route('college.modules.create') }}"
                                                        class="btn btn-primary btn-block">Add Module</a></span>
                        </div>
                    @endif
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['method' => 'get']) !!}
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Year Of Study</label>
                                            <select name="year" id="year" class="form-control" required>
                                                <option value="4">All Years</option>
                                                <option value="1"
                                                        @if(request('year') == 1 || request('ayear') == 1) selected @endif>
                                                    First Year
                                                </option>
                                                <option value="2"
                                                        @if(request('year') == 2 || request('ayear') == 2) selected @endif>
                                                    Second Year
                                                </option>
                                                <option value="3"
                                                        @if(request('year') == 3 || request('ayear') == 3) selected @endif>
                                                    Third Year
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Program</label>
                                            @if(auth()->guard('college')->check())
                                                <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                            @endif
                                            <select name="program_id" id="nProgram" class="form-control">
                                                <option value="">Choose Program</option>
                                                <option value="1"
                                                        @if(isset($department) && $department->program_id == 1) selected @endif >
                                                    Diploma (Level 6)
                                                </option>
                                                <option value="2"
                                                        @if(isset($department) && $department->program_id == 2) selected @endif >
                                                    Advanced Diploma (Level 7)
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Department</label>
                                            <select name="department_id" id="nDepartment" class="form-control">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Option</label>
                                            <select name="option_id" id="nOption" class="form-control">
                                                @if (isset($options))
                                                    <option value="">Choose Option</option>
                                                    @foreach ($options as $option)
                                                        <option value="{{ $option->id }}"
                                                                @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{--<div class="col-md-2">--}}
                                    {{--<div class="form-group">--}}
                                    {{--<label for="">Program</label>--}}
                                    {{--<select name="program_id" id="nProgram" class="form-control" >--}}
                                    {{--<option value="">Choose Program</option>--}}
                                    {{--<option value="1" @if(request()->program_id == 1) selected @endif >Diploma (Level 6)</option>--}}
                                    {{--<option value="2" @if(request()->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>--}}
                                    {{--</select>--}}
                                    {{----}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-3">--}}
                                    {{--<div class="form-group">--}}
                                    {{--<label for="">Department</label>--}}
                                    {{--<select name="department_id" id="nDepartment" class="form-control" >--}}
                                    {{--@if (isset($departments))--}}
                                    {{--@foreach ($departments as $department)--}}
                                    {{--<option value="{{ $department->id }}" @if(request()->department_id == $department->id) selected @endif >{{ $department->department_name }}</option>--}}
                                    {{--@endforeach--}}
                                    {{--@endif--}}
                                    {{--</select>--}}
                                    {{----}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-3">--}}
                                    {{--<div class="form-group">--}}
                                    {{--<label for="">Option</label>--}}
                                    {{--<select name="option_id" id="nOption" class="form-control" required>--}}
                                    {{--@if (isset($options))--}}
                                    {{--<option value="">Choose Option</option>--}}
                                    {{--@foreach ($options as $option)--}}
                                    {{--<option value="{{ $option->id }}" @if(request()->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>--}}
                                    {{--@endforeach    --}}
                                    {{--@endif--}}
                                    {{--</select>--}}
                                    {{----}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-2">--}}
                                    {{--<div class="form-group">--}}
                                    {{--<label for="">Year Of Study</label>--}}
                                    {{--<select name="year" id="" class="form-control" required>--}}
                                    {{--<option value="1" @if(request()->year == 1) selected @endif >First Year</option>--}}
                                    {{--<option value="2" @if(request()->year == 2) selected @endif >Second Year</option>--}}
                                    {{--<option value="3" @if(request()->year == 3) selected @endif >Third Year</option>--}}
                                    {{--</select>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {!! Form::label('academic_year', "Academic Year", ['class' => 'control-label']) !!}
                                            {!! Form::select('academic_year', academicYear(), request()->academic_year, ['class' => 'form-control select2']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Semester</label>
                                            <select name="semester" id="" class="form-control" required>
                                                <option value="1" @if(request()->semester == 1) selected @endif >
                                                    Semester 1
                                                </option>
                                                <option value="2" @if(request()->semester == 2) selected @endif >
                                                    Semester 2
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <br>
                                        {!! Form::submit('View Modules',['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        @if (isset($modules))


                            <table class="table table-striped table-bordered table-responsive" id="dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Program</th>
                                    <th>Department</th>
                                    <th>Option</th>
                                    <th>Year of Study</th>
                                    <th>Semester</th>
                                    <th>Module Name</th>
                                    <th>Module Code</th>
                                    <th>Credits</th>
                                    <th>Acad. Year</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                    $year_arr = ['1' => 'First Year', '2' => 'Second Year', '3' => 'Third Year'];
                                @endphp
                                @foreach($modules as $module)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        {{-- <td>
                                            <a href="{{ route('college.modules.index',['o_o'=>$module->id]) }}">{{ $module->program_name }}</a>
                                        </td> --}}
                                        <td>{{ $module->option->department->program_id == 1 ? 'Diploma (Level 6)' : 'Advanced Diploma (Level 7)' }} </td>
                                        <td>{{ $module->option->department->department_name }} </td>
                                        <td>{{ $module->option->option_name }} </td>
                                        <td>{{ $year_arr[$module->year_of_study] }}</td>
                                        <td>Semester {{ $module->semester }}</td>
                                        <td>{{ $module->module_name }}</td>
                                        <td>{{ $module->module_code }}</td>
                                        <td>{{ $module->credits }}</td>
                                        <td>{{ $module->academic_year }}</td>
                                        <td>
                                            <a href="{{ route('college.modules.edit', $module->id) }}"
                                               class="btn btn-primary btn-block " style="margin-bottom: 5px;">Edit</a>
                                            <form action="{{ route('college.modules.destroy', $module->id) }}"
                                                  method="post">
                                                @method("DELETE")
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@endif
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script>
        $(function () {

            $("#year").change(function () {
                if ($("#year").val() === '1') {
                    var id = $("#aCollege").val();
                    getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
                } else {
                    $("#nDepartment").html('');
                    $("#nOption").html('');
                }
            });

            $("#nProgram").change(function () {
                if ($("#year").val() !== '1') {
                    var program = $(this).val();
                    $("#nDepartment").html('');
                    $.get('/college/data/departments/' + program, function (data, status) {
                        $("#nDepartment").html('<option value="">Choose Department</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nDepartment").append('<option value=' + data[i].id + '>' + data[i].department_name + '</option>');
                        }
                    });
                }
            });

            $("#nDepartment").change(function () {
                if ($("#year").val() === '1') {
                    var id = $(this).val();
                    var cl = $("#aCollege").val();
                    getingOPtions(cl, "/poly/c/" + id + "/", $("#nOption"), 'c');
                } else {
                    var program = $(this).val();
                    $("#nOption").html('');
                    $.get('/college/data/options/' + program, function (data, status) {
                        $("#nOption").html('<option value="">Choose Option</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nOption").append('<option value=' + data[i].id + '>' + data[i].option_name + '</option>');
                        }
                    });
                }

            });

            function getingOPtions(id, url, elem, val, cl) {
                $.get(url + id, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select ...</option>";
                    $.each(obj, function (key, value) {
                        var v = "";
                        var a = "";
                        if (val == 'd')
                            v = value.department_name;
                        if (val == 'c') {
                            v = value.program_name;
                            var cd = value.choice_code;
                            var aa = value.study_area;
                            if (cd != undefined) {
                                if (cd.length > 0)
                                    a = " ( " + cd + " ) ";
                            }
                        }
                        if (val == "m")
                            v = value.course_unit_name;
                        html += "<option value='" + value.id + "'>" + v + a + "</option>";
                    });
                    elem.html(html);
                });
            }
        });
    </script>
@show