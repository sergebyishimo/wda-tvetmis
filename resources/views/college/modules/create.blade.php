@extends('college.layout.main')

@section('panel-title', "Create College Module")

@section('htmlheader_title', "Create College Module")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            @if(isset($module))
                @php($route = ['college.modules.update', $module->id])
                @php($id = "updateProgram")
            @else
                @php($route = 'college.modules.store')
                @php($id = "createProgram")
            @endif
            {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
            @if(isset($module))
                @method("PATCH")
                {!! Form::hidden('id', $module->id) !!}
            @endif
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Year Of Study</label>
                                <select name="year" id="year" class="form-control" required>
                                    <option value="4">All Years</option>
                                    <option value="1" @if(request('year') == 1 || request('ayear') == 1) selected @endif>First Year</option>
                                    <option value="2" @if(request('year') == 2 || request('ayear') == 2) selected @endif>Second Year</option>
                                    <option value="3" @if(request('year') == 3 || request('ayear') == 3) selected @endif>Third Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Program</label>
                                @if(auth()->guard('college')->check())
                                    <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                @endif
                                <select name="program_id" id="nProgram" class="form-control">
                                    <option value="">Choose Program</option>
                                    <option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>
                                    <option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Department</label>
                                <select name="department_id" id="nDepartment" class="form-control">

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Option</label>
                                <select name="option_id" id="nOption" class="form-control">
                                    @if (isset($options))
                                        <option value="">Choose Option</option>
                                        @foreach ($options as $option)
                                            <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="">Program</label>--}}
                                {{--<select name="option_id" id="nProgram" class="form-control" >--}}
                                    {{--<option value="">Choose Program</option>--}}
                                    {{--<option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>--}}
                                    {{--<option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>--}}
                                {{--</select>--}}
                                {{----}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="">Department</label>--}}
                                {{--<select name="option_id" id="nDepartment" class="form-control" >--}}
                                    {{-- <option value="">Choose Option</option>--}}
                                    {{--@foreach ($options as $option)--}}
                                        {{--<option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>--}}
                                    {{--@endforeach --}}
                                {{--</select>--}}
                                {{----}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="">Option</label>--}}
                                {{--<select name="option_id" id="nOption" class="form-control" required>--}}
                                    {{--@if (isset($options))--}}
                                        {{--<option value="">Choose Option</option>--}}
                                        {{--@foreach ($options as $option)--}}
                                            {{--<option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>--}}
                                        {{--@endforeach    --}}
                                    {{--@endif--}}
                                {{--</select>--}}
                                {{----}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="">Year Of Study</label>--}}
                                {{--<select name="year" id="" class="form-control" required>--}}
                                    {{--<option value="1" @if(isset($module) &&$module->year_of_study == 1) selected @endif >First Year</option>--}}
                                    {{--<option value="2" @if(isset($module) &&$module->year_of_study == 2) selected @endif >Second Year</option>--}}
                                    {{--<option value="3" @if(isset($module) &&$module->year_of_study == 3) selected @endif >Third Year</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        
                    </div>
                    
                    
                    <div class="row" id="module_row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Semester</label>
                                <select name="semester[]" id="" class="form-control" required>
                                    <option value="">Choose Semester</option>
                                    <option value="1" @if(isset($module) &&$module->semester == 1) selected @endif >Semester 1</option>
                                    <option value="2" @if(isset($module) &&$module->semester == 2) selected @endif >Semester 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Module Code</label>
                                <input type="text" name="module_code[]" class="form-control" placeholder="Module Code" value="{{ $module->module_code or '' }}" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Module Name</label>
                                <input type="text" name="module_name[]" class="form-control" placeholder="Module Name" value="{{ $module->module_name or '' }}" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Credits</label>
                                <input type="text" name="credits[]" class="form-control" placeholder="Credits" value="{{ $module->credits or '' }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Academic Year</label>
                                <select name="academic_year[]" id="" class="form-control" required>
                                    @php($academic_arr = '')
                                    @for ($i = (date('Y') + 1); $i >= 2015; $i--)
                                        @php($acad = $i - 1 . ' - ' . $i)
                                        <option value="{{ $acad }}">{{ $acad }}</option>
                                        @php($academic_arr .= "<option value='".$acad."'>". $acad ."</option>")
                                    @endfor
                                </select>
                            </div>
                        </div>
                        @if (!isset($module))
                            <div class="col-md-1">
                                <br clear="left">
                                <button type="button" class="btn btn-primary" style="margin-top: 10px" onclick="addModule()"><i class="fa fa-plus"></i></button>
                            </div>    
                        @endif
                        
                    </div>

                    
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $module ? "Update Module" : "Save Module" }}
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('l-scripts')
@parent
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<!-- Laravel Javascript Validation -->
{{-- <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
@if($module)
    {!! JsValidator::formRequest('App\Http\Requests\RpProgramUpdateRequest', '#updateProgram'); !!}
@else
    {!! JsValidator::formRequest('App\Http\Requests\RpProgramRequest', '#createProgram'); !!}
@endif --}}
@if(isset($academic_arr))
    <script type="text/javascript"> var academic_arr = "<?= $academic_arr;  ?>"; </script>
@endif
<script>
    $(function() {

        $("#year").change(function() {
            if($("#year").val() === '1') {
                var id = $("#aCollege").val();
                getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
            } else {
                $("#nDepartment").html('');
                $("#nOption").html('');
            }
        });

        $("#nProgram").change(function() {
            if ($("#year").val() !== '1') {
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program, function(data,status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                    }
                });
            }
        });

        $("#nDepartment").change(function() {
            if ($("#year").val() === '1') {
                var id = $(this).val();
                var cl = $("#aCollege").val();
                getingOPtions(cl, "/poly/c/"+id+"/", $("#nOption"), 'c');
            } else {
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program, function(data,status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                    }
                });
            }

        });

        function getingOPtions(id, url, elem, val, cl) {
            $.get(url + id, {}, function (data) {
                var obj = jQuery.parseJSON(data);
                var html = "<option value='' selected disabled>Select ...</option>";
                $.each(obj, function (key, value) {
                    var v = "";
                    var a = "";
                    if (val == 'd')
                        v = value.department_name;
                    if (val == 'c') {
                        v = value.program_name;
                        var cd = value.choice_code;
                        var aa = value.study_area;
                        if (cd != undefined){
                            if (cd.length > 0)
                                a = " ( " + cd + " ) ";
                        }
                    }
                    if (val == "m")
                        v = value.course_unit_name;
                    html += "<option value='" + value.id + "'>" + v + a + "</option>";
                });
                elem.html(html);
            });
        }
    });
</script>
<script>

function addModule() {
    $("#module_row").append('<div class="col-md-2"> <div class="form-group"> <label for="">Semester</label> <select name="semester[]" id="" class="form-control" required> <option value="">Choose Semester</option> <option value="1">Semester 1</option> <option value="2">Semester 2</option> </select> </div> </div><div class="col-md-2"> <div class="form-group"> <label for="">Module Code</label> <input type="text" name="module_code[]" class="form-control" placeholder="Module Code" value="{{ $module->module_code or '' }}" required> </div> </div> <div class="col-md-3"> <div class="form-group"> <label for="">Module Name</label> <input type="text" name="module_name[]" class="form-control" placeholder="Module Name" value="{{ $module->module_name or '' }}" required> </div> </div> <div class="col-md-2"> <div class="form-group"> <label for="">Credits</label> <input type="text" name="credits[]" class="form-control" placeholder="Credits" value="{{ $module->credits or '' }}"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label for="">Academic Year</label> <select name="academic_year[]" class="form-control" required> ' + academic_arr +' </select> </div>  </div> <div class="col-md-1"> <br clear="left"> <button type="button" class="btn btn-primary" style="margin-top: 10px" onclick="addModule()"><i class="fa fa-plus"></i></button> </div>');
}

</script>

@show