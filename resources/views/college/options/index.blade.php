@extends('college.layout.main')

@section('panel-title', "College Options")

@section('htmlheader_title', "College Options")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @if($option)
                        @php($route = ['college.options.update', $option->id])
                        @php($id = "updateDepartment")
                    @else
                        @php($route = 'college.options.store')
                        @php($id = "createDepartment")
                    @endif
                    {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                    @if($option)
                        @method("PATCH")
                        {!! Form::hidden('id', $option->id) !!}
                    @endif
                    <div class="box-header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Program</label>
                                    <select name="option_id" id="nProgram" class="form-control" >
                                        <option value="">Choose Program</option>
                                        <option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>
                                        <option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Departments</label>
                                    <select name="department_id" id="nDepartment" class="form-control" required>
                                        @if (isset($option))
                                            <option value="">Choose Department</option>
                                            @foreach ($departments as $department)
                                                <option value="{{ $department->id }}" @if(isset($option) && $option->department_id == $department->id) selected  @endif >{{ $department->department_name }}</option>
                                            @endforeach    
                                        @endif
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('option_name', "Option Name *") !!}
                                    {!! Form::text('option_name', $option ? $option->option_name: "", ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-md btn-primary pull-left">
                                    {{ $option ? "Update Option" : "Save Option" }}
                                </button>
                                <a href="{{ route('college.options.index') }}"
                                   class="btn btn-md btn-warning pull-right">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

            <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List Of Departments</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Program</th>
                                <th>Department</th>
                                <th>Option Name</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($options as $depart)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $depart->department->program_id == 1 ? 'Diploma (Level 6)' : 'Advanced Diploma (Level 7)'  }}</td>
                                    <td>{{ $depart->department->department_name  }}</td>
                                    <td>{{ ucwords($depart->option_name) }}</td>
                                    <td>
                                        <a href="{{ route('college.options.edit', $depart->id) }}"
                                           class="btn btn-primary btn-block" style="margin-bottom: 5px;">Edit</a>
                                        <form action="{{ route('college.options.destroy', $depart->id) }}"
                                              method="post">
                                            @method("DELETE")
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>

    @include('college.layout.modals.warnings')
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });


        });
    </script>

<script>

    $(function() {
        $("#nProgram").change(function() {
            var program = $(this).val();
            $("#nDepartment").html('');
            $.get('/college/data/departments/' + program, function(data,status) {
                $("#nDepartment").html('<option value="">Choose Department</option>');
                for (i = 0; i < data.length; i++) {
                    $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                }
            });
        });
    
        
    });
</script>
    <!-- Laravel Javascript Validation -->
    {{-- <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($option)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateRpDepartmentRequest', '#updateDepartment'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateRpDepartmentRequest', '#createDepartment'); !!}
    @endif --}}
@show