@extends('college.layout.main')

@section('panel-title', "Staff List")

@section('htmlheader_title', "Staff List")

@section('l-style')
    @parent

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body">
                @if(request()->input('search'))
                    {!! Form::open() !!}
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Year Of Study</label>
                                <select name="year" id="nYearOfStudy" class="form-control select2" style="width: 100%;"
                                        required>
                                    <option value="">Select ...</option>
                                    <option value="1"
                                            @if(isset($module) &&$module->year_of_study == 1) selected @endif >First
                                        Year
                                    </option>
                                    <option value="2"
                                            @if(isset($module) &&$module->year_of_study == 2) selected @endif >
                                        Second Year
                                    </option>
                                    <option value="3"
                                            @if(isset($module) &&$module->year_of_study == 3) selected @endif >Third
                                        Year
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="keypoints">
                            {{--<div class="col-md-2">--}}
                            {{--<div class="form-group">--}}
                            {{--<label for="rp" style="font-weight: bold;margin-bottom: 0px;cursor: pointer">--}}
                            {{--RP--}}
                            {{--</label>--}}
                            {{--<div class="checkbox icheck">--}}
                            {{--<div>--}}
                            {{--<input type="checkbox"--}}
                            {{--checked--}}
                            {{--name="rp" id="rp" value="y">--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Program</label>
                                    <select name="program_id" id="nProgram" class="form-control select2"
                                            style="width: 100%;">
                                        <option value="">Choose Program</option>
                                        <option value="1"
                                                @if(isset($department) && $department->program_id == 1) selected @endif >
                                            Diploma (Level 6)
                                        </option>
                                        <option value="2"
                                                @if(isset($department) && $department->program_id == 2) selected @endif >
                                            Advanced Diploma (Level 7)
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Department</label>
                                    <select name="department_id" id="nDepartment" class="form-control select2"
                                            style="width: 100%;" required>
                                        <option value="">Choose Program</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Option</label>
                                    <select name="option_id" id="nOption" class="form-control select2" required
                                            style="width: 100%;">
                                        @if (isset($options))
                                            <option value="">Choose Option</option>
                                            @foreach ($options as $option)
                                                <option value="{{ $option->id }}"
                                                        @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('semester', "Semester", ['class' => 'control-label']) !!}
                                    {!! Form::select('semester', [1 => 1, 2 => 2], null, ['class' => 'form-control select2', 'placeholder' => 'select ...', 'required' => true, 'style' => 'width: 100%;']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('module', "Modules", ['class' => 'control-label']) !!}
                                    {!! Form::select('module', [], null, ['class' => 'form-control select2', 'placeholder' => 'select ...', 'style' => 'width: 100%;']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-sm btn-primary pull-left" style="margin-top: 26px;">
                                View
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                @else
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('college.lecturers.index') }}?search=1" class="btn btn-primary">
                                <i class="fa fa-search-plus"></i>
                                <span>Filter</span>
                            </a>
                        </div>
                        @if (collegeAllowed(4, '8'))
                            <div class="col-md-6">
                                <a href="{{ route('college.lecturers.create') }}" class="btn btn-warning pull-right">
                                    <i class="fa fa-plus-circle"></i>
                                    <span>Add New Staff</span>
                                </a>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
        </div>
        @isset($view)
            <div class="panel">
                <div class="panel-body">
                    @include($view)
                </div>
            </div>
        @endisset
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#dataTableLecturers").dataTable({
                dom: 'Blfrtip',
                pager: true,
                "ordering": true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
        });
        $(function () {
            $(".keypoints").hide();
            $("#nYearOfStudy").on("change", function () {
                $(".keypoints").fadeIn(555);
                $("*.keypoints select").val("").trigger('change');
            });
            $("#nProgram").change(function () {
                let year = $("#nYearOfStudy").val();
                let program = $(this).val();
                if (program) {
                    $("#nDepartment").html('');
                    $.get('/college/data/departments/' + program + "?y=" + year, function (data, status) {
                        $("#nDepartment").html('<option value="">Choose Department</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nDepartment").append('<option value=' + data[i].id + '>' + data[i].department_name + '</option>');
                        }
                    });
                }
            });
            $("#nDepartment").change(function () {
                let year = $("#nYearOfStudy").val();
                let program = $(this).val();
                if (program) {
                    $("#nOption").html('');
                    $.get('/college/data/options/' + program + "?y=" + year, function (data, status) {
                        $("#nOption").html('<option value="">Choose Option</option>');
                        if (year === '1') {
                            let o = $.parseJSON(data);
                            $.each(o, function (i, item) {
                                $("#nOption").append('<option value=' + item.id + '>' + item.program_name + '</option>');
                            });
                        } else {
                            for (i = 0; i < data.length; i++) {
                                $("#nOption").append('<option value=' + data[i].id + '>' + data[i].option_name + '</option>');
                            }
                        }
                    });
                }
            });
            $("#nOption").on("change", function () {
                let program = $(this).val();
                if (program) {
                    let year = $("#nYearOfStudy").val();
                    $.get('/college/data/modules/' + program + "?y=" + year, function (data, status) {
                        if (data.length > 0) {
                            $("#module").html('<option value="">Select ...</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#module").append('<option value=' + data[i].id + '>( ' + data[i].module_code + ' ) ' + data[i].module_name.toUpperCase() + '</option>');
                            }
                        }
                    });
                }
            });
            $("#semester").on("change", function () {
                let program = $("#nOption").val();
                if (program) {
                    let year = $("#nYearOfStudy").val();
                    let semester = $(this).val();
                    $.get('/college/data/modules/' + program + "?y=" + year + "&semester=" + semester, function (data, status) {
                        if (data.length > 0) {
                            $("#module").html('<option value="">Select ...</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#module").append('<option value=' + data[i].id + '>( ' + data[i].module_code + ' ) ' + data[i].module_name.toUpperCase() + '</option>');
                            }
                        }
                    });
                }
            });
        });
    </script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
@show
