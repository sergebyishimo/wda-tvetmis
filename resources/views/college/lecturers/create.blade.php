@extends('college.layout.main')

@section('panel-title', "Add New Staff")

@section('htmlheader_title', "Add New Staff")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('college.lecturers.index') }}" class="btn btn-warning pull-right">
                            <i class="fa fa-list-ul"></i>
                            <span>Go Back</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('college.lecturers.store') }}"
                      id="createLecturer" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                   autofocus required>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                        <label for="telephone" class="col-md-4 control-label">Telephone</label>

                        <div class="col-md-6">
                            <input id="telephone" type="number" class="form-control" name="telephone"
                                   value="{{ old('telephone') }}" required>

                            @if ($errors->has('telephone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('nationID') ? ' has-error' : '' }}">
                        <label for="nationID" class="col-md-4 control-label">NationID</label>

                        <div class="col-md-6">
                            <input id="nationID" type="number" class="form-control" value="{{ old('nationID') }}"
                                   name="nationID">

                            @if ($errors->has('nationID'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('nationID') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                        <label for="nationality" class="col-md-4 control-label">Nationality</label>

                        <div class="col-md-6">
                            <input id="nationality" type="text" class="form-control" value="{{ old('nationality') }}"
                                   name="nationality">

                            @if ($errors->has('nationality'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('nationality') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                        <label for="category" class="col-md-4 control-label">Staff Type</label>

                        <div class="col-md-6">
                            {!! Form::select('category', getCollegeCategoryStaff() , null, [ 'class' => 'form-control select2', 'required' => true, 'placeholder' => 'select ...' ] ) !!}
                            @if ($errors->has('category'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                        <label for="position" class="col-md-4 control-label">Staff Position</label>

                        <div class="col-md-6">
                            {!! Form::select('position', $positions , null, [ 'class' => 'form-control select2', 'required' => true, 'placeholder' => 'select ...' ] ) !!}
                            @if ($errors->has('position'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        <label for="gender" class="col-md-4 control-label">Gender</label>

                        <div class="col-md-6">
                            {!! Form::select('gender', ['Male' => "Male", "Female" => 'Female'], null, ['class' => 'form-control select2', "placeholder" => 'select ...']) !!}

                            @if ($errors->has('gender'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                        <label for="photo" class="col-md-4 control-label">Photo</label>

                        <div class="col-md-6">
                            {!! Form::file('photo', null) !!}

                            @if ($errors->has('photo'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('is_head_of_department') ? ' has-error' : '' }}">
                        <label for="isHeadOfDepartment" class="col-md-4 control-label">Is head of department ?</label>

                        <div class="col-md-6">
                            {!! Form::select('is_head_of_department', [0 => 'No', 1 => 'Yes'], null, ['class' => 'form-control select2', "placeholder" => 'select ...', 'id' => 'isHeadOfDepartment']) !!}

                            @if ($errors->has('is_head_of_department'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('is_head_of_department') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div id="whichDepartment" class="row" style="margin-left: 120px;">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Year Of Study</label>
                                <select name="year_of_study" id="year" class="form-control" required>
                                    <option value="4">All Years</option>
                                    <option value="1"
                                            @if(request('year') == 1 || request('ayear') == 1) selected @endif>First
                                        Year
                                    </option>
                                    <option value="2"
                                            @if(request('year') == 2 || request('ayear') == 2) selected @endif>Second
                                        Year
                                    </option>
                                    <option value="3"
                                            @if(request('year') == 3 || request('ayear') == 3) selected @endif>Third
                                        Year
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Program</label>
                                @if(auth()->guard('college')->check())
                                    <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                @endif
                                <select name="program_id" id="nProgram" class="form-control">
                                    <option value="">Choose Program</option>
                                    <option value="1"
                                            @if(isset($department) && $department->program_id == 1) selected @endif >
                                        Diploma (Level 6)
                                    </option>
                                    <option value="2"
                                            @if(isset($department) && $department->program_id == 2) selected @endif >
                                        Advanced Diploma (Level 7)
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nDepartment">Department</label>
                                <select name="department" id="nDepartment" class="form-control"></select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $("#whichDepartment").hide();
            $("#isHeadOfDepartment").on("change", function () {
                let v = $(this).val();
                if (v === '1'){
                    $("#whichDepartment").fadeIn(555);
                    $("#department").attr("required", true);
                }
                else{
                    $("#whichDepartment").fadeOut(555);
                    $("#department").attr("required", false);
                }
            });

            $("#year").change(function () {
                if ($("#year").val() === '1') {
                    var id = $("#aCollege").val();
                    getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
                } else {
                    $("#nDepartment").html('');
                }
            });

            $("#nProgram").change(function () {
                if ($("#year").val() !== '1') {
                    var program = $(this).val();
                    $("#nDepartment").html('');
                    $.get('/college/data/departments/' + program, function (data, status) {
                        $("#nDepartment").html('<option value="">Choose Department</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nDepartment").append('<option value=' + data[i].id + '>' + data[i].department_name + '</option>');
                        }
                    });
                }
            });
        });
    </script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreLecturerRequest', '#createLecturer'); !!}
@show
