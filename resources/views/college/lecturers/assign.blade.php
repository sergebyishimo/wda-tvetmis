@extends('college.layout.main')

@section('panel-title', "Geo Location")

@section('htmlheader_title', "Geo Location")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ auth()->guard('lecturer')->check() ? route('lecturer.lecturers.index') : route('college.lecturers.index') }}" class="btn btn-warning pull-right">
                            <i class="fa fa-list-ul"></i>
                            <span>Go Back</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            {!! Form::label('lecturer', "Lecturer", ['class' => 'control-label']) !!}
                            {!! Form::select('lecturer', $lecturers, null, ['class' =>'form-control select2', 'placeholder' => 'select ...']) !!}
                        </div>
                    </div>
                </div>
                <div class="row lecturer-proper">
                    @if (isHOD())
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Year Of Study</label>
                                        <input type="text" class="form-control" readonly
                                               name="year" id="nYearOfStudy"
                                               value="{{ lecturer(null, 'year_of_study') }}">
                                    </div>
                                </div>
                                <div class="keypoints">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Department</label>
                                            <input type="hidden" name="department_id" id="nDepartment" value="{{ lecturer(null,'department') }}">
                                            <select class="form-control disabled"
                                                    disabled
                                                    style="width: 100%;" required>
                                                @if (isset($departments))
                                                    <option value="">Choose Option</option>
                                                    @foreach ($departments as $department)
                                                        <option value="{{ $department->id }}"
                                                                @if(isset($department) && $department->id == lecturer(null,'department')) selected @endif >{{ $department->department_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Option</label>
                                            <select name="option_id" id="nOption" class="form-control select2" required
                                                    style="width: 100%;">
                                                @if (isset($options))
                                                    <option value="">Choose Option</option>
                                                    @foreach ($options as $option)
                                                        <option value="{{ $option->id }}"
                                                                @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('semester', "Semester", ['class' => 'control-label']) !!}
                                            {!! Form::select('semester', [1 => 1, 2 => 2], null, ['class' => 'form-control select2', 'placeholder' => 'select ...', 'required' => true, 'style' => 'width: 100%;']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('module', "Modules", ['class' => 'control-label']) !!}
                                            {!! Form::select('module', [], null, ['class' => 'form-control select2', 'placeholder' => 'select ...', 'style' => 'width: 100%;']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary pull-left save">
                                        <i class="fa fa-save"></i>
                                        <span>Save</span>
                                    </button>&nbsp;&nbsp;&nbsp;
                                    <span class="text-success feedback"></span>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-{{ isHOD() ? '8' : '12' }}" style="overflow-y: auto;">
                        <h4 class="text-center bg-primary">All Course Assigned</h4>
                        <div id="content-course">
                            <center>
                                <img src="{{ asset('img/ajaxload.gif') }}" class="mt-2"/>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script type="text/javascript">
        $(function () {
            $(".lecturer-proper").hide();
            $("#lecturer").on("change", function () {
                let v = $(this).val();
                if (v) {
                    $("#content-course").load("{{ auth()->guard('college')->check() ? '/college/lecturers/assigned/' : '/lecturer/location/' }}" + v);
                    $(".lecturer-proper").fadeIn(555);
                } else {
                    $(".lecturer-proper").fadeOut();
                }
            });
            $(".save").hide();
            $("#nYearOfStudy").on("change", function () {
                $(".keypoints").fadeIn(555);
                $("*.keypoints select").val("").trigger('change');
            });
            $("#nProgram").change(function () {
                let year = $("#nYearOfStudy").val();
                let program = $(this).val();
                if (program) {
                    $("#nDepartment").html('');
                    $.get('/college/data/departments/' + program + "?y=" + year, function (data, status) {
                        $("#nDepartment").html('<option value="">Choose Department</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nDepartment").append('<option value=' + data[i].id + '>' + data[i].department_name + '</option>');
                        }
                    });
                }
            });
            $("#nDepartment").change(function () {
                let year = $("#nYearOfStudy").val();
                let program = $(this).val();
                if (program) {
                    $("#nOption").html('');
                    $.get('/college/data/options/' + program + "?y=" + year, function (data, status) {
                        $("#nOption").html('<option value="">Choose Option</option>');
                        if (year === '1') {
                            let o = $.parseJSON(data);
                            $.each(o, function (i, item) {
                                $("#nOption").append('<option value=' + item.id + '>' + item.program_name + '</option>');
                            });
                        } else {
                            for (i = 0; i < data.length; i++) {
                                $("#nOption").append('<option value=' + data[i].id + '>' + data[i].option_name + '</option>');
                            }
                        }
                    });
                }
            });
            $("#nOption").on("change", function () {
                let program = $(this).val();
                if (program) {
                    let year = $("#nYearOfStudy").val();
                    $.get('/college/data/modules/' + program + "?y=" + year, function (data, status) {
                        if (data.length > 0) {
                            $("#module").html('<option value="">Select ...</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#module").append('<option value=' + data[i].id + '>( ' + data[i].module_code + ' ) ' + data[i].module_name.toUpperCase() + '</option>');
                            }
                        }
                    });
                }
            });
            $("#semester").on("change", function () {
                let program = $("#nOption").val();
                if (program) {
                    let year = $("#nYearOfStudy").val();
                    let semester = $(this).val();
                    $.get('/college/data/modules/' + program + "?y=" + year + "&semester=" + semester, function (data, status) {
                        if (data.length > 0) {
                            $("#module").html('<option value="">Select ...</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#module").append('<option value=' + data[i].id + '>( ' + data[i].module_code + ' ) ' + data[i].module_name.toUpperCase() + '</option>');
                            }
                        }
                    });
                }
            });
            $("#module").on("change", function () {
                let program = $(this).val();
                if (program) {
                    $(".save").fadeIn(555);
                }
            });
            $(".save").on("click", function () {
                let d = $("#nDepartment").val();
                let p = $("#nOption").val();
                let y = $("#nYearOfStudy").val();
                let m = $("#module").val();
                let v = $("#lecturer").val();
                $.post("{{ route('lecturer.assigning') }}", {
                    _token: "{{ csrf_token() }}",
                    department_id: d,
                    program_id: p,
                    year_of_study: y,
                    course_id: m,
                    lecturer_id: v
                }, function (data) {
                    if (data.status == 'ok') {
                        let v = $("#lecturer").val();
                        if (v)
                            $("#content-course").load("{{ auth()->guard('college')->check() ? '/college/lecturers/assigned/' : '/lecturer/location/' }}" + v);
                        $(".feedback").text("Saved Successfully");
                    } else {
                        $(".feedback").removeClass("text-success")
                            .addClass("text-danger")
                            .text(data.message);
                    }
                });
            });
        });
    </script>
@show