<table class="table table-condensed table-bordered">
    <thead>
    <tr>
        <th class="text-center">#</th>
        <th class="text-center">Course Name</th>
        <th class="text-center">Program Name</th>
        <th class="text-center">Semester</th>
        <th class="text-center">YS</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @php
        $x = 1;
    @endphp
    @forelse($courses as $course)
        <tr>
            <td class="text-center">{{ $x++ }}</td>
            <td class="text-center">{{ getLecturerCourse($course,'name') }}</td>
            <td class="text-center">{{ getLecturerCourse($course,'program') }}</td>
            <td class="text-center">{{ getLecturerCourse($course,'semester') }}</td>
            <td class="text-center">{{ $course->year_of_study }}</td>
            <td class="text-center">
                <button data-row="{{ $course->id }}" class="btn btn-danger delete-course">Delete</button>
            </td>
        </tr>
    @empty
        <tr>
            <td class="text-center" colspan="5">No course assigned !!</td>
        </tr>
    @endforelse
    </tbody>
</table>