<table class="table table-condensed table-bordered" id="dataTableLecturers">
    <thead>
    <tr>
        <th class="text-center">#</th>
        <th>Photo</th>
        <th>Names</th>
        <th>Telephone</th>
        <th>Email</th>
        <th>Category</th>
        <th>Position</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @isset($lecturers)
        @php
            $x = 1;
        @endphp
        @foreach($lecturers as $lecturer)
            <tr>
                <td class="text-center" style="vertical-align: middle">{{ $x++ }}</td>
                <td style="vertical-align: middle">
                    @if ($lecturer->photo)
                        <img src="{{ asset('storage/'.$lecturer->photo) }}" alt="{{ $lecturer->name }}" width="80px"
                             class="img-rounded shadow-lg"
                             height="85px">
                    @else
                        <img src="{{ asset('img/user_default.png') }}" alt="{{ $lecturer->name }}" width="80px"
                             class="img img-responsive img-rounded"
                             height="80px">
                    @endif
                </td>
                <td style="vertical-align: middle">{{ $lecturer->name }}</td>
                <td style="vertical-align: middle">{{ $lecturer->telephone }}</td>
                <td style="vertical-align: middle">{{ $lecturer->email }}</td>
                <td style="vertical-align: middle">{!! $lecturer->category ? getCollegeCategoryStaff($lecturer->category) : "No set" !!}</td>
                <td style="vertical-align: middle">{{ $lecturer->position_name }}</td>
                <td style="vertical-align: middle">
                    <di class="row p-0">
                        <div class="col-md-6">
                            <a href="{{ route('college.lecturers.edit', $lecturer->id) }}"
                               class="btn btn-primary btn-sm">
                                <i class="fa fa-edit"></i>
                                <span>Edit</span>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <form action="{{ route('college.lecturers.destroy', $lecturer->id) }}"
                                  method="post">
                                @method("DELETE")
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash-o"></i>
                                    <span>Delete</span>
                                </button>
                            </form>
                        </div>
                    </di>
                </td>
            </tr>
        @endforeach
    @endisset
    </tbody>
</table>