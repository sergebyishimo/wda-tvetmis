@extends('college.layout.main')

@section('panel-title', "Accommodation Applications")

@section('htmlheader_title', "Accommodation Applications")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
        <!-- List box -->
            <div class="box">
                <div class="box-body">
                    <table class="table table-striped table-bordered table-responsive" id="dataTable">
                        <thead>
                            <tr>
                                <th>Student Names</th>
                                <th>Department</th>
                                <th>Option</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($apps as $app)
                                <tr>
                                    <td>{{ $app->student->first_name . ' ' . $app->student->other_names }}</td>
                                    <td>{{ $app->student ? getDepartment($app->student->department_id, 'department_name', isContinuingStudent($app->student_reg)) : '' }}</td>
                                    <td>{{ $app->student ? getCourse($app->student->department_id, 'program_name', isContinuingStudent($app->student_reg)) : '' }}</td>
                                    <td><a href="/storage/{{ $app->file }}" target="_blank">Download File</a></td>
                                    <td>
                                        <a href="#" class="btn btn-primary"><i class="fa fa-check"></i> Approve</a>
                                        <a href="#" class="btn btn-danger"><i class="fa fa-close"></i> Deny</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection