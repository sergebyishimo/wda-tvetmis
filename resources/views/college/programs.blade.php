@extends('college.layout.main')

@section('panel-title', "Programs Offered : " . App\Model\Rp\CollegeProgram::where('college_id', college('college')->id)->count())

@section('htmlheader_title', "Programs Offered")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            
                                <table class="table table-striped table-bordered" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Program Name</th>
                                            <th>Program Code</th>
                                            <th>Program Load</th>
                                            <th>RTQF Level</th>
                                            <th>STEM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i= 1;
                                        @endphp
                                        @foreach ($programs as $program)
                                            @php
                                                $program = App\Model\Rp\RpProgram::find($program->program_id);
                                            @endphp
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $program->program_name . ' (' . count($program->students) . ')' }} </td>
                                                <td>{{ $program->program_code }}</td>
                                                <td>{{ $program->program_load }}</td>
                                                <td>{{ $program->rtqf ? $program->rtqf->level_name : '' }}</td>
                                                <td>{{ ucwords($program->program_is_stem) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                paging:false
            });
            
        });
        
    </script>
@show