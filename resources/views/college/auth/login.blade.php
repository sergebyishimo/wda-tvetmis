@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
    <body class="hold-transition login-page" style="background-image: url({{ asset('img/bg_wood.png') }}); background-attachment: fixed;; background-attachment: fixed;">
    <div id="app" v-cloak>
        <div class="login-box" style="margin-top: 40px;">
            <div class="row">
                <div class="col-md-6 col-md-offset-4 img-responsive">
                    <img src="{{ asset('img/Coat_of_arms_of_Rwanda.png') }}" alt="{{ config('app.name') }}"
                         class="img-responsive" width="100px">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center" style="color: #37752f;">
                        <b>TVET Information System</b>
                    </h1>
                </div>
            </div>
            <div class="login-logo">
                <div class="row">
                    <div class="col-md-12" style="font-size: 20px">
                        <a href="{{ url('/college/login') }}"><b>College</b>&nbsp;login</a>
                    </div>
                </div>
            </div><!-- /.login-logo -->

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="login-box-body" style="box-shadow: 0 2px 4px #777777; border-radius: 0.2em;">
                <p class="login-box-msg"> {{ trans('adminlte_lang::message.siginsession') }} </p>
                <form action="{{ url('/college/login') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control"
                               placeholder="{{ trans('adminlte_lang::message.username') }}" name="username"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control"
                               placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 pull-right">
                            <button type="submit"
                                    class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.buttonsign') }}</button>
                        </div><!-- /.col -->
                    </div>
                    <div class="row" style="margin-top: 5px;">
                        <div class="col-xs-12">
                            <a href="{{ route('lecturer.login') }}"
                               class="btn btn-warning">Lecturer Login</a>
                        </div><!-- /.col -->
                    </div>
                </form>

                {{--<a href="{{ url('/college/password/reset') }}">{{ trans('adminlte_lang::message.forgotpassword') }}</a><br>--}}
            </div><!-- /.login-box-body -->

        </div><!-- /.login-box -->
    </div>
    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    </body>
@endsection