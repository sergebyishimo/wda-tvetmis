@if(auth()->guard('college')->check())
    <li class="{{ activeMenu('college.home') }}">
        <a href="{{ route('college.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>
    {{--<li><a href="{{ route('college.departments.index') }}"><i class='fa fa-building'></i>&nbsp;Departments</a></li>--}}
    {{--<li><a href="{{ route('college.options.index') }}"><i class='fa fa-building'></i>&nbsp;Options</a></li>--}}

    @if (collegeAllowed(1, 'd') || collegeAllowed(1, 'a'))
        <li class="{{ activeMenu(['college.modules.index', 'college.modules.create', 'college.modules.view']) }}"><a
                    href="{{ route('college.modules.index') }}"><i class='fa fa-building'></i>&nbsp;Modules</a></li>
    @endif
    {{-- <li><a href="{{ route('college.programs.index') }}"><i class='fa fa-building'></i>&nbsp;Programs</a></li> --}}
    {{-- <li class="{{ activeMenu('college.programs') }}">
        <a href="{{ route('college.programs') }}"><i class='fa fa-bookmark'></i>&nbsp;<span>Programs Offered</span>
            <span class="pull-right-container">
                <span class="label label-primary pull-right">{{ App\Model\Rp\CollegeProgram::where('college_id', college('college')->id)->count() }}</span>
            </span>
        </a>
    </li> --}}
    @if (collegeAllowed(1, 'd') || collegeAllowed(1, 'r'))
        <li class="treeview {{ activeMenu([
            'college.students.admitted', 'college.students.index', 'college.students.edit',
            'college.students.view',
            'college.social.index', 'college.social.edit', 'college.social.view', 'college.social.upload',
            'college.social.index', 'college.social.create', 'college.social.edit', 'college.social.show',
            'college.student.transfer.going', 'college.student.transfer.coming',
            'college.student.single', 'college.student.single', 'college.query.execute','college.suspension.index',
            'college.cards.index'
        ]) }}">
            <a href="#">
                <i class='fa fa-magnet'></i>
                <span>Students Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['college.students.admitted', 'college.students.edit', 'college.students.view']) }}">
                    <a href="{{ route('college.students.admitted') }}">
                        <i class='fa fa-link'></i>&nbsp;Admitted Students
                        <span class="pull-right-container">
                        <span class="label label-primary pull-right">{{ App\Rp\AdmittedStudent::where('college_id', college('college')->id)->count() }}</span>
                    </span>
                    </a>
                </li>

                @if (collegeAllowed(1, 'r'))
                    <li class="{{ activeMenu(['college.students.index', 'college.students.edit', 'college.students.view']) }}">
                        <a href="{{ route('college.registration.index') }}">
                            <i class='fa fa-link'></i>&nbsp;Registered Students
                        </a>
                    </li>
                @endif

                <li class="{{ activeMenu(['college.social.index', 'college.social.edit', 'college.social.view', 'college.social.create']) }}">
                    <a href="{{ route('college.social.index') }}">
                        <i class='fa fa-link'></i>&nbsp;Social Students
                    </a>
                </li>
                <li class="{{ activeMenu(['college.student.transfer.going', 'college.student.transfer.coming']) }}">
                    <a href="{{ route('college.student.transfer.going') }}"><i class='fa fa-link'></i>&nbsp;Transferring
                        Requests</a>
                </li>
                <li class="treeview {{ activeMenu([
                'college.student.single', 'college.student.single', 'college.query.execute'
                ]) }}">
                    <a href="#">
                        <i class='fa fa-search'></i>
                        <span>Make Queries</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        {{--<li class="{{ activeMenu(['college.students', 'college.students']) }}">--}}
                        {{--<a href="{{ route('college.students') }}">--}}
                        {{--<i class='fa fa-users'></i>&nbsp;--}}
                        {{--<span>Students List</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        <li class="{{ activeMenu(['college.student.single', 'college.student.single', 'college.query.execute']) }}">
                            <a href="{{ route('college.student.single') }}">
                                <i class='fa fa-link'></i>&nbsp;
                                <span>On Single Student</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @if (collegeAllowed(1, 'a'))
                    <li class="{{ activeMenu(['college.accommodation.index', 'college.accommodation.edit', 'college.accommodation.view', 'college.accommodation.create']) }}">
                        <a href="{{ route('college.accommodation.index') }}">
                            <i class='fa fa-link'></i>&nbsp;Accommodation Applications
                        </a>
                    </li>
                @endif
                <li class="{{ activeMenu(['college.suspension.index']) }}">
                    <a href="{{ route('college.suspension.index') }}">
                        <i class='fa fa-link'></i>&nbsp;Suspensions Submitted
                    </a>
                </li>
                <li class="{{ activeMenu(['college.cards.index']) }}">
                    <a href="{{ route('college.cards.index') }}">
                        <i class='fa fa-link'></i>&nbsp;Student Cards
                    </a>
                </li>
            </ul>
        </li>
    @endif
    @if (collegeAllowed(1, 'd') || collegeAllowed(1, 'r') || collegeAllowed(1, '8') || collegeAllowed(1, '8') || collegeAllowed(1, '9'))
        <li class="treeview {{ activeMenu([
        'college.registration.index', 'college.reg.not-finished', 'college.reg.finished', 'college.reg.new.student',
        'college.reg.uploads.continuing.student', 'college.reg.uploads.uploaded','college.students.enrol', 'college.registration.show',
        'college.privates.index', 'college.admission.privates.finished'
        ]) }}">
            <a href="#">
                <i class='fa fa-diamond'></i>
                <span>Registration Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                @if (collegeAllowed(1, 'd') || collegeAllowed(1, 'r'))
                    <li class="{{ activeMenu(['college.privates.index', 'college.admission.privates.finished']) }}">
                        <a href="{{ route('college.privates.index') }}"><i
                                    class='fa fa-link'></i>&nbsp;<span>Private Students</span></a>
                    </li>
                @endif

                @if (collegeAllowed(1, 'd') || collegeAllowed(1, 'r') || collegeAllowed(1, '8') || collegeAllowed(1, '9'))
                    <li class="{{ activeMenu(['college.registration.index', 'college.registration.show']) }}">
                        <a href="{{ route('college.registration.index') }}"><i class='fa fa-link'></i>&nbsp;Registered
                            Students</a></li>
                    @if (collegeAllowed(1, 'd') || collegeAllowed(1, 'r'))
                        <li class="{{ activeMenu(['college.reg.not-finished']) }}">
                            <a href="{{ route('college.reg.not-finished') }}"><i class='fa fa-link'></i>&nbsp;Unregistered
                                Students</a>
                        </li>
                    @endif
                @endif
                {{--<li class="{{ activeMenu(['college.students.enrol']) }}" disabled="">--}}
                {{--<a href="{{ route('college.students.enrol') }}"><i class='fa fa-link'></i>&nbsp;Enrolled Students</a>--}}
                {{--</li>--}}
                {{--<li class="{{ activeMenu(['college.reg.finished']) }}">--}}
                {{--<a href="{{ route('college.reg.finished') }}"><i class='fa fa-link'></i>&nbsp;Finished Student</a></li>--}}
                {{--<li>&nbsp;</li>--}}
                {{--<li class="{{ activeMenu(['college.reg.new.student']) }}">--}}
                {{--<a href="{{ route('college.reg.new.student') }}">--}}
                {{--<i class='fa fa-link'></i>&nbsp;Add New Students--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li class="{{ activeMenu(['college.reg.new.student']) }}">--}}
                {{--<a href="{{ route('college.reg.new.student') }}">--}}
                {{--<i class='fa fa-link'></i>&nbsp;Add New Students--}}
                {{--</a>--}}
                {{--</li>--}}
                @if (collegeAllowed(1, 'd') || collegeAllowed(1, 'r'))
                    <li class="{{ activeMenu(['college.reg.uploads.continuing.student']) }}">
                        <a href="{{ route('college.reg.uploads.continuing.student') }}">
                            <i class='fa fa-link'></i>&nbsp;Upload Continuing Students
                        </a>
                    </li>
                    <li class="{{ activeMenu(['college.reg.uploads.uploaded']) }}">
                        <a href="{{ route('college.reg.uploads.uploaded') }}">
                            <i class='fa fa-link'></i> View&nbsp;Uploaded Students
                        </a>
                    </li>
                @endif
            </ul>
        </li>
    @endif

    @if (collegeAllowed(1, '8'))
        <li class="treeview {{ activeMenu([
        'college.lecturers.index', 'college.lecturers.create',
        'college.lecturers.edit', 'college.lecturers.uploading',
        'college.lecturers.assign',
        ]) }}">
            <a href="{{ route('college.lecturers.index') }}">
                <i class='fa fa-legal'></i>&nbsp;
                <span>Manage Staff Lecturers</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['college.lecturers.index']) }}">
                    <a href="{{ route('college.lecturers.index') }}">
                        <i class='fa fa-link'></i>
                        <span>Staff list</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['college.lecturers.assign']) }}">
                    <a href="{{ route('college.lecturers.assign') }}">
                        <i class='fa fa-link'></i>
                        <span>Geo Location</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif
    @if (collegeAllowed(1, '8'))
        <li class="treeview {{ activeMenu([
        'college.marks.upload','college.marks.other.upload',
        'college.marks.index', 'college.marks.processed.transcript',
        ]) }}">
            <a href="#">
                <i class='fa fa-magic'></i>
                <span>Manage Academic Results</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="treeview">
                    <a href="#">
                        <i class='fa fa-gear'></i>
                        <span>Settings</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="">
                            <a href="#">
                                <i class='fa fa-link'></i>&nbsp;<span>Classification scales</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class='fa fa-link'></i>&nbsp;<span>Active Semester</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="treeview {{ activeMenu([
                'college.marks.upload', 'college.marks.other.upload',
                'college.marks.index', 'college.marks.processed.transcript',
                ]) }}">
                    <a href="#">
                        <i class='fa fa-suitcase'></i>
                        <span>Students Marking</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview {{ activeMenu(['college.marks.upload', 'college.marks.other.upload']) }}">
                            <a href="{{ route('college.marks.upload')  }}">
                                <i class='fa fa-upload'></i>&nbsp;<span>Upload marks</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['college.marks.upload']) }}">
                                    <a href="{{ route('college.marks.upload')  }}">
                                        <i class='fa fa-link'></i>&nbsp;<span>Current Academic Year</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['college.marks.other.upload']) }}">
                                    <a href="{{ route('college.marks.other.upload')  }}">
                                        <i class='fa fa-link'></i>&nbsp;<span>Other Academic Year</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview {{ activeMenu(['college.marks.index', 'college.marks.processed.transcript',
                            'college.marks.edit', 'college.marks.view', 'college.marks.upload']) }}">
                            <a href="{{ route('college.marks.index') }}">
                                <i class='fa fa-database'></i>&nbsp;<span>Recorded Marks</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['college.marks.index']) }}">
                                    <a href="{{ route('college.marks.index') }}">
                                        <i class='fa fa-link'></i>&nbsp;View Marks
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['college.marks.processed.transcript']) }}">
                                    <a href="{{ route('college.marks.processed.transcript') }}">
                                        <i class='fa fa-link'></i>&nbsp;Process Transcript
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    @endif

    @if (collegeAllowed(1, '0') || collegeAllowed(1, '9'))
        <li class="treeview {{ activeMenu([
            'college.payments.category', 'college.payments.index', 'college.reg.paid',
            'college.payments.history', 'college.payments.override', 'college.payments.filtering',
            'college.payments.get.invoice.detail', 'college.payments.get.invoices',
            'college.payments.invoices', 'college.payments.history', 'college.payments.filtering', 'college.payments.post.filtering',
            'college.payments.outside'
        ]) }}">
            <a href="#">
                <i class='fa fa-money'></i>
                <span>Payments</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">

                {{--<li class="{{ activeMenu(['college.reg.paid']) }}">--}}
                {{--<a href="{{ route('college.reg.paid') }}">--}}
                {{--<i class='fa fa-link'></i>&nbsp;Paid Students--}}
                {{--</a>--}}
                {{--</li>--}}

                <li class="{{ activeMenu(['college.payments.history']) }}">
                    <a href="{{ route('college.payments.history') }}">
                        <i class='fa fa-link'></i>&nbsp;Payments History
                    </a>
                </li>
                <li class="{{ activeMenu(['college.payments.filtering', 'college.payments.post.filtering']) }}">
                    <a href="{{ route('college.payments.filtering') }}">
                        <i class='fa fa-link'></i>&nbsp;Payments Filtering
                    </a>
                </li>
                <li class="{{ activeMenu(['college.payments.override']) }}">
                    <a href="{{ route('college.payments.override') }}">
                        <i class='fa fa-link'></i>&nbsp;Payment Installments
                    </a>
                </li>
                {{--<li class="{{ activeMenu(['college.payments.outside']) }}">--}}
                {{--<a href="{{ route('college.payments.outside') }}">--}}
                {{--<i class='fa fa-link'></i>&nbsp;Payments Made Outside--}}
                {{--</a>--}}
                {{--</li>--}}
            </ul>
        </li>
    @endif

@endif