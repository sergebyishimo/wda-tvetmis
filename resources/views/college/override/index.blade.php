@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Payments Installments
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Payments Installments
    </div>
@endsection

@section('head_css')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        @include('feedback.feedback')
        <div class="box">
            {!! Form::open(['route' => 'college.payments.override.store']) !!}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::label('student_id', "Associated Students") !!}
                        {!! Form::select('student_id[]', $students, null, ['class' => 'form-control select2', 'multiple' => true]) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('installment_due_date', "Installment Due Date") !!}
                        {!! Form::date('installment_due_date', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('fee_category', "Payment Categories") !!}
                        {!! Form::select('fee_category', $categories, null, ['class' => 'form-control select2', 'placeholder' => 'Select category ...', 'required' => true]) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('installment_amount', "Installment Amount") !!}
                        {!! Form::number('installment_amount', null, ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('academic_year', "Academic Year") !!}
                        {!! Form::text('academic_year', date('Y - ').date('Y', strtotime("+1 year")), ['class' => 'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="col-md-1">
                        <div class="clearfix">&nbsp;</div>
                        {!! Form::submit('Save', ['class' => 'btn btn-block btn-primary']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="box">
            <div class="box-header">
                All Payments Installments
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped" id="dataTable">
                    <thead>
                    <tr>
                        <th width="50px" class="text-center">#</th>
                        <th>Assigned Student</th>
                        <th>Payment Categories</th>
                        <th>Installment Amount</th>
                        <th>Installment Due Date</th>
                        <th>Academic Year</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($installments)
                        @php
                            $x = 1;
                        @endphp
                        @foreach($installments as $installment)
                            <tr>
                                <td class="text-center">{{ $x++ }}</td>
                                <td>{{ $installment->student_id ? $installment->student ? $installment->student->names : "" : "ALL" }}</td>
                                <td>{{ $installment->category->category_name or "" }}</td>
                                <td>{{ number_format($installment->installment_amount) }} RWF</td>
                                <td>{{ date("F d, Y", strtotime($installment->installment_due_date)) }}</td>
                                <td>{{ $installment->academic_year }}</td>
                                <td>
                                    {!! Form::open(['route' => ['college.payments.override.destroy', $installment->id]]) !!}
                                    @method("DELETE")
                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
@endsection