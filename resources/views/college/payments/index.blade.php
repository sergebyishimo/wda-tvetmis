@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Set Payments Details
    </div>
@endsection

@section('head_css')
    <style>
        .table > thead > tr > td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                {!! Form::open(['route' => 'college.payments.store', 'method' => 'post', 'class' => 'form']) !!}
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-warning">
                                    <h4><i class="icon fa fa-info"></i>&nbsp;Warning</h4>

                                    <p>If there is unfilled or 0 in payment category means that it is not set, and if
                                        you want
                                        to unset just empty the box.</p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered" style="width: 200px !important;">
                                    <tr>
                                        <td class="bg-aqua-active">Academic Year</td>
                                        <td class="bg-info text-center">{!! date('Y') !!}</td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-condensed">
                                    @php($cg = $globalCategories->count())
                                    <thead>
                                    <tr>
                                        <th colspan="{{ $cg + 1 }}" class="text-center bg-aqua-active">Global Category
                                        </th>
                                    </tr>
                                    <tr class="bg-black-gradient">
                                        <th></th>
                                        @foreach($globalCategories as $globalCategory)
                                            <th style="vertical-align: middle !important;">{!! ucwords($globalCategory->name) !!}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Amount</td>
                                        @foreach($globalCategories as $globalCategory)
                                            <td>
                                                <div class="input-group">
                                                    {!! Form::number('amount['.$globalCategory->id.']',
                                                    getAmountPerCategory($globalCategory->id),
                                                    ['class' => 'form-control', 'aria-label' => 'active'.$globalCategory->id]) !!}
                                                    <span class="input-group-addon {{ getPaymentDetail($globalCategory->id,null,'active') == '1' ? 'bg-blue-active' : 'bg-blue' }}">
                                                        <input type="checkbox"
                                                               name="active[{{$globalCategory->id}}]"
                                                               value="1"
                                                               @if(getPaymentDetail($globalCategory->id,null,'active') == '1')
                                                               checked
                                                               @endif
                                                               aria-label="active{{$globalCategory->id}}">
                                                    </span>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered table-condensed">
                                    @php($cc = $collegeCategories->count())
                                    <thead class="bg-black-gradient">
                                    <tr>
                                        <th colspan="{{ $cc + 1 }}" class="text-center bg-aqua-active">Custom Category
                                        </th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        @foreach($collegeCategories as $ccategory)
                                            <th style="vertical-align: middle !important;">{!! ucwords($ccategory->name) !!}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td width="70px">Amount</td>
                                        @foreach($collegeCategories as $ccategory)
                                            <td>
                                                <div class="input-group">
                                                    {!! Form::number('amount['.$ccategory->id.']',
                                                        getAmountPerCategory($ccategory->id),
                                                    ['class' => 'form-control']) !!}
                                                    <span class="input-group-addon {{ getPaymentDetail($ccategory->id,null,'active') == '1' ? 'bg-blue-active' : 'bg-blue' }}">
                                                        <input type="checkbox"
                                                               name="active[{{$ccategory->id}}]"
                                                               value="1"
                                                               @if(getPaymentDetail($ccategory->id,null,'active') == '1')
                                                               checked
                                                               @endif
                                                               aria-label="active{{$ccategory->id}}">
                                                    </span>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button class="btn btn-primary btn-md">Save</button>
                    </div>
                </div>
                <!-- /.box -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
@show