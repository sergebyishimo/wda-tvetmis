@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        List Of Students Who Paid
    </div>
@endsection

@section('head_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>Student Reg Number</th>
                                <th>Student Name</th>
                                <th>Department</th>
                                <th>Course</th>
                                <th>Amount</th>
                                <th>Operator</th>
                                <th>Value Date</th>
                                <th>Operation Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php($i = 1 )
                                @foreach ($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->std_id }}</td>
                                        <td>{{ $payment->student ?  $payment->student->first_name . ' ' . $payment->student->other_names : '' }}</td>
                                        <td>{{ $payment->department ?  $payment->department->department_name : '' }}</td>
                                        <td>{{ $payment->course ?  $payment->course->program_name : '' }}</td>
                                        <td>{{ $payment->paid_maount }}</td>
                                        <td>{{ $payment->momo }}</td>
                                        <td>{{ $payment->year }}</td>
                                        <td>{{ date('d/m/Y', strtotime($payment->op_date)) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $payments->links() }}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable( {
                retrieve: true,
                paging: false,
            });
        });
    </script>
@show