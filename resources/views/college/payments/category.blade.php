@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Payment Historic
    </div>
@endsection

@section('head_css')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Enter Custom Category</h3>
                    </div>
                    {!! Form::open(['route' => 'college.payments.category.store', 'method' => 'post']) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', 'Category Name') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-md btn-primary" type="submit">Save</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box-body -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>Category Name</th>
                                <th>Active</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($categories as $category)
                                <tr>
                                    <td>{!! ucwords($category->name) !!}</td>
                                    <td>
                                        <label class="label {!! $category->active ? 'label-success' : 'label-warning' !!}">
                                            {!! $category->active ? 'Yes' : 'No' !!}
                                        </label>
                                    </td>
                                    <td>
                                        @if($category->active === 1)
                                            <a href="{{ route('college.payment.category.action', [$category->id, 'DISABLE']) }}"
                                               class="btn btn-sm btn-warning">Disable</a>
                                        @else
                                            <a href="{{ route('college.payment.category.action', [$category->id, 'ENABLE']) }}"
                                               class="btn btn-sm btn-primary">Enable</a>
                                        @endif
                                        <a href="{{ route('college.payment.category.action', [$category->id, 'DELETE']) }}"
                                           data-name="{{ $category->name }}"
                                           class="btn btn-danger btn-sm delete">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();

            $(".delete").on('click', function (e) {
                // e.preventDefault();
                let name = $(this).data('name');
                if (confirm("Are you sure to delete\n-- " + name)) {
                    return true;
                } else {
                    return false;
                }
            })
        });
    </script>
@show