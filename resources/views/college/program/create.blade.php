@extends('college.layout.main')

@section('panel-title', "Create College Program")

@section('htmlheader_title', "Create College Program")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            @if(isset($program))
                @php($route = ['college.programs.update', $program->id])
                @php($id = "updateProgram")
            @else
                @php($route = 'college.programs.store')
                @php($id = "createProgram")
            @endif
            {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
            @if(isset($program))
                @method("PATCH")
                {!! Form::hidden('id', $program->id) !!}
            @endif
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('department_id', 'Department  Name *', ['class' => 'label-control']) !!}
                                {!! Form::select('department_id',$departments->pluck('department_name','id'),$program ? $program->department_id : null ,
                                   ['placeholder' => 'Choose Department...', 'class' => 'form-control select2']) !!}
                                @if ($errors->has('department_id'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('department_id') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('program_name', 'Program Name *', ['class' => 'label-control']) !!}
                                {!! Form::text('program_name', $program ? $program->program_name : null,['class' => 'form-control']) !!}
                                @if ($errors->has('program_name'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('program_name') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('program_code', 'Program Code *', ['class' => 'label-control']) !!}
                                {!! Form::text('program_code', $program ? $program->program_code : null,['class' => 'form-control']) !!}
                                @if ($errors->has('program_code'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('program_code') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('program_load', 'Program Load *', ['class' => 'label-control']) !!}
                                {!! Form::text('program_load', $program ? $program->program_load : null,['class' => 'form-control']) !!}
                                @if ($errors->has('program_load'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('program_load') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('rtqf_level', 'Rtqf Level ', ['class' => 'label-control']) !!}
                                {!! Form::select('rtqf_level',$rtqf_level->pluck('level_name','id'),$program ? $program->rtqf_level: null,
                                    ['placeholder' => 'Choose RTQF LEVEL...', 'class' => 'form-control select2']) !!}
                                @if ($errors->has('rtqf_level'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('rtqf_level') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('program_is_stem', 'Program Is STEM? ', ['class' => 'label-control']) !!}
                                {!! Form::select('program_is_stem',['yes'=>"YES",'no'=>"NO"],$program ? $program->program_is_stem : null,
                                    ['placeholder' => 'IS STEM?...', 'class' => 'form-control select2']) !!}
                                @if ($errors->has('program_is_stem'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('program_is_stem') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('program_status', 'Program Status ', ['class' => 'label-control']) !!}
                                {!! Form::select('program_status',['active'=>"ACTIVE",'inactive'=>"INACTIVE"],$program ? $program->program_status : null,
                                    ['placeholder' => 'program status...', 'class' => 'form-control select2']) !!}
                                @if ($errors->has('program_status'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('program_status') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{--<div class="col-md-4">--}}
                        {{--<div class="form-group">--}}
                        {{--{!! Form::label('level_applied', 'Level Applied ', ['class' => 'label-control']) !!}--}}
                        {{--{!! Form::text('level_applied', $program ? $program->level_applied : null,['class' => 'form-control']) !!}--}}
                        {{--@if ($errors->has('level_applied'))--}}
                        {{--<span class="help-block text-red">--}}
                        {{--{{ $errors->first('level_applied') }}--}}
                        {{--</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('tuition_fees', 'Tuition Fees * ', ['class' => 'label-control']) !!}
                                {!! Form::text('tuition_fees', $program ? $program->tuition_fees : null,['class' => 'form-control']) !!}
                                @if ($errors->has('tuition_fees'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('tuition_fees') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('other_course_fees', 'Other Course Fees ', ['class' => 'label-control']) !!}
                                {!! Form::text('other_course_fees', $program ? $program->other_course_fees : null,['class' => 'form-control']) !!}
                                @if ($errors->has('other_course_fees'))
                                    <span class="help-block text-red">
                                        {{ $errors->first('other_course_fees') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('admission_requirements', 'Admission Requirements * ', ['class' => 'label-control']) !!}
                            {!! Form::textarea('admission_requirements',$program ? $program->admission_requirements : null, ['class' => 'form-control summernote']) !!}
                            @if ($errors->has('admission_requirements'))
                                <span class="help-block text-red">
                                        {{ $errors->first('admission_requirements') }}
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('attachment', 'Attachment * ', ['class' => 'label-control']) !!}
                            {!! Form::file('attachment',null, ['class' => 'form-control filer_docs_input']) !!}
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $program ? "Update Program" : "Save Program" }}
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('l-scripts')
@parent
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<!-- Laravel Javascript Validation -->
{{-- <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
@if($program)
    {!! JsValidator::formRequest('App\Http\Requests\RpProgramUpdateRequest', '#updateProgram'); !!}
@else
    {!! JsValidator::formRequest('App\Http\Requests\RpProgramRequest', '#createProgram'); !!}
@endif --}}
@show