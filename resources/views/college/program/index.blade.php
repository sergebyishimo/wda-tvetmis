@extends('college.layout.main')

@section('panel-title', "College Programs")

@section('htmlheader_title', "College Programs")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@if(isset($details) && $details == true)
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ route('college.programs.index') }}" class="btn btn-primary btn-sm">back</a>
                        <center><h3 class="box-title text-primary">Details Information of program</h3></center>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <tr>
                                <th class="text-green col-md-4">Program Name</th>
                                <td style="font-weight: bold;" class="col-md-8">{{ $program->program_name }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Code</th>
                                <td>{{ $program->program_code }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Load</th>
                                <td>{{ $program->program_load }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Rtqf Level</th>
                                <td>{{ $program->rtqflevel->rtqf_level  or ""}}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Is It STEM ?</th>
                                <td>{{ $program->program_is_stem }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Status</th>
                                <td>{{ $program->program_status }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Level Applied</th>
                                <td>{{ $program->level_applied }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Tuition Fees</th>
                                <td>{{ $program->tuition_fees }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Other Course Fees</th>
                                <td>{{ $program->other_course_fees }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Admission Requirements</th>
                                <td>{{ $program->admission_requirements }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Attachment</th>
                                <td><a href="{{ asset('storage/'.$program->attachment) }}">View Attachment</a></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <center><h3 class="box-title text-primary">Course Units In <span
                                        class="text-green">{{$program->program_name}}</span></h3></center>
                        <div class="list-group list-group-horizontal pull-right">
                            <a href="#"
                               class="list-group-item active">
                                Create a Course Unit</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <tr>
                                <th>Course Unit Code</th>
                                <th>Course Unit Name</th>
                                <th>Credit Unit</th>
                                <th>Level Of Study</th>
                                <th>Semester Id</th>
                                <th>Unit Class Id</th>
                            </tr>
                            @if ($program->courseunits)
                                @foreach($program->courseunits as $courseunit)
                                    <tr>
                                        <td>
                                            {{ $courseunit->course_unit_code }}
                                        </td>
                                        <td>
                                            <h5>
                                                {{ $courseunit->course_unit_name }}
                                            </h5>
                                        </td>
                                        <td>
                                            {{ $courseunit->credit_unit }}
                                        </td>
                                        <td>
                                            {{ $courseunit->levelofstudy->level_of_study or "" }}
                                        </td>
                                        <td>
                                            {{ $courseunit->semester->semester  or ""}}
                                        </td>
                                        <td>
                                            {{ $courseunit->unitclass->course_unit_class or ""}}
                                        </td>
                                        <td>
                                        </td>
                                        <th>
                                            <a href="{{ route('college.courseunits.edit', $courseunit->id) }}"
                                            class="btn btn-info btn-sm">Edit</a>
                                            <button type="button"
                                                    data-url="{{ route('college.courseunits.destroy', $courseunit->id) }}"
                                                    data-names="{{ $courseunit->id }}"
                                                    class="btn btn-danger btn-sm btn-delete">
                                                Delete
                                            </button>
                                        </th>
                                    </tr>
                                @endforeach    
                            @endif
                            
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@else
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
            <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3> <span class="pull-right"><a
                                    href="{{ route('college.programs.create') }}" class="btn btn-primary btn-block">Add Program</a></span>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Department</th>
                                <th>Program Name</th>
                                <th>Program Code</th>
                                <th>Tuition Fees</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($programs as $program)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>
                                        <a href="{{ route('college.programs.index',['o_o'=>$program->id]) }}">{{ $program->program_name }}</a>
                                    </td>
                                    <td>{{ $program->department->department_name }} </td>
                                    <td>{{ $program->program_code }}</td>
                                    <td>{{ $program->tuition_fees }}</td>
                                    <td>
                                        <a href="{{ route('college.programs.edit', $program->id) }}"
                                           class="btn btn-primary btn-block " style="margin-bottom: 5px;">Edit</a>
                                        <form action="{{ route('college.programs.destroy', $program->id) }}"
                                              method="post">
                                            @method("DELETE")
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@endif
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@show