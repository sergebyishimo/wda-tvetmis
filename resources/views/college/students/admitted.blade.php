@extends('college.layout.main')

@section('panel-title', "Admitted Students : " . App\Rp\AdmittedStudent::where('college_id', college('college')->id)->count())

@section('htmlheader_title', "Admitted Students")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection


@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <form method="GET" action="{{ route('college.students.admitted')  }}">
                            <div class="row">

                                @if(auth()->guard('college')->check())
                                    <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                @endif
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Department</label>
                                        <select name="department_id" id="nDepartment" class="form-control">

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Option</label>
                                        <select name="option_id" id="nOption" class="form-control">
                                            @if (isset($options))
                                                <option value="">Choose Option</option>
                                                @foreach ($options as $option)
                                                    <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <br>
                                    <button type="submit" class="btn btn-md btn-primary pull-left">
                                        View
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="box-body" style="overflow: auto">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')

                                <table class="table table-striped table-bordered" id="qwerty">
                                    <thead>
                                        <tr>
                                            <th width="100px" class="text-center">Photo</th>
                                            <th width="160px">Student Reg Number</th>
                                            <th>Student Name</th>
                                            <th>Department</th>
                                            <th>Course</th>
                                            <th width="60px">Aggregates</th>
                                            <th>Sponsorship</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($students) > 0)
                                            @php($i = 1 )
                                            @foreach($students as $student)
                                                <tr>
                                                    <td align="center">
                                                        <img src="{{ regGetStudentPhoto($student) }}" alt="{{ $student->names }}"
                                                                style="width: 5em;height: 5em;box-shadow: 0px 0px 10px #8d9499"
                                                                class="img-circle img-responsive">
                                                    </td>
                                                    <td>{{ $student->std_id }}</td>
                                                    <td>{{ $student->names }}</td>
                                                    <td>{{ $student->department->department or "" }}</td>
                                                    <td>{{ $student->course->choices or "" }}</td>
                                                    <td>{{ $student->aggregates_obtained }}</td>
                                                    <td>{{ ucwords($student->sponsorship_status == '.' ? 'government' : $student->sponsorship_status) }}</td>
                                                    <td>
                                                        @if(auth()->guard('college')->check())
                                                            <a href="{{ route('college.registration.show', strtolower($student->student_reg)) }}"
                                                                class="btn btn-primary btn-sm pull-right">See More</a>
                                                            <a href="{{ route('college.registration.edit', strtolower($student->std_id)) }}"
                                                                    class="btn btn-success btn-sm pull-right">Edit</a>
                                                        @elseif(auth()->guard('rp')->check())
                                                            <a href="{{ route('rp.registration.show', strtolower($student->student_reg)) }}"
                                                                class="btn btn-primary btn-sm pull-right">See More</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    @if (count($students) ==0 )
                                        {{--<tfoot>--}}
                                        {{--<tr>--}}
                                            {{--<th width="30px" class="text-center">#</th>--}}
                                            {{--<th width="100px" class="text-center">Photo</th>--}}
                                            {{--<th width="160px">Student Reg Number</th>--}}
                                            {{--<th>Student Name</th>--}}
                                            {{--<th>Department</th>--}}
                                            {{--<th>Course</th>--}}
                                            {{--<th width="60px">Aggregates</th>--}}
                                            {{--<th>Sponsorship</th>--}}
                                            {{--<th></th>--}}
                                        {{--</tr>--}}
                                        {{--</tfoot>--}}
                                    @endif
                                </table>
                                {{--{{ $students->links() }}--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    @if (count($students) == 0)
        <script>
            $(document).ready(function () {
                // $('#qwerty').DataTable();
                /*$('#qwerty').DataTable({
                    destroy: true,
                    dom: 'Blfrtip',
                    serverSide: true,
                    processing: true,
                    pager: true,
                    colReorder: true,
                    ajax: '/college/data/admittedStudents/ss',
                    "oSearch": {"bSmart": false},
                    columns: [
                        {data: 'photo', orderable:false, searchable:false},
                        {data: 'student_reg_number', name: 'admitted_students.std_id', orderable:true, searchable:true},
                        {data: 'student_name', name: 'admitted_students.first_name', orderable:true, searchable:true},
                        {data: 'department', orderable:false, searchable:false},
                        {data: 'course', orderable:false, searchable:false},
                        {data: 'aggregates', name: 'admitted_students.aggregates_obtained', orderable:true, searchable:true},
                        {data: 'sponsorship_status', name: 'admitted_students.sponsorship_status', orderable:true, searchable:true},
                        {data: 'action', orderable: false, searchable: false}
                    ],
                    buttons: [
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column',
                            postfixButtons: ['colvisRestore']
                        },
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    // initComplete: function () {
                    //     var x = 0;
                    //     var lenC = this.api().columns().length;
                    //     this.api().columns().every(function () {
                    //         if (x > 0 ){
                    //             var column = this;
                    //             var select = $('<select class="form-control"><option value=""></option></select>')
                    //                 .appendTo($(column.footer()).empty())
                    //                 .on('change', function () {
                    //                     var val = $.fn.dataTable.util.escapeRegex(
                    //                         $(this).val()
                    //                     );
                    //
                    //                     column
                    //                         .search(val ? '^' + val + '$' : '', true, false)
                    //                         .draw();
                    //                 });
                    //
                    //             column.data().unique().sort().each(function (d, j) {
                    //                 select.append('<option value="' + d + '">' + d + '</option>')
                    //             });
                    //         }
                    //         x++;
                    //     });
                    // }
                })*/
            });
        </script>
    @else
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#qwerty').DataTable({
                    destroy: true,
                    dom: 'Blfrtip',
                    "oSearch": {"bSmart": false},
                    buttons: [
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column',
                            postfixButtons: ['colvisRestore']
                        },
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    pager: true,
                    initComplete: function () {
                        var x = 0;
                        var lenC = this.api().columns().length;
                        this.api().columns().every(function () {
                            if (x > 1 ){
                                var column = this;
                                var select = $('<select class="select2"><option value=""></option></select>')
                                    .appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function (d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                });
                            }
                            x++;
                        });
                    }
                });
            });
        </script>
    @endif

    <script>
        $(function() {

            if($("#year").val() === '1') {
                var id = $("#aCollege").val();
                getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
            }

            $("#nProgram").change(function() {
                if ($("#year").val() !== '1') {
                    var program = $(this).val();
                    $("#nDepartment").html('');
                    $.get('/college/data/departments/' + program, function(data,status) {
                        $("#nDepartment").html('<option value="">Choose Department</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                        }
                    });
                }
            });

            $("#nDepartment").change(function() {
                if ($("#year").val() === '1') {
                    var id = $(this).val();
                    var cl = $("#aCollege").val();
                    getingOPtions(cl, "/poly/c/"+id+"/", $("#nOption"), 'c');
                } else {
                    var program = $(this).val();
                    $("#nOption").html('');
                    $.get('/college/data/options/' + program, function(data,status) {
                        $("#nOption").html('<option value="">Choose Option</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                        }
                    });
                }

            });

            function getingOPtions(id, url, elem, val, cl) {
                $.get(url + id, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select ...</option>";
                    $.each(obj, function (key, value) {
                        var v = "";
                        var a = "";
                        if (val == 'd')
                            v = value.department_name;
                        if (val == 'c') {
                            v = value.program_name;
                            var cd = value.choice_code;
                            var aa = value.study_area;
                            if (cd != undefined){
                                if (cd.length > 0)
                                    a = " ( " + cd + " ) ";
                            }
                        }
                        if (val == "m")
                            v = value.course_unit_name;
                        html += "<option value='" + value.id + "'>" + v + a + "</option>";
                    });
                    elem.html(html);
                });
            }
        });
    </script>
@show