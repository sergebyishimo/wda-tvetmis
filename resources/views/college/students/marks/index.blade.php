@extends('college.layout.main')

@section('panel-title', "View Students Marks ")

@section('htmlheader_title', "View Students Marks")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['method' => 'get']) !!}
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Program</label>
                                            <select name="program_id" id="nProgram" class="form-control" >
                                                <option value="">Choose Program</option>
                                                <option value="1" @if(request()->program_id == 1) selected @endif >Diploma (Level 6)</option>
                                                <option value="2" @if(request()->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Department</label>
                                            <select name="department_id" id="nDepartment" class="form-control" >
                                                @if (isset($departments))
                                                    @foreach ($departments as $department)
                                                        <option value="{{ $department->id }}" @if(request()->department_id == $department->id) selected @endif >{{ $department->department_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Option</label>
                                            <select name="option_id" id="nOption" class="form-control" required>
                                                @if (isset($options))
                                                    <option value="">Choose Option</option>
                                                    @foreach ($options as $option)
                                                        <option value="{{ $option->id }}" @if(request()->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                    @endforeach    
                                                @endif
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Year Of Study</label>
                                            <select name="year" id="" class="form-control" required>
                                                <option value="1" @if(request()->year == 1) selected @endif >First Year</option>
                                                <option value="2" @if(request()->year == 2) selected @endif >Second Year</option>
                                                <option value="3" @if(request()->year == 3) selected @endif >Third Year</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {!! Form::label('academic_year', "Academic Year", ['class' => 'control-label']) !!}
                                            {!! Form::select('academic_year', academicYear(), request()->academic_year, ['class' => 'form-control select2']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <div>&nbsp;</div>
                                            {!! Form::submit('View Marks',['class' => 'btn btn-md btn-primary pull-right']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="box-body" style="overflow:auto">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        @if (isset($one) && isset($two))
                            
                        
                        <table class="table table-bordered  table-striped table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th colspan="{{ count($one) }}" class="text-center" >Semester 1</th>
                                    <th colspan="{{ count($two) }}" class="text-center" >Semester 2</th>
                                    <th> Sem 1 & 2 </th>
                                    <th> Failed Modules </th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Module Codes</th>
                                    @php($modules_arr = [])
                                    @foreach ($one as $module)
                                        @php($modules_arr[] = $module->module_id )
                                        @php($model = \App\Rp\CollegeModule::find($module->module_id) )
                                        <th title="{{ $model->module_name }}">{{ $model->module_code }}</th>
                                    @endforeach
                                    @foreach ($two as $module)
                                        @php($modules_arr[] = $module->module_id)
                                        @php($model = \App\Rp\CollegeModule::find($module->module_id) )
                                        <th title="{{ $model->module_name }}">{{ $model->module_code }}</th>
                                    @endforeach
                                    <th></th>
                                    <th></th>
                                    
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Max.</th>
                                    @foreach ($modules_arr as $item)
                                        <th>100</th>
                                    @endforeach
                                    <th></th>
                                    <th></th>
                                    
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Pass.</th>
                                    @foreach ($modules_arr as $item)
                                        <th>50</th>
                                    @endforeach
                                    <th></th>
                                    <th></th>
                                    
                                </tr>
                                <tr>
                                    <th>Names</th>
                                    <th>New. Reg.</th>
                                    <th>#Credits.</th>
                                    @foreach ($modules_arr as $item)
                                        <th>{{ App\Rp\CollegeModule::find($item)->credits }}</th>
                                    @endforeach
                                    <th></th>
                                    <th></th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($students as $student)
                                    @php($credits_count = 0)
                                    @php($failed_modules = [])
                                    <tr>
                                        <td>{{ $student->student ? $student->student->first_name . ' ' . $student->student->last_name : '' }}</td>
                                        <td>{{ $student->student ? $student->student->student_reg : '' }}</td>
                                        <td>{{ $student->student ? $student->student->registration_number : '' }}</td>
                                        @foreach ($one as $module)
                                            @php($marks = \App\Model\Rp\StudentMarks::where('college_id', college('college')->id)->where('academic_year', request()->academic_year)->where('module_id', $module->module_id)->where('semester', 1)->where('registration_number', $student->registration_number)->where('retake', null)->first())
                                            @php($module_info = \App\Rp\CollegeModule::find($module->module_id))
                                            

                                            @if ($marks)
                                                @php($real_marks = $marks->obtained_marks)

                                                @if($real_marks >= 50)
                                                    @php($credits_count += $module_info->credits)
                                                @else 
                                                    @php($failed_modules[$module_info->module_code] = $module_info->module_name)
                                                @endif

                                                <td>{{ number_format($real_marks, 1) }}</td>
                                            @else
                                                @php($real_marks = '-')
                                                <td>{{ $real_marks }}</td>
                                            @endif

                                            
                                        @endforeach
                                        @if (count($one) == 0)
                                            <td></td>
                                        @endif
                                        @foreach ($two as $module)
                                            @php($marks = \App\Model\Rp\StudentMarks::where('college_id', college('college')->id)->where('academic_year', request()->academic_year)->where('module_id', $module->module_id)->where('semester', 2)->where('registration_number', $student->registration_number)->where('retake', null)->first())
                                            @php($module_info = \App\Rp\CollegeModule::find($module->module_id))
                                            

                                            @if ($marks)
                                                @php($real_marks = $marks->obtained_marks)
                                                @if($real_marks >= 50)
                                                    @php($credits_count += $module_info->credits)
                                                @else 
                                                    @php($failed_modules[$module_info->module_code] = $module_info->module_name)
                                                @endif

                                                <td>{{ number_format($real_marks, 1) }}</td>
                                            @else
                                                @php($real_marks = '-')
                                                <td>{{ $real_marks }}</td>
                                            @endif

                                            
                                        @endforeach
                                        @if (count($two) == 0)
                                            <td></td>
                                        @endif
                                        <td>{{ $credits_count }}</td>
                                        <td> 
                                            @if (count($failed_modules) > 0)
                                                <button type="button" class="btn btn-danger" title=" @foreach ($failed_modules as $key => $value) {{ $key . ' => ' . $value }}, @endforeach ">Failed Modules</button>    
                                            @endif
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $students->links() }}
                        @endif
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                responsive: true,
                "scrollX": true,
            });
        });
    </script>
    <script>

        $(function() {
            $("#nProgram").change(function() {
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program, function(data,status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                    }
                });
            });
        
            $("#nDepartment").change(function() {
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program, function(data,status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                    }
                });
            });

            $("#nOption").change(function() {
                var option = $(this).val();
                $("#nModule").html('');
                $.get('/college/data/modules/' + option, function(data, status) {
                    for (i = 0; i < data.length; i++) {
                        $("#nModule").append('<option value='+ data[i].id +'>'+ data[i].module_name +'</option>');
                    }
                });
            });
        });
        
    </script>
@show
