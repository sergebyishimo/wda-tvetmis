@extends('college.layout.main')

@section('panel-title', "Transcript")

@section('htmlheader_title', "transcript")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="panel">
            <div class="panel-body">
                {!! Form::open(['route' => 'college.marks.processed.post.transcript']) !!}
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Year Of Study</label>
                            <select name="year" id="nYearOfStudy" class="form-control select2" style="width: 100%;"
                                    required>
                                <option value="">Select ...</option>
                                <option value="1"
                                        @if(isset($module) &&$module->year_of_study == 1) selected @endif >First
                                    Year
                                </option>
                                <option value="2"
                                        @if(isset($module) &&$module->year_of_study == 2) selected @endif >
                                    Second Year
                                </option>
                                <option value="3"
                                        @if(isset($module) &&$module->year_of_study == 3) selected @endif >Third
                                    Year
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="keypoints">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Program</label>
                                <select name="program_id" id="nProgram" class="form-control select2"
                                        style="width: 100%;">
                                    <option value="">Choose Program</option>
                                    <option value="1"
                                            @if(isset($department) && $department->program_id == 1) selected @endif >
                                        Diploma (Level 6)
                                    </option>
                                    <option value="2"
                                            @if(isset($department) && $department->program_id == 2) selected @endif >
                                        Advanced Diploma (Level 7)
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Department</label>
                                <select name="department_id" id="nDepartment" class="form-control select2"
                                        style="width: 100%;" required>
                                    <option value="">Choose Program</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Option</label>
                                <select name="option_id" id="nOption" class="form-control select2" required
                                        style="width: 100%;">
                                    @if (isset($options))
                                        <option value="">Choose Option</option>
                                        @foreach ($options as $option)
                                            <option value="{{ $option->id }}"
                                                    @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Students</label>
                                <select name="students" id="nStudents" class="form-control select2"
                                        style="width: 100%;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('academic_year', "Academic Year", ['class' => 'control-label']) !!}
                            {!! Form::select('academic_year', academicYear(2008, date("Y",strtotime("-1 year"))), null, ['class' => 'form-control select2', 'placeholder' => 'select ...', 'required' => true]) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('semester', "Semester", ['class' => 'control-label']) !!}
                            {!! Form::select('semester', [1 => 1, 2 => 2, 3 => 'All'], null, ['class' => 'form-control select2', 'placeholder' => 'select ...', 'required' => true]) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-sm btn-primary pull-left" style="margin-top: 26px;">
                            View
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @isset($view)
            <div class="panel">
                <div class="panel-body">
                    @include($view)
                </div>
            </div>
        @endisset
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script>
        $(function () {
            $(".keypoints").hide();
            $("#nYearOfStudy").on("change", function () {
                $(".keypoints").fadeIn(555);
                $("*.keypoints select").val("").trigger('change');
            });
            $("#nProgram").change(function () {
                let year = $("#nYearOfStudy").val();
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program + "?y=" + year, function (data, status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value=' + data[i].id + '>' + data[i].department_name + '</option>');
                    }
                });
            });
            $("#nDepartment").change(function () {
                let year = $("#nYearOfStudy").val();
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program + "?y=" + year, function (data, status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    if (year === '1') {
                        let o = $.parseJSON(data);
                        $.each(o, function (i, item) {
                            $("#nOption").append('<option value=' + item.id + '>' + item.program_name + '</option>');
                        });
                    } else {
                        for (i = 0; i < data.length; i++) {
                            $("#nOption").append('<option value=' + data[i].id + '>' + data[i].option_name + '</option>');
                        }
                    }
                });
            });
            $("#nOption").on("change", function () {
                let program = $(this).val();
                let year = $("#nYearOfStudy").val();
                $.get('/college/data/students/' + program + "?y=" + year, function (data, status) {
                    $("#nStudents").html('<option value="">Select ...</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nStudents").append('<option value=' + data[i].student_reg + '>( '+ data[i].student_reg + ' ) ' + data[i].first_name.toUpperCase() + ' ' + data[i].other_names.toUpperCase() + '</option>');
                    }
                });
            });
        });
    </script>
@show
