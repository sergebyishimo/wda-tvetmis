@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        Upload Marks
    </div>
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
          integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @include('feedback.feedback')
                <!-- Default box -->
                <div class="box">
                    {{-- <div class="box-header with-border">
                        <a href="{{ asset('files/students_upload_marks_format.xlsx') }}" target="_blank">Download Excel Format</a>
                        <a href="{{ route('college.marks.download') }}" target="_blank">Download Excel Format</a>
                    </div> --}}
                    <div class="box-body">
                        {!! Form::open(['id' => 'upload', 'route' => 'college.marks.store.marks', 'files' => true]) !!}
                        @method('POST')
                        <div class="row">
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                    {!! Form::label('aDepartment', "Department", ['class' => 'control-label']) !!}
                                    {!! Form::select('department', $departments, null, [
                                    'class' => 'form-control department select2',
                                    'id' => 'aDepartment',
                                    'placeholder' => 'Choose Department'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('aProgram', "Program", ['class' => 'control-label']) !!}
                                    {!! Form::select('program', [], null, ['class' => 'form-control program select2', 'id' => 'aProgram', 'placeholder' => 'Select Program']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('aModule', "Module", ['class' => 'control-label']) !!}
                                    {!! Form::select('module', [], null, ['class' => 'form-control module select2', 'id' => 'aModule', 'placeholder' => 'Select Module']) !!}
                                </div>
                            </div> --}}
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Program</label>
                                        <select name="program_id" id="nProgram" class="form-control" >
                                            <option value="">Choose Program</option>
                                            <option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>
                                            <option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Department</label>
                                        <select name="department_id" id="nDepartment" class="form-control" >
                                            {{-- <option value="">Choose Option</option>
                                            @foreach ($options as $option)
                                                <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                            @endforeach --}}
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Option</label>
                                        <select name="option_id" id="nOption" class="form-control" required>
                                            @if (isset($options))
                                                <option value="">Choose Option</option>
                                                @foreach ($options as $option)
                                                    <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                @endforeach    
                                            @endif
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Year Of Study</label>
                                        <select name="year" id="" class="form-control" required>
                                            <option value="1" @if(isset($module) &&$module->year_of_study == 1) selected @endif >First Year</option>
                                            <option value="2" @if(isset($module) &&$module->year_of_study == 2) selected @endif >Second Year</option>
                                            <option value="3" @if(isset($module) &&$module->year_of_study == 3) selected @endif >Third Year</option>
                                        </select>
                                    </div>
                                </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('academic_year', "Academic Year", ['class' => 'control-label']) !!}
                                    {!! Form::select('academic_year', academicYear(2008, date("Y",strtotime("-1 year"))), null, ['class' => 'form-control select2', 'placeholder' => 'select ...', 'required' => true]) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('file', "File", ['class' => 'control-label']) !!}
                                    {!! Form::file('file',['class' => 'filer_docs_input_excel']) !!}
                                    {{-- <div class="helper">
                                        <span class="text-muted text-sm">
                                            <i class="fa fa-info-circle"></i>
                                            Please, 500 students is the maximum to upload at once.
                                        </span>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div>&nbsp;</div>
                                    {!! Form::submit('Download Template',['class' => 'btn btn-block btn-md btn-success pull-right', 'name' => 'download']) !!}
                                    {!! Form::submit('Upload',['class' => 'btn btn-block btn-md btn-primary pull-right']) !!}
                                    
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                @if(session()->has('uploaded'))
                    <div class="box box-success">
                        <div class="box-header with-border">
                            Uploaded Students
                        </div>
                        <div class="box-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>Registration Number</td>
                                    <td>Marks</td>
                                    <td>Academic Year</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count(session()->get('uploaded')))
                                    @foreach(session()->get('uploaded') as $student)
                                        <tr>
                                            <td>{{ $student['registration_number'] }}</td>
                                            <td>{{ $student['marks'] }}</td>
                                            <td>{{ $student['academic_year'] }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: xlsx");
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>

        $(function() {
            $("#nProgram").change(function() {
                var program = $(this).val();
                $("#nDepartment").html('');
                $.get('/college/data/departments/' + program, function(data,status) {
                    $("#nDepartment").html('<option value="">Choose Department</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                    }
                });
            });
        
            $("#nDepartment").change(function() {
                var program = $(this).val();
                $("#nOption").html('');
                $.get('/college/data/options/' + program, function(data,status) {
                    $("#nOption").html('<option value="">Choose Option</option>');
                    for (i = 0; i < data.length; i++) {
                        $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                    }
                });
            });
        });
        
    </script>

    {{-- <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CollegeStudentsMarksUploadRequest', '#upload'); !!} --}}
@show