<div class="row">
    <div class="col-md-4">
        <table class="table">
            <thead>
            <tr>
                <td width="80px">REG N<sup>o</sup></td>
                <td>: {{ $students->student_reg }}</td>
            </tr>
            <tr>
                <td width="80px">Names</td>
                <td>: {{ $students->names }}</td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="col-md-8">
        <table class="table">
            <thead>
            <tr>
                <td>DEPARTMENT</td>
                <td>
                    : {{ ucwords(strtolower(getDepartment($students->department_id, 'department_name', isContinuingStudent($students->student_reg)))) }}</td>
            </tr>
            <tr>
                <td>OPTION</td>
                <td>
                    : {{ ucwords(strtolower(getCourse($students->course_id, 'program_name', isContinuingStudent($students->student_reg)))) }}</td>
            </tr>
            <tr>
                <td>YEAR OF STUDY</td>
                <td>: {{ $yearOfStudy }}</td>
            </tr>
            @isset($semester)
                <tr>
                    <td>SEMESTER</td>
                    <td>: {{ $semester }}</td>
                </tr>
            @endisset
            <tr>
                <td>ACADEMIC YEAR</td>
                <td>: {{ $academicYear }}</td>
            </tr>
            </thead>
        </table>
    </div>
    <h5 class="text-center">ACADEMIC TRANSCRIPT</h5>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SN</th>
                <th>SUBJECT CODE</th>
                <th>SUBJECT NAME</th>
                <th class="text-center">CREDIT UNIT</th>
                <th class="text-center">MARKS (%)</th>
                <th class="text-center">STATUS</th>
            </tr>
            </thead>
            <tbody>
            @php
                $x = 0;
                $totalCU = 0;
                $totalMK = 0;
            @endphp
            @if($modules->count())
                @foreach($modules as $module)
                    @php
                        $marks = $getMarks->where('module_id', $module->id)->sum('obtained_marks');
                        $totalCU += $module->credits;
                        $totalMK += $marks;
                    @endphp
                    <tr>
                        <td>{{ ++$x }}</td>
                        <td>{{ ucwords($module->module_code) }}</td>
                        <td>{{ ucwords($module->module_name) }}</td>
                        <td class="text-center">{{ ucwords($module->credits) }}</td>
                        <td class="text-center">{{ $marks }}</td>
                        <td class="text-center">{{ $marks >= 50 ? 'PASS' : 'FAIL'  }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">No modules much with the condition ...</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table>
            <tr>
                <td>Total Credit at this level</td>
                <td>: <b>{{ $totalCU }}</b></td>
            </tr>
            <tr>
                @php
                    $avg = 0;
                    if ($totalMK && $x)
                        $avg = ceil($totalMK / $x)
                @endphp
                <td>Annual Average (%)</td>
                <td>: <b>{{ $avg }}</b></td>
            </tr>
            <tr>
                <td>Classification</td>
                <td>: <b>{{ getClassification($avg) }}</b></td>
            </tr>
            <tr>
                <td>Verdict</td>
                <td>: <b>{{ $avg >= 50 ? 'PASS' : 'FAIL'  }}</b></td>
            </tr>
        </table>
    </div>
</div>