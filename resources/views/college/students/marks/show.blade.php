@extends('college.layout.main')

@section('panel-title', "View Students Marks")

@section('htmlheader_title', "View Students Marks")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['method' => 'get']) !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                            {!! Form::label('aDepartment', "Department", ['class' => 'control-label']) !!}
                                            {!! Form::select('department', $departments, null, [
                                            'class' => 'form-control department select2',
                                            'id' => 'aDepartment',
                                            'placeholder' => 'Choose Department'
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('aProgram', "Program", ['class' => 'control-label']) !!}
                                            {!! Form::select('program', [], null, ['class' => 'form-control program select2', 'id' => 'aProgram', 'placeholder' => 'Select Program']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('aModule', "Module", ['class' => 'control-label']) !!}
                                            {!! Form::select('module', [], null, ['class' => 'form-control module select2', 'id' => 'aModule', 'placeholder' => 'Select Module']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {!! Form::label('academic_year', "Academic Year", ['class' => 'control-label']) !!}
                                            {!! Form::select('academic_year', academicYear(), date('Y', strtotime('-1 year')), ['class' => 'form-control select2']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <div>&nbsp;</div>
                                            {!! Form::submit('Search',['class' => 'btn btn-md btn-primary pull-right']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th width="30px" class="text-center">#</th>
                                <th width="100px" class="text-center">Photo</th>
                                <th width="160px">Reg Number</th>
                                <th>Names</th>
                                <th>Department</th>
                                <th>Program</th>
                                <th>Marks</th>
                                <th width="60px">Study Year</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show