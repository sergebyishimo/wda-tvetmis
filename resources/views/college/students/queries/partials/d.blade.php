@if($model == 'u')
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>StudentID</th>
            <th>Names</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>NationalID</th>
            <th>Department</th>
            <th>Program</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $student }}</td>
            <td>{{ getStudentInfo($student,'names') }}</td>
            <td>{{ getStudentInfo($student,'email') }}</td>
            <td>{{ getStudentInfo($student,'phone') }}</td>
            <td>{{ getStudentInfo($student,'national_id_number') }}</td>
            <td>{{ getStudentInfo($student,'department_id') }}</td>
            <td>{{ getStudentInfo($student,'course_id') }}</td>
            <td>{{ getStudentInfo($student,'created_at') }}</td>
        </tr>
        </tbody>
    </table>
    @php
        $deleting = [];
    @endphp
    @if(getStudentInvoices($student))
        <table style="margin-top: 20px;" class="table table-bordered">
            <thead>
            <th>Invoice Code</th>
            <th>Payment Name</th>
            <th>Total Amount</th>
            <th>Total Paid</th>
            </thead>
            <tbody>
            @foreach(getStudentInvoices($student, 'a') as $invoice)
                <tr>
                    <td>{{ $invoice->code }}</td>
                    <td>{{ $invoice->name }}</td>
                    <td>{{ number_format($invoice->amount) }}</td>
                    <td>{{ number_format($invoice->paid) }}</td>
                </tr>
                @if($invoice->paid > 0)
                    @php
                        $deleting[] = $invoice->paid;
                    @endphp
                @endif
            @endforeach
            </tbody>
        </table>
    @endif
    @if(count($deleting) <= 0 && ! applied($student) && ! paidOutSide($student) && ! sponsored($student))
        @if(auth()->guard('rp')->check())
            {!! Form::open(['route' => 'rp.query.execute']) !!}
        @elseif(auth()->guard('college')->check())
            {!! Form::open(['route' => 'college.query.execute']) !!}
        @endif
        {!! Form::hidden('execute', $action) !!}
        {!! Form::hidden('student', $student) !!}
        {!! Form::hidden('model', $model) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger mt-3']) !!}
        {!! Form::close() !!}
    @endif
@endif