@if($model == 'r')
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>StudentID</th>
            <th>Names</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>NationalID</th>
            <th>Department</th>
            <th>Program</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $student }}</td>
            <td>{{ getStudentInfo($student,'names') }}</td>
            <td>{{ getStudentInfo($student,'email') }}</td>
            <td>{{ getStudentInfo($student,'phone') }}</td>
            <td>{{ getStudentInfo($student,'national_id_number') }}</td>
            <td>{{ getStudentInfo($student,'department_id') }}</td>
            <td>{{ getStudentInfo($student,'course_id') }}</td>
            <td>{{ getStudentInfo($student,'created_at') }}</td>
        </tr>
        </tbody>
    </table>
    @php
        $deleting = [];
    @endphp

    @if(count($deleting) <= 0 && ! applied($student) && ! paidOutSide($student) && ! sponsored($student))
        @if(auth()->guard('rp')->check())
            {!! Form::open(['route' => 'rp.query.execute']) !!}
        @elseif(auth()->guard('college')->check())
            {!! Form::open(['route' => 'college.query.execute']) !!}
        @endif
        {!! Form::hidden('execute', $action) !!}
        {!! Form::hidden('student', $student) !!}
        {!! Form::hidden('model', $model) !!}

        <div class="col-md-2">
            <div class="form-group">
                <label for="">Year of Study</label>
                <select name="year_of_study" id="" class="form-control">
                    <option value="1">First Year</option>
                    <option value="2">Second Year</option>
                    <option value="3">Third Year</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <br>
                {!! Form::submit('Repeating Student', ['class' => 'btn btn-primary mt-3']) !!}
            </div>
        </div>

        {!! Form::close() !!}
    @endif
@endif