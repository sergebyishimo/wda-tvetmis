@extends('college.layout.main')

@section('panel-title', "Make Queries on Students")

@section('htmlheader_title', "Make Queries on Students")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        @if(auth()->guard('college')->check())
                            {!! Form::open(['method' => 'POST', 'route' => ['college.query.execute']]) !!}
                        @elseif(auth()->guard('rp')->check())
                            {!! Form::open(['method' => 'POST', 'route' => ['rp.query.execute']]) !!}
                        @endif
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('student', "Student ID or Student Email", ['class' => 'control-label']) !!}
                                    {!! Form::text('student', request()->get('student'), ['class' => 'form-control', 'placeholder' =>  'Student ID or Email' , 'required' => true]) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('action', "Action", ['class' => 'control-label']) !!}
                                    {!! Form::select('action', getActionForSingleStudent(), request()->get('action'), ['class' => 'form-control select2', 'placeholder' => 'Choose ...', 'required' => true]) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('model', "Model", ['class' => 'control-label']) !!}
                                    {!! Form::select('model', getModalForSingleStudentSearch(), request()->get('model'), ['class' => 'form-control select2', 'placeholder' => 'Choose ...', 'required' => true]) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <button type="submit" class="btn btn-warning btn-block">Execute</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- /.box -->
                @isset($action)
                    <div class="box">
                        <div class="box-body" style="overflow-x: auto;">
                            @if($action == 'd')
                                @include('college.students.queries.partials.d')
                            @endif
                            @if($action == 'e')
                                @include('college.students.queries.partials.e')
                            @endif
                        </div>
                    </div>
                @endisset
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                paging: false
            });


            var college_id = $("#nCollege").val();
            var department_id = $("#nDepartment").val();
            $.get("/api/programs/c/" + college_id + "/d/" + department_id, {}, function (data) {
                $("#nProgram").html("<option value='' selected disabled>Select..</option>");
                for (let i = 0; i < data.length; i++) {
                    $("#nProgram").append("<option value='" + data[i].id + "'>" + data[i].program_name + "</option>");

                }
            });


            $("#nDepartment").change(function () {
                var college_id = $("#nCollege").val();
                var department_id = $("#nDepartment").val();
                $.get("/api/programs/c/" + college_id + "/d/" + department_id, {}, function (data) {
                    $("#nProgram").html("<option value='' selected disabled>Select..</option>");
                    for (let i = 0; i < data.length; i++) {
                        $("#nProgram").append("<option value='" + data[i].id + "'>" + data[i].program_name + "</option>");

                    }
                });
            });

        });

    </script>
@show