@extends('college.layout.main')

@section('panel-title', "View All Students")

@section('htmlheader_title', "View All Students")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        {!! Form::open(['method' => 'POST', 'route' => ['college.students.search']]) !!}
                            <div class="row">
                                @if (auth()->guard('rp')->user())
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {!! Form::label('nCollege', "College", ['class' => 'control-label']) !!}
                                            {!! Form::select('college',  $colleges->pluck('short_name', 'id'), request()->get('college'), ['class' => 'form-control select2', 'placeholder' => 'Choose College','id' => 'nCollege', 'required' => true]) !!}
                                        </div>
                                    </div>    
                                @else   
                                    <input type="hidden" id="nCollege" value="{{ college('college')->id }}">
                                @endif
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('department', "Department", ['class' => 'control-label']) !!}
                                        {!! Form::select('department', $departments->pluck('department_name', 'id'), request()->get('department'), ['class' => 'form-control select2', 'placeholder' =>  'Choose Department' ,'id' => 'nDepartment', 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('program', "Program", ['class' => 'control-label']) !!}
                                        {!! Form::select('program', [], null, ['class' => 'form-control select2','id' => 'nProgram', 'name' => 'program', 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('year', "Year", ['class' => 'control-label']) !!}
                                        {!! Form::select('year', ['1' => 'First', '2' => 'Second', '3' => 'Third'], request()->get('year'), ['class' => 'form-control select2', 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('sponsor', "Sponsorship", ['class' => 'control-label']) !!}
                                        {!! Form::select('sponsor', ['government' => 'Government', 'private' => 'Private', 'social' => "Social"], request()->get('sponsor'), ['class' => 'form-control select2',  'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-block">View</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    @if (isset($students))

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('feedback.feedback')
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th width="30px" class="text-center">#</th>
                                <th width="100px" class="text-center">Photo</th>
                                <th width="160px">Reg Number</th>
                                <th>Names</th>
                                <th>Department</th>
                                <th>Program</th>
                                <th>Study Year</th>
                                <th>Sponsorship</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($students as $student)
                                <tr>
                                    <td class="text-center">{{ $i++ }}</td>
                                    <td align="center">
                                        <img src="{{ regGetStudentPhoto($student) }}" alt="{{ $student->names }}"
                                             style="width: 5em;height: 5em;box-shadow: 0px 0px 10px #8d9499"
                                             class="img-circle img-responsive">
                                    </td>
                                    <td>{{ $student->std_id }}</td>
                                    <td>{{ $student->names }}</td>
                                    <td>{{ $student->department->department or "" }}</td>
                                    <td>{{ $student->course->choices or "" }}</td>
                                    <td>{{ getMyYearOfStudy(false, $student->year_of_study) }}</td>
                                    <td>{{ ucwords($student->sponsorship_status == '.' ? 'government' : $student->sponsorship_status) }}</td>
                                    <td>
                                        @if(auth()->guard('college')->check())
                                            <a href="{{ route('college.registration.show', strtolower($student->student_reg)) }}"
                                               class="btn btn-primary btn-sm pull-right">See More</a>
                                        @elseif(auth()->guard('rp')->check())
                                            <a href="{{ route('rp.registration.show', strtolower($student->student_reg)) }}"
                                               class="btn btn-primary btn-sm pull-right">See More</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $students->links() }}
                    </div>
                    <!-- /.box-body -->
                    @endif
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                paging:false
            });
            
            
            var college_id = $("#nCollege").val();
            var department_id = $("#nDepartment").val();
            $.get("/api/programs/c/" + college_id + "/d/" + department_id, {}, function (data) {
                $("#nProgram").html("<option value='' selected disabled>Select..</option>");
                for (let i = 0; i < data.length; i++) {
                    $("#nProgram").append("<option value='" + data[i].id + "'>" + data[i].program_name + "</option>");
                    
                }
            });
            

            $("#nDepartment").change(function() {
                var college_id = $("#nCollege").val();
                var department_id = $("#nDepartment").val();
                $.get("/api/programs/c/" + college_id + "/d/" + department_id, {}, function (data) {
                    $("#nProgram").html("<option value='' selected disabled>Select..</option>");
                    for (let i = 0; i < data.length; i++) {
                        $("#nProgram").append("<option value='" + data[i].id + "'>" + data[i].program_name + "</option>");
                        
                    }
                });
            });

        });
        
    </script>
@show