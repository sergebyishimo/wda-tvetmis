@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Enrolled Students
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Enrolled Students: {{ $students->count() }}
    </div>
@endsection

@section('head_css')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        @include('feedback.feedback')
        <div class="box">
            <div class="box-header">
                All Enrolled Students
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped" id="dataTable">
                    <thead>
                    <tr>
                        <th width="50px" class="text-center">#</th>
                        <th>Student Names</th>
                        <th>StudentID</th>
                        @if(auth()->guard('rp')->check())
                            <th>College</th>
                        @endif
                        <th>Department</th>
                        <th>Program</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($students->count() > 0)
                        @php
                            $x = 1;
                        @endphp
                        @foreach($students as $student)
                            <tr>
                                <td class="text-center">{{ $x++ }}</td>
                                <td>
                                    @if($student->admitted)
                                        {{ $student->admitted->names or "" }}
                                    @elseif($student->admittedPrivate)
                                        {{ $student->admittedPrivate->names or "" }}
                                    @endif
                                </td>
                                <td>{{ $student->student_id }}</td>
                                @if(auth()->guard('rp')->check())
                                    <td>{{ $student->college ? $student->college->short_name : "" }}</td>
                                @endif
                                <td>
                                    @if($student->admitted)
                                        {{ getDepartment($student->admitted->department_id, 'department_name') }}
                                    @elseif($student->admittedPrivate)
                                        {{ getDepartment($student->admittedPrivate->department_id, 'department_name') }}
                                    @endif
                                </td>
                                <td>
                                    @if($student->admitted)
                                        {{ getCourse($student->admitted->course_id, 'program_name') }}
                                    @elseif($student->admittedPrivate)
                                        {{ getCourse($student->admittedPrivate->course_id, 'program_name') }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if($students->count() > 0)
                    {!! $students->links() !!}
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
@endsection