@extends('college.layout.main')

@section('panel-title', "Students Cards")

@section('htmlheader_title', "Students Cards")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        @if (auth()->guard('rp')->check())
                            <form method="GET" action="{{ route('rp.cards.index')  }}">
                                @else
                                    <form method="GET" action="{{ route('college.cards.index')  }}">
                                        @endif

                                        <div class="row">
                                            @if (auth()->guard('rp')->check())
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="">College</label>
                                                        <select name="college" id="college" class="form-control">
                                                            <option value="">All Colleges</option>
                                                            @foreach ($colleges as $college)
                                                                <option value="{{ $college->id }}" @if(request('college') == $college->id) selected @endif>{{ $college->short_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Year Of Study</label>
                                                    <select name="year" id="year" class="form-control" required>
                                                        <option value="4">All Years</option>
                                                        <option value="1" @if(request('year') == 1) selected @endif>First Year</option>
                                                        <option value="2" @if(request('year') == 2) selected @endif>Second Year</option>
                                                        <option value="3" @if(request('year') == 3) selected @endif>Third Year</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Program</label>
                                                    @if(auth()->guard('college')->check())
                                                        <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                                    @endif
                                                    <select name="program_id" id="nProgram" class="form-control">
                                                        <option value="">Choose Program</option>
                                                        <option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>
                                                        <option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Department</label>
                                                    <select name="department_id" id="nDepartment" class="form-control">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Option</label>
                                                    <select name="option_id" id="nOption" class="form-control">
                                                        @if (isset($options))
                                                            <option value="">Choose Option</option>
                                                            @foreach ($options as $option)
                                                                <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <br>
                                                <button type="submit" class="btn btn-md btn-primary pull-left">
                                                    View
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                    </div>
                    <div class="box-body">

                        @if (isset($students))
                            @foreach ($students as $student)
                                <div style="background-image: url('/img/cardBackGroundNew.png');background-repeat: no-repeat;background-size: contain;height: 254px;width:549px;font-family: sans-serif;margin-left: 250px">
                                    <div style="float: left">
                                        <img src="/storage/{{ $student->photo }}" style="width: 110px;height: 110px;margin-top: 70px;margin-left: 30px">
                                        
                                    </div>
                                    <img src="/img/logos/{{ $student->college_id }}.png"  style="width:240px;height: 60px;margin-top: 30px;margin-left: 30px" >
                                    <div style="float: left;margin-top: 0px;margin-left: 30px;font-size: 13px; font-weight:bold">
                                       
                                        <span style="float: left"><span style="color:#1b75bc">Names:</span> {{ $student->first_name . ' ' . $student->other_names }}</span> <br clear="left">
                                        @php($arr = ['1' => 'First Year', '2' => 'Second Year', '3' => 'Third Year'])
                                        <span style="float: left;margin-left: 0px"><span style="color:#1b75bc">Level:</span> {{ $arr[$student->year_of_study] }}</span> <br clear="left">
                                        {{-- <span style="float: left;margin-top: 0px;">Reg No:{{ $student->student_reg }}</span> <br clear="left"> --}}
                                        {{-- <span style="float: left;margin-top: 1px;margin-left: 0px">Date of Birth: {{ $student->dob }}</span> <br clear="left"> --}}
                                        <span style="float: left;margin-left: 0px"><span style="color:#1b75bc">Option: </span>{{ substr(getDepartment($student->department_id, 'department_name' , isContinuingStudent($student->student_reg)), 0, 25)  }}</span> <br clear="left">
                                        <span style="float: left;margin-left: 0px"><span style="color:#1b75bc">Department:</span> {{ substr(getCourse($student->course_id, 'program_name', isContinuingStudent($student->student_reg)), 0, 25) }}</span> <br clear="left">
                                        {{-- <span style="float: left;margin-top: 0px;margin-left: 0px">Program: {{ request('program_id') == 1 ? 'Diploma' : 'Advanced Diploma' }}</span> <br clear="left">
                                        <span style="float: left;margin-top: 0px;margin-left: 0px">Study Mode: {{ $student->student_category }} Program</span> <br clear="left"> --}}
                                        <span style="float: left;margin-left: 0px"><span style="color:#1b75bc">Academic Year:</span> 2018 - 2019</span> <br clear="left">
                                    </div>
                                    <br clear="left">
                                    <div style="float: left;margin-top: 20px;margin-left: 15px;font-size: 13px;font-weight:bold">
                                        <span style="color:#1b75bc">Reg No: </span>{{ $student->student_reg }}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <span style="color:#1b75bc">Expired Date:</span> 01/01/2020
                                    </div>

                                </div>
                                <br clear="left">
                            @endforeach
                             {{ $students->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    @if (auth()->guard('rp')->check())
        <script>
            $(function() {

                if($("#year").val() === '1') {
                    var id = $("#aCollege").val();
                    getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
                }

                $("#nProgram").change(function() {
                    if ($("#year").val() !== '1') {
                        var program = $(this).val();
                        $("#nDepartment").html('');
                        $.get('/rp/data/departments/' + program, function(data,status) {
                            $("#nDepartment").html('<option value="">Choose Department</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                            }
                        });
                    }
                });

                $("#nDepartment").change(function() {
                    if ($("#year").val() === '1') {
                        var id = $(this).val();
                        var cl = $("#aCollege").val();
                        getingOPtions(cl, "/poly/c/"+id+"/", $("#nOption"), 'c');
                    } else {
                        var program = $(this).val();
                        $("#nOption").html('');
                        $.get('/rp/data/options/' + program, function(data,status) {
                            $("#nOption").html('<option value="">Choose Option</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                            }
                        });
                    }

                });

                function getingOPtions(id, url, elem, val, cl) {
                    $.get(url + id, {}, function (data) {
                        var obj = jQuery.parseJSON(data);
                        var html = "<option value='' selected disabled>Select ...</option>";
                        $.each(obj, function (key, value) {
                            var v = "";
                            var a = "";
                            if (val == 'd')
                                v = value.department_name;
                            if (val == 'c') {
                                v = value.program_name;
                                var cd = value.choice_code;
                                var aa = value.study_area;
                                if (cd != undefined){
                                    if (cd.length > 0)
                                        a = " ( " + cd + " ) ";
                                }
                            }
                            if (val == "m")
                                v = value.course_unit_name;
                            html += "<option value='" + value.id + "'>" + v + a + "</option>";
                        });
                        elem.html(html);
                    });
                }
            });
        </script>
    @else
        <script>
            $(function() {

                $("#year").change(function() {
                    if($("#year").val() === '1') {
                        var id = $("#aCollege").val();
                        getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
                    } else {
                        $("#nDepartment").html('');
                    }
                });


                $("#nProgram").change(function() {
                    if ($("#year").val() !== '1') {
                        var program = $(this).val();
                        $("#nDepartment").html('');
                        $.get('/college/data/departments/' + program, function(data,status) {
                            $("#nDepartment").html('<option value="">Choose Department</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                            }
                        });
                    }
                });

                $("#nDepartment").change(function() {
                    if ($("#year").val() === '1') {
                        var id = $(this).val();
                        var cl = $("#aCollege").val();
                        getingOPtions(cl, "/poly/c/"+id+"/", $("#nOption"), 'c');
                    } else {
                        var program = $(this).val();
                        $("#nOption").html('');
                        $.get('/college/data/options/' + program, function(data,status) {
                            $("#nOption").html('<option value="">Choose Option</option>');
                            for (i = 0; i < data.length; i++) {
                                $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                            }
                        });
                    }

                });

                function getingOPtions(id, url, elem, val, cl) {
                    $.get(url + id, {}, function (data) {
                        var obj = jQuery.parseJSON(data);
                        var html = "<option value='' selected disabled>Select ...</option>";
                        $.each(obj, function (key, value) {
                            var v = "";
                            var a = "";
                            if (val == 'd')
                                v = value.department_name;
                            if (val == 'c') {
                                v = value.program_name;
                                var cd = value.choice_code;
                                var aa = value.study_area;
                                if (cd != undefined){
                                    if (cd.length > 0)
                                        a = " ( " + cd + " ) ";
                                }
                            }
                            if (val == "m")
                                v = value.course_unit_name;
                            html += "<option value='" + value.id + "'>" + v + a + "</option>";
                        });
                        elem.html(html);
                    });
                }
            });
        </script>
    @endif
@endsection