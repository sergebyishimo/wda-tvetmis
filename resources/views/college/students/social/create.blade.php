@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Social Students
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Social Students
    </div>
@endsection

@section('head_css')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-12">
                <a href="{{ route('college.social.index') }}" class="btn btn-warning">
                    <i class="fa fa-list"></i>
                    &nbsp;<span>View All</span>
                </a>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <form class="" id="register" role="form" method="POST"
                      action="{{ route('college.social.store') }}">
                    {{ csrf_field() }}
                    @include('flash_msg.flash-message')
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group col-md-12">
                                {{--<label for="aProgram">Sponsor</label>--}}
                                {!! Form::label('student', "Student", ['class' => 'control-label']) !!}
                                {!! Form::select('student', $students, null, ['class' => 'form-control select2', 'placeholder' => 'Select student ...', 'required' => true]) !!}
                                {{--<input type="text" name="sponsor" value="" class="form-control" required>--}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group col-md-12">
                                {{--<label for="aProgram">Sponsor</label>--}}
                                {!! Form::label('sponsor', "Sponsor", ['class' => 'control-label']) !!}
                                {!! Form::select('sponsor', $sponsors, null, ['class' => 'form-control select2', 'placeholder' => 'Select sponsor ...', 'required' => true]) !!}
                                {{--<input type="text" name="sponsor" value="" class="form-control" required>--}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-condensed">
                                    @if($feesCategories)
                                        <thead>
                                        <tr>
                                            <th width="50px"></th>
                                            <th>Fees Name</th>
                                            <th>Percentage Covered</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($feesCategories as $feesCategory)
                                            <tr>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" name="fees[{{$feesCategory->id}}]"
                                                               value="{{$feesCategory->id}}"
                                                               id="fees{{$feesCategory->id}}">
                                                    </center>
                                                </td>
                                                <td>
                                                    <label for="fees{{$feesCategory->id}}" style="cursor:pointer;">
                                                        {!! $feesCategory->category_name !!}
                                                    </label>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="number" name="percentage[{{$feesCategory->id}}]"
                                                               class="form-control" max="100" min="1" required>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                        @endforelse
                                        </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\SocialStudentRegisterRequest', '#register'); !!}
@endsection