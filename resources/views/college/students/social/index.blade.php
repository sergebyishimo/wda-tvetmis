@extends('college.layout.main')

@section('panel-title', "View All Social Students")

@section('htmlheader_title', "View All Social Students")

@section('l-style')
    @parent
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row" style="margin-bottom: 10px;">
            @if(auth()->guard('college')->check())
                <div class="col-md-4">
                    <a href="{{ route('college.social.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus-square"></i>
                        &nbsp;<span>Add New</span>
                    </a>
                </div>
            @endif
            <div class="col-md-4">
                <a href="?f=a" class="btn btn-info {{ request()->get('f') == 'a' ? 'active' : '' }}">
                    <i class="fa fa-star"></i>
                    &nbsp;<span>Admitted && Continuing</span>
                </a>
            </div>
            <div class="col-md-4">
                <a href="?f=o"
                   class="btn btn-success {{ request()->get('f') == 'o' || ! request()->has('f') ? 'active' : '' }}">
                    <i class="fa fa-star-half"></i>
                    &nbsp;<span>Others</span>
                </a>
            </div>
        </div>

        <div class="box">
            <div class="box-header with-border">
                <form method="GET" action="{{ route('college.social.index')  }}">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Year Of Study</label>
                                <select name="year" id="year" class="form-control" required>
                                    <option value="1" @if(isset($module) &&$module->year_of_study == 1) selected @endif >First Year</option>
                                    <option value="17" @if(isset($module) &&$module->year_of_study == 2) selected @endif >Second Year</option>
                                    <option value="16" @if(isset($module) &&$module->year_of_study == 3) selected @endif >Third Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Program</label>
                                @if(auth()->guard('college')->check())
                                    <input type="hidden" id="aCollege" value="{{ college('college_id') }}">
                                @endif
                                <select name="program_id" id="nProgram" class="form-control" required>
                                    <option value="">Choose Program</option>
                                    <option value="1" @if(isset($department) && $department->program_id == 1) selected @endif >Diploma (Level 6)</option>
                                    <option value="2" @if(isset($department) && $department->program_id == 2) selected @endif >Advanced Diploma (Level 7)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Department</label>
                                <select name="department_id" id="nDepartment" class="form-control" required>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Option</label>
                                <select name="option_id" id="nOption" class="form-control" required>
                                    @if (isset($options))
                                        <option value="">Choose Option</option>
                                        @foreach ($options as $option)
                                            <option value="{{ $option->id }}" @if(isset($module) && $module->option_id == $option->id) selected @endif >{{ $option->option_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <br>
                            <button type="submit" class="btn btn-md btn-primary pull-left">
                                View
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td width="140px">StudentID</td>
                        <td width="140px">First name</td>
                        <td width="140px">Last name</td>
                        @if(auth()->guard('rp')->check())
                            <td>College</td>
                        @endif
                        <td>Department</td>
                        <td>Program</td>
                        <td>Sponsor</td>
                        <td>Category</td>
                        <td>Percentage</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($socials as $social)
                            <tr>
                                <td>{{ $social->student_id }}</td>
                                <td>{{ getStudentInfo($social->student_id, 'first_name')   }}</td>
                                <td>{{ getStudentInfo($social->student_id, 'other_names')  }}</td>
                                <td>{{ getStudentInfo($social->student_id, 'department_id')  }}</td>
                                <td>{{ getStudentInfo($social->student_id, 'course_id') }}</td>
                                <td>{{ strtoupper($social->sponsor ) }}</td>
                                <td>{{ $social->category ? $social->category->category_name : ''}}</td>
                                <td>{{ $social->percentage }}</td>
                                <td>
                                    <form method="POST" action="{{ route('college.social.destroy', $social->student_id)  }}"> {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <input type="hidden" name="id" value="{{ $social->student_id  }}">
                                        <button class="btn btn-sm btn-danger">
                                            <i class="fa fa-trash"></i>
                                            DELETE
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="10" class="text-center">No Social Students Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                    {{--<tfoot>--}}
                    {{--<tr>--}}
                        {{--<td width="140px">StudentID</td>--}}
                        {{--<td width="140px">First name</td>--}}
                        {{--<td width="140px">Last name</td>--}}
                        {{--@if(auth()->guard('rp')->check())--}}
                            {{--<td>College</td>--}}
                        {{--@endif--}}
                        {{--<td>Department</td>--}}
                        {{--<td>Program</td>--}}
                        {{--<td>Sponsor</td>--}}
                        {{--<td>Category</td>--}}
                        {{--<td>Percentage</td>--}}
                        {{--<td>Action</td>--}}
                    {{--</tr>--}}
                    {{--</tfoot>--}}
                </table>
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    @if(auth()->guard('college')->check())
        @if(request()->get('f') == 'a')
            <script>
                $('#dataTable').DataTable({
                    dom: 'Blfrtip',
                    serverSide: true,
                    processing: true,
                    pager: true,
                    colReorder: true,
                    // "columnDefs": [
                    //     { "width": "200px", "targets": 0 },
                    //     { "width": "90px", "targets": 8 }
                    // ],
                    ajax: '{{ route('college.get.sponsored.students') }}',
                    "oSearch": {"bSmart": false},
                    columns: [
                        {
                            data: 'std_id', name: 'rp_admitted_students.std_id'
                        },
                        {data: 'first_name', name: 'rp_admitted_students.first_name'},
                        {
                            data: 'other_names',
                            name: 'rp_admitted_students.other_names',
                            orderable: true,
                            searchable: true
                        },
                        {
                            data: 'department_name',
                            name: 'rp_departments.department_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'program_name', name: 'rp_programs.program_name', orderable: true, searchable: true},
                        {data: 'sponsor', name: 'rp_sponsor_students.sponsor', orderable: true, searchable: true},
                        {
                            data: 'category_name',
                            name: 'rp_source_fees_category.category_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'percentage', name: 'rp_sponsor_students.percentage', orderable: true, searchable: true},
                        {data: 'action', orderable: false, searchable: false},
                    ],
                    buttons: [
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column',
                            postfixButtons: ['colvisRestore']
                        },
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    initComplete: function () {
                        var x = 0;
                        var lenC = this.api().columns().length;
                        this.api().columns().every(function () {
                            if (x > 2) {
                                var column = this;
                                var select = $('<select class="select2"><option value=""></option></select>')
                                    .appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function (d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                });
                            }
                            x++;
                        });
                    }
                });
            </script>
        @else
            <script>
                $('#dataTable').DataTable({
                    dom: 'Blfrtip',
                    serverSide: true,
                    processing: true,
                    pager: true,
                    colReorder: true,
                    // "columnDefs": [
                    //     { "width": "200px", "targets": 0 },
                    //     { "width": "90px", "targets": 8 }
                    // ],
                    ajax: '{{ route('college.get.sponsored.students.others') }}',
                    "oSearch": {"bSmart": false},
                    columns: [
                        {
                            data: 'std_id', name: 'rp_admission_privates.std_id'
                        },
                        {data: 'first_name', name: 'rp_admission_privates.first_name'},
                        {
                            data: 'other_names',
                            name: 'rp_admission_privates.other_names',
                            orderable: true,
                            searchable: true
                        },
                        {
                            data: 'department_name',
                            name: 'rp_departments.department_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'program_name', name: 'rp_programs.program_name', orderable: true, searchable: true},
                        {data: 'sponsor', name: 'rp_sponsor_students.sponsor', orderable: true, searchable: true},
                        {
                            data: 'category_name',
                            name: 'rp_source_fees_category.category_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'percentage', name: 'rp_sponsor_students.percentage', orderable: true, searchable: true},
                        {data: 'action', orderable: false, searchable: false},
                    ],
                    buttons: [
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column',
                            postfixButtons: ['colvisRestore']
                        },
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    initComplete: function () {
                        var x = 0;
                        var lenC = this.api().columns().length;
                        this.api().columns().every(function () {
                            if (x > 2) {
                                var column = this;
                                var select = $('<select class="select2"><option value=""></option></select>')
                                    .appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function (d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                });
                            }
                            x++;
                        });
                    }
                });
            </script>
        @endif
    @endif
    @if(auth()->guard('rp')->check())
        @if(request()->get('f') == 'a')
            <script>
                $('#dataTable').DataTable({
                    dom: 'Blfrtip',
                    serverSide: true,
                    processing: true,
                    pager: true,
                    colReorder: true,
                    // "columnDefs": [
                    //     { "width": "200px", "targets": 0 },
                    //     { "width": "90px", "targets": 8 }
                    // ],
                    ajax: '{{ route('rp.get.sponsored.students') }}',
                    "oSearch": {"bSmart": false},
                    columns: [
                        {
                            data: 'std_id', name: 'rp_admitted_students.std_id'
                        },
                        {data: 'first_name', name: 'rp_admitted_students.first_name'},
                        {
                            data: 'other_names',
                            name: 'rp_admitted_students.other_names',
                            orderable: true,
                            searchable: true
                        },
                        {
                            data: 'short_name', name: 'adm_polytechnics.short_name'
                        },
                        {
                            data: 'department_name',
                            name: 'rp_departments.department_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'program_name', name: 'rp_programs.program_name', orderable: true, searchable: true},
                        {data: 'sponsor', name: 'rp_sponsor_students.sponsor', orderable: true, searchable: true},
                        {
                            data: 'category_name',
                            name: 'rp_source_fees_category.category_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'percentage', name: 'rp_sponsor_students.percentage', orderable: true, searchable: true},
                    ],
                    buttons: [
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column',
                            postfixButtons: ['colvisRestore']
                        },
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    initComplete: function () {
                        var x = 0;
                        var lenC = this.api().columns().length;
                        this.api().columns().every(function () {
                            if (x > 2) {
                                var column = this;
                                var select = $('<select class="select2"><option value=""></option></select>')
                                    .appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function (d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                });
                            }
                            x++;
                        });
                    }
                });
            </script>
        @else
            <script>
                $('#dataTable').DataTable({
                    dom: 'Blfrtip',
                    serverSide: true,
                    processing: true,
                    pager: true,
                    colReorder: true,
                    // "columnDefs": [
                    //     { "width": "200px", "targets": 0 },
                    //     { "width": "90px", "targets": 8 }
                    // ],
                    ajax: '{{ route('rp.get.sponsored.students.others') }}',
                    "oSearch": {"bSmart": false},
                    columns: [
                        {
                            data: 'std_id', name: 'rp_admission_privates.std_id'
                        },
                        {data: 'first_name', name: 'rp_admission_privates.first_name'},
                        {
                            data: 'other_names',
                            name: 'rp_admission_privates.other_names',
                            orderable: true,
                            searchable: true
                        },
                        {
                            data: 'short_name', name: 'adm_polytechnics.short_name'
                        },
                        {
                            data: 'department_name',
                            name: 'rp_departments.department_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'program_name', name: 'rp_programs.program_name', orderable: true, searchable: true},
                        {data: 'sponsor', name: 'rp_sponsor_students.sponsor', orderable: true, searchable: true},
                        {
                            data: 'category_name',
                            name: 'rp_source_fees_category.category_name',
                            orderable: true,
                            searchable: true
                        },
                        {data: 'percentage', name: 'rp_sponsor_students.percentage', orderable: true, searchable: true},
                    ],
                    buttons: [
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column',
                            postfixButtons: ['colvisRestore']
                        },
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    initComplete: function () {
                        var x = 0;
                        var lenC = this.api().columns().length;
                        this.api().columns().every(function () {
                            if (x > 2) {
                                var column = this;
                                var select = $('<select class="select2"><option value=""></option></select>')
                                    .appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function (d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                });
                            }
                            x++;
                        });
                    }
                });
            </script>
        @endif
    @endif
    <script>
        $(document).ready(function () {
            $('#filterSearchremove').click(function (e) {
                e.preventDefault();
                $('#filterSearch').hide();
            });

            $('#columnremove').click(function (e) {
                e.preventDefault();
                $('#filtercolumns').hide();
            });
        });
    </script>

    <script>
        $(function() {

            if($("#year").val() === '1') {
                var id = $("#aCollege").val();
                getingOPtions(id, "/poly/d/", $("#nDepartment"), 'd');
            }

            $("#nProgram").change(function() {
                if ($("#year").val() !== '1') {
                    var program = $(this).val();
                    $("#nDepartment").html('');
                    $.get('/college/data/departments/' + program, function(data,status) {
                        $("#nDepartment").html('<option value="">Choose Department</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nDepartment").append('<option value='+ data[i].id +'>'+ data[i].department_name +'</option>');
                        }
                    });
                }
            });

            $("#nDepartment").change(function() {
                if ($("#year").val() === '1') {
                    var id = $(this).val();
                    var cl = $("#aCollege").val();
                    getingOPtions(cl, "/poly/c/"+id+"/", $("#nOption"), 'c');
                } else {
                    var program = $(this).val();
                    $("#nOption").html('');
                    $.get('/college/data/options/' + program, function(data,status) {
                        $("#nOption").html('<option value="">Choose Option</option>');
                        for (i = 0; i < data.length; i++) {
                            $("#nOption").append('<option value='+ data[i].id +'>'+ data[i].option_name +'</option>');
                        }
                    });
                }

            });

            function getingOPtions(id, url, elem, val, cl) {
                $.get(url + id, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select ...</option>";
                    $.each(obj, function (key, value) {
                        var v = "";
                        var a = "";
                        if (val == 'd')
                            v = value.department_name;
                        if (val == 'c') {
                            v = value.program_name;
                            var cd = value.choice_code;
                            var aa = value.study_area;
                            if (cd != undefined){
                                if (cd.length > 0)
                                    a = " ( " + cd + " ) ";
                            }
                        }
                        if (val == "m")
                            v = value.course_unit_name;
                        html += "<option value='" + value.id + "'>" + v + a + "</option>";
                    });
                    elem.html(html);
                });
            }
        });
    </script>
@endsection