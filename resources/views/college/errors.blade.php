<html>
    <body>
        <h3>{{ $message }}</h3>
        <table border="1" cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <td></td>
                    <td>Acad. Year</td>
                    <td>School</td>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Sector</th>
                    <th>Sub Sector</th>
                    <th>Qualification</th>
                    <th>RTQF</th>
                    <th>Phone</th>
                    <th>E-mail</th>
                    <th>Gender</th>
                    <th>Mode</th>
                    <th>Ubudehe</th>
                    <th>Father</th>
                    <th>Phone</th>
                    <th>Mother</th>
                    <th>Phone</th>
                </tr>
            </thead>
            <tbody>
                @foreach($errors as $student)
                    @php($school = \App\Model\Accr\SchoolInformation::where('id', $student->school_name)->first())
                    @php($sector = \App\Model\Accr\TrainingSectorsSource::where('id', $student->sector_studied)->first())
                    @php($trade = \App\Model\Accr\TrainingTradesSource::where('id', $student->sub_sector_studied)->first())
                    @php($rtqf = \App\Model\Accr\RtqfSource::where('id', $student->rtqf_level_studied)->first())
                    <tr>
                        <td>{{ $student->id  }}</td>
                        <td>{{ $student->academic_year  }}</td>
                        <td>{{  $school ? $school->school_name : $student->school_name }}</td>
                        <td>{{ $student->fname  }}</td>
                        <td>{{ $student->lname  }}</td>
                        <td>{{ $sector ? $sector->tvet_field : $student->sector_studied }}</td>
                        <td>{{ $trade ? $trade->sub_field_name : $student->sub_sector_studied }}</td>
                        <td>{{ $student->qualification_studied }}</td>
                        <td>{{ $rtqf ? $rtqf->level_name : $student->rtqf_level_studied }}</td>
                        <td>{{ $student->phone }}</td>
                        <td>{{ $student->email }}</td>
                        <td>{{ $student->gender }}</td>
                        <td>{{ $student->mode }}</td>
                        <td>{{ $student->ubudehe }}</td>
                        <td>{{ $student->ft_name }}</td>
                        <td>{{ $student->ft_phone }}</td>
                        <td>{{ $student->mt_name }}</td>
                        <td>{{ $student->mt_phone }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>