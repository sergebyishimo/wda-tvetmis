@extends('college.layout.main')

@section('htmlheader_title')
    Admission Letter
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Admission Letter
    </div>
@endsection

@section('l-style')
    @parent
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
    {{--<section class="content">--}}
    <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {!! Form::open(['method' => 'post', 'route' => 'college.store.letter']) !!}
                    <div class="box-body">
                        @include('flash_msg.flash-message')
                        <div class="form-group">
                            {!! Form::textarea('letter', college('myLetter'), ['class' => 'form-control summernote']) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-primary btn-md">Save</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
@endsection

@section('l-scripts')
    @parent
    <!-- include summernote css/js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script>
        $(function () {
            $('.summernote').summernote({
                height : "260"
            });
        });
    </script>
@endsection