@extends('wda.layout.main')

@section('panel-title', "Create School")

@section('htmlheader_title', "Create School")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-md-2">
                <a class="btn btn-primary btn-md" href="{{ route('wda.mineduc.index') }}">
                    <i class="fa fa-list"></i>&nbsp;<span>Return to list</span></a>
            </div>
        </div>
        {!! Form::open(['route' => 'wda.store.school', 'files' => true, 'id' => 'createSchool']) !!}
            <div class="box box-info box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Primary information</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-12 col-form-label">School Type</label>
                                <div class="col-md-12">
                                    <select name="school_type" class="form-control flat select2">
                                        <option>Choose School Type ...</option>
                                        @foreach ($schoolTypes as $type)
                                            <option value="{{ $type->id  }}">{{ $type->school_type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>School Code</label>
                                <input type="text" class="form-control flat" name="school_code"
                                    placeholder="School Code">
                            </div>
                            <div class="form-group">
                                <label>School Name</label>
                                <input type="text" class="form-control flat" name="school_name"
                                    placeholder="School Name">
                            </div>
                            <div class="form-group">
                                <label>School Acronym</label>
                                <input type="text" class="form-control flat" name="school_acronym"
                                    placeholder="School Acronym">
                            </div>
                            <div class="form-group">
                                <label for="">E-mail:</label>
                                <input type="text" class="form-control flat" name="email"
                                        value="{{ $school_info['email'] or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Phone:</label>
                                <input type="text" class="form-control flat" name="phone"
                                        value="{{ $school_info['phone'] or '' }}">

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">School Status:</label>
                                <select class="form-control flat" required name="school_status">
                                    <option value="">Choose Here</option>
                                    <option @if( isset($school_info) && $school_info['school_status'] == "Public") selected @endif>
                                        Public
                                    </option>
                                    <option @if( isset($school_info) && $school_info['school_status'] == "Government Aid") selected @endif>
                                        Government Aid
                                    </option>
                                    <option @if( isset($school_info) && $school_info['school_status'] == "Private") selected @endif>
                                        Private
                                    </option>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="">School Activity</label>
                                <select class="form-control" name="school_activity">
                                    <option>Active</option>
                                    <option>Not Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Manager Name:</label>
                                <input type="text" class="form-control flat" name="manager_name"
                                        value="{{  $school_info['manager_name'] or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Manager Phone:</label>
                                <input type="text" class="form-control flat" name="manager_phone"
                                        value="{{  $school_info['manager_phone'] or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Manager E-mail:</label>
                                <input type="text" class="form-control flat" name="manager_email"
                                        value="{{  $school_info['manager_email'] or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Accreditation Status</label>
                                <select class="form-control flat" name="accreditation_status">
                                    <option>Accredited</option>
                                    <option>Not Accredited</option>
                                    <option>In Process</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-info box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Location information</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                        
                            <div class="form-group">
                                <label>Province</label>
                                <select class="form-control flat" id="nprovince" name="province">
                                    <option value="">Choose Here</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>District</label>
                                <select class="form-control flat" id="ndistrict" name="district">
                                    <option value="">Choose Here</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Sector:</label>
                                <select class="form-control flat" id="nsector" name="sector">
                                    <option value="">Choose Here</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Cell:</label>
                                <select class="form-control flat" id="ncell" name="cell">
                                    <option value="">Choose Here</option>
                                </select>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Village:</label>
                                <select class="form-control flat" id="nvillage" name="village">
                                    <option value="">Choose Here</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Latitude:</label>
                                <input type="text" class="form-control flat" name="latitude"
                                        value="{{ $school_info['latitude'] or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Longitude:</label>
                                <input type="text" class="form-control flat" name="longitude"
                                        value="{{ $school_info['longitude'] or '' }}">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-info box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Other Information</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                        <label for="">Accreditation Number</label>
                                        <input type="text" class="form-control flat" name="accreditation_number"
                                                placeholder="Accreditation Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Website</label>
                                        <input type="text" class="form-control flat" name="website"
                                                placeholder="Website">
                                    </div>
                                    <div class="form-group">
                                        <label>Date of Establishment</label>
                                        <input type="text" class="form-control datepicker-year flat"
                                                name="date_of_establishment">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Boarding / Day</label>
                                        <select class="form-control flat" name="boarding_or_day">
                                            <option>Boarding</option>
                                            <option>Day</option>
                                            <option>Both</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">School Logo</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="school_logo"
                                                        id="exampleInputFile">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="col-md-6">
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label for="">Owner Name:</label>
                                        <input type="text" class="form-control flat" name="owner_name"
                                            value="{{ $school_info['owner_name'] or '' }}">
            
                                    </div>
                                    <div class="form-group">
                                        <label for="">Owner Phone:</label>
                                        <input type="text" class="form-control flat" name="owner_phone"
                                            value="{{ $school_info['owner_phone'] or '' }}">
            
                                    </div>
                                    <div class="form-group">
                                        <label for="">Owner Type:</label>
                                        <select class="form-control" name="owner_type">
                                            @foreach ($ownerships as $ownership)
                                                <option value="{{ $ownership->id }}">{{ $ownership->owner }}</option>
                                            @endforeach
                                        </select>
            
                                    </div>
                                    <div class="form-group">
                                        <label for="">Owner E-mail:</label>
                                        <input type="text" class="form-control flat" name="owner_email"
                                            value="{{ $school_info['owner_email'] or '' }}">
            
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-12 col-form-label">School Rating</label>
                                        <div class="col-md-12">
                                            <select name="school_rating" class="form-control flat select2">
                                                <option>Choose School Rate ...</option>
                                                @foreach ($schoolRating as $rating)
                                                    <option name="{{ $rating->id  }}">{{ $rating->rating }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                            </div>
                    </div>
                </div>
                
                
            </div>
            <button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i> Save School Information </button>
        {!! Form::close() !!}
    </div>
@endsection
@section("l-scripts")
    @parent
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CreateNewSchoolRequest', '#createSchool'); !!}
@endsection