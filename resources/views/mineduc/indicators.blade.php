@extends('mineduc.layout.main')
@section('panel-title', "Quality Indicators")
@section('htmlheader_title', "Quality Indicators")
@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-success btn-round pull-right" href="{{route('wda.mineduc.quality.indicator.index',['action'=> "add"])}}"><span class="ion-plus"></span>Add Indicator</a>
            @if(isset($_GET['action']) && $_GET['action']  == "add")
                <form method="POST" action="{{ route('wda.mineduc.quality.indicator.store') }}">
                    {{ csrf_field() }}
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Add Indicator</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label>Sector</label>
                                {!! Form::select('section_id', $sections->pluck('section_name','id') , null , ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>Indicator Name:</label>
                                <input type="text" step="any" class="form-control flat" name="indicator_name" placeholder="Enter Indicator Names">
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Grade1(Excellent) Desc:</label>
                                    {!! Form::textarea('grade1', null, ['class' => 'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Grade2(Very Good) Desc:</label>
                                    {!! Form::textarea('grade2', null, ['class' => 'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Grade3(Good) Desc:</label>
                                    {!! Form::textarea('grade3', null, ['class' => 'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Grade4(Fair) Desc:</label>
                                    {!! Form::textarea('grade4', null, ['class' => 'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Grade5(Poor) Desc:</label>
                                    {!! Form::textarea('grade5', null, ['class' => 'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Evidences:</label>
                                    {!! Form::textarea('evidences', null, ['class' => 'form-control','rows'=>3]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="text" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </form>
            @endif
            <table class="table table-bordered table-condensed" style="background: #fff">
                <thead>
                <tr class="bg-dark-gradient">
                    <th class="text-center" width="40px">#</th>
                    <th colspan="3">Sectors</th>
                </tr>
                </thead>
                <tbody>
                @php($x = 1)
                @foreach ($sections as $section)
                    <tr>
                        <td class="text-center">{{ $x ++ }}</td>
                        <td width="200px" class="bg-dark-gradient"><h6>{{ $section->section_name }}</h6></td>
                        <td colspan="3">
                            @if($section->indicators->count() > 0)
                                <table class="">
                                    <thead>
                                    <th class="bg-dark-gradient text-center">Indicators</th>
                                    <th class="bg-dark-gradient text-center">Grade1:<br>(Excellent)</th>
                                    <th class="bg-dark-gradient text-center">Grade2:<br>(Very Good)</th>
                                    <th class="bg-dark-gradient text-center">Grade3:<br>(Good)</th>
                                    <th class="bg-dark-gradient text-center">Grade4:<br>(Fair)</th>
                                    <th class="bg-dark-gradient text-center">Grade5:<br>(Poor)</th>
                                    <th class="bg-dark-gradient text-center">Evidences</th>
                                    <th class="bg-dark-gradient text-center">Marks</th>
                                    </thead>
                                    @foreach($section->indicators as $indicator)
                                        <tr>
                                            <td class="bg-dark-gradient">
                                                {{$indicator->indicator_name}}
                                            </td>
                                            <td class="bg-dark-gradient">
                                                {{$indicator->grade1}}
                                            </td>
                                            <td class="bg-dark-gradient">
                                                {{$indicator->grade2}}
                                            </td>
                                            <td class="bg-dark-gradient">
                                                {{$indicator->grade3}}
                                            </td>
                                            <td class="bg-dark-gradient">
                                                {{$indicator->grade4}}
                                            </td>
                                            <td class="bg-dark-gradient">
                                                {{$indicator->grade5}}
                                            </td>
                                            <td class="bg-dark-gradient">
                                                {{$indicator->evidences}}
                                            </td>
                                            <td class="bg-dark-gradient text-center">5</td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
    </div>
@endsection