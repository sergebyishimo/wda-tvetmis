@extends('wda.layout.main')

@section('panel-title',"Quality Audit")
@section('htmlheader_title', "Quality Audit")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {!! Form::open(['route' => 'wda.mineduc.qualityaudit']) !!}
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('school_id', 'Select School', ['class' => 'label-control']) !!}
                            {!! Form::select('school_id', $schools->pluck('school_name','id'), ['placeholder'=>'Select School','class' => 'form-control submit-date select2']) !!}
                        </div>
                        <div class="col-md-2 pull-right">
                            <a href="{{ url()->previous() }}" class="btn btn-warning pull-right">
                                <i class="fa fa-list"></i>&nbsp;Return Back
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="bg-gray">
                        <tr>
                            <th class="text-left">Sector</th>
                            <th class="text-right">Answers</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sections as $section)
                            <tr>
                                <td class="bg-gray-light">
                                    <div>{{$section->section_name}}</div>
                                </td>
                                <td>
                                    <table class="table">
                                        @foreach ($section->indicators as $indicator)
                                            <tr>
                                                <td align="left">{{$indicator->indicator_name}}</td>
                                                <td align="right" class="bg-success">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-radio">
                                                            <span>{{$indicator->grade1}}</span>
                                                            <input type="radio" id="customRadioY" name="data[{{$section->id}}][{{$indicator->id}}]" value="5"
                                                                   required class="custom-control-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-radio">
                                                            <span>{{$indicator->grade2}}</span>
                                                            <input type="radio" id="customRadioN " name="data[{{$section->id}}][{{$indicator->id}}]" value="4"
                                                                   required class="custom-control-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-radio">
                                                            <span>{{$indicator->grade3}}</span>
                                                            <input type="radio" id="customRadioN " name="data[{{$section->id}}][{{$indicator->id}}]" value="3"
                                                                   required class="custom-control-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-radio">
                                                            <span>{{$indicator->grade4}}</span>
                                                            <input type="radio" id="customRadioN " name="data[{{$section->id}}][{{$indicator->id}}]" value="2"
                                                                   required class="custom-control-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-radio">
                                                            <span>{{$indicator->grade5}}</span>
                                                            <input type="radio" id="customRadioN " name="data[{{$section->id}}][{{$indicator->id}}]" value="1"
                                                                   required class="custom-control-input">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! Form::submit('Submit', ['class' => 'form-control btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
@endsection