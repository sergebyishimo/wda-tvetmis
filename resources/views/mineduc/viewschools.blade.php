@extends('mineduc.layout.main')

@section('panel-title', "All School Management")

@section('htmlheader_title', "All School Management")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-2">
                <a class="btn btn-primary" href="{{ route('wda.mineduc.createschool') }}">
                    <i class="fa fa-plus-square"></i>
                    &nbsp;<span>Add New School</span>
                </a>
            </div>
            <div class="col-md-2">
                <button class="btn btn-success" type="button"
                data-toggle="modal" data-target="#uploadSchools">
                    <i class="fa fa-upload"></i>
                    &nbsp;<span>Upload Schools</span>
                </button>
            </div>
        </div>
        <div class="row" style="overflow-y: scroll;">
            <div class="col-md-12">
                <table class="table table-bordered table-striped" id="myTable">
                    <thead>
                    <tr>
                        <th>School Name</th>
                        <th>Province</th>
                        <th>District</th>
                        <th>School Type</th>
                        <th>Accreditation</th>
                        <th>School Activity</th>
                        <th>School Status</th>
                        <th>Phone Number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        {{-- @php
                            $i = 1;
                        @endphp
                        @foreach ($schools as $school)
                            <tr>
                                <td><a href="/schools/{{ $school->id }}" target="_blank" style="color: #333">{{ $school->school_name }}</a></td>
                                <td>{{ $school->province }}</td>
                                <td>{{ $school->district }}</td>
                                <td>{{ ($school->accreditation_status == 1) ? 'Accredited' : 'Not Accredited' }}</td>
                                <td>{{ $school->phone }}</td>
                                <td>
                                    <form method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <input type="hidden" name="delete_school" value="{{ $school->id }}">

                                        <a href="{{ route('wda.view.school', $school->id) }}"
                                        title="View More"
                                        class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i></a>
                                        <a href="{{ route('wda.edit.school', $school->id) }}"
                                        title="Edit"
                                        class="btn btn-primary btn-sm btn-flat"><i
                                                    class="fa fa-edit"></i></a>
                                        <button type="button" class="btn btn-primary btn-sm"
                                                data-school="{{ $school->id }}"
                                                data-school-name="{{ $school->school_name }}"
                                                data-toggle="modal" data-target="#assignQualifications">
                                            <i class="fa fa-anchor"></i>
                                        </button>
                                        <button type="submit" class="btn btn-danger btn-sm pull-right btn-flat"
                                                title="Delete"
                                                onclick="return window.confirm('Are you sure you want to remove this school:\n{{ $school->school_name }}?')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        @endforeach --}}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>School Name</th>
                            <th>Province</th>
                            <th>District</th>
                            <th>School Type</th>
                            <th>Accreditation</th>
                            <th>School Activity</th>
                            <th>School Status</th>
                            <th>Phone Number</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
                
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                dom: 'Blfrtip',
                serverSide: true,
                processing: true,
                pager: true,
                colReorder: true,
                "columnDefs": [
                    { "width": "200px", "targets": 0 },
                    { "width": "90px", "targets": 8 }
                ],
                ajax: '/wda/datatable/object-data/ss',
                "oSearch": {"bSmart": false},
                columns: [
                    {data: 'school_name', name: 'accr_schools_information.school_name'},
                    {data: 'province', name: 'accr_schools_information.province', orderable:true, searchable:true},
                    {data: 'district', name: 'accr_schools_information.district', orderable:true, searchable:true},
                    {data: 'school_type', name: 'accr_source_school_type.school_type', orderable:true, searchable:true},
                    {data: 'accreditation_status', name: 'accr_schools_information.accreditation_status', orderable:true, searchable:true},
                    {data: 'school_activity', name: 'accr_schools_information.school_activity', orderable:true, searchable:true},
                    {data: 'school_status', name: 'accr_schools_information.school_status', orderable:true, searchable:true},
                    {data: 'phone', name: 'accr_schools_information.phone', orderable:true, searchable:true},
                    {data: 'action', orderable: false, searchable: false}
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                initComplete: function () {
                    var x = 0;
                    var lenC = this.api().columns().length;
                    this.api().columns().every(function () {
                        if (x > 0 ){
                            var column = this;
                            var select = $('<select class="select2"><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        x++;
                    });
                }
            });

            $('#filterSearchremove').click(function (e) {
                e.preventDefault();
                $('#filterSearch').hide();
            });

            $('#columnremove').click(function (e) {
                e.preventDefault();
                $('#filtercolumns').hide();
            });
        });
    </script>
@endsection