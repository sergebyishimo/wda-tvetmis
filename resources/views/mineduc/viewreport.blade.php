@extends('mineduc.layout.main')

@section('htmlheader_title')
    Quality Audit Report
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Quality Audit Report
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-body" style="overflow-x: auto;">
                        <div style="text-align: center;" class="text-bold">RANKING OF PRIMARY AND SECONDARY SCHOOLS IN RWANDA --ALL DISTRICTS</div>
                        <div style="text-align: center;" class="text-bold">RANKING PERIOD: FORM <em>date</em> TO <em>date</em></div>
                        <div class="col-md-offset-5" style="text-align: center;margin-bottom: 10px">
                            <table border="1">
                                <tr>
                                    <th>Grades</th>
                                    <th>Average(%)</th>
                                    <th>Rank</th>
                                </tr>
                                <tr>
                                    <td>Grade1</td>
                                    <td>90-100%</td>
                                    <td style="background-color: green;">Excellent</td>
                                </tr>
                                <tr>
                                    <td>Grade2</td>
                                    <td>80-89.9%</td>
                                    <td style="background-color: lightgreen;">Very Good</td>
                                </tr>
                                <tr>
                                    <td>Grade3</td>
                                    <td>70-79.9%</td>
                                    <td style="background-color: yellow;">Good</td>
                                </tr>
                                <tr>
                                    <td>Grade4</td>
                                    <td>50-69.9%</td>
                                    <td style="background-color: yellow;">Poor</td>
                                </tr>
                                <tr>
                                    <td>Grade5</td>
                                    <td>00-49.9%</td>
                                    <td style="background-color: red;">Very Poor</td>
                                </tr>
                            </table>
                        </div>
                        <table class="table table-bordered table-responsive table-condensed table-hover"
                               style="table-border-color-dark: #0c0c0c;border-width: thin;" id="mdtable0">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th width="120px" class="text-center">District</th>
                                <th width="100px" class="text-center">Schools</th>
                                <th width="100px" class="text-center">Category</th>
                                @foreach($sections as $section)
                                    <th width="100px" class="text-center">{{$section->section_name}}</th>
                                @endforeach
                                <th width="100px" class="text-center">Overall Average<br>(%)</th>
                                <th width="100px" class="text-center">Ranking</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--Datables--}}
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(function () {
            let url = "{!! $url !!}";

            let columns = [
                {
                    data: 'DT_Row_Index',
                    name: 'DT_Row_Index',
                    orderable: true,
                    searchable: true
                },
                {
                    data: 'district',
                    name: 'accr_schools_information.district',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'school_name',
                    name: 'accr_schools_information.school_name',
                    orderable: true,
                    searchable: true
                },
                {
                    data: 'school_type',
                    name: 'accr_source_school_type.school_type',
                    orderable: true,
                    searchable: true
                }
            ];
            let cols = [
                @foreach($sections as $section)
                {
                    data : "{{$section->id}}",
                    name: "{{$section->id}}",
                    orderable: true,
                    searchable: true
                },
                @endforeach
            ];
            let lastones = [
                {
                    data : "overall",
                    name: "overall",
                    orderable: true,
                    searchable: true
                },
                {
                    data : "ranking",
                    name: "ranking",
                    orderable: true,
                    searchable: true
                }
            ]
            columns = columns.concat(cols);
            columns =  columns.concat(lastones);

            $("#mdtable0").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: true,
                processing: true,
                ajax: url,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columns: columns,
                'rowCallback': function(row, data, index){
                    if(data[9] < 50){
                        console.log(row);
                    }
                }
            });

        });
    </script>
@show