@extends('wda.layout.main')
@section('panel-title', "Quality Audit Section")
@section('htmlheader_title', "Quality Audit Section")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center" width="40px">#</th>
                    <th>Quality Section</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($sections as $att)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $att->section_name }}</td>
                        <td>
                            {!! Form::open(['route' => ['wda.mineduc.quality.section.destroy', $att->id]]) !!}
                            @method('DELETE')
                            @csrf
                            {!! Form::hidden('id', $att->id) !!}

                            <a href="{{ route('wda.mineduc.quality.section.edit', ['id'=>$att->id]) }}"
                               class="btn btn-primary btn-flat btn-sm"><i
                                        class="fa fa-edit"></i> Edit</a>

                            <button type="submit"
                                    class="btn btn-danger btn-flat btn-sm"
                                    onclick="return window.confirm('Are you sure you want to remove this quality section:\n{{ $att->name }} ?')">
                                <i class="fa fa-trash"></i> Delete
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @php($route = 'wda.mineduc.quality.section.edit')
            @isset($section)
                @php($route = ['wda.mineduc.quality.section.update', $section->id])
            @endisset
            {!! Form::open(['route' => $route]) !!}
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        {!! Form::text('section_name', isset($section) ? $section->section_name : null, ['class' => 'form-control', 'placeholder' => 'Enter Quality Section Name Here', 'required' => true]) !!}
                    </div>
                </div>
                @if(isset($section))
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-flat btn-block">
                            <i class="fa fa-save"></i> Update
                        </button>
                    </div>
                @else
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-flat btn-block">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </div>
                @endif
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection