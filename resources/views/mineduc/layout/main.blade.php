@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        @yield('panel-title')
    </div>
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote.css"
          integrity="sha256-3en7oGyoTD7A1GPFbWeqdIGvDlNWU5+4oWgJQE2dnQs=" crossorigin="anonymous"/>
@endsection

@section('main-content')
    {{--@if(session()->has('status'))--}}
        {{--<div class="box">--}}
            {{--<div class="box-body">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        @include('feedback.feedback')
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--@endif--}}
    
    @if (Route::currentRouteName() == 'wda.school.create' || Route::currentRouteName() == 'wda.edit.school')
        @yield('panel-body')
    @else
        <div class="box">
            <div class="box-body">
                @yield('panel-body')
            </div>
        </div>
    @endif
    
    @include('wda.layout.modals.warnings')
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
            integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
    {{--Datables--}}
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>

    @if (Route::currentRouteName() == 'wda.home')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
        new Chart(document.getElementById("chartjs-0"),
            {"type":"bar","data":
                {
                    "labels":["North","South","East","West","Kigali"],
                    "datasets":[
                        {"label":"Schools Per Province","data":[{{ implode(', ', $schools_per_province_arr) }}],
                        "fill":false,
                        "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                        "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
                },
                "options":
                    {"scales":
                        {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});</script>
    <script>
        new Chart(document.getElementById("chartjs-1"),
            {"type":"bar","data":
                {
                    "labels":["North","South","East","West","Kigali"],
                    "datasets":[
                        {
                            "label":"Students Per Province",
                            "data":[{{ implode(', ', $students_per_province) }}],
                            "fill":false,
                            "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                            "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],
                            "borderWidth":1}]},"options":{"scales":{"yAxes":[{"ticks":{"beginAtZero":true}}]}}});
    </script>
    @endif


    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.js"
            integrity="sha256-vtAyL1164tJW23BstoGrdXyOXvBR47n1PoKtlT0CEdE=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote.min.js"
            integrity="sha256-uvqllsjdlExrP0kB/YNZeK+Lt/Knmkf3bPJK+uDiQ8Q=" crossorigin="anonymous"></script>
    <script>
        $(function () {
            $(".select2, select").select2();
            $(".select2").css({
                "width" : "100%"
            });
            $("* #dataTable").dataTable();
            $(".summernote").summernote();
            $(".summernote-sm").summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ol', 'paragraph']],
                    ['misc', ['fullscreen']]
                ]
            });
            $("* #dataTableBtn").dataTable({
                dom: 'Blfrtip',
                pager: true,
                "ordering": true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
            $('#rootwizard').bootstrapWizard();
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd'
            });
            $('#assignQualifications').on('show.bs.modal', function (e) {
                var $invoker = $(e.relatedTarget);
                let id = $invoker.data('school');
                $("#schoolModalList").val(id).trigger('change');
            });
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '15%' // optional
            });
        });
    </script>
@endsection