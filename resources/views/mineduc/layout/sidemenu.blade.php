@if(auth()->guard('wda')->check())
    <li class="{{ activeMenu('wda.home') }}">
        <a href="{{ route('wda.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>
    @if(wdaAllowed(1) || wdaAllowed(3))
        <li class="treeview {{ activeMenu([
    'wda.schools', 'wda.edit.school', 'wda.view.school',
    'wda.school.internet','wda.school.electricity', 'wda.school.water',
    'wda.accr.settings', 'wda.accr.edit.settings',
    'wda.school.assign.qualification', 'wda.school.create', 'wda.editAllSchools', 'wda.school.costoftrainings'
    ]) }}">
            <a href="#">
                <i class="fa fa-graduation-cap"></i>
                <span>Schools Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="treeview {{ activeMenu([
                'wda.schools',  'wda.edit.school', 'wda.view.school',
                'wda.school.internet','wda.school.electricity', 'wda.school.water',
                'wda.school.assign.qualification', 'wda.school.create', 'wda.editAllSchools', 'wda.school.costoftrainings'
                ]) }}">
                    <a href="#">
                        <i class="fa fa-table"></i>
                        <span>Manage School</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ activeMenu([
                            'wda.schools',  'wda.edit.school', 'wda.view.school',
                            'wda.school.internet','wda.school.electricity', 'wda.school.water',
                            'wda.school.assign.qualification', 'wda.school.create'
                        ]) }}">
                            <a href="{{ route('wda.schools') }}">
                                <i class="fa fa-graduation-cap"></i><span>&nbsp;View Schools</span>
                            </a>
                        </li>
                        {{--<li class="{{ activeMenu([ 'wda.editAllSchools' ]) }}">--}}
                        {{--<a href="{{ route('wda.editAllSchools') }}"><i class="fa fa-edit"></i><span>&nbsp;Edit Schools Info</span></a>--}}
                        {{--</li>--}}
                        <li class="{{ activeMenu(['wda.school.costoftrainings']) }}">
                            <a href={{ route('wda.school.costoftrainings') }}><i class="fa fa-industry"></i><span>&nbsp;Costs Of Trainings</span></a>
                        </li>
                        {{--<li class="">--}}
                        {{--<a href=""><i class="fa fa-industry"></i><span>&nbsp;Infrastructure</span></a>--}}
                        {{--</li>--}}
                        {{--<li class="">--}}
                        {{--<a href=""><i class="fa fa-beer"></i><span>&nbsp;Workshops</span></a>--}}
                        {{--</li>--}}
                        {{--<li class="">--}}
                        {{--<a href=""><i class="fa fa-file-archive-o"></i><span>&nbsp;Attachments</span></a>--}}
                        {{--</li>--}}
                    </ul>
                </li>

            </ul>
        </li>
    @endif

    {{--Student Management--}}
    @if(wdaAllowed(1) || wdaAllowed(3))
        <li class="treeview {{ activeMenu([
        'wda.studentmanagement.index', 'wda.studentmanagement.edit', 'wda.studentmanagement.show'
        ]) }}">
            <a href="#">
                <i class="fa fa-book"></i>
                <span>Student Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['wda.district.index', 'wda.district.edit', 'wda.district.show']) }}">
                    <a href="{{ route('wda.studentmanagement.index') }}"><i class="fa fa-graduation-cap"></i><span>View Students</span></a>
                </li>
            </ul>
        </li>
    @endif

    {{-- Quality Management --}}
    @if(wdaAllowed(1))
        <li class="treeview {{ activeMenu([
        'wda.accr.application', 'wda.view.accr.application',
        'wda.school.assessment', 'wda.view.school.assessment', 'wda.sorting.school.assessment',
        'wda.manage.edit.attachment', 'wda.manage.attachments',
        'wda.manage.edit.infrastructure', 'wda.manage.infrastructures',
        'wda.manage.criteria','wda.manage.edit.criteria',
        'wda.manage.indicators', 'wda.manage.edit.indicator',
        'wda.manage.quality.area', 'wda.manage.edit.quality.area',
        'wda.qm.manual', 'wda.edit.timeline','wda.import.export.format'
        ]) }}">
            <a href="#">
                <i class="fa fa-random"></i>
                <span>Quality Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="treeview {{ activeMenu([
                'wda.accr.application', 'wda.view.accr.application',
                'wda.school.assessment', 'wda.view.school.assessment', 'wda.sorting.school.assessment',
                'wda.manage.edit.infrastructure', 'wda.manage.infrastructures', 'wda.import.export.format'
                ]) }}">
                    <a href="#">
                        <i class="fa fa-question-circle"></i>
                        <span>Assessment</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ activeMenu([ 'wda.accr.application', 'wda.view.accr.application' ])}}">
                            <a href="{{ route('wda.accr.application') }}"><i
                                        class="fa fa-link"></i><span>School Application</span></a>
                        </li>
                        <li class="treeview {{ activeMenu(['wda.school.assessment',
                        'wda.view.school.assessment', 'wda.import.export.format',
                        'wda.sorting.school.assessment'])}}">
                            <a href="{{ route('wda.school.assessment') }}">
                                <i class="fa fa-history"></i>
                                <span>School Assessment</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['wda.school.assessment',
                                'wda.view.school.assessment',
                                'wda.sorting.school.assessment'])}}">
                                    <a href="{{ route('wda.school.assessment') }}"><i
                                                class="fa fa-link"></i><span>All Assessment</span></a>
                                </li>
                                <li class="{{ activeMenu(['wda.import.export.format'])}}">
                                    <a href="{{ route('wda.import.export.format') }}"><i
                                                class="fa fa-link"></i><span>Export/Import</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="treeview {{
            activeMenu([
                'wda.manage.edit.attachment', 'wda.manage.attachments',
                'wda.manage.edit.infrastructure', 'wda.manage.infrastructures',
                'wda.manage.criteria','wda.manage.edit.criteria',
                'wda.manage.indicators', 'wda.manage.edit.indicator',
                'wda.manage.quality.area', 'wda.manage.edit.quality.area'
            ]) }}">
                    <a href="#">
                        <i class="fa fa-database"></i>
                        <span>Manage Data</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview {{
                        activeMenu([
                            'wda.manage.edit.attachment', 'wda.manage.attachments',
                            'wda.manage.edit.infrastructure', 'wda.manage.infrastructures',
                        ]) }}">
                            <a href="#">
                                <i class="fa fa-product-hunt"></i>
                                <span>School Applications</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['wda.manage.edit.attachment', 'wda.manage.attachments' ]) }}">
                                    <a href="{{ route('wda.manage.attachments') }}"><i class="fa fa-link"></i><span>Attachment Name</span></a>
                                </li>
                                <li class="{{ activeMenu(['wda.manage.edit.infrastructure', 'wda.manage.infrastructures']) }}">
                                    <a href="{{ route('wda.manage.infrastructures') }}">
                                        <i class="fa fa-link"></i><span>Infrastructure Name</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="treeview {{ activeMenu( [
                        'wda.manage.criteria','wda.manage.edit.criteria',
                        'wda.manage.indicators', 'wda.manage.edit.indicator',
                        'wda.manage.quality.area', 'wda.manage.edit.quality.area'
                        ])}}">
                            <a href="#">
                                <i class="fa fa-android"></i>
                                <span>Process & Inputs</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['wda.manage.criteria','wda.manage.edit.criteria']) }}">
                                    <a href="{{ route('wda.manage.criteria') }}"><i
                                                class="fa fa-link"></i><span>Criteria</span></a>
                                </li>

                                <li class="{{ activeMenu(['wda.manage.indicators', 'wda.manage.edit.indicator']) }}">
                                    <a href="{{ route('wda.manage.indicators') }}">
                                        <i class="fa fa-link"></i><span>Indicators</span></a>
                                </li>

                                <li class="{{ activeMenu(['wda.manage.quality.area', 'wda.manage.edit.quality.area']) }}">
                                    <a href="{{ route('wda.manage.quality.area') }}">
                                        <i class="fa fa-link"></i><span>Quality Areas</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="{{ activeMenu(['wda.qm.manual', 'wda.edit.timeline']) }}">
                    <a href="{{ route('wda.qm.manual') }}">
                        <i class="fa fa-anchor"></i><span>QM Manual</span>
                    </a>
                </li>

                <li class="{{ activeMenu(['wda.accr.settings', 'wda.accr.edit.settings']) }}">
                    <a href="{{ route('wda.accr.settings') }}">
                        <i class="fa fa-cogs"></i><span>General Settings</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif
    {{-- Staffs Management --}}
    @if(wdaAllowed(1))
        <li class="treeview {{ activeMenu(['wda.staff.list']) }}">
            <a href="{{ route('wda.staff.list') }}">
                <i class="fa fa-stumbleupon"></i>
                <span>Teachers Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['wda.staff.list']) }}">
                    <a href="{{ route('wda.staff.list') }}"><i class="fa fa-link"></i><span>All Teachers</span></a>
                </li>
                {{-- <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Academic Qualifications</span></a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Teaching Experience</span></a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Work Experience</span></a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Medical Information</span></a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Permissions</span></a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Attendance</span></a>
                </li> --}}
                {{-- <li class="treeview">
                    <a href="#">
                        <i class="fa fa-neuter"></i>
                        <span>Other</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="">
                            <a href=""><i class="fa fa-link"></i><span>Attachments</span></a>
                        </li>
                        <li class="">
                            <a href=""><i class="fa fa-link"></i><span>Languages</span></a>
                        </li>
                        <li class="">
                            <a href=""><i class="fa fa-link"></i><span>Computer Skills</span></a>
                        </li>
                        <li class="">
                            <a href=""><i class="fa fa-link"></i><span>Pedagogical</span></a>
                        </li>
                        <li class="">
                            <a href=""><i class="fa fa-link"></i><span>Industrial Attachment</span></a>
                        </li>
                    </ul>
                </li> --}}
                {{-- <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Manage Data</span></a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-link"></i><span>Module Settings</span></a>
                </li> --}}
            </ul>
        </li>
    @endif
    {{-- Studnets Management --}}

    {{-- <li class="treeview">
        <a href="#">
            <i class="fa fa-safari"></i>
            <span>Students Management</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Manage Students</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Manage Alumni</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Permissions</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Work Experience</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Industrial Attachment</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Attendance</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Discipline</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Manage Parents</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-link"></i><span>Module Settings</span></a>
            </li>
        </ul>
    </li> --}}

    {{-- Curricula Management --}}
    @if(wdaAllowed(1) || wdaAllowed(2))
        <li class="treeview {{
        activeMenu([
            'wda.curricula.index','wda.curricula.edit',
            'wda.sectors.index', 'wda.sectors.edit',
            'wda.trades.index', 'wda.trades.edit',
            'wda.rtqfs.index', 'wda.rtqfs.edit',
            'wda.curriculum.index', 'wda.curriculum.edit', 'wda.curriculum.view',
            'wda.modules.index', 'wda.modules.edit',
            'wda.modules.category.index', 'wda.modules.category.edit', 'wda.modules.uploading',
            'wda.curricula.view.schools', 'wda.curricula.view.students', 'wda.curricula.view.school.students'
        ]) }}">
            <a href="#">
                <i class="fa fa-sticky-note"></i>
                <span>Curricula Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['wda.curricula.index', 'wda.curricula.edit']) }}">
                    <a href="{{ route('wda.curricula.index') }}">
                        <i class="fa fa-table"></i><span>View Curricula</span>
                    </a>
                </li>
                <li class="treeview {{ activeMenu([
                    'wda.sectors.index', 'wda.sectors.edit',
                    'wda.trades.index', 'wda.trades.edit',
                    'wda.rtqfs.index', 'wda.rtqfs.edit',
                    'wda.curriculum.index', 'wda.curriculum.edit', 'wda.curriculum.view',
                    'wda.modules.index', 'wda.modules.edit',
                    'wda.modules.category.index', 'wda.modules.category.edit', 'wda.modules.uploading',
                    'wda.curricula.view.schools', 'wda.curricula.view.students', 'wda.curricula.view.school.students'
                ]) }}">
                    <a href="#">
                        <i class="fa fa-database"></i>
                        <span>Data Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ activeMenu(['wda.sectors.index', 'wda.sectors.edit']) }}">
                            <a href="{{ route('wda.sectors.index') }}">
                                <i class="fa fa-link"></i><span>Sectors</span>
                                <span class="pull-right-container">
                                <span class="label label-primary pull-right">{{ App\Model\Accr\TrainingSectorsSource::count() }}</span>
                            </span>
                            </a>
                        </li>

                        <li class="{{ activeMenu(['wda.trades.index', 'wda.trades.edit']) }}">
                            <a href="{{ route('wda.trades.index') }}">
                                <i class="fa fa-link"></i><span>Sub Sector</span>
                                <span class="pull-right-container">
                                <span class="label label-primary pull-right">{{ App\Model\Accr\TrainingTradesSource::count() }}</span>
                            </span>
                            </a>
                        </li>

                        <li class="{{ activeMenu(['wda.rtqfs.index', 'wda.rtqfs.edit']) }}">
                            <a href="{{ route('wda.rtqfs.index') }}">
                                <i class="fa fa-link"></i><span>RTQF</span>
                            </a>
                        </li>
                        <li class="{{ activeMenu(['wda.curriculum.index', 'wda.curriculum.edit', 'wda.curriculum.view', 'wda.curricula.view.schools', 'wda.curricula.view.students', 'wda.curricula.view.school.students']) }}">
                            <a href="{{ route('wda.curriculum.index') }}">
                                <i class="fa fa-link"></i><span>Qualification</span>
                                <span class="pull-right-container">
                                <span class="label label-primary pull-right">{{ App\Model\Accr\CurriculumQualification::count() }}</span>
                            </span>
                            </a>
                        </li>
                        <li class="treeview {{ activeMenu([
                        'wda.modules.index', 'wda.modules.edit',
                        'wda.modules.category.index', 'wda.modules.category.edit', 'wda.modules.uploading'
                        ]) }}">
                            <a href="#">
                                <i class="fa fa-angellist"></i>
                                <span>Modules Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['wda.modules.category.index', 'wda.modules.category.edit']) }}">
                                    <a href="{{ route('wda.modules.category.index') }}">
                                        <i class="fa fa-link"></i><span>Categories</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.modules.index', 'wda.modules.edit', 'wda.modules.uploading']) }}">
                                    <a href="{{ route('wda.modules.index') }}">
                                        <i class="fa fa-link"></i><span>Modules</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    @endif
    @if(wdaAllowed(1))
        <li class="treeview {{ activeMenu([
            'wda.sp.monitoring',
            'wda.sp.rp.index', 'wda.sp.rp.edit',
            'wda.sp.programs.index', 'wda.sp.programs.edit',
            'wda.sp.results.index', 'wda.sp.results.edit',
            'wda.sp.indicators.index', 'wda.sp.indicators.edit',
            'wda.sp.report.index', 'wda.sp.report.edit', 'wda.sp.report.result',
            'wda.sp.report.more', 'wda.sp.report.indicator',
            'wda.ap.report.index', 'wda.ap.report.older', 'wda.ap.report.edit',
            'wda.ap.report.post.older', 'wda.ap.report.result', 'wda.ap.report.print',
            'wda.ap.indicators.index', 'wda.ap.indicators.edit',
            'wda.ap.results.index', 'wda.ap.results.edit',
            'wda.ap.programs.index', 'wda.ap.programs.edit',
            'wda.ap.rp.index', 'wda.ap.rp.edit',
            'wda.settings', 'wda.settings.edit',
            'wda.pr.report.index', 'wda.pr.report.edit',
            'wda.pr.report.result', 'wda.pr.export.results',
            'wda.pr.export',
            'wda.pr.dev.partners.index', 'wda.pr.dev.partners.edit',
            'wda.pr.fin.types.index', 'wda.pr.fin.types.edit',
            'wda.pr.info.index', 'wda.pr.info.edit',
            'wda.pr.programs.index', 'wda.pr.programs.edit',
            'wda.pr.results.index', 'wda.pr.results.edit',
            'wda.pr.indicators.index', 'wda.pr.indicators.edit',
        ]) }}">
            <a href="#">
                <i class="fa fa-hashtag"></i>
                <span>Monitoring & Evaluation</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['wda.sp.monitoring']) }}">
                    <a href="{{ route('wda.sp.monitoring') }}">
                        <i class="fa fa-table"></i><span>Overview</span>
                    </a>
                </li>
                <li class="treeview {{ activeMenu([
                    'wda.sp.rp.index', 'wda.sp.rp.edit',
                    'wda.sp.programs.index', 'wda.sp.programs.edit',
                    'wda.sp.results.index', 'wda.sp.results.edit',
                    'wda.sp.indicators.index', 'wda.sp.indicators.edit',
                    'wda.sp.report.index', 'wda.sp.report.edit', 'wda.sp.report.result', 'wda.sp.report.more', 'wda.sp.report.indicator'
                ]) }}">
                    <a href="#">
                        <i class="fa fa-paper-plane"></i>
                        <span>Strategic Plan</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ activeMenu(['wda.sp.report.index', 'wda.sp.report.edit', 'wda.sp.report.result', 'wda.sp.report.more', 'wda.sp.report.indicator']) }}">
                            <a href="{{ route('wda.sp.report.index') }}">
                                <i class="fa fa-link"></i><span>View Strategic Plan</span>
                            </a>
                        </li>
                        <li class="treeview {{ activeMenu([
                            'wda.sp.rp.index', 'wda.sp.rp.edit',
                            'wda.sp.programs.index', 'wda.sp.programs.edit',
                            'wda.sp.results.index', 'wda.sp.results.edit',
                            'wda.sp.indicators.index', 'wda.sp.indicators.edit',
                        ]) }}">
                            <a href="#">
                                <i class="fa fa-database"></i>
                                <span>Data Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['wda.sp.rp.index', 'wda.sp.rp.edit']) }}">
                                    <a href="{{ route('wda.sp.rp.index') }}"><i class="fa fa-link"></i><span>Reporting Period</span></a>
                                </li>
                                <li class="{{ activeMenu(['wda.sp.programs.index', 'wda.sp.programs.edit']) }}">
                                    <a href="{{ route('wda.sp.programs.index') }}"><i
                                                class="fa fa-link"></i><span>Programs</span></a>
                                </li>
                                <li class="{{ activeMenu(['wda.sp.results.index', 'wda.sp.results.edit']) }}">
                                    <a href="{{ route('wda.sp.results.index') }}"><i
                                                class="fa fa-link"></i><span>Results</span></a>
                                </li>
                                <li class="{{ activeMenu(['wda.sp.indicators.index', 'wda.sp.indicators.edit']) }}">
                                    <a href="{{ route('wda.sp.indicators.index') }}"><i class="fa fa-link"></i><span>Indicators</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="treeview {{ activeMenu([
                    'wda.ap.report.index', 'wda.ap.report.older', 'wda.ap.report.edit',
                    'wda.ap.report.post.older', 'wda.ap.report.result', 'wda.ap.report.print',
                    'wda.ap.indicators.index', 'wda.ap.indicators.edit',
                    'wda.ap.results.index', 'wda.ap.results.edit',
                    'wda.ap.programs.index', 'wda.ap.programs.edit',
                    'wda.ap.rp.index', 'wda.ap.rp.edit'
                ]) }}">
                    <a href="#">
                        <i class="fa fa-paper-plane-o"></i>
                        <span>Action Plan</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ activeMenu('wda.ap.report.index') }}">
                            <a href="{{ route('wda.ap.report.index') }}">
                                <i class="fa fa-link"></i><span>View Action Plan</span>
                            </a>
                        </li>
                        <li class="treeview {{ activeMenu([
                            'wda.ap.indicators.index', 'wda.ap.indicators.edit',
                            'wda.ap.results.index', 'wda.ap.results.edit',
                            'wda.ap.programs.index', 'wda.ap.programs.edit',
                            'wda.ap.rp.index', 'wda.ap.rp.edit'
                        ]) }}">
                            <a href="#">
                                <i class="fa fa-database"></i>
                                <span>Data Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['wda.ap.rp.index', 'wda.ap.rp.edit']) }}">
                                    <a href="{{ route('wda.ap.rp.index') }}">
                                        <i class="fa fa-link"></i><span>Reporting Period</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.ap.programs.index', 'wda.ap.programs.edit']) }}">
                                    <a href="{{ route('wda.ap.programs.index') }}">
                                        <i class="fa fa-link"></i><span>Programs</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.ap.results.index', 'wda.ap.results.edit']) }}">
                                    <a href="{{ route('wda.ap.results.index') }}">
                                        <i class="fa fa-link"></i><span>Results</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.ap.indicators.index', 'wda.ap.indicators.edit']) }}">
                                    <a href="{{ route('wda.ap.indicators.index') }}">
                                        <i class="fa fa-link"></i><span>Indicators</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="treeview {{ activeMenu([
                    'wda.pr.report.index', 'wda.pr.report.edit',
                    'wda.pr.report.result', 'wda.pr.export.results',
                    'wda.pr.export',
                    'wda.pr.dev.partners.index', 'wda.pr.dev.partners.edit',
                    'wda.pr.fin.types.index', 'wda.pr.fin.types.edit',
                    'wda.pr.info.index', 'wda.pr.info.edit',
                    'wda.pr.programs.index', 'wda.pr.programs.edit',
                    'wda.pr.results.index', 'wda.pr.results.edit',
                    'wda.pr.indicators.index', 'wda.pr.indicators.edit'
                ]) }}">
                    <a href="#">
                        <i class="fa fa-server"></i>
                        <span>Program / Projects</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ activeMenu('wda.pr.report.index') }}">
                            <a href="{{ route('wda.pr.report.index') }}">
                                <i class="fa fa-link"></i><span>View Project</span>
                            </a>
                        </li>
                        <li class="treeview {{ activeMenu([
                            'wda.pr.dev.partners.index', 'wda.pr.dev.partners.edit',
                            'wda.pr.fin.types.index', 'wda.pr.fin.types.edit',
                            'wda.pr.info.index', 'wda.pr.info.edit',
                            'wda.pr.programs.index', 'wda.pr.programs.edit',
                            'wda.pr.results.index', 'wda.pr.results.edit',
                            'wda.pr.indicators.index', 'wda.pr.indicators.edit'
                        ]) }}">
                            <a href="#">
                                <i class="fa fa-database"></i>
                                <span>Data Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ activeMenu(['wda.pr.dev.partners.index', 'wda.pr.dev.partners.edit']) }}">
                                    <a href="{{ route('wda.pr.dev.partners.index') }}">
                                        <i class="fa fa-link"></i><span>Dev. Partners</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.pr.fin.types.index', 'wda.pr.fin.types.edit']) }}">
                                    <a href="{{ route('wda.pr.fin.types.index') }}">
                                        <i class="fa fa-link"></i><span>Finance Type</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.pr.info.index', 'wda.pr.info.edit']) }}">
                                    <a href="{{ route('wda.pr.info.index') }}">
                                        <i class="fa fa-link"></i><span>Project Info</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.pr.programs.index', 'wda.pr.programs.edit']) }}">
                                    <a href="{{ route('wda.pr.programs.index') }}">
                                        <i class="fa fa-link"></i><span>Interventions</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.pr.results.index', 'wda.pr.results.edit']) }}">
                                    <a href="{{ route('wda.pr.results.index') }}">
                                        <i class="fa fa-link"></i><span>Results</span>
                                    </a>
                                </li>
                                <li class="{{ activeMenu(['wda.pr.indicators.index', 'wda.pr.indicators.edit']) }}">
                                    <a href="{{ route('wda.pr.indicators.index') }}">
                                        <i class="fa fa-link"></i><span>Indicators</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="{{ activeMenu(['wda.settings', 'wda.settings.edit']) }}">
                    <a href="{{ route('wda.settings') }}">
                        <i class="fa fa-cogs"></i><span>General Settings</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif
    {{-- Examinations --}}

    {{-- <li class="treeview">
        <a href="#">
            <i class="fa fa-empire"></i>
            <span>Examinations</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="#"><i class="fa fa-stumbleupon"></i><span> REB Teachers</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-magic"></i><span> Markers</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-empire"></i><span> Assessors</span></a>
            </li>
        </ul>
    </li> --}}

    {{-- Financial Management --}}

    {{-- <li>
        <a href="#">
            <i class="fa fa-pagelines"></i>
            <span>Financial Management</span>
        </a>
    </li> --}}

    {{-- Marks Management --}}

    {{-- <li>
        <a href="#">
            <i class="fa fa-address-card"></i>
            <span>Marks Management</span>
        </a>
    </li> --}}

    {{-- Admissions and Registration --}}

    {{-- <li class="treeview">
        <a href="#">
            <i class="fa fa-krw"></i>
            <span>RP Students</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-star-half"></i>
                    <span>Admissions</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">

                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-star"></i>
                    <span>Registration</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">

                </ul>
            </li>

        </ul>
    </li> --}}

    {{-- User Management --}}
    @if(wdaAllowed(1))
        <li class="treeview {{ activeMenu([
        'wda.district.index', 'wda.district.edit', 'wda.district.show'
        ]) }}">
            <a href="#">
                <i class="fa fa-users"></i>
                <span>User Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                {{-- <li>
                    <a href="#"><i class="fa fa-stumbleupon"></i><span>Schools Users</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-magic"></i><span>Examiner Users</span></a>
                </li> --}}
                <li class="{{ activeMenu(['wda.district.index', 'wda.district.edit', 'wda.district.show']) }}">
                    <a href="{{ route('wda.district.index') }}"><i
                                class="fa fa-xing"></i><span>District Users</span></a>
                </li>
                {{-- <li>
                    <a href="#"><i class="fa fa-empire"></i><span>WDA Users</span></a>
                </li> --}}
            </ul>
        </li>
    @endif
    @if(wdaAllowed(1) || wdaAllowed(4))

        {{-- User Management --}}

        <li class="treeview {{ activeMenu([
        'wda.calendar.index', 'wda.calendar.edit',
        'wda.calendar.show', 'wda.attachments.index',
        'wda.attachments.upload', 'wda.audit.index',
        'notifications.index', 'notifications.edit', 'notifications.show'
        ]) }}">
            <a href="#">
                <i class="fa fa-star"></i>
                <span>Others</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['wda.calendar.index', 'wda.calendar.show', 'wda.calendar.edit']) }}">
                    <a href="{{ route('wda.calendar.index') }}"><i
                                class="fa fa-calendar"></i><span>Event Calendar</span></a>
                </li>
                <li class="{{ activeMenu(['wda.uploadstudents.index']) }}">
                    <a href="{{ route('wda.uploadstudents.index') }}"><i class="fa fa-upload"></i><span>Mass Uploads Students</span></a>
                </li>
                <li class="{{ activeMenu(['wda.attachments.index']) }}">
                    <a href="{{ route('wda.attachments.index') }}"><i class="fa fa-upload"></i><span>Attachments</span></a>
                </li>
                @if(wdaAllowed(1))
                    <li class="{{ activeMenu(['wda.audit.index']) }}">
                        <a href="{{ route('wda.audit.index') }}"><i
                                    class="fa fa-calendar"></i><span>Audit Log</span></a>
                    </li>
                @endif
                <li class="{{ activeMenu(['notifications.index', 'notifications.edit', 'notifications.show']) }}">
                    <a href="{{ route('notifications.index') }}"><i class="fa fa-bell"></i><span>Notice Board</span></a>
                </li>
            </ul>
        </li>
    @endif
    @if(wdaAllowed(1))
        <li class="treeview {{ activeMenu([
        'wda.nationalexams.index', 'wda.nationalexams.edit',
        'wda.nationalexams.show', 'wda.nationalexams.index',
        'wda.combinations.index',
        'wda.combinations.show', 'wda.combinations.edit',
        'wda.combinations.destroy','wda.oldcourses.index',
        'wda.oldcourses.show', 'wda.oldcourses.edit',
        'wda.oldcourses.destroy'
        ]) }}">
            <a href="#">
                <i class="fa fa-star"></i>
                <span>Exam Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['wda.grading.index', 'wda.grading.show', 'wda.grading.edit','wda.grading.destroy']) }}">
                    <a href="{{ route('wda.grading.index')}}">
                        <i class="fa fa-hand-grab-o"></i><span>Grading</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.weightscale.index', 'wda.weightscale.show', 'wda.weightscale.edit','wda.weightscale.destroy']) }}">
                    <a href="{{ route('wda.weightscale.index')}}">
                        <i class="fa fa-graduation-cap"></i><span>Weight Scale</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.combinations.index', 'wda.combinations.show', 'wda.combinations.edit','wda.combinations.destroy']) }}">
                    <a href="{{ route('wda.combinations.index')}}">
                        <i class="fa fa-camera-retro"></i><span>Options (Programs)</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.oldcourses.index', 'wda.oldcourses.show', 'wda.oldcourses.edit','wda.oldcourses.destroy']) }}">
                    <a href="{{ route('wda.oldcourses.index')}}">
                        <i class="fa fa-caret-square-o-left"></i><span>Modules / Subjects</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.nationalexams.index', 'wda.nationalexams.show', 'wda.nationalexams.edit']) }}">
                    <a href="{{ route('wda.nationalexams.search')}}">
                        <i class="fa fa-search"></i><span>Search Student</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.nationalexams.index', 'wda.nationalexams.show', 'wda.nationalexams.edit']) }}">
                    <a href="{{ route('wda.nationalexams.index')}}">
                        <i class="fa fa-hashtag"></i><span>Uploads Marks</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.uploadedmarks']) }}">
                    <a href="{{ route('wda.uploadedmarks')}}">
                        <i class="fa fa-backward"></i><span>Uploaded Marks</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.nationalexams.']) }}">
                    <a href="{{ route('wda.nationalexams.overallreport')}}">
                        <i class="fa fa-pie-chart"></i><span>Overall Report</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.nationalexams.perprogram']) }}">
                    <a href="{{ route('wda.nationalexams.perprogram')}}">
                        <i class="fa fa-pie-chart"></i><span>Report Per Program</span>
                    </a>
                </li>
                <li class="{{ activeMenu(['wda.nationalexams.registered']) }}">
                    <a href="{{ route('wda.nationalexams.registered')}}">
                        <i class="fa fa-bar-chart-o"></i><span>Registered Candidates</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

@endif