@extends('admin.layout.main')

@section('l-style')

@endsection

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header">
                    <button type="button" class="btn btn-primary"
                            data-toggle="modal" data-target="#modal-wda">
                        Register New User
                    </button>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th class="text-center" width="50px">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Permission</th>
                            <th>Created</th>
                            <th>Last Update</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($x = 1)
                        @forelse($wdas as $user)
                            <tr>
                                <td class="text-center">{{ $x++ }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ getWDAPermission($user->permission) }}</td>
                                <td>{{ $user->created_at->diffForHumans() }}</td>
                                <td>{{ $user->updated_at->diffForHumans() }}</td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['admin.wda.destroy', $user->id]]) !!}
                                    <button
                                            onclick="return window.confirm('Are you sure you want to remove :\n{{ $user->name }} ?');"
                                            class="btn btn-sm btn-danger pull-right">Delete
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-wda" tabindex="-1" role="dialog" aria-labelledby="modalWda">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Register WDA User</h4>
                </div>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/wda/register') }}">
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                       autofocus required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                            <label for="permission" class="col-md-4 control-label">Permission</label>

                            <div class="col-md-6">
                                {!! Form::select('permission', getWDAPermission(), null, ['class' => 'form-control select2', 'style' => 'width: 100%;', 'placeholder' => 'Select Here ...', 'required' => true]) !!}
                                @if ($errors->has('permission'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permission') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('l-scripts')
    @parent
    @if($errors->count() > 0)
        <script>
            $(function () {
                $(window).on('load', function () {
                    $('#modal-wda').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                });
            });
        </script>
    @endif
@endsection