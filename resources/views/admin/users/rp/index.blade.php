@extends($ext)

@section('l-style')

@endsection

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header">
                    <button type="button" class="btn btn-primary"
                            data-toggle="modal" data-target="#modal-wda">
                        Register New User
                    </button>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th class="text-center" width="50px">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Permission</th>
                            <th>Created</th>
                            <th>Last Update</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($x = 1)
                        @forelse($users as $user)
                            <tr>
                                <td style="vertical-align: middle;" class="text-center">{{ $x++ }}</td>
                                <td style="vertical-align: middle;">{{ $user->name }}</td>
                                <td style="vertical-align: middle;">{{ $user->email }}</td>
                                <td style="vertical-align: middle;">{{ getRPPermission($user->permission) }}</td>
                                <td style="vertical-align: middle;">{{ $user->created_at->diffForHumans() }}</td>
                                <td style="vertical-align: middle;">{{ $user->updated_at->diffForHumans() }}</td>
                                <td style="vertical-align: middle;">
                                    @if (auth()->guard('rp')->check() && !rpAllowed(3))
                                    @else
                                        <a href="{{ route($edit, $user->id) }}"
                                           class="btn btn-sm btn-info btn-block mb-1">Edit</a>
                                    @endif
                                    @if (auth()->guard('rp')->check() && !rpAllowed(3))
                                    @else
                                        {!! Form::open(['method' => 'DELETE', 'route' => [$destroy, $user->id]]) !!}
                                        <button
                                                onclick="return window.confirm('Are you sure you want to remove :\n{{ $user->name }} ?');"
                                                class="btn btn-sm btn-danger btn-block pull-right">Delete
                                        </button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-wda" tabindex="-1" role="dialog" aria-labelledby="modalWda">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ isset($umuntu) ? 'Update RP User' : 'Register RP User' }}</h4>
                </div>
                <form class="form-horizontal" role="form" method="POST"
                      action="{{ isset($umuntu) ? route($update, $umuntu->id) : url('/rp/register') }}">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        @isset($umuntu)
                            @method('PATCH')
                            {!! Form::hidden('user_id', $umuntu->id) !!}
                        @endisset
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                       name="name" value="{{ isset($umuntu) ? $umuntu->name : old('name')  }}"
                                       autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ isset($umuntu) ? $umuntu->email : old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                            <label for="permission" class="col-md-4 control-label">Permission</label>

                            <div class="col-md-6">
                                {!! Form::select('permission', getRPPermission(), isset($umuntu) ? $umuntu->permission : null, ['class' => 'form-control select2', 'style' => 'width: 100%;', 'placeholder' => 'Select Here ...', 'required' => true]) !!}
                                @if ($errors->has('permission'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permission') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit"
                                class="btn btn-primary">{{ isset($umuntu) ? 'Update' : 'Register' }}</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('l-scripts')
    @parent
    @if($errors->count() > 0 || isset($umuntu))
        <script>
            $(function () {
                $(window).on('load', function () {
                    $('#modal-wda').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                });
            });
        </script>
    @endif
@endsection