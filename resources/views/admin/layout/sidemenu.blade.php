@if(auth()->guard('admin')->check())
    <li class="{{ activeMenu('admin.home') }}">
        <a href="{{ route('admin.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>
    {{-- User Management --}}

    <li class="treeview {{ activeMenu([
            'admin.wda.index', 'admin.wda.edit', 'admin.wda.create', 'admin.wda.show',
            'admin.rp.index', 'admin.rp.edit', 'admin.rp.create', 'admin.rp.show',
            'admin.college.index', 'admin.college.edit', 'admin.college.create', 'admin.college.show',
            'admin.school.index', 'admin.school.edit', 'admin.school.create', 'admin.school.show',
            'admin.examiner.index', 'admin.examiner.edit', 'admin.examiner.create', 'admin.examiner.show'
        ]) }}">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>User Management</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="{{ activeMenu(['admin.wda.index', 'admin.wda.edit', 'admin.wda.create', 'admin.wda.show']) }}">
                <a href="{{ route('admin.wda.index') }}"><i class="fa fa-empire"></i><span>WDA Users</span></a>
            </li>
            <li class="{{ activeMenu(['admin.rp.index', 'admin.rp.edit', 'admin.rp.create', 'admin.rp.show']) }}">
                <a href="{{ route('admin.rp.index') }}"><i class="fa fa-stumbleupon"></i><span>RP Users</span></a>
            </li>
            {{--<li class="{{ activeMenu(['admin.college.index', 'admin.college.edit', 'admin.college.create', 'admin.college.show']) }}" disabled>--}}
                {{--<a href="{{ route('admin.college.index') }}"><i class="fa fa-adjust"></i><span>College Users</span></a>--}}
            {{--</li>--}}
            {{--<li class="{{ activeMenu(['admin.school.index', 'admin.school.edit', 'admin.school.create', 'admin.school.show']) }}" disabled>--}}
                {{--<a href="{{ route('admin.school.index') }}"><i class="fa fa-feed"></i><span>Schools Users</span></a>--}}
            {{--</li>--}}
            <li class="{{ activeMenu(['admin.examiner.index', 'admin.examiner.edit', 'admin.examiner.create', 'admin.examiner.show']) }}">
                <a href="{{ route('admin.examiner.index') }}"><i class="fa fa-magic"></i><span>Examiner Users</span></a>
            </li>
        </ul>
    </li>

@endif