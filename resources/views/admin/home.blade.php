@extends('admin.layout.main')

@section('l-style')

@endsection

@section('panel-body')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-default">
                <div class="box-header">Dashboard</div>

                <div class="box-body">
                    You are logged in as Admin!
                </div>
            </div>
        </div>
    </div>
@endsection
