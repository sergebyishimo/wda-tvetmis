<!DOCTYPE html>
<html>
<head>
    <title>Confirmed Invoice Created Successfully</title>
</head>
<body>

Hello {{ $data['names'] }}<br>

<p>An invoice has been created in your RP Student Profile with the following details</p>

<div>Student Code: <b>{{ $data['reg_no'] }}</b></div>
<div>College: <b>{{ $data['college'] }}</b></div>
<div>Department: <b>{{ $data['department'] }}</b></div>
<div>Option Studied: <b>{{ $data['program'] }}</b></div>
<div>Academic Year: <b>{{ $data['academic_year'] }}</b></div>
<div>Year Of Study: <b>{{ $data['year_of_study'] }} Year</b></div>
<div>Payment Name: <b>{{ $data['category'] }}</b></div>
<div>Invoice Number: <b>{{ $data['code'] }}</b></div>
<div>Total Amount to Pay : <b>{{ $data['amount'] }}</b></div>
<div>Invoice Date Created: <b>{{ $data['date_invoice'] }}</b></div>
<div>Sponsor: <b>{{ $data['sponsor'] }}</b></div>

<p>You can now proceed and make payment either through mobile payment by dialing * 720 # and choose option 4, or by
    going to make payment at any CogeBank brank</p>

<p>Sincerely</p>

<br>
<div>Rwanda Polytechnic</div>
<div>Phone: 0788889668/0783295189/0788752880/0788752880</div>
<div>Email: dmugume@rp.ac.rw</div>
</body>
</html>