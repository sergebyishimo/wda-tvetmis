<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<p>
    Hello {{$name}}
    <br>Thank you for sign up for an account using the Rwanda Polytechnic Student Registration System.
    <br>Your Student code is {{$stdid}}
</p>
<p>
<div>Names: <b>{{$name}}</b></div>
<div>College: <b>{{ isset($additional['college']) ? $additional['college'] : "" }}</b></div>
<div>Year of Study: <b>{{ isset($additional['year_of_study']) ? $additional['year_of_study'] : "" }}</b></div>
<div>Your Student code is: <b>{{ isset($additional['student_id']) ? $additional['student_id'] : "" }}</b></div>
<div>Department: <b>{{ isset($additional['department']) ? $additional['department'] : "" }}</b></div>
<div>Option: <b>{{ isset($additional['option']) ? $additional['option'] : "" }}</b></div>
<div>Academic Year: <b>{{ isset($additional['academic_year']) ? $additional['academic_year'] : "" }}</b></div>
</p>
<p>If any of the information above is not valid, please contact our help desk immediately for assistance.</p>
<p>To continue with Registration, please login to the Student portal through <a href="{{ url('www.mis.rp.ac.rw') }}"
                                                                                target="_blank">www.mis.rp.ac.rw</a> and
    follow the following steps.</p>
<p>
    Click on Payments Tab
    Select the payment you want to make and then Click on Generate Code
    On the next screen, Click on Confirm and Pay
    Proceed to make a payment of the registration fee using either mobile money (Instant Verification) through the code
    *720# and choosing menu #4 "Rwanda Polytechnic" and follow instructions or by paying through any of the Cogebank
    branches. Please note that payment is only valid if you use your student code as a reference and not your names, as
    this will be null and void (NOT VALID).
    Once your payment has been verified, login to the system again and click on Registration
    Proceed with your registration.
</p>
<p>
    For more information, you can see our FAQs at <a
            href="{{ url('www.solutions.rp.ac.rw') }}">www.solutions.rp.ac.rw</a> or submit a request for assistance.
</p>
<p>
    Thank you,<br>
    Rwanda Polytechnic
</p>
</body>
</html>