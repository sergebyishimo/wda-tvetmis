<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<p>
    Hello {{$name}}
</p>
<p>
    Thank you for sign up for an account using the Rwanda Polytechnic Student Admissions System.
</p>
Your Student code is {{$stdid}}
@if($additional)
    <p>
    <div>College: <b>{{ isset($additional['college']) ? ucwords($additional['college']) : "" }}</b></div>
    <div>Department: <b>{{ isset($additional['department']) ? ucwords($additional['department']) : "" }}</b></div>
    <div>Program: <b>{{ isset($additional['program']) ? ucwords($additional['program']) : "" }}</b></div>
    </p>
@endif
<p>
<div>You need to make an application fee payment of RWF 5,000 to proceed with your application. Follow the following
    steps
</div>
<div>
    <ol>
        <li>
            Login to the RP student’s portal through the link
            <a href="http://www.mis.rp.ac.rw/student/login">http://www.mis.rp.ac.rw/student/login</a>
        </li>
        <li>Click on Payments</li>
        <li>Under list of payments available, click on Generate invoice</li>
        <li>
            Once this is done, you can make payment by dialing on your phone * 720 # then choose Rwanda Polytechnic,
            which is Option <b>4.</b> Enter your student number and then choose the corresponding payment. After you make
            payment, you will get a confirmation message, and there after you can login to the student portal and click
            on Applications to Submit your application.
        </li>
    </ol>
</div>
</p>

</body>
</html>