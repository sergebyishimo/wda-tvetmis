<!DOCTYPE html>
<html>
<head>
    <title>Override Has Been Set</title>
</head>

<body>
<p>
    Hello
    <br>This is to confirm that an override has been set for your profile as follows
</p>
<div>Students Names: <b>{{ isset($data['names']) ? $data['names'] : '' }}</b></div>
<div>Student Code: <b>{{ isset($data['id']) ? $data['id'] : '' }}</b></div>
<div>College Name: <b>{{ isset($data['college']) ? $data['college'] : '' }}</b></div>
<div>Academic Year: <b>{{ date('Y') }} - {{ date('Y', strtotime('+1 year')) }}</b></div>
<div>Program Name: <b>{{ isset($data['program']) ? $data['program'] : '' }}</b></div>
<div>Department Name: <b>{{ isset($data['department']) ? $data['department'] : '' }}</b></div>
<div>Sponsor Name: <b>{{ isset($data['sponsor']) ? $data['sponsor'] : '' }}</b></div>
<div>Covered Payments</div>
<div>
    <table border="1">
        <thead>
        <tr>
            <th>Payment Category</th>
            <th>Covered Percentage</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($data['categories']))
            @foreach($data['categories'] as $item)
                <tr>
                    <td>{{ isset($item['category']) ? $item['category'] : '' }}</td>
                    <td>{{ isset($item['category']) ? $item['percentage']  : '0'}} %</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
</body>
</html>