<!DOCTYPE html>
<html>
<head>
    <title>Qualification Application</title>
</head>
<body>
School <b>{{ $data['school'] }}</b><br>
<p>
    This is to inform you that following your application for <b>{{ $data['type'] }}</b>, have been <b>{{ $data['action'] }}</b>
    with the following details
</p>

<div>Sector: <b>{{ $data['sector'] }}</b></div>
<div>Sub Sector: <b>{{ $data['sub_sector'] }}</b></div>
<div>Qualification: <b>{{ $data['qualification'] }}</b></div>
<div>Rtqf: <b>{{ $data['rtqf'] }}</b></div>
<div>Accreditation Type: <b>{{ $data['type'] }}</b></div>
<div>Decision: <b>{{ $data['action'] }}</b></div>
</body>
</html>