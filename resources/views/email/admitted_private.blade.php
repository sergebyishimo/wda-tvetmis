<!DOCTYPE html>
<html>
<head>
    <title>Confirmed Admitted</title>
</head>
<body>
Dear <b>{{ $data['names'] }}</b><br>
<p>
    This is to inform you that following your application for admission to Rwanda Polytechnic,
    you have been admitted with the following details
</p>

<div>Names: <b>{{ $data['names'] }}</b></div>
<div>StudentCode: <b>{{ $data['reg_no'] }}</b></div>
<div>Allocated College: <b>{{ $data['college'] }}</b></div>
<div>Allocated Department: <b>{{ $data['department'] }}</b></div>
<div>Allocated Course: <b>{{ $data['course'] }}</b></div>
<p>
    You can therefore login to the RP Students portal and download your admission letter. You are also informed that
    you can processed and register.
</p>
<p>
    Go to <a href="{{ url('http://www.mis.rp.ac.rw/') }}">RP MIS</a> and click on Students then <a
            href="{{ url('http://www.mis.rp.ac.rw/student/login') }}">Sign-in</a>
    Use the email and password used during application.
</p>
<br>
<div>Congratulations</div>
<div>Admissions team</div>
<div>Rwanda Polytechnic</div>
<div>Phone: 0788889668/0783295189/0788752880</div>
<div>Email: dmugume@rp.ac.rw</div>
</body>
</html>