<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ ucwords('Confirmation of assessor application') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #333333;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 80vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: left;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h3>Hello {{ ucwords($data['names'])  }}</h3>
        <p>
            Your application to become an assessor for National Examinations conducted by the Workforce Development
            Authority (WDA) for the academic year <b>{{ $data['academic_year'] }}</b> has been received. You have applied with the following
            details.
        </p>

        <div>Your School: <b>{{ $data['school'] }}</b></div>
        <div>Academic Year: <b>{{ $data['academic_year'] }}</b></div>
        <div>Trade applied for: <b>{{ $data['trade'] }}</b></div>
        <div>Assessed Before: <b>{{ $data['trade_assessed_before'] }}</b></div>

        <p>You will be notified of the results and further information after selection by the WDA.</p>
        <p>Sincerely</p>
    </div>
</div>
</body>
</html>
