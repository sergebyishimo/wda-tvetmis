<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>TVET Information System</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->

    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <link rel="stylesheet" src="//cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@yield('styles')
<!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>

</head>
<body class="hold-transition sidebar-mini sidebar-open">
{{--<div class="content-wrapper">--}}
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">
                <div class="card">
                    <div id="step-8">
                        <h5><i class="fa fa-check-circle"></i> Confirmation</h5>

                        <p>Please Confirm Below Information</p>

                        <h5><i class="fa fa-check-circle"></i>Accreditation Type</h5>
                        <div class="p-1 mb-3">{{ ucwords($data['accreditation_type']) }}</div>

                        <h5><i class="fa fa-check-circle"></i> Attachments Provided</h5>
                        <table class="table table-bordered table-hover">
                            <thead style="background: #efefef">
                            <tr>
                                <th style="width: 25%">#</th>
                                <th>Attachment Name</th>
                                <th style="width: 25%">Upload PDF or Image</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($data['attachments_data'])
                                @php
                                    $x = 1;
                                @endphp
                                @foreach($data['attachments_data'] as $ad)
                                    <tr>
                                        <td>{{ $x++ }}</td>
                                        <td>{{ $ad->source }}</td>
                                        <td><a href="{{ url(Storage::url("$ad->attachment"))}}">Download File</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                        <h5><i class="fa fa-check-circle"></i> Materials &amp; Infrastructure</h5>
                        <table class="table table-bordered table-hover">
                            <thead style="background: #efefef">
                            <tr>
                                <th>#</th>
                                <th>Building</th>
                                <th>Infrastructure</th>
                                <th>Purpose</th>
                                <th>Size</th>
                                <th>Capacity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $x = 1;
                            @endphp
                            @foreach ($data['buildings_plots'] as $bp)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>{{ $bp->name }}</td>
                                    <td>

                                        @foreach ($bp['infras'] as $infra)
                                            {{ $infra->quantity. ' ' .$infra->name .  ', ' }}
                                        @endforeach

                                    </td>
                                    <td>{{ $bp->purpose }}</td>
                                    <td>{{ $bp->size }}</td>
                                    <td>{{ $bp->capacity }}</td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                        <h5><i class="fa fa-check-circle"></i> Programs Offered</h5>
                        <table class="table table-bordered table-hover">
                            <thead style="background: #efefef">
                            <tr>
                                <th>#</th>
                                <th>Sector</th>
                                <th style="width: 15%">Sub-Sector</th>
                                <th style="width: 15%">Qualification</th>
                                <th style="width: 15%">RTQF Level</th>
                                <th style="width: 10%">Male Trainees</th>
                                <th style="width: 10%">Female Trainees</th>
                                <th>Curriculum</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach($data['programs_offered'] as $pd)

                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $pd->subsector->sector->tvet_field }}</td>
                                    <td>{{ $pd->subsector->sub_field_name }}</td>
                                    <td>{{ $pd->rtqf_level->level_name }}</td>
                                    <td>{{ $pd->qualification->qualification_title }}</td>
                                    <td>{{ $pd->male_trainees }}</td>
                                    <td>{{ $pd->female_trainees }}</td>
                                    <td>{{ $pd->curriculum }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                        <h5><i class="fa fa-check-circle"></i> Trainer / Teachers</h5>
                        <table class="table table-bordered table-hover">
                            <thead style="background: #efefef">
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th style="width: 10%">Gender</th>
                                <th>Level / Degree</th>
                                <th>Qualification</th>
                                <th>Certification</th>
                                <th>Experience</th>
                                <th>Modules Taught</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach($data['trainers'] as $td)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $td->names }}</td>
                                    <td>{{ $td->gender }}</td>
                                    <td>{{ $td->q_level->name }}</td>
                                    <td>{{ $td->qualification }}</td>
                                    <td>{{ $td->certification }}</td>
                                    <td>{{ $td->experience }}</td>
                                    <td>{{ $td->modules_taught }}</td>

                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                        <h5><i class="fa fa-check-circle"></i> Administration Staff</h5>
                        <table class="table table-bordered table-hover">
                            <thead style="background: #efefef">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Position</th>
                                <th>Qualification</th>
                                <th>Institution Studied</th>
                                <th>Qualification Level</th>
                                <th>Teaching Experience</th>
                                <th>Assessor Qualification</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach($data['staffs'] as $sd)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $sd->names }}</td>
                                    <td>{{ $sd->gender }}</td>
                                    <td>{{ $sd->position }}</td>
                                    <td>{{ $sd->academic_qualification }}</td>
                                    <td>{{ $sd->institution_studied }}</td>
                                    <td>{{ $sd->q_level->name }}</td>
                                    <td>{{ $sd->teaching_experience }}</td>
                                    <td>{{ $sd->assessor_qualification }}</td>

                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                        <h5><i class="fa fa-check-circle"></i> Qualification Applied For</h5>
                        <table class="table table-bordered table-hover">
                            <thead style="background: #efefef">
                            <tr>
                                <th>#</th>
                                <th style="width: 30%">Sector</th>
                                <th style="width: 20%">Sub Sector</th>
                                <th style="width: 20%">Curriculum Qualification</th>
                                <th>RTQF Level</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach($data['application_data'] as $ad)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $ad->curr->subsector->sector->tvet_field }}</td>
                                    <td>{{ $ad->curr->subsector->sub_field_name }}</td>
                                    <td>{{ $ad->curr->qualification_title }}</td>
                                    <td>{{ $ad->rtqf->level_name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--</div>--}}
<!-- Bootstrap -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- AdminLTE -->
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>


<script src="{{ asset('js/adminlte.js') }}"></script>
<script src="{{ asset('js/demo.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script type="text/javascript">
    $('.textarea').wysihtml5({
        toolbar: {fa: true}
    })

    $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
    $('[data-mask]').inputmask();

    $(document).ready(function () {
        $('#myTable').DataTable({
            dom: 'Bfrtip',
            ordering: false,
            searching: false,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
</script>
</body>
</html>