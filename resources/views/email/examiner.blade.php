<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ ucwords('WDA examiner account successfully created') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #333333;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 80vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: left;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h3>Hello {{ ucwords($data['name'])  }}</h3>
        <p>
            The WDA Examiner account have been created successfully.
        </p>

        <div>User this email to login: <b>{{ $data['email'] }}</b></div>
        <div>And this password: <b>{{ $data['password'] }}</b></div>
        <div>This is the link to long <a href="http://mis.rp.ac.rw/examiner/login" target="_blank">WDA Examiner login link</a>
        </div>
    </div>
</div>
</body>
</html>
