<!DOCTYPE html>
<html>
<head>
    <title>Confirmed Transfer Accepted</title>
</head>
<body>
Dear <b>{{ $data['names'] }}</b><br>
<p>
    Following your transfer request at Rwanda Polytechnic to <b>{{ $data['college'] }}</b> under the
    Program
    <b>{{ $data['course'] }}</b>, this is to inform you that your request has been <b>Accepted</b>.
</p>
<div>Details</div>
<div>Names: <b>{{ $data['names'] }}</b></div>
<div>StudentCode: <b>{{ $data['reg_no'] }}</b></div>
<div>To College: <b>{{ $data['college'] }}</b></div>
<div>To Department: <b>{{ $data['department'] }}</b></div>
<div>To Course: <b>{{ $data['course'] }}</b></div>
<hr>
<div>From College: <b>{{ $data['from_college'] }}</b></div>
<div>From Department: <b>{{ $data['from_department'] }}</b></div>
<div>From Course: <b>{{ $data['from_course'] }}</b></div>
<p>
    You can therefore login to the RP Students portal and download your admission letter. You are also informed that
    you can processed and register.
</p>
<p>
    Go to <a href="{{ url('http://www.mis.rp.ac.rw/') }}">RP MIS</a> and click on Students then <a
            href="{{ url('http://www.mis.rp.ac.rw/student/login') }}">Sign-in</a>
    Use the email and password used during application.
</p>
<br>
<div>Congratulations</div>
<div>Admissions team</div>
<div>Rwanda Polytechnic</div>
<div>Phone: 0788889668/0783295189/0788752880</div>
<div>Email: dmugume@rp.ac.rw</div>
</body>
</html>