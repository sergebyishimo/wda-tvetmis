<!DOCTYPE html>
<html>
<head>
    <title>Confirmed Registration</title>
</head>
<body>

Dear <br>

<p>This is to confirm your registration as follows</p>

<div>Student Names: <b>{{ $data['names'] }}</b></div>
<div>Student Code: <b>{{ $data['reg_no'] }}</b></div>
<div>College: <b>{{ $data['college'] }}</b></div>
<div>Department: <b>{{ $data['department'] }}</b></div>
<div>Option Studied: <b>{{ $data['course'] }}</b></div>
<div>Academic Year: <b>{{ $data['academic_year'] }}</b></div>
<div>Year Of Study: <b>{{ $data['year_of_study'] }} Year</b></div>
<div>Date of Registration: <b>{{ $data['date'] }}</b></div>

<p>No further steps are necessary</p>

<p>Thank you</p>
<p>Sincerely</p>

<br>

<div>Congratulations</div>
<div>Admissions team</div>
<div>Rwanda Polytechnic</div>
<div>Phone: 0788889668/0783295189/0788752880/0788752880</div>
<div>Email: dmugume@rp.ac.rw</div>
</body>
</html>