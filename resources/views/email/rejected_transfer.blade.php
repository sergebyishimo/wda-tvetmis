<!DOCTYPE html>
<html>
<head>
    <title>Confirmed Admitted</title>
</head>
<body>
Dear <b>{{ $data['names'] }}</b><br>
<p>
    Following your transfer request at Rwanda Polytechnic to <b>{{ $data['college'] }}</b> under the
    Program
    <b>{{ $data['course'] }}</b>, this is to inform you that your request has been <b>REJECTED</b>.
</p>
<p>
    We thank you for expressing interest in Studying at Rwanda Polytechnic
</p>
<br>
<div>Rwanda Polytechnic</div>
<div>Phone: 0788889668/0783295189/0788752880</div>
<div>Email: dmugume@rp.ac.rw</div>
</body>
</html>