<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

<style>

td,th {                                          
    padding: 2px !important;                            
                   
       
}

</style>
</head>
<body>
<nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144"
     style="background-color: rgb(255, 255, 255);">
    <div class="container">
        <div class="row bg-white p-2" style="width: inherit !important;">
            <div class="col-md-10">
                <div class="navbar-toggleable-sm">
                    <span class="navbar-logo"><a href="/"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                    <span><a class="navbar-brand" href="/">{{ config('app.name') }}</a></span>
                    {{-- <!-- Example single danger button -->
                    <a class="btn btn-sm btn-primary shadow-sm" href="{{ route('home.dashboard') }}">
                        <span>Dashboard</span> </a>
                    <a class="btn btn-sm btn-dark shadow-sm"
                       href="{{ route('curricula.index') }}">
                        <span>Curriculum</span>
                    </a>

                    <a class="btn btn-sm btn-info shadow-sm"
                       href="{{ route('home.schools') }}">
                        <span>Schools</span>
                    </a>

                    <span class="dropdown">
                        <button class="btn btn-sm btn-warning shadow-sm btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Support
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               target="_blank"
                               href="{{ url('https://solutions.rp.ac.rw') }}">Help Desk</a>
                            <a class="dropdown-item"
                               href="{{ route('contact.us') }}">Contact Us</a>
                        </div>
                    </span>

                    <a class="btn btn-sm btn-success shadow-sm" href="{{ route('home.attachments') }}">
                        <span>Downloads</span>
                    </a>

                    <a class="btn btn-sm btn-success shadow-sm" href="{{ url('https://gis.rp.ac.rw/') }}">
                        <span>GIS</span>
                    </a>

                    <a class="btn btn-sm btn-primary shadow-sm" href="{{ url('results') }}">
                        <span>Senior Six Results</span>
                    </a> --}}

                </div>
            </div>
        </div>
    </div>
</nav>
<section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
         style="background-image: url({{ asset('img/wlc_bg_n_4.jpg') }});background-attachment: fixed;">
    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="col-md-9" style="margin: 0 auto">
                    <div class="mbr-section-title display-5 text-dark bg-white shadow-md rounded p-2 pt-0">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10 col-sm-9">
                                        Check Senior Six Results
                                    </div>
                                    {{--<div class="col-md-2 col-sm-3">--}}
                                        {{--<button type="button" class="btn btn-outline-secondary btn-block" id="print">Print</button>--}}
                                    {{--</div>--}}
                                </div>


                            </div>
                            <div class="card-body">
                                {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => "results.view.2018"]) !!}
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            {!! Form::label('Index Number  (Format:  060505217ACC001)') !!}
                                            {!! Form::text('index_number', request('index_number'), ['class' => 'form-control', 'required' => true]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">&nbsp;</label>
                                        {!! Form::submit('Verify', ['class' => 'btn btn-sm btn-primary btn-block']) !!}
                                    </div>
                                </div>
                                <div id="printablearea">
                                {!! Form::close() !!}
                                @if(isset($results))
                                    <div class="row">
                                        <div class="col-md-12" style="display: none">
                                            <table class="table table-striped table-hover">
                                                <thead style="display: none">
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Year of Exam</td>
                                                    <td>: {{ $results->exam_year  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Family Name</td>
                                                    <td>: {{ $results->family_name  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Other Names</td>
                                                    <td>: {{ $results->other_names  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender</td>
                                                    <td>: {{ $results->sex  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Index Number</td>
                                                    <td>: {{ $results->index_number  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>School Name</td>
                                                    <td>: {{ $results->school_name  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Obtained Marks</td>
                                                    <td>: {{ $results->tot  }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Mention</td>
                                                    <td>: {{ $results->mention  }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        <div class="col-md-12">
                                        <img class="img-responsive" src="/images/wda2.png" width="90%" height="30%">                                                   
                                        <p class="text-center" style="font-size: 30px">TVET National Examination Results</p>
                                        <br><br>
                                        </div>
                                        <div class="col-md-12"  style="margin: 0 auto">
                                            <table class="table table-striped table-hover table-bordered">
                                               
                                                <tbody>
                                                        <tr>
                                                                <th colspan="2" style="background-color: orange">Student Identification </th>
                                                               
                                                                        
                                                            </tr>
                                                    <tr>
                                                        <td>NAMES:  </td>
                                                        <th>{{ $results->family_name  }} &nbsp&nbsp {{ $results->other_names  }}</th>
                                                                
                                                    </tr>
                                                    <tr>
                                                        <td>ACADEMIC YEAR : </td>
                                                        <th> {{ $results->exam_year  }}</th>
                                                       
                                                    </tr>  
                                                    <tr>
                                                        <td>INDEX NUMBER : </td>
                                                        <th> {{ $results->index_number  }}</th>
                                                    </tr> 
                                                    <tr>
                                                        <td>GENDER: </td>
                                                        <th> {{ $results->sex  }}</th>

                                                    </tr>
                                                    <tr>
                                                        <td>SCHOOL NAME : </td>
                                                        <th> {{ $results->school_name  }}</th>
                                                        
                                                    </tr>
                                                   
                                                </tbody>
                                            </table>
                                            <table class="table table-striped table-hover table-bordered" cellpadding="5" cellspacing="2">
                                                <tbody>
                                                    <tr style="background-color: orange">
                                                                <th>COURSE</th>
                                                                <th>GRADE</th>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ $results->exam1 }}</td>
                                                        <td>{{ $results->grade1 }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ $results->exam2 }}</td>
                                                        <td>{{ $results->grade2 }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ $results->exam3 }}</td>
                                                        <td>{{ $results->grade3 }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ $results->exam4 }}</td>
                                                        <td>{{ $results->grade4 }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ $results->exam5 }}</td>
                                                        <td>{{ $results->grade5 }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ $results->exam6 }}</td>
                                                        <td>{{ $results->grade6 }}</td>
                                                    </tr>
                                                    @if(isset($results->exam7) && $results->exam7 != '')
                                                        <tr>
                                                            <td>{{ $results->exam7 }}</td>
                                                            <td>{{ $results->grade7 }}</td>
                                                        </tr>
                                                        @endif
                                                        @if(isset($results->exam8) && $results->exam8 != '')    
                                                        <tr>
                                                            <td>{{ $results->exam8 }}</td>
                                                            <td>{{ $results->grade8 }}</td>
                                                        </tr>
                                                    @endif
                                                    @if(isset($results->exam9) && $results->exam9 != '')
                                                        <tr>
                                                            <td>{{ $results->exam9 }}</td>
                                                            <td>{{ $results->grade9 }}</td>
                                                        </tr>
                                                        @endif
                                                    @if(isset($results->exam10) && $results->exam10 != '')
                                                        <tr>
                                                            <td>{{ $results->exam10 }}</td>
                                                            <td>{{ $results->grade10 }}</td>
                                                        </tr>
                                                    @endif
                                                    @if(isset($results->exam11) && $results->exam11 != '')
                                                        <tr>
                                                            <td>{{ $results->exam11 }}</td>
                                                            <td>{{ $results->grade11 }}</td>
                                                        </tr>
                                                        @endif
                                                        @if(isset($results->exam12) && $results->exam12 != '')
                                                        <tr>
                                                            <td>{{ $results->exam12 }}</td>
                                                            <td>{{ $results->grade12 }}</td>
                                                        </tr>
                                                    @endif

                                                    @if(isset($results->exam13) && $results->exam13 != '')
                                                        <tr>
                                                            <td>{{ $results->exam13 }}</td>
                                                            <td>{{ $results->grade13 }}</td>
                                                        </tr>
                                                        
                                                    @endif
                                                </tbody>
                                            </table>
                                            <table class="table table-striped table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Mention:</th>
                                                        <th>{{ $results->mention  }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Total Aggregate:</th>
                                                        <th>{{ $results->tot }}</th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        </div>
                                       
                                    </div>
                                    <div style="margin: 0 auto">
                                    <button style="margin:initial" class="btn btn-primary" onclick="generatePDF()"></i> Print Result</button>
                                    <a href="/results/all" ><button style="margin: auto" class="btn btn-primary" > Go Back </button></a>
                                    </div>
                                @else
                                    {{-- <p style="color: red">Invalid Index Number.   Please Enter Another Valid Index Number .....</p> --}}
                                @endif
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script>
    $(document).ready( function () {
        var table = $("#resultsTable").DataTable({
            paging: false,
            searching: false,
            info: false,
            ordering: false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    className: 'fade',
                    text: 'Print in PDF',
                    filename: 'Senior Six Results',
                    title: 'Senior Six Results',
                    customize: function(doc) {
                        doc.styles.tableBodyEven = {
                            background: '#fff',
                        };

                        doc.styles.tableBodyOdd = {
                            background: '#fff',
                        };

                        doc.content[1].table.widths = [ '30%', '50%'];

                        var objLayout = {};
                        objLayout['paddingTop'] = function(i) { return 6; };
                        objLayout['paddingBottom'] = function(i) { return 6; };
                        objLayout['paddingLeft'] = function(i) { return 6; };
                        objLayout['paddingRight'] = function(i) { return 6; };
                        doc.content[1].layout = objLayout;
                    }
                }
            ]
        });

        $("#print").click(function() {
            table.button(1-1).trigger();
        })
    } );
</script>
<script src="http://kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
<script>

    var generatePDF = function () {
        var draw = kendo.drawing;

        draw.drawDOM($("#printablearea"), {
            avoidLinks: true,
            paperSize: "A4",
            margin: {left: "1cm", top: "2cm", right: "1cm", bottom: "0cm"},
            multiPage: true,
            scale: 0.5

        })
                .then(function (root) {
                    return draw.exportPDF(root);
                })
                .done(function (data) {
                    kendo.saveAs({
                        dataURI: data,
                        fileName: "Tvet_Examination_Result.pdf"
                    });
                });
    }



</script>
</body>
</html>