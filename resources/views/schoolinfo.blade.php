<!DOCTYPE html>
<html id="ctl00_Html1" lang="en" __expr-val-dir="ltr" dir="ltr" style="height: 100%;">

<head id="ctl00_Head1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=2.0,user-scalable=yes">
    <!-- Icons -->
    <link rel="shortcut icon" href="">
    <link rel="apple-touch-icon" href="{{asset('favicon.ico')}}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{asset('favicon.ico')}}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{asset('favicon.ico')}}" sizes="152x152">

    <title>{{ $school->school_name }}</title>

    <!-- SharePoint Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/schoolinfo/controls.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/schoolinfo/core.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0="
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin="" />
    <!-- Bootstrap CSS -->
    <link href="/css/schoolinfo/bootstrap.min.css" rel="stylesheet" media="all">
    <!-- Font Awesome CSS -->
    <link href="/css/schoolinfo/font-awesome.css" rel="stylesheet">
    <!-- Glyphicons CSS -->
    <link href="/css/schoolinfo/glyphicons.css" rel="stylesheet" media="all">
    <link href="/css/schoolinfo/social.css" rel="stylesheet" media="all">
    <!-- Bootstrap CSS overrides -->
    <link href="/css/schoolinfo/profile.css" rel="stylesheet" media="all">
    <!-- IE8 Support of media queries -->
    <script async="" src="/js/schoolinfo/analytics.js"></script>
    <script type="text/javascript" src="/js/schoolinfo/respond.min.js"></script>
    <!-- jQuery -->
    <script type="text/javascript" src="/js/schoolinfo/jquery-1.10.2.min.js"></script>
    <!-- jQuery Migratation support of ie 6, 7, 8-->
    <script type="text/javascript" src="/js/schoolinfo/jquery-migrate-1.2.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script type="text/javascript" src="/js/schoolinfo/bootstrap.min(2).js"></script>
    <!-- Print CSS -->
    <link href="./print.css" rel="stylesheet" media="print">

    <!-- Footer Styles -->
    <style>
        footer {
            bottom: 0;
            background: linear-gradient(#111, #414141, #414141);
            background: -webkit-linear-gradient(#111, #414141, #414141);
            background: -o-linear-gradient(#111, #414141, #414141);
            background: -moz-linear-gradient(#111, #414141, #414141);
            color: #fff;
            padding: 70px 50px;
        }

        footer a {
            text-decoration: none;
            color: #fff;
        }

        footer a:hover {
            text-decoration: none;
            color: #008eaa;
        }
    </style>
    <!-- Bootstrap 3.2 CSS -->
    <link rel="stylesheet" href="{{asset('css/schoolinfo/bootstrap.min.css')}}">


    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('css/schoolinfo/font-awesome.min.css')}}">

    <!-- Responsive Style Customizations CSS -->
    <link rel="stylesheet" href="{{asset('css/schoolinfo/responsive-mobile.css')}}">
    <!-- Dev/Prod -->

    <!-- jQuery JS in header (instead of footer) to work with animatedcollapse.js -->
    <script src="{{asset('js/schoolinfo/jquery-1.11.1.min.js')}}" type="text/javascript"></script>


    <!-- bootstrap 3.2 JS -->
    <script src="{{asset('js/schoolinfo/bootstrap.min.js')}}" type="text/javascript"></script>

    <!-- gradeTimeLine JS -->
    <script src="{{asset('js/schoolinfo/cbpScroller.js')}}" type="text/javascript"></script>
    {{--
    <script src="{{asset('js/schoolinfo/classie.js')}}" type="text/javascript"></script>--}}

    <!-- handlebars JS  - used in keyevents-->
    {{--
    <script src="{{asset('js/schoolinfo/handlebars-v1.3.0.js')}}" type="text/javascript"></script>--}}

    <!-- responsive JS -->
    <script src="{{asset('js/schoolinfo/responsive-mobile.js')}}" type="text/javascript"></script>
    <!-- Dev/Prod -->

    {{--
    <script src="{{asset('js/schoolinfo/init.js')}}"></script>--}} {{--
    <script type="text/javascript" language="javascript" src="{{asset('js/schoolinfo/non_ie.js')}}"></script>--}} {{--
    <script type="text/javascript" src="{{asset('js/schoolinfo/custom_activex_override.js')}}"></script>--}}

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
        //Function to the css rule
        function checkSize() {
            if ($(".socialDiv").css("display") == "none") {
                $('#google_translate_element').appendTo($('#google_translate_element_bottom'));
            } else {
                $('#google_translate_element').appendTo($('#google_translate_element_top'));
            }
        }
    </script>


    <style>
        .goog-te-gadget-simple {
            border: 0 !important;
        }

        .goog-te-gadget-simple .goog-te-menu-value {
            color: #0033A0 !important;
        }

        .goog-te-gadget-simple .goog-te-menu-value span {
            color: #0033A0 !important;
        }

        @media (max-width: 767px) {
            .goog-te-gadget-simple {
                background-color: #E6E6E6 !important;
            }
    </style>

    <style type="text/css">
        .ctl00_PlaceHolderMain_Webpartzone100_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone1_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone200_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone17_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone18_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone300_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone21_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone22_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone400_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone5_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone6_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone500_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone2_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone25_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone600_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone28_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone29_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone700_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone19_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone20_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone800_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone23_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone24_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone900_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone3_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone4_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone1000_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone26_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone27_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone1100_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone7_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone8_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone9_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone1200_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone10_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone11_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone12_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone13_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone1300_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone14_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone15_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone16_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }

        .ctl00_PlaceHolderMain_Webpartzone1400_0 {
            border-color: Black;
            border-width: 1px;
            border-style: Solid;
        }
    </style>
    <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{asset('css/schoolinfo/translateelement.css')}}">
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/schoolinfo/main.js')}}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/schoolinfo/element_main.js')}}"></script>
    <script src="{{asset('js/schoolinfo/jsapi')}}" type="text/javascript"></script>
    <link type="text/css" href="{{asset('css/schoolinfo/default+en.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('css/schoolinfo/default.css')}}" rel="stylesheet">
    <script type="text/javascript" src="{{asset('js/schoolinfo/default+en.I.js')}}"></script>
    <style type="text/css">
        .gsc-control-cse {
            font-family: Arial, sans-serif;
            border-color: #FFFFFF;
            background-color: #FFFFFF
        }

        .gsc-control-cse .gsc-table-result {
            font-family: Arial, sans-serif
        }

        input.gsc-input,
        .gsc-input-box,
        .gsc-input-box-hover,
        .gsc-input-box-focus {
            border-color: #D9D9D9
        }

        .gsc-search-button-v2,
        .gsc-search-button-v2:hover,
        .gsc-search-button-v2:focus {
            border-color: #2F5BB7;
            background-color: #357AE8;
            background-image: none;
            filter: none
        }

        .gsc-search-button-v2 svg {
            fill: #FFFFFF
        }

        .gsc-tabHeader.gsc-tabhInactive {
            border-color: #CCCCCC;
            background-color: #FFFFFF
        }

        .gsc-tabHeader.gsc-tabhActive {
            border-color: #CCCCCC;
            border-bottom-color: #FFFFFF;
            background-color: #FFFFFF
        }

        .gsc-tabsArea {
            border-color: #CCCCCC
        }

        .gsc-webResult.gsc-result,
        .gsc-results .gsc-imageResult {
            border-color: #FFFFFF;
            background-color: #FFFFFF
        }

        .gsc-webResult.gsc-result:hover,
        .gsc-imageResult:hover {
            border-color: #FFFFFF;
            background-color: #FFFFFF
        }

        .gs-webResult.gs-result a.gs-title:link,
        .gs-webResult.gs-result a.gs-title:link b,
        .gs-imageResult a.gs-title:link,
        .gs-imageResult a.gs-title:link b {
            color: #1155CC
        }

        .gs-webResult.gs-result a.gs-title:visited,
        .gs-webResult.gs-result a.gs-title:visited b,
        .gs-imageResult a.gs-title:visited,
        .gs-imageResult a.gs-title:visited b {
            color: #1155CC
        }

        .gs-webResult.gs-result a.gs-title:hover,
        .gs-webResult.gs-result a.gs-title:hover b,
        .gs-imageResult a.gs-title:hover,
        .gs-imageResult a.gs-title:hover b {
            color: #1155CC
        }

        .gs-webResult.gs-result a.gs-title:active,
        .gs-webResult.gs-result a.gs-title:active b,
        .gs-imageResult a.gs-title:active,
        .gs-imageResult a.gs-title:active b {
            color: #1155CC
        }

        .gsc-cursor-page {
            color: #1155CC
        }

        a.gsc-trailing-more-results:link {
            color: #1155CC
        }

        .gs-webResult .gs-snippet,
        .gs-imageResult .gs-snippet,
        .gs-fileFormatType {
            color: #333333
        }

        .gs-webResult div.gs-visibleUrl,
        .gs-imageResult div.gs-visibleUrl {
            color: #007928
        }

        .gs-webResult div.gs-visibleUrl-short {
            color: #007928
        }

        .gs-webResult div.gs-visibleUrl-short {
            display: none
        }

        .gs-webResult div.gs-visibleUrl-long {
            display: block
        }

        .gs-promotion div.gs-visibleUrl-short {
            display: none
        }

        .gs-promotion div.gs-visibleUrl-long {
            display: block
        }

        .gsc-cursor-box {
            border-color: #FFFFFF
        }

        .gsc-results .gsc-cursor-box .gsc-cursor-page {
            border-color: #CCCCCC;
            background-color: #FFFFFF;
            color: #1155CC
        }

        .gsc-results .gsc-cursor-box .gsc-cursor-current-page {
            border-color: #CCCCCC;
            background-color: #FFFFFF;
            color: #1155CC
        }

        .gsc-webResult.gsc-result.gsc-promotion {
            border-color: #F6F6F6;
            background-color: #F6F6F6
        }

        .gsc-completion-title {
            color: #1155CC
        }

        .gsc-completion-snippet {
            color: #333333
        }

        .gs-promotion a.gs-title:link,
        .gs-promotion a.gs-title:link *,
        .gs-promotion .gs-snippet a:link {
            color: #1155CC
        }

        .gs-promotion a.gs-title:visited,
        .gs-promotion a.gs-title:visited *,
        .gs-promotion .gs-snippet a:visited {
            color: #1155CC
        }

        .gs-promotion a.gs-title:hover,
        .gs-promotion a.gs-title:hover *,
        .gs-promotion .gs-snippet a:hover {
            color: #1155CC
        }

        .gs-promotion a.gs-title:active,
        .gs-promotion a.gs-title:active *,
        .gs-promotion .gs-snippet a:active {
            color: #1155CC
        }

        .gs-promotion .gs-snippet,
        .gs-promotion .gs-title .gs-promotion-title-right,
        .gs-promotion .gs-title .gs-promotion-title-right * {
            color: #333333
        }

        .gs-promotion .gs-visibleUrl,
        .gs-promotion .gs-visibleUrl-short {
            color: #007928
        }
    </style>
    <style type="text/css">
        .gscb_a {
            display: inline-block;
            font: 27px/13px arial, sans-serif
        }

        .gsst_a .gscb_a {
            color: #a1b9ed;
            cursor: pointer
        }

        .gsst_a:hover .gscb_a,
        .gsst_a:focus .gscb_a {
            color: #36c
        }

        .gsst_a {
            display: inline-block
        }

        .gsst_a {
            cursor: pointer;
            padding: 0 4px
        }

        .gsst_a:hover {
            text-decoration: none !important
        }

        .gsst_b {
            font-size: 16px;
            padding: 0 2px;
            position: relative;
            user-select: none;
            -webkit-user-select: none;
            white-space: nowrap
        }

        .gsst_e {
            opacity: 0.55;
        }

        .gsst_a:hover .gsst_e,
        .gsst_a:focus .gsst_e {
            opacity: 0.72;
        }

        .gsst_a:active .gsst_e {
            opacity: 1;
        }

        .gsst_f {
            background: white;
            text-align: left
        }

        .gsst_g {
            background-color: white;
            border: 1px solid #ccc;
            border-top-color: #d9d9d9;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            margin: -1px -3px;
            padding: 0 6px
        }

        .gsst_h {
            background-color: white;
            height: 1px;
            margin-bottom: -1px;
            position: relative;
            top: -1px
        }

        .gsib_a {
            width: 100%;
            padding: 4px 6px 0
        }

        .gsib_a,
        .gsib_b {
            vertical-align: top
        }

        .gssb_c {
            border: 0;
            position: absolute;
            z-index: 989
        }

        .gssb_e {
            border: 1px solid #ccc;
            border-top-color: #d9d9d9;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            cursor: default
        }

        .gssb_f {
            visibility: hidden;
            white-space: nowrap
        }

        .gssb_k {
            border: 0;
            display: block;
            position: absolute;
            top: 0;
            z-index: 988
        }

        .gsdd_a {
            border: none !important
        }

        .gsq_a {
            padding: 0
        }

        .gsq_a {
            padding: 0
        }

        .gscsep_a {
            display: none
        }

        .gssb_a {
            padding: 0 7px
        }

        .gssb_a,
        .gssb_a td {
            white-space: nowrap;
            overflow: hidden;
            line-height: 22px
        }

        #gssb_b {
            font-size: 11px;
            color: #36c;
            text-decoration: none
        }

        #gssb_b:hover {
            font-size: 11px;
            color: #36c;
            text-decoration: underline
        }

        .gssb_g {
            text-align: center;
            padding: 8px 0 7px;
            position: relative
        }

        .gssb_h {
            font-size: 15px;
            height: 28px;
            margin: 0.2em;
            -webkit-appearance: button
        }

        .gssb_i {
            background: #eee
        }

        .gss_ifl {
            visibility: hidden;
            padding-left: 5px
        }

        .gssb_i .gss_ifl {
            visibility: visible
        }

        a.gssb_j {
            font-size: 13px;
            color: #36c;
            text-decoration: none;
            line-height: 100%
        }

        a.gssb_j:hover {
            text-decoration: underline
        }

        .gssb_l {
            height: 1px;
            background-color: #e5e5e5
        }

        .gssb_m {
            color: #000;
            background: #fff
        }

        .gsfe_a {
            border: 1px solid #b9b9b9;
            border-top-color: #a0a0a0;
            box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
        }

        .gsfe_b {
            border: 1px solid #4d90fe;
            outline: none;
            box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.3);
        }

        .gssb_a {
            padding: 0 9px
        }

        .gsib_a {
            padding-right: 8px;
            padding-left: 8px
        }

        .gsst_a {
            padding-top: 5.5px
        }

        .gssb_e {
            border: 0
        }

        .gssb_l {
            margin: 5px 0
        }

        input.gsc-input::-webkit-input-placeholder {
            font-size: 14px
        }

        input.gsc-input:-moz-placeholder {
            font-size: 14px
        }

        input.gsc-input::-moz-placeholder {
            font-size: 14px
        }

        input.gsc-input:-ms-input-placeholder {
            font-size: 14px
        }

        input.gsc-input:focus::-webkit-input-placeholder {
            color: transparent
        }

        input.gsc-input:focus:-moz-placeholder {
            color: transparent
        }

        input.gsc-input:focus::-moz-placeholder {
            color: transparent
        }

        input.gsc-input:focus:-ms-input-placeholder {
            color: transparent
        }

        .gssb_c .gsc-completion-container {
            position: static
        }

        .gssb_c {
            z-index: 5000
        }

        .gsc-completion-container table {
            background: transparent;
            font-size: inherit;
            font-family: inherit
        }

        .gssb_c>tbody>tr,
        .gssb_c>tbody>tr>td,
        .gssb_d,
        .gssb_d>tbody>tr,
        .gssb_d>tbody>tr>td,
        .gssb_e,
        .gssb_e>tbody>tr,
        .gssb_e>tbody>tr>td {
            padding: 0;
            margin: 0;
            border: 0
        }

        .gssb_a table,
        .gssb_a table tr,
        .gssb_a table tr td {
            padding: 0;
            margin: 0;
            border: 0
        }
    </style>
    <style type="text/css">
        .gscb_a {
            display: inline-block;
            font: 27px/13px arial, sans-serif
        }

        .gsst_a .gscb_a {
            color: #a1b9ed;
            cursor: pointer
        }

        .gsst_a:hover .gscb_a,
        .gsst_a:focus .gscb_a {
            color: #36c
        }

        .gsst_a {
            display: inline-block
        }

        .gsst_a {
            cursor: pointer;
            padding: 0 4px
        }

        .gsst_a:hover {
            text-decoration: none !important
        }

        .gsst_b {
            font-size: 16px;
            padding: 0 2px;
            position: relative;
            user-select: none;
            -webkit-user-select: none;
            white-space: nowrap
        }

        .gsst_e {
            opacity: 0.55;
        }

        .gsst_a:hover .gsst_e,
        .gsst_a:focus .gsst_e {
            opacity: 0.72;
        }

        .gsst_a:active .gsst_e {
            opacity: 1;
        }

        .gsst_f {
            background: white;
            text-align: left
        }

        .gsst_g {
            background-color: white;
            border: 1px solid #ccc;
            border-top-color: #d9d9d9;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            margin: -1px -3px;
            padding: 0 6px
        }

        .gsst_h {
            background-color: white;
            height: 1px;
            margin-bottom: -1px;
            position: relative;
            top: -1px
        }

        .gsib_a {
            width: 100%;
            padding: 4px 6px 0
        }

        .gsib_a,
        .gsib_b {
            vertical-align: top
        }

        .gssb_c {
            border: 0;
            position: absolute;
            z-index: 989
        }

        .gssb_e {
            border: 1px solid #ccc;
            border-top-color: #d9d9d9;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            cursor: default
        }

        .gssb_f {
            visibility: hidden;
            white-space: nowrap
        }

        .gssb_k {
            border: 0;
            display: block;
            position: absolute;
            top: 0;
            z-index: 988
        }

        .gsdd_a {
            border: none !important
        }

        .gsq_a {
            padding: 0
        }

        .gsq_a {
            padding: 0
        }

        .gscsep_a {
            display: none
        }

        .gssb_a {
            padding: 0 7px
        }

        .gssb_a,
        .gssb_a td {
            white-space: nowrap;
            overflow: hidden;
            line-height: 22px
        }

        #gssb_b {
            font-size: 11px;
            color: #36c;
            text-decoration: none
        }

        #gssb_b:hover {
            font-size: 11px;
            color: #36c;
            text-decoration: underline
        }

        .gssb_g {
            text-align: center;
            padding: 8px 0 7px;
            position: relative
        }

        .gssb_h {
            font-size: 15px;
            height: 28px;
            margin: 0.2em;
            -webkit-appearance: button
        }

        .gssb_i {
            background: #eee
        }

        .gss_ifl {
            visibility: hidden;
            padding-left: 5px
        }

        .gssb_i .gss_ifl {
            visibility: visible
        }

        a.gssb_j {
            font-size: 13px;
            color: #36c;
            text-decoration: none;
            line-height: 100%
        }

        a.gssb_j:hover {
            text-decoration: underline
        }

        .gssb_l {
            height: 1px;
            background-color: #e5e5e5
        }

        .gssb_m {
            color: #000;
            background: #fff
        }

        .gsfe_a {
            border: 1px solid #b9b9b9;
            border-top-color: #a0a0a0;
            box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.1);
        }

        .gsfe_b {
            border: 1px solid #4d90fe;
            outline: none;
            box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: inset 0px 1px 2px rgba(0, 0, 0, 0.3);
        }

        .gssb_a {
            padding: 0 9px
        }

        .gsib_a {
            padding-right: 8px;
            padding-left: 8px
        }

        .gsst_a {
            padding-top: 5.5px
        }

        .gssb_e {
            border: 0
        }

        .gssb_l {
            margin: 5px 0
        }

        input.gsc-input::-webkit-input-placeholder {
            font-size: 14px
        }

        input.gsc-input:-moz-placeholder {
            font-size: 14px
        }

        input.gsc-input::-moz-placeholder {
            font-size: 14px
        }

        input.gsc-input:-ms-input-placeholder {
            font-size: 14px
        }

        input.gsc-input:focus::-webkit-input-placeholder {
            color: transparent
        }

        input.gsc-input:focus:-moz-placeholder {
            color: transparent
        }

        input.gsc-input:focus::-moz-placeholder {
            color: transparent
        }

        input.gsc-input:focus:-ms-input-placeholder {
            color: transparent
        }

        .gssb_c .gsc-completion-container {
            position: static
        }

        .gssb_c {
            z-index: 5000
        }

        .gsc-completion-container table {
            background: transparent;
            font-size: inherit;
            font-family: inherit
        }

        .gssb_c>tbody>tr,
        .gssb_c>tbody>tr>td,
        .gssb_d,
        .gssb_d>tbody>tr,
        .gssb_d>tbody>tr>td,
        .gssb_e,
        .gssb_e>tbody>tr,
        .gssb_e>tbody>tr>td {
            padding: 0;
            margin: 0;
            border: 0
        }

        .gssb_a table,
        .gssb_a table tr,
        .gssb_a table tr td {
            padding: 0;
            margin: 0;
            border: 0
        }
    </style>
</head>

<body class="body" style="position: relative; min-height: 100%; top: 0px;">


    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;">
        <div class="container">

            <!-- SharePoint Header-->
            <div id="ctl00_MSO_ContentDiv" style="background-color: #fff;">
                <div class="row" style="margin: 0;">


                </div>
                <div class="masterContent row" style="margin: 0;">


                </div>
                <div class="row" style="margin: 0;">
                    <!-- SharePoint Site Actions Drop-down button -->
                    <div id="authoringRegionSection">
                        <div class="siteActionMenu" id="siteActionsDiv" style="position:absolute; right:10px; margin-top:0px; z-index:2000;">

                        </div>
                        <!-- SharePoint Ribbon -->
                        <div class="console">

                            <!-- Console -->
                            <span id="ctl00_ctl17_publishingContext1"></span>

                            <!-- Console -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- END SharePoint Header-->

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars fa-2x cps-blue"> </i>
                    <span class="cps-blue toggbutton">MENU</span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="{{asset('img/app_logo.jpg')}}" class="navbar-brand-image" alt="mis logo">
                </a>
            </div>

            <div id="collapsible-nav" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a class="navParents" href="#">Students</a></li>
                    <li><a class="navSchools" href="#">Schools</a></li>
                    <li><a class="navAbout" href="#">About</a></li>
                    <li><a class="navCalendar" href="#">Calendar</a></li>
                    <li><a class="navStaff" href="#">Staff</a></li>
                    <li><a class="navTopics" href="#">Topics</a></li>
                </ul>

                <div style="clear:both;"></div>

            </div>

        </div>
        <!-- /.container -->
    </nav>
    <!-- /.navbar -->

    <!--Page Content -->


    <div class="container">

        <div class="row">
            <div class="col-xs-12">

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">


            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

            </div>
        </div>

    </div>


    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"></div>
            </div>
        </div>
    </div>


    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody>
            <tr>
                <td id="MSOZoneCell_WebPartWPQ1" valign="top">
                    <table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td valign="top">
                                    <div webpartid="fa138c5b-4c2d-4792-b6a7-539efb83f8df" haspers="false" id="WebPartWPQ1" width="100%" class="ms-WPBody" allowdelete="false"
                                        style="">
                                        <script>
                                            document.domain = "cps.edu";
                                        </script>

                                        <script type="text/javascript" src="{{asset('js/schoolinfo/iframe.js')}}"></script>

                                        <script type="text/javascript" src="{{asset('js/schoolinfo/iframeResizer.min.js')}}"></script>
                                        <div>

                                            <style>
                                                @font-face {
                                                    text-rendering: optimizeLegibility;
                                                }

                                                .nav>li>a:hover,
                                                .nav>li>a:focus {
                                                    text-decoration: none;
                                                    background-color: transparent !important;
                                                }

                                                a:hover {
                                                    color: #008eaa;
                                                }

                                                .grad-blue {
                                                    background: -webkit-linear-gradient(-200deg, #0033a0, #008eaa);
                                                    background: -o-linear-gradient(-200deg, #0033a0, #008eaa);
                                                    background: -moz-linear-gradient(-200deg, #0033a0, #008eaa);
                                                    background: linear-gradient(-200deg, #0033a0, #008eaa);
                                                    background-color: #0033a0;
                                                }

                                                #socialprofile>a {
                                                    color: #fff;
                                                }

                                                /* google translate */

                                                .goog-te-gadget-simple {
                                                    margin-top: 15px !important;
                                                    margin-left: 10px !important;
                                                    background-color: #fff;
                                                    border-left: 0px solid #d5d5d5 !important;
                                                    border-top: 0px solid #9b9b9b !important;
                                                    border-bottom: 0px solid #e8e8e8 !important;
                                                    border-right: 0px solid #d5d5d5 !important;
                                                    font-size: 10pt;
                                                    display: inline-block;
                                                    cursor: pointer;
                                                    zoom: 1;

                                                }

                                                .secondRow {
                                                    padding-right: 20px;
                                                }

                                                #google_translate_element_menu {
                                                    display: inline-block;
                                                }

                                                @media screen and (max-width: 856px) {
                                                    .navstyles {
                                                        font-size: 14px;
                                                        margin: 0px;
                                                    }

                                                }
                                            </style>


                                            <div class="container">
                                                <div class="row ">
                                                    <div class="col-xs-12">

                                                    </div>
                                                </div>
                                            </div>


                                            <div id="schoolprofilebanner" class="container-fluid grad-blue">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-print">
                                                            <div class="text-center" style="margin-top:10px; margin-bottom:10px;">
                                                                @if($school->latitude and $school->longitude)
                                                                <div id="mapids">
                                                                    <a href="{{ route('home.schools.map',['name' => $school->school_name ,'latitude'=>$school->latitude,'longitude'=> $school->longitude]) }}"><img
                                                                                class="img img-responsive img-circle"
                                                                                src="https://api.mapbox.com/v4/mapbox.streets/url-https%3A%2F%2Fmaps.google.com%2Fmapfiles%2Fms%2Ficons%2Fblue.png({{$school->latitude}},{{$school->longitude}})/{{$school->latitude}},{{$school->longitude}},14/200x200.png?access_token=pk.eyJ1IjoibXVjeW9taWxsZXIiLCJhIjoiY2ppcWdmOTVkMTczejNrbXBtNmJzdWM1eiJ9.aPk5vXhGxBOKNomnZrUThA"/></a>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div id="divSchoolInfoHeader" class="col-xs-12 col-sm-6 col-md-7 col-lg-7" style="padding-top:20px; color: #fff;">
                                                            <h2>
                                                                <span id="ctl00_ContentPlaceHolder1_lbOfficialSchoolName">{{ $school->school_name?:'Not set' }}</span>
                                                            </h2>
                                                            <h4>
                                                                <span id="ctl00_ContentPlaceHolder1_lbOfficialSchoolName">{{$school->school_moto?:'Not set'}}</span>
                                                            </h4>
                                                            <p><i class="fa fa-map-marker fa-lg fa-fw" style="padding-right:5px;"></i>
                                                                <a
                                                                    id="ctl00_ContentPlaceHolder1_lnkSchoolLocatorMap2" class="h5 liblue"
                                                                    href="#" target="_blank"><span id="ctl00_ContentPlaceHolder1_lbAddress">
                                                                        {{ ($school->province?:'Not set').',' }} {{($school->district?:'Not set').','}} {{($school->sector?:'Not set').','}} {{($school->cell?:'Not set').','}} {{ $school->village?:'Not set' }}
                                                                        </span></a>
                                                            </p>
                                                            <p><span style="white-space:nowrap;">
                                                                        <span id="ctl00_ContentPlaceHolder1_globeicon"
                                                                              class="fa fa-globe fa-lg fa-fw"
                                                                              style="padding-right:5px;"></span>
                                                                <a id="ctl00_ContentPlaceHolder1_lnkSchoolWebsite" class="h5 liblue" href="#" target="_blank" style="padding-right:10px;">{{ $school->website?:'Not set' }}</a></span>
                                                                <span style="white-space:nowrap; padding-right:10px; ">
                                                                        <span id="ctl00_ContentPlaceHolder1_earphoneicon"
                                                                              class="fa fa-phone fa-lg fa-fw"
                                                                              style="padding-right:5px;"></span>
                                                                <span id="ctl00_ContentPlaceHolder1_lblPhone" class="h5">{{$school->phone?:'Not set'}}</span>
                                                                </span>
                                                                <span id="ctl00_ContentPlaceHolder1_spFax" style="white-space:nowrap;"><i
                                                                        style="padding-right:5px;"
                                                                        class="fa fa-fax fa-fw"></i><span
                                                                        id="ctl00_ContentPlaceHolder1_lblFax"
                                                                        class="h5">{{$school->post_office_post_number?:'Not set'}}</span>&nbsp;(Fax/Po
                                                                Box)</span>
                                                            </p>
                                                            <p id="socialprofile"><span style="white-space:nowrap;">
                                                                        <i id="ctl00_ContentPlaceHolder1_spFacebook"
                                                                           class="fa fa-facebook-official fa-lg fa-fw"
                                                                           style="padding-right:5px;"></i>
                                                                        <a id="ctl00_ContentPlaceHolder1_hlFacebookURL"
                                                                           class="h5 liblue" href="#" target="_blank"
                                                                           style="padding-right:10px;">
                                                                            {{ $school->facebook?:'Not set' }}
                                                                        </a></span><span style="white-space:nowrap;"><i
                                                                        id="ctl00_ContentPlaceHolder1_spTwitter"
                                                                        class="fa fa-twitter-square fa-lg fa-fw"
                                                                        style="padding-right:5px;"></i><a
                                                                        id="ctl00_ContentPlaceHolder1_hlTwitterURL"
                                                                        class="h5 liblue" href="#" target="_blank"
                                                                        style="padding-right:10px;">{{ $school->twitter?:'Not set' }}</a></span>
                                                                <span style="white-space:nowrap;"></span>
                                                                <span style="white-space:nowrap;"></span>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-4 col-xs-offset-4 col-sm-offset-0 col-sm-2 col-md-2 col-lg-2" style="margin-top:20px; margin-bottom:20px">
                                                            <img src="{{ $school->school_logo ? asset('storage/'.$school->school_logo) : asset('img/logo_placeholder.png') }}" class="img img-responsive img-circle">

                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="hidden-print" style="background:#fff;padding:5px;">
                                                    <div class="container">
                                                        <div class="row">
                                                            <ul class="nav navbar-nav hidden-xs navstyles" id="myTab">
                                                                <li class="active"><a id="btnOverview" class="btnPill" href="#profile" data-toggle="tab">Profile </a>
                                                                </li>
                                                                <li><a id="btnProgress" class="btnPill" href="#admissions" data-toggle="tab">Admissions </a></li>
                                                                <li><a id="btnAdmissions" class="btnPill" href="#courses" data-toggle="tab">Courses </a></li>
                                                                <li><a id="btnExtras" class="btnPill" href="#shortcourses" data-toggle="tab">Short Courses </a></li>
                                                                <li><a id="btnExtras" class="btnPill" href="#staffprofiles" data-toggle="tab">Staff Profiles </a></li>
                                                                <li><a id="btnExtras" class="btnPill" href="#announcements" data-toggle="tab">Announcements </a></li>
                                                                <li><a id="btnReports" class="btnPill" href="#contactus" data-toggle="tab">Contact us</a></li>

                                                            </ul>
                                                            <ul class="nav navbar-nav navbar-right hidden-xs navstyles">
                                                                <li id="ctl00_ContentPlaceHolder1_liRefresh" class="leftnavborder">
                                                                    <a href="{{ route('home.schools')}}" id="refresh" alt="Find Another School"><i
                                                                            class="fa fa-reply fa-lg"></i></a></li>
                                                                <li><a href="javascript:window.print()" alt="Print Profile" id="printAll"><i class="fa fa-print fa-lg"></i></a>
                                                                </li>
                                                            </ul>
                                                            <div class="visible-xs">
                                                                <div class="list-group navstyles">
                                                                    <a href="#" class="list-group-item" data-toggle="collapse" data-target="#navcollapse1" onclick="return false;">School Profile Menu <span
                                                                            class="caret"></span></a></div>
                                                            </div>
                                                            <div class="collapse" id="navcollapse1">
                                                                <div class="list-group visible-xs navstyles">
                                                                    <a id="btnOverview" class="list-group-item" title="School Profile" href="#profile" data-toggle="tab">Profile</a>
                                                                    <a id="btnProgress" class="list-group-item" title="School Admissions Requirements" href="#admissions" data-toggle="tab">Admission</a>
                                                                    <a id="btnAdmissions" class="list-group-item" title="Courses Available" href="#courses" data-toggle="tab">Courses</a>
                                                                    <a id="btnAdmissions" class="list-group-item" title="Short Courses Available" href="#shortcourses" data-toggle="tab">Short Courses</a>
                                                                    <a id="btnExtras" class="list-group-item" title="School Staff Profiles" href="#staffprofiles" data-toggle="tab">Staff Profiles</a>
                                                                    <a id="btnReports" class="list-group-item" title="School Announcements" href="#announcements" data-toggle="tab">Announcements</a>
                                                                    <a id="btnReports" class="list-group-item" title="School Contact us" href="#contactus" data-toggle="tab">Contact us</a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="container">


                                                <div class="tab-content col-m-8 offset-2" style="padding-top: 20px;">

                                                    <div class="tab-pane active" id="profile">


                                                        <div style="font-size: 1.2em;">
                                                            <span id="ctl00_ContentPlaceHolder1_lbSchoolSummary" class="grey-darkest">
                                                        {!! $school->description !!}
                                                        </span>
                                                            <p>&nbsp;</p>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <div class="panel panel-default panel-style">
                                                                    <div class="panel-body text-center" style="padding:30px 10px;">
                                                                        <i style="color:#0032a0;" class="fa fa-trophy fa-2x"></i>
                                                                        <h3>School Rating</h3>
                                                                        <h4>
                                                                            {{ $school->schoolrating->rating or "Not Set" }}
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <div class="panel panel-default panel-style">
                                                                    <div class="panel-body text-center" style="padding:30px 10px;">
                                                                        <i style="color:#0032a0;" class="fa fa-star fa-2x"></i>
                                                                        <h3>School Status</h3>
                                                                        <h4>
                                                                            <span id="GradeText">
                                                                                {{ $school->school_status }}
                                                                        </span>
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <div class="panel panel-default panel-style">
                                                                    <div class="panel-body text-center" style="padding:30px 10px;">
                                                                        <i style="color:#0032a0;" class="fa fa-child fa-2x"></i>
                                                                        <h3># of Students</h3>
                                                                        <h4>
                                                                            <span id="ctl00_ContentPlaceHolder1_lbNumberOfStudents" class="text-aqua">
                                                                            {{ $school->students->count() }}
                                                                        </span>
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                                <div class="panel panel-default panel-style">
                                                                    <div class="panel-body text-center" style="padding:30px 10px;">
                                                                        <i style="color:#0032a0;" class="fa fa-bank fa-2x"></i>
                                                                        <h3>School Type</h3>
                                                                        <h4>
                                                                            <span id="ctl00_ContentPlaceHolder1_lbSchoolType">
                                                                            {{ $school->schooltype->school_type or "Not Set" }}
                                                                        </span>
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row card-style">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                {{-- <h3 style="color:#93328e"><span class="h4"><i class="fa fa-circle"></i></span> Faculty</h3> --}}
                                                                <h3 style="color:#93328e"> </h3>
                                                                <hr style="border-top: 2px solid #93328e;">

                                                                <div id="ctl00_ContentPlaceHolder1_liHours" class="media">
                                                                    <div class="glyphicons nameplate pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHours" class="h4">{{ $school->manager_name }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHoursTitle">Manager Name</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons envelope pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4">{{ $school->email or 'Not Set' }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">E-mail</span>

                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons pushpin pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4">{{ $school->province or 'Not Set' }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Province</span>

                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                {{--<h3 style="color:#6dad1d"><span class="h4"><i class="fa fa-circle"></i></span> Hours</h3> --}}
                                                                <h3 style="color:#6dad1d"></h3>
                                                                <hr style="border-top: 2px solid #6dad1d;">

                                                                <div id="ctl00_ContentPlaceHolder1_liHours" class="media">
                                                                    <div class="glyphicons clock pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHours" class="h4">{{ $school->boarding_or_day }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHoursTitle">Boarding / Day</span>
                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons global pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4">{{ $school->website or 'Not Set' }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Website</span>

                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons pushpin pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4">{{ $school->district or 'Not Set' }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">District</span>

                                                                    </div>
                                                                </div>

                                                                <p>&nbsp;</p>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                {{--<h3 style="color:#0032a0"><span class="h4"><i class="fa fa-circle"></i></span> Public Transit</h3> --}}
                                                                <h3 style="color:#0032a0"></h3>
                                                                <hr style="border-top: 2px solid #0032a0;">


                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons global pull-left media-leftbar-pad">
                                                                        </div>
                                                                    <div class="media-body">
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4">{{ $school->website or 'Not Set' }}</span><br>
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Website</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons earphone pull-left media-leftbar-pad">
                                                                        </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4">{{ $school->phone or 'Not Set' }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Phone Number</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons pushpin pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4">{{ $school->sector or 'Not Set' }}</span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Sector</span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        
                                                        @if(Auth::guard('wda')->user())
                                                        <div class="row card-style">
                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <h3 style="color:#93328e"><span class="h4"><i class="fa fa-circle"></i></span> Key Infrastructure</h3>
                                                                <h3 style="color:#93328e"> </h3>
                                                                <hr style="border-top: 2px solid #93328e;">

                                                                <div id="ctl00_ContentPlaceHolder1_liHours" class="media">
                                                                    <div class="glyphicons check pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHours" class="h4"><b>{{ $school->has_electricity == 2 ? "Yes" : "No" }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHoursTitle">Has Electricity</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons check pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->has_water  == 2 ? "Yes" : "No"  }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Has Water</span>

                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons check pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->has_water  == 2 ? "Yes" : "No" }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Has Computer Lab</span>

                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons check pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->has_internet  == 2 ? "Yes" : "No"  }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Has Internet</span>

                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons check pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->has_library  == 2 ? "Yes" : "No"  }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Has Library</span>

                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons check pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->has_strategic_plan  == 2 ? "Yes" : "No"  }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Has Strategic Plan</span>

                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <h3 style="color:#6dad1d"><span class="h4"><i class="fa fa-circle"></i></span> Key Statistics</h3>
                                                                <h3 style="color:#6dad1d"></h3>
                                                                <hr style="border-top: 2px solid #6dad1d;">

                                                                <div id="ctl00_ContentPlaceHolder1_liHours" class="media">
                                                                    <div class="glyphicons bookmark pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHours" class="h4"><b>{{ $school->number_of_generators or 'Not Set' }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbHoursTitle">Generators</span>
                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons female pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ App\StaffsInfo::where('school_id', $school->id)->where('gender', 'Female')->count() }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Female Staff</span>

                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons male pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ App\StaffsInfo::where('school_id', $school->id)->where('gender', 'Male')->count() }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Male Staff</span>

                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons female pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ App\Student::where('school', $school->id)->where('gender', 'Female')->count() }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Female Students</span>

                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons bookmark pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ App\Department::where('school_id', $school->id)->sum('capacity_rooms') or 'Not Set' }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Classrooms</span>

                                                                    </div>
                                                                </div>

                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons bookmark pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->number_of_desktops or 'Not Set' }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Desktops</span>

                                                                    </div>
                                                                </div>

                                                                <p>&nbsp;</p>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                                <h3 style="color:#0032a0"><span class="h4"><i class="fa fa-circle"></i></span> Other Information</h3>
                                                                <h3 style="color:#0032a0"></h3>
                                                                <hr style="border-top: 2px solid #0032a0;">


                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons record pull-left media-leftbar-pad">
                                                                        </div>
                                                                    <div class="media-body">
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->owner_name or 'Not Set' }}</b></span><br>
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Owner Name</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons record pull-left media-leftbar-pad">
                                                                        </div>
                                                                    <div class="media-body">
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->owner_type ? App\Model\Accr\SourceSchoolOwnership::find($school->owner_type)->owner : 'Not Set' }}</b></span><br>
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Owner Type</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons record pull-left media-leftbar-pad">
                                                                        </div>
                                                                    <div class="media-body">
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->owner_email or 'Not Set' }}</b></span><br>
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Owner E-mail</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons record pull-left media-leftbar-pad">
                                                                        </div>
                                                                    <div class="media-body">
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->owner_phone or 'Not Set' }}</b></span><br>
                                                                            <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Owner Phone</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons record pull-left media-leftbar-pad">
                                                                        </div>
                                                                    <div class="media-body">
                                                                    @php
                                                                        $accr_array = array('1' => 'Accredited', '2' => 'Not Accredited', '3' => 'In Progress');
                                                                    @endphp
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->accreditation_status ? : "Not Set" }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Accreditation Status</span>
                                                                    </div>
                                                                </div>
                                                                <div id="ctl00_ContentPlaceHolder1_liEarliestDropOffTime" class="media">
                                                                    <div class="glyphicons record pull-left media-leftbar-pad">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTime" class="h4"><b>{{ $school->accreditation_number or 'Not Set' }}</b></span><br>
                                                                        <span id="ctl00_ContentPlaceHolder1_lbEarliestDropOffTimeTitle">Accreditation Number</span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif

                                                        <div class="row">
                                                            {{-- <div class="col-sm-6 col-md-6">

                                                                <div id="divSchoolInfo" class="row card-style">
                                                                    <table class="table table-hover">
                                                                        <thead>
                                                                            <tr id="ctl00_ContentPlaceHolder1_liKindergarten" style="border-top:1px solid transparent;">
                                                                                <th>
                                                                                    <h4><span id="ctl00_ContentPlaceHolder1_lbKindergartenTitle"
                                                                                            class="text-bold">Kindergarten: </span></h4>
                                                                                </th>
                                                                                <th valign="middle">
                                                                                    <h5><span id="ctl00_ContentPlaceHolder1_lbKindergarten">Full Day</span></h5>
                                                                                </th>
                                                                            </tr>

                                                                        </thead>

                                                                        <tbody>
                                                                            <tr id="ctl00_ContentPlaceHolder1_liBilingualServices">
                                                                                <td>
                                                                                    <h4><span id="ctl00_ContentPlaceHolder1_lbBilingualServicesTitle"
                                                                                            class="text-bold">Bilingual Services: </span></h4>
                                                                                </td>
                                                                                <td valign="bottom">
                                                                                    <h5><span id="ctl00_ContentPlaceHolder1_lbBilingualServices">Yes</span></h5>
                                                                                </td>
                                                                            </tr>

                                                                            <tr id="ctl00_ContentPlaceHolder1_liClassroomLanguages">
                                                                                <td>
                                                                                    <h4><span id="ctl00_ContentPlaceHolder1_lbClassroomLanguagesTitle"
                                                                                            class="text-bold">Classroom Languages: </span></h4>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <h5><span id="ctl00_ContentPlaceHolder1_lbClassroomLanguages">Spanish</span></h5>
                                                                                </td>
                                                                            </tr>

                                                                            <tr id="ctl00_ContentPlaceHolder1_liUniform">
                                                                                <td>
                                                                                    <h4><span id="ctl00_ContentPlaceHolder1_lbUniformTitle"
                                                                                            class="text-bold">Dress Code: </span></h4>
                                                                                </td>
                                                                                <td valign="middle"><span id="dresscheck"></span>
                                                                                    <h5><span id="ctl00_ContentPlaceHolder1_lbUniform">Yes</span></h5>
                                                                                </td>
                                                                            </tr>

                                                                            <tr id="ctl00_ContentPlaceHolder1_liAttendanceBoundaries">
                                                                                <td>
                                                                                    <h4><span id="ctl00_ContentPlaceHolder1_lbAttendanceBoundariesTitle"
                                                                                            class="text-bold">Attendance Boundaries: </span></h4>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <h5><span id="ctl00_ContentPlaceHolder1_lbAttendanceBoundaries">No</span></h5>
                                                                                    <span
                                                                                        id="attcheck"></span>
                                                                                </td>
                                                                            </tr>

                                                                            <tr id="ctl00_ContentPlaceHolder1_liRefugeeServices">
                                                                                <td>
                                                                                    <h4><span id="ctl00_ContentPlaceHolder1_lbRefugeeServicesTitle"
                                                                                            class="text-bold">Refugee Services: </span></h4>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <h5><span id="ctl00_ContentPlaceHolder1_lbRefugeeServices">N/A</span></h5>
                                                                                </td>
                                                                            </tr>

                                                                            <tr id="ctl00_ContentPlaceHolder1_liTitle1Eligible">
                                                                                <td>
                                                                                    <h4><span id="ctl00_ContentPlaceHolder1_lbTitle1EligibleTitle"
                                                                                            class="text-bold">Title I Eligible: </span></h4>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <h5><span id="ctl00_ContentPlaceHolder1_lbTitle1Eligible">Yes</span></h5>
                                                                                </td>
                                                                            </tr>









                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div> --}}
                                                        </div>

                                                        <hr>

                                                        <div class="row">
                                                            <div id="schoolyearFindaSchool">
                                                                <div class="text-center" style="margin-bottom:20px;">
                                                                    <h5 class="text-muted">School Year 2017-2018</h5>
                                                                    <div id="ctl00_ContentPlaceHolder1_divFindAnotherSchool">
                                                                        <a id="btnFindAnotherSchool" class="btn btn-primary btn-xs hidden-print">Find
                                                                        Another School</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="admissions" style="margin-bottom: 2em;">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                &nbsp;&nbsp;&nbsp;
                                                            </div>
                                                            <div class="col-md-8">
                                                                <h3 style="margin-bottom: 2em; color: #555;text-align: center;">
                                                                    Program Application Requirements</h3>
                                                                @if($school->admissionrequirement) {!! $school->admissionrequirement->admission_requirements !!} @else
                                                                <h5>There are no program application requirements available.
                                                                </h5>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="courses">


                                                        <div id="ctl00_ContentPlaceHolder1_pnlHasPerformance">

                                                            <div class="row">
                                                                <div class="panel panel-info text-center">
                                                                    <div class="panel-heading">
                                                                        <h4>
                                                                            <span id="ctl00_ContentPlaceHolder1_lblProgressReportText" class="red-light">Courses We Offers</span>
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">

                                                                <div id="ctl00_ContentPlaceHolder1_pnlGrowthES">

                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading"><span id="ctl00_ContentPlaceHolder1_lblGrowthESbyGradeLevelTitle"
                                                                                class="h5 margin-right-15">Courses available</span>
                                                                        </div>
                                                                        <div class="panel-body text-bold grey-darkest">
                                                                            <table class="table table-responsive table-bordered table-light">
                                                                                <tr>
                                                                                    <th>Sector</th>
                                                                                    <th>SubSector</th>
                                                                                    <th>Qualification</th>
                                                                                    <th>Module</th>
                                                                                </tr>
                                                                                @foreach($school->departments as $department)
                                                                                <tr>
                                                                                    <td>{{$department->qualification->subSector->sector->sector_name
                                                                                        or "" }}</td>
                                                                                    <td>{{$department->qualification->subSector->sub_sector_name
                                                                                        or "" }}</td>
                                                                                    <td>
                                                                                        @if($department->qualification)
                                                                                            {{--<a href="{{ route('curriculum.view',['id'=> getIDFromUUID('qu',$department->qualification->id)]) }}">{{$department->qualification->qualification_title or "" }}</a>--}}
                                                                                        @endif

                                                                                    </td>
                                                                                    <td>
                                                                                        @if($department->qualification)
                                                                                            @foreach($department->qualification->modules as $module)
                                                                                            <tr>
                                                                                                <td>{{$module->module_title }}</td>
                                                                                            </tr>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </td>
                                                                                </tr>
                                                                                @endforeach
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="shortcourses">

                                                        <div class="row">
                                                            <div class="col-sm-12 extras">


                                                                <div id="ctl00_ContentPlaceHolder1_liSchoolWidePgmsAndModels" class="col-sm-10">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading"><span id="ctl00_ContentPlaceHolder1_lbSchoolWidePgmsAndModelsTitle"
                                                                                class="h5">Short Courses Details</span>

                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <table class="table table-responsive">
                                                                                <tr>
                                                                                    <th>
                                                                                        Course Name
                                                                                    </th>
                                                                                    <th>
                                                                                        Course Details
                                                                                    </th>
                                                                                    <th>View Attachments</th>
                                                                                </tr>
                                                                                @foreach($school->shortcourses as $shortcourse)
                                                                                <tr>
                                                                                    <td>
                                                                                        {{ $shortcourse->course_name }}
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $shortcourse->course_details }}
                                                                                    </td>
                                                                                    <td>
                                                                                        <a href="{{ asset('storage/'.$shortcourse->attachment)}}">
                                                                                            view</a>
                                                                                    </td>
                                                                                </tr>
                                                                                @endforeach
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="staffprofiles">
                                                        <div class="col-sm-12" id="Div1">

                                                            @if($school->staffs()->where('privilege',2)->count() > 0)
                                                            <div class="col-sm-4" id="cat">
                                                                <div class="panel panel-primary">
                                                                    <div class="panel-heading"><span id="ctl00_ContentPlaceHolder1_rpDocumentBoxes_ctl00_DOCUMENT_TAB"
                                                                            class=" h5">Head Master</span>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <img class="img img-responsive img-size-50" src="{{ getStaffPhoto($school->staffs()->where('privilege',2)->first()) }}" />
                                                                    </div>
                                                                    <div class="panel-footer">
                                                                        <center>{{$school->staffs()->where('privilege',2)->first()->first_name."
                                                                            ".$school->staffs()->where('privilege',2)->first()->last_name
                                                                            }}</center>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endif @if($school->staffs()->where('privilege',5)->count() > 0)
                                                            <div class="col-sm-4" id="cat">
                                                                <div class="panel panel-success">
                                                                    <div class="panel-heading"><span id="ctl00_ContentPlaceHolder1_rpDocumentBoxes_ctl00_DOCUMENT_TAB"
                                                                            class=" h5">Deputy Header Teachers</span>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <center><img class="img img-responsive" style="width: 50%;"
                                                                                src="{{ getStaffPhoto($school->staffs()->where('privilege',5)->first()) }}"
                                                                            />
                                                                        </center>
                                                                    </div>
                                                                    <div class="panel-footer">
                                                                        <center>{{$school->staffs()->where('privilege',5)->first()->first_name."
                                                                            ".$school->staffs()->where('privilege',5)->first()->last_name
                                                                            }}</center>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="announcements">
                                                        <div class="col-sm-12" id="Div1">

                                                            <div class="col-sm-12" id="cat">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading"><span id="ctl00_ContentPlaceHolder1_rpDocumentBoxes_ctl00_DOCUMENT_TAB"
                                                                            class=" h5">Announcements</span>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        @foreach($school->announcements as $announcement)
                                                                        <div class="well well-success">
                                                                            {!! $announcement->announcement !!}
                                                                            <div class="panel-footer">
                                                                                <label>Creation Date
                                                                                    : </label>                                                                                {{ $announcement->created_at->diffForHumans()
                                                                                }}
                                                                            </div>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="contactus">
                                                        <div class="col-sm-12" id="Div1">

                                                            <div class="col-sm-8 offset-2" id="cat">
                                                                @if ($message = Session::get('success'))
                                                                <div class="alert alert-success alert-block">
                                                                    <button type="button" class="close" data-dismiss="alert">×
                                                                    </button>
                                                                    <strong>{{ $message }}</strong>
                                                                </div>
                                                                @endif
                                                                <form method="POST" action="{{ route('home.schools.sendmessage') }}">
                                                                    {{ csrf_field() }}
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <input type="hidden" name="school_email" value="{{ $school->email }}" />
                                                                                <input type="text" class="form-control" name="name" autocomplete="off" id="Name" placeholder="Name">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <input type="email" class="form-control" name="user_email" autocomplete="off" id="email" placeholder="E-mail">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <textarea class="form-control textarea" rows="5" name="message" id="Message" placeholder="Message"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <button type="submit" class="btn btn-info pull-right" style="margin-bottom: 20px;">Send a
                                                                            message
                                                                        </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix" style="clear: both;">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both; display: block;"></div>

                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>


    <!-- /Page Content > -->
    <!-- Start of Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4><a href="#">{{$school->school_name?:""}}</a></h4>
                    <hr>
                    <p><a href="#">Students</a></p>
                    <p><a href="#">Schools</a></p>
                    <p><a href="#">About</a></p>
                    <p><a href="#">Calendar</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>© <span id="currentYear">{{ date('Y') }}</span> <a href="#" target="_blank"> rp schools
                            websites</a></p>
                </div>
            </div>
        </div>
    </footer>
    <!-- /Start of Footer -->
    <!--/.container-->
    <!--/.footerwrapper-->

    <script type="text/javascript" language="javascript" src="{{asset('js/schoolinfo/core.js')}}"></script>
    @include('jsview') {!! JsValidator::formRequest('App\Http\Requests\CreateSchoolContactUsRequest', '#contactus'); !!} {{--
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/schoolinfo/main.js')}}"></script>--}} {{--
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/schoolinfo/element_main.js')}}"></script>--}}
</body>

</html>