<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show
<link rel='stylesheet' href='/fullcalendar/fullcalendar.css'/>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-red layout-boxed fixed">
<div>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper"
             style="background-image: url({{ asset('img/bg_wood.png') }}); background-attachment: fixed;">

        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->

                @yield('main-content')

                @if(auth()->guard('school')->check())
                    @include('school.layout.modals.setting')
                    @include('school.layout.modals.setPeriod')
                    @if( ! checkIfCompletedInfo() )
                        @include('school.staff.layout.modals.warnings')
                    @endif
                @endif
                @include('school.layout.modals.chagePassword')

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        {{--@include('adminlte::layouts.partials.controlsidebar')--}}

        @include('adminlte::layouts.partials.footer')
    </div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show
@if(activeMenu(['school.home']) == 'active')
    {{--<script src='/fullcalendar/lib/jquery.min.js'></script>--}}
    <script src='/fullcalendar/lib/moment.min.js'></script>
    <script src='/fullcalendar/fullcalendar.js'></script>
    <script>
        $(function () {

            // page is now ready, initialize the calendar...

            $('#calendar').fullCalendar({
                // put your options and callbacks here
            })

        });
    </script>
@endif
</body>
</html>
