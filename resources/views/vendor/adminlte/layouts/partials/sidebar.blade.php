<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    @if(isAllowed('7'))
                        <img src="{{ strlen(getStaff('photo')) > 0 ? getStaffPhoto(getStaff('photo')) : Gravatar::get($user->email) }}"
                             class="img-circle" alt="User Image" style="width: 80px; height: 43px;"/>
                    @elseif(auth()->guard('college')->check() || ! isset($user->email))
                        <img src="{{  getUserGravatar() }}" class="user-image" alt="User Image"/>
                    @else
                        <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                    @endif
                </div>
                <div class="pull-left info">
                    @if (auth()->guard('college')->check())
                        <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip"
                            title="{{ Auth::user()->name }}">{{ college('college') ? college('college')->short_name : Auth::user()->name }}</p>
                    @else
                        <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip"
                            title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>    
                    @endif
                    
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}
                    </a>
                </div>
            </div>
    @endif

    <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree"
            @if(auth()->guard('school')->check())
            {{--style="overflow-y: scroll;"--}}
            @endif>
            {{--<li class="header">{{ trans('adminlte_lang::message.header') }}</li>--}}

            @if(auth()->guard('school')->check() or auth()->guard('reb')->check())
                @include('school.layout.sidemenu')
            @elseif(auth()->guard('examiner')->check())
                @include('examiner.layout.sidemenu')
            @elseif(auth()->guard('wda')->check())
                @include('wda.layout.sidemenu')
            @elseif(auth()->guard('rp')->check())
                @include('rp.layout.sidemenu')
            @elseif(auth()->guard('college')->check())
                @include('college.layout.sidemenu')
            @elseif(auth()->guard('admin')->check())
                @include('admin.layout.sidemenu')
            @elseif(auth()->guard('district')->check())
                @include('district.layout.sidemenu')
            @elseif(auth()->guard('lecturer')->check())
                @include('lecturer.layout.sidemenu')
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
