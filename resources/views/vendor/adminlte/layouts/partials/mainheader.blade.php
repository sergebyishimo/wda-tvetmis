<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    {{--<a href="{{ url('/home') }}" class="logo">--}}
        {{--<!-- mini logo for sidebar mini 50x50 pixels -->--}}
        {{--<span class="logo-mini"><b>TVET</b></span>--}}
        {{--<!-- logo for regular state and mobile devices -->--}}
        {{--<span class="logo-lg"><b>TVET</b> IS</span>--}}
    {{--</a>--}}

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5) )
                        @include('school.qm-manual.layout.topmenu')
                    @endif
                <!-- User Account Menu -->
                    <li>
                        <a href="{{ url('/logout') }}" class="btn btn-info btn-flat" id="logout"
                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            {{ trans('adminlte_lang::message.signout') }}
                        </a>
                    </li>
                    <li class="dropdown user user-menu" id="user_menu" style="max-width: 280px;white-space: nowrap;">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           style="max-width: 280px;white-space: nowrap;overflow: hidden;overflow-text: ellipsis">
                            <!-- The user image in the navbar-->
                            @if(isAllowed('7'))
                                <img src="{{ strlen(getStaff('photo')) > 0 ? getStaffPhoto(getStaff('photo')) : Gravatar::get($user->email) }}"
                                     class="user-image" alt="User Image"/>
                            @elseif(auth()->guard('college')->check() || ! isset($user->email))
                                <img src="{{  getUserGravatar() }}" class="user-image" alt="User Image"/>
                            @else
                                <img src="{{  Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>
                        @endif
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs" data-toggle="tooltip"
                                              title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                @if(isAllowed('7'))
                                    <img src="{{ strlen(getStaff('photo')) > 0 ? getStaffPhoto(getStaff('photo')) : Gravatar::get($user->email) }}"
                                         class="user-image" alt="User Image"/>
                                @elseif(auth()->guard('college')->check() || ! isset($user->email))
                                    <img src="{{  getUserGravatar() }}" class="user-image" alt="User Image"/>
                                @else
                                    <img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>
                                @endif
                                <p>
                                    <span data-toggle="tooltip"
                                          title="{{ Auth::user()->name }}"><a href="{{route('get.user.profile')}}" style="text-decoration: none;color:white;">{{ Auth::user()->name }}</a></span>
                                    {{--<small>{{ trans('adminlte_lang::message.login') }} Nov. 2012</small>--}}
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <button type="button" data-toggle="modal" data-url=""
                                            class="btn btn-primary"
                                            data-target="#changePassword">Password
                                    </button>
                                </div>
                                @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5) )
                                    <div class="col-xs-4 text-center">
                                        <button type="button" data-toggle="modal"
                                                class="btn btn-success"
                                                data-target="#schoolSettings">Period
                                        </button>
                                    </div>
                                @endif
                                @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5) )
                                    <div class="col-xs-4 text-center">
                                        <a href="{{ route('school.get.info') }}" class="btn btn-info">Settings</a>
                                    </div>
                                @endif
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    @if(school('website') && auth()->guard('school')->check())
                                        <a class="btn btn-default btn-flat" href="{{ url(school('website')) }}"
                                           target="_blank">Website</a>
                                    @endif
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" class="btn btn-warning"
                                               style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</header>
