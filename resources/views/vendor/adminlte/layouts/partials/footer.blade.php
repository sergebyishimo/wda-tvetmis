<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
         <a href="http://wda.gov.rw/" target="_blank">WDA</a>
    </div>
    <!-- Default to the left -->
    <a href="http://www.rp.ac.rw/" target="_blank"></a><b>{{ config('app.name') }}</b></a>
</footer>
