<!-- REQUIRED JS SCRIPTS -->

@if(activeMenu(['wda.sp.report.result', 'wda.ap.report.result', 'wda.pr.report.result', 'wda.calendar.index']) != 'active')
    <!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
    <!-- Laravel App -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
@endif

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>
@if(!auth()->guard('school')->check())
    <script src="{{ asset('js/app_wda.js') }}" type="text/javascript"></script>
@else
@endif

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"
        integrity="sha256-qE/6vdSYzQu9lgosKxhFplETvWvqAAlmAuR+yPh/0SI=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.js"
        integrity="sha256-GqCMs8eqcNJo0k1Zw3TBSve9COCvjIX45PYKJlH0urU=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
        integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
<script>
    $(function () {
        $(".select2").select2();
        $(".dropdown-toggle").dropdown();
        $('#calendar').datepicker();
        $(".datepicker-year").datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            orientation: "bottom"
        });
    })
</script>
{{--@if(!auth()->guard('school')->check())--}}
@if(auth()->guard('wda')->check())
    <script>
        $(function () {
            $(".sidebar-menu").slimscroll({
                height: 'auto',
                alwaysVisible: true,
                color: '#ff2200',
                railVisible: true,
                railColor: '#999',
                railOpacity: 1,
                railVisible: true
            });
        })
    </script>
@endif
@yield('loop_st')
{{--@endif--}}
@yield('l-scripts')
<script src="{{ asset('js/rwanda-boarders.js') }}" type="text/javascript"></script>
@if(auth()->guard('school')->check() == true)
    <script src="{{ asset('js/school.js?v='.time()) }}" type="text/javascript"></script>
@else
<script src="{{ asset('js/password.js?v='.time()) }}" type="text/javascript"></script>
@endif


