@extends('school.settings.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span> School Settings </span>
@endsection

@section('box-body')
    <div class="box">
        <form class="" method="POST" action="{{ route('school.post.info') }}" enctype="multipart/form-data" id="info">
            <div class="box-body">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">School Logo</label>
                    <div style="margin-bottom: 5px;">
                        @if(school('logo'))
                            <img src="{{ Voyager::image(school('logo')) }}" alt="{{ school('name') }}" width="130px"
                                 height="100px">
                        @endif
                    </div>
                    <input type="file" name="logo">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="mid">MINEDUC ID</label>
                            <input type="text" class="form-control" name="mid" placeholder="Mineduc ID"
                                   value="{{ school('mid') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">School Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="School Name"
                                   value="{{ school('name') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="acronym">School Acronym</label>
                            <input type="text" class="form-control" id="acronym" name="acronym"
                                   placeholder="School Acronym"
                                   value="{{ school('acronym') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" size="30" maxlength="30" class="form-control" id="phone_number"
                                   name="phone_number"
                                   placeholder="School Phone Number" value="{{ school('phone_number') }}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">School Email</label>
                            <input type="email" size="30" maxlength="30" class="form-control" id="email" name="email"
                                   placeholder="School Public E-mail" value="{{ school('email') }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="website">School Website</label>
                            <input type="text" size="30" maxlength="30" id="website" class="form-control" name="website"
                                   placeholder="School Website" value="{{ school('website') }}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="mode">Studying Mode</label>
                            <select class="form-control" id="mode" name="mode">
                                <option {{ school('studying_mode') == "Boarding" ? 'selected' : "" }}>Boarding</option>
                                <option {{ school('studying_mode') == "Day" ? 'selected' : "" }}>Day</option>
                                <option {{ school('studying_mode') == "Both" ? 'selected' : "" }}>Both</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="diplinemax">Discipline Max</label>
                            <input type="text" class="form-control" size="3" maxlength="3"
                                   onkeypress="return isNumber(event)" name="discipline_max" required
                                   placeholder="Discipline Marks Maximum" id="diplinemax"
                                   value="{{ school('discipline_max') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="province">Province</label>
                            <select class="form-control" id="province" name="province">
                                <option value="">Choose here</option>
                                <option @if(school('province') == 'Kigali City') selected= @endif>Kigali City
                                </option>
                                <option @if(school('province') == 'Northern Province') selected= @endif>Northern
                                    Province
                                </option>
                                <option @if(school('province') == 'Southern Province') selected= @endif>Southern
                                    Province
                                </option>
                                <option @if(school('province') == 'Eastern Province') selected= @endif>Eastern
                                    Province
                                </option>
                                <option @if(school('province') == 'Western Province') selected= @endif>Western
                                    Province
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="district">District</label>
                            <select class="form-control" id="district" name="district">
                                @if(school('district'))
                                    <option>{{ school('district') }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sector">Sector</label>
                            <input type="text" name="sector" size="25" maxlength="25" class="form-control"
                                   placeholder="School Sector" value="{{ school('sector') }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="sector">School Type: </label>
                            <select name="school_type" class="select2">
                                <option>Choose School Type</option>
                                @foreach($schoolTypes as $type)
                                    <option name="{{$type->id  }}">{{ $type->school_type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="sector">School Rating: </label>
                            <select name="school_rating" class="select2">
                                <option>Choose School Rating:</option>
                                @foreach($schoolRatings as $rating)
                                    <option name="{{$rating->id  }}">{{ $rating->rating}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"
                                                                    aria-hidden="true"></span> Save
                </button>
            </div>
        </form>
    </div>
    @include('school.desks.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UpdateSchoolInfo', '#info'); !!}
@endsection