<div class="list-group list-group-horizontal">
    <a href="{{ route('school.get.info') }}" class="list-group-item {{ activeMenu('school.get.info') }}">School Info</a>
    <a href="{{ route('school.requests.index') }}" class="list-group-item {{ activeMenu('school.requests.index') }}">Requests</a>
    <a href="{{ route('school.desks.index') }}" class="list-group-item {{ activeMenu('school.desks.index') }}">Desks</a>
    <a href="{{ route('school.activityreports.index') }}"
       class="list-group-item {{ activeMenu('school.activityreports.index') }}">Activity Reports</a>
    <a href="{{ route('school.workshops.index') }}" class="list-group-item {{ activeMenu('school.workshops.index') }}">Workshops</a>
    <a href="{{ route('school.incubationcenters.index') }}"
       class="list-group-item {{ activeMenu('school.incubationcenters.index') }}">Incubation Centers</a>
    <a href="{{ route('school.productionunit.index') }}"
       class="list-group-item {{ activeMenu('school.productionunit.index') }}">Productionunit</a>
    <a href="{{ route('school.otherinfrastructure.index') }}"
       class="list-group-item {{ activeMenu('school.otherinfrastructure.index') }}">Otherinfrastructure</a>
</div>
<div class="list-group list-group-horizontal">
    <a href="{{ route('school.admissions.index') }}"
       class="list-group-item {{ activeMenu('school.admissions.index') }}">School Admissions</a>
    <a href="{{ route('school.shortcourses.index') }}"
       class="list-group-item {{ activeMenu('school.shortcourses.index') }}">Shortcourses</a>
    <a href="{{ route('school.announcements.index') }}"
       class="list-group-item {{ activeMenu('school.announcements.index') }}">Announcements</a>
    <a href="{{ route('school.costoftrainings.index') }}"
       class="list-group-item {{ activeMenu('school.costoftrainings.index') }}">Cost Of Trainings</a>
</div>