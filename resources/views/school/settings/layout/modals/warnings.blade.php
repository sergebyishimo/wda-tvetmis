<div class="modal fade " tabindex="-1" role="dialog" id="removeLevel">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Are Sure To Delete <b class="names"></b> ?</h4>
            </div>
            <form action=""
                  method="post">
                @method("DELETE")
                {{ csrf_field() }}
                <input type="hidden" class="level" name="level" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger pull-left">Yes, Continue</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade " tabindex="-1" role="dialog" id="disableDepartment">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Are Sure To Remove This Qualification ?</h4>
            </div>
            <form action=""
                  method="post">
                @method("DELETE")
                {{ csrf_field() }}
                <input type="hidden" class="department" name="department" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger pull-left">Yes, Continue</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->