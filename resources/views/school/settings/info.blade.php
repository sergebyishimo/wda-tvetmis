@extends('school.settings.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span> School Settings </span>
@endsection

@section('box-body')
    {!! Form::open(['route' => 'school.post.info', 'files' => true, 'id' => 'info']) !!}
    {!! Form::hidden('school_id', school(true)->id) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Primary information</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>School Code</label>
                                <input type="text" class="form-control flat" name="school_code"
                                       value="{{ $school->school_code }}"
                                       placeholder="School Code" readonly>
                            </div>
                            <div class="form-group">
                                <label>School Name</label>
                                <input type="text" class="form-control flat" name="school_name"
                                       value="{{ $school->school_name }}"
                                       placeholder="School Name">
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 col-form-label">School Type</label>
                                <div class="col-md-12">
                                    {!! Form::hidden('school_type', $school->school_type) !!}
                                    <select class="form-control flat" disabled>
                                        <option>Choose School Type ...</option>
                                        @foreach ($schoolTypes as $type)
                                            <option name="{{ $type->id  }}" {{ $school->school_type == $type->id ? 'selected' : '' }}>{{ $type->school_type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">E-mail:</label>
                                <input type="text" class="form-control flat" name="email"
                                       value="{{ $school->email or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Phone:</label>
                                <input type="text" class="form-control flat" name="phone"
                                       value="{{ $school->phone or '' }}">

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">School Status:</label>
                                <input type="text" class="form-control flat" readonly
                                       name="school_status"
                                       value="{{ $school->school_status }}">

                            </div>
                            <div class="form-group">
                                <label for="">School Activity</label>
                                <input type="text" class="form-control flat"
                                       name="school_activity"
                                       value="{{ $school->school_activity }}"
                                       readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Manager Name:</label>
                                <input type="text" class="form-control flat" name="manager_name"
                                       value="{{  $school->manager_name or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Manager Phone:</label>
                                <input type="text" class="form-control flat" name="manager_phone"
                                       value="{{  $school->manager_phone or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Manager E-mail:</label>
                                <input type="text" class="form-control flat" name="manager_email"
                                       value="{{  $school->manager_email or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Accreditation Status</label>
                                <input type="text" class="form-control" readonly
                                       name="accreditation_status"
                                       value="{{ $school->accreditation_status }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Location information</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Province</label>
                                        <select class="form-control flat" id="anprovince" name="province"
                                                data-autoload="n">
                                            @forelse($provinces as $key => $province)
                                                <option value="{{ $province->Province }}" {{ $province->Province == $school->province ? 'selected' : '' }}>
                                                    {{ $province->Province }}
                                                </option>
                                            @empty
                                                @if($school->province)
                                                    <option value="{{ $school->province }}" selected>
                                                        {{ $school->province }}
                                                    </option>
                                                @endif
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>District</label>
                                        <select class="form-control flat" id="ndistrict" name="district">
                                            @if($school->district)
                                                <option value="{{ $school->district }}" selected>
                                                    {{ $school->district }}
                                                </option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Sector:</label>
                                        <select class="form-control flat" id="nsector" name="sector">
                                            @if($school->sector)
                                                <option value="{{ $school->sector }}" selected>
                                                    {{ $school->sector }}
                                                </option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Cell:</label>
                                        <select class="form-control flat" id="ncell" name="cell">
                                            @if($school->cell)
                                                <option value="{{ $school->cell }}" selected>
                                                    {{ $school->cell }}
                                                </option>
                                            @endif
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Village:</label>
                                        <select class="form-control flat" id="nvillage" name="village">
                                            @if($school->village)
                                                <option value="{{ $school->village }}" selected>
                                                    {{ $school->village }}
                                                </option>
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Latitude:</label>
                                        <input type="text" class="form-control flat" name="latitude"
                                               value="{{ $school->latitude or '' }}">

                                    </div>
                                    <div class="form-group">
                                        <label for="">Longitude:</label>
                                        <input type="text" class="form-control flat" name="longitude"
                                               value="{{ $school->longitude or '' }}">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Key Infrastructure</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-7 col-form-label">Has Electricity</label>
                                        <div class="col-md-5">
                                            <select class="form-control flat" name="has_electricity">
                                                <option value="2" {{ $school->has_electricity == 2 ? 'selected': ''  }}>
                                                    Yes
                                                </option>
                                                <option value="1" {{ $school->has_electricity == 1 ? 'selected': ''  }}>
                                                    No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-7 col-form-label">Has Water</label>
                                        <div class="col-md-5">
                                            <select class="form-control flat" name="has_water">
                                                <option value="2" {{ $school->has_water == 2 ? 'selected': ''  }}>Yes
                                                </option>
                                                <option value="1" {{ $school->has_water == 1 ? 'selected': ''  }}>No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-7 col-form-label">Has Computer Lab</label>
                                        <div class="col-md-5">
                                            <select class="form-control flat" name="has_computer_lab">
                                                <option value="2" {{ $school->has_water == 2 ? 'selected': ''  }}>Yes
                                                </option>
                                                <option value="1" {{ $school->has_water == 1 ? 'selected': ''  }}>No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-7 col-form-label">Has Internet</label>
                                        <div class="col-md-5">
                                            <select class="form-control flat" name="has_internet">
                                                <option value="2">Yes</option>
                                                <option value="1">No</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-7 col-form-label">Has Library</label>
                                        <div class="col-md-5">
                                            <select class="form-control flat" name="has_library">
                                                <option value="2" {{ $school->has_library == 2 ? 'selected': ''  }}>
                                                    Yes
                                                </option>
                                                <option value="1" {{ $school->has_library == 1 ? 'selected': ''  }}>No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-7 col-form-label">Has Business or Strategic
                                            Plan</label>
                                        <div class="col-md-5">
                                            <select class="form-control flat"
                                                    name="has_business_or_strategic_plan">
                                                <option value="2" {{ $school->has_business_or_strategic_plan == 2 ? 'selected': ''  }}>
                                                    Yes
                                                </option>
                                                <option value="1" {{ $school->has_business_or_strategic_plan == 1 ? 'selected': ''  }}>
                                                    No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-7 col-form-label">Has Feeding Program</label>
                                        <div class="col-md-5">
                                            <select class="form-control flat" name="has_feeding_program">
                                                <option value="2" {{ $school->has_feeding_program == 2 ? 'selected': ''  }}>
                                                    Yes
                                                </option>
                                                <option value="1" {{ $school->has_feeding_program == 1 ? 'selected': ''  }}>
                                                    No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12" style="float:right;">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Key Statistics</h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group row">
                                <label class="col-md-7 col-form-label">Number of Desktops</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control flat"
                                           value="{{ $school->number_of_desktops }}"
                                           name="number_of_desktops">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-7 col-form-label">Number of Classrooms</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control flat"
                                           value="{{ $school->number_of_classrooms }}"
                                           name="number_of_classrooms">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-7 col-form-label">Number of Male Students</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control flat" value="{{ $school->students_males }}"
                                           name="students_males">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-7 col-form-label">Number of Female Students</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control flat"
                                           value="{{ $school->students_female }}"
                                           name="students_female">
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-7 col-form-label">Number of Male Staff
                                    Members</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control flat" name="staff_male"
                                           value="{{ $school->staff_male }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-7 col-form-label">Number of Female Staff
                                    Members</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control flat" name="staff_female"
                                           value="{{ $school->staff_female }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-7 col-form-label">Number of Generators</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control flat"
                                           value="{{ $school->number_of_generators }}"
                                           name="number_of_generators">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12" style="float:left">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Other Information</h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Accreditation Number</label>
                                <input type="text" class="form-control flat" value="{{ $school->accreditation_number }}"
                                       name="accreditation_number"
                                       placeholder="Accreditation Number" readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Website</label>
                                <input type="text" class="form-control flat" name="website"
                                       value="{{ $school->website }}"
                                       placeholder="Website">
                            </div>
                            <div class="form-group">
                                <label>Date of Establishment</label>
                                <input type="text" class="form-control datepicker-year flat"
                                       value="{{ $school->date_of_establishment }}"
                                       name="date_of_establishment">
                            </div>

                            <div class="form-group">
                                <label for="">Boarding / Day</label>
                                <select class="form-control flat" name="boarding_or_day">
                                    <option>Boarding</option>
                                    <option>Day</option>
                                    <option>Both</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">School Logo</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="school_logo"
                                               id="exampleInputFile">
                                    </div>
                                </div>
                                @if($school->school_logo)
                                    <img class="img img-responsive img-sm"
                                         src="{{ asset('storage/'.$school->school_logo) }}"/>
                                @endif
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Owner Name:</label>
                                <input type="text" class="form-control flat" name="owner_name"
                                       value="{{ $school->owner_name or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Owner Phone:</label>
                                <input type="text" class="form-control flat" name="owner_phone"
                                       value="{{ $school->owner_phone or '' }}">

                            </div>
                            <div class="form-group">
                                <label for="">Owner Type:</label>
                                <select class="form-control" name="owner_type">
                                    @foreach ($ownerships as $ownership)
                                        <option value="{{ $ownership->id }}">{{ $ownership->owner }}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="">Owner E-mail:</label>
                                <input type="text" class="form-control flat" name="owner_email"
                                       value="{{ $school->owner_email or '' }}">

                            </div>

                            <div class="form-group row">
                                <label class="col-md-12 col-form-label">School Rating</label>
                                <div class="col-md-12">
                                    {!! Form::hidden('school_rating', $school->school_rating) !!}
                                    <select name="school_rating" class="form-control flat" disabled>
                                        <option>Choose School Rate ...</option>
                                        @foreach ($schoolRating as $rating)
                                            <option value="{{ $rating->id  }}" {{ $school->school_rating ? ($school->school_rating == $rating->id ? 'selected' : '' ) : '' }}>{{ $rating->rating }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save School
                Information
            </button>

        </div>
    </div>
    {!! Form::close() !!}
    @include('school.desks.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UpdateExistingSchoolRequest', '#info'); !!}
@endsection