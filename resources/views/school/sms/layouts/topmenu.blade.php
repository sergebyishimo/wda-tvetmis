<div class="list-group list-group-horizontal">
    <a href="{{ route('school.sms.send', 's') }}" class="list-group-item">Send to
        Single Student</a>
    <a href="{{ route('school.sms.send', 'd') }}" class="list-group-item">Send By
        Department</a>
    <a href="{{ route('school.sms.send', 'l') }}" class="list-group-item">Send By
        Level</a>
    <a href="{{ route('school.sms.send', 'f') }}" class="list-group-item">Send
        to Staff</a>
</div>