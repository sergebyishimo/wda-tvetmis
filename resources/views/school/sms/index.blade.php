@extends('school.sms.layouts.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        th.dt-center, td.dt-center {
            text-align: center;
        }
    </style>
    @parent
@overwrite

@section('panel-title')
    <span>SMS</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h5 class="box-title">
                @isset($outbox)
                    All sent and received SMS
                @endisset
                @isset($sending)
                    Sending SMS
                @endisset
            </h5>
        </div>
        <div class="box-body">
            @isset($outbox)
                <table class="table table-striped table-bordered" id="dataTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>SMS Type</th>
                        <th>Receiver Number</th>
                        <th>SMS Body</th>
                        <th>Term</th>
                        <th>Period</th>
                        <th>Academic Year</th>
                        <th>Send</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($outbox)
                        @php($i = 1)
                        @foreach($outbox as $item)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item->type }}</td>
                                <td>{{ $item->receiver }}</td>
                                <td>{{ $item->message }}</td>
                                <td>{{ $item->term }}</td>
                                <td>{{ $item->period }}</td>
                                <td>{{ $item->acad_year }}</td>
                                <td>{{ $item->created_at->diffForHumans() }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            @endisset
            @isset($sending)
                {!! Form::open(['route'=>'school.sms.store', 'method' => 'post']) !!}
                {!! Form::hidden('on', $sending) !!}
                @if($sending == 's')
                    <div class="form-group">
                        {!! Form::label('student', "Student *") !!}
                        {!! Form::select('student', getFeeStudentsFormat(), null,
                        ['class' => 'form-control student-q select2',
                        'style' => 'width:100%',
                        'placeholder' => 'Select a student']) !!}
                    </div>
                @endif
                @if($sending == 'f')
                    <div class="form-group">
                        {!! Form::label('staff', "Staff *") !!}
                        {!! Form::select('staff', getStaffsFormat(), null,
                        ['class' => 'form-control staff-q select2',
                        'style' => 'width:100%',
                        'placeholder' => 'Select a student']) !!}
                    </div>
                @endif

                @if($sending == 'd' || $sending == 'l')
                    <div class="form-group">
                        <label for="qualification">Qualification <sup>(Department)</sup></label>
                        <select name="qualification" id="qualification" class="form-control select2 class-q"
                                style="width: 100%"
                                required>
                            <option value="" selected disabled>Select Qualification</option>
                            @foreach(getQualifications(school(true)->id) as $item)
                                <option value="{{ $item->id }}" @isset($request) {{ $request->input('qualification') == $item->id ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if($sending == 'l')
                    <div class="form-group level-group">
                        <label for="level">Level <sup>(Class)</sup></label>
                        <select name="level" id="level" class="form-control select2 class-l"
                                style="width: 100%"
                                required>
                            @if(isset($request))
                                <option value="{{ $request->input('level') }}"
                                        selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                            @else
                                <option value="" selected disabled>Select Level</option>
                            @endif
                        </select>
                    </div>
                @endif

                <div class="form-group">
                    {!! Form::label('message', "Message *") !!}
                    {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-md">Send</button>
                </div>
                {!! Form::close() !!}
            @endisset
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                ]
            });
        });
    </script>
@endsection