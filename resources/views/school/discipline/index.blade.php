@extends('school.discipline.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
@overwrite

@section('panel-title')
    <span>Discipline</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h5 class="box-title">Reduce marks to a student</h5>
        </div>
        {!! Form::open(['route' => 'school.discipline.store', 'id' => 'reduceMarks', 'class' => 'form', 'method' => 'post']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('student', "Student *") !!}
                        {!! Form::select('student', getFeeStudentsFormat(), null,
                        ['class' => 'form-control student-q select2',
                        'style' => 'width:100%',
                        'placeholder' => 'Select a student']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('marks', "Marks to reduce *") !!}
                        {!! Form::number('marks', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <center>
                            {!! Form::label('send_sms', "Inform Parent *") !!}
                            {!! Form::checkbox('send_sms', true, true,['class' => 'checkbox']) !!}
                        </center>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('fault', "Fault committed *") !!}
                        {!! Form::text('fault', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('comment', "Comment *") !!}
                        {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreDisciplineInfoRequest', '#reduceMarks'); !!}
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
        });
    </script>
@endsection