<div class="modal fade " tabindex="-1" role="dialog" id="queries">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-info">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Make a Query</h4>
            </div>
            {!! Form::open(['route' => 'school.discipline.view', 'method' => 'post', 'id' => 'searchQForm']) !!}
            <div class="modal-body">
                {{--<div class="panel panel-default">--}}
                {{--<div class="panel-body">--}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="radio-2">Reducing Marks</label>
                            <input type="radio" name="action" id="radio-2" value="c" checked>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="radio-1">View History</label>
                            <input type="radio" name="action" id="radio-1" value="h">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('term', "Term *") !!}
                            {!! Form::select('term', getTerm(), school(true)->settings->active_term?:null,
                            ['class' => 'form-control select2',
                            'style' => 'width:100%',
                            'placeholder' => 'Select a term']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('acad_year', "Academic Year *") !!}
                            {!! Form::select('acad_year', academicYear(), date('Y'),
                            ['class' => 'form-control select2',
                            'style' => 'width:100%',
                            'placeholder' => 'Choose class']) !!}
                        </div>
                    </div>
                </div>
                {{--</div>--}}
                {{--</div>--}}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 class="panel-title"><label for="classQ">Base on a single class</label></h5>
                            </div>
                            <div class="col-md-4 form-group">
                                <div class="radio pull-right">
                                    <input type="radio" name="based" id="classQ" value="c">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="qualification">Qualification <sup>(Department)</sup></label>
                                    <select name="qualification" id="qualification" class="form-control select2 class-q"
                                            style="width: 100%"
                                            required>
                                        <option value="" selected disabled>Select Qualification</option>
                                        @foreach(getQualifications(school(true)->id) as $item)
                                            <option value="{{ $item->id }}" @isset($request) {{ $request->input('qualification') == $item->id ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group level-group">
                                    <label for="level">Level <sup>(Class)</sup></label>
                                    <select name="level" id="level" class="form-control select2 class-l"
                                            style="width: 100%"
                                            required>
                                        @if(isset($request))
                                            <option value="{{ $request->input('level') }}"
                                                    selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                                        @else
                                            <option value="" selected disabled>Select Level</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 class="panel-title"><label for="studentQ">Base on a single student</label></h5>
                            </div>
                            <div class="col-md-4 form-group">
                                <div class="radio pull-right">
                                    <input type="radio" name="based" id="studentQ" value="s">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('student', "Student *") !!}
                                    {!! Form::select('student', getFeeStudentsFormat(), null,
                                    ['class' => 'form-control student-q select2',
                                    'style' => 'width:100%',
                                    'placeholder' => 'Select a student']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-md pull-left" type="submit">Search</button>
                        <button type="button" class="btn btn-default btn-md pull-right" data-dismiss="modal">Cancel
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('l-scripts')
    @parent
@endsection