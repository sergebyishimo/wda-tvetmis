<div class="list-group list-group-horizontal">
    {{--    @if(activeMenu(['school.discipline.index']) == "" )--}}
    <a href="{{ route('school.discipline.index') }}"
       class="list-group-item {{ activeMenu(['school.discipline.index']) }}">Single Student</a>
    {{--@endif--}}
    @if(isAllowed() || isAllowed(1) || isAllowed(6))
        <a href="#"
           class="list-group-item {{ activeMenu(['school.discipline.view']) }}" data-toggle="modal"
           data-target="#queries">Make
            Query</a>
    @endif
</div>