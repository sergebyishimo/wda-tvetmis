@extends('school.discipline.layout.main')

@section('l-style')
    @aprent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
@overwrite

@section('panel-title')
    <span>Discipline</span>
@endsection

@section('panel-body')
    <div class="box box-default">
        <div class="box-header">
            <h5 class="box-title"></h5>
        </div>
        {!! Form::open(['route' => 'school.discipline.store', 'id' => 'reduceMarks', 'class' => 'form', 'method' => 'post']) !!}
        <div class="box-body">

        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreDisciplineInfoRequest', '#reduceMarks'); !!}
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
        });
    </script>
@endsection