@extends('school.discipline.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
@overwrite

@section('panel-title')
    <span>Reduce Discipline marks</span>
@endsection

@section('panel-body')
    <div class="box box-default">
        @if($based == 's')
            <div class="box-heading">
                <h5 class="box-title row">
                    <span class="pull-left col-md-6 text-success">Names:&nbsp;{{ trim(strtoupper($student->fname." ".$student->lname)) }}</span>
                    <span class="col-md-6 text-primary">
                        <u class="pull-right">{{ $student->reg_no }}</u>
                        <spanl class="pull-right">Student Number:&nbsp;</spanl>
                    </span>
                </h5>
            </div>
        @endif
        @if($based == 'c')
            <div class="box-heading">
                <h5 class="box-title">{{ $level->rtqf->level_name }}
                    - {{ $level->department->qualification->qualification_title }}</h5>
            </div>
        @endif
        @if($based == 'c')
            {!! Form::open([ 'route' => 'school.discipline.store', 'class' => 'form inline-form', 'method' => 'post' ]) !!}
            {!! Form::hidden('term', $term) !!}
            {!! Form::hidden('year', $year) !!}
        @endif
        <div class="box-body">
            @if($based == 's')
                <p><h3>{{ $student->level->rtqf->level_name }}
                    - {{ $student->level->department->qualification->qualification_title }}</h3></p>
                <p><h5>{{ "Study Mode: ".$student->mode }}</h5></p>
                <p><h6>{{ "Term ".$term." | Academic Year: ".$year }}</h6></p>
                <div class="row">
                    <div class="col-md-12">
                        @php
                            $ex = getExpectedFees($student->level->id, $term, strtolower($student->mode));
                            $pa = getPaidFees($student->id, $term, $year);
                            $re = getRemainPayment($ex, $pa);
                        @endphp
                        <table class="table table-bordered">
                            <thead>
                            <tr class="bg-info" align="center">
                                <th>Expected Amount / RWF</th>
                                <th>Paid Amount / RWF</th>
                                <th>Remaining Amount / RWF</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr align="center">
                                <td>{{ number_format($ex) }}</td>
                                <td>{{ number_format($pa) }}</td>
                                <td>
                                    {{ number_format($re) }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        @if($pa < $ex)
                            <div class="box box-default">
                                <div class="box-body">
                                    {!! Form::open([ 'route' => 'school.fees.update', 'id' => 'addPayment', 'class' => 'form', 'method' => 'post', 'files' => true ]) !!}
                                    {!! Form::hidden('student', $student->id) !!}
                                    {!! Form::hidden('term', $term) !!}
                                    {!! Form::hidden('year', $year) !!}

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('amount', "Amount Paid *") !!}
                                                {!! Form::number('amount', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('slip_number', "Slip Number") !!}
                                                {!! Form::text('slip_number', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('bank_name', "Bank Name") !!}
                                                {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('bank_slip', "Bank Slip") !!}
                                                {!! Form::file('bank_slip', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-md btn-primary pull-left" type="submit">Save
                                                    Change
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
            @if($based == 'c')
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('marks', "Marks to reduce *") !!}
                                    {!! Form::number('marks', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('fault', "Fault committed *") !!}
                                    {!! Form::text('fault', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('comment', "Comment *") !!}
                                    {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => '2']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="dataTable" class="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Student Number</th>
                        <th>Student Names</th>
                        <th>Marks Have</th>
                        <th align="center">
                            <label for="sms_all">Send SMS</label>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($students->count() > 0)
                        @foreach($students->get() as $student)
                            <tr>
                                <td class="checkbox-student">
                                    <label for="s{{$student->id}}"></label>
                                    <input type="checkbox" name="student[]" class="checkbox-ui all-student"
                                           id="s{{$student->id}}" value="{{$student->id}}" checked>
                                </td>
                                <td>{{ $student->reg_no }}</td>
                                <td>{{ trim(strtoupper($student->fname." ".$student->lname)) }}</td>
                                <td>{{ getDisciplineMarks($student->id, $term, $year) }}</td>
                                <td align="center">
                                    <label for="{{$student->id}}"></label>
                                    <input type="checkbox" name="send_sms[{{$student->id}}]" class="checkbox-ui"
                                           id="{{$student->id}}" value="1" checked>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            @endif
        </div>
        @if($based == 'c')
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-md btn-primary pull-left" type="submit">Save Change</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        @endif
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable();
            // Listen for click on toggle checkbox
            $('.all-sms').click(function(event) {
                if(this.checked) {
                    // Iterate each checkbox
                    $('.checkbox-sms :checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $('checkbox-sms :checkbox').each(function() {
                        this.checked = false;
                    });
                }
                console.log('SMS Student');
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    @if($based == 's')
        @if($pa < $ex)
            {!! JsValidator::formRequest('App\Http\Requests\AddPaymentRequest', '#addPayment'); !!}
        @endif
    @endif
    <script>

    </script>
@endsection