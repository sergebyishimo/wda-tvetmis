<div class="list-group list-group-horizontal">
    {{--@if( activeMenu('school.students.index') == "" )--}}
    <a href="{{ route('school.shortcourses.index') }}"
       class="list-group-item {{ activeMenu('school.shortcourses.index') }}">View Short Courses</a>
    {{--@endif--}}
    <a href="{{ route('school.shortcourses.create') }}"
       class="list-group-item {{ activeMenu('school.shortcourses.create') }}">
        Add New ShortCourse</a>
</div>