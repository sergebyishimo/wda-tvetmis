@extends('school.permissions.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>School Qualification</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h4 class="box-title">Give Permission</h4>
        </div>
        {!! Form::open(['route' => 'school.permission.store', 'method' => 'post', 'id' => 'searchQForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('term', "Term *") !!}
                        {!! Form::select('term', getTerm(), school(true)->settings->active_term?:null,
                        ['class' => 'form-control',
                        'readonly' => true,
                        'placeholder' => 'Select a term']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('acad_year', "Academic Year *") !!}
                        {!! Form::select('acad_year', academicYear(), date('Y'),
                        ['class' => 'form-control',
                        'readonly' => true,
                        'placeholder' => 'Choose class']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="qualification">Qualification <sup>(Department)</sup></label>
                        <select name="qualification" id="qualification" class="form-control select2 class-q"
                                style="width: 100%"
                                required>
                            <option value="" selected disabled>Select Qualification</option>
                            @foreach(getQualifications(school(true)->id) as $item)
                                <option value="{{ $item->id }}" @isset($request) {{ $request->input('qualification') == $item->id ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group level-group">
                        <label for="level">Level <sup>(Class)</sup></label>
                        <select name="level" id="level" class="form-control select2 class-l"
                                style="width: 100%"
                                onchange="levelList(this.value, 'studentList2')"
                                required>
                            @if(isset($request))
                                <option value="{{ $request->input('level') }}"
                                        selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                            @else
                                <option value="" selected disabled>Select Level</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="list">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr style="background-color: #f0f0f0;color: #383838;font-size:13px">
                                <th width="40px" align="center">
                                    <center>
                                        <input type="checkbox" id="main_check" name="class_com" checked
                                               onClick="toggle(this)"/>
                                    </center>
                                </th>
                                <th>Registration Number</th>
                                <th>Student Name</th>
                            </tr>
                            </thead>
                            <tbody id="studentList2"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('destination', "Destination") }}
                        {{ Form::textarea('destination', null, ['class' => 'form-control', 'rows' => 2, 'required' => true]) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('reason', "Reason") }}
                        {{ Form::textarea('reason', null, ['class' => 'form-control', 'rows' => 2, 'required' => true]) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('observation', "Observation") }}
                        {{ Form::textarea('observation', null, ['class' => 'form-control', 'rows' => 2, 'required' => true]) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Leaving Date/Time </label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="leave_datetime" readonly
                                   value="{{ date('m/d/Y H:i') }}"/>
                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o bigger-110"></i>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="date-timepicker1">Returning Date/Time</label>
                        <div class="input-group">
                            <input id="date-timepicker1" type="text" name="return_datetime"
                                   class="form-control datepicker" required/>
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div style="height: 26px;">&nbsp;</div>
                    <div class="form-group">
                        <label for="informParents">Inform Parents</label>
                        <input type="checkbox" name="informParents" id="informParents" class="checkbox-ui" checked>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary btn-md pull-left" id="submitP" type="submit">Submit</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $("button[type='submit']").hide();
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('school.get.course.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = '<option value="" selected disabled>Select ...</option>';
                        $.each(JSON.parse(data), function (index, level) {
                            h += '<option value="' + level.id + '">' + level.name + '</option>';
                        });
                        $(".level-group select").html(h);
                    });
            });
        });

    </script>
@endsection