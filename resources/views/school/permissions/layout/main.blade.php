@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        @yield('panel-title')
    </div>
@endsection

@section('l-style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
          integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.0.9/daterangepicker.min.css"
          integrity="sha256-JJRV0bnuBsLoneb4spJy2UQETZAaLMGX23s5Z0np+s4=" crossorigin="anonymous"/>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div style="padding-left: 3px;">
                    @include('school.permissions.layout.topmenu')
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('feedback.feedback')
            </div>
        </div>
        @yield('panel-body')
    </div>
    @include('school.permissions.layout.modals.verify')
@endsection

@section('l-scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
            integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.min.js"
            integrity="sha256-fuPJ7xvV6OPcIGSJd2Xj7s/+2aWsVGapv+Uj/cuVOzk=" crossorigin="anonymous"></script>
    <script>
        $(function () {
            $(".select2").select2();
            $(".reduce, .historia, input[name='action'], .checkbox-ui").checkboxradio();
        });
    </script>
    <script>
        $(function () {
            $(".select2").select2();
            $('.datepicker').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            $('input[name=date-range-picker]').daterangepicker({
                'applyClass': 'btn-sm btn-success',
                'cancelClass': 'btn-sm btn-default',
                locale: {
                    applyLabel: 'Apply',
                    cancelLabel: 'Cancel',
                },
            })
                .prev().on('click', function () {
                $(this).next().focus();
            });
        });
        $("#verifyForm").on('submit', function () {
            $('#verifyForm select').prop('disabled', false);
        });
        function toggle(source) {
            checkboxes = document.getElementsByName('std[]');
            for (var i = 0, n = checkboxes.length; i < n; i++) {
                checkboxes[i].checked = source.checked;
            }
        }

    </script>
@endsection