<!-- Modal -->
<div class="modal fade" id="verifyPaermissions" tabindex="-1" role="dialog" aria-labelledby="verifyPaermissionsModal">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Check Given Permissions</h4>
            </div>
            {!! Form::open(['route' => 'school.permission.verify', 'class' => 'form', 'method' => 'post' , 'id' => 'verifyForm']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('term', "Term *") !!}
                            {!! Form::select('term', getTerm(), school(true)->settings->active_term?:null,
                            ['class' => 'form-control',
                            'disabled' => true,
                            'placeholder' => 'Select a term']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('acad_year', "Academic Year *") !!}
                            {!! Form::select('acad_year', academicYear(), date('Y'),
                            ['class' => 'form-control',
                            'disabled' => true,
                            'placeholder' => 'Choose class']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="qualification">Qualification <sup>(Department)</sup></label>
                            <select name="qualification" id="qualification" class="form-control select2 class-q"
                                    style="width: 100%"
                                    required>
                                <option value="" selected disabled>Select Qualification</option>
                                @foreach(getQualifications(school(true)->id) as $item)
                                    <option value="{{ $item->id }}" @isset($request) {{ $request->input('qualification') == $item->id ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group level-group">
                            <label for="level">Level <sup>(Class)</sup></label>
                            <select name="level" id="level" class="form-control select2 class-l"
                                    style="width: 100%"
                                    required>
                                @if(isset($request))
                                    <option value="{{ $request->input('level') }}"
                                            selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                                @else
                                    <option value="" selected disabled>Select Level</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">
                <button class="btn btn-primary pull-left">
                    Check Permissions
                </button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    Cancel
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>