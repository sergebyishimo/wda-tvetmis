@extends('school.permissions.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>School Qualification</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h4 class="box-title">Verify Permission
                | {{ ucwords($level->department->qualification->qualification_title) }}
                - {{ strtoupper($level->rtqf->level_name) }}</h4>
        </div>
        {!! Form::open(['route' => 'school.discipline.store', 'method' => 'post', 'id' => 'searchQForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="dataTable">
                        <thead>
                        <tr>
                            <th width="40px">#</th>
                            <th width="150px">Registration Number</th>
                            <th>Student Name</th>
                            <th>Leaving Date</th>
                            <th>Returning Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($permissions->count() > 0)
                            @php($x=1)
                            @foreach($permissions->get() as $item)
                                @if($item->student->level_id == $level->id)
                                    <tr>
                                        <td>{{ $x++ }}</td>
                                        <td>{{ $item->student->reg_no }}</td>
                                        <td>{{ $item->student->names }}</td>
                                        <td>{{ date("F d, Y", strtotime($item->leaving_datetime)) }}</td>
                                        <td>{{ date("F d, Y", strtotime($item->returning_datetime)) }}</td>
                                        <td>
                                            <a href="{{ route('school.permission.destroy', $item->id) }}" class="btn btn-sm btn-primary">Verify</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'desc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
        });
    </script>
@endsection