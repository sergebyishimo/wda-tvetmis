@extends('school.costoftrainings.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Costs Of  Trainings</span>
@endsection

@section('box-body')
    <div class="box">
        @if($costoftraining)
            @php($route = ['school.costoftrainings.update', $costoftraining->id])
            @php($id = "updateCost")
        @else
            @php($route = 'school.costoftrainings.store')
            @php($id = "createCost")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
        {!! Form::hidden('school_id', school(true)->id) !!}
        @if($costoftraining)
            @method("PATCH")
            {!! Form::hidden('id', $costoftraining->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                {{--<div class="col-md-4">--}}
                {{--<div class="form-group">--}}
                {{--{!! Form::label('trades_id', "Trades *") !!}--}}
                {{--{!! Form::select('trades_id', $array= ['key'=>'value'] , null , ['class' => 'form-control select2']) !!}--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('cost_of_consumables_per_year', "Estimated Consumable Budget Required per Year *") !!}
                        {!! Form::text('cost_of_consumables_per_year', $costoftraining ? $costoftraining->cost_of_consumables_per_year: "", ['class' => 'form-control currency']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('budget_actually_available_for_salaries_per_year', "Budget Actually Available for Salaries per Year *") !!}
                        {!! Form::text('budget_actually_available_for_salaries_per_year', $costoftraining ? $costoftraining->budget_actually_available_for_salaries_per_year: "", ['class' => 'form-control currency']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('estimated_operational_budget_required_per_year', "Estimated Operational Budget Required per Year *") !!}
                        {!! Form::text('estimated_operational_budget_required_per_year', $costoftraining ? $costoftraining->estimated_operational_budget_required_per_year: "", ['class' => 'form-control currency']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('estimated_total_salaries_year', "Estimated Total Salaries Required per Year *") !!}
                        {!! Form::text('estimated_total_salaries_year', $costoftraining ? $costoftraining->estimated_total_salaries_year: "", ['class' => 'form-control currency']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('budget_available_for_consumables', "Budget Actually Available for Consumables per Year *") !!}
                        {!! Form::text('budget_available_for_consumables', $costoftraining ? $costoftraining->budget_available_for_consumables: "", ['class' => 'form-control currency']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('operational_funds_available_per_year', "Budget Actually Available for Operational Funds per Year *") !!}
                        {!! Form::text('operational_funds_available_per_year', $costoftraining ? $costoftraining->operational_funds_available_per_year: "", ['class' => 'form-control currency', 'required' => true]) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('academic_year', "Academic Year *") !!}
                        {!! Form::select('academic_year', academicYear(null, null, true), $costoftraining ? $costoftraining->academic_year: null, ['class' => 'form-control select2', 'required' => true] ) !!}
                        {{--{!! Form::text('academic_year', $costoftraining ? $costoftraining->academic_year: "", ['class' => 'form-control', 'required' => true]) !!}--}}
                    </div>
                </div>

            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $costoftraining ? "Update Cost Of Training" : "Save Cost Of Trainings" }}
                    </button>
                    <a href="{{ route('school.costoftrainings.index') }}"
                       class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('school.requests.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
            $(".currency").inputmask({alias: "currency", prefix: ''});
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($costoftraining)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateCostOfTrainingRequest', '#updateCost'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateCostOfTrainingRequest', '#createCost'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $("#qualification").on("change", function () {
                var dep = $(this).val();
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@endsection