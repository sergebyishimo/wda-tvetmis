@extends('wda.layout.main')
@section('htmlheader_title', "Cost Of Trainings")
@section('l-style')
    @parent
@overwrite

@section('panel-title')
    <span>Cost Of Trainings</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-body" style="overflow-x: auto;">
            <table class="table table-striped" id="dataTableBtnLoad">
                <thead>
                <th>School Name</th>
                <th>Academic Year</th>
                <th>Estimated Consumable Budget Required per Year</th>
                <th>Estimated Total Salaries Required per Year</th>
                <th>Budget Actually Available for Consumables per Year</th>
                <th>Budget Actually Available for Operational Funds per Year</th>
                <th>Budget Actually Available for Salaries per Year</th>
                <th>Estimated Operational Budget Required per Year</th>
                <th>Totals</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script>
        $(function () {
            $("#dataTableBtnLoad").DataTable({
                dom: 'Blfrtip',
                serverSide: true,
                processing: true,
                pager: true,
                colReorder: true,
                ajax: '{{ route("wda.data.get.costoftrainings") }}',
                "oSearch": {"bSmart": false},
                columns: [
                    {data: 'school_name', name: 'accr_schools_information.school_name'},
                    {data: 'academic_year', name: 'cost_of_training.academic_year', orderable:true, searchable:true},
                    {data: 'cost_of_consumables_per_year', name: 'cost_of_training.cost_of_consumables_per_year', orderable:true, searchable:true},
                    {data: 'estimated_total_salaries_year', name: 'cost_of_training.estimated_total_salaries_year', orderable:true, searchable:true},
                    {data: 'budget_available_for_consumables', name: 'cost_of_training.budget_available_for_consumables', orderable:true, searchable:true},
                    {data: 'operational_funds_available_per_year', name: 'cost_of_training.operational_funds_available_per_year', orderable:true, searchable:true},
                    {data: 'budget_actually_available_for_salaries_per_year', name: 'cost_of_training.budget_actually_available_for_salaries_per_year', orderable:true, searchable:true},
                    {data: 'estimated_operational_budget_required_per_year', name: 'cost_of_training.estimated_operational_budget_required_per_year', orderable:true, searchable:true},
                    {data: 'total', orderable:false, searchable:false},
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                initComplete: function () {
                    var x = 0;
                    var lenC = this.api().columns().length;
                    this.api().columns().every(function () {
                        if (x > 0 ){
                            var column = this;
                            var select = $('<select class="select2"><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        x++;
                    });
                }
            });
        });
    </script>
@endsection