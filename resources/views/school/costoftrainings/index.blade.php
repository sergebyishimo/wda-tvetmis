@extends('school.costoftrainings.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Cost Of Trainings</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th>Estimated Consumable Budget Required per Year</th>
                <th>Estimated Total Salaries Required per Year</th>
                <th>Budget Actually Available for Consumables per Year</th>
                <th>Budget Actually Available for Operational Funds per Year</th>
                <th>Budget Actually Available for Salaries per Year</th>
                <th>Estimated Operational Budget Required per Year</th>
                <th>Academic Year</th>
                {{--<th>Annual Turn Over(RWF)</th>--}}
                {{--<th>Attachment</th>--}}
                <th>Created At</th>
                <th></th>
                </thead>
                <tbody>
                @if($costoftrainings->count() > 0)
                    @foreach($costoftrainings as $costoftraining)
                        <tr>
                            <td>
                                {{ $costoftraining->cost_of_consumables_per_year }}
                            </td>
                            <td>
                                {{ $costoftraining->estimated_total_salaries_year }}
                            </td>
                            <td>
                                {{ $costoftraining->budget_available_for_consumables }}
                            </td>
                            <td>
                                {{ $costoftraining->operational_funds_available_per_year }}
                            </td>
                            <td>
                                {{ $costoftraining->budget_actually_available_for_salaries_per_year }}
                            </td>
                            <td>
                                {{ $costoftraining->estimated_operational_budget_required_per_year }}
                            </td>
                            <td>
                                {{ $costoftraining->academic_year }}
                            </td>
                            <td>
                                {{ $costoftraining->created_at->diffForHumans() }}
                            </td>
                            <th>
                                <a href="{{ route('school.costoftrainings.edit', $costoftraining->id) }}" class="btn btn-info btn-sm">Edit</a>
                                <button type="button"
                                        data-url="{{ route('school.workshops.destroy', $costoftraining->id) }}"
                                        data-names="{{ $costoftraining->id }}"
                                        class="btn btn-danger btn-sm btn-delete">Delete
                                </button>
                            </th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('school.costoftrainings.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false,
                "responsive": true,
                "scrollX": true
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
@endsection