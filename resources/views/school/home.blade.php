@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    
    
    <div class="container-fluid">
        @if(isAllowed())
            Dashboard
        @else
            Welcome {{ getStaff('names') }} <sub>from <i>{{ school('name') }}</i></sub>
        @endif
    </div>
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            {{--<div class="col-md-12">--}}
                {{--<div class="alert alert-info alert-dismissible">--}}
                    {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
                    {{--<h4><i class="icon fa fa-info"></i> Welcome Message</h4>--}}
                    {{--Info alert preview. This alert is dismissable.--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Notification</h4>
                    Info alert preview. This alert is dismissable.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{ school(true)->students()->where('status','active')->count() }}</h3>
                                <p>Students</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            @if(isAllowed())
                                <a href="{{ route('school.students.index') }}" class="small-box-footer">
                                    View all <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-gray">
                            <div class="inner">
                                <h3>{{ school(true)->staffs()->where('status','active')->count() }}</h3>

                                <p>Staffs</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-knife"></i>
                            </div>
                            @if(isAllowed())
                                <a href="{{ route('school.staff.index') }}" class="small-box-footer">View all <i
                                            class="fa fa-arrow-circle-right"></i></a>
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-blue-active">
                            <div class="inner">
                                <h3> {{ school(true)->staffs()->count() }} </h3>
                                <p>Staff</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-ios-gear"></i>
                            </div>
                            @if(isAllowed())
                                <a class="small-box-footer">
                                    &nbsp;
                                </a>
                            @endif
                        </div>
                    </div>

                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red-active">
                            <div class="inner">
                                    <h3> {{ school(true)->staffs()->where('privilege', 7)->count() }} </h3>

                                <p>Teaching Staff</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-chatbox"></i>
                            </div>
                            @if(isAllowed())
                                <a class="small-box-footer">&nbsp;</a>
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                </div>

                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-purple">
                            <div class="inner">
                                <h3 style="font-size:30px"> {{ number_format(App\FeesExpected::where('school_id', school('id'))->sum('boarding') + App\FeesExpected::sum('day')) }} </h3>
                                <p>Fees Due</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-male"></i>
                            </div>
                            @if(isAllowed())
                                <a class="small-box-footer">
                                    &nbsp;
                                </a>
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{ number_format(App\Payment::where('school_id', school('id'))->whereYear('created_at', '=', date('Y'))->sum('amount')) }}</h3>
                                <p>Fees Paid</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            @if(isAllowed())
                                <a href="{{ route('school.class.index') }}" class="small-box-footer">View all <i
                                            class="fa fa-arrow-circle-right"></i></a>
                            @endif
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{ App\Department::where('school_id', school('id'))->sum('capacity_students') }}</h3>
                                <p>Students Capacity</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            @if(isAllowed())
                                <a href="{{ route('school.class.index') }}" class="small-box-footer">View all <i
                                            class="fa fa-arrow-circle-right"></i></a>
                            @endif
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                        <div class="small-box bg-orange">
                            <div class="inner">
                                <h3>{{ App\Department::where('school_id', school('id'))->count() }}</h3>
                                <p>Qualification Offered</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            @if(isAllowed())
                                <a href="{{ route('school.class.index') }}" class="small-box-footer">View all <i
                                            class="fa fa-arrow-circle-right"></i></a>
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    {{--div.col-lg---}}
                <!-- ./col -->
                </div>
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id='calendar'></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @if(school('studying_mode') == 'Both')
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-black-gradient">
                                <div class="inner">
                                    <h3>
                                        {{
                                            school(true)->students()
                                            ->where('status','active')
                                            ->where('mode', "Day")
                                            ->count()
                                         }}
                                    </h3>

                                    <p>Study Mode: Day</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pizza"></i>
                                </div>
                                @if(isAllowed())
                                    <a class="small-box-footer">&nbsp;</a>
                                @endif
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-fuchsia-active">
                                <div class="inner">
                                    <h3>
                                        {{
                                            school(true)->students()
                                            ->where('status','active')
                                            ->where('mode', "Boarding")
                                            ->count()
                                        }}
                                    </h3>
                                    <p>Study Mode: Boarding</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-disc"></i>
                                </div>
                                @if(isAllowed())
                                    <a class="small-box-footer">&nbsp;</a>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>



@endsection