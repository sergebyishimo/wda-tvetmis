<div class="list-group list-group-horizontal">
    {{--@if( activeMenu('school.students.index') == "" )--}}
    <a href="{{ route('school.admissions.index') }}"
       class="list-group-item {{ activeMenu('school.admissions.index') }}">View Admission Requirements</a>
    {{--@endif--}}
    <a href="{{ route('school.admissions.create') }}"
       class="list-group-item {{ activeMenu('school.admissions.create') }}">
        Add Admission Requirements</a>
</div>