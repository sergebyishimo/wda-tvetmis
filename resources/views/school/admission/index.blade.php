@extends('school.admission.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>School Admission requirement</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            @if($schooladmission)
                <div class="panel panel-success">
                    <div class="panel-heading"><label> Admissions Requirements for : </label> <label
                                class="text-green">{{ $schooladmission->school->school_name or "" }}</label> @if($schooladmission->attachment)
                            <span class="pull-right"><a class="btn btn-sm btn-success"
                                                        href="{{ asset('storage/'.$schooladmission->attachment)}}"> view attachment</a></span> @else
                            <span class="pull-right">No Attachments Available</span> @endif</div>
                    <div class="panel-body">
                        {!! $schooladmission->admission_requirements !!}

                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('school.admissions.edit', $schooladmission->id) }}"
                           class="btn btn-info btn-sm">Edit</a>
                        <button type="button" data-url="{{ route('school.admissions.destroy', $schooladmission->id) }}"
                                data-names="{{ $schooladmission->id }}"
                                class="btn btn-danger btn-sm btn-delete">Delete
                        </button>
                    </div>
                </div>
            @else
                <p><label class="text-green"> No Admission Requirements Available</label></p>
            @endif
        </div>
    </div>
    @include('school.admission.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection