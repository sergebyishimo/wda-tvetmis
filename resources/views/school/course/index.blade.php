@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Courses
@endsection

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        input[type="checkbox"][readonly] {
            pointer-events: none;
        }

        .have-checked {
            background-color: #f5f5f5;
        }
    </style>
@overwrite

@section('contentheader_title')
    <div class="container-fluid">
        <span>Courses</span>
    </div>
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-body">
                                <form action="{{ route('school.course.listing') }}" method="post" class="form">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="term">Term or Semester</label>
                                                <select name="term" id="term" class="form-control select2" required>
                                                    <option value="" selected disabled>Select Term</option>
                                                    @if(count(getTerm()) > 0)
                                                        @foreach(getTerm() as $key => $item)
                                                            <option value="{{ $key }}" @isset($request) {{ $request->input('term') == $key ? 'selected' : '' }} @endisset >{{ $item }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="qualification">Qualification <sup>(Department)</sup></label>
                                                <select name="qualification" id="qualification"
                                                        class="form-control select2 class-q"
                                                        required>
                                                    <option value="" selected disabled>Select Qualification</option>
                                                    @foreach(getQualifications(school(true)->id) as $item)
                                                        <option value="{{ $item->uuid }}" @isset($request) {{ $request->input('qualification') == $item->uuid ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group level-group">
                                                <label for="level">Level <sup>(Class)</sup></label>
                                                <select name="level" id="level" class="form-control select2 class-l"
                                                        required>
                                                    @if(isset($request))
                                                        <option value="{{ $request->input('level') }}"
                                                                selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                                                    @else
                                                        <option value="" selected disabled>Select Level</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">&nbsp;</label>
                                                <button type="submit" class="btn btn-sm btn-block btn-info btn-submit"
                                                        disabled>
                                                    View
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @isset($request)
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title row">
                                <div class="col-md-12">
                                    <span class="pull-left">List of <u><b>{{  ucwords($qualification->qualification_title)  }}</b></u>'s Modules <sup>Courses</sup></span>
                                    <span class="pull-right">Code: <label
                                                class="label-dark">{{ $qualification->qualification_code }}</label></span>
                                </div>
                            </h3>
                        </div>
                        <form action="{{ route('school.course.store') }}" method="post" class="form" id="createCourse">
                            {{ csrf_field() }}
                            <input type="hidden" name="level" value="{{ $request->input('level') }}">
                            <input type="hidden" name="term" value="{{ $request->input('term') }}">
                            <div class="panel-body">
                                <table class="table table-hover" id="dataTable">
                                    <thead>
                                    <th>#</th>
                                    <th><input type="checkbox" disabled></th>
                                    <th>Module Title</th>
                                    <th>Module Code</th>
                                    <th>Module Type</th>
                                    <th>Max Marks</th>
                                    <th>Module Teacher</th>
                                    </thead>
                                    <tbody>
                                    @if($qualification->modules->count() > 0)
                                        @php($l=1)
                                        @foreach($qualification->modules as $module)
                                            @php($ex = getMyCourseInfo($module->id, $request->input('level'), $request->input('term'), false, "module_id"))
                                            <tr class="tr-{{ $module->id }} {{ $ex == $module->id ? 'have-checked' : '' }}">
                                                <th>
                                                    <label class="label label-default {{ $ex == $module->id ? 'label-success' : $module->id }}">{{ $l }}</label>
                                                </th>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <input type="checkbox" class="module"
                                                                   name="modules[{{$l}}]"
                                                                   id="{{ $module->id }}"
                                                                   data-id="{{ $module->id }}"
                                                                   {{ $ex == $module->id ? "checked readonly" : '' }}
                                                                   value="{{ $module->id }}">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <small class="test"></small>
                                                    <h5><b><label for="{{ $ex == $module->id ? '' : $module->id }}"
                                                                  class="module-h"
                                                                  data-id="{{ $module->id }}"
                                                                  style="cursor: pointer;">{{ ucwords($module->module_title) }}</label></b>
                                                    </h5>
                                                    <small>Credits: {{ $module->credits }} |
                                                        Hours: {{ $module->learning_hours }}</small>
                                                </td>
                                                <td>{{ $module->module_code }}</td>
                                                <td>{{ $module->type ? ucwords($module->type->title) : "" }}</td>
                                                <td width="40px">
                                                    <div class="form-group">
                                                        <input type="number" class="form-control"
                                                               @if(getMyCourseInfo($module->id, $request->input('level'), $request->input('term')) != "")
                                                               value="{{ getMyCourseInfo($module->id, $request->input('level'), $request->input('term')) }}"
                                                               @else
                                                               value=""
                                                               @endif
                                                               name="max_marks[{{$l}}]" style="width: 70px">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="teacher[{{$l}}]" class="form-control select2"
                                                                style="width: inherit">
                                                            <option value="" selected disabled>Select...</option>
                                                            @foreach(getSchoolTeach() as $item)
                                                                <option value="{{ $item->id }}"
                                                                        {{ getMyCourseInfo($module->id, $request->input('level'), $request->input('term'), false, "staffs_info_id") == $item->id ? "selected" : '' }}>
                                                                    {{ ucwords(trim($item->first_name." ".$item->last_name)) }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php($l++)
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-md btn-success pull-left">Save Changes
                                        </button>
                                        <a href="{{ route('school.course.index') }}"
                                           class="btn btn-md btn-warning pull-right">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endisset
    </div>
    @include('school.course.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".level-group").hide();
            $(".class-q").on("change", function (e) {
                $(".level-group").show();
                var op = $(this).val();
                $.get("{{ route('school.get.course.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = '<option value="" selected disabled>Select ...</option>';
                        $.each(JSON.parse(data), function (index, level) {
                            h += '<option value="' + level.id + '">' + level.name + '</option>';
                        });
                        $(".level-group select").html(h);
                        $(".btn-submit").attr("disabled", false);
                    }
                );
            });
            $(".module").on("change", function () {
                var d = $(this).data('id');
                if ($('.checkbox #' + d).is(":checked") != true) {
                    $(".checkbox #" + d).val("");
                    $("." + d).addClass("label-default").removeClass("label-success");
                    $(".tr-" + d).removeClass('have-checked');
                }
                else {
                    $(".checkbox #" + d).val(d);
                    $("." + d).removeClass("label-default").addClass("label-success");
                    $(".tr-" + d).addClass('have-checked');
                }
            });
        });

    </script>
    @isset($request)
        <script>
            $(function () {
                $(".level-group").show();
                $(".btn-submit").attr("disabled", false);
            })
        </script>
    @endisset
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection