@extends('school.productionunit.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Production Unit</span>
@endsection

@section('box-body')
    <div class="box">
        @if($productionunit)
            @php($route = ['school.productionunit.update', $productionunit->id])
            @php($id = "updateProductionUnitRequest")
        @else
            @php($route = 'school.productionunit.store')
            @php($id = "createProductionUnitRequest")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
            {!! Form::hidden('school_id', school(true)->id) !!}
        @if($productionunit)
            @method("PATCH")
            {!! Form::hidden('id', $productionunit->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('production_unit_name', "Production Unit Name") !!}
                        {!! Form::text('production_unit_name', $productionunit ? $productionunit->production_unit_name : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('operation_field', "Operation Field *") !!}
                        {!! Form::text('operation_field', $productionunit ? $productionunit->operation_field : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('production_unit_capital_francs', "Production Unit Capital Francs *") !!}
                        {!! Form::text('production_unit_capital_francs', $productionunit ? $productionunit->production_unit_capital_francs : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('annual_turnover_francs', "Annual Turnover Francs *") !!}
                        {!! Form::text('annual_turnover_francs', $productionunit ? $productionunit->production_unit_capital_francs : "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('monthly_revenues_francs', "Monthly Revenues Francs *") !!}
                        {!! Form::text('monthly_revenues_francs', $productionunit ? $productionunit->monthly_revenues_francs : "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('employees_male', "Employees Male *") !!}
                        {!! Form::text('employees_male', $productionunit ? $productionunit->employees_male : "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('employees_female', "Employees Female *") !!}
                        {!! Form::text('employees_female', $productionunit ? $productionunit->employees_female : "", ['class' => 'form-control', 'required' => true]) !!}

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('funds_source', "Funds Source *") !!}
                        {!! Form::text('funds_source', $productionunit ? $productionunit->funds_source : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('rdb_registration_number', "RDB Registration Number *") !!}
                        {!! Form::text('rdb_registration_number', $productionunit ? $productionunit->rdb_registration_number : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('community_impact', "Community Impact *") !!}
                        {!! Form::text('community_impact',$productionunit ? $productionunit->community_impact : "",['class' => 'form-control']) !!}

                    </div>
                </div>
                <div class="col-md-4 pull-right">
                    <div class="form-group">
                    {!! Form::label('attachments', "Attachements") !!}
                    {!! Form::file('attachments',['class' => 'filer_docs_input form-control']) !!}

                    </div>
                </div>
            </div>


        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $productionunit ? "Update Request" : "Send Request" }}
                    </button>
                    <a href="{{ route('school.requests.index') }}"
                       class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('school.productionunit.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($productionunit)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateProductionUnitRequest', '#updateProductionUnitRequest'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateProductionUnitRequest', '#createProductionUnitRequest'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: docx, xlsx,ppt or pdf");

            $("#qualification").on("change", function () {
                var dep = $(this).val();
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@endsection