@extends('school.productionunit.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Production Unit Management</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable">
                    <thead>
                    <th>Production Unit Name</th>
                    <th>Operation Field</th>
                    <th>Production Unit Capital(RWF)</th>
                    <th>Annual Turn Over(RWF)</th>
                    <th>Monthly Revenues(RWF) </th>
                    <th>Employee Male</th>
                    <th>Employee Female</th>
                    <th>Founds Source</th>
                    <th>RDB Regestration Number</th>
                    <th>Community Impact</th>
                    <th>Attachment</th>
                    <th>Created At</th>
                    <th></th>
                    </thead>
                    <tbody>
                    @if($productionunits->count() > 0)
                        @foreach($productionunits as $productionunit)
                            <tr>
                                <td>
                                    {{ $productionunit->production_unit_name }}
                                </td>
                                <td>
                                    {{ $productionunit->operation_field }}
                                </td>
                                <td>
                                    {{ $productionunit->production_unit_capital_francs }}
                                </td>
                                <td>
                                    {{ $productionunit->annual_turnover_francs }}
                                </td>
                                <td>
                                    {{ $productionunit->monthly_revenues_francs }}
                                </td>
                                <td>
                                    {{ $productionunit->employees_male }}
                                </td>
                                <td>
                                    {{ $productionunit->employees_female }}
                                </td>
                                <td>
                                    {{ $productionunit->funds_source }}
                                </td>
                                <td>
                                    {{ $productionunit->rdb_registration_number }}
                                </td>
                                <td>
                                    {{ $productionunit->community_impact }}
                                </td>
                                <td>
                                    <a href="{{ asset('storage/'.$productionunit->attachments) }}">View</a>
                                </td>
                                <td>
                                    {{ $productionunit->created_at->diffForHumans() }}
                                </td>
                                <th>
                                    <a href="{{ route('school.productionunit.edit', $productionunit->id) }}" class="btn btn-info btn-sm">Edit</a>
                                    <button type="button" data-url="{{ route('school.productionunit.destroy', $productionunit->id) }}"
                                            data-names="{{ $productionunit->id }}"
                                            class="btn btn-danger btn-sm btn-delete">Delete
                                    </button>
                                </th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('school.productionunit.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection