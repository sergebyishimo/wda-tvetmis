@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Staff
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('contentheader_title')
    <div class="container-fluid">
        School Staff
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-2">
                <div class="box">
                    <div class="box-body">
                        @include('school.staff.layout.topmenu')
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="dataTable">
                            <thead>
                            <th>Photo</th>
                            <th>Names</th>
                            <th>Position</th>
                            <th>Gender</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>TVET Training</th>
                            @can('isSchoolManager')
                            <th>Actions</th>
                            @endcan
                            </thead>
                            <tbody>
                            @if($staffs->count() > 0)
                                @foreach($staffs as $staff)
                                    <tr>
                                        <td>
                                            @if($staff->photo != null)
                                                <img src="{{ getStaffPhoto($staff->photo) }}" alt=""
                                                     class="img-responsive img-rounded" style="width: 100px;">
                                            @endif
                                        </td>
                                        <td>
                                            @if(Gate::check('isSchoolManager'))
                                            <a href="{{ route('school.staff.profile',['profile'=>$staff->id]) }}">{!! $staff->names !!}</a>
                                            @else
                                              {!! $staff->names !!}

                                            @endif
                                        </td>
                                        <td>{!! getSchoolUserPrivilege($staff->id) !!}</td>
                                        <td>{!! $staff->gender !!}</td>
                                        <td><a href="#" class="label label-primary phone-l"
                                               data-phone="{{ $staff->phone_number }}">{!! $staff->phone_number !!}</a>
                                        </td>
                                        <td><a href="#" class="label label-primary email-l"
                                               data-email="{{ $staff->email }}">{!! $staff->email !!}</a></td>
                                        <td>{{ $staff->attended_rtti }}</td>
                                        @can('isSchoolManager')
                                        <td class="action-btn">
                                            {{--<a href="#" class="btn btn-sm btn-warning">Vacation</a>--}}
                                            <a href="{{ route('school.staff.edit', $staff->id) }}"
                                               class="btn btn-sm btn-info">Edit</a>

                                            <button type="button"
                                                    data-url="{{ route('school.staff.destroy', $staff->id) }}"
                                                    data-names="{{ $staff->names }}"
                                                    data-toggle="modal" data-target="#removeStaff"
                                                    class="btn btn-sm btn-danger btn-delete">Delete
                                            </button>
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal("show");
            });
        });
    </script>
@endsection