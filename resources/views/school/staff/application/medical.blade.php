@extends('school.staff.layout.main')

@section('htmlheader_title')
    Medical History
@endsection

@section('l-style')
    @parent
    <style>
        .datepicker {
            z-index: 999 !important;
        }
    </style>
@overwrite
@section('panel-title')
    <span>Medical History</span>
@endsection

@section('panel-body')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">Medical History</div>
            {!! Form::open() !!}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td class="bg-gray">Allergies</td>
                                <td colspan="3">
                                    <textarea name="allergies" rows="3"
                                              class="form-control">{{ getStaff('allergies','medical') }}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="bg-gray">Last time Hospitalized</td>
                                <td>
                                    <input type="text" class="form-control datepicker"
                                           value="{{ getStaff('last_date_hospitalized','medical') }}"
                                           name="last_date_hospitalized">
                                </td>
                                <td class="bg-gray">Last time sick and cause</td>
                                <td>
                                    <input type="text" class="form-control datepicker"
                                           value="{{ getStaff('last_time_sick','medical') }}"
                                           name="last_time_sick">
                                </td>
                            </tr>
                            <tr>
                                <td class="bg-gray">Medical History background</td>
                                <td colspan="3">
                                    <textarea name="medical_history_background" rows="3"
                                              class="form-control">{{ getStaff('medical_history_background','medical') }}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="bg-gray">Disability(ies)/Special Need(s)</td>
                                <td colspan="3">
                                    <textarea name="disabilities_or_special_needs" rows="3"
                                              class="form-control">{{ getStaff('disabilities_or_special_needs','medical') }}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="bg-gray">Is there any medical information<br>Diet prescribed by a doctor that
                                    the facilitator<br>trainer should know about ?
                                </td>
                                <td colspan="3">
                                    <textarea name="detailed_medical_information_or_dr_Instructions" cols="30" rows="10"
                                              class="form-control">{{ getStaff('detailed_medical_information_or_dr_Instructions','medical') }} </textarea>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-primary btn-md">Save Changes</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script>

    </script>
@endsection