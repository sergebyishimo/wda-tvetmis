@extends('school.staff.layout.main')

@section('htmlheader_title')
    Assessor Application
@endsection

@section('l-style')
    @parent
@overwrite
@section('panel-title')
    <span>Assessor Application</span>
@endsection

@section('panel-body')
    <div class="alert alert-danger">
        <b>
            <p>Before you submit an application, please make sure that you have filled or updated in the following
                sections
                (Where Applicable)</p>
            <ul>
                <li>My Information</li>
                <li>Work Experience</li>
                <li>Teaching Experience</li>
                <li>Marking Background</li>
                <li>Assessor Background</li>
            </ul>
        </b>
    </div>
    @if($currentAssessor === false)
        @if(getMarkingActivation('assessor') == '1')
            {!! Form::open(['route' => 'school.teach.store.assessor.application']) !!}
            {!! Form::hidden('school_id', getStaff('school_id')) !!}
            {!! Form::hidden('staff_id', getStaff('id')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Form::label('academic_year', "Academic Year", ['class' => 'label-control']) !!}
                    {!! Form::text('academic_year', date('Y'), ['class' => 'form-control', 'readonly' => true]) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('trade', "Trade to be assessor", ['class' => 'label-control']) !!}
                        {!! Form::select('trade', getSelectTradeMarker(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('trade_assessed_before', "Trade assessor before", ['class' => 'label-control']) !!}
                        {!! Form::select('trade_assessed_before', getYesNo(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {!! Form::submit('Submit', ['class' => 'btn btn-block btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        @else
            <div class="alert alert-info">
                <p><b>System not active, please wait..</b></p>
            </div>
        @endif
    @else
        <div class="row">
            <div class="col-md-12">
                <h3>Your Application For Assessor</h3>
            </div>
            <div class="col-md-12">
                <div>Your Names: <b>{{ getStaff('names') }}</b></div>
                <div>Academic Year: <b>{{ $currentAssessor->academic_year }}</b></div>
                <div>Trade applied for: <b>{{ getSelectTradeMarker($currentAssessor->trade) }}</b></div>
                <div>Assessed Before: <b>{{ getYesNo($currentAssessor->trade_assessed_before) }}</b></div>
                <div class="clearfix">&nbsp;</div>
                <p class="alert alert-success">
                    You application has been received successfully. WDA will contact you with further information after
                    selection. <br>Should you wish to submit an edited application, <br>Click on Cancel Application when
                    you want to edit and then
                    Resubmit, otherwise you can logout.
                </p>
            </div>
            @if(getMarkingActivation('assessor') == '1')
                <div class="col-md-12">
                    {!! Form::open(['id' => 'cancelForm', 'route' => 'school.staff.cancel.application']) !!}
                    {!! Form::hidden('school_id', getStaff('school_id')) !!}
                    {!! Form::hidden('staff_id', getStaff('id')) !!}
                    {!! Form::hidden('academic_year', $currentAssessor->academic_year) !!}
                    {!! Form::hidden('where', 'a') !!}
                    <button type="button" class="btn btn-warning pull-right cancel-btn"
                            data-toggle="modal" data-target="#cancelApplication"
                    >Cancel This Application
                    </button>
                    {!! Form::close() !!}
                </div>
            @endif
        </div>
    @endif

@overwrite

@section('l-scripts')
    @parent
    <script>
        $(function () {
            $('#cancelApplication').on('show.bs.modal', function (e) {
                let modelDelete = $(this).find(".continue");
                console.log("ok");
                modelDelete.on("click", function () {
                    $("#cancelForm").submit();
                });
            });
        });
    </script>
@endsection