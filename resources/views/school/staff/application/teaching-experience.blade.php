@extends('school.staff.layout.main')

@section('htmlheader_title')
    My teaching Experience
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('panel-title')
    <span>My teaching Experience</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <div class="box-title">Add New Teaching Experience</div>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'school.staff.post.teaching', 'files' => false]) !!}
            {!! Form::hidden('school_id', getStaff('school_id')) !!}
            {!! Form::hidden('staff_id', getStaff('id')) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('institution_taught', "Institution Taught", ['class' => 'label-control']) !!}
                        {!! Form::text('institution_taught', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('subject_taught', "Subject Taught", ['class' => 'label-control']) !!}
                        {!! Form::text('subject_taught', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('tvet_sub_field', "Option / Trade", ['class' => 'label-control']) !!}
                        {!! Form::select('tvet_sub_field', getSelectTradeMarker(), null,['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('class_or_level_taught', "Level Taught", ['class' => 'label-control']) !!}
                        {!! Form::select('class_or_level_taught', getLevelTaught(), null,['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('from_date', "From Date", ['class' => 'label-control']) !!}
                        {!! Form::text('from_date', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('to_date', "To Date", ['class' => 'label-control']) !!}
                        {!! Form::text('to_date', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-2">
                    {!! Form::label("") !!}
                    {!! Form::submit("Submit", ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="dataTable">
                        <thead>
                        <th>#</th>
                        <th>Institution Taught</th>
                        <th>Subject Taught</th>
                        <th>Option / Trade</th>
                        <th>Level Taught</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @if($backgrounds->count() > 0)
                            @php($x=1)
                            @foreach($backgrounds as $background)
                                <tr>
                                    <td width="30px">{{ $x++ }}</td>
                                    <td>{{ $background->institution_taught }}</td>
                                    <td>{{ $background->subject_taught }}</td>
                                    <td>{{ getSelectTradeMarker($background->tvet_sub_field) }}</td>
                                    <td>{{ getLevelTaught($background->class_or_level_taught) }}</td>
                                    <td>{{ $background->from_date }}</td>
                                    <td>{{ $background->to_date }}</td>
                                    <td>
                                        <button type="button"
                                                data-url="{{ route('school.staff.background.destroy', [$background->id, 'te']) }}"
                                                data-names="{{  $background->institution_taught }}"
                                                data-toggle="modal" data-target="#removeStaff"
                                                class="btn btn-danger btn-sm btn-delete">Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                format: "yyyy-mm-dd",
                autoclose: true
            });
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
@endsection