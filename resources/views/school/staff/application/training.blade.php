@extends('school.staff.layout.main')

@section('htmlheader_title')
    Training Reference
@endsection

@section('l-style')
    @parent
@overwrite
@section('panel-title')
    <span>Training Reference</span>
@endsection

@section('panel-body')
    {!! Form::open([]) !!}
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr class="bg-gray">
                    <td colspan="2">Training Reference</td>
                </tr>
                @forelse($traings as $traing)
                    <tr>
                        <td class="bg-gray" width="500px">{{ $traing->training_reference }}</td>
                        <td>
                            <div class="form-group">
                                <textarea rows="2" name="response[{{ $traing->id }}]" class="form-control"
                                          required>{{ getStaffTrainingReference(getStaff('id'), $traing->id) }}</textarea>
                            </div>
                        </td>
                    </tr>
                @empty
                @endforelse
            </table>
        </div>
        <div class="col-md-2">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
    {!! Form::close() !!}
@overwrite

@section('l-scripts')
    @parent
    <script>

    </script>
@endsection