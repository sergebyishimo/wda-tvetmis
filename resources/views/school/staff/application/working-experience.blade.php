@extends('school.staff.layout.main')

@section('htmlheader_title')
    Working Experience
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('panel-title')
    <span>Working Experience</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <div class="box-title">Add New Working Experience</div>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'school.staff.post.experience', 'files' => true]) !!}
            {!! Form::hidden('school_id', getStaff('school_id')) !!}
            {!! Form::hidden('staff_id', getStaff('id')) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('title', "Title", ['class' => 'label-control']) !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('institution', "Institution", ['class' => 'label-control']) !!}
                        {!! Form::text('institution', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('status', "Status", ['class' => 'label-control']) !!}
                        {!! Form::select('status', getStatusWorkExperience(), null,['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('period_from', "Period From", ['class' => 'label-control']) !!}
                        {!! Form::text('period_from', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('period_to', "Period To", ['class' => 'label-control']) !!}
                        {!! Form::text('period_to', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('responsibility', "Responsibility", ['class' => 'label-control']) !!}
                        {!! Form::text('responsibility', null,['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label("") !!}
                        {!! Form::submit("Submit", ['class' => 'btn btn-primary btn-block']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="dataTable">
                        <thead>
                        <th>#</th>
                        <th>Title</th>
                        <th>Institution</th>
                        <th>Status</th>
                        <th>Period From</th>
                        <th>Period To</th>
                        <th>Responsibility</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @if($backgrounds->count() > 0)
                            @php($x=1)
                            @foreach($backgrounds as $background)
                                <tr>
                                    <td width="30px">{{ $x++ }}</td>
                                    <td>{{ $background->title }}</td>
                                    <td>{{ $background->institution }}</td>
                                    <td>{{ getStatusWorkExperience($background->status) }}</td>
                                    <td>{{ $background->period_from }}</td>
                                    <td>{{ $background->period_to }}</td>
                                    <td>{{ $background->responsibility }}</td>
                                    <td>
                                        <button type="button"
                                                data-url="{{ route('school.staff.background.destroy', [$background->id, 'we']) }}"
                                                data-names="{{ $background->title }}"
                                                data-toggle="modal" data-target="#removeStaff"
                                                class="btn btn-danger btn-sm btn-delete">Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                format: "yyyy-mm-dd",
                autoclose: true

            });

            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal();
            });
        })
        ;
    </script>
@endsection