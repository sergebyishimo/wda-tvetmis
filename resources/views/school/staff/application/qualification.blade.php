@extends('school.staff.layout.main')

@section('htmlheader_title')
    Qualification
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite

@section('panel-title')
    <span>Qualification</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <div class="box-title">Add New Qualification</div>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'school.staff.qualification']) !!}
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('institution_of_study', "Awarding Institution") !!}
                        {!! Form::text('institution_of_study', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('qualification_name', "Program/Field of specialisation") !!}
                        {!! Form::text('qualification_name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('qualification_level', "Qualification Level") !!}
                        {!! Form::select('qualification_level', getStudyQualification(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('date_of_award', "Date of graduation") !!}
                        {!! Form::text('date_of_award', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    {!! Form::label("") !!}
                    {!! Form::submit("Submit", ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <th class="text-center">#</th>
                        <th>Program</th>
                        <th>Institution</th>
                        <th>Level</th>
                        <th>Graduation Year</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @if($qualifications)
                            @php($x=1)
                            @foreach($qualifications as $qualification)
                                <tr>
                                    <td width="30px">{{ $x++ }}</td>
                                    <td>{{ $qualification->institution_of_study }}</td>
                                    <td>{{ ucwords($qualification->qualification_name) }}</td>
                                    <td>{{ getStudyQualification($qualification->qualification_level) }}</td>
                                    <td>{{ $qualification->date_of_award }}</td>
                                    <td>
                                        <button type="button"
                                                data-url="{{ route('school.staff.background.destroy', [$qualification->id, 'qua']) }}"
                                                data-names="{{ $qualification->qualification_name }}"
                                                data-toggle="modal" data-target="#removeStaff"
                                                class="btn btn-danger btn-sm btn-delete">Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
@endsection