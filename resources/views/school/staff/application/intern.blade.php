@extends('school.staff.layout.main')

@section('htmlheader_title')
    Internship Program
@endsection

@section('l-style')
    @parent
@overwrite
@section('panel-title')
    <span>Internship Program</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <div class="box-title">Add New Qualification</div>
        </div>
        <div class="box-body">
            {!! Form::open() !!}
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('company_name', "Company Name") !!}
                        {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('from_date', "Date From") !!}
                        {!! Form::text('from_date', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('to_date', "Date To") !!}
                        {!! Form::text('to_date', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('days', "Number of Days") !!}
                        {!! Form::number('days', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('place', "Place") !!}
                        {!! Form::text('place', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    {!! Form::label("&nbsp;") !!}
                    {!! Form::submit("Submit", ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <th class="text-center">#</th>
                        <th>Company Name</th>
                        <th>Date From</th>
                        <th>Date To</th>
                        <th>Number of Days</th>
                        <th>Place</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @if($interns)
                            @php($x=1)
                            @foreach($interns as $intern)
                                <tr>
                                    <td width="30px">{{ $x++ }}</td>
                                    <td>{{ $intern->company_name }}</td>
                                    <td>{{ $intern->from_date }}</td>
                                    <td>{{ $intern->to_date }}</td>
                                    <td>{{ $intern->days }}</td>
                                    <td>{{ $intern->place }}</td>
                                    <td>
                                        <button type="button"
                                                data-url="{{ route('school.staff.background.destroy', [$intern->id, 'inter']) }}"
                                                data-names="{{ $intern->company_name }}"
                                                data-toggle="modal" data-target="#removeStaff"
                                                class="btn btn-danger btn-sm btn-delete">Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
@endsection