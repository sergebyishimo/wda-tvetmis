@extends('school.staff.layout.main')

@section('htmlheader_title')
    Family Background
@endsection

@section('l-style')
    @parent
@overwrite
@section('panel-title')
    <span>Family Background</span>
@endsection

@section('panel-body')
    {!! Form::open(['route' =>  'school.staff.family.info', 'class' => "form-horizontal"]) !!}
    @if(strtolower(getStaff('civil_status')) != 'single')
        <div class="form-group">
            {!! Form::label('spouse_names', "Spouse's Name", ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('spouse_names', getStaff('spouse_names', 'detail'), ['class' => "form-control"]) !!}

                @if ($errors->has('spouse_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('spouse_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('number_of_children', "Number of Children", ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('number_of_children', getStaff('number_of_children', 'detail'), ['class' => "form-control"]) !!}

                @if ($errors->has('number_of_children'))
                    <span class="help-block">
                    <strong>{{ $errors->first('number_of_children') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('age_range_children', "Age of Children", ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                <div class="row" style="">
                    <div class="col-md-2">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="age_range_children[]"
                                       {{ getChildrenRage(getStaff('id'), "0-5") == "0-5" ? "checked" : "" }}
                                       value="0-5"> 0-5
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="age_range_children[]"
                                       {{ getChildrenRage(getStaff('id'), "6-10") == "6-10" ? "checked" : "" }}
                                       value="6-10"> 6-10
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="age_range_children[]"
                                       {{ getChildrenRage(getStaff('id'), "11-15") == "11-15" ? "checked" : "" }}
                                       value="11-15"> 11-15
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="age_range_children[]"
                                       {{ getChildrenRage(getStaff('id'), "16-20") == "16-20" ? "checked" : "" }}
                                       value="16-20"> 16-20
                            </label>
                        </div>
                    </div>
                </div>
                @if ($errors->has('age_range_children'))
                    <span class="help-block">
                    <strong>{{ $errors->first('age_range_children') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('whom_you_stay_with', "Who is looking after them ?", ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('whom_you_stay_with', getStaff('whom_you_stay_with', 'detail'), ['class' => "form-control"]) !!}

                @if ($errors->has('whom_you_stay_with'))
                    <span class="help-block">
                    <strong>{{ $errors->first('whom_you_stay_with') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <hr>
    @endif
    <div class="form-group">
        {!! Form::label('fathers_names', "Father's Name", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('fathers_names', getStaff('fathers_names', 'detail'), ['class' => "form-control"]) !!}

            @if ($errors->has('fathers_names'))
                <span class="help-block">
                    <strong>{{ $errors->first('fathers_names') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('mothers_names', "Mother's Name", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('mothers_names', getStaff('mothers_names', 'detail'), ['class' => "form-control"]) !!}

            @if ($errors->has('mothers_names'))
                <span class="help-block">
                    <strong>{{ $errors->first('mothers_names') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('family_province', "Family Province", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('family_province', getStaff('family_province', 'detail'), ['class' => "form-control"]) !!}

            @if ($errors->has('family_province'))
                <span class="help-block">
                    <strong>{{ $errors->first('family_province') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('family_district', "Family District", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('family_district', getStaff('family_district', 'detail'), ['class' => "form-control"]) !!}

            @if ($errors->has('family_district'))
                <span class="help-block">
                    <strong>{{ $errors->first('family_district') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('family_sector', "Family Sector", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('family_sector', getStaff('family_sector', 'detail'), ['class' => "form-control"]) !!}

            @if ($errors->has('family_sector'))
                <span class="help-block">
                    <strong>{{ $errors->first('family_sector') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('number_brothers', "Number brothers", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::number('number_brothers', getStaff('number_brothers', 'detail'), ['class' => "form-control"]) !!}

            @if ($errors->has('number_brothers'))
                <span class="help-block">
                    <strong>{{ $errors->first('number_brothers') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('number_sisters', "Number Sisters", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::number('number_sisters', getStaff('number_sisters', 'detail'), ['class' => "form-control"]) !!}

            @if ($errors->has('number_sisters'))
                <span class="help-block">
                    <strong>{{ $errors->first('number_sisters') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('about_family', "About Family", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::textarea('about_family', getStaff('about_family', 'detail'), ['rows' => '3', 'class' => "form-control"]) !!}

            @if ($errors->has('about_family'))
                <span class="help-block">
                    <strong>{{ $errors->first('about_family') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('your_family_responsibilities', "Your Family Responsibilities", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::textarea('your_family_responsibilities', getStaff('your_family_responsibilities', 'detail'),
            [ 'class' => "form-control",
              'rows' => '3',
              "placeholder" => "E.g caring for my own child(ren), Looking after brothers/sisters, Caring for mother"]) !!}

            @if ($errors->has('your_family_responsibilities'))
                <span class="help-block">
                    <strong>{{ $errors->first('your_family_responsibilities') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-2"></div>
        <div class="col-md-2">
            {{ Form::submit('Save', ['class' => 'btn btn-primary btn-block btn-md']) }}
        </div>
    </div>

    {!! Form::close() !!}
@overwrite

@section('l-scripts')
    @parent

@endsection