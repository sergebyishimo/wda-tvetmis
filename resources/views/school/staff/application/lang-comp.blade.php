@extends('school.staff.layout.main')

@section('htmlheader_title')
    Language & Computer
@endsection

@section('l-style')
    @parent
@overwrite

@section('panel-title')
    <span>Language & Computer</span>
@endsection

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">Languages</div>
                {!! Form::open() !!}
                {!! Form::hidden('type', 'lang') !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr class="bg-gray">
                                    <td>Language</td>
                                    <td>Reading</td>
                                    <td>Speaking</td>
                                    <td>Writing</td>
                                    {{--<td>Listening</td>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="bg-gray-light">English</td>
                                    <td>
                                        {{ Form::select('english_r', getLangRating(), getStaff("english_r", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('english_s', getLangRating(), getStaff("english_s", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('english_w', getLangRating(), getStaff("english_w", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-gray-light">French</td>
                                    <td>
                                        {{ Form::select('french_r', getLangRating(), getStaff("french_r", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('french_s', getLangRating(), getStaff("french_r", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('french_w', getLangRating(), getStaff("french_w", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-gray-light">Kinyarwanda</td>
                                    <td>
                                        {{ Form::select('kinyarwanda_r', getLangRating(), getStaff("kinyarwanda_r", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('kinyarwanda_s', getLangRating(), getStaff("kinyarwanda_s", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('kinyarwanda_w', getLangRating(), getStaff("kinyarwanda_w", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-gray-light">Swahili</td>
                                    <td>
                                        {{ Form::select('swahili_r', getLangRating(), getStaff("swahili_r", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('swahili_s', getLangRating(), getStaff("swahili_s", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('swahili_w', getLangRating(), getStaff("swahili_w", "languages"), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Language Spoken at home</td>
                                    <td>{{ Form::select('language_spoken', getLanguage(), getStaff("language_spoken", "detail"), ['class' => 'form-control select2']) }}</td>
                                </tr>
                                <tr>
                                    <td>Preferred Language of communication</td>
                                    <td>{{ Form::select('language_prefered', getLanguage(), getStaff("language_prefered", "detail"), ['class' => 'form-control select2']) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button class="btn btn-primary btn-md">Save Changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">Computer Skills</div>
                {!! Form::open() !!}
                {!! Form::hidden('type', 'comp') !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr class="bg-gray">
                                    <td>Computer Skills</td>
                                    <td>Skills</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="bg-gray-light">Internet</td>
                                    <td>
                                        {{ Form::select('internet', getLangRating(), getStaff('internet', 'computer'), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-gray-light">Ms Word</td>
                                    <td>
                                        {{ Form::select('word', getLangRating(), getStaff('word', 'computer'), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-gray-light">Ms Excel</td>
                                    <td>
                                        {{ Form::select('excel', getLangRating(), getStaff('excel', 'computer'), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bg-gray-light">Ms Powerpoint</td>
                                    <td>
                                        {{ Form::select('powerpoint', getLangRating(), getStaff('powerpoint', 'computer'), ['class' => 'form-control select2']) }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button class="btn btn-primary btn-md">Save Changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script>

    </script>
@endsection