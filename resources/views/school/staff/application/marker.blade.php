@extends('school.staff.layout.main')

@section('htmlheader_title')
    Marker Application
@endsection

@section('l-style')
    @parent
@overwrite
@section('panel-title')
    <span>Marker Application</span>
@endsection

@section('panel-body')
    <div class="alert alert-danger">
        <b>
            <p>Before you submit an application, please make sure that you have filled or updated in the following
                sections
                (Where Applicable)</p>
            <ul>
                <li>My Information</li>
                <li>Work Experience</li>
                <li>Teaching Experience</li>
                <li>Marking Background</li>
                <li>Assessor Background</li>
            </ul>
        </b>
    </div>
    @if(getStaff('currentMarking') === false)
        @if(getMarkingActivation('marking') == '1')
            {!! Form::open(['route' => 'school.teach.store.marker.application']) !!}
            {!! Form::hidden('school_id', getStaff('school_id')) !!}
            {!! Form::hidden('staff_id', getStaff('id')) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Form::label('academic_year', "Academic Year", ['class' => 'label-control']) !!}
                    {!! Form::text('academic_year', date('Y'), ['class' => 'form-control', 'readonly' => true]) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('education_program_id', "Trade to be Marked", ['class' => 'label-control']) !!}
                        {!! Form::select('education_program_id', getSelectTradeMarker(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('trade_marked_before', "Trade to be Marked", ['class' => 'label-control']) !!}
                        {!! Form::select('trade_marked_before', getYesNo(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('first_subject_applied_for', "First Subject Applied For", ['class' => 'label-control']) !!}
                        {!! Form::text('first_subject_applied_for', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('first_subject_marked_before', "First Subject Marked Before", ['class' => 'label-control']) !!}
                        {!! Form::select('first_subject_marked_before', getYesNo(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('second_subject_applied_for', "Second Subject Applied For", ['class' => 'label-control']) !!}
                        {!! Form::text('second_subject_applied_for', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('second_subject_marked_before', "Second Subject Marked Before", ['class' => 'label-control']) !!}
                        {!! Form::select('second_subject_marked_before', getYesNo(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('third_subject_applied_for', "Third Subject Applied For", ['class' => 'label-control']) !!}
                        {!! Form::text('third_subject_applied_for', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('third_subject_marked_before', "Third Subject Marked Before", ['class' => 'label-control']) !!}
                        {!! Form::select('third_subject_marked_before', getYesNo(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {!! Form::submit('Submit', ['class' => 'btn btn-block btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        @else
            <div class="alert alert-info">
                <p><b>System not active, please wait..</b></p>
            </div>
        @endif
    @else
        <div class="row">
            <div class="col-md-12">
                <h3>Your Application For Marking</h3>
            </div>
            <div class="col-md-12">
                <div>Academic Year: <b>{{ $currentMarker->academic_year }}</b></div>
                <div>Trade applied for: <b>{{ getSelectTradeMarker($currentMarker->education_program_id) }}</b></div>
                <div>Assessed Before: <b>{{ getYesNo($currentMarker->trade_marked_before) }}</b></div>
                <div>First Subject applied for: <b>{{ $currentMarker->first_subject_applied_for }}</b></div>
                <div>First Subject Assessed Before: <b>{{ getYesNo($currentMarker->first_subject_marked_before) }}</b>
                </div>
                @isset($currentMarker->second_subject_applied_for)
                    <div>Second Subject applied for: <b>{{ $currentMarker->second_subject_applied_for }}</b></div>
                    <div>Second Subject Assessed Before:
                        <b>{{ getYesNo($currentMarker->second_subject_marked_before) }}</b></div>
                @endisset
                @isset($currentMarker->third_subject_applied_for)
                    <div>Third Subject applied for: <b>{{ $currentMarker->third_subject_applied_for }}</b></div>
                    <div>Third Subject Assessed Before:
                        <b>{{ getYesNo($currentMarker->third_subject_marked_before) }}</b></div>
                @endisset
                <div class="clearfix">&nbsp;</div>
                <p class="alert alert-success">
                    You application has been received successfully. WDA will contact you with further information after
                    selection. <br>Should you wish to submit an edited application, <br>Click on Cancel Application when
                    you want to edit and then
                    Resubmit, otherwise you can logout.
                </p>
            </div>
            @if(getMarkingActivation('marking') == '1')
                <div class="col-md-12">
                    {!! Form::open(['id' => 'cancelForm', 'route' => 'school.staff.cancel.application']) !!}
                    {!! Form::hidden('school_id', getStaff('school_id')) !!}
                    {!! Form::hidden('staff_id', getStaff('id')) !!}
                    {!! Form::hidden('academic_year', $currentMarker->academic_year) !!}
                    {!! Form::hidden('where', 'm') !!}
                    <button type="button" class="btn btn-warning pull-right" data-toggle="modal"
                            data-target="#cancelApplication">Cancel This Application
                    </button>
                    {!! Form::close() !!}
                </div>
            @endif
        </div>
    @endif
@overwrite

@section('l-scripts')
    @parent
    <script>
        $(function () {
            $('#cancelApplication').on('show.bs.modal', function (e) {
                let modelDelete = $(this).find(".continue");
                console.log("ok");
                modelDelete.on("click", function () {
                    $("#cancelForm").submit();
                });
            });
        });
    </script>
@endsection