@extends('school.staff.layout.main')

@section('htmlheader_title')
    My Assessor Background
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('panel-title')
    <span>My Assessor Background</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <div class="box-title">Add New Assessor Background</div>
        </div>
        <div class="box-body">
            {!! Form::open(['route' => 'school.staff.post.background.assessor', 'files' => true]) !!}
            {!! Form::hidden('school_id', getStaff('school_id')) !!}
            {!! Form::hidden('staff_id', getStaff('id')) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('academic_year', "Academic Year", ['class' => 'label-control']) !!}
                        {!! Form::select('academic_year', academicYear(2008), null, ['class' => 'form-control select2', 'readonly' => false]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('education_program_assessed', "Program Assessed", ['class' => 'label-control']) !!}
                        {!! Form::select('education_program_assessed', getSelectTradeMarker(), null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('assessement_center', "	Assessment Center", ['class' => 'label-control']) !!}
                        {!! Form::text('assessement_center', null,['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {!! Form::label("") !!}
                    {!! Form::submit("Submit", ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="dataTable">
                        <thead>
                        <th width="30px">#</th>
                        <th>Academic Year</th>
                        <th>Program Assessed</th>
                        <th>Assessement Center</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @if($backgrounds->count() > 0)
                            @php($x=1)
                            @foreach($backgrounds as $background)
                                <tr>
                                    <td width="30px">{{ $x++ }}</td>
                                    <td>{{ $background->academic_year }}</td>
                                    <td>{{ getSelectTradeMarker($background->education_program_assessed) }}</td>
                                    <td>{{ ucwords($background->assessement_center) }}</td>
                                    <th>
                                        <button type="button"
                                                data-url="{{ route('school.staff.background.destroy', [$background->id, 'ab']) }}"
                                                data-names="{{ getSelectTradeMarker($background->education_program_assessed) }}"
                                                data-toggle="modal" data-target="#removeStaff"
                                                class="btn btn-danger btn-sm btn-delete">Delete
                                        </button>
                                    </th>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
@endsection