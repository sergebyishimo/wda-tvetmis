@extends('adminlte::layouts.app')

@section('htmlheader_title')
    More on {!! $staff->names !!}
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap.min.css"/>
    {{--<link rel="stylesheet"--}}
    {{--href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>--}}
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/colreorder/1.5.1/css/colReorder.dataTables.min.css" rel="stylesheet"/>

    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('contentheader_title')
    <div class="container-fluid">
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body" style="overflow-x: auto !important;">
                @if(!isset($_GET['profile']))<a href="{{ route('school.staff.update.myinfo') }}"
                                                class="btn btn-primary  {{ activeMenu('school.staff.index') }}">Edit
                    Profile</a>@endif
                <a href="javascript:window.print();" class="btn btn-primary  pull-right">print</a>
                <h2 style="text-align: center;"> Profile For <span class="text-primary">{{ $staff->names }}</span></h2>
                <div class="row">
                    <div class="col-md-3">
                        @php
                            $frm = 'nom';
                        @endphp
                        <img src="{{ getStaffPhoto($staff->photo) }}" alt=""
                             class="img-responsive img-rounded" style="width: 150px;">
                    </div>
                    <div class="col-md-9">
                        <table class="table table-striped">
                            <tr>
                                <td>Names</td>
                                <td><b>{{ $staff->names  }}</b></td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td><b>{{$staff->gender}}</b></td>
                            </tr>
                            <tr>
                                <td>Marital/Civil Status</td>
                                <td><b>{{ $staff->national_id_number  }}</b></td>
                            </tr>
                            <tr>
                                <td>Date of Birth</td>
                                <td><b></b></td>
                            </tr>
                            <tr>
                                <td>Phone Number</td>
                                <td><b>{{ $staff->phone_number }}</b></td>
                            </tr>
                            <tr>
                                <td>Email- Address</td>
                                <td><b>{{ $staff->email }}</b></td>
                            </tr>
                            <tr>
                                <td>ID No or Passport</td>
                                <td><b>{{ $staff->national_id_number }}</b></td>
                            </tr>
                            <tr>
                                <td>Nationality/Citizenship</td>
                                <td><b>{{ $staff->nationality }}</b></td>
                            </tr>
                            <tr>
                                <td>Have You Attended TVET Training?</td>
                                <td><b>{{ $staff->attended_rtti }}</b></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="box box-primary">
                                    <div class="box-header bg-aqua-active text-bold">School /Institution Profile</div>
                                    <div class="box-body">
                                        <table class="table table-striped" id="dataTable">
                                            <tr>
                                                <th>Name</th>
                                                <td class="col-md-8">{{ $school->school_name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Province</th>
                                                <td>{{ $school->province }}</td>
                                            </tr>
                                            <tr>
                                                <th>District</th>
                                                <td>{{ $school->district }}</td>
                                            </tr>
                                            <tr>
                                                <th>Sector</th>
                                                <td> {{ $school->sector }}</td>
                                            </tr>
                                            <tr>
                                                <th>Level</th>
                                                <td>{{$school->schooltype->school_type or ""}}</td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>{{ $school->school_status or "" }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box box-primary">
                                    <div class="box-header bg-aqua-active text-bold">School Manager/Principal</div>
                                    <div class="box-body">
                                        <table class="table table-striped" id="dataTable">
                                            <tr>
                                                <th>Name</th>
                                                <td class="col-md-8">{{ $school->manager_name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Phone</th>
                                                <td>{{ $school->manager_phone }}</td>
                                            </tr>
                                            <tr>
                                                <th>Mobile Phone No.</th>
                                                <td>{{ $school->manager_phone }}</td>
                                            </tr>
                                            <tr>
                                                <th>Email- Address</th>
                                                <td>{{ $school->manager_email or "" }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Home Profile</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr class="c11">
                                        <td class="c156 c140" colspan="2" rowspan="1"><p class="c21"><span class="c19">For Married Trainee</span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c171 c140" colspan="1" rowspan="1"><p><span
                                                        class="c19">Spouses&rsquo; Name</span></p>
                                        </td>
                                        <td class="c162" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{$staff->detail->spouse_names or ""}}</span></p>
                                        </td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c171 c140" colspan="1" rowspan="1"><p><span class="c19">Number of Children</span>
                                            </p></td>
                                        <td class="c162" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{$staff->detail->number_of_children or ""}}</span>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td class="c94" colspan="1" rowspan="1"><p><span
                                                        class="c19">Age of Children</span></p></td>
                                        <td class="c54" colspan="1" rowspan="1"><p class="c21">
                                                <span class="c3">&nbsp; &nbsp; &nbsp;</span></span><span
                                                        class="c3">&nbsp;{{ $staff->detail->age_range_children or "" }} </span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c94" colspan="1" rowspan="1"><p><span
                                                        class="c19">Who is looking after them?</span></p>
                                        </td>
                                        <td class="c54" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->detail->whom_you_stay_with or "" }}</span>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Home Profile</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr class="c11">
                                        <td class="c156 c140" colspan="2" rowspan="1"><p class="c21"><span class="c19">For Married Trainee</span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c171 c140" colspan="1" rowspan="1"><p><span
                                                        class="c19">Spouses&rsquo; Name</span></p>
                                        </td>
                                        <td class="c162" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{$staff->detail->spouse_names or ""}}</span></p>
                                        </td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c171 c140" colspan="1" rowspan="1"><p><span class="c19">Number of Children</span>
                                            </p></td>
                                        <td class="c162" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{$staff->detail->number_of_children or ""}}</span>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td class="c94" colspan="1" rowspan="1"><p><span
                                                        class="c19">Age of Children</span></p></td>
                                        <td class="c54" colspan="1" rowspan="1"><p class="c21">
                                                <span class="c3">&nbsp; &nbsp; &nbsp;</span></span><span
                                                        class="c3">&nbsp;{{ $staff->detail->age_range_children or "" }} </span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c94" colspan="1" rowspan="1"><p><span
                                                        class="c19">Who is looking after them?</span></p>
                                        </td>
                                        <td class="c54" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->detail->whom_you_stay_with or "" }}</span>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Trainee Living Conditions</div>
                            <div class="box-body">
                                <table class="c89">
                                    <tbody>
                                    <tr class="c11">
                                        <td class="c140 c181" colspan="1" rowspan="1"><p><span
                                                        class="c19">With whom do you live?</span></p>
                                            <p class="c1"><span class="c96"></span></p></td>
                                        <td class="c135" colspan="1" rowspan="1"><p class="c21"></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Family Background</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr class="c11">
                                        <td class="col-md-6" colspan="1" rowspan="1"><p><span class="c19">Father&rsquo;s Name</span>
                                            </p>
                                        </td>
                                        <td class="c103" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->detail->fathers_names or "" }}</span></p>
                                        </td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c28" colspan="1" rowspan="1"><p><span
                                                        class="c19">Mother&rsquo;s name</span></p>
                                        </td>
                                        <td class="c103" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->detail->mothers_names or "" }}</span></p>
                                        </td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c38" colspan="1" rowspan="1"><p><span
                                                        class="c19">House Location</span></p></td>
                                        <td class="c25" colspan="1" rowspan="1"><p class="c21"><span class="c3">&nbsp; &nbsp; &nbsp;Province: &nbsp;{{ $staff->detail->family_province or "" }}</span>
                                            </p>
                                            <p class="c21"><span
                                                        class="c3">&nbsp; &nbsp; &nbsp;District: {{ $staff->detail->family_district or "" }}</span>
                                            </p>
                                            <p class="c21"><span
                                                        class="c3">&nbsp; &nbsp; &nbsp;Sector: {{ $staff->detail->family_sector or "" }} </span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c38" colspan="1" rowspan="1"><p><span
                                                        class="c19">Number of Brother</span></p>
                                        </td>
                                        <td class="c25" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->detail->number_brothers or "" }}</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c38" colspan="1" rowspan="1"><p><span
                                                        class="c19">Number of Sister</span></p>
                                        </td>
                                        <td class="c25" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3"> {{ $staff->detail->number_sisters or "" }}</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c38" colspan="1" rowspan="1"><p><span class="c19">You &amp; Your Family (Tell Something About Your Family</span>
                                            </p></td>
                                        <td class="c25" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->detail->about_family or "" }} </span></p>
                                        </td>
                                    </tr>
                                    <tr class="c131">
                                        <td class="c38" colspan="1" rowspan="1"><p><span class="c176">Do you have family responsibilities? E.g. </span><span
                                                        class="c148">caring for my own child(ren), Looking after brothers/sisters, Caring for mother</span>
                                            </p>
                                        </td>
                                        <td class="c25" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->detail->your_family_responsibilities or "" }}</span>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Language Preference</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr class="bg-gray-light text-bold">
                                        <td class="c2" colspan="1" rowspan="1"><p class="c21"><span
                                                        class="c19">Language</span></p>
                                        </td>
                                        <td class="c2" colspan="1" rowspan="1"><p><span class="c19">Reading</span></p>
                                        </td>
                                        <td class="c2" colspan="1" rowspan="1"><p><span class="c19">Speaking</span></p>
                                        </td>
                                        <td class="c114 c85" colspan="1" rowspan="1"><p><span class="c19">Writing</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c77">
                                        <td class="c82" colspan="1" rowspan="1"><p><span class="c3">English</span></p>
                                        </td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->english_r) : "" }}</span>
                                            </p></td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{ $staff->languages ? getRating($staff->languages->english_s) : "" }}</span>
                                            </p></td>
                                        <td class="c114" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->english_w) : "" }}</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c77">
                                        <td class="c82" colspan="1" rowspan="1"><p><span class="c3">French</span></p>
                                        </td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{ $staff->languages ? getRating($staff->languages->french_r) : ""}}</span></p>
                                        </td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->french_s) : ""}}</span></p>
                                        </td>
                                        <td class="c114" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->french_w) : ""}}</span></p>
                                        </td>
                                    </tr>
                                    <tr class="c120">
                                        <td class="c82" colspan="1" rowspan="1"><p><span class="c3">Kinyarwanda</span>
                                            </p></td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->kinyarwanda_w) : ""}}</span>
                                            </p></td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->kinyarwanda_w) : ""}}</span>
                                            </p></td>
                                        <td class="c114" colspan="1" rowspan="1"><p class="c46 c34 c88"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->kinyarwanda_w) : ""}}</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c77">
                                        <td class="c82" colspan="1" rowspan="1"><p><span class="c3">Swahili</span></p>
                                        </td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->swahili_w) : ""}}</span>
                                            </p></td>
                                        <td class="c27" colspan="1" rowspan="1"><p class="c34 c46"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->swahili_w) : ""}}</span>
                                            </p></td>
                                        <td class="c114" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{{$staff->languages ? getRating($staff->languages->swahili_w) : ""}}</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c77">
                                        <td class="c165" colspan="5" rowspan="1"><p class="c46 c34"><span
                                                        class="c5"></span></p>
                                        </td>
                                    </tr>
                                    <tr class="c77">
                                        <td class="c129" colspan="2" rowspan="1"><p><span class="c3">Language Spoken at home</span>
                                            </p></td>
                                        <td class="c186" colspan="2" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{!! $staff->detail ? $staff->detail->language_spoken : "" !!}</span></p>
                                        </td>
                                        <td class="c32" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5"></span></p></td>
                                    </tr>
                                    <tr class="c77">
                                        <td class="c129" colspan="2" rowspan="1"><p><span
                                                        class="c3">Preferred Language of communication</span></p></td>
                                        <td class="c186" colspan="2" rowspan="1"><p class="c46 c34"><span
                                                        class="c5">{!! $staff->detail ? $staff->detail->language_prefered : "" !!}</span></p>
                                        </td>
                                        <td class="c32" colspan="1" rowspan="1"><p class="c46 c34"><span
                                                        class="c5"></span></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Teaching Experience</div>
                            <div class="box-body">
                                <table class="table" id="dataTable">
                                    <thead class="bg-gray-light text-bold">
                                    <th>#</th>
                                    <th>Institution Taught</th>
                                    <th>Subject Taught</th>
                                    <th>Option / Trade</th>
                                    <th>Level Taught</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    </thead>
                                    <tbody>
                                    @if($staff->teachingExperience->count() > 0)
                                        @php($x=1)
                                        @foreach($staff->teachingExperience as $background)
                                            <tr>
                                                <td width="30px">{{ $x++ }}</td>
                                                <td>{{ $background->institution_taught }}</td>
                                                <td>{{ $background->subject_taught }}</td>
                                                <td>{{ getSelectTradeMarker($background->tvet_sub_field) }}</td>
                                                <td>{{ getLevelTaught($background->class_or_level_taught) }}</td>
                                                <td>{{ $background->from_date }}</td>
                                                <td>{{ $background->to_date }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="">None</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Working Experience</div>
                            <div class="box-body">
                                <table class="table" id="dataTable">
                                    <thead class="bg-gray-light text-bold">
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Institution</th>
                                    <th>Status</th>
                                    <th>Period From</th>
                                    <th>Period To</th>
                                    <th>Responsibility</th>
                                    </thead>
                                    <tbody>
                                    @if($staff->workingExperience->count() > 0)
                                        @php($x=1)
                                        @foreach($staff->workingExperience as $background)
                                            <tr>
                                                <td width="30px">{{ $x++ }}</td>
                                                <td>{{ $background->title }}</td>
                                                <td>{{ $background->institution }}</td>
                                                <td>{{ getStatusWorkExperience($background->status) }}</td>
                                                <td>{{ $background->period_from }}</td>
                                                <td>{{ $background->period_to }}</td>
                                                <td>{{ $background->responsibility }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="">None</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box box-header bg-aqua-active text-bold">Internship Program</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr class="bg-gray-light text-bold">
                                        <td rowspan="1"><p><span>Company Name &amp; Address</span></p>
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <p><span>Date</span></p>
                                            <p><span>(From ...To...)</span></p></td>
                                        <td colspan="1" rowspan="1"><p><span>No. of Days</span>
                                            </p></td>
                                    </tr>
                                    @foreach($staff->internship as $intership)
                                        <tr>
                                            <td colspan="1" rowspan="1"><p><span>{{ $intership->company_name }}</span>
                                                </p></td>
                                            <td colspan="1" rowspan="1"><p><span class="text-sm">{{ $intership->from_date }}
                                                        to {{ $intership->to_date }} </span></p></td>
                                            <td colspan="1" rowspan="1"><p><span>{{ $intership->days }}</span></p></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Medical History</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr class="c11">
                                        <td class="c140 c189" colspan="2" rowspan="1"><p class="c40"><span>Medical History</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c60" colspan="1" rowspan="1"><p><span class="c30">Allergies</span>
                                            </p></td>
                                        <td class="c110" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->medical->allergies or "" }}</span></p>
                                        </td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c60" colspan="1" rowspan="1"><p><span class="c30">Last time Hospitalized</span>
                                            </p></td>
                                        <td class="c110" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{$staff->medical->last_date_hospitalized or ""}}</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c60" colspan="1" rowspan="1"><p><span class="c30">Last time sick and cause</span>
                                            </p>
                                        </td>
                                        <td class="c110" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{$staff->medical->last_time_sick or ""}}</span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="c60" colspan="1" rowspan="1"><p><span
                                                        class="c30">Medical History background</span></p>
                                        </td>
                                        <td class="c87" colspan="1" rowspan="1"><p class="c21"><span
                                                        class="c3">{{ $staff->medical->medical_history_background or "" }}</span>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td class="c60" colspan="1" rowspan="1"><p><span class="c30">Disability(ies) /Special Need(s)</span>
                                            </p></td>
                                        <td class="c87" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->medical->disabilities_or_special_needs or "" }}</span>
                                            </p></td>
                                    </tr>
                                    <tr class="c11">
                                        <td class="c60" colspan="1" rowspan="1"><p><span class="c30">Is there any medical information/Diet prescribed by a doctor that the facilitator / trainer should know about?</span>
                                            </p></td>
                                        <td class="c87" colspan="1" rowspan="1"><p class="c21 c34"><span
                                                        class="c3">{{ $staff->medical->detailed_medical_information_or_dr_Instructions or "" }}</span>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Computer Skills</div>
                            <div class="box-body">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td colspan="1" rowspan="1"><p class="c21"><span class="c19">Internet</span></p>
                                        </td>
                                        <td colspan="1" rowspan="1"><p><span class="c19">Word</span></p></td>
                                        <td colspan="1" rowspan="1"><p><span class="c19">Excel</span></p></td>
                                        <td colspan="1" rowspan="1"><p><span class="c19">Power Point</span></p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1"><p><span
                                                        class="c3">{{$staff->computer ? getRating($staff->computer->internet) : "" }}</span></p>
                                        </td>
                                        <td colspan="1" rowspan="1"><p><span
                                                        class="c3">{{$staff->computer ? getRating($staff->computer->word) : ""}}</span>
                                            </p></td>
                                        <td colspan="1" rowspan="1"><p><span
                                                        class="c3">{{$staff->computer ? getRating($staff->computer->excel) : ""}}</span></p>
                                        </td>
                                        <td colspan="1" rowspan="1"><p><span
                                                        class="c3">{{$staff->computer ? getRating($staff->computer->powerpoint) : ""}}</span>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Describe Your Self</div>
                            <div class="box-body">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td colspan="1" rowspan="1"><p><span>Describe yourself &ndash; the person you are / what people think about you / what you think about yourself</span>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1"><p class="c1"><span></span></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Qualification</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr class="bg-gray-light text-bold">
                                        <td><p><span>Name of Qualification</span>
                                            </p></td>
                                        <td><p><span>Program/Field of specialisation</span></p></td>
                                        <td><p><span>Awarding institution</span>
                                            </p>
                                        </td>
                                        <td><p><span>Date of graduation/completion</span></p></td>
                                    </tr>
                                    @foreach($staff->qualifications  as $qualification)
                                        <tr>
                                            <td><p><span
                                                    >{{ strtoupper($qualification->qualification_level) }}</span>
                                                </p></td>
                                            <td><p><span>{{ $qualification->qualification_name  }}</span></p></td>
                                            <td><p><span>{{ $qualification->institution_of_study  }}</span></p></td>
                                            <td><p><span>{{ $qualification->date_of_award  }}</span></p></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-header bg-aqua-active text-bold">Training Reference</div>
                            <div class="box-body">
                                <table class="table table-striped">
                                    <tbody>
                                    @foreach($staff->training  as $training)
                                        <tr>
                                            <td class="col-md-6"><p>{{ $training->source->training_reference or "" }}</p></td>
                                            <td><p><span>{{ $training->response }}</span></p></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if($frm == 'nom')
                            <div class="box box-primary">
                                <div class="box-header bg-aqua-active text-bold">Assessor Experience</div>
                                <div class="box-body">
                                    <table class="table" id="dataTable">
                                        <thead class="bg-gray-light text-bold">
                                        <th width="30px">#</th>
                                        <th>Academic Year</th>
                                        <th>Program Assessed</th>
                                        <th>Assessment Center</th>
                                        </thead>
                                        <tbody>
                                        @if($staff->assessorBackgrounds->count() > 0)
                                            @php($x=1)
                                            @foreach($staff->assessorBackgrounds as $background)
                                                <tr>
                                                    <td width="30px">{{ $x++ }}</td>
                                                    <td>{{ $background->academic_year }}</td>
                                                    <td>{{ getSelectTradeMarker($background->education_program_assessed) }}</td>
                                                    <td>{{ ucwords($background->assessement_center) }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="">None</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
@endsection