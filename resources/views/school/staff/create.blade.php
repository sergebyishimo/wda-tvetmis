@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Add Staff
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Add Staff
    </div>
@endsection

@section('l-style')
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-2">
                <div class="box">
                    <div class="box-body">
                        @include('school.staff.layout.topmenu')
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">

                    <div class="box-header">
                        <h5></h5>
                    </div>

                    @if($staff)
                        @php($route = route('school.staff.update', $staff->id))
                        @php($id="updateStaff")
                    @else
                        @php($route = route('school.staff.store'))
                        @php($id="createStaff")
                    @endif
                    <form action="{{ $route }}" method="POST" class="form" id="{{ $id }}" enctype="multipart/form-data">
                        <div class="box-body">
                            {{ csrf_field() }}
                            @include('feedback.feedback')
                            @if($staff)
                                @method("PATCH")
                                {!! Form::hidden('id', $staff->id) !!}
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" name="first_name" id="first_name" class="form-control"
                                               value="{{ $staff ? $staff->first_name : old('first_name') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="last_name">Other Names</label>
                                        <input type="text" name="last_name" id="last_name" class="form-control"
                                               value="{{ $staff ? $staff->last_name : old('last_name') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="privilege">Position</label>
                                        <select name="privilege" id="privilege" class="form-control select2">
                                            <option value="" selected disabled>Select Main Position</option>
                                            @if(\App\SchoolUserPrivilege::all()->count() > 0)
                                                @foreach(\App\SchoolUserPrivilege::orderBy('name', 'ASC')->get() as $item)
                                                    @if($item->id != '1' && $item->id != '2')
                                                        @php($selected = "")
                                                        @if($staff)
                                                            @if($staff->privilege == $item->id || old('privilege') == $item->id)
                                                                @php($selected = "selected")
                                                            @endif
                                                        @endif
                                                        <option value="{{ $item->id }}" {{ $selected }} >{{ $item->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <select name="gender" id="gender" class="form-control select2" required>
                                            <option value="" selected disabled>Select Gender</option>
                                            <option value="Male" {{ old('gender') == 'Male' ? "selected" : $staff ? $staff->gender == "Male" ? "selected" : '' :"" }}>
                                                Male
                                            </option>
                                            <option value="Female" {{ old('gender') == 'Female' ? "Female" : $staff ? $staff->gender == "Female" ? "selected" : '' : "" }}>
                                                Female
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" id="email"
                                               value="{{ $staff ? old('email')? : $staff->email : old('email') }}"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number</label>
                                        <input type="text" name="phone_number" id="phone_number"
                                               value="{{ $staff ? $staff->phone_number : old('phone_number') }}"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="attended_rtti">Attended TVET Training?</label>
                                        <select name="attended_rtti" class="form-control select2">
                                            <option name="Yes" {{ $staff ? ($staff->attended_rtti == "Yes" ? 'selected': "") : ""}}>
                                                Yes
                                            </option>
                                            <option name="No" {{ $staff ? ($staff->attended_rtti ==  "No"   ? 'selected': "") : "" }}>
                                                No
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="staff_category">Staff category</label>
                                        <select name="staff_category" class="form-control select2">
                                            <option value="Administrative" {{ $staff ? ($staff->staff_category == "Administrative" ? 'selected': "") : ""}}>
                                                Administrative
                                            </option>
                                            <option value="Academic" {{ $staff ? ($staff->staff_category ==  "Academic"   ? 'selected': "") : "" }}>
                                                Academic
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('image', "Photo") !!}
                                        {!! Form::file('image', ['class' => 'filer_input form-control']) !!}
                                        @if($staff)
                                            @if(getStaffPhoto($staff))
                                                <div class="img-responsive mt-2">
                                                    <img src="{{ getStaffPhoto($staff) }}" alt="{{ $staff->names }}"
                                                         class="img-rounded"
                                                         style="width: 9em; height: 9em;">
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <fieldset>
                                <legend align="left">Only For Teaching Staff</legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sector" class="control-label">Sector Taught</label>
                                            <select name="r_i" data-url="{{ url('school/get/trades') }}"
                                                    class="form-control select2"
                                                    style="width: 100%;"
                                                    id="sector">
                                                <option value="" disabled selected>Choose Here</option>
                                                @isset($sectors)
                                                    @foreach ($sectors as $sector)
                                                        <option value="{{ $sector->id }}"
                                                                @if( isset($curr_info) && $curr_info->subsector->sector->id == $sector->id) selected @endif >{{ $sector->tvet_field }}</option>
                                                    @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="trades" class="control-label">Sub Sector Taught</label>
                                            <select name="d_d" class="form-control select2"
                                                    data-url="{{ url('school/get/quafication') }}" id="trades"
                                                    style="width: 100%;">
                                                <option value="" disabled selected>Choose Here</option>
                                                @isset($sub_sectors)
                                                    @foreach ($sub_sectors as $subtrade)
                                                        <option value="{{ $subtrade->id }}"
                                                                @if( isset($curr_info) && $curr_info->sub_sector_id == $subtrade->id) selected @endif >{{  $subtrade->sub_field_name }}</option>
                                                    @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="qua" class="control-label">Qualification Taught</label>
                                            <select name="qualification_id" class="form-control select2" id="qua"
                                                    style="width: 100%;">
                                                <option value="" disabled selected>Choose Here</option>
                                                @isset($currs)
                                                    @foreach ($currs as $curr)
                                                        <option value="{{ $curr->uuid }}" {{ isset($mod) ? $curr->uuid == $mod->qualification_id ? 'selected' : '' : "" }} >
                                                            {{  $curr->qualification_title }}
                                                        </option>
                                                    @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="modules_taught">Module Taught </label>
                                            {{-- {!! Form::select('modules_taught[]', $courses, $staff ? $staff->modules()->get()->lists('module_id')->toArray() : null, ['class' => 'form-control select2', 'multiple'=> true]) !!} --}}
                                            {{-- <select name="" id="" class="form-control">
                                                @foreach ($courses as $course)
                                                    <option value="{{ $course->id }}" @if(isset($staff) && ) @endif >{{ $course->module_title }}</option>
                                                @endforeach
                                            </select> --}}
                                            @if($staff)
                                                <select name="modules_taught[]" id="" class="form-control select2"
                                                        multiple>
                                                    @if($courses)
                                                        @foreach ($courses as $course)
                                                            <option value="{{ $course->id }}"
                                                                    @if($staff)
                                                                    @foreach($staff->modules as $p)
                                                                    @if($course->id == $p->module_id)
                                                                    selected="selected"
                                                                    @endif
                                                                    @endforeach
                                                                    @endif
                                                            >{{ $course->module_title }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            @else
                                                <select name="modules_taught[]" id="" class="form-control select2"
                                                        multiple>
                                                    @if($courses)
                                                        @foreach ($courses as $course => $name)
                                                            <option value="{{ $course }}"
                                                                    @if($staff)
                                                                    @foreach($staff->modules as $p)
                                                                    @if($course == $p->module_id)
                                                                    selected="selected"
                                                                    @endif
                                                                    @endforeach
                                                                    @endif
                                                            >{{$name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="box-footer">
                            <div class="form-group">
                                <button type="submit"
                                        class="btn btn-primary pull-left">{{ $staff ? "Update" : "Save" }}</button>
                                <button type="reset" class="btn btn-warning pull-right">Rest</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@overwrite

@section('l-scripts')
    <script src="{{ asset('js/main.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".datepicker").datepicker({
                autoclose: true
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CreateStaffRequest', '#createStaff'); !!}
@endsection