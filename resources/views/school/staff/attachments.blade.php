@extends('school.staff.layout.main')

@section('htmlheader_title')
    My Attachment
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('panel-title')
    <span>My Attachment</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <div class="box-title">Add New Attachment</div>
        </div>
        <div class="box-body">
            @if($myAttachement)
                <div class="alert alert-danger">
                    <b>Please Verify that you have attached the right files by clicking on each attached link and making
                        sure that the correct file is attached.</b>
                </div>
            @endif
            {!! Form::open(['route' => 'school.staff.post.attachment', 'files' => true]) !!}
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('description', "Attachment Description") !!}
                        {!! Form::select('description', $attachementDesc, null,
                        ['class' => 'form-control select2', 'placeholder' => 'Select attachment ...']) !!}
                    </div>
                </div>
                <div class="col-md-5">
                    {!! Form::label('file', "Attachment") !!}
                    {!! Form::file('file') !!}
                </div>
                <div class="col-md-2">
                    {!! Form::label("") !!}
                    {!! Form::submit("Submit", ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="dataTable">
                        <thead>
                        <th>#</th>
                        <th>Attachment Description</th>
                        <th>Link</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @if($myAttachement)
                            @php($x=1)
                            @foreach($myAttachement as $attachement)
                                <tr>
                                    <td width="30px">{{ $x++ }}</td>
                                    <td>{{ ucwords($attachement->name) }}</td>
                                    <td><a href="{{ asset('storage/'.$attachement->attachment) }}" target="_blank"
                                           class="btn btn-link">Download</a></td>
                                    <td>
                                        <button type="button"
                                                data-url="{{ route('school.staff.background.destroy', [$attachement->id, 'atc']) }}"
                                                data-names="{{ $attachement->name }}"
                                                data-toggle="modal" data-target="#removeStaff"
                                                class="btn btn-danger btn-sm btn-delete">Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('school.staff.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                console.log(names);
                var modalDelete = $("#removeStaff");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".panel-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
@endsection