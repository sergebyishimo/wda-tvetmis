@if(Gate::check('isSchoolManager') || Gate::check('isTeachOrStaff'))
    <a href="{{ route('school.staff.index') }}"
   class="btn btn-success btn-sm {{ activeMenu('school.staff.index') }}">All Staff</a>
@endif
@can('isSchoolManager')
<a href="{{ route('school.staff.create') }}"
   class="btn btn-success btn-sm {{ activeMenu('school.staff.create') }}">Add Staff</a>
@endcan

