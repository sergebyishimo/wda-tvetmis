<div class="modal fade" id="removeStaff">
    <div class="modal-dialog">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Are Sure To Delete <b class="names"></b> ?</h4>
            </div>
            <form action=""
                  method="POST">
                @if(activeMenu('school.staff.index') != "")
                    @method("DELETE")
                @endif
                {{ csrf_field() }}
                <input type="hidden" class="id" name="id" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger pull-left">Yes, Continue</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="cancelApplication">
    <div class="modal-dialog">
        <div class="modal-content panel-warning">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Are Sure To Cancel this Application <b class="names"></b> ?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-warning pull-left continue">Yes, Continue</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade " tabindex="-1" role="dialog" id="notCompleted">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-warning">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Please, Complete Your Information !!</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Later</button>
                <a href="{{ route('school.staff.update.myinfo') }}" type="submit" class="btn btn-danger pull-left">Ok,
                    Continue</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bg-danger" id="checkAttachment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Attachment Checking</h4>
            </div>
            <div class="modal-body">
                <b>Please Verify that you have attached the right files by clicking on each attached link and making
                    sure that the correct file is attached.</b>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Later</button>
                <a href="{{ route('school.staff.update.myinfo') }}" type="submit" class="btn btn-danger pull-left">Ok,
                    Continue</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->