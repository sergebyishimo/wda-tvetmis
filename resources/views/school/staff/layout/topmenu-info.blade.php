<div class="list-group list-group-horizontal">
    <a href="{{ route('school.staff.profile') }}"
       class="list-group-item {{ activeMenu('school.staff.update.myinfo') }}">My Profile</a>

    <a href="{{ route('school.staff.get.teaching') }}"
       class="list-group-item {{ activeMenu('school.staff.get.teaching') }}">Teaching Experience</a>

    <a href="{{ route('school.staff.background.marking') }}"
       class="list-group-item {{ activeMenu('school.staff.background.marking') }}">Marking Background</a>

    <a href="{{ route('school.staff.background.assessor') }}"
       class="list-group-item {{ activeMenu('school.staff.background.assessor') }}">Assessor Background</a>

    <a href="{{ route('school.staff.get.attachment') }}"
       class="list-group-item {{ activeMenu('school.staff.get.attachment') }}">Attachment</a>

    <a href="{{ route('school.staff.family.info') }}"
       class="list-group-item {{ activeMenu(['school.staff.family.info']) }}">Family Background</a>

    <a href="{{ route('school.staff.qualification') }}"
       class="list-group-item {{ activeMenu(['school.staff.qualification']) }}">Qualifications</a>

    <a href="{{ route('school.staff.lang.pc') }}"
       class="list-group-item {{ activeMenu('school.staff.lang.pc') }}">Language & Computer</a>

    <a href="{{ route('school.staff.intern') }}"
       class="list-group-item {{ activeMenu('school.staff.intern') }}">Internship Program</a>

    <a href="{{ route('school.staff.medical') }}"
       class="list-group-item {{ activeMenu('school.staff.medical') }}">Medical History</a>

    <a href="{{ route('school.staff.training') }}"
       class="list-group-item {{ activeMenu('school.staff.training') }}">Training Reference</a>

</div>