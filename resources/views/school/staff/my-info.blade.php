@extends('school.staff.layout.main')

@section('l-style')
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }

        .flag-text {
            margin-left: 10px;
        }
    </style>
@endsection

@section('panel-title')
    <span>Staff Information</span>
@endsection

@section('panel-body')
    {!! Form::open(['route' => 'school.staff.update.submit.myinfo', 'method' => 'post', 'id' => 'myinfoForm', 'class' => 'form', 'files' => true]) !!}
    @if($staff)
        {!! Form::hidden('id', $staff->id) !!}
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="first_name">First Name *</label>
                <input type="text" name="first_name" id="first_name" class="form-control"
                       value="{{ $staff ? $staff->first_name : old('first_name') }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="last_name">Other Names *</label>
                <input type="text" name="last_name" id="last_name" class="form-control"
                       value="{{ $staff ? $staff->last_name : old('last_name') }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="gender">Gender *</label>
                <select name="gender" id="gender" class="form-control select2" required>
                    <option value="" selected disabled>Select Gender</option>
                    <option value="Male" {{ old('gender') == 'Male' ? "selected" : $staff ? $staff->gender == "Male" ? "selected" : '' :"" }}>
                        Male
                    </option>
                    <option value="Female" {{ old('gender') == 'Female' ? "Female" : $staff ? $staff->gender == "Female" ? "selected" : '' : "" }}>
                        Female
                    </option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="email">Email *</label>
                <input type="email" name="email" id="email" value="{{ $staff ? $staff->email : old('email') }}"
                       class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('civil_status', 'Civil Status') !!}
                {!! Form::select('civil_status', [
                    'Single' => 'Single',
                    'Married' => 'Married',
                    'Widowed' => 'Widowed',
                    'Separated' => 'Separated',
                    'Divorced' => 'Divorced'
                ], $staff->civil_status, [
                    'class' => 'form-control select2',
                    'placeholder' => 'Select civil status ...'
                ]) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('image', "Photo *") !!}
                {!! Form::file('image', ['class' => 'filer_input form-control']) !!}
                {!! Form::hidden('image_old', $staff->photo) !!}
                @if($staff)
                    @if(getStaffPhoto($staff))
                        <div class="img-responsive">
                            <img src="{{ getStaffPhoto($staff) }}" alt="{{ $staff->names }}"
                                 class="img-rounded"
                                 style="width: 9em; height: 9em;">
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('phone_number', "Telephone Number") !!}
                {!! Form::text('phone_number', $staff ? $staff->phone_number : null, [
                'class' =>  'form-control',
                ]) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('nationality', "Nationality *") !!}
                {!! Form::select('nationality', $countries , $staff ? $staff->nationality: null , ['class' => 'form-control select2']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('national_id_number', "National ID *") !!}
                {!! Form::text('national_id_number', $staff ? $staff->national_id_number : null, [
                'class' =>  'form-control',
                ]) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('Province', "Province *") !!}
                {{--{!! dd(getProvince(true)) !!}--}}
                {!! Form::select('Province', getProvince(true), $staff ? $staff->province : null, ['placeholder' => 'Pick Province ...',
                'class' => 'form-control p select2', 'required' => true]) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('District', "District *") !!}
                {!! Form::select('District', $staff->district ? [ $staff->district => $staff->district ] : [], $staff->district ? $staff->district : null, ['placeholder' => 'Pick District ...',
                'class' => 'form-control d select2', 'required' => true]) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('Sector', "Sector *") !!}
                {!! Form::select('Sector', $staff->sector ? [ $staff->sector => $staff->sector ] : [], $staff->sector ? $staff->sector : null, ['placeholder' => 'Pick Sector ...',
                'class' => 'form-control s select2', 'required' => true]) !!}
            </div>
        </div>
        {{--<div class="col-md-2">--}}
            {{--<div class="form-group">--}}
                {{--{!! Form::label('Cell', "Cell *") !!}--}}
                {{--{!! Form::select('Cell', $staff->cell ? [ $staff->cell => $staff->cell ] : [], $staff->cell ? $staff->cell : null, ['placeholder' => 'Pick Cell ...',--}}
                {{--'class' => 'form-control c select2', 'required' => true]) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-2">--}}
            {{--<div class="form-group">--}}
                {{--{!! Form::label('Village', "Village *") !!}--}}
                {{--{!! Form::select('Village', $staff->village ? [ $staff->village => $staff->village ] : [], $staff->village ? $staff->village : null, ['placeholder' => 'Pick Village ...',--}}
                {{--'class' => 'form-control v select2', 'required' => true]) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('bank_name', "Bank Name") !!}
                {!! Form::text("bank_name", $staff->bank_name, ['class' => 'form-control', 'placeholder' => 'Bank Name']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('bank_account_number', "Bank Account Number") !!}
                {!! Form::text("bank_account_number", $staff->bank_account_number, ['class' => 'form-control', 'placeholder' => 'Bank Account Number']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('rssb_number', "RSSB Number") !!}
                {!! Form::text("rssb_number", $staff->rssb_number, ['class' => 'form-control', 'placeholder' => 'RSSB Number']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('qualification', "Qualification Name") !!}
                {!! Form::text('qualification', $staff ? $staff->qualification : null, [
                'class' =>  'form-control',
                ]) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('institution', "Institution") !!}
                {!! Form::text('institution', $staff ? $staff->institution : null, [
                'class' =>  'form-control',
                ]) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('graduated_year', "Graduated Year") !!}
                {!! Form::number('graduated_year', $staff ? $staff->graduated_year : null, [
                'class' =>  'form-control',
                'placeholder' => 'YYYY'
                ]) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="checkbox icheck">
                {!! Form::label('attended_rtti', "Have You Attended RTTI Training?") !!}
                <div>
                    {!! Form::select('attended_rtti', ['Yes'=> 'Yes','No'=>'No'] , $staff ? $staff->attended_rtti: null, ['class' => 'form-control select2']) !!}
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary pull-left">{{ $staff ? "Update" : "Save" }}</button>
        <button type="reset" class="btn btn-warning pull-right">Rest</button>
    </div>
    {!! Form::close() !!}
@overwrite

@section('l-scripts')
    <script>
        $(document).ready(function () {

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UpdateMyInfoRequest', '#myinfoForm  '); !!}
@endsection
