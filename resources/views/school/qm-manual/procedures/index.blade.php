@extends('school.qm-manual.layout.main')

@section('panel-title')
    <span>Procedures</span>
@endsection

@section('panel-body')
    <div class="box box-default">
        @if($model)
            <div class="box-body">
                {!! $model->description !!}
            </div>
        @endif
    </div>
@endsection