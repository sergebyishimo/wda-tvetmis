@extends('school.qm-manual.layout.main')

@section('panel-title')
    <span>Timeline</span>
@endsection
@section('htmlheader_title', 'Timeline')
@section('panel-body')
    <div class="box">
        <div class="box-body">
            <table border="1" width="100%">
                <tr>
                    <td class="text-center bg-gray-light" width="200px" rowspan="2">Activities</td>
                    @foreach(getMonths('M') as $m => $month)
                        @php
                            $weeks = getWeeks($m, date('Y')) -1;
                            if($m == 2)
                                $weeks++;
                        @endphp
                        <td class="text-center" colspan="{{ $weeks }}">{{ $month }}</td>
                    @endforeach
                </tr>
                <tr>

                    @foreach(getMonths('M') as $m => $mm)
                        @php
                            $weeks = getWeeks($m, date('Y')) - 1;
                            if($m == 2)
                                $weeks++;
                        @endphp
                        @for($x = 1; $x <= $weeks; $x++ )
                            <td class="pl-1 pt-0 pb-0 pr-1">{{ $x }}</td>
                        @endfor
                    @endforeach
                </tr>
                @if($timelines->count() > 0)
                    @foreach($timelines as $timeline)
                        <tr>
                            <td width="300px" class="pl-1 pt-0 pb-0 pr-1">
                                <div>
                                    <small>{{ $timeline->activity }}</small>
                                </div>
                            </td>
                            @foreach(getMonths('M') as $m => $mm)
                                @php
                                    $period = getDateRange($timeline->date_from, $timeline->date_to);
                                    $period = iterator_to_array($period);
                                    $dates = [];
                                @endphp
                                @if(is_array($period))
                                    @foreach($period as $key => $value)
                                        @if($value->format('M') == $mm)
                                            @php
                                                $dates[] = $value->format('Y-m-d');
                                            @endphp
                                        @endif
                                    @endforeach
                                @endif
                                @php
                                    $weeks = getWeeks($m, date('Y')) - 1;
                                    if($m == 2)
                                        $weeks++;
                                @endphp
                                @for($x = 1; $x <= $weeks; $x++ )
                                    @php
                                        $ok = false;
                                        foreach ($dates as $date):
                                            $whichWeek = getWhichWeek($date);
                                            if ($whichWeek == $x)
                                                $ok = true;
                                        endforeach;
                                    @endphp
                                    <td class="{{ $ok ? 'bg-gray' : '' }}">&nbsp;</td>
                                @endfor
                            @endforeach
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $('table tr').each(function () {
                let $firstRow
                    , colspan = 0;
                $(this).find('td').each(function () {
                    if ($(this).hasClass('bg-gray')) {
                        // Save the first cell with class in $firstRow, remove the rest
                        colspan === 0 ? $firstRow = $(this) : $(this).remove();
                        // Count the number of cells
                        colspan++;
                    } else if (colspan > 0) {
                        // Assign the colspan and reset the counter
                        $firstRow.attr('colspan', colspan);
                        colspan = 0;
                    }
                });
                if (colspan > 0) {
                    $firstRow.attr('colspan', colspan);
                    colspan = 0;
                }
            });
        })
    </script>
@endsection