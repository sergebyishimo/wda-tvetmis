@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        @yield('panel-title')
    </div>
@endsection

@section('l-style')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @include('feedback.feedback')
            </div>
        </div>
        @yield('panel-body')
    </div>
@endsection

@section('l-scripts')

@endsection