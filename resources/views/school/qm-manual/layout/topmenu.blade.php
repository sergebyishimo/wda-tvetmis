<li class="dropdown {{ activeMenu([
        'school.accr.application', 'school.accr.application.view'
    ]) }}">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
       aria-expanded="true"
       class="btn btn-success btn-flat">
        QM Applications <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="{{ route('school.accr.application') }}"><a href="{{ route('school.accr.application') }}">Submit
                New</a></li>
        <li role="separator" class="divider"></li>
        <li class="{{ route('school.accr.application.view') }}"><a href="{{ route('school.accr.application.view') }}">View
                All</a></li>
    </ul>
</li>

<li class="dropdown {{ activeMenu([
        'school.accr.input', 'school.accr.process', 'school.accr.output',
        'school.myAssessment', 'school.view.myAssessment', 'school.print.view.myAssessment'
    ]) }}">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
       aria-expanded="false"
       class="btn btn-success btn-flat">
        QM Self Assessment <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="{{ activeMenu('school.accr.input') }}"><a href="{{ route('school.accr.input') }}">Section 1:
                Input</a></li>
        <li role="separator" class="divider"></li>
        <li class="{{ activeMenu('school.accr.process') }}"><a href="{{ route('school.accr.process') }}">Section 2:
                Process</a></li>
        <li role="separator" class="divider"></li>
        <li class="{{ activeMenu('school.accr.output') }}"><a href="{{ route('school.accr.output') }}">Section 3:
                Output</a></li>
        <li role="separator" class="divider"></li>
        <li class="{{ activeMenu('school.myAssessment') }}">
            <a href="{{ route('school.myAssessment') }}">View Assessments</a>
        </li>
    </ul>
</li>

<li class="dropdown {{ activeMenu([
        'school.procedures.index', 'school.accreditation-types.index', 'school.responsibilities.index',
        'school.timelines.index'
    ]) }}">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
       aria-expanded="false"
       class="btn btn-success btn-flat">
        QM Manual <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="{{ activeMenu('school.procedures.index') }}">
            <a href="{{ route('school.procedures.index') }}">Procedures</a>
        </li>
        <li role="separator" class="divider"></li>
        <li class="{{ activeMenu('school.accreditation-types.index') }}">
            <a href="{{ route('school.accreditation-types.index') }}">Accreditation Type</a>
        </li>
        <li role="separator" class="divider"></li>
        <li class="{{ activeMenu('school.responsibilities.index') }}">
            <a href="{{ route('school.responsibilities.index') }}">Responsibilities</a>
        </li>
        <li role="separator" class="divider"></li>
        <li class="{{ activeMenu('school.timelines.index') }}">
            <a href="{{ route('school.timelines.index') }}">Timelines</a>
        </li>
    </ul>
</li>