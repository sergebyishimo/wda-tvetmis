@extends('school.workshops.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Workshops Management</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th>School Name</th>
                <th>Workshop Name</th>
                <th>Sector</th>
                <th>Sub Sector</th>
                <th>Created At</th>
                <th></th>
                </thead>
                <tbody>
                @if($workshops->count() > 0)
                    @foreach($workshops as $workshop)
                        <tr>
                            <td>
                                {{ $workshop->school->school_name }}
                            </td>
                            <td>
                                {{ $workshop->workshop_name }}
                            </td>
                            <td>
                                {{ $workshop->wsector->sector_name or "" }}
                            </td>
                            <td>
                                <h5>
                                    {{ $workshop->wsubsector->sub_sector_name or "" }}
                                </h5>
                            </td>
                            <td>
                                {{ $workshop->created_at->diffForHumans() }}
                            </td>
                            <th>
                                <a href="{{ route('school.workshops.edit', $workshop->id) }}" class="btn btn-info btn-sm">Edit</a>
                                <button type="button" data-url="{{ route('school.workshops.destroy', $workshop->id) }}"
                                        data-names="{{ $workshop->id }}"
                                        class="btn btn-danger btn-sm btn-delete">Delete
                                </button>
                            </th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('school.workshops.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection