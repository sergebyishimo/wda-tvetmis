@extends('school.accr.layout.main')

@section('panel-title', "Accreditation Output")

@section('htmlheader_title', "Accreditation Output")

@section('panel-body')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Section 3 : Output</h3>
				</div>
				<div class="box-body">
					<form method="GET">
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Graduation Year</label>
							<div class="col-md-2">
								<select class="form-control flat" name="year">
									@foreach ($data_source_academic_year as $data1)
										<option {{ isset($cyear) ? $cyear == $data1->year ? 'selected' : '' : "" }} value="{{ $data1->year }}">{{ $data1->year }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-1">
								<button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-list"></i> View</button>
							</div>

							<div class="col-md-3">

								<button type="button" class="btn btn-success btn-flat btn-block" data-toggle="modal" data-target="#addGraduate"><i class="fa fa-plus"></i> Add Graduate</button>
							</div>

							{{--<div class="col-md-3">--}}
							{{--<a href="/files/Graduates Format.xlsx" class="btn btn-info btn-flat btn-block"><i class="fa fa-file-excel-o"></i> Download Format</a>--}}
							{{--</div>--}}
						</div>
					</form>

					<div class="modal fade" id="addGraduate">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title"> <i class="fa fa-plus"></i> Add Graduate</h4>
								</div>
								<form method="POST">
									{{ csrf_field() }}

									<div class="modal-body" style="padding: 30px;height: 450px;overflow-y: scroll;background: #f0f1f3">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Year</label>
													<select class="form-control flat" name="year" style="width: 100%">
														@foreach ($data_source_academic_year as $data1)
															<option value="{{ $data1->year }}">{{ $data1->year }}</option>
														@endforeach
													</select></div>
												<div class="form-group">
													<label>Names</label>
													<input type="text" class="form-control flat" name="names" placeholder="Names" required>
												</div>
												<div class="form-group">
													<label>Gender</label>
													<select class="form-control flat" name="gender" style="width: 100%">
														<option>Male</option>
														<option>Female</option>
													</select>
												</div>
												<div class="form-group">
													<label>Education Sub Sector</label>
													@php
														$trades = App\Model\Accr\TrainingTradesSource::all();
													@endphp
													<select class="form-control flat" name="trade" style="width: 100%">
														@foreach ($trades as $trade)
															<option value="{{ $trade->id }}">{{ $trade->sub_field_name }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label>RTQF Achieved</label>
													@php
														$rtqfs = App\Model\Accr\RtqfSource::all();
													@endphp
													<select class="form-control flat" name="rtqf_achieved" style="width: 100%">
														@foreach ($rtqfs as $rtqf)
															<option value="{{ $rtqf->id }}">{{ $rtqf->level_name }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label>Phone Number</label>
													<input type="text" class="form-control flat" name="phone_number" placeholder="Phone Number">
												</div>
												<div class="form-group">
													<label>Employment Status</label>
													<select class="form-control flat" name="employment_status" style="width: 100%">
														@foreach ($data_source_graduate_emp_statuses as $data1)
															<option value="{{ $data1->emp_status }}">{{ $data1->emp_status }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label>Employment Timing</label>
													<select class="form-control flat" name="employment_timing" style="width: 100%">
														@foreach ($data_source_graduate_emp_timings as $data1)
															<option value="{{ $data1->timing }}">{{ $data1->timing }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label>Monthly Salary</label>
													<input type="text" class="form-control flat" name="monthly_salary" placeholder="Monthly Salary">
												</div>
												<div class="form-group">
													<label>Name of Employment Company</label>
													<input type="text" class="form-control flat" name="name_of_employment_company" placeholder="Name of Employment Company">
												</div>
												<div class="form-group">
													<label>Employment Sector</label>
													@php
														$sectors = App\Model\Accr\TrainingSectorsSource::all();
													@endphp
													<select class="form-control flat" name="sector" style="width: 100%">
														@foreach ($sectors as $sector)
															<option value="{{ $sector->id }}">{{ $sector->tvet_field }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label> Employment Sub Sector</label>
													@php
														$sub_sectors = App\Model\Accr\TrainingTradesSource::all();
													@endphp
													<select class="form-control flat" name="sub_sector" style="width: 100%">
														@foreach ($sub_sectors as $sub_sector)
															<option value="{{ $sub_sector->id }}">{{ $sub_sector->sub_field_name }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label>Employer Contact</label>
													<input type="text" class="form-control flat" name="employment_contact" placeholder="Employer Contact">
												</div>
												<div class="form-group">
													<label>Knowledge level</label>

													<select class="form-control flat" name="knowledge_level" style="width: 100%">
														@foreach ($data_source_rating as $data1)
															<option value="{{ $data1->rating }}">{{ $data1->rating }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label>Attitude level</label>
													<select class="form-control flat" name="attitude_level" style="width: 100%">
														@foreach ($data_source_rating as $data)
															<option value="{{ $data->rating }}">{{ $data->rating }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label>Skills level</label>
													<select class="form-control flat" name="skill_level" style="width: 100%">
														@foreach ($data_source_rating as $data1)
															<option value="{{ $data1->rating }}">{{ $data1->rating }}</option>
														@endforeach
													</select>
												</div>
											</div>

										</div>
									</div>
									<div class="modal-footer">
										<div class="row">
											<div class="col-md-6">
												<button type="submit" class="btn btn-success btn-block btn-flat"> <i class="fa fa-save"></i> Save</button>
											</div>
											<div class="col-md-6">
												<button type="button" class="btn btn-default btn-block btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div style="overflow-y: auto;">

						<table id="myTable" class="table table-bordered table-hover">
							<thead style="background: #eaeaea">
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Gender</th>
								<th>Trade</th>
								<th>RTQF Achieved</th>
								<th>Phone</th>
								<th>Employment Status</th>
								<th>Employment Timing</th>
								<th>Monthly Salary</th>
								<th>Name of Employment Company</th>
								<th>Sector</th>
								<th>Sub Sector</th>
								<th>Employer Contact</th>
								<th>Knowledge level</th>
								<th>Attitude level</th>
								<th>Skills level</th>
							</tr>
							</thead>
							<tbody>
							@if (isset($graduates))
								@php
									$i = 1;
								@endphp
								@foreach ($graduates as $graduate)
									<tr>
										<td>{{ $i++ }}</td>
										<td>{{ $graduate->names }}</td>
										<td>{{ $graduate->gender }}</td>
										<td>{{ $graduate->trade }}</td>
										<td>{{ $graduate->rtqf_achieved }}</td>
										<td>{{ $graduate->phone_number }}</td>
										<td>{{ $graduate->employment_status }}</td>
										<td>{{ $graduate->employment_timing }}</td>
										<td>{{ $graduate->monthly_salary }}</td>
										<td>{{ $graduate->name_of_employment_company }}</td>
										<td>{{ $graduate->sector }}</td>
										<td>{{ $graduate->sub_sector }}</td>
										<td>{{ $graduate->employment_contact }}</td>
										<td>{{ $graduate->knowledge_level }}</td>
										<td>{{ $graduate->attitude_level }}</td>
										<td>{{ $graduate->skill_level }}</td>
									</tr>
								@endforeach

								@if (count($graduates) == 0)
									<tr>
										<td colspan="20" class="text-center">No Graduates Recorded in The Selected Period</td>
									</tr>
								@endif
							@endif
							</tbody>
						</table>
					</div>
				</div>
				<br clear="left">
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
@endsection