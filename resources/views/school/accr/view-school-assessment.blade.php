@extends('school.accr.layout.main')

@section('panel-title', "My Assessment")

@section('htmlheader_title', " My Assessment")

@section('panel-body')
    <div class="box">
        <div class="box-header">
            {!! Form::open(['route' => ['school.print.view.myAssessment', $funct->id], 'target' => '_blank']) !!}
            <button type="submit" class="btn btn-primary pull-right shadow-sm">Print</button>
            {!! Form::close() !!}
        </div>
        <div class="box-body" id="elementToPrint">
            <table class="table table-bordered">
                @php($c_indicators=0)
                @php($c_answers=0)
                @foreach($qualities as $quality)
                    <thead class="bg-gray">
                    <tr>
                        <th>Quality Area</th>
                        <th class="text-left" width="200px">Criteria</th>
                        <th class="text-left">Indicator</th>
                        <th class="text-right pr-4">Answers</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="bg-gray-light">
                            <div>{{ ucwords($quality->name) }}</div>
                            <div class="p-1 label text-sm label-info rounded shadow-sm">Scores:
                                {{ getQualityAreaScores($quality->criteria , $school) }}
                                / {{ getTotalWeightForQualityAreas($quality->criteria) }}</div>
                        </td>
                        <td colspan="3">
                            <table class="table">

                                @foreach ($quality->criteria as $criteria_section)
                                    <tr>
                                        <td class="bg-gray-light"
                                            width="188px">
                                            <div>{{ ucwords($criteria_section->criteria_section)  }}</div>
                                            <div class="p-1 label text-sm label-info rounded shadow-sm">
                                                Scores:
                                                {{ getCriteriaScores($criteria_section['criterias'], $school) }}
                                                / {{ $criteria_section->criterias->sum('weight') }}</div>
                                        </td>
                                        <td>
                                            <table class="table">
                                                @foreach ($criteria_section['criterias'] as $criteria)
                                                    @php($c_indicators += 1)
                                                    <thead>
                                                    <th class="text-right bg-gray-light p-0 pr-1"
                                                        colspan="3">
                                                        District
                                                    </th>
                                                    <th class="text-center bg-gray-light p-0 pr-1 pl-1"
                                                        colspan="3">
                                                        WDA
                                                    </th>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td width="430px">
                                                            @php($weight= strtolower($criteria->getSchoolAnswer($school)) == 'yes'? $criteria->weight : 0 )
                                                            <div>{{ ucwords($criteria->criteria) }}</div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="p-1 label text-sm label-info rounded shadow-sm">
                                                                        Scores: {{ $weight }}
                                                                        / {{ $criteria->weight }}</div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td width="50px"
                                                            class="text-center {{ $criteria->getSchoolAnswer($school) ? '' : 'bg-danger' }}">
                                                            @if($criteria->getSchoolAnswer($school))
                                                                @php($c_answers += 1)
                                                                <div>{{ $criteria->getSchoolAnswer($school) }}</div>
                                                            @else
                                                                ---
                                                            @endif
                                                        </td>
                                                        <td width="50px">
                                                            {{ $criteria->getDistrictComment($school->id) }}
                                                        </td>
                                                        <td align="right">
                                                            {{ $criteria->getWdaComment($school->id) }}
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>
                    <tr class="bg-dark">
                        <td colspan="4">
                            <div>
                                <b>{{ ucwords($quality->name) }}</b>
                                <span class="pull-right p-1 text-sm label-info rounded shadow-sm">
                                            <i>
                                                <b>{{ getQualityAreaScores($quality->criteria , $school) }}
                                                    /{{ getTotalWeightForQualityAreas($quality->criteria) }}</b>
                                            </i>
                                        </span>
                            </div>
                        </td>
                    </tr>
                    <tr class="p-0 bg-gray ">
                        <td colspan="4" class="p-0 pl-2 text-center">District Comments</td>
                    </tr>
                    <tr class="p-0 bg-gray ">
                        <td class="p-0 pl-2">Strength</td>
                        <td class="p-0 pl-2">Weakness</td>
                        <td class="p-0 pl-2">Recommendation</td>
                        <td class="p-0 pl-2">Time Line</td>
                    </tr>
                    <tr class="bg-gray-light">
                        <td>
                            <div class="form-group">
                                {!! getCommentsDistrict($school->id,$funct->id,$quality->id, 'strength') !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! getCommentsDistrict($school->id,$funct->id,$quality->id, 'weakness') !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! getCommentsDistrict($school->id,$funct->id,$quality->id, 'recommendation') !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! getCommentsDistrict($school->id, $funct->id, $quality->id, 'timeline') !!}
                            </div>
                        </td>
                    </tr>
                    <tr class="p-0 bg-gray">
                        <td colspan="4" class="p-0 pl-2 text-center">WDA Comments</td>
                    </tr>
                    <tr class="p-0 bg-gray ">
                        <td class="p-0 pl-2">Strength</td>
                        <td class="p-0 pl-2">Weakness</td>
                        <td class="p-0 pl-2">Recommendation</td>
                        <td class="p-0 pl-2">Time Line</td>
                    </tr>
                    <tr class="bg-gray-light">
                        <td>
                            <div class="form-group">
                                {!! getCommentsWda($school->id,$funct->id,$quality->id, 'strength') !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! getCommentsWda($school->id,$funct->id,$quality->id, 'weakness') !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! getCommentsWda($school->id,$funct->id,$quality->id, 'recommendation') !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! getCommentsWda($school->id, $funct->id, $quality->id, 'timeline') !!}
                            </div>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
        </div>
    </div>
@endsection