@extends('layouts.master')

@section('content')
	<link href="{{ asset('css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/smart_wizard_theme_arrows.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/smart_wizard_theme_dots.css') }}" rel="stylesheet" type="text/css" />

	<div class="content-wrapper">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> Quality Process</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	Accreditation / Process</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Section 2 : Process</h3>
							</div>
							<div class="card-body">
									<div id="smartwizard">
							            <ul>
							            	@php
							            		$i = 1;
							            	@endphp
							            	@foreach ($criteria_sections as $criteria_section)
							            		<li><a href="#step-{{ $i }}">Step {{ $i++ }}<br /><small>{{ $criteria_section->criteria_section }}</small></a></li>	

							            	@endforeach
							            </ul>

							            <div style="padding: 10px 20px;background: #fff">

							            	@php
							            		$i = 1;
							            	@endphp
							            	@foreach ($criteria_sections as $criteria_section)
							            		<div id="step-{{ $i }}">
							            			
							            			<h5><i class="fa fa-check-circle"></i> {{ $criteria_section->criteria_section }}</h5>
							            			<form method="POST" action="/accreditation/process/save">

							            				{{ csrf_field() }}
							            				
									            		@foreach ($criteria_section['criterias'] as $criteria)
									            			<input type="hidden" name="criteria_id[]" value="{{ $criteria->id }}">
									            			<div class="form-group row">
																<label class="col-md-8 col-form-label">{{ $criteria->criteria }}</label>
																<div class="col-md-2">
																	<select class="form-control flat" name="answers[]">
																		<option {{ $criteria->myAnswer == 'Yes' ? 'selected' : '' }} >Yes</option>
																		<option {{ $criteria->myAnswer == 'No' ? 'selected' : '' }} >No</option>
																	</select>
																</div>
															</div>
									            		@endforeach

								            		@if (count($criteria_section['criterias']) == 0)
								            			<p class="text-center">No Questions set for this section.</p>
								            		@else
								            			<button type="submit" class="btn btn-primary btn-block btn-flat"> <i class="fa fa-save"></i> Save Answers</button>
								            		@endif
								            		
								            		</form>
								            		<br>

								                    
								                    <div class="row">
								                    	<div class="col-md-12">
								                    		<div class="row">
								                    			<div class="col-md-6">
								                    				
								                    					@if ($i == 1)
								                    						<a href="/accreditation/input" class="btn btn-default btn-block btn-flat"> <i class="fa fa-arrow-left"></i>
								                    						Go Back To Section 1
								                    						</a>
								                    					@else
								                    						<a href="#step-{{ ($i - 1) }}" class="btn btn-default btn-block btn-flat"> <i class="fa fa-arrow-left"></i>
								                    						Previous 	
								                    						</a>
								                    					@endif
								                    				 

								                    							
								                    			</div>
								                    			<div class="col-md-6">
								                    				<a href="#step-{{ ($i + 1) }}" class="btn btn-default btn-block btn-flat"> Next <i class="fa fa-arrow-right"></i></a>				
								                    			</div>
								                    		</div>
								                    		
								                    	</div>
								                    </div>
								                </div>
								                @php
								                	$i++;
								                @endphp
							            	@endforeach

							            	@if (count($criteria_sections) == 0)
							            		<p class="text-center">No Criteria Sections Found</p>
							            	@endif
							            	
							            </div>
							        </div>
							</div>
							<br clear="left">
							<div class="card-footer"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

<script type="text/javascript" src="{{ asset('js/jquery.smartWizard.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        // Toolbar extra buttons


        // Smart Wizard
        $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'arrows',
                keyNavigation:true,

                transitionEffect:'fade',
                toolbarSettings: {toolbarPosition: 'bottom',
                                  showNextButton:false,
                                  showPreviousButton:false
                                },
                anchorSettings: {
                			anchorClickable: true,
            				enableAllAnchors: true,
                            markDoneStep: true, // add done css
                            markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                            removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                        }
             });

        

    });

    $("#finish").click();
</script>
@endsection