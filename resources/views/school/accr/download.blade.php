
<head>
	<title>Accreditation & Q.A PDF Download</title>
</head>


<h2>Acrreditation Application Information Provided</h2>
<h2>Date: {{ date('d/m/Y') }}</h2>
<h3>School: {{ $school_info->school_name }}</h3>
<h3>Manager: {{ $school_info->manager_name }}</h3>
<h3>Phone Number: {{ $school_info->phone_number }}</h3>

<h3>Attachments Provided</h3>	
<table style="width: 100%" border="1" cellspacing="0" cellpadding="5">
	<thead style="background: #efefef">
		<tr>
			<th></th>
			<th>Attachment Name</th>
			
		</tr>
	</thead>
	<tbody>
		@foreach($attachments_data as $ad)
			<tr>
				<td></td>
				<td>{{ $ad->source->attachment_name }}</td>
			</tr>
		@endforeach
		
	</tbody>
</table>

<br>

<h3>Materials &amp; Infrastructure</h3>	
<table style="width: 100%" border="1" cellspacing="0" cellpadding="5">
    	<thead style="background: #efefef">
    		<tr>
    			<th>#</th>
    			<th>Building</th>
    			<th>Infrastructure</th>
    			<th>Purpose</th>
    			<th>Size</th>
    			<th>Capacity</th>
    			<th>Details</th>
    			<th>Construction Materials</th>
    			<th>Roofing Materials</th>
    			<th>Furniture</th>
    			<th>Equipment</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach ($buildings_plots as $bp)
    			<tr>
    				<td></td>
    				<td>{{ $bp->name }}  {{ $bp->class }}</td>	
    				<td>

    					@foreach ($bp['infras'] as $infra)
    						{{ $infra->quantity. ' ' .$infra->name .  ', ' }}
    					@endforeach
    					
    				</td>
    				<td>{{ $bp->purpose }}</td>
    				<td>{{ $bp->size }}</td>
    				<td>{{ $bp->capacity }}</td>
    				<td>
					
								
						<p>Harvests Rain Water {{ $bp->harvests_rain_water }}</p>
								
						<p>Has Water :{{ $bp->has_water }}</p>
					
						<p>Has Electricity : {{ $bp->has_electricity }}</p>
									
						<p>Has Internet :{{ $bp->has_internet }}</p>

						<p>Has Extinguisher :{{ $bp->has_extinguisher }}</p>

									
						<p>Has Lightening Arrestor :{{ $bp->has_lightening_arrestor }}</p>
									
						<p>Has External Lighting :{{ $bp->has_external_lighting }}</p>
											

    				</td>
    				<td>{{ $bp->construction_materials }}</td>
    				<td>{{ $bp->roofing_materials }}</td>
    				<td>{{ $bp->furniture }}</td>
    				<td>{{ $bp->equipment }}</td>
    			</tr>
    			
    		@endforeach
    	</tbody>
</table>

<br>

<h3>Programs Offered</h3>	
<table style="width: 100%" border="1" cellspacing="0" cellpadding="5">
	<thead style="background: #efefef">
		<tr>
			<th>#</th>
			<th>Sector</th>
			<th style="width: 15%">Sub-Sector</th>
			<th style="width: 15%">RTQF Level</th>
			<th style="width: 10%">Male Trainees</th>
			<th style="width: 10%">Female Trainees</th>
			<th>Curriculum</th>
			
		</tr>
	</thead>
	<tbody>
		@php
			$i = 1;
		@endphp
		@foreach($programs_offered as $pd)
			
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $pd->subsector->sector->tvet_field }}</td>
				<td>{{ $pd->subsector->sub_field_name }}</td>
				<td>{{ $pd->rtqf_level->level_name }}</td>
				
				<td>{{ $pd->male_trainees }}</td>
				<td>{{ $pd->female_trainees }}</td>
				<td>{{ $pd->curriculum }}</td>
			</tr>
		@endforeach

	</tbody>
</table>

<br>
<h3>Trainer / Teachers</h3>	
<table style="width: 100%" border="1" cellspacing="0" cellpadding="5">
	<thead style="background: #efefef">
		<tr>
			<th></th>
			<th>Name</th>
			<th style="width: 10%">Gender</th>
			<th>Level / Degree</th>
			<th>Qualification</th>
			<th>Certification</th>
			<th>Experience</th>
			<th>Modules Taught</th>
		</tr>
	</thead>
	<tbody>
		@php
			$i = 1;
		@endphp
		@foreach($trainers as $td)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $td->names }}</td>
				<td>{{ $td->gender }}</td>
				<td>{{ $td->q_level->name }}</td>
				<td>{{ $td->qualification }}</td>
				<td>{{ $td->certification }}</td>
				<td>{{ $td->experience }}</td>
				<td>{{ $td->modules_taught }}</td>
				
			</tr>
		@endforeach

		
	</tbody>
</table>

<br>
<h3>Administration Staff</h3>	
<table style="width: 100%" border="1" cellspacing="0" cellpadding="5">
	<thead style="background: #efefef">
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Gender</th>
			<th>Position</th>
			<th>Qualification</th>
			<th>Institution Studied</th>
			<th>Qualification Level</th>
			<th>Teaching Experience</th>
			<th>Assessor Qualification</th>
			
		</tr>
	</thead>
	<tbody>

		@foreach($staffs as $sd)
			<tr>
				<td></td>
				<td>{{ $sd->names }}</td>
				<td>{{ $sd->gender }}</td>
				<td>{{ $sd->position }}</td>
				<td>{{ $sd->academic_qualification }}</td>
				<td>{{ $sd->institution_studied }}</td>
				<td>{{ $sd->q_level->name }}</td>
				<td>{{ $sd->teaching_experience }}</td>
				<td>{{ $sd->assessor_qualification }}</td>
				
			</tr>
		@endforeach

		
	</tbody>
</table>
<br>
<h3>Qualification Applied For</h3>	
<table style="width: 100%" border="1" cellspacing="0" cellpadding="5">
	<thead style="background: #efefef">
		<tr>
			<th></th>
			<th style="width: 30%">Sector</th>
			<th style="width: 20%">Sub Sector</th>
			<th style="width: 20%">Curriculum Qualification</th>
			<th>RTQF Level</th>
		</tr>
	</thead>
	<tbody>
		@foreach($application_data as $ad) 
			<tr>
				<td></td>
				<td>{{ $ad->curr->subsector->sector->tvet_field }}</td>
				<td>{{ $ad->curr->subsector->sub_field_name }}</td>
				<td>{{ $ad->curr->qualification_title }}</td>
				<td>{{ $ad->rtqf->level_name }}</td>
			</tr>
		@endforeach
	</tbody>
</table>