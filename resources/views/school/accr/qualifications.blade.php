@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Attachments Source</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	  Accreditation / Source Input / Attachments</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						@if (isset($att_info))
							
							<form method="POST" action="/accr/input/attachments">
								{{ csrf_field() }}
								
								<input type="hidden" name="att_id" value="{{ $att_info->id }}">
								<div class="card card-info">
									<div class="card-header">
										<h3 class="card-title"><i class="fa fa-edit"></i> Edit Attachment</h3>
									</div>
								

									
									<div class="card-body">
										<div class="form-group">
											<label>Title</label>
											<input type="text" class="form-control flat" name="attachment" value="{{ $att_info->attachment_name }}" placeholder="Program">
										</div>
										
									</div>
									<div class="card-footer">
										<button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save Attachment</button>
									</div>
								</div>
							</form>
						@endif
						
						<table class="table table-bordered" style="background: #fff">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th>Code</th>
									<th>Title</th>
									<th>Sector</th>
									<th>Sub Sector</th>
									<th>RTQF Level</th>
									<th>Credits</th>
									<th>Release Date</th>
									<th>Status</th>
									<th style="width: 200px">Action</th>
								</tr>
							</thead>
							<tbody>
								@php
									$i = 1;
								@endphp
								@foreach ($qualifications as $qualification)
									<tr>
										<td>{{ $i++ }}</td>
										<td>{{ $qualification->qualification_code }}</td>
										<td>{{ $qualification->qualification_title }}</td>
										<td>{{ $qualification->subsector->sector->tvet_field }}</td>
										<td>{{ $qualification->subsector->sub_field_name }}</td>
										<td>{{ $qualification->rtqf->level_name }}</td>
										<td>{{ $qualification->credits }}</td>
										<td>{{ $qualification->release_date }}</td>
										<td>{{ $qualification->status }}</td>
										<td>
											<form method="POST">
												{{ csrf_field() }}
												<input type="hidden" name="_method" value="delete">
												<input type="hidden" name="delete_qualification" value="{{ $qualification->id }}">
											
												<a href="/accr/input/qualifications/view/{{ $qualification->id }}" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i> View More</a>
												<a href="/accr/input/qualifications/{{ $qualification->id }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i> Edit</a>
												<button class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to remove this qualification: \n\n{{ $qualification->qualification_title }}?')"><i class="fa fa-trash"></i> Delete</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					
						<form method="POST" enctype="multipart/form-data" action="/accr/input/qualifications">
							{{ csrf_field() }}
						
						@if (isset($qualification_info))
							<input type="hidden" name="qualification_id" value="{{ $qualification_info->id }}">
						@endif

						<div class="card card-info">
							<div class="card-header">
								@if (isset($qualification_info))
									<h4 class="card-title"><i class="fa fa-edit"></i> Edit Qualification</h4>
								@else
									<h4 class="card-title"><i class="fa fa-plus"></i> Add Qualification</h4>
								@endif
								
							</div>		
							<div class="card-body">
								<div class="form-group">
									<label>Qualification Code</label>
									<input type="text" class="form-control flat" name="qualification_code" placeholder="Qualification Code" value="{{ $qualification_info->qualification_code or '' }}">
								</div>
								<div class="form-group">
									<label>Qualification Title</label>
									<input type="text" class="form-control flat" name="qualification_title" placeholder="Qualification Title" value="{{ $qualification_info->qualification_title or '' }}">
								</div>
								<div class="form-group">
									<label>Sector</label>
									<select class="form-control flat" name="sector_id" id="sector2">
										<option value="">Choose Here</option>
										@foreach ($sectors as $sector)
											<option value="{{ $sector->id }}" @if(isset($qualification_info) && $sector->id == $qualification_info->sector_id) selected @endif>{{ $sector->tvet_field }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label>Sub Sector</label>
									<select class="form-control flat" name="sub_sector_id" id="sub_sectors2">
										@if (isset($qualification_info))
											@foreach ($qualification_info->subsector->sector->subsectors as $subsector)
											 	<option value="{{ $subsector->id }}" @if($subsector->id == $qualification_info->sub_sector_id) selected @endif >{{ $subsector->sub_field_name }}</option>
											 @endforeach 
										@endif
									</select>
								</div>
								<div class="form-group">
									<label>RTQF Level</label>
									<select class="form-control flat" name="rtqf_level_id">
										@foreach ($rtqfs as $rtqf)
											<option value="{{ $rtqf->id }}" @if(isset($qualification_info) && $rtqf->id == $qualification_info->rtqf_level_id) selected @endif>{{ $rtqf->level_name }}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group">
									<label>Credits</label>
									<input type="number" class="form-control flat" name="credits" placeholder="Credits" value="{{ $qualification_info->credits or '' }}">
								</div>
								<div class="form-group">
									<label>Release Date</label>
									<div class="input-group">
					                    <div class="input-group-prepend">
					                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
					                    </div>
					                    <input type="text" class="form-control" name="release_date" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
					                  </div>
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control flat">
										<option value="1">Current</option>
										<option value="2">Superseded</option>
									</select>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea class="form-control flat" name="description" placeholder="Description">{{ $qualification_info->description or '' }}</textarea>
								</div>
								<div class="form-group">
									<label>Job Related Information</label>
									<textarea class="form-control flat" name="job_related_information" placeholder="Job Related Information">{{ $qualification_info->job_related_information or '' }}</textarea>
								</div>
								<div class="form-group">
									<label>Entry Requirements</label>
									<textarea class="form-control flat" name="entry_requirement" placeholder="Entry Requirements">{{ $qualification_info->entry_requirement or '' }}</textarea>
								</div>
								<div class="form-group">
									<label>Information About Pathways</label>
									<textarea class="form-control flat" name="information_about_pathways" placeholder="Information About Pathways">{{ $qualification_info->information_about_pathways  or ''}}</textarea>
								</div>
								<div class="form-group">
									<label>Employability And Life Skills</label>
									<textarea class="form-control flat" name="employability_and_life_skills" placeholder="Employability And Life Skills">{{ $qualification_info->employability_and_life_skills or '' }}</textarea>
								</div>
								<div class="form-group">
									<label>Qualification Arrangments</label>
									<textarea class="form-control flat" name="qualification_arrangement" placeholder="Qualification Arrangments"{{ $qualification_info->qualification_arrangement or '' }}></textarea>
								</div>
								<div class="form-group">
									<label>Attachment</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="attachment_name;" id="exampleInputFile">
											<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
										</div>
				                    </div>
								</div>
								<div class="form-group">
									<label>Occupation Profile Attachment</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="occupational_profile_attachment" id="exampleInputFile">
											<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
										</div>
				                    </div>
								</div>
								<div class="form-group">
									<label>TOG Download</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="tog_download" id="exampleInputFile">
											<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
										</div>
				                    </div>
								</div>
								<div class="form-group">
									<label>Training Manual</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="training_manual" id="exampleInputFile">
											<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
										</div>
				                    </div>
								</div>
								<div class="form-group">
									<label>Compentency</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="competency_standards" id="exampleInputFile">
											<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
										</div>
				                    </div>
								</div>
								<div class="form-group">
									<label>Qualification Summary Attachment</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="qualification_summary_attachment_name" id="exampleInputFile">
											<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
										</div>
				                    </div>
								</div>
							</div>
							<div class="card-footer">
								<button  type="submit" class="btn btn-info btn-block btn-flat"><i class="fa fa-save"></i> Save Qualification</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>

@endsection