<h3 class="label label-primary">WDA Final AQA</h3>
<table class="table table-bordered table-hover">
    <thead style="background: #efefef">
    <tr>
        <th style="width: 25%">Strength</th>
        <th>Weakness</th>
        <th>Opportunity</th>
        <th>Threat</th>
        <th>Recommendation</th>
        <th>Marks</th>
    </tr>
    </thead>
    <tbody>

        <tr>
            <td>{!!  $Wda_comments->strength or "" !!}</td>
            <td>{!! $Wda_comments->weakness or ""  !!}</td>
            <td>{!! $Wda_comments->opportunity or ""  !!}</td>
            <td>{!! $Wda_comments->threat or ""  !!}</td>
            <td>{!! $Wda_comments->recommendation or ""  !!}</td>
            <td>{!! $Wda_comments->marks or ""  !!}</td>
        </tr>

    </tbody>
</table>