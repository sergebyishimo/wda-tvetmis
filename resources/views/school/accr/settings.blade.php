@extends('layouts.master')

@section('content')

	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-cogs"></i> Accreditation General Settings</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	Accreditation / General Settings</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">
						<form method="POST">
							{{ csrf_field() }}

							<input type="hidden" name="form" value="welcome">
							<input type="hidden" name="welcome_id" value="{{ $welcome->id or "" }}">
						<div class="card card-primary card-outline">
							<div class="card-header">
								<h3 class="card-title">	<i class="fa fa-edit"></i> Edit Welcome Message </h3>
								<div class="card-tools">
									<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								</div>
						</div>
						<div class="card-body pad">
							<div class="mb-3">
				                <textarea class="textarea" name="message" placeholder="Place some text here"
				                          style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>{{ $welcome->message or "" }}</textarea>
							</div>
							
						</div>
						<div class="card-footer">
							<button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Save Welcome Message</button>
						</div>
					</div>
						</form>

						
								
							<div class="card card-primary card-outline">
								<div class="card-header">
									<h3 class="card-title">	<i class="fa fa-edit"></i> Edit Notifications</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
							</div>
							<div class="card-body pad">
								
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Notification Message</th>
											<th>Last Update</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@php
											$i = 1;
										@endphp
										@foreach ($notifications as $notification)
											<tr>
												<td>{{ $i++ }}</td>
												<td>{{ $notification->message }}</td>
												<td>{{ date('d/m/Y', strtotime($notification->updated_at)) }}</td>
												<td>
													<form method="POST">
														{{ csrf_field() }}
														<input type="hidden" name="_method" value="delete">
														
														<input type="hidden" name="form" value="notification">
														<input type="hidden" name="delete_noti" value="{{ $notification->id }}">
														
														<a href="/accr/settings/notifications/edit/{{ $notification->id }}" class="btn btn-primary btn-sm btn-flat">	<i class="fa fa-edit"></i> Edit </a>

														<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to remove this notification : \n\n {{ $notification->message }} ? ')"> <i class="fa fa-trash "></i> Delete </button>

													</form>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
								<form method="POST" action="/accr/settings">
								{{ csrf_field() }}

									<input type="hidden" name="form" value="notification">
								<br clear="left"><br>

								@if (isset($notification_info))
									<input type="hidden" name="noti_id" value="{{ $notification_info->id }}">
									<div class="form-group">
										<label style="color: green"><i class="fa fa-edit"></i> Edit Notification</label>
										<textarea class="form-control flat" name="u_message" placeholder="Notification Message" required>{{ $notification_info->message }}</textarea>
									</div>
								@else
									<div class="form-group">
										<label><i class="fa fa-bell"></i> Add Notification</label>
										<textarea class="form-control flat" name="message" placeholder="Notification Message" required></textarea>
									</div>
								@endif

								
								
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Save Notification</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>


@endsection