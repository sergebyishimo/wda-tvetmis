@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-table"></i> Curricula</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	  Curriculum</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card bg-primary-gradient">
							<div class="card-header no-border ui-sortable-handle">
				                <h3 class="card-title">
				                	<i class="fa fa-table"></i>
				                  Curricula 
				                </h3>
			                </div>
			                <div class="card-body">
			                	<div class="row text-center">
			                		<div class="col-md-9" style="float: inherit;margin: auto;">
			                			<div class="row">
			                				<div class="col-md-4" >
			                					<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                			
					                		</div>
					                		<div class="col-md-4">
					                			<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                		</div>
					                		<div class="col-md-4">
					                			<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                		</div>		
			                			</div>
			                			<br>
			                			<div class="row">
			                				<div class="col-md-4" >
			                					<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                			
					                		</div>
					                		<div class="col-md-4">
					                			<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                		</div>
					                		<div class="col-md-4">
					                			<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                		</div>		
			                			</div>
			                			<br>
			                			<div class="row">
			                				<div class="col-md-4" >
			                					<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                			
					                		</div>
					                		<div class="col-md-4">
					                			<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                		</div>
					                		<div class="col-md-4">
					                			<div style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 10px 10px">
			                						<p class="col-form-label">Sector N</p>	
			                					</div>
					                		</div>		
			                			</div>
			                		</div>
			                		
			                	</div>
			                </div>
			                <div class="card-footer"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection