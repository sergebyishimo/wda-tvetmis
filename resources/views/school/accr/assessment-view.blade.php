@extends('school.accr.layout.main')

@section('panel-title', " My Assessment")

@section('htmlheader_title', " My Assessment")

@section('panel-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-bordered table-condensed text-center" id="dataTable" style="width: 100%;">
                <thead>
                <tr>
                    <th width="40px">#</th>
                    <th>Function</th>
                    <th>Scores</th>
                    <th>Rating</th>
                    <th>Stage/Status</th>
                    <th>Accreditation</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @php
                    $x = 1;
                @endphp
                @if($submited->count() > 0)
                    @foreach($submited as $assessment)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ $assessment->functions->name }}</td>
                            <td width="50px">
                                @php
                                    $tt = getFunctionScores($assessment->functions->qualityAreas, $assessment->school, true);
                                    $hv = getFunctionScores($assessment->functions->qualityAreas, $assessment->school);
                                    $pp = ($hv * 100) / $tt;
                                    $pp = ceil($pp);
                                @endphp
                                {{ $pp ." %" }}
                            </td>
                            <td>
                                <label class="label label-success text-sm p-1 rounded shadow-sm">{{ getRattingScore($pp) }}</label>
                            </td>
                            <td>
                                @if($assessment->district == null)
                                    <label class="label label-info p-1 rounded shadow-sm">At district</label>
                                @elseif($assessment->wda == null)
                                    <label class="label label-info p-1 rounded shadow-sm">At WDA</label>
                                @else
                                    <label class="label label-info p-1 rounded shadow-sm">Viewed</label>
                                @endif
                            </td>
                            <td>
                                @if($assessment->district == 1 && $assessment->wda == 1 )
                                @else
                                    <label class="label label-warning p-1 rounded shadow-sm">In Process</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('school.view.myAssessment', [$assessment->function_id]) }}"
                                   class="btn btn-sm btn-dark btn-block">View</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
@endsection