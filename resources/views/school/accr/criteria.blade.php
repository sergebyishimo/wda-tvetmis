@extends('layouts.master')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="font-size: 22px;padding-left: 5px;"><i class="fa fa-edit"></i> Indicators</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/home">Home</a></li>
                            <li class="breadcrumb-item active"> Accreditation / Input / Indicators</li>
                        </ol>
                    </div>
                </div>
                @if (session('successMessage'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fa fa-check"></i> Operation Done!</h5>
                        {!! session('successMessage') !!}
                    </div>

                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        @if (isset($program_info))
                            <form method="POST" action="/accr/input/criterias">
                                {{ csrf_field() }}

                                <input type="hidden" name="program_id" value="{{ $program_info->id }}">
                                <input type="hidden" name="criteria_section_id"
                                       value="{{ $program_info->section->id }}">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fa fa-edit"></i> Edit Indicator</h3>
                                    </div>


                                    <div class="card-body">

                                        <div class="form-group">
                                            <label>Reporting Period</label>
                                            <p>{{ $program_info->section->criteria_section }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Program</label>
                                            <input type="text" class="form-control flat" name="name"
                                                   value="{{ $program_info->criteria }}" placeholder="Program">
                                        </div>
                                        <div class="form-group">
                                            <label>Weight</label>
                                            <input type="text" step="any" class="form-control flat" name="weight"
                                                   value="{{ $program_info->weight }}" placeholder="Weight">
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="text" class="btn btn-info btn-flat btn-block"><i
                                                    class="fa fa-save"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif

                        <table class="table table-bordered table-condensed" style="background: #fff">
                            <thead>
                            <tr class="bg-dark-gradient">
                                <th class="text-center" width="40px">#</th>
                                <th colspan="3">Functions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($x = 1)
                            @foreach ($rps as $function)
                                <tr>
                                    <td class="text-center">{{ $x ++ }}</td>
                                    <td width="200px" class="bg-dark-gradient"><h6>{{ $function->name }}</h6></td>
                                    <td colspan="3">
                                        @if($function->qualityAreas->count() > 0)
                                            <table>
                                                <thead class="bg-dark-gradient">
                                                <th colspan="2">Quality Area</th>
                                                </thead>
                                                @foreach($function->qualityAreas as $quality)
                                                    <tr>
                                                        <td class="bg-dark-gradient">
                                                            <h6>{{ $quality->name }}</h6>
                                                            <small>Weight:
                                                            </small>&nbsp;<span>{{ getTotalWeightForQualityAreas($quality->criteria) }}</span>
                                                        </td>
                                                        <td>
                                                            @if($quality->criteria->count() > 0)
                                                                <table class="table">
                                                                    <thead class="bg-dark-gradient">
                                                                    <th colspan="2">Criteria</th>
                                                                    </thead>
                                                                    @foreach($quality->criteria as $rpp)
                                                                        <tr>
                                                                            <td class="bg-dark-gradient">
                                                                                <h6>{{ $rpp->criteria_section }}</h6>
                                                                                <small>Weight:
                                                                                </small>&nbsp;<span>{{ $rpp->criterias->sum('weight') }}</span>
                                                                            </td>
                                                                            <td>
                                                                                <table class="table">
                                                                                    <thead class="bg-dark-gradient">
                                                                                    <th>Indicator</th>
                                                                                    <th colspan="2">Weight</th>
                                                                                    </thead>
                                                                                    @foreach ($rpp->criterias as $criteria)
                                                                                        <tr>
                                                                                            <td width="300px">{{ $criteria->criteria }}</td>
                                                                                            <td class="text-center"
                                                                                                width="60px">{{ $criteria->weight }}</td>
                                                                                            <td>
                                                                                                <form method="POST">
                                                                                                    {{ csrf_field() }}
                                                                                                    <input type="hidden"
                                                                                                           name="_method"
                                                                                                           value="delete">
                                                                                                    <input type="hidden"
                                                                                                           name="delete_pro"
                                                                                                           value="{{ $criteria->id }}">

                                                                                                    <a href="/accr/input/criterias/{{ $criteria->id }}"
                                                                                                       class="btn btn-primary btn-sm btn-flat">
                                                                                                        <i
                                                                                                                class="fa fa-edit"></i>
                                                                                                        Edit </a>
                                                                                                    <button type="submit"
                                                                                                            class="btn btn-danger btn-sm btn-flat pull-right"
                                                                                                            onclick="return window.confirm('Are you sure you want to remove this program : \n\n {{ $criteria->criteria }} ?')">
                                                                                                        <i class="fa fa-trash"></i>
                                                                                                        Delete
                                                                                                    </button>

                                                                                                </form>

                                                                                            </td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </table>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach

                            <form method="POST" id="form">
                                {{ csrf_field() }}
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <select class="form-control flat select2"
                                                            name="criteria_section_id">
                                                        @foreach ($rps as $function)
                                                            <optgroup label="-{{ ucwords($function->name) }}">
                                                                @foreach($function->qualityAreas as $quality)
                                                                    <optgroup label="--{{ ucwords($quality->name) }}">
                                                                        @foreach($quality->criteria as $rp)
                                                                            <option value="{{ $rp->id }}">{{ $rp->criteria_section }}</option>
                                                                        @endforeach
                                                                    </optgroup>
                                                                @endforeach
                                                            </optgroup>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td><input type="text" class="form-control flat" name="name"
                                                           placeholder="Indicators"></td>
                                                <td width="130px"><input type="text" class="form-control flat"
                                                                         name="weight"
                                                                         placeholder="Weight"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text-center">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <button type="submit" class="btn btn-success btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </form>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@section('scripts')

@endsection