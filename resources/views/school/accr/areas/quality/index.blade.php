@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="font-size: 22px;padding-left: 5px;"> Quality Area</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/home">Home</a></li>
                            <li class="breadcrumb-item active"> Quality Area</li>
                        </ol>
                    </div>
                </div>
                @if (session('successMessage'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fa fa-check"></i> Operation Done!</h5>
                        {!! session('successMessage') !!}
                    </div>

                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <table class="table table-bordered" style="background: #fff">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="40px">#</th>
                                        <th>Functions</th>
                                        <th>Quality Area</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($functions as $att)
                                        <tr>
                                            <td class="text-center">{{ $i++ }}</td>
                                            <td>{{ $att->name }}</td>
                                            <td>
                                                <table class="table">
                                                    @php($c=1)
                                                    @if($att->qualityAreas->count() > 0)
                                                        @foreach($att->qualityAreas as $area)
                                                            <tr>
                                                                <td>{{ $area->name }}</td>
                                                                <td>
                                                                    {!! Form::open(['route' => ['destroy.quality.area', $area->id]]) !!}
                                                                    @method('DELETE')
                                                                    @csrf
                                                                    {!! Form::hidden('id', $area->id) !!}

                                                                    <a href="{{ route('edit.quality.area', $area->id) }}"
                                                                       class="btn btn-primary btn-flat btn-sm"><i
                                                                                class="fa fa-edit"></i> Edit</a>

                                                                    <button type="submit"
                                                                            class="btn btn-danger btn-flat btn-sm"
                                                                            onclick="return window.confirm('Are you sure you want to remove this quality area: \n\n {{ $att->criteria_section }} ?')">
                                                                        <i class="fa fa-trash"></i> Delete
                                                                    </button>
                                                                    {!! Form::close() !!}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </table>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @php($route = 'store.quality.area')
                                    @isset($quality)
                                        @php($route = ['update.quality.area', $quality->id])
                                    @endisset
                                    {!! Form::open(['route' => $route]) !!}
                                    @csrf
                                    <tr>
                                        <td colspan="2">
                                            {!! Form::select('function_id', getFunctionsForSelect(), isset($quality) ? $quality->function_id : null , ['class' => 'form-control', 'placeholder' => 'Select Function', 'required' => true]) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('name', isset($quality) ? $quality->name : null, ['class' => 'form-control', 'placeholder' => 'Quality Area', 'required' => true]) !!}
                                        </td>
                                        <td class="text-center">
                                            <button type="submit" class="btn btn-success btn-flat btn-block"><i
                                                        class="fa fa-save"></i> Save
                                            </button>
                                        </td>

                                    </tr>
                                    {!! Form::close() !!}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
@endsection