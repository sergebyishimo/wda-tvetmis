@extends('school.accr.layout.main')

@section('panel-title', "Quality Input")

@section('htmlheader_title', "Quality Input")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Inputs</h3>
                </div>
                <div class="box-body">
                    <!-- SmartWizard html -->
                    <div id="smartwizard">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                        @php
                                            $v=1;
                                            $vv = $qualities->count() + 1;
                                        @endphp
                                        @foreach($qualities as $quality)
                                            <li class="{{ $step == $v ? 'active' : '' }}">
                                                <a href="?step={{ $v }}">Area {{ $v++ }}<br/>
                                                    <small>{{ $quality->name }}</small>
                                                </a>
                                            </li>
                                        @endforeach

                                        <li class="{{ $step == $vv ? 'active' : '' }}">
                                            <a href="?step={{ $vv }}" style="margin-top: 11px">
                                                Confirmation
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        <div style="padding: 10px 20px;background: #fff">
                            @php
                                $v=1;
                            @endphp
                            @foreach($qualities as $quality)
                                {{--Section $v : Input--}}
                                @if($step == $v)
                                    <div id="step-{{ $v }}">
                                        <div id="csmartwizard" class="csmartwizard">
                                            @if ($quality->criteria->count() > 0)
                                                <nav class="navbar navbar-default">
                                                    <div class="container-fluid">
                                                        <div class="collapse navbar-collapse"
                                                             id="bs-example-navbar-collapse-1">
                                                            <ul class="nav navbar-nav">
                                                                @php($i = 1)
                                                                @foreach ($quality->criteria as $criteria_section)
                                                                    <li class="{{ $istep == $i && $step == $v ? 'active' : '' }}">
                                                                        <a href="?step={{ $v }}&istep={{ $i }}">
                                                                            Step {{ $i++ }}<br/>
                                                                            <small>{{ $criteria_section->criteria_section }}</small>
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </nav>
                                            @endif
                                            <div style="padding: 10px 20px;background: #fff">
                                                @php($i = 1)
                                                @if ($quality->criteria->count() == 0)
                                                    <p class="text-center">No Criteria Sections Found</p>
                                                @else
                                                    @foreach ($quality->criteria as $criteria_section)
                                                        @if($istep == $i && $step == $v)
                                                            <div id="step-{{ $v }}-{{ $i }}">
                                                                <h5>
                                                                    <i class="fa fa-check-circle"></i> {{ $criteria_section->criteria_section }}
                                                                </h5>
                                                                <form method="POST"
                                                                      action="{{ route('school.accr.save.process') }}">

                                                                    {{ csrf_field() }}
                                                                    {!! Form::hidden('step', $step) !!}
                                                                    {!! Form::hidden('istep', $istep) !!}
                                                                    {!! Form::hidden('lstep', count($quality->criteria)) !!}
                                                                    @foreach ($criteria_section['criterias'] as $criteria)
                                                                        <input type="hidden" name="criteria_id[]"
                                                                               value="{{ $criteria->id }}">
                                                                        {!! Form::hidden('url', route('school.accr.input')) !!}
                                                                        <div class="form-group row">
                                                                            <label class="col-md-8 col-form-label">{{ $criteria->criteria }}</label>
                                                                            <div class="col-md-4">
                                                                                <select class="form-control flat select2m"
                                                                                        name="answers[]" required
                                                                                        style="width: 100%;">
                                                                                    <option value="" selected disabled>
                                                                                        Select answer
                                                                                    </option>
                                                                                    <option {{ $criteria->myAnswer == 'Yes' ? 'selected' : '' }} >
                                                                                        Yes
                                                                                    </option>
                                                                                    <option {{ $criteria->myAnswer == 'No' ? 'selected' : '' }} >
                                                                                        No
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach

                                                                    @if (count($criteria_section['criterias']) == 0)
                                                                        <p class="text-center">No Questions set for this
                                                                            section.</p>
                                                                    @else
                                                                        <button type="submit"
                                                                                class="btn btn-primary btn-block btn-flat">
                                                                            <i class="fa fa-save"></i>
                                                                            @if(count($criteria_section['criterias']) < $i )
                                                                                Save Answers and Click Next
                                                                            @else
                                                                                Save Answers and Click Next Area
                                                                            @endif
                                                                        </button>
                                                                    @endif

                                                                </form>
                                                                <br>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                @if ($i == 1)
                                                                                    <a href="{{ route('school.accr.input') }}"
                                                                                       class="btn btn-default btn-block btn-flat">
                                                                                        <i class="fa fa-arrow-left"></i>
                                                                                        Go Back To Section 1
                                                                                    </a>
                                                                                @else
                                                                                    <a href="?istep={{ ($i > 2 ? $i- 1 : $i) }}&step={{ $v }}"
                                                                                       class="btn btn-default btn-block btn-flat">
                                                                                        <i class="fa fa-arrow-left"></i>
                                                                                        Previous
                                                                                    </a>
                                                                                @endif
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <a href="?istep={{ ($quality->criteria->count() == $i ? $i : $i + 1) }}&step={{ $v }}"
                                                                                   class="btn btn-default btn-block btn-flat">
                                                                                    Next <i class="fa fa-arrow-right"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @break
                                                        @endif
                                                        @php($i++)
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @break
                                @endif
                                @php($v++)
                            @endforeach
                            @if($step == $vv)
                                <div id="step-{{ $vv }}">
                                    <div class="box">
                                        <div class="box-header">
                                            <h6>
                                                {{--<span class="pull-left">SubmitID: {{ $submitId }}</span>--}}
                                                <span class="pull-right"><i>Done on: </i> {{ $today }}</span>
                                            </h6>
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-bordered">
                                                @php($c_indicators=0)
                                                @php($c_answers=0)
                                                @foreach($qualities as $quality)
                                                    <thead class="bg-gray">
                                                    <tr>
                                                        <th>Quality Area</th>
                                                        <th class="text-left">Indicator</th>
                                                        <th class="text-right pr-4">Answers</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="bg-gray-light">{{ ucwords($quality->name) }}</td>
                                                        <td colspan="2">
                                                            <table class="table">

                                                                @foreach ($quality->criteria as $criteria_section)
                                                                    <tr>
                                                                        <td class="bg-gray-light">{{ ucwords($criteria_section->criteria_section)  }}</td>
                                                                        <td>
                                                                            <table class="table">
                                                                                @foreach ($criteria_section['criterias'] as $criteria)
                                                                                    @php($c_indicators += 1)
                                                                                    <tr>
                                                                                        <td>{{ ucwords($criteria->criteria) }}</td>
                                                                                        <td width="50px"
                                                                                            class="text-center {{ $criteria->myAnswer ? '' : 'bg-danger' }}">
                                                                                            @if($criteria->myAnswer)
                                                                                                @php($c_answers += 1)
                                                                                                {{ $criteria->myAnswer }}
                                                                                            @else
                                                                                                ---
                                                                                            @endif
                                                                                        </td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                @endforeach
                                            </table>
                                        </div>
                                        <br clear="left">
                                        <div class="box-footer">
                                            @if(getCurrentSelfAssessmentStatus(null, $function_id) == null)
                                                {!! Form::open(['route' => 'school.confirm.submit']) !!}
                                                @method('POST')
                                                @isset($function_id)
                                                    {!! Form::hidden('function_id', $function_id) !!}
                                                @endisset
                                                {!! Form::hidden('step', $step) !!}
                                                {!! Form::hidden('istep', $istep) !!}
                                                {!! Form::hidden('school_id', $school_id) !!}
                                                {!! Form::hidden('academic_year', date('Y')) !!}
                                                @if($c_answers == $c_indicators)
                                                    {!! Form::submit('Confirm to submit', ['class' => 'btn btn-primary']) !!}
                                                @else
                                                    <p class="alert alert-warning">
                                                        Please Provide
                                                        Answer{{ $c_indicators - $c_answers > 1 ? 's' : '' }}
                                                        on each
                                                        indicator.
                                                        Remain: {{ $c_indicators - $c_answers }}
                                                        indicator{{ $c_indicators - $c_answers > 1 ? 's' : '' }} !!
                                                    </p>
                                                @endif
                                                {!! Form::close() !!}
                                            @else
                                                <p class="alert alert-warning">
                                                    You already submited your assessment !!
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("l-scripts")
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            $("#finish").click();
        });
    </script>
@endsection