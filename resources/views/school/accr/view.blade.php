@extends('school.accr.layout.main')

@section('panel-title', "Qualifications Applied for")

@section('htmlheader_title', "Qualifications Applied for")

@section('panel-body')
    @if(isset($apps))
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered" style="background: #fff">
                            <thead style="background: #e8e8e8">
                            <tr>
                                <td class="text-center">#</td>
                                <th>Accreditation Type</th>
                                <th>Sector</th>
                                <th>Sub Sector</th>
                                <th>Qualification</th>
                                <th>Last Update</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @php($x=1)
                            @foreach ($apps as $app)
                                <tr>
                                    <td class="text-center">{{ $x++ }}</td>
                                    <td>{{ getApplicationAccreditationType($app->application_id)  }}</td>
                                    <td>{{ $app->curr->subsector->sector->tvet_field or ""  }}</td>
                                    <td>{{ $app->curr->subsector->sub_field_name or "" }}</td>
                                    <td>{{ $app->curr->qualification_title or "" }}</td>
                                    <td>{{ date('d/m/Y', strtotime($app->created_at)) }}</td>
                                    <td>
                                        <a href="{{ route('school.accr.view', $application_id) }}"
                                           class="btn btn-primary btn-flat"><i class="fa fa-list"></i> View More</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="box box-danger">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="text-center">No Accreditation Applications Found. First Fill the application.</h5>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection