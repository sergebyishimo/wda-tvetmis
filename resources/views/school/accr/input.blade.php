@extends('school.accr.layout.main')

@section('panel-title', "School Application")

@section('htmlheader_title', "School Application")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="card-title">School Application</h3>
                </div>
                <!-- /.card-header -->
                <div class="box-body">
                    <div id="myForm">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                        <li class="{{ $step == 1 ? 'active' : '' }}">
                                            <a href="?step=1">Step 1<br/>
                                                <small>School Info</small>
                                            </a>
                                        </li>
                                        <li class="{{ $step == 2 ? 'active' : '' }}">
                                            <a href="?step=2">Step 2<br/>
                                                <small>Attachments</small>
                                            </a>
                                        </li>
                                        <li class="{{ $step == 3 ? 'active' : '' }}">
                                            <a href="?step=3">Step 3<br/>
                                                <small>Materials</small>
                                            </a>
                                        </li>
                                        <li class="{{ $step == 4 ? 'active' : '' }}">
                                            <a href="?step=4">Step 4<br/>
                                                <small>Programs</small>
                                            </a>
                                        </li>
                                        <li class="{{ $step == 5 ? 'active' : '' }}">
                                            <a href="?step=5">Step 5<br/>
                                                <small>Trainers</small>
                                            </a>
                                        </li>
                                        <li class="{{ $step == 6 ? 'active' : '' }}">
                                            <a href="?step=6">Step 6<br/>
                                                <small>Admin Staff</small>
                                            </a>
                                        </li>
                                        <li class="{{ $step == 7 ? 'active' : '' }}">
                                            <a href="?step=7">Step 7<br/>
                                                <small>Applying For</small>
                                            </a>
                                        </li>
                                        <li class="{{ $step == 8 ? 'active' : '' }}">
                                            <a href="?step=8">Step 8<br/>
                                                <small>Confirmation</small>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        <div class="content" style="padding: 10px 20px;background: #fff">
                            @if($step == 1)
                                {{--Section 1 : Input--}}
                                <div id="step-1">
                                    <h5><i class="fa fa-check-circle"></i> School Information</h5>
                                    <form id="form-step-0" role="form" method="POST"
                                          action="{{ route('school.accr.input.save') }}">
                                        {{ csrf_field() }}

                                        <input type="hidden" name="form" value="school_info">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Name:</label>
                                                    <input type="text" class="form-control flat" name="name"
                                                           value="{{ $school_info['school_name'] }}" readonly>

                                                </div>

                                                <div class="form-group">
                                                    <label for="">Province:</label>
                                                    <input type="text" class="form-control flat" name="province"
                                                           value="{{ $school_info['province'] }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">District:</label>
                                                    <input type="text" class="form-control flat" name="district"
                                                           value="{{ $school_info['district'] }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Sector:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="sector"
                                                           readonly
                                                           value="{{ $school_info['sector'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Cell:</label>
                                                    <input type="text" class="form-control flat" name="cell"
                                                           readonly
                                                           value="{{ $school_info['cell'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Village:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="village"
                                                           readonly
                                                           value="{{ $school_info['village'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Manager Name:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="manager_name"
                                                           readonly
                                                           value="{{ $school_info['manager_name'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Manager Phone:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="manager_phone"
                                                           readonly
                                                           value="{{ $school_info['manager_phone'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Manager E-mail:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="manager_email"
                                                           readonly
                                                           value="{{ $school_info['manager_email'] }}">

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Latitude:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="latitude"
                                                           readonly
                                                           value="{{ $school_info['latitude'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Longitude:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="longitude"
                                                           readonly
                                                           value="{{ $school_info['longitude'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Phone:</label>
                                                    <input type="text" class="form-control flat"
                                                           readonly
                                                           name="phone" value="{{ $school_info['phone'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">E-mail:</label>
                                                    <input type="text" class="form-control flat"
                                                           readonly
                                                           name="email" value="{{ $school_info['email'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">School Status:</label>
                                                    <input type="text" class="form-control flat"
                                                           readonly
                                                           name="school_status"
                                                           value="{{ $school_info['school_status'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Owner Name:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="owner_name" readonly
                                                           value="{{ $school_info['owner_name'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Owner Phone:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="owner_phone" readonly
                                                           value="{{ $school_info['owner_phone'] }}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="owner_type">Owner Type:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="owner_type" readonly
                                                           value="{{ App\Model\Accr\SourceSchoolOwnership::find($school_info['owner_type']) ? App\Model\Accr\SourceSchoolOwnership::find($school_info['owner_type'])->owner : $school_info['owner_type'] }}">
                                                    {{--<input type="text" class="form-control flat"--}}
                                                    {{--name="owner_type"--}}
                                                    {{--value="{{ $school_info['owner_type'] }}">--}}

                                                </div>
                                                <div class="form-group">
                                                    <label for="owner_email">Owner E-mail:</label>
                                                    <input type="text" class="form-control flat"
                                                           name="owner_email" id="owner_email" readonly
                                                           value="{{ $school_info['owner_email'] }}">

                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ route('school.get.info') }}"
                                                   class="btn btn-warning btn-block btn-flat">
                                                    In case of changes, go to school setting.
                                                </a>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="?step=2"
                                                           class="btn btn-default btn-block btn-flat"> Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </form>

                                </div> <!-- Step One -->
                            @elseif($step == 2)
                                {{--Section 2 : Input--}}
                                <div id="step-2">
                                    <h5><i class="fa fa-check-circle"></i> Upload Attachments</h5>

                                    <table class="table table-bordered table-hover">
                                        <thead style="background: #efefef">
                                        <tr>
                                            <th>Attachment Name</th>
                                            <th style="width: 25%">Upload PDF or Image</th>
                                            <th style="width: 25%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($attachments_data as $ad)
                                            @foreach($ad->accrAttachment as $dr)
                                                <tr>
                                                    <td>{{ $dr->source }}</td>
                                                    <td>
                                                        <a href="{{ asset("storage/".$dr->attachment) }}"
                                                           target="_blank">
                                                            Download File
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <form method="POST"
                                                              action="{{ route('school.accr.input.save') }}">
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="_method"
                                                                   value="delete">
                                                            <input type="hidden" name="form"
                                                                   value="attachments">
                                                            <input type="hidden" name="delete_attachment"
                                                                   value="{{ $dr->id }}">
                                                            <button type="submit"
                                                                    class="btn btn-danger btn-sm btn-flat"
                                                                    onclick="return window.confirm('Are you sure you want to remove this attachment : \n\n {{ $dr->source }} ?')">
                                                                <i class="fa fa-trash"></i> Delete
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="row mb-5">
                                        <form method="POST" action="{{ route('school.accr.input.save') }}"
                                              role="form"
                                              id="attachments_form" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            {!! Form::hidden('step', $step) !!}
                                            <input type="hidden" name="form" value="attachments">
                                            <div class="col-md-6">
                                                <select class="form-control flat"
                                                        name="attachment_name">
                                                    <option value="">Choose Here</option>
                                                    @foreach($attachments_source as $sa)
                                                        <option value="{{ $sa->id }}">{{ $sa->attachment_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               name="upload_file" id="exampleInputFile"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <i class="fa fa-save"
                                                   onclick='$("#attachments_form").trigger("submit")'
                                                   style="font-size: 25px;color: green;cursor: pointer">
                                                    Save</i>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="?step=1"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous
                                                    </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="?step=3"
                                                       class="btn btn-primary btn-block btn-flat"> Next
                                                        <i class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @elseif($step == 3)
                                {{--Section 3 : Input--}}
                                <div id="step-3">
                                    <h5><i class="fa fa-check-circle"></i> Infrastructure and Materials</h5>
                                    <button type="button" class="btn btn-primary btn-flat"
                                            data-toggle="modal" data-target="#materials"><i
                                                class="fa fa-plus"></i> Add Building / Plot
                                    </button>
                                    <div class="modal fade" id="materials">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal">&times;
                                                    </button>
                                                    <h4 class="modal-title"><i class="fa fa-home"></i> Add
                                                        Building / Plot</h4>
                                                </div>
                                                <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                    {{ csrf_field() }}
                                                    {!! Form::hidden('step', $step) !!}
                                                    <input type="hidden" name="form"
                                                           value="buildings_plots">
                                                    <div class="modal-body"
                                                         style="padding: 30px;height: 450px;overflow-y: scroll;background: #f0f1f3">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Infrastructure (Building / Plot)
                                                                        Name</label>
                                                                    <input type="text"
                                                                           class="form-control flat"
                                                                           name="name"
                                                                           placeholder="Infrastructure">
                                                                </div>


                                                                <div class="form-group">
                                                                    <label>Infrastructure (Building / Plot
                                                                        Class)</label>
                                                                    <select class="form-control flat"
                                                                            name="class">
                                                                        <option>Didactive</option>
                                                                        <option>Administrative</option>
                                                                        <option>General</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group row"
                                                                     id="addBuilding">
                                                                    <label class="col-md-12 col-form-label">Infrastructure
                                                                        /Furniture /Equipment</label>
                                                                    <div class="col-md-6">
                                                                        <select class="form-control flat"
                                                                                name="infrastructure[]">
                                                                            <option value="">Choose Here
                                                                            </option>
                                                                            @php
                                                                                $infra_sec = '';
                                                                            @endphp
                                                                            @foreach ($infrastructures_source as $infrastructure)
                                                                                <option value="{{ $infrastructure->id }}">{{ $infrastructure->name }}</option>
                                                                                @php
                                                                                    $infra_sec .= "<option value=".$infrastructure['id'].">". $infrastructure['name'] ."</option>";
                                                                                @endphp
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <input type="number"
                                                                               class="form-control flat"
                                                                               name="quantity[]"
                                                                               placeholder="Quantity">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <button type="button" class="btn btn-info"
                                                                                onclick="addBuilding()"><i
                                                                                    class="fa fa-plus"></i></button>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Purpose</label>
                                                                    <input type="text"
                                                                           class="form-control flat"
                                                                           name="purpose">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Size</label>
                                                                    <input type="text"
                                                                           class="form-control flat"
                                                                           name="size">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Capacity</label>
                                                                    <input type="text"
                                                                           class="form-control flat"
                                                                           name="capacity">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Construction Materials</label>
                                                                    <input type="text"
                                                                           class="form-control flat"
                                                                           name="construction_materials">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Roofing Materials</label>
                                                                    <input type="text"
                                                                           class="form-control flat"
                                                                           name="roofing_materials">
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for=""
                                                                           class="col-md-5 col-form-label">Harvests
                                                                        Rain Water</label>
                                                                    <div class="col-md-3">
                                                                        <select class="form-control flat"
                                                                                name="harvests_rain_water">
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Water</label>
                                                                    <div class="col-md-3">
                                                                        <select class="form-control flat"
                                                                                name="has_water">
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Electricity</label>
                                                                    <div class="col-md-3">
                                                                        <select class="form-control flat"
                                                                                name="has_electricity">
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Internet</label>
                                                                    <div class="col-md-3">
                                                                        <select class="form-control flat"
                                                                                name="has_internet">
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Extinguisher</label>
                                                                    <div class="col-md-3">
                                                                        <select class="form-control flat"
                                                                                name="has_extinguisher">
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Lightening Arrestor</label>
                                                                    <div class="col-md-3">
                                                                        <select class="form-control flat"
                                                                                name="has_lightening_arrestor">
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        External Lighting</label>
                                                                    <div class="col-md-3">
                                                                        <select class="form-control flat"
                                                                                name="has_external_lighting">
                                                                            <option>Yes</option>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                {{--<div class="form-group">--}}
                                                                {{--<label>Furniture</label>--}}
                                                                {{--<input type="text" class="form-control flat" name="furniture" placeholder="Furniture">--}}
                                                                {{--</div>--}}

                                                                {{--<div class="form-group">--}}
                                                                {{--<label>Equipment</label>--}}
                                                                {{--<input type="text" class="form-control flat" name="equipment" placeholder="Equipment">--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer" style="text-align: center;">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <button type="submit"
                                                                        class="btn btn-success btn-block btn-flat">
                                                                    <i class="fa fa-save"></i> Save
                                                                </button>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <button type="button"
                                                                        class="btn btn-default btn-block btn-flat"
                                                                        data-dismiss="modal"><i
                                                                            class="fa fa-close"></i> Cancel
                                                                </button>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div id="form-step-2" role="form" data-toggle="">
                                        <table class="table table-bordered table-hover">
                                            <thead style="background: #efefef">
                                            <tr>
                                                <th>#</th>
                                                <th>Building</th>
                                                <th>Infrastructure</th>
                                                <th>Purpose</th>
                                                <th>Size</th>
                                                <th>Capacity</th>
                                                <th style="width: 120px;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($buildings_plots as $bp)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $bp->name }}</td>
                                                    <td>

                                                        @foreach ($bp['infras'] as $infra)
                                                            {{ $infra->quantity. ' ' .$infra->name .  ', ' }}
                                                        @endforeach

                                                    </td>
                                                    <td>{{ $bp->purpose }}</td>
                                                    <td>{{ $bp->size }}</td>
                                                    <td>{{ $bp->capacity }}</td>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-warning btn-flat btn-sm"
                                                                data-toggle="modal"
                                                                data-target="#edit_{{ $bp->id }}"><i
                                                                    class="fa fa-tasks"></i> View More /
                                                            Edit
                                                        </button>

                                                        <div class="modal fade" id="edit_{{ $bp->id }}">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">&times;
                                                                        </button>
                                                                        <h4 class="modal-title"><i
                                                                                    class="fa fa-home"></i>
                                                                            View / Edit Building Information
                                                                        </h4>
                                                                    </div>
                                                                    <form method="POST"
                                                                          action="{{ route('school.accr.input.save') }}">
                                                                        {{ csrf_field() }}
                                                                        {!! Form::hidden('step', $step) !!}
                                                                        <input type="hidden" name="form"
                                                                               value="edit_buildings_plots">
                                                                        <input type="hidden" name="bp_id"
                                                                               value="{{ $bp->id }}">
                                                                        <div class="modal-body"
                                                                             style="padding: 30px;height: 450px;overflow-y: scroll;background: #f0f1f3">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Building /
                                                                                            Plot
                                                                                            Name</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="name"
                                                                                               placeholder="Building / Plot Name"
                                                                                               value="{{ $bp->name }}">
                                                                                    </div>


                                                                                    <div class="form-group">
                                                                                        <label>Building /
                                                                                            Plot
                                                                                            Class</label>
                                                                                        <select class="form-control flat"
                                                                                                name="class">
                                                                                            <option @if($bp->class == 'Didactive') selected @endif>
                                                                                                Didactive
                                                                                            </option>
                                                                                            <option @if($bp->class == 'Administrative') selected @endif>
                                                                                                Administrative
                                                                                            </option>
                                                                                            <option @if($bp->class == 'General') selected @endif>
                                                                                                General
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>

                                                                                    <div class="form-group row"
                                                                                         id="addBuilding2">
                                                                                        <label class="col-md-12 col-form-label">Infrastructure</label>
                                                                                        @foreach ($bp['infras'] as $infra)
                                                                                            <div class="col-md-6">
                                                                                                <select class="form-control flat"
                                                                                                        name="infrastructure[]">
                                                                                                    <option value="">
                                                                                                        Choose
                                                                                                        Here
                                                                                                    </option>
                                                                                                    @foreach ($infrastructures_source as $infrastructure)
                                                                                                        <option value="{{ $infrastructure->id }}"
                                                                                                                @if($infrastructure->id  == $infra->infrastructure_id ) selected @endif >{{ $infrastructure->name }}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-5">
                                                                                                <input type="number"
                                                                                                       class="form-control flat"
                                                                                                       name="quantity[]"
                                                                                                       placeholder="Quantity"
                                                                                                       value="{{ $infra->quantity }}">
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <button type="button"
                                                                                                        class="btn btn-info"
                                                                                                        onclick="addBuilding2()">
                                                                                                    <i class="fa fa-plus"></i>
                                                                                                </button>
                                                                                            </div>
                                                                                        @endforeach

                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Purpose</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="purpose"
                                                                                               value="{{ $bp->purpose }}">
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Size</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="size"
                                                                                               value="{{ $bp->size }}">
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Capacity</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="capacity"
                                                                                               value="{{ $bp->capacity }}">
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Construction
                                                                                            Materials</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="construction_materials"
                                                                                               value="{{ $bp->construction_materials }}">
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Roofing
                                                                                            Materials</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="roofing_materials"
                                                                                               value="{{ $bp->roofing_materials }}">
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label for=""
                                                                                               class="col-md-5 col-form-label">Harvests
                                                                                            Rain
                                                                                            Water</label>
                                                                                        <div class="col-md-3">
                                                                                            <select class="form-control flat"
                                                                                                    name="harvests_rain_water">
                                                                                                <option @if($bp->harvests_rain_water == 'Yes') selected @endif>
                                                                                                    Yes
                                                                                                </option>
                                                                                                <option @if($bp->harvests_rain_water == 'No') selected @endif>
                                                                                                    No
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Water</label>
                                                                                        <div class="col-md-3">
                                                                                            <select class="form-control flat"
                                                                                                    name="has_water">
                                                                                                <option @if($bp->has_water == 'Yes') selected @endif>
                                                                                                    Yes
                                                                                                </option>
                                                                                                <option @if($bp->has_water == 'No') selected @endif>
                                                                                                    No
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Electricity</label>
                                                                                        <div class="col-md-3">
                                                                                            <select class="form-control flat"
                                                                                                    name="has_electricity">
                                                                                                <option @if($bp->has_electricity == 'Yes') selected @endif>
                                                                                                    Yes
                                                                                                </option>
                                                                                                <option @if($bp->has_electricity == 'No') selected @endif>
                                                                                                    No
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Internet</label>
                                                                                        <div class="col-md-3">
                                                                                            <select class="form-control flat"
                                                                                                    name="has_internet">
                                                                                                <
                                                                                                <option @if($bp->has_internet == 'Yes') selected @endif>
                                                                                                    Yes
                                                                                                </option>
                                                                                                <option @if($bp->has_internet == 'No') selected @endif>
                                                                                                    No
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Extinguisher</label>
                                                                                        <div class="col-md-3">
                                                                                            <select class="form-control flat"
                                                                                                    name="has_extinguisher">
                                                                                                <option @if($bp->has_extinguisher == 'Yes') selected @endif>
                                                                                                    Yes
                                                                                                </option>
                                                                                                <option @if($bp->has_extinguisher == 'No') selected @endif>
                                                                                                    No
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Lightening
                                                                                            Arrestor</label>
                                                                                        <div class="col-md-3">
                                                                                            <select class="form-control flat"
                                                                                                    name="has_lightening_arrestor">
                                                                                                <option @if($bp->has_lightening_arrestor == 'Yes') selected @endif>
                                                                                                    Yes
                                                                                                </option>
                                                                                                <option @if($bp->has_lightening_arrestor == 'No') selected @endif>
                                                                                                    No
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            External
                                                                                            Lighting</label>
                                                                                        <div class="col-md-3">
                                                                                            <select class="form-control flat"
                                                                                                    name="has_external_lighting">
                                                                                                <option @if($bp->has_external_lighting == 'Yes') selected @endif>
                                                                                                    Yes
                                                                                                </option>
                                                                                                <option @if($bp->has_external_lighting == 'No') selected @endif>
                                                                                                    No
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Furniture</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="furniture"
                                                                                               placeholder="Furniture"
                                                                                               value="{{ $bp->furniture }}">
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Equipment</label>
                                                                                        <input type="text"
                                                                                               class="form-control flat"
                                                                                               name="equipment"
                                                                                               placeholder="Equipment"
                                                                                               value="{{ $bp->equipment }}">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer"
                                                                             style="text-align: center;">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <button type="submit"
                                                                                            class="btn btn-success btn-block btn-flat">
                                                                                        <i class="fa fa-save"></i>
                                                                                        Save
                                                                                    </button>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <button type="button"
                                                                                            class="btn btn-default btn-block btn-flat"
                                                                                            data-dismiss="modal">
                                                                                        <i class="fa fa-close"></i>
                                                                                        Cancel
                                                                                    </button>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>

                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a href="?step=2"
                                                           class="btn btn-default btn-block btn-flat"> <i
                                                                    class="fa fa-arrow-left"></i> Previous
                                                        </a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a href="?step=4"
                                                           class="btn btn-primary btn-block btn-flat"> Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif($step == 4)
                                {{--Section 4 : Input--}}
                                <div id="step-4">
                                    <h5><i class="fa fa-check-circle"></i> Programs Offered</h5>

                                    @if (isset($program))
                                        {{-- <h5 style="color: green"><i class="fa fa-edit"></i> Edit Program
                                            Offered</h5> --}}
                                        {{-- <form method="POST" action="{{ route('school.accr.input.save') }}">
                                            {{ csrf_field() }}
                                            {!! Form::hidden('step', $step) !!}
                                            <input type="hidden" name="form" value="programs">
                                            <input type="hidden" name="po_id" value="{{ $program->id }}">

                                            <table class="table">
                                                <tr>

                                                    <td>
                                                        <select class="form-control flat" name="sector_id"
                                                                id="sector3">
                                                            @foreach ($sectors as $sector)
                                                                <option value="{{ $sector->id }}"
                                                                        @if($sector->id == $program->subsector->sector->id) selected @endif>{{ $sector->tvet_field }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>

                                                    <td>
                                                        <select class="form-control flat"
                                                                name="sub_sector_id" id="sub_sectors3">
                                                            @foreach ($program->subsector->sector->subsectors as $subsector)
                                                                <option value="{{ $subsector->id }}"
                                                                        @if($subsector->id == $program->sub_sector_id) selected @endif >{{ $subsector->sub_field_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control flat"
                                                                name="qualification_id">
                                                            <option value="">Choose Level</option>
                                                            @foreach ($curriculum_qualifications as $qualification)
                                                                <option value="{{ $qualification->id }}"
                                                                        @if($qualification->id == $program->qualification_id) selected @endif >{{ $qualification->qualification_title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control flat"
                                                                name="rtqf_level_id">
                                                            @foreach($rtqfs as $rtqf)
                                                                <option value="{{ $rtqf->id }}">{{ $rtqf->level_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control flat"
                                                               name="male_trainees"
                                                               placeholder="Male Trainees"
                                                               value="{{ $program->male_trainees }}">
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control flat"
                                                               name="female_trainees"
                                                               placeholder="Female Trainees"
                                                               value="{{ $program->female_trainees }}">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control flat"
                                                               name="curriculum" placeholder="Curriculum"
                                                               value="{{ $program->curriculum }}">
                                                    </td>
                                                    <td>
                                                        <button type="submit"
                                                                class="btn btn-success btn-flat"><i
                                                                    class="fa fa-save"></i> Save
                                                        </button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>--}}
                                    @endif

                                    <table class="table table-bordered table-hover" id="dataTableBtn">
                                        <thead style="background: #efefef">
                                        <tr>
                                            <th>#</th>

                                            <th>Sector</th>
                                            <th style="width: 15%">Sub-Sector</th>
                                            <th>Qualification</th>
                                            <th style="width: 15%">RTQF Level</th>
                                            <th style="width: 10%">Male Trainees</th>
                                            <th style="width: 10%">Female Trainees</th>
                                            {{--<th>Curriculum</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($programs_offered as $pd)

                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $pd->qualification->subsector->sector->tvet_field or "" }}</td>
                                                <td>{{ $pd->qualification->subsector->sub_field_name or ""  }}</td>
                                                <td>{{ $pd->qualification->qualification_title or "" }}</td>
                                                <td>{{ $pd->qualification->rtqf->level_name or "" }}</td>
                                                <td>{{ $pd->qualification ? getStudentsNber($pd->qualification->rtqf->id, $pd->qualification_id, $pd->school_id, [['key' => 'gender', 'operator' => '=', 'needed' => ucwords('male')]]) : "" }}</td>
                                                <td>{{ $pd->qualification ? getStudentsNber($pd->qualification->rtqf->id, $pd->qualification_id, $pd->school_id, [['key' => 'gender', 'operator' => '=', 'needed' => ucwords('female')]]) : "" }}</td>
                                                {{--                                                <td>{{ $pd->curriculum }}</td>--}}
                                                {{-- <td width="80px">
                                                    <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="_method" value="delete">

                                                        <input type="hidden" name="form" value="programs">
                                                        <input type="hidden" name="delete_program"
                                                               value="{{ $pd->id }}">

                                                        <a href="/school/accreditation/input/programs/edit/{{ $pd->id }}/?step=4"
                                                           class="btn btn-primary btn-sm btn-flat"><i
                                                                    class="fa fa-edit"></i></a>

                                                        <button type="submit"
                                                                class="btn btn-danger btn-sm btn-flat"
                                                                onclick="return window.confirm('Are you sure you want to remove this program?')">
                                                            <i class="fa fa-trash"></i>
                                                        </button>

                                                    </form>
                                                </td> --}}
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>

                                    <div class="row mb-5">
                                        {{-- <form method="POST" action="{{ route('school.accr.input.save') }}"
                                              id="programs_forms">
                                            {{ csrf_field() }}
                                            {!! Form::hidden('step', $step) !!}
                                            <input type="hidden" name="form" value="programs">

                                            <div class="col-md-2">
                                                <select class="form-control flat" name="sector_id"
                                                        id="sectorDamo">
                                                    <option value="">Choose Here</option>
                                                    @foreach($sectors as $sector)
                                                        <option value="{{ $sector->id }}">{{ $sector->tvet_field }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control flat" name="sub_sector_id"
                                                        id="sub_sectorsDamo">

                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control flat"
                                                        name="qualification_id" required
                                                        id="curr_qualifications2" required>

                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control flat" name="rtqf_level_id">
                                                    @foreach($rtqfs as $rtqf)
                                                        <option value="{{ $rtqf->id }}">{{ $rtqf->level_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <input type="number" class="form-control flat"
                                                       name="male_trainees">
                                            </div>
                                            <div class="col-md-1">
                                                <input type="number" class="form-control flat"
                                                       name="female_trainees">
                                            </div>
                                            <div class="col-md-1">
                                                <input type="text" class="form-control flat"
                                                       name="curriculum">
                                            </div>
                                            <div class="col-md-1 text-center"><i class="fa fa-save"
                                                                                 onclick='$("#programs_forms").trigger("submit")'
                                                                                 style="font-size: 25px;color: green;cursor: pointer"></i>
                                            </div>
                                        </form> --}}
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="?step=3"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="?step=5"
                                                       class="btn btn-primary btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @elseif($step == 5)
                                {{--Section 5 : Input--}}
                                <div id="step-5">
                                    <h5><i class="fa fa-check-circle"></i> Trainers / Teachers</h5>

                                    <a href="{{ route('school.staff.create') }}" class="btn btn-primary btn-flat"><i
                                                class="fa fa-plus"></i> Add Trainer Information</a>
                                    <br><br>
                                    <div class="modal fade" id="trainer">
                                        {{-- <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal">&times;
                                                    </button>
                                                    <h4 class="modal-title"><i class="fa fa-home"></i> Add Tranier Information</h4>
                                                </div>
                                                <form method="POST" action="{{ route('school.accr.input.save') }}" id="teachers_forms">
                                                        {{ csrf_field() }}
                                                        {!! Form::hidden('step', $step) !!}
                                                        <input type="hidden" name="form" value="teachers" placeholder="Name" required>
                                                <div class="modal-body" style="padding: 30px;height: 450px;overflow-y: scroll;background: #f0f1f3">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Trainer Name</label>
                                                                <input type="text" class="form-control flat" name="names" placeholder="Trainer Name" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Gender</label>
                                                                <select class="form-control flat" name="gender">
                                                                    <option>Male</option>
                                                                    <option>Female</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Level / Degree</label>
                                                                <select class="form-control flat" name="qualification_level">
                                                                    @foreach ($qualification_levels as $level)
                                                                        <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Qualification</label>
                                                                <input type="text" class="form-control flat" name="qualification" placeholder="Qualification">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Certification</label>
                                                                <select class="form-control flat" name="certification">
                                                                    @foreach($certications as $certication)
                                                                        <option value="{{$certication->id}}">{{ $certication->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Experience</label>
                                                                <input type="text" class="form-control flat" name="experience" placeholder="Experience">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Modules Taught</label>
                                                                <input type="text" class="form-control flat" name="modules_taught" placeholder="Modules Taught">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer" style="text-align: center;">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <button type="submit" class="btn btn-success btn-block btn-flat"> <i class="fa fa-save"></i> Save </button>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-default btn-block btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Cancel </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div> --}}
                                    </div>

                                    @if (isset($trainer))
                                        <h5 style="color: green"><i class="fa fa-edit"></i> Edit Trainer
                                            Information</h5>

                                        {{-- <form method="POST" action="{{ route('school.accr.input.save') }}">
                                            {{ csrf_field() }}
                                            {!! Form::hidden('step', $step) !!}
                                            <input type="hidden" name="form" value="teachers">
                                            <input type="hidden" name="trainer_id"
                                                   value="{{ $trainer->id }}">

                                            <table class="table">
                                                <tr>
                                                    <td><input type="text" class="form-control flat"
                                                               name="names" value="{{ $trainer->names }}">
                                                    </td>
                                                    <td>
                                                        <select class="form-control flat" name="gender">
                                                            <option @if($trainer->gender == 'Male') selected @endif>
                                                                Male
                                                            </option>
                                                            <option @if($trainer->gender == 'Female') selected @endif>
                                                                Female
                                                            </option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control flat"
                                                                name="qualification_level">
                                                            <option value="">Choose Level</option>
                                                            @foreach ($qualification_levels as $level)
                                                                <option value="{{ $level->id }}"
                                                                        @if($level->id == $trainer->qualification_level_id) selected @endif >{{ $level->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input type="text" class="form-control flat"
                                                               name="qualification"
                                                               placeholder="Qualification"
                                                               value="{{ $trainer->qualification }}"></td>
                                                    <td>
                                                        <select class="form-control flat"
                                                                name="certification">
                                                            @foreach($certications as $certication)
                                                                <option value="{{$certication->id}}"
                                                                        @if($certication->id == $trainer->certificationId) selected @endif >{{ $certication->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input type="text" class="form-control flat"
                                                               name="experience" placeholder="Experience"
                                                               value="{{ $trainer->experience }}"></td>
                                                    <td><input type="text" class="form-control flat"
                                                               name="modules_taught"
                                                               placeholder="Modules Taught"
                                                               value="{{ $trainer->modules_taught }}"></td>
                                                    <td class="text-center">
                                                        <button type="submit"
                                                                class="btn btn-success btn-flat"><i
                                                                    class="fa fa-save"></i> Save
                                                        </button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form> --}}
                                    @endif

                                    <table class="table table-bordered table-hover" id="dataTableBtn">
                                        <thead style="background: #efefef">
                                        <tr>
                                            <th></th>
                                            <th>Photo</th>
                                            <th>Name</th>
                                            <th style="width: 10%">Gender</th>
                                            {{--<th>Level / Degree</th>--}}
                                            <th>Qualification</th>
                                            <th>Institution</th>
                                            <th>Graduated Year</th>
                                            <th>Attended RTTI</th>
                                            {{--<th>Certification</th>--}}
                                            {{--<th>Experience</th>--}}
                                            {{--<th>Modules Taught</th>--}}
                                            {{-- <th></th> --}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($trainers as $td)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>
                                                    <img src="{{ getStaffPhoto($td->photo) }}" alt=""
                                                         class="img-responsive img-rounded" style="width: 90px;">
                                                </td>
                                                <td>{{ $td->names }}</td>
                                                <td>{{ $td->gender }}</td>
                                                {{--<td>--}}
                                                {{--@if($td->qualifications)--}}
                                                {{--@php--}}
                                                {{--$degree = "";--}}
                                                {{--@endphp--}}
                                                {{--@foreach($td->qualifications as $quall)--}}
                                                {{--@php--}}
                                                {{--$degree .= ",".$quall->qualification_level;--}}
                                                {{--@endphp--}}
                                                {{--@endforeach--}}
                                                {{--@endif--}}
                                                {{--</td>--}}
                                                <td>
                                                    {{ $td->qualification }}
                                                </td>
                                                <td>
                                                    {{ $td->institution }}
                                                </td>
                                                <td>{{ $td->graduated_year }}</td>
                                                <td>{{ $td->attended_rtti }}</td>
                                                {{--<td>{{ $td->certiName }}</td>--}}
                                                {{--<td>{{ $td->experience }}</td>--}}
                                                {{--<td>{{ $td->modules_taught }}</td>--}}
                                                {{-- <td>
                                                    <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="_method" value="delete">
                                                        <input type="hidden" name="form" value="teachers">
                                                        <input type="hidden" name="delete_trainer"
                                                               value="{{ $td->id }}">

                                                        <button type="submit"
                                                                class="btn btn-danger btn-sm btn-flat"
                                                                onclick="return window.confirm('Are you sure you want to remove this trainer : \n\n {{ $td->names }} ?')">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </button>
                                                    </form>
                                                </td> --}}
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                    {{-- <form method="POST" action="{{ route('school.accr.input.save') }}"
                                          id="teachers_forms">
                                        {{ csrf_field() }}
                                        {!! Form::hidden('step', $step) !!}
                                        <input type="hidden" name="form" value="teachers"
                                               placeholder="Name" required>

                                        <div class="row mb-5">
                                            <div class="col-md-1">
                                                <input type="text" class="form-control flat"
                                                       name="names" required>
                                            </div>
                                            <div class="col-md-1">
                                                <select class="form-control flat" name="gender">
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <select class="form-control flat"
                                                        name="qualification_level">
                                                    <option value="">Choose Level</option>
                                                    @foreach ($qualification_levels as $level)
                                                        <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control flat"
                                                       name="qualification" placeholder="Qualification">
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control flat" name="certification">
                                                    @foreach($certications as $certication)
                                                        <option value="{{$certication->id}}">{{ $certication->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control flat"
                                                       name="experience" placeholder="Experience">
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control flat"
                                                       name="modules_taught"
                                                       placeholder="Modules Taught">
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <button type="submit" class="btn btn-success btn-flat">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                            </div>
                                        </div>
                                    </form> --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="?step=4"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="?step=6"
                                                       class="btn btn-primary btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @elseif($step == 6)
                                {{--Section 6 : Input--}}
                                <div id="step-6">
                                    <h5><i class="fa fa-check-circle"></i> Administration Staff</h5>
                                    <a href="{{ route('school.staff.create') }}" class="btn btn-primary btn-flat"><i
                                                class="fa fa-plus"></i> Add Staff Information</a>
                                    <br><br>
                                    {{-- <div class="modal fade" id="staff">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal">&times;
                                                    </button>
                                                    <h4 class="modal-title"><i class="fa fa-home"></i> Add Staff Information</h4>
                                                </div>
                                                <form method="POST" action="{{ route('school.accr.input.save') }}" id="staffs_form">
                                                    {{ csrf_field() }}
                                                    {!! Form::hidden('step', $step) !!}
                                                <div class="modal-body" style="padding: 30px;height: 450px;overflow-y: scroll;background: #f0f1f3">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Staff Name</label>
                                                                <input type="text" class="form-control flat" name="names" placeholder="Staff Name" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Gender</label>
                                                                <select class="form-control flat" name="gender">
                                                                    <option>Male</option>
                                                                    <option>Female</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Position</label>
                                                                <input type="text" class="form-control flat" name="position" placeholder="Position">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Qualification</label>
                                                                <input type="text" class="form-control flat" name="academic_qualification" placeholder="Qualification">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Institution Studied</label>
                                                                <input type="text" class="form-control flat" name="institution_studied" placeholder="Institution Studied">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Qualification Level</label>
                                                                <select class="form-control flat" name="qualification_level">
                                                                    @foreach ($qualification_levels as $level)
                                                                        <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer" style="text-align: center;">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <button type="submit" class="btn btn-success btn-block btn-flat"> <i class="fa fa-save"></i> Save </button>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-default btn-block btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Cancel </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div> --}}

                                    @if (isset($staff))
                                        {{-- <form method="POST" action="{{ route('school.accr.input.save') }}">
                                            {{ csrf_field() }}
                                            {!! Form::hidden('step', $step) !!}
                                            <input type="hidden" name="form" value="staff_members">
                                            <input type="hidden" name="staff_id"
                                                   value="{{ $staff->id }}">
                                            <div class="row mb-1">
                                                <div class="col-md-2"><input type="text" class="form-control flat"
                                                                             name="names" placeholder="Names"
                                                                             value="{{ $staff->names }}"></div>
                                                <div class="col-md-2">
                                                    <select class="form-control flat" name="gender">
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2"><input type="text" class="form-control flat"
                                                                             name="position" placeholder="Position"
                                                                             value="{{ $staff->position }}" required>
                                                </div>
                                                <div class="col-md-2"><input type="text" class="form-control flat"
                                                                             name="academic_qualification"
                                                                             value="{{ $staff->academic_qualification }}"
                                                                             placeholder="Qualification"></div>
                                                <div class="col-md-2"><input type="text" class="form-control flat"
                                                                             name="institution_studied"
                                                                             value="{{ $staff->institution_studied }}"
                                                                             placeholder="Institution Studied"></div>
                                                <div class="col-md-2">
                                                    <select class="form-control flat"
                                                            name="qualification_level">
                                                        <option value="">Choose Level</option>
                                                        @foreach ($qualification_levels as $level)
                                                            <option value="{{ $level->id }}"
                                                                    @if($level->id == $staff->qualification_level_id ) selected @endif>{{ $level->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                {{-- <div class="col-md-2">
                                                    <input type="text" class="form-control flat"
                                                           name="teaching_experience"
                                                           value="{{ $staff->teaching_experience }}"
                                                           placeholder="Teaching Experience"></div>
                                                <div class="col-md-2"><input type="text" class="form-control flat"
                                                                             name="assessor_qualification"
                                                                             value="{{ $staff->assessor_qualification }}"
                                                                             placeholder="Assessor Qualification"></div> --}}
                                        <div class="col-md-1">
                                            <button type="submit"
                                                    class="btn btn-success btn-flat"><i
                                                        class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                </div>
                                </form> --}}
                            @endif

                            <table class="table table-bordered table-hover">
                                <thead style="background: #efefef">
                                <tr>
                                    <th></th>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th style="width: 10%">Gender</th>
                                    {{--<th>Level / Degree</th>--}}
                                    <th>Qualification</th>
                                    <th>Institution</th>
                                    <th>Graduated Year</th>
                                    <th>Attended RTTI</th>
                                    {{-- <th>Teaching Experience</th>
                                    <th>Assessor Qualification</th> --}}
                                    {{-- <th></th> --}}
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @forelse($staffs as $sd)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <img src="{{ getStaffPhoto($sd->photo) }}" alt=""
                                                 class="img-responsive img-rounded" style="width: 90px;">
                                        </td>
                                        <td>{{ $sd->names }}</td>
                                        <td>{{ $sd->gender }}</td>
                                        {{--<td>--}}
                                        {{--@if($td->qualifications)--}}
                                        {{--@php--}}
                                        {{--$degree = "";--}}
                                        {{--@endphp--}}
                                        {{--@foreach($td->qualifications as $quall)--}}
                                        {{--@php--}}
                                        {{--$degree .= ",".$quall->qualification_level;--}}
                                        {{--@endphp--}}
                                        {{--@endforeach--}}
                                        {{--@endif--}}
                                        {{--</td>--}}
                                        <td>
                                            {{ $sd->qualification }}
                                        </td>
                                        <td>
                                            {{ $sd->institution }}
                                        </td>
                                        <td>{{ $sd->graduated_year }}</td>
                                        <td>{{ $sd->attended_rtti }}</td>
                                        {{--<td>{{ $td->certiName }}</td>--}}
                                        {{--<td>{{ $td->experience }}</td>--}}
                                        {{--<td>{{ $td->modules_taught }}</td>--}}
                                        {{-- <td>
                                            <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="_method" value="delete">
                                                <input type="hidden" name="form" value="teachers">
                                                <input type="hidden" name="delete_trainer"
                                                       value="{{ $td->id }}">

                                                <button type="submit"
                                                        class="btn btn-danger btn-sm btn-flat"
                                                        onclick="return window.confirm('Are you sure you want to remove this trainer : \n\n {{ $td->names }} ?')">
                                                    <i class="fa fa-trash"></i> Delete
                                                </button>
                                            </form>
                                        </td> --}}
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="10" class="text-center">Empty !!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {{-- <form method="POST" action="{{ route('school.accr.input.save') }}" id="staffs_form">
                                {{ csrf_field() }}
                                {!! Form::hidden('step', $step) !!}
                                <input type="hidden" name="form" value="staff_members" required>
                                <div class="row mb-1">
                                    <div class="col-md-2"><input type="text" class="form-control flat"
                                                                 name="names" placeholder="Names"></div>
                                    <div class="col-md-2">
                                        <select class="form-control flat" name="gender">
                                            <option>Male</option>
                                            <option>Female</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2"><input type="text" class="form-control flat"
                                                                 name="position" placeholder="Position"
                                                                 required>
                                    </div>
                                    <div class="col-md-2"><input type="text" class="form-control flat"
                                                                 name="academic_qualification"
                                                                 placeholder="Qualification"></div>
                                    <div class="col-md-2"><input type="text" class="form-control flat"
                                                                 name="institution_studied"
                                                                 placeholder="Institution Studied"></div>
                                    <div class="col-md-2">
                                        <select class="form-control flat"
                                                name="qualification_level">
                                            <option value="">Choose Level</option>
                                            @foreach ($qualification_levels as $level)
                                                <option value="{{ $level->id }}">{{ $level->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-md-2">
                                        <input type="text" class="form-control flat"
                                               name="teaching_experience"
                                               placeholder="Teaching Experience">
                                    </div>
                                    <div class="col-md-2"><input type="text" class="form-control flat"
                                                                 name="assessor_qualification"
                                                                 placeholder="Assessor Qualification"></div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-success btn-flat">
                                            <i class="fa fa-save"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form> --}}

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="?step=5"
                                               class="btn btn-default btn-block btn-flat"> <i
                                                        class="fa fa-arrow-left"></i> Previous </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="?step=7"
                                               class="btn btn-primary btn-block btn-flat"> Next <i
                                                        class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @elseif($step == 7)
                            {{--Section 8 : Input--}}
                            <div id="step-7">
                                <h5><i class="fa fa-check-circle"></i> Qualifications Applied for</h5>

                                <table class="table table-bordered table-hover">
                                    <thead style="background: #efefef">
                                    <tr>
                                        <th></th>
                                        <th style="width: 30%">Sector</th>
                                        <th style="width: 20%">Sub Sector</th>
                                        <th style="width: 20%">Curriculum Qualification</th>
                                        <th>RTQF Level</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($application_data as $applies)
                                        @if($applies->accrQualificationsApplied->count() > 0)
                                            @foreach($applies->accrQualificationsApplied as $ad)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $ad->curr->subsector->sector->tvet_field or "" }}</td>
                                                    <td>{{ $ad->curr->subsector->sub_field_name or "" }}</td>
                                                    <td>{{ $ad->curr->qualification_title or "" }}</td>
                                                    <td>{{ $ad->rtqf->level_name or "" }}</td>
                                                    <td>
                                                        <form method="POST"
                                                              action="{{ route('school.accr.input.save') }}">
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="_method"
                                                                   value="delete">
                                                            <input type="hidden" name="form"
                                                                   value="apply">
                                                            <input type="hidden"
                                                                   name="delete_application"
                                                                   value="{{ $ad->id }}">

                                                            <button type="submit"
                                                                    class="btn btn-danger btn-sm btn-flat"
                                                                    onclick="return window.confirm('Are you sure you want to remove this trainer : \n\n {{ $ad->names }} ?')">
                                                                <i class="fa fa-trash"></i> Delete
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                                <form method="POST" action="{{ route('school.accr.input.save') }}"
                                      id="apply_form">
                                    {{ csrf_field() }}
                                    {!! Form::hidden('step', $step) !!}
                                    <input type="hidden" name="form" value="apply">
                                    <div class="row mb-4">
                                        <div class="col-md-3">
                                            <select class="form-control flat" name="sector_id"
                                                    id="sector2" required>
                                                <option value="" selected disabled>Choose Sector</option>
                                                @foreach($sectors as $sector)
                                                    <option value="{{ $sector->id }}">{{ $sector->tvet_field }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control flat" name="sub_sector_id"
                                                    id="sub_sectors2" required>
                                                <option value="" selected disabled>Choose Sub Sector</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control flat"
                                                    name="curriculum_qualification_id"
                                                    id="curr_qualifications" required>
                                                <option value="" selected disabled>Curriculum Qualification</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control flat" name="rtqf_level_id" required>
                                                <option value="" selected disabled>RTQF Level</option>
                                                @foreach($rtqfs as $rtqf)
                                                    <option value="{{ $rtqf->id }}">{{ $rtqf->level_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <button type="submit" class="btn btn-success btn-flat">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="?step=7"
                                                   class="btn btn-default btn-block btn-flat"> <i
                                                            class="fa fa-arrow-left"></i> Previous </a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="?step=8"
                                                   class="btn btn-primary btn-block btn-flat"> Next
                                                    Section <i class="fa fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif($step == 8)
                            {{--Section 10 : Input--}}
                            <div id="step-8">
                                <h5><i class="fa fa-check-circle"></i> Confirmation</h5>

                                <p>Please Confirm Below Information</p>

                                <h5>
                                    <i class="fa fa-check-circle @if($errors->has('accreditation_type_id')) text-danger @endif"></i>Accreditation
                                    Type</h5>
                                <div class="p-1 mb-3">
                                    <div class="form-group @if($errors->has('accreditation_type_id')) has-error @endif">
                                        <select id="accreditation_type_select"
                                                @if($errors->has('accreditation_type_id'))
                                                autofocus
                                                @endif
                                                class="form-control select2" required>
                                            <option value="" selected disabled>
                                                Select Accreditation Type
                                            </option>
                                            @if($accreditation_types)
                                                @foreach($accreditation_types as $accreditation_type)
                                                    <option value="{{ $accreditation_type->id }}">
                                                        {{ ucwords($accreditation_type->type) }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <h5><i class="fa fa-check-circle"></i> Attachments Provided</h5>
                                <table class="table table-bordered table-hover">
                                    <thead style="background: #efefef">
                                    <tr>
                                        <th>Attachment Name</th>
                                        <th style="width: 25%">Upload PDF or Image</th>
                                        <th style="width: 25%"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($attachments_data as $ads)
                                        @foreach($ads->accrAttachment as $ad)
                                            <tr>
                                                <td>{{ $ad->source }}</td>
                                                <td>
                                                    <a href="{{ Storage::url($ad->attachment) }}" target="_blank">Download
                                                        File</a></td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @endforeach

                                    </tbody>
                                </table>

                                <h5><i class="fa fa-check-circle"></i> Materials &amp; Infrastructure
                                </h5>
                                <table class="table table-bordered table-hover">
                                    <thead style="background: #efefef">
                                    <tr>
                                        <th>#</th>
                                        <th>Building</th>
                                        <th>Infrastructure</th>
                                        <th>Purpose</th>
                                        <th>Size</th>
                                        <th>Capacity</th>
                                        <th style="width: 120px;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($buildings_plots as $bp)
                                        <tr>
                                            <td></td>
                                            <td>{{ $bp->name }}</td>
                                            <td>

                                                @foreach ($bp['infras'] as $infra)
                                                    {{ $infra->quantity. ' ' .$infra->name .  ', ' }}
                                                @endforeach

                                            </td>
                                            <td>{{ $bp->purpose }}</td>
                                            <td>{{ $bp->size }}</td>
                                            <td>{{ $bp->capacity }}</td>
                                            <td>
                                                <button type="button"
                                                        class="btn btn-warning btn-flat btn-sm"
                                                        data-toggle="modal"
                                                        data-target="#view_{{ $bp->id }}"><i
                                                            class="fa fa-tasks"></i> View More
                                                </button>

                                                <div class="modal fade" id="view_{{ $bp->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title"><i
                                                                            class="fa fa-home"></i> View
                                                                    Building Information</h4>
                                                            </div>
                                                            <form method="POST"
                                                                  action="{{ route('school.accr.input.save') }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="form"
                                                                       value="edit_buildings_plots">
                                                                <input type="hidden" name="bp_id"
                                                                       value="{{ $bp->id }}">
                                                                <div class="modal-body"
                                                                     style="padding: 30px;height: 450px;overflow-y: scroll;background: #f0f1f3">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>Building / Plot
                                                                                    Name</label>
                                                                                <p>{{ $bp->name }}</p>
                                                                            </div>


                                                                            <div class="form-group">
                                                                                <label>Building / Plot
                                                                                    Class</label>
                                                                                <p>{{ $bp->class }}</p>
                                                                            </div>

                                                                            <div class="form-group row"
                                                                                 id="addBuilding2">
                                                                                <label class="col-md-12 col-form-label">Infrastructure</label>
                                                                                @foreach ($bp['infras'] as $infra)
                                                                                    <div class="col-md-6">
                                                                                        <p>{{ $infra->name }}</p>
                                                                                    </div>
                                                                                    <div class="col-md-5">
                                                                                        <p>{{ $infra->quantity }}</p>
                                                                                    </div>
                                                                                @endforeach

                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>Purpose</label>
                                                                                <p>{{ $bp->purpose }}</p>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>Size</label>
                                                                                <p>{{ $bp->size }}</p>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>Capacity</label>
                                                                                <p>{{ $bp->capacity }}</p>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>Construction
                                                                                    Materials</label>
                                                                                <p>{{ $bp->construction_materials }}</p>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>Roofing
                                                                                    Materials</label>
                                                                                <p>{{ $bp->roofing_materials }}</p>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label for=""
                                                                                       class="col-md-5 col-form-label">Harvests
                                                                                    Rain Water</label>
                                                                                <div class="col-md-3">
                                                                                    <p class="col-form-label">{{ $bp->harvests_rain_water }}</p>
                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-md-5 col-form-label">Has
                                                                                    Water</label>
                                                                                <div class="col-md-3">
                                                                                    <p class="col-form-label">{{ $bp->has_water }}</p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-md-5 col-form-label">Has
                                                                                    Electricity</label>
                                                                                <div class="col-md-3">
                                                                                    <p class="col-form-label">{{ $bp->has_electricity }}</p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-md-5 col-form-label">Has
                                                                                    Internet</label>
                                                                                <div class="col-md-3">
                                                                                    <p class="col-form-label">{{ $bp->has_internet }}</p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-md-5 col-form-label">Has
                                                                                    Extinguisher</label>
                                                                                <div class="col-md-3">
                                                                                    <p class="col-form-label">{{ $bp->has_extinguisher }}</p>
                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-md-5 col-form-label">Has
                                                                                    Lightening
                                                                                    Arrestor</label>
                                                                                <div class="col-md-3">
                                                                                    <p class="col-form-label">{{ $bp->has_lightening_arrestor }}</p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-md-5 col-form-label">Has
                                                                                    External
                                                                                    Lighting</label>
                                                                                <div class="col-md-3">
                                                                                    <p class="col-form-label">{{ $bp->has_external_lighting }}</p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>Furniture</label>
                                                                                <p>{{ $bp->furniture }}</p>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>Equipment</label>
                                                                                <p>{{ $bp->equipment }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer"
                                                                     style="text-align: center;">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <button type="button"
                                                                                    class="btn btn-default btn-block btn-flat"
                                                                                    data-dismiss="modal">
                                                                                <i class="fa fa-close"></i>
                                                                                Cancel
                                                                            </button>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>

                                <h5><i class="fa fa-check-circle"></i> Programs Offered</h5>
                                <table class="table table-bordered table-hover">
                                    <thead style="background: #efefef">
                                    <tr>
                                        <th>#</th>
                                        <th>Sector</th>
                                        <th style="width: 15%">Sub-Sector</th>
                                        <th style="width: 15%">Qualification</th>
                                        <th style="width: 15%">RTQF Level</th>
                                        <th style="width: 10%">Male Trainees</th>
                                        <th style="width: 10%">Female Trainees</th>
                                        {{--<th>Curriculum</th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($programs_offered as $pd)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $pd->qualification->subsector->sector->tvet_field or "" }}</td>
                                            <td>{{ $pd->qualification->subsector->sub_field_name or "" }}</td>
                                            <td>{{ $pd->qualification->qualification_title or "" }}</td>
                                            <td>{{ $pd->qualification->rtqf->level_name or "" }}</td>
                                            <td>{{ $pd->qualification ? getStudentsNber($pd->qualification->rtqf->id, $pd->qualification_id, $pd->school_id, [['key' => 'gender', 'operator' => '=', 'needed' => ucwords('male')]]) : "" }}</td>
                                            <td>{{ $pd->qualification ? getStudentsNber($pd->qualification->rtqf->id, $pd->qualification_id, $pd->school_id, [['key' => 'gender', 'operator' => '=', 'needed' => ucwords('female')]]) : "" }}</td>
                                            {{--<td>{{ $pd->curriculum }}</td>--}}
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                <h5><i class="fa fa-check-circle"></i> Trainer / Teachers</h5>
                                <table class="table table-bordered table-hover" id="dataTableBtn">
                                    <thead style="background: #efefef">
                                    <tr>
                                        <th></th>
                                        <th>Photo</th>
                                        <th>Name</th>
                                        <th style="width: 10%">Gender</th>
                                        {{--<th>Level / Degree</th>--}}
                                        <th>Qualification</th>
                                        <th>Institution</th>
                                        <th>Graduated Year</th>
                                        <th>Attended RTTI</th>
                                        {{--<th>Certification</th>--}}
                                        {{--<th>Experience</th>--}}
                                        {{--<th>Modules Taught</th>--}}
                                        {{-- <th></th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($trainers as $td)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>
                                                <img src="{{ getStaffPhoto($td->photo) }}" alt=""
                                                     class="img-responsive img-rounded" style="width: 90px;">
                                            </td>
                                            <td>{{ $td->names }}</td>
                                            <td>{{ $td->gender }}</td>
                                            {{--<td>--}}
                                            {{--@if($td->qualifications)--}}
                                            {{--@php--}}
                                            {{--$degree = "";--}}
                                            {{--@endphp--}}
                                            {{--@foreach($td->qualifications as $quall)--}}
                                            {{--@php--}}
                                            {{--$degree .= ",".$quall->qualification_level;--}}
                                            {{--@endphp--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--</td>--}}
                                            <td>
                                                {{ $td->qualification }}
                                            </td>
                                            <td>
                                                {{ $td->institution }}
                                            </td>
                                            <td>{{ $td->graduated_year }}</td>
                                            <td>{{ $td->attended_rtti }}</td>
                                            {{--<td>{{ $td->certiName }}</td>--}}
                                            {{--<td>{{ $td->experience }}</td>--}}
                                            {{--<td>{{ $td->modules_taught }}</td>--}}
                                            {{-- <td>
                                                <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                    {{ csrf_field() }}

                                                    <input type="hidden" name="_method" value="delete">
                                                    <input type="hidden" name="form" value="teachers">
                                                    <input type="hidden" name="delete_trainer"
                                                           value="{{ $td->id }}">

                                                    <button type="submit"
                                                            class="btn btn-danger btn-sm btn-flat"
                                                            onclick="return window.confirm('Are you sure you want to remove this trainer : \n\n {{ $td->names }} ?')">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                            </td> --}}
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                <h5><i class="fa fa-check-circle"></i> Administration Staff</h5>
                                <table class="table table-bordered table-hover">
                                    <thead style="background: #efefef">
                                    <tr>
                                        <th></th>
                                        <th>Photo</th>
                                        <th>Name</th>
                                        <th style="width: 10%">Gender</th>
                                        {{--<th>Level / Degree</th>--}}
                                        <th>Qualification</th>
                                        <th>Institution</th>
                                        <th>Graduated Year</th>
                                        <th>Attended RTTI</th>
                                        {{-- <th>Teaching Experience</th>
                                        <th>Assessor Qualification</th> --}}
                                        {{-- <th></th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @forelse($staffs as $sd)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>
                                                <img src="{{ getStaffPhoto($sd->photo) }}" alt=""
                                                     class="img-responsive img-rounded" style="width: 90px;">
                                            </td>
                                            <td>{{ $sd->names }}</td>
                                            <td>{{ $sd->gender }}</td>
                                            {{--<td>--}}
                                            {{--@if($td->qualifications)--}}
                                            {{--@php--}}
                                            {{--$degree = "";--}}
                                            {{--@endphp--}}
                                            {{--@foreach($td->qualifications as $quall)--}}
                                            {{--@php--}}
                                            {{--$degree .= ",".$quall->qualification_level;--}}
                                            {{--@endphp--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--</td>--}}
                                            <td>
                                                {{ $sd->qualification }}
                                            </td>
                                            <td>
                                                {{ $sd->institution }}
                                            </td>
                                            <td>{{ $sd->graduated_year }}</td>
                                            <td>{{ $sd->attended_rtti }}</td>
                                            {{--<td>{{ $td->certiName }}</td>--}}
                                            {{--<td>{{ $td->experience }}</td>--}}
                                            {{--<td>{{ $td->modules_taught }}</td>--}}
                                            {{-- <td>
                                                <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                    {{ csrf_field() }}

                                                    <input type="hidden" name="_method" value="delete">
                                                    <input type="hidden" name="form" value="teachers">
                                                    <input type="hidden" name="delete_trainer"
                                                           value="{{ $td->id }}">

                                                    <button type="submit"
                                                            class="btn btn-danger btn-sm btn-flat"
                                                            onclick="return window.confirm('Are you sure you want to remove this trainer : \n\n {{ $td->names }} ?')">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                            </td> --}}
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="10" class="text-center">Empty !!</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>

                                <h5><i class="fa fa-check-circle"></i> Qualification Applied For</h5>
                                <table class="table table-bordered table-hover">
                                    <thead style="background: #efefef">
                                    <tr>
                                        <th></th>
                                        <th style="width: 30%">Sector</th>
                                        <th style="width: 20%">Sub Sector</th>
                                        <th style="width: 20%">Curriculum Qualification</th>
                                        <th>RTQF Level</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($application_data as $applies)
                                        @if($applies->accrQualificationsApplied->count() > 0)
                                            @foreach($applies->accrQualificationsApplied as $ad)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $ad->curr->subsector->sector->tvet_field or "" }}</td>
                                                    <td>{{ $ad->curr->subsector->sub_field_name or "" }}</td>
                                                    <td>{{ $ad->curr->qualification_title or "" }}</td>
                                                    <td>{{ $ad->rtqf->level_name or "" }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>

                                <br>
                                <p style="color: #2d7b27">
                                    I confirm that the information above entered by me is valid, updated
                                    and truthful and any updates to it will be submitted to the relevant
                                    office as soon as is practical.
                                </p>
                                <br>

                                <div class="row">
                                    <div class="col-md-2">
                                        <a href="?step=7" class="btn btn-default btn-block btn-flat"> <i
                                                    class="fa fa-arrow-left"></i> Go Back</a>
                                    </div>
                                    @if(strlen(school('district')) > 0)
                                        <div class="col-md-5">
                                            <form method="POST" action="{{ route('school.accr.confirm') }}">
                                                {{ csrf_field() }}
                                                {!! Form::hidden('step', $step) !!}
                                                {!! Form::hidden('accreditation_type_id', null,['class' => 'accreditation_type']) !!}
                                                <button type="submit"
                                                        class="btn btn-primary btn-block btn-flat confirm">
                                                    <i
                                                            class="fa fa-save"></i> Confirm &amp; Submit
                                                </button>
                                            </form>
                                        </div>
                                        <div class="col-md-5">
                                            <form method="POST" action="{{ route('school.accr.confirm') }}">
                                                {{ csrf_field() }}
                                                {!! Form::hidden('step', $step) !!}
                                                {!! Form::hidden('accreditation_type_id', null,['class' => 'accreditation_type']) !!}
                                                <input type="hidden" name="next" value="yes">

                                                <button type="submit"
                                                        class="btn btn-info btn-block btn-flat confirm"><i
                                                            class="fa fa-save"></i> Confirm &amp; Go to the
                                                    next section
                                                </button>
                                            </form>
                                        </div>
                                    @else
                                        <div class="col-md-10">
                                            <div class="alert alert-danger">
                                                <h3>Please go in your school settings and update location section !!</h3>
                                                <a href="{{ route('school.get.info') }}" class="btn btn-warning" style="text-decoration: none;">Click here ...</a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>

                    {{--</div>--}}
                </div>
            </div>
            <br clear="left">
            <div class="box-footer"></div>
        </div>
    </div>
    </div>
    @if(isset($infra_sec))
        <script type="text/javascript"> var infra_sec = "<?= $infra_sec;  ?>"  </script>
    @endif
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            $("#accreditation_type_select").on("change", function () {
                let v = $(this).val();
                $("* .accreditation_type").val(v);
            });
        });

        $("#finish").click();
    </script>
@endsection