<h3 class="label label-warning">District Provisional AQA</h3>
<table class="table table-bordered table-hover">
    <thead style="background: #efefef">
    <tr>
        <th style="width: 25%">Strength</th>
        <th>Weakness</th>
        <th>Opportunity</th>
        <th>Threat</th>
        <th>Recommendation</th>
        <th>Marks</th>
    </tr>
    </thead>
    <tbody>

        <tr>
            <td>{!!  $District_comments->strength or "" !!}</td>
            <td>{!! $District_comments->weakness or ""  !!}</td>
            <td>{!! $District_comments->opportunity or ""  !!}</td>
            <td>{!! $District_comments->threat or ""  !!}</td>
            <td>{!! $District_comments->recommendation or ""  !!}</td>
            <td>{!! $District_comments->marks or ""  !!}</td>
        </tr>

    </tbody>
</table>