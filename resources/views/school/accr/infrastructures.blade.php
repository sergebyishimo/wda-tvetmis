@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Infrastructure Source</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	  Accreditation / Source Input / Infrastructure</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						@if (isset($att_info))
							
							<form method="POST" action="/accr/input/infrastructures/">
								{{ csrf_field() }}
								
								<input type="hidden" name="att_id" value="{{ $att_info->id }}">
								<div class="card card-info">
									<div class="card-header">
										<h3 class="card-title"><i class="fa fa-edit"></i> Edit Infrastructure</h3>
									</div>
								

									
									<div class="card-body">
										<div class="form-group">
											<label>Infrastructure</label>
											<input type="text" class="form-control flat" name="attachment" value="{{ $att_info->name }}" placeholder="Infrastructure">
										</div>
										
									</div>
									<div class="card-footer">
										<button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save Infrastructure</button>
									</div>
								</div>
							</form>
						@endif
						
						<table class="table table-bordered" style="background: #fff">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th>Infrastructure</th>
									<th style="width: 200px">Action</th>
								</tr>
							</thead>
							<tbody>
								@php
									$i = 1;
								@endphp
								@foreach ($atts as $att)
									<tr>
										<td class="text-center">{{ $i++ }}</td>
										<td>{{ $att->name }}</td>
										<td>
											<form method="POST" action="/accr/input/infrastructures">
												{{ csrf_field() }}	
											
												<input type="hidden" name="_method" value="delete">
												<input type="hidden" name="delete_att" value="{{ $att->id }}">

												<a href="/accr/input/attachments/{{ $att->id }}" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-edit"></i> Edit</a>
												<button type="submit" class="btn btn-danger btn-flat btn-sm" onclick="return window.confirm('Are you sure you want to remove this attachment: \n\n {{ $att->attachment_name }} ?')"><i class="fa fa-trash"></i> Delete</button>
											</form>
										</td>
									</tr>
								@endforeach
								
								<form method="POST">
									{{ csrf_field() }}
								
									<tr>
										<td></td>
										<td><input type="text" class="form-control flat" name="attachment" placeholder="Infrastructure" required> </td>
										<td class="text-center">
											<button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i> Save</button>
										</td>
										
									</tr>
								</form>
							</tbody>
						</table>
					
						

					</div>
				</div>
			</div>
		</section>
	</div>

@endsection