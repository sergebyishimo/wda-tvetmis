@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
					
						<div class="card card-primary">
							<div class="card-header">
								<h4 class="card-title"><i class="fa fa-list"></i> View Qualification Information</h4>
							</div>
							<div class="card-body">
								
								<div class="form-group">
									<label>Qualification Code</label>
									<p>{{$qualification->qualification_code  }}</p>
								</div>
								<div class="form-group">
									<label>Qualification Title</label>
									<p>{{$qualification->qualification_title  }}</p>
								</div>
								<div class="form-group">
									<label>Sector</label>
									<p>{{$qualification->subsector->sector->tvet_field  }}</p>
								</div>
								<div class="form-group">
									<label>Sub Sector</label>
									<p>{{$qualification->subsector->sub_field_name  }}</p>
								</div>
								<div class="form-group">
									<label>RTQF Level</label>
									<p>{{$qualification->rtqf->level_name  }}</p>
								</div>
								<div class="form-group">
									<label>Credits</label>
									<p>{{$qualification->credits  }}</p>
								</div>
								<div class="form-group">
									<label>Release Date</label>
									<p>{{$qualification->credits  }}</p>
								</div>
								<div class="form-group">
									<label>Status</label>
									<p>{{$qualification->status  }}</p>
								</div>
								<div class="form-group">
									<label>Description</label>
									<p>{{$qualification->description  }}</p>
								</div>
								<div class="form-group">
									<label>Job Related Information</label>
									<p{{ $qualification->job_related_information }}></p>
								</div>
								<div class="form-group">
									<label>Entry Requirements</label>
									<p>{{ $qualification->entry_requirement }}</p>
								</div>
								<div class="form-group">
									<label>Information About Pathways</label>
									<p>{{ $qualification->information_about_pathways }}</p>
								</div>
								<div class="form-group">
									<label>Employability And Life Skills</label>
									<p>{{ $qualification->employability_and_life_skills }}</p>
								</div>
								<div class="form-group">
									<label>Qualification Arrangments</label>
									<p>{{ $qualification->qualification_arrangement }}</p>
								</div>

								<div class="form-group">
									<label>Attachment</label>
									<a href="">
										<p>Download File</p>
									</a>
								</div>
								<div class="form-group">
									<label>Occupation Profile Attachment</label>
									<a href="">
										<p>Download File</p>
									</a>
								</div>
								<div class="form-group">
									<label>TOG Download</label>
									<a href="">
										<p>Download File</p>
									</a>
								</div>
								<div class="form-group">
									<label>Training Manual</label>
									<a href="">
										<p>Download File</p>
									</a>
								</div>
								<div class="form-group">
									<label>Compentency</label>
									<a href="">
										<p>Download File</p>
									</a>
								</div>
								<div class="form-group">
									<label>Qualification Summary Attachment</label>
									<a href="">
										<p>Download File</p>
									</a>
								</div>

							</div>
							<div class="card-footer">
								<a href="/accr/input/qualifications/" class="btn btn-info btn-block btn-flat"><i class="fa fa-arrow-left"></i> Go Back</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
	</div>
@endsection