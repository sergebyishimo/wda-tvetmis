@extends('layouts.master')

@if ($school_info)
	

@section('content')
	<link href="{{ asset('css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/smart_wizard_theme_arrows.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/smart_wizard_theme_dots.css') }}" rel="stylesheet" type="text/css" />

	<div class="content-wrapper">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> Polytechnic Accreditation</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	Accreditation / Input</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card" >
							<div class="card-header">
								<h3 class="card-title">Polytechnic Accreditation Process</h3>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								
								<div id="myForm" role="form" accept-charset="utf-8">

							        <!-- SmartWizard html -->
							        <div id="smartwizard">
							            <ul>
							                <li><a href="#step-1">Step 1<br /><small>School Information</small></a></li>
							                <li><a href="#step-2">Step 2<br /><small>Infrastructure</small></a></li>
							                <li><a href="#step-3">Step 3<br /><small>Educational Facilities</small></a></li>
							                <li><a href="#step-4">Step 4<br /><small>Staffing</small></a></li>
							                <li><a href="#step-5">Step 5<br /><small>Students and Student Numbers</small></a></li>
							                <li><a href="#step-6">Step 6<br /><small>Vision of the Polytechnic College</small></a></li>
							                <li><a href="#step-7">Step 7<br /><small>Confirmation</small></a></li>
							            </ul>

							            <div style="padding: 10px 20px;background: #fff">
							            	<div id="step-1">
							                    <h5><i class="fa fa-check-circle"></i> School Information</h5>

							                    <div class="row">
						                    		<div class="col-md-6">
						                    			<div class="form-group">
								                            <label for="">Name:</label>
								                            <input type="text" class="form-control flat" name="name" placeholder="School Name" value="{{ $school_info->school_name }}">
								                        </div>
								                        <div class="form-group">
								                            <label for="">Postal Address:</label>
								                            <input type="text" class="form-control flat" name="name" placeholder="Postal Address" value="{{ $school_info->post_office_box_number }}">
								                        </div>

								                        <div class="form-group">
								                            <label for="">E-mail Address:</label>
								                            <input type="text" class="form-control flat" name="name" placeholder="E-mail Address" value="{{ $school_info->email }}">
								                        </div>
								                        <div class="form-group">
								                            <label for="">Web Address:</label>
								                            <input type="text" class="form-control flat" name="name" placeholder="Web Address" value="{{ $school_info->webaddress }}">
								                        </div>
								                        <div class="form-group">
								                            <label for="">Telephone Number(s):</label>
								                            <input type="text" class="form-control flat" name="name" placeholder="Telephone Number" value="{{ $school_info->phone }}">
								                        </div>
								                        <div class="form-group">
								                            <label for="">Province:</label>
								                            <select class="form-control flat" id="province" name="province">
								                            	<option value="kigali">Kigali</option>
								                            	<option value="north">Northern Province</option>
								                            	<option value="south">Southern Province</option>
								                            	<option value="east">Eastern Province</option>
								                            	<option value="west">Western Province</option>
								                            	
								                            </select>
								                            
								                        </div>
								                        <div class="form-group">
								                            <label for="">District:</label>
								                            <select class="form-control flat" id="districts" name="district">
								                            	
								                            </select>
								                        </div>
								                    </div>
								                    <div class="col-md-6">
								                    	
								                        <div class="form-group">
								                            <label for="">Sector:</label>
								                            <input type="text" class="form-control flat" name="sector" value="{{ $school_info->sector }}">
								                            
								                        </div>
								                        <div class="form-group">
								                            <label for="">Cell:</label>
								                            <input type="text" class="form-control flat" name="cell" value="{{ $school_info->cell }}">
								                        </div>
								                        <div class="form-group">
								                            <label for="">Village:</label>
								                            <input type="text" class="form-control flat" name="village" value="{{ $school_info->village }}">
								                        </div>
								                        <div class="form-group">
								                            <label for="">The size of land owned by the College</label>
								                            <input type="text" class="form-control flat" name="village">
								                        </div>	
								                        <div class="form-group">
								                            <label for="">Size of land to be used </label>
								                            <input type="text" class="form-control flat" name="village">
								                        </div>
								                        <div class="form-group">
								                            <label for="">Year when the land was purchased if applicable </label>
								                            <input type="text" class="form-control flat" name="village">
								                        </div>		
								                        <div class="form-group">
								                            <label for="">If the land on which the College is leased or rented please provide a copy of the lease/tenancy agreement</label>
								                            <div class="input-group">
																<div class="custom-file">
																	<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
																	<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
																</div>
										                    </div>
								                        </div>
								                    </div>
								                </div>
								                 <div class="row">
							                    	<div class="col-md-6">
							                    		<button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Save & Continue <i class="fa fa-arrow-right"></i></button>		
							                    	</div>
							                    	<div class="col-md-6">
							                    		<div class="row">
							                    			<div class="col-md-12">
							                    				<a href="#step-2" class="btn btn-default btn-block btn-flat"> Next <i class="fa fa-arrow-right"></i></a>				
							                    			</div>
							                    		</div>
							                    		
							                    	</div>
							                    </div>
							                </div>
							                <div id="step-2">
							                	<h5><i class="fa fa-check-circle"></i> A. Buildings</h5>
							                	<p>State the total area in square meters of the following buildings:</p>
							                	<table class="table table-bordered table-striped">
							                		<thead>
							                			<tr>
								                			<th style="width: 25%">Buildings</th>
								                			<th>Number</th>
								                			<th style="width: 30%">Size</th>
								                			<th>Upload File</th>
								                			<th></th>
								                		</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
							                				<td>
							                					<select class="form-control flat">
							                						<option>Classrooms</option>
							                						<option>Workshops</option>
							                						<option>Computer Laboratories</option>
							                						<option>Adminstration Bloc</option>
							                						<option>Offices for administration staff</option>
							                						<option>Student Welfare Offices</option>
							                						<option>Health Clinic/ Sickbay area</option>
							                						<option>Student Canteen</option>
							                					</select>
							                				</td>
							                				<td><input type="number" class="form-control flat" name="" placeholder="Number"></td>
							                				<td><textarea class="form-control flat" placeholder="Size"></textarea></td>
							                				<td>
							                					<div class="input-group">
																	<div class="custom-file">
																		<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
																		<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
																	</div>
											                    </div>
							                				</td>
							                				<td><button type="button" class="btn btn-success btn-block btn-flat"><i class="fa fa-save"></i> Save</button></td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	
							                	<p>Student hostels – number, total area in square meters and number of bed spaces </p>
							                	<div class="form-group row" style="padding-left: 50px">
							                		<label class="col-md-3 col-form-label-">Male Students</label>
							                		<div class="col-md-4">
							                			<input type="number" class="form-control flat" name="" placeholder="Number of bed spaces">
							                		</div>
							                		<div class="col-md-4">
							                			<input type="text" class="form-control flat"  name="" placeholder="Total area">
							                		</div>
							                	</div>
							                	<div class="form-group row" style="padding-left: 50px">
							                		<label class="col-md-3 col-form-label-">Female Students</label>
							                		<div class="col-md-4">
							                			<input type="number" class="form-control flat" name="" placeholder="Number of bed spaces">
							                		</div>
							                		<div class="col-md-4">
							                			<input type="text" class="form-control flat"  name="" placeholder="Total area">
							                		</div>
							                	</div>
							                	<div class="form-group">
							                		<label>Provide a master plan of your campus showing all the actual and any planned buildings. Please indicate those buildings that are already in place.</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>

							                	<h5><i class="fa fa-check-circle"></i> B. Transport</h5>
							                	<div class="form-group">
							                		<label>How many vehicles do the institution own?</label>
							                		<textarea class="form-control flat"></textarea>
							                	</div>
							                </div>
							                <div id="step-3">
							                	<h5><i class="fa fa-check-circle"></i> Educational Facilities in Places</h5>
							                	<b> A. Total number of library books by subject  (e.g. Mechanical   Engineering , Electrical Engineering, Hospitality trades , ...)</b>
							                	<br><br>
							                	<div class="form-group">
							                		<div class="row">
							                			<div class="col-md-4">
								                			<input type="text" class="form-control flat" name="" placeholder="Subject">
								                		</div>
								                		<div class="col-md-4">
								                			<input type="number" class="form-control flat" name="" placeholder="Number of Books">
								                		</div>	
								                		<div class="col-md-1">
								                			<button class="btn btn-primary btn-block btn-flat"><i class="fa fa-plus"></i></button>
								                		</div>
							                		</div>
							                	</div>

							                	
							                	<div class="form-group">
							                		<label>B. Total number of text books by  trades area</label>
							                		<div class="row">
							                			<div class="col-md-4">
								                			<input type="text" class="form-control flat" name="" placeholder="Trade">
								                		</div>
								                		<div class="col-md-4">
								                			<input type="number" class="form-control flat" name="" placeholder="Number of Books">
								                		</div>	
								                		<div class="col-md-1">
								                			<button class="btn btn-primary btn-block btn-flat"><i class="fa fa-plus"></i></button>
								                		</div>
							                		</div>
							                	</div>
							                	<b>C. Dates of publication of the majority of books (Give in blocks)</b>
							                	<br><br>
							                	
							                	<div class="form-group">
							                		<label>D. Total number of computers in the library</label>
							                		<input type="number" class="form-control flat" name="">
							                	</div>
							                	<div class="form-group">
							                		<label>E. Please give details of other learning materials the provided for student use</label>
							                		<textarea class="form-control flat"></textarea>
							                	</div>
							                	<div class="form-group">
							                		<label>F. Total number of computers for student use in labs</label>
							                		<input type="number" class="form-control flat" name="">
							                	</div>
							                	<div class="form-group">
							                		<label>G. Total number of computers for academic staff use</label>
							                		<input type="number" class="form-control flat" name="">
							                	</div>
							                	<div class="form-group">
							                		<label>H. Total number of computers for administration</label>
							                		<input type="number" class="form-control flat" name="">
							                	</div>
							                	<div class="form-group">
							                		<label>I. What programmes are used to search for and retrieve materials in the library</label>
							                		<textarea class="form-control flat"></textarea>
							                	</div>
							                	<div class="form-group">
							                		<label>J. Give details of the cataloguing system used in the library including if it is computerized.</label>
							                		<textarea class="form-control flat"></textarea>
							                	</div>
							                	<div class="form-group">
							                		<label>K. Please give details of the internet access available for staff and students</label>
							                		<textarea class="form-control flat"></textarea>
							                	</div>
							                	<div class="form-group">
							                		<label>L. State the number of chairs in  the  library</label>
							                		<input type="number" class="form-control flat" name="">
							                	</div>
							                	<b>What residential accommodation for students is provided?</b>
							                	<div class="form-group" style="padding-left: 30px">
							                		<label>Female Students</label>
							                		<input type="text" class="form-control flat" name="">
							                	</div>
							                	<div class="form-group" style="padding-left: 30px">
							                		<label>Male Students</label>
							                		<input type="text" class="form-control flat" name="">
							                	</div>
							                </div>
							                <div id="step-4">
							                	<b> A. How many full time academic staff does the Polytechnic College – TVET Institution employ</b>
							                	<br><br>
								                <table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Grade</th>
							                				<th>Number Of Full Time</th>
							                				<th>Number Of Part Time</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
							                				<th>
							                					<select class="form-control flat">
							                						<option>Senior Lecture</option>
							                						<option>Lecture</option>
							                						<option>Assistant Lecture</option>
							                						<option>Tutorial Assistant</option>
							                						<option>Chief Instructor</option>
							                						<option>Instructor</option>
							                						<option>Workshop Assistant</option>
							                					</select>
							                				</th>
							                				<td>
							                					<input type="number" class="form-control flat" name="">
							                				</td>
							                				<td>
							                					<input type="number" class="form-control flat" name="">
							                				</td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	<b>Table 1 A. For Full Time Staff in Institutions Teaching to Degree Level </b>
							                	<br><br>
							                	<table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Trades</th>
							                				<th style="width: 12%">Senior Lecturer</th>
							                				<th style="width: 12%">Lecturer</th>
							                				<th style="width: 12%">Assistant Lecturer</th>
							                				<th style="width: 12%">Tutorial Assistant</th>
							                				<th style="width: 12%">Chief Instructor</th>
							                				<th style="width: 12%">Instructor</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
							                				<td>
							                					<select class="form-control flat"></select>
							                				</td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	<b> Table 1 B. For Full Time Staff in Institutions Teaching to Degree Level </b>
							                	<br><br>
							                	<table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Trades</th>
							                				<th style="width: 12%">Senior Lecturer</th>
							                				<th style="width: 12%">Lecturer</th>
							                				<th style="width: 12%">Assistant Lecturer</th>
							                				<th style="width: 12%">Tutorial Assistant</th>
							                				<th style="width: 12%">Chief Instructor</th>
							                				<th style="width: 12%">Instructor</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
							                				<td>
							                					<select class="form-control flat"></select>
							                				</td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                				<td><input type="number" class="form-control flat" name=""></td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	<div class="form-group">
							                		<label>Please append an Academic Staffing Plan indicating the staff/trainee ratio for each trades taught. <br>(note staff and trainees  who are part time are counted fractionally.)</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>
							                	<div class="form-group">
							                		<label>How many administrative staff does the institution employ? Please attach a structure for administrative staff listing all posts and indicating grades.</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>
							                	<div class="form-group">
							                		<label>How many support staff will the institution employ Please attach a structure for support staff listing all posts and indicating grades.</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>

							                	<table class="table table-bordered table-striped">
							                		<thead>
							                			<tr>
							                				<th>Name</th>
							                				<th>Gender</th>
							                				<th>Position</th>
							                				<th>Qualification</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Name"></td>
							                				<td>
							                					<select class="form-control flat">
							                						<option>Male</option>
							                						<option>Female</option>
							                					</select>
							                				</td>
							                				<td>
							                					<select class="form-control flat">
							                						<option>Chair of Board of Directors</option>
							                						<option>Members of Board of Directors / Governors</option>
							                						<option>Vice-Chancelor / Principal</option>
							                						<option>Deputy Vice-Chancellor Academic Affairs/Vice-Principal Academic trainings </option>
							                						<option>Director of Academic Services </option>
							                						<option>Director of students affairs and other staff</option>
							                					</select>
							                				</td>
							                				<td><input type="text" class="form-control flat" name=""></td>
							                			</tr>
							                		</tbody>
							                	</table>

							                	<div class="form-group">
							                		<label>Please attach an organizational diagram.</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>
							                	<div class="form-group">
							                		<label>Please attach post profiles for all senior posts.</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>
							                	<div class="form-group">
							                		<label>Please provide details of the procedures used for appointing the Rector/Principal and other senior staff.</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>
							                	<div class="form-group">
							                		<label>Ownership of the Polytechnic College-TVET Institution</label>
							                		<input type="text" class="form-control flat" name="" placeholder=" Ownership of the Polytechnic College-TVET Institution">
							                	</div>
							                </div>
							                <div id="step-5">
							                	<div class="form-group">
							                		<label>Please attach a list of students at each level by programme.</label>
							                		<div class="input-group">
														<div class="custom-file">
															<input type="file" class="custom-file-input" name="upload_file" id="exampleInputFile" required>
															<label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
														</div>
								                    </div>
							                	</div>
							                	<b>A.  Please list all the faculties and departments of the institution</b>
							                	<br><br>
							                	<table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Faculties</th>
							                				<th>Departments</th>
							                				<th></th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
							                				<td><input type="text" class="form-control flat" name=""></td>
							                				<td><input type="text" class="form-control flat" name=""></td>
							                				<td><button type="button" class="btn btn-success btn-block btn-flat"><i class="fa fa-save"></i> Save</button></td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	<b>B. Please list all the programmes that will be offered together with the awards to be offered and whether the programmes will be offered as full time (students not in employment) or part time (students in employment) (Please see the Rwanda TVET Qualification Framework  for guidance on the length of programmes for full and part time students). (Note when the site visit takes place all programme documentation must be available together with the name (s) of the external members of validation panels and the report of the validation panel meeting). </b>
							                	<br><br>
							                	<table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Faculty</th>
							                				<th>Department</th>
							                				<th>Programme</th>
							                				<th>Awards</th>
							                				<th>Full Time / Part Time</th>
							                				<th>Level 6</th>
							                				<th>Total Student Number</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Faculty"></td>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Department"></td>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Programme"></td>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Awards"></td>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Full Time / Part Time"></td>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Level 6"></td>
							                				<td><input type="text" class="form-control flat" name="" placeholder="Total Student Number"></td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	<b>C. Please indicate for each programme the projected number of FULL TIME students that will be recruited annually and provide a student number projections for  the next four years</b>
							                	<br><br>
							                	<table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Trade</th>
							                				<th style="width: 13%">Annual Recruitment</th>
							                				<th style="width: 13%">Year 1</th>
							                				<th style="width: 13%">Year 2</th>
							                				<th style="width: 13%">Year 3</th>
							                				<th style="width: 13%">Year 4</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
						                					<td>
						                						<select class="form-control flat">
						                							
						                						</select>
						                					</td>
						                					<td><input type="number" class="form-control flat" name="" placeholder=""></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 1"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 2"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 3"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 4"></td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	<b>D. Please indicate for each programme the projected number of PART TIME students that will be recruited annually and provide a student number projections for  the next four years</b>
							                	<br><br>
							                	<table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Trade</th>
							                				<th style="width: 12%">Annual Recruitment</th>
							                				<th style="width: 12%">Year 1</th>
							                				<th style="width: 12%">Year 2</th>
							                				<th style="width: 12%">Year 3</th>
							                				<th style="width: 12%">Year 4</th>
							                				<th style="width: 12%">Year 5</th>
							                				<th style="width: 12%">Year 6</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
						                					<td>
						                						<select class="form-control flat">
						                							
						                						</select>
						                					</td>
						                					<td><input type="number" class="form-control flat" name="" placeholder=""></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 1"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 2"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 3"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 4"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 3"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 4"></td>
							                			</tr>
							                		</tbody>
							                	</table>
							                	<b>E. Please indicate for each programme the number of distance students recruited annually and provide student number projections for the next  six years</b>
							                	<br><br>
							                	<table class="table table-bordered table-striped">
							                		<thead style="background: #f5f5f5">
							                			<tr>
							                				<th>Trade</th>
							                				<th style="width: 12%">Annual Recruitment</th>
							                				<th style="width: 12%">Year 1</th>
							                				<th style="width: 12%">Year 2</th>
							                				<th style="width: 12%">Year 3</th>
							                				<th style="width: 12%">Year 4</th>
							                				<th style="width: 12%">Year 5</th>
							                				<th style="width: 12%">Year 6</th>
							                			</tr>
							                		</thead>
							                		<tbody>
							                			<tr>
						                					<td>
						                						<select class="form-control flat">
						                							
						                						</select>
						                					</td>
						                					<td><input type="number" class="form-control flat" name="" placeholder=""></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 1"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 2"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 3"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 4"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 3"></td>
						                					<td><input type="number" class="form-control flat" name="" placeholder="Year 4"></td>
							                			</tr>
							                		</tbody>
							                	</table>

							                	<div class="form-group">
							                		<label>F. Please list any additional programmes you  plan to develop and indicate when it is hoped to begin offering them (Please note that you MUST get permission from the WDA before you plan to open a new department/faculty)</label>
							                		<textarea class="form-control flat"></textarea>
							                	</div>
							                </div>
							                <div id="step-6">
							                	<h5><i class="fa fa-check-circle"></i> Vision and Mission</h5>
							                	<div class="form-group">
							                		<label>Mission</label>
							                		<textarea class="form-control flat" placeholder="Mission"></textarea>
							                	</div>
							                	<div class="form-group">
							                		<label>Vision</label>
							                		<textarea class="form-control flat" placeholder="Vision"></textarea>
							                	</div>
							                	<div class="form-group">
							                		<label>Specific Objectives</label>
							                		<textarea class="form-control flat" placeholder="Specific Objectives"></textarea>
							                	</div>
							                	<div class="form-group">
							                		<label>Provide the logo of your institution</label>
							                		<textarea class="form-control flat" placeholder="Specific Objectives"></textarea>
							                	</div>
							                </div>
							                <div id="step-7">
							                	<br><br>
							                	<p>We confirm that all the information submitted on this form and provided as additional information is true</p>
							                	<br><br>
							                	<div class="row">
							                		<div class="col-md-2">
							                			<a href="#step-7" class="btn btn-default btn-block btn-flat"> <i class="fa fa-arrow-left"></i> Go Back</a>
							                		</div>
							                		<div class="col-md-10">
							                			<form method="POST" action="/accreditation/input/confirm">
							                				{{ csrf_field() }}

							                				<button type="submit" class="btn btn-primary btn-block btn-flat"> <i class="fa fa-save"></i> Confirm &amp; Submit</button>
							                			</form>
							                		</div>
							                	</div>
							                </div>
							            </div>
							        </div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

<script type="text/javascript" src="{{ asset('js/jquery.smartWizard.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        // Toolbar extra buttons


        // Smart Wizard
        $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'arrows',
                keyNavigation:true,

                transitionEffect:'fade',
                toolbarSettings: {toolbarPosition: 'bottom',
                                  showNextButton:false,
                                  showPreviousButton:false
                                },
                anchorSettings: {
                			anchorClickable: true,
            				enableAllAnchors: true,
                            markDoneStep: true, // add done css
                            markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                            removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                        }
             });

        

    });

    $("#finish").click();
</script>
@endsection

@else

@section('content')

	<div class="content-wrapper">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-warning fa-lg"></i> Your School is not Polytechnic</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	Accreditation / Input</li>
						</ol>
					</div>
				</div>
			</div><!-- /.container-fluid -->
		</section>
	</div>

@endsection

@endif