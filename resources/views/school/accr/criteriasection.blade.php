@extends('layouts.master')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="font-size: 22px;padding-left: 5px;"><i class="fa fa-edit"></i> Criteria</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/home">Home</a></li>
                            <li class="breadcrumb-item active"> Accreditation / Source Input / Criteria</li>
                        </ol>
                    </div>
                </div>
                @if (session('successMessage'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fa fa-check"></i> Operation Done!</h5>
                        {!! session('successMessage') !!}
                    </div>

                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        @if (isset($att_info))

                            <form method="POST" action="/accr/input/criteriasection/">
                                {{ csrf_field() }}

                                <input type="hidden" name="att_id" value="{{ $att_info->id }}">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fa fa-edit"></i> Edit Attachment</h3>
                                    </div>


                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Quality Area</label>
                                                <select name="quality_area_id"
                                                        id="quality_area_id"
                                                        class="form-control"
                                                        required=true>
                                                    <option value="">Select Quality Area</option>
                                                    @foreach($atts as $quality)
                                                        <option value="{{ $quality->id }}" {{ $att_info->quality_area_id == $quality->id ? 'selected' : '' }} >{{ $quality->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Criteria Section</label>
                                                    <input type="text" class="form-control flat" name="attachment"
                                                           value="{{ $att_info->criteria_section }}"
                                                           placeholder="Program">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-info btn-flat btn-block"><i
                                                    class="fa fa-save"></i> Save Criteria Section
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif

                        <table class="table table-bordered" style="background: #fff">
                            <thead>
                            <tr>
                                <th class="text-center" width="40px">#</th>
                                <th>Quality Area</th>
                                <th>Criteria Section</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($ii = 1)
                            @foreach ($atts as $att)
                                <tr>
                                    <td>{{ $ii++ }}</td>
                                    <td>{{ $att->name }}</td>
                                    <td>
                                        <table class="table">
                                            @if($att->criteria->count() > 0)
                                                @php($i = 1)

                                                @foreach($att->criteria as $criteria)
                                                    <tr>
                                                        <td class="text-center">{{ $i++ }}</td>
                                                        <td>{{ $criteria->criteria_section }}</td>
                                                        <td class="text-center">{{ $criteria->criterias->sum('weight') }}</td>
                                                        <td>
                                                            <form method="POST" action="/accr/input/criteriasection">
                                                                {{ csrf_field() }}

                                                                <input type="hidden" name="_method" value="delete">
                                                                <input type="hidden" name="delete_att"
                                                                       value="{{ $criteria->id }}">

                                                                <a href="/accr/input/attachments/{{ $criteria->id }}"
                                                                   class="btn btn-primary btn-flat btn-sm"><i
                                                                            class="fa fa-edit"></i> Edit</a>
                                                                <button type="submit"
                                                                        class="btn btn-danger btn-flat btn-sm"
                                                                        onclick="return window.confirm('Are you sure you want to remove this criteria: \n\n {{ $criteria->criteria_section }} ?')">
                                                                    <i class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </table>
                                    </td>
                                </tr>
                            @endforeach

                            <form method="POST">
                                {{ csrf_field() }}

                                <tr>
                                    <td colspan="2">
                                        <select name="quality_area_id"
                                                id="quality_area_id"
                                                class="form-control"
                                                required=true>
                                            <option value="">Select Quality Area</option>
                                            @foreach($atts as $quality)
                                                <option value="{{ $quality->id }}">{{ $quality->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control flat" name="attachment"
                                               placeholder="Criteria" required>
                                    </td>
                                    <td class="text-center">
                                        <button type="submit" class="btn btn-success btn-flat btn-block"><i
                                                    class="fa fa-save"></i> Save
                                        </button>
                                    </td>

                                </tr>
                            </form>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection