@extends('school.accr.layout.main')

@section('panel-title', "View Application Details")

@section('htmlheader_title', "View Application Details")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header d-flex p-0 ui-sortable-handle">
                    <h3 class="box-title p-3"><i class="fa fa-table"></i> View Application Details</h3>
                    <div class="row ml-auto p-2">
                        <div class="col-md-6">
                            <a class="btn btn-warning btn-link" href="{{ route('school.accr') }}"><i
                                        class="fa fa-arrow-left"></i> Go Back</a>
                        </div>
                        <div class="col-md-6">
                            {{--<a class="nav-link" href="/accreditation/download/{{ $application_id }}"><i class="fa fa-file-pdf-o"></i> PDF</a>--}}
                            <a class="btn btn-warning btn-link btnPrint pull-right"
                               href="{{ route('school.accr.print', $application_id) }}"><i class="fa fa-print"></i>
                                Print</a>
                        </div>
                    </div>
                </div>
                <div class="box-body">

                    <h5><i class="fa fa-check-circle"></i> Attachments Provided</h5>
                    <table class="table table-bordered table-hover">
                        <thead style="background: #efefef">
                        <tr>
                            <th style="width: 25%"></th>
                            <th>Attachment Name</th>
                            <th style="width: 25%">Upload PDF or Image</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attachments_data as $ad)
                            <tr>
                                <td></td>
                                <td>{{ $ad->source }}</td>
                                <td>
                                    <a href="{{ Storage::url("$ad->attachment")}}?{{ $school_info['school_name'] . ' ' . str_replace('/', ' ', $ad->source ) }}">Download
                                        File</a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    @php
                        $District_comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 2)->first();
                        $Wda_comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 2)->first();
                    @endphp
                    <br>
                    @include('school.accr.district_display')
                    <br>
                    @include('school.accr.wda_display')
                    <br>
                    <h5><i class="fa fa-check-circle"></i> Materials &amp; Infrastructure</h5>
                    <table class="table table-bordered table-hover">
                        <thead style="background: #efefef">
                        <tr>
                            <th>#</th>
                            <th>Building</th>
                            <th>Infrastructure</th>
                            <th>Purpose</th>
                            <th>Size</th>
                            <th>Capacity</th>
                            <th style="width: 120px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($buildings_plots as $bp)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $bp->name }}</td>
                                <td>

                                    @foreach ($bp['infras'] as $infra)
                                        {{ $infra->quantity. ' ' .$infra->name .  ', ' }}
                                    @endforeach

                                </td>
                                <td>{{ $bp->purpose }}</td>
                                <td>{{ $bp->size }}</td>
                                <td>{{ $bp->capacity }}</td>
                                <td>
                                    <button type="button" class="btn btn-warning btn-flat btn-sm" data-toggle="modal"
                                            data-target="#view_{{ $bp->id }}"><i class="fa fa-tasks"></i> View More
                                    </button>

                                    <div class="modal fade" id="view_{{ $bp->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                    <h4 class="modal-title"><i class="fa fa-home"></i> View Building
                                                        Information</h4>
                                                </div>
                                                <form method="POST" action="/accreditation/input/save">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="form" value="edit_buildings_plots">
                                                    <input type="hidden" name="bp_id" value="{{ $bp->id }}">
                                                    <div class="modal-body"
                                                         style="padding: 30px;height: 450px;overflow-y: scroll;background: #f0f1f3">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Building / Plot Name</label>
                                                                    <p>{{ $bp->name }}</p>
                                                                </div>


                                                                <div class="form-group">
                                                                    <label>Building / Plot Class</label>
                                                                    <p>{{ $bp->class }}</p>
                                                                </div>

                                                                <div class="form-group row" id="addBuilding2">
                                                                    <label class="col-md-12 col-form-label">Infrastructure</label>
                                                                    @foreach ($bp['infras'] as $infra)
                                                                        <div class="col-md-6">
                                                                            <p>{{ $infra->name }}</p>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <p>{{ $infra->quantity }}</p>
                                                                        </div>
                                                                    @endforeach

                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Purpose</label>
                                                                    <p>{{ $bp->purpose }}</p>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Size</label>
                                                                    <p>{{ $bp->size }}</p>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Capacity</label>
                                                                    <p>{{ $bp->capacity }}</p>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Construction Materials</label>
                                                                    <p>{{ $bp->construction_materials }}</p>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Roofing Materials</label>
                                                                    <p>{{ $bp->roofing_materials }}</p>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="" class="col-md-5 col-form-label">Harvests
                                                                        Rain Water</label>
                                                                    <div class="col-md-3">
                                                                        <p class="col-form-label">{{ $bp->harvests_rain_water }}</p>
                                                                    </div>

                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Water</label>
                                                                    <div class="col-md-3">
                                                                        <p class="col-form-label">{{ $bp->has_water }}</p>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Electricity</label>
                                                                    <div class="col-md-3">
                                                                        <p class="col-form-label">{{ $bp->has_electricity }}</p>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Internet</label>
                                                                    <div class="col-md-3">
                                                                        <p class="col-form-label">{{ $bp->has_internet }}</p>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Extinguisher</label>
                                                                    <div class="col-md-3">
                                                                        <p class="col-form-label">{{ $bp->has_extinguisher }}</p>
                                                                    </div>

                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has
                                                                        Lightening Arrestor</label>
                                                                    <div class="col-md-3">
                                                                        <p class="col-form-label">{{ $bp->has_lightening_arrestor }}</p>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-5 col-form-label">Has External
                                                                        Lighting</label>
                                                                    <div class="col-md-3">
                                                                        <p class="col-form-label">{{ $bp->has_external_lighting }}</p>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Furniture</label>
                                                                    <p>{{ $bp->furniture }}</p>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Equipment</label>
                                                                    <p>{{ $bp->equipment }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer" style="text-align: center;">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="button"
                                                                        class="btn btn-default btn-block btn-flat"
                                                                        data-dismiss="modal"><i class="fa fa-close"></i>
                                                                    Cancel
                                                                </button>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>

                    <br>
                    @php
                        $District_comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 3)->first();
                        $Wda_comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 3)->first();
                    @endphp
                    <br>
                    @include('school.accr.district_display')
                    <br>
                    @include('school.accr.wda_display')
                    <br>
                    <h5><i class="fa fa-check-circle"></i> Programs Offered</h5>
                    <table class="table table-bordered table-hover">
                        <thead style="background: #efefef">
                        <tr>
                            <th>#</th>
                            <th>Sector</th>
                            <th style="width: 15%">Sub-Sector</th>
                            <th style="width: 15%">RTQF Level</th>
                            <th style="width: 10%">Male Trainees</th>
                            <th style="width: 10%">Female Trainees</th>
                            <th>Curriculum</th>

                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach($programs_offered as $pd)

                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $pd->subsector->sector->tvet_field }}</td>
                                <td>{{ $pd->subsector->sub_field_name }}</td>
                                <td>{{ $pd->rtqf_level->level_name }}</td>

                                <td>{{ $pd->male_trainees }}</td>
                                <td>{{ $pd->female_trainees }}</td>
                                <td>{{ $pd->curriculum }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <br>
                    @php
                        $District_comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                        $Wda_comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                    @endphp
                    <br>
                    @include('school.accr.district_display')
                    <br>
                    @include('school.accr.wda_display')
                    <br>
                    <h5><i class="fa fa-check-circle"></i> Trainer / Teachers</h5>
                    <table class="table table-bordered table-hover">
                        <thead style="background: #efefef">
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th style="width: 10%">Gender</th>
                            <th>Level / Degree</th>
                            <th>Qualification</th>
                            <th>Certification</th>
                            <th>Experience</th>
                            <th>Modules Taught</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach($trainers as $td)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $td->names }}</td>
                                <td>{{ $td->gender }}</td>
                                <td>{{ $td->q_level->name }}</td>
                                <td>{{ $td->qualification }}</td>
                                <td>{{ $td->certification }}</td>
                                <td>{{ $td->experience }}</td>
                                <td>{{ $td->modules_taught }}</td>

                            </tr>
                        @endforeach


                        </tbody>
                    </table>

                    <br>
                    @php
                        $District_comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                        $Wda_comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                    @endphp
                    <br>
                    @include('school.accr.district_display')
                    <br>
                    @include('school.accr.wda_display')
                    <br>
                    <h5><i class="fa fa-check-circle"></i> Administration Staff</h5>
                    <table class="table table-bordered table-hover">
                        <thead style="background: #efefef">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Position</th>
                            <th>Qualification</th>
                            <th>Institution Studied</th>
                            <th>Qualification Level</th>
                            <th>Teaching Experience</th>
                            <th>Assessor Qualification</th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach($staffs as $sd)
                            <tr>
                                <td></td>
                                <td>{{ $sd->names }}</td>
                                <td>{{ $sd->gender }}</td>
                                <td>{{ $sd->position }}</td>
                                <td>{{ $sd->academic_qualification }}</td>
                                <td>{{ $sd->institution_studied }}</td>
                                <td>{{ $sd->q_level->name }}</td>
                                <td>{{ $sd->teaching_experience }}</td>
                                <td>{{ $sd->assessor_qualification }}</td>

                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                    <br>
                    @php
                        $District_comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                        $Wda_comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                    @endphp
                    <br>
                    @include('school.accr.district_display')
                    <br>
                    @include('school.accr.wda_display')
                    <br>
                    <h5><i class="fa fa-check-circle"></i> Qualification Applied For</h5>
                    <table class="table table-bordered table-hover">
                        <thead style="background: #efefef">
                        <tr>
                            <th></th>
                            <th style="width: 30%">Sector</th>
                            <th style="width: 20%">Sub Sector</th>
                            <th style="width: 20%">Curriculum Qualification</th>
                            <th>RTQF Level</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($application_data as $ad)
                            <tr>
                                <td></td>
                                <td>{{ $ad->curr->subsector->sector->tvet_field }}</td>
                                <td>{{ $ad->curr->subsector->sub_field_name }}</td>
                                <td>{{ $ad->curr->qualification_title }}</td>
                                <td>{{ $ad->rtqf->level_name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br>
                    @php
                        $District_comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                        $Wda_comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                    @endphp
                    <br>
                    @include('school.accr.district_display')
                    <br>
                    @include('school.accr.wda_display')
                    <br>
                </div>
                <div class="box-footer"></div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".btnPrint").printPage();
        });
    </script>
@endsection