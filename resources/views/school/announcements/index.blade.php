@extends('school.announcements.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Announcements requirement</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            @if($announcements)
                <label> Announcements</label>
                @foreach($announcements as $announcement)
                    <div class="panel panel-success">
                        <div class="panel-body">
                            {!! $announcement->announcement !!}

                        </div>
                        <div class="panel-footer">
                            <label>Creation Date : </label> {{ $announcement->created_at->diffForHumans() }}
                            <span class="pull-right">
                                    <a href="{{ route('school.announcements.edit', $announcement->id) }}"
                                       class="btn btn-info btn-sm">Edit</a>
                                    <button type="button"
                                            data-url="{{ route('school.announcements.destroy', $announcement->id) }}"
                                            data-names="{{ $announcement->id }}"
                                            class="btn btn-danger btn-sm btn-delete">Delete</button>
                                </span>
                        </div>
                    </div>
                @endforeach
            @else
                <p><label class="text-green"> No Announcements Available</label></p>
            @endif
        </div>
    </div>
    @include('school.announcements.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection