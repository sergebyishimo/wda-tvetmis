<div class="list-group list-group-horizontal">
    {{--@if( activeMenu('school.students.index') == "" )--}}
    <a href="{{ route('school.announcements.index') }}"
       class="list-group-item {{ activeMenu('school.announcements.index') }}">View Announcements</a>
    {{--@endif--}}
    <a href="{{ route('school.announcements.create') }}"
       class="list-group-item {{ activeMenu('school.announcements.create') }}">
        Add Announcement</a>
</div>