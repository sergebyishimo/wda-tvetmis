@extends('school.layout.auth')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">Dashboard</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Students</h2>
                        </div>
                        <div class="panel-body">
                            <h2 class="text-center">{{ school(true)->students()->where('status','active')->count() }}</h2>
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Staffs</h2>
                        </div>
                        <div class="panel-body">
                            <h2 class="text-center">{{ school(true)->staffs()->where('status','active')->count() }}</h2>
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Departments <sup>Qualifications</sup></h2>
                        </div>
                        <div class="panel-body">
                            <h2 class="text-center">{{ school(true)->departments()->where('status',1)->count() }}</h2>
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Class <sup>Levels</sup></h2>
                        </div>
                        <div class="panel-body">
                            <h2 class="text-center">{{ school(true)->levels()->where('status',1)->count() }}</h2>
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Male</h2>
                        </div>
                        <div class="panel-body">
                            <h2 class="text-center">{{ school(true)->students()
                                                                   ->where('status','active')
                                                                   ->where('gender', "Male")
                                                                   ->count() }}</h2>
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Female</h2>
                        </div>
                        <div class="panel-body">
                            <h2 class="text-center">{{ school(true)->students()
                                                                   ->where('status','active')
                                                                   ->where('gender', "Female")
                                                                   ->count() }}</h2>
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                @if(school('studying_mode') == 'Both')
                    <div class="col-md-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h2 class="panel-title text-center">Study Mode: Day</h2>
                            </div>
                            <div class="panel-body">
                                <h2 class="text-center">{{ school(true)->students()
                                                                   ->where('status','active')
                                                                   ->where('mode', "Day")
                                                                   ->count() }}</h2>
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h2 class="panel-title text-center">Study Mode: Boarding</h2>
                            </div>
                            <div class="panel-body">
                                <h2 class="text-center">{{ school(true)->students()
                                                                   ->where('status','active')
                                                                   ->where('mode', "Boarding")
                                                                   ->count() }}</h2>
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
