@extends('school.fees.layout.main')

@section('l-style')

@overwrite

@section('panel-title')
    <span>Fees</span>
@endsection

@section('panel-body')
    <div class="box">

        {!! Form::open([ 'route' => 'school.fees.update', 'id' => 'addPayment', 'class' => 'form', 'method' => 'post', 'files' => true ]) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('student', "Student *") !!}
                        {!! Form::select('student', getFeeStudentsFormat(), null, ['class' => 'form-control select2', 'placeholder' => 'Select a student']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('term', "Term *") !!}
                        {!! Form::select('term', getTerm(), school(true)->settings->active_term?:null, ['class' => 'form-control select2', 'placeholder' => 'Select a term']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('amount', "Amount Paid *") !!}
                        {!! Form::number('amount', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('slip_number', "Slip Number") !!}
                        {!! Form::text('slip_number', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('bank_name', "Bank Name") !!}
                        {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('bank_slip', "Bank Slip") !!}
                        {!! Form::file('bank_slip', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-md btn-primary pull-left" type="submit">Save Change</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@overwrite

@section('l-scripts')
    @parent
    {!! JsValidator::formRequest('App\Http\Requests\AddPaymentRequest', '#addPayment'); !!}
    <script>

    </script>
@endsection