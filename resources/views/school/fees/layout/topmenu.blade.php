<div class="list-group list-group-horizontal">
    <a href="{{ route('school.fees.create') }}" class="list-group-item {{ activeMenu(['school.fees.create']) }}" >Add Payment Record</a>
    <a href="#" class="list-group-item {{ activeMenu(['school.fees.payment']) }}" data-toggle="modal" data-target="#additionalFees">Additional Fees</a>
    <a href="#" class="list-group-item {{ activeMenu(['school.fees.additional']) }}" data-toggle="modal" data-target="#queries">Make Query</a>
</div>