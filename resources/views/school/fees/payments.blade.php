@extends('school.fees.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
@overwrite

@section('box-title')
    <span>Fees</span>
@endsection

@section('box-body')
    <div class="box">
        @if($based == 's')
            <div class="box-header">
                <h5 class="box-title row">
                    <span class="pull-left col-md-6 text-success">Names:&nbsp;{{ trim(strtoupper($student->fname." ".$student->lname)) }}</span>
                    <span class="col-md-6 text-primary">
                        <u class="pull-right">{{ $student->reg_no }}</u>
                        <spanl class="pull-right">Student Number:&nbsp;</spanl>
                    </span>
                </h5>
            </div>
        @endif
        @if($based == 'c')
            <div class="box-header">
                <h5 class="box-title">{{ $level->rtqf->level_name }}
                    - {{ $level->department->qualification->qualification_title }}</h5>
            </div>
        @endif
        @if($based == 'c')
            {!! Form::open([ 'route' => 'school.fees.updating', 'class' => 'form inline-form', 'method' => 'post' ]) !!}
            {!! Form::hidden('term', $term) !!}
            {!! Form::hidden('year', $year) !!}
        @endif
        <div class="box-body">
            @if($based == 's')
                <p><h3>{{ $student->level->rtqf->level_name }}
                    - {{ $student->level->department->qualification->qualification_title }}</h3></p>
                <p><h5>{{ "Study Mode: ".$student->mode }}</h5></p>
                <p><h6>{{ "Term ".$term." | Academic Year: ".$year }}</h6></p>
                <div class="row">
                    <div class="col-md-12">
                        @php
                            $ex = getExpectedFees($student->level->id, $term, strtolower($student->mode));
                            $pa = getPaidFees($student->id, $term, $year);
                            $re = getRemainPayment($ex, $pa);
                        @endphp
                        <table class="table table-bordered">
                            <thead>
                            <tr class="bg-info" align="center">
                                <th>Expected Amount / RWF</th>
                                @if($additionalFees != null)
                                    @foreach($additionalFees as $item)
                                        <th>{{ ucwords($item->label) }} / RWF</th>
                                    @endforeach
                                @endif
                                <th>Paid Amount / RWF</th>
                                <th>Remaining Amount / RWF</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr align="center">
                                <td>{{ number_format($ex) }}</td>
                                @if($additionalFees != null)
                                    @foreach($additionalFees as $item)
                                        <th>{{ ucwords($item->amount) }}</th>
                                    @endforeach
                                @endif
                                <td>{{ number_format($pa) }}</td>
                                <td>
                                    {{ number_format($re) }}
                                    @if($additionalFees)
                                        {{ " + " . number_format($additionalFees->sum('amount')) }}
                                        {{ " = " . (number_format($additionalFees->sum('amount') + $re)) }}
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        @if($pa < $ex)
                            <div class="box box-default">
                                <div class="box-body">
                                    {!! Form::open([ 'route' => 'school.fees.update', 'id' => 'addPayment', 'class' => 'form', 'method' => 'post', 'files' => true ]) !!}
                                    {!! Form::hidden('student', $student->id) !!}
                                    {!! Form::hidden('term', $term) !!}
                                    {!! Form::hidden('year', $year) !!}

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('amount', "Amount Paid *") !!}
                                                {!! Form::number('amount', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('slip_number', "Slip Number") !!}
                                                {!! Form::text('slip_number', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('bank_name', "Bank Name") !!}
                                                {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('bank_slip', "Bank Slip") !!}
                                                {!! Form::file('bank_slip', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-md btn-primary pull-left" type="submit">Save
                                                    Change
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
            @if($based == 'c')
                <table id="dataTable" class="table">
                    <thead>
                    <tr>
                        <th>Student Number</th>
                        <th>Student Names</th>
                        <th>Expected Amount</th>
                        <th>Paid Amount</th>
                        <th>Remain Amount</th>
                        <th>New Payment</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($students->count() > 0)
                        @foreach($students->get() as $student)
                            @php
                                $ex = getExpectedFees($student->level->id, $term, strtolower($student->mode));
                                $pa = getPaidFees($student->id, $term, $year);
                                $re = getRemainPayment($ex, $pa);
                            @endphp
                            <tr>
                                <td>{{ $student->reg_no }}</td>
                                <td>{{ trim(strtoupper($student->fname." ".$student->lname)) }}</td>
                                <td>{{ number_format($ex) }}</td>
                                <td>{{ number_format($pa) }}</td>
                                <td>{{ number_format($re) }}</td>
                                <td>
                                    @if($pa < $ex)
                                        <div class="form-group" id="fg-{{ $student->reg_no }}">
                                            <input type="number" name="student[{{$student->id}}]" value="0"
                                                   class="form-control">
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            @endif
        </div>
        @if($based == 'c')
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-md btn-primary pull-left" type="submit">Save Change</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        @endif
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable();
        });
    </script>
    <!-- Laravel Javascript Validation -->
    @if($based == 's')
        @if($pa < $ex)
            {!! JsValidator::formRequest('App\Http\Requests\AddPaymentRequest', '#addPayment'); !!}
        @endif
    @endif
    <script>

    </script>
@endsection