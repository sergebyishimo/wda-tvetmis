@extends('school.fees.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .fees-table th {
            font-size: smaller;
            font-weight: 500;
        }

        .fees-table input[type="text"] {
            font-size: smaller;
        }
    </style>
@overwrite

@section('panel-title')
    <span>Fees</span>
@endsection

@section('panel-body')
    <div class="box box-default">

        {!! Form::open([ 'route' => 'school.fees.store', 'id' => 'updateFees', 'class' => 'form', 'method' => 'post' ]) !!}
        <div class="box-body">
            <table class="table table-bordered fees-table">
                <thead>
                <tr>
                    <th></th>
                    <th {!! school('studying_mode') == "Both" ? 'colspan="2"' : '' !!} style="text-align: center;">1<sup>rst</sup> Term</th>
                    <th {!! school('studying_mode') == "Both" ? 'colspan="2"' : '' !!} style="text-align: center;">2<sup>nd</sup> Term</th>
                    <th {!! school('studying_mode') == "Both" ? 'colspan="2"' : '' !!} style="text-align: center;">3<sup>rd</sup> Term</th>
                </tr>
                <tr align="center">
                    <th width="80px">Level Name</th>
                    @if(school('studying_mode') == "Boarding" || school('studying_mode') == "Both")
                        <th>Boarding</th>
                    @endif
                    @if(school('studying_mode') == "Day" || school('studying_mode') == "Both")
                        <th>Day</th>
                    @endif
                    @if(school('studying_mode') == "Boarding" || school('studying_mode') == "Both")
                        <th>Boarding</th>
                    @endif
                    @if(school('studying_mode') == "Day" || school('studying_mode') == "Both")
                        <th>Day</th>
                    @endif
                    @if(school('studying_mode') == "Boarding" || school('studying_mode') == "Both")
                        <th>Boarding</th>
                    @endif
                    @if(school('studying_mode') == "Day" || school('studying_mode') == "Both")
                        <th>Day</th>
                    @endif
                </tr>
                </thead>

                <tbody>

                @if($departments->count() > 0)
                    @foreach($departments->get() as $department)
                        @if($department->levels->count() > 0)
                            <tr style="background-color: #f9f9f9;">
                                <td {!! school('studying_mode') == "Both" ? 'colspan="7"' : 'colspan="4"' !!} align="center">
                                    <b>{{ ucwords($department->qualification->qualification_title) }}</b>
                                </td>
                            </tr>
                            @foreach($department->levels as $level)
                                <input type="hidden" name="level_id[]" value="{{ $level->id }}">
                                <tr>
                                    <td>{{ $level->rtqf->level_name }}</td>
                                    @if(school('studying_mode') == "Boarding" || school('studying_mode') == "Both")
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control" maxlength="6"
                                                   id="term1Bo{{ $level->id }}"
                                                   name="term1Bo[]" onkeypress="return isNumber(event)"
                                                   placeholder="0"
                                                   value="{{ getExpectedFees($level->id, '1', 'boarding')?:'' }}">
                                            <span class="input-group-addon" id="basic-addon1">RWF</span>
                                        </div>

                                    </td>
                                    @endif
                                    @if(school('studying_mode') == "Day" || school('studying_mode') == "Both")
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control" maxlength="6"
                                                   id="term1Day{{ $level->id }}"
                                                   name="term1Day[]" onkeypress="return isNumber(event)"
                                                   placeholder="0"
                                                   value="{{ getExpectedFees($level->id, '1', 'day')?:'' }}">
                                            <span class="input-group-addon" id="basic-addon1">RWF</span>
                                        </div>
                                    </td>
                                    @endif
                                    @if(school('studying_mode') == "Boarding" || school('studying_mode') == "Both")
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control" maxlength="6"
                                                   id="term2Bo{{ $level->id }}"
                                                   name="term2Bo[]" onkeypress="return isNumber(event)"
                                                   placeholder="0"
                                                   value="{{ getExpectedFees($level->id, '2', 'boarding')?:'' }}">
                                            <span class="input-group-addon" id="basic-addon1">RWF</span>
                                        </div>
                                    </td>
                                    @endif
                                    @if(school('studying_mode') == "Day" || school('studying_mode') == "Both")
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control" maxlength="6"
                                                   id="term2Day{{ $level->id }}"
                                                   name="term2Day[]" onkeypress="return isNumber(event)"
                                                   placeholder="0"
                                                   value="{{ getExpectedFees($level->id, '2', 'day')?:'' }}">
                                            <span class="input-group-addon" id="basic-addon1">FRW</span>
                                        </div>
                                    </td>
                                    @endif
                                    @if(school('studying_mode') == "Boarding" || school('studying_mode') == "Both")
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control" maxlength="6"
                                                   id="term3Bo{{ $level->id }}"
                                                   name="term3Bo[]" onkeypress="return isNumber(event)"
                                                   placeholder="0"
                                                   value="{{ getExpectedFees($level->id, '3', 'boarding')?:'' }}">
                                            <span class="input-group-addon" id="basic-addon1">RWF</span>
                                        </div>
                                    </td>
                                    @endif
                                    @if(school('studying_mode') == "Day" || school('studying_mode') == "Both")
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control" maxlength="6"
                                                   id="term3Day{{ $level->id }}"
                                                   name="term3Day[]" onkeypress="return isNumber(event)"
                                                   placeholder="0"
                                                   value="{{ getExpectedFees($level->id, '3', 'day')?:'' }}">
                                            <span class="input-group-addon" id="basic-addon1">FRW</span>
                                        </div>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-md btn-primary pull-left" type="submit">Save Change</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('school.fees.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UpdateFeesRequest', '#updateFees'); !!}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
@endsection