@extends('school.activityreport.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Activity Report Management</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th>School Name</th>
                <th>Activity Description</th>
                <th>Implemented From</th>
                <th>Implemented To</th>
                <th>Impact of Activity</th>
                <th>Activity Completed</th>
                <th>Activity Progress Details</th>
                <th>Created At</th>
                <th></th>
                </thead>
                <tbody>
                @if($reports->count() > 0)
                    @foreach($reports as $report)
                        <tr>
                            <td>
                                {{ $report->school->school_name }}
                            </td>
                            <td>
                                {{ $report->activity_description }}
                            </td>
                            <td>
                                {{ $report->implemented_from }}
                            </td>
                            <td>
                                {{ $report->implemented_to }}
                            </td>
                            <td>
                                {{ $report->impact_of_activity }}
                            </td>
                            <td>
                                {{ $report->activity_completed }}
                            </td>
                            <td>
                                {{ $report->activity_progress_details }}
                            </td>
                            <td>
                                {{ $report->created_at->diffForHumans() }}
                            </td>
                            <th>
                                <a href="{{ route('school.activityreports.edit', $report->id) }}" class="btn btn-info btn-sm">Edit</a>
                                <button type="button" data-url="{{ route('school.activityreports.destroy', $report->id) }}"
                                        data-names="{{ $report->id }}"
                                        class="btn btn-danger btn-sm btn-delete">Delete
                                </button>
                            </th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('school.activityreport.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection