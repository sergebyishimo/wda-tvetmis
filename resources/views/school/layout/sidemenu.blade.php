@if(auth()->guard('school')->check())
    <li class="{{ activeMenu('school.home') }}">
        <a href="{{ route('school.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>
    @if(Gate::check('isSchoolManager') || Gate::check('isTeachOrStaff'))
        <li class="{{ activeMenu(['school.staff.index','school.staff.create','school.staff.edit'])}}">
            <a href="{{ route('school.staff.index') }}"><i
                        class='fa fa-building'></i>&nbsp;<span>Staff Management</span></a>
        </li>

    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(3))
    <li class="{{ activeMenu(['school.get.info'])}}">
            <a href="{{ route('school.get.info') }}"><i
                        class='fa fa-gear'></i>&nbsp;<span>School Management</span></a>
        </li>
    @endif

    @if(isAllowed() || isAllowed(1) || isAllowed(5))
        <li class="{{ activeMenu(['school.class.index'])}}">
            <a href="{{ route('school.class.index') }}"><i
                        class='fa fa-key'></i>&nbsp;<span>Qualification Management</span></a>
        </li>
    @endif

    @if(isAllowed() || isAllowed(1) || isAllowed(5))
        <li class="{{ activeMenu([
        'school.course.index',
        'school.course.show'
    ]) }}">
            <a href="{{ route('school.course.index') }}"><i
                        class='fa fa-puzzle-piece'></i>&nbsp;<span>Courses Management</span></a>
        </li>
    @endif
    {{--@if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5))--}}

        @if(Gate::check('isSchoolManager') || Gate::check('isTeachOrStaff'))
        <li class="{{ activeMenu([
    'school.students.index',
    'school.students.create',
    'school.students.edit',
    'school.students.upload',
    'school.students.upload.pictures',
    'school.students.view.class',
    'school.p.students.view.class'
    ]) }}">
            <a href="{{ route('school.students.index') }}"><i
                        class='fa fa-bus'></i>&nbsp;<span>Students Management</span></a>
        </li>
    {{--@endcan--}}
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(4))
        <li class="{{ activeMenu([
    'school.fees.index',
    'school.fees.create',
    'school.fees.payment',
    'school.fees.view',
    'school.fees.edit'
    ]) }}">
            <a href="{{ route('school.fees.index') }}"><i class='fa fa-bank'></i>&nbsp;<span>Finance Manager</span></a>
        </li>
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(7) || isAllowed(6))
        <li class="{{ activeMenu([
    'school.discipline.index',
    'school.discipline.create',
    ]) }}">
            <a href="{{ route('school.discipline.index') }}"><i
                        class='fa fa-bullseye'></i>&nbsp;<span>Discipline Management</span></a>
        </li>
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(6))
        <li class="{{ activeMenu([
           'school.permission.index',
           'school.permission.verify'
           ]) }}">
            <a href="{{ route('school.permission.index') }}"><i
                        class='fa fa-pagelines'></i>&nbsp;<span>Permissions Management</span></a>
        </li>
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5) || isAllowed(7))
        <li class="{{ activeMenu([
       'school.marks.index',
       'school.marks.period.entry',
       'school.marks.get.exam',
       'school.marks.view.exam',
       'school.marks.term.period.exam',
       'school.marks.file.exam',
       'school.marks.periodic.results',
       'school.reporting.marks'
       ]) }}">
            <a href="{{ route('school.marks.index') }}"><i
                        class='fa fa-book'></i>&nbsp;<span>Marks Management</span></a>
        </li>
    @endif
    <li class="{{ activeMenu(['school.gallery.index', 'school.gallery.show']) }}">
        <a href="{{ route('school.gallery.index') }}"><i class='fa fa-link'></i>&nbsp;<span>Photo Gallery</span></a>
    </li>
    @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5) || isAllowed(6) )
        <li class="{{ activeMenu([
       'school.attendance.index',
       'school.attendance.event',
       'school.attendance.in_out',
       'school.attendance.staff',
       'school.attendance.visiting',
       ]) }}">
            <a href="{{ route('school.attendance.index') }}"><i class='fa fa-bars'></i>&nbsp;<span>Attendance</span></a>
        </li>
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(3) )
        <li class="{{ activeMenu(['school.sms.index', 'school.sms.send']) }}">
            <a href="{{ route('school.sms.index') }}"><i class='fa fa-bookmark-o'></i>&nbsp;SMS</a>
        </li>
    @endif
    {{--@if(isAllowed() || isAllowed(1) || isAllowed(3))--}}
    {{--<li class="{{ activeMenu('school.get.-bookmark-o') }}">--}}
    {{--<a href="{{ route('school.get.info') }}"><i class='fa fa-server'></i>&nbsp;School Settings</a>--}}
    {{--</li>--}}
    {{--@endif--}}
    @if( ! isAllowed() && ! isAllowed(1))
        <li class="{{ activeMenu([
        'school.staff.update.myinfo',
        'school.staff.get.attachment',
        'school.staff.get.experience',
        'school.staff.get.teaching',
        'school.staff.profile'
    ])}}">
            <a href="{{ route('school.staff.profile') }}"><i class='fa fa-user'></i>&nbsp;My Profile</a>
        </li>
        <li class="{{ activeMenu(['school.teach.marker.application']) }}">
            <a href="{{ route('school.teach.marker.application') }}"><i class='fa fa-adjust'></i>&nbsp;Marker
                Application</a>
        </li>
        <li class="{{ activeMenu(['school.teach.assessor.application']) }}">
            <a href="{{ route('school.teach.assessor.application') }}"><i class='fa fa-adn'></i>&nbsp;Assessor
                Application</a>
        </li>
    @endif
@elseif(auth()->guard('reb')->check())
    <li class="{{ activeMenu([
    'reb.home',
    'reb.staff.get.attachment',
    'reb.staff.get.experience',
    'reb.staff.get.teaching'
    ]) }}">
        <a href="{{ route('reb.home') }}"><i class='fa fa-link'></i>&nbsp;<span>My Information</span></a>
    </li>
    <li class="{{ activeMenu(['reb.teach.marker.application']) }}">
        <a href="{{ route('reb.teach.marker.application') }}"><i class='fa fa-adjust'></i>&nbsp;Marker
            Application</a>
    </li>
@endif
