<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(Auth::guest())
        <title>{{ config('app.name', '') }}</title>
    @else
        <title>{{ config('app.name', '') ." | ". school('acronym') }}</title>
@endif

<!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha256-916EbMg70RQy9LHiGkXzG8hSg9EdNy97GazNG/aiY1w=" crossorigin="anonymous"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{ asset('/css/app.css?'.time()) }}" rel="stylesheet">

@yield('style')
<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="school-bg">
<nav class="navbar navbar-affix navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        @if(auth()->guard('school')->check())
            <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/school') }}">
                    {!! school('name') !!} <sub><i><b>
                                <small>{{ auth()->user()->schoolPrivilege->name }}</small>
                            </b></i></sub>
                </a>
            @endif
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/') }}">Home</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5) )
                                    <a href="{{ route('school.get.info') }}">School Settings</a>
                                @endif
                                <a href="" data-toggle="modal" data-url="" data-target="#changePassword">Change
                                    Password</a>
                                @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5) )
                                    <a href="" data-toggle="modal" data-target="#schoolSettings">Set Current Period</a>
                                    @if(school('website'))
                                        <a href="{{ url(school('website')) }}" target="_blank">Website</a>
                                    @endif
                                @endif
                                <a href="{{ url('/school/logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/school/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@if( ! Auth::guest() )
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                @include('school.layout.sidemenu')
            </div>
            <div class="col-md-10">
                @yield('content')
            </div>
        </div>
    </div>

    @include('school.layout.modals.setting')
    @include('school.layout.modals.chagePassword')
    @include('school.layout.modals.setPeriod')
    @if( ! checkIfCompletedInfo() )
        @include('school.staff.layout.modals.warnings')
    @endif
@else
    @yield('content')
@endif
<!-- Scripts -->
<script src="/js/app.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
@if(activeMenu('school.home') != '')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
            integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
@endif
@yield('script')
<script src="{{ asset('js/school.js?'.time()) }}"></script>
@if( ! checkIfCompletedInfo() && activeMenu('school.staff.update.myinfo') == "" )
    <script>
        $(function () {
            $("#notCompleted").modal({backdrop: 'static', keyboard: false});
        })
    </script>
@endif
</body>
</html>
