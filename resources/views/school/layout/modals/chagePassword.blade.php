<div class="modal fade" id="changePassword">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"> &times;</button>
                    <h4 class="modal-title"><i class="fa fa-key"></i> Change Your Password </h4>
                </div>
                @if (auth()->guard('school')->check())
                    <form method="POST" action="{{ route('school.change.password') }}">    
                @else
                    <form method="POST" action="{{ route('change.password') }}">
                @endif
                
                    {{ csrf_field() }}

                    <div class="modal-body">
                        <table style="width: 100%;line-height: 40px">
                            <tr>
                                <th>Old Password</th>
                                <td><input type="password" class="form-control" name="old_pass" id="old_pass"
                                           placeholder="Old Password" required></td>
                                <td id="oldPassMsg" style="padding-left: 10px;width: 10px;"></td>
                            </tr>
                            <tr>
                                <th>New Password</th>
                                <td><input type="password" class="form-control" id="newPass" name="new_pass"
                                           placeholder="New Password" disabled required></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Conf. Password</th>
                                <td><input type="password" class="form-control" id="confPass" name="conf_pass"
                                           placeholder="Confirm Password" disabled required></td>
                                <td id="confPassMsg" style="padding-left: 10px;width: 10px;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary pull-left"><span class="glyphicon glyphicon-ok"
                                                                            aria-hidden="true"></span> Save
                        </button>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><span
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>