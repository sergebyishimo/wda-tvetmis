<div class="modal fade" id="schoolSettings">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"> &times;</button>
                <h4 class="modal-title"><i class="fa fa-user-plus"></i> Set School Period - Academic
                    Year {{ date('Y') }} </h4>
            </div>
            {!! Form::open(['method' => 'post', 'route' => 'school.save.settings', 'method' => 'post']) !!}
            <div class="modal-body">
                <table style="width: 100%">
                    <tr>
                        <td>Current Term</td>
                        <td>
                            {!! Form::select('term', getTerm(), getTerm(true), ['class' => 'form-control select2', 'required' => 1]) !!}
                        </td>
                        <td>Current Period</td>
                        <td id="period">
                            {!! Form::select('period', getPeriod(), getPeriod(true), ['class' => 'form-control select2', 'required' => 1]) !!}
                        </td>

                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>