@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        @yield('panel-title')
    </div>
@endsection

@section('l-style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
          integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>
    <style>
        input[name='based'] {
            margin-top: -6px !important;
        }
        .radio{
            background-color: #0c0c0c;
        }
    </style>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div style="padding-left: 3px;">
                    @include('school.attendance.layout.topmenu')
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('feedback.feedback')
            </div>
        </div>
        @yield('panel-body')
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
            integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\SearchPaymentRequest', '#searchQForm'); !!}
    <script>
        $(function () {
            $(".select2").select2();
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                endDate: "0d",
                orientation: "bottom"
            });
        })
    </script>
    <script>
        // $(".level-group").hide();
        $(".student-q").on("change", function () {
            $("#studentQ").attr("checked", true);
        });
        $(".class-q").on("change", function (e) {
            var op = $(this).val();
            $("#classQ").attr("checked", true);
            $.get("{{ route('school.get.course.levels') }}",
                {
                    id: op
                }, function (data) {
                    var h = '<option value="" selected disabled>Select ...</option>';
                    $.each(JSON.parse(data), function (index, level) {
                        h += '<option value="' + level.id + '">' + level.name + '</option>';
                    });
                    $(".level-group select").html(h);
                });
        });
    </script>
@endsection