<div class="list-group list-group-horizontal">
    <a href="{{ route('school.attendance.index') }}"
       class="list-group-item {{ activeMenu(['school.attendance.index']) }}">Course</a>
    @if(school('studying_mode') == 'Day' || school('studying_mode') == 'Both' )
        <a href="{{ route('school.attendance.in_out') }}"
           class="list-group-item {{ activeMenu([
           'school.attendance.in_out',
           ]) }}">IN & OUT</a>
    @endif
    <a href="{{ route('school.attendance.event') }}"
       class="list-group-item {{ activeMenu(['school.attendance.event']) }}">Event</a>
    <a href="{{ route('school.attendance.staff') }}"
       class="list-group-item {{ activeMenu(['school.attendance.staff']) }}">Staffs</a>
    <a href="{{ route('school.attendance.visiting') }}"
       class="list-group-item {{ activeMenu(['school.attendance.visiting']) }}">Visiting</a>
</div>