@extends('school.attendance.layout.main-att')

@section('contentheader_title')
    <div class="container-fluid">
        View Event Attendance
    </div>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="box-title pull-left">View Event Attendance for
                        <b>{{ strtoupper($type) }}</b> taken
                        in <b>{{ date("F, Y", strtotime($month)) }}</b></h3>
                    <button class="btn btn-primary btn-sm pull-right" id="btnExport">Export Excel</button>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div id="dvData">
                <div class="row">
                    <div class="col-md-4">
                        <table class="table table-responsive table-condensed">
                            <tr>
                                <td width="100px">Event</td>
                                <td align="top"><b>{{ strtoupper($type) }}</b></td>
                            </tr>
                            <tr>
                                <td>Department</td>
                                <td align="top">
                                    <b>{{ $class ? ucwords($class->department->qualification->qualification_title) : "" }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td>Class</td>
                                <td align="top"><b>{{ $class ? strtoupper($class->rtqf->level_name) : "" }}</b></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td align="top"><b><?= date("F,  Y", strtotime($month)) ?></b></td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed table-responsive" border="1">
                            <thead>
                            <tr>
                                <td colspan="2" align="center">
                                    <span class=""><b>Students Names</b></span>
                                </td>
                                <?php for ($x = 1; $x <= $number; $x++): ?>
                                <td style="width: 30px;" align="center"><b><?= sprintf("%02d", $x); ?></b></td>
                                <?php endfor; ?>
                                <td style="width: 100px;"><b>Total</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @if($students->count() > 0)
                                @php($xx = 1)
                                @foreach($students->get() as $student)
                                    @php($classAtt = eventAttendance($month, $student->id, $event))
                                    <tr>
                                        <td align="center" width="30px"> <?= $xx++; ?> </td>
                                        <td style="width: 240px;" align="left">
                                           <span>
                                               {{ ucwords(trim($student->fname." ".$student->lname)) }}
                                           </span>
                                        </td>
                                        <?php for ($x = 1; $x <= $number; $x++): ?>
                                        <td align="center"
                                            style="background-color: {{ $classAtt->isAttend($x) == '' ? '#eee;' : '' }}">
                                            {{ $classAtt->isAttend($x) }}
                                        </td>
                                        <?php endfor; ?>
                                        <td>&nbsp;</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="{{ $number + 3 }}" align="center">No students find !!</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tr>
                            <td style="background-color: #eee;" width="30px">&nbsp;</td>
                            <td><b>Weekend Day</b></td>

                            <td class="" width="30px" align="center">-</td>
                            <td><b>No {{ $type }} Taken</b></td>

                            <td class="" width="30px" align="center">P</td>
                            <td><b>Student Attended</b></td>

                            <td class="" width="30px" align="center">A</td>
                            <td><b>Student Didn't Attend</b></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script>
        $("#btnExport").click(function (e) {
            let file = new Blob([$('#dvData').html()], {type: "application/vnd.ms-excel"});

            let url = URL.createObjectURL(file);
            let name = "{{ 'Attendance_'.date("F_Y", strtotime($month))."_".$level->rtqf->level_id }}";
            let a = $("<a />", {
                href: url,
                download: name + ".xls"
            })
                .appendTo("body")
                .get(0)
                .click();
            e.preventDefault();
        });
    </script>
@endsection