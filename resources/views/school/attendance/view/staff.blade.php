@extends('school.attendance.layout.main-att')

@section('contentheader_title')
    <div class="container-fluid">
        View Staff Attendance
    </div>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="box-title pull-left">View Staff Attendance From {{ $attendance->getStartDate() }}
                        To {{ $attendance->getEndDate() }}</h3>
                    <button class="btn btn-primary btn-sm pull-right" id="btnExport">Export Excel</button>
                </div>
            </div>
        </div>
        <div class="box-body" style="overflow-x: scroll">
            <div id="dvData">
                @php
                    $cols = 0;
                    $dates = array();
                @endphp
                <table border="1" class="table table-bordered table-striped">
                    <thead>
                    <tr align="center">
                        <td width="40px">#</td>
                        <td align="center">Names</td>
                        <td width="150px">Working Days</td>
                        <td width="150px">Worked Days</td>
                        <td width="150px">Absent Days</td>
                        <td width="180px" align="center">
                            <span> Attendance Rate </span>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @php($xp = 1)
                    @foreach($staffs as $staff)
                        @php($attendance->checkStaff($staff->id))
                        <tr align="center">
                            <td>{{ $xp++ }}</td>
                            <td>{{ $staff->names }}</td>
                            <td>{{ $attendance->normalWDay() }}</td>
                            <td>{{ $attendance->getWorkedDay() }}</td>
                            <td class="{{ $attendance->getAttRate() < 50 ? 'bg-danger' : '' }}">{{ $attendance->getAbsentDay() }}</td>
                            <td align="center" class="{{ $attendance->getAttRate() < 50 ? 'bg-danger' : '' }}">
                                {{ $attendance->getAttRate() ." %" }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@overwrite

@section('script')
    @parent
    <script>
        $("#btnExport").click(function (e) {
            let file = new Blob([$('#dvData').html()], {type: "application/vnd.ms-excel"});

            let url = URL.createObjectURL(file);
            let name = "in_out_attendance";
            let a = $("<a />", {
                href: url,
                download: name + ".xls"
            })
                .appendTo("body")
                .get(0)
                .click();
            e.preventDefault();
        });
    </script>
@endsection