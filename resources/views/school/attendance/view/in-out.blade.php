@extends('school.attendance.layout.main-att')

@section('contentheader_title')
    <div class="container-fluid">
        View in/out Attendance
    </div>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="box-title pull-left">View In/Out Attendance</h3>
                    <button class="btn btn-primary btn-sm pull-right" id="btnExport">Export Excel</button>
                </div>
            </div>
        </div>
        <div class="box-body" style="overflow-x: scroll">
            <div id="dvData">
                @php
                    $cols = 0;
                    $dates = array();
                @endphp
                <table border="1" class="table table-bordered table-condensed">
                    <thead>
                    <tr>
                        <td rowspan="2" colspan="2" align="center"></td>
                        @foreach ($daterange as $date)
                            <td colspan="2" align="center" style="padding: 3px;table-layout: fixed;">
                                <span style="font-size: 10px;">
                                    {{ $date->format("d-M-Y") }}
                                    @php($dates[] = $date->format("Y-m-d"))
                                </span>
                            </td>
                            @php($cols++)
                        @endforeach
                        <td colspan="2" align="center" style="padding: 3px;table-layout: fixed;">
                            <span style="font-size: 10px;"> Total </span>
                        </td>
                    </tr>
                    <tr>
                        @foreach ($daterange as $date)
                            <td align="center" style="width: 100px !important;">
                                <span style="font-size: 10px;"> IN&nbsp; </span>
                            </td>
                            <td align="center" style="width: 100px !important;">
                                <span style="font-size: 10px;"> OUT </span>
                            </td>
                        @endforeach
                        <td align="center" style="width: 100px !important;">
                            <span style="font-size: 10px;"> Presence&nbsp;</span>
                        </td>
                        <td align="center" style="width: 100px !important;">
                            <span style="font-size: 10px;"> Absence&nbsp;</span>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @php($xp = 1)
                    @foreach($students as $student)
                        @php($pr = 0)
                        @php($ab = 0)
                        <tr>
                            <td align="center"> {{ $xp++ }} </td>
                            <td style="width: 250px;">
                                <span >
                                    {{ strtoupper(trim($student->fname . " " . $student->lname)) }}
                                </span>
                            </td>
                            @for ($x = 1; $x <= ($cols); $x++ )
                                @php($in = $attendance->get($student->id, $dates[$x - 1], "IN"))
                                @php($out = $attendance->get($student->id, $dates[$x - 1], "OUT"))
                                <td align="center"
                                    style="background-color: <?=  $in == null ? 'rgba(0,0,0,.03)' : 'rgba(0,255,0,.2)' ?>; width: 100px !important;">
                                    @if( $in != null )
                                        @php($pr += 1)
                                        {{ "P-" . $in }}
                                    @else
                                        @php($ab += 1)
                                        <span style="font-size: 10px" class="glyphicon glyphicon-remove"></span>
                                    @endif
                                </td>
                                <td align="center"
                                    style="background-color: <?=  $out == null ? 'rgba(0,0,0,.03)' : 'rgba(0,255,0,.2)' ?>; width: 100px !important;">
                                    {!! $out != null ? "P-" . $out : '<span style="font-size: 10px" class="glyphicon glyphicon-remove"></span>' !!}
                                </td>
                            @endfor
                            <td align="center"
                                style="background-color: <?=  $in == null ? 'rgba(0,0,0,.03)' : 'rgba(0,255,0,.2)' ?>; width: 100px !important;">{!! $pr !!}</td>
                            <td align="center"
                                style="background-color: <?=  $in == null ? 'rgba(0,0,0,.03)' : 'rgba(0,255,0,.2)' ?>; width: 100px !important;">{!! $ab !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tr>
                            <td class="" width="30px" align="center">
                                <span style="font-size: 10px" class="glyphicon glyphicon-remove"></span>
                            </td>
                            <td><b>Student Didn't Attend</b></td>

                            <td class="" width="40px" align="center">P - </td>
                            <td><b>Student Attended</b></td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script>
        $("#btnExport").click(function (e) {
            let file = new Blob([$('#dvData').html()], {type: "application/vnd.ms-excel"});

            let url = URL.createObjectURL(file);
            let name = "in_out_attendance";
            let a = $("<a />", {
                href: url,
                download: name + ".xls"
            })
                .appendTo("body")
                .get(0)
                .click();
            e.preventDefault();
        });
    </script>
@endsection