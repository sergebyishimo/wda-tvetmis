@extends('school.attendance.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>Attendance</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h5 class="box-title">Staff Attendance</h5>
        </div>
        {!! Form::open(['method' => 'post', 'route' => 'school.attendance.post.staff', 'target' => '_blank']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        {!! Form::label('startdate', "Start Date *") !!}
                        {!! Form::text('startdate', null, ['class' => 'form-control datepicker',
                        'placeholder'=>"Choose Start Date",
                        'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('enddate', "Ending Date *") !!}
                        {!! Form::text('enddate', null, ['class' => 'form-control datepicker',
                        'placeholder'=>"Choose End Date",
                        'required' => true]) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">View</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@overwrite

@section('l-scripts')
    @parent
@endsection