@extends('school.attendance.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>Attendance</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h5 class="box-title">Visiting</h5>
        </div>
        {!! Form::open(['method' => 'post', 'route' => 'school.attendance.post.visiting']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="qualification">Qualification <sup>(Department)</sup></label>
                        <select name="qualification" id="qualification" class="form-control select2 class-q"
                                style="width: 100%"
                                required>
                            <option value="" selected disabled>Select Qualification</option>
                            @foreach(getQualifications(school(true)->id) as $item)
                                <option value="{{ $item->id }}" @isset($request) {{ $request->input('qualification') == $item->id ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 period">
                    <div class="form-group level-group">
                        <label for="levelCourses">Level <sup>(Class)</sup></label>
                        <select name="level_id" id="levelCourses" class="form-control select2 class-l"
                                style="width: 100%"
                                required>
                            @if(isset($request))
                                <option value="{{ $request->input('level') }}"
                                        selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                            @else
                                <option value="" selected disabled>Select Level</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="date">Choose <sup>Visiting Date</sup></label>
                    <select name="date" id="date" class="form-control select2 module-l"
                            style="width: 100%" required>
                        <option value="">Select Date</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">View</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@overwrite

@section('l-scripts')
    @parent
@endsection