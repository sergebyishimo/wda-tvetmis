@extends('school.requests.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Requests Management</span>
@endsection

@section('box-body')
    <div class="box">
        @if($schoolrequest)
            @php($route = ['school.requests.update', $schoolrequest->id])
            @php($id = "updateSchoolRequests")
        @else
            @php($route = 'school.requests.store')
            @php($id = "createSchoolRequests")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
            {!! Form::hidden('school_id', school(true)->id) !!}
        @if($schoolrequest)
            @method("PATCH")
            {!! Form::hidden('id', $schoolrequest->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('request_class', "Request Class *") !!}
                        {!! Form::select('request_class', ['Consumables' => 'Consumables', 'Trainers' => 'Trainers','Trainings'=>'Trainings'], $schoolrequest ? $schoolrequest->request_class : null,
                            ['placeholder' => 'Choose a Request Class...', 'class' => 'form-control select2', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('required_units', "Required Unit *") !!}
                        {!! Form::text('required_units', $schoolrequest ? $schoolrequest->required_units : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('unit_cost', "Unit Cost *") !!}
                            {!! Form::text('unit_cost', $schoolrequest ? $schoolrequest->unit_cost : "", ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('request_description', "Request Description *") !!}
                        {!! Form::textarea('request_description', $schoolrequest ?$schoolrequest->request_description: "", ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('required_by_date', "Required  By Date *") !!}
                            {!! Form::text('required_by_date', $schoolrequest ? $schoolrequest->required_by_date : null, ['class' => 'datepicker form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('request_attachment', "Attachment") !!}
                        {!! Form::file('request_attachment', ['class' => 'filer_docs_input form-control']) !!}
                    </div>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $schoolrequest ? "Update Request" : "Send Request" }}
                    </button>
                    <a href="{{ route('school.requests.index') }}"
                       class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('school.requests.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($schoolrequest)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateSchoolRequestsRequest', '#updateSchoolRequests'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateSchoolRequestsRequest', '#createSchoolRequests'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: docx, xlsx,pdf or ppt");

            $("#qualification").on("change", function () {
                var dep = $(this).val();
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@endsection