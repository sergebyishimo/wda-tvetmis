@extends('school.requests.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css"
          integrity="sha256-0Z6mOrdLEtgqvj7tidYQnCYWG3G2GAIpatAWKhDx+VM=" crossorigin="anonymous"/>
    <style>
        .dropzone.dz-clickable {
            cursor: pointer;
        }

        .dropzone {
            border: 0px dashed #66FCF1;
            border-radius: 5px;
            background-color: rgba(31, 40, 51, 1);
            color: #FFFFFF;
        }

        .dropzone .dz-message {
            font-weight: 400;
        }

        .dropzone .dz-preview.dz-image-preview {
            background-color: transparent !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Reuquest Management</span>
@endsection

@section('box-body')
    @parent
    <div class="box">
        <div class="row">
            <div class="success-o" style="margin-left: 20px;margin-right: 20px;"></div>
        </div>
        <div class="box-body">
            {!! Form::open(['url' => '', 'files' => true, 'class' => 'dropzone', 'id' => 'my-awesome-dropzone']) !!}
            <div class="dz-message needsclick">
                <h3>Drop images here or click to select from directory.</h3><br>
                <span class="note needsclick text-warning">( Do <strong>not</strong> select more that <strong>40</strong>.)</span><br>
                <span class="note needsclick text-danger"><strong>Make sure name of image matches with <u>student's registration number</u></strong></span>
            </div>
            {!! Form::close() !!}

            {{--<form action="/file-upload"--}}
            {{--class="dropzone"--}}
            {{--id="my-awesome-dropzone">--}}
            {{--</form>--}}
        </div>
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"
            integrity="sha256-w75OOJSGJDHFCH5MXAIpXig0n6hhZoGUU/3Em3BuAT8=" crossorigin="anonymous"></script>
    <script>
        Dropzone.options.myAwesomeDropzone = {
            maxFilesize: 2, // MB
            maxFiles: 40,
            acceptedFiles: ".jpeg,.jpg,.png",
            url: "{{ route('school.students.upload.pictures.store') }}",
            success: function (file, v) {
                var o = '<lebal class="col-md-2 label label-success" style="margin-right: 5px;margin-top: 5px;">' + file.upload.filename + '&nbsp;&nbsp;<span class="glyphicon glyphicon-ok"></span></lebal>&nbsp;&nbsp;';
                $(".success-o").append(o);
                file.previewElement.classList.remove("dz-error");
                file.previewElement.classList.add("dz-success");
            },
            error: function (file, v) {
                var o = '<lebal class="col-md-2 label label-warning" style="margin-right: 5px;margin-top: 5px;">' + file.upload.filename + '&nbsp;&nbsp;<span class="glyphicon glyphicon-remove"></span></lebal>&nbsp;&nbsp;';
                $(".success-o").append(o);
                file.previewElement.classList.remove("dz-success");
                file.previewElement.classList.add("dz-error");
                $(file.previewElement).find('.dz-error-message').text(v);
            }
        };
    </script>
@endsection