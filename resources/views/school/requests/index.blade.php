@extends('school.requests.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Requests Management</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th>Request Class</th>
                <th>Number of Required Units</th>
                <th>unit cost</th>
                <th>Description</th>
                <th>Require by Date</th>
                <th>School Name</th>
                <th>Request Status</th>
                <th>WDA Comment</th>
                <th>Attachment</th>
                <th></th>
                </thead>
                <tbody>
                @if($schoolrequests->count() > 0)
                    @foreach($schoolrequests as $schoolrequest)
                        <tr>
                            <td>
                                {{ $schoolrequest->request_class }}
                            </td>
                            <td>
                                {{ $schoolrequest->required_units }}
                            </td>
                            <td>
                                <h5>
                                    {{ $schoolrequest->unit_cost }}
                                </h5>
                            </td>
                            <td>
                                {{ $schoolrequest->request_description }}
                            </td>
                            <td>
                                {{ $schoolrequest->required_by_date }}
                            </td>
                            <td>
                                {{ $schoolrequest->school->school_name}}
                            </td>
                            <td>
                                {{ $schoolrequest->request_status}}
                            </td>
                            <td>
                                {{ $schoolrequest->wda_comments}}
                            </td>
                            <td>
                                <a href="{{ asset('storage/'.$schoolrequest->request_attachment) }}">View</a>
                            </td>
                            <th>
                                <a href="{{ route('school.requests.edit', $schoolrequest->id) }}" class="btn btn-info btn-sm">Edit</a>
                                <button type="button" data-url="{{ route('school.requests.destroy', $schoolrequest->id) }}"
                                        data-names="{{ $schoolrequest->request_description }}"
                                        class="btn btn-danger btn-sm btn-delete">Delete
                                </button>
                            </th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('school.students.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection