@extends('school.incubationcenter.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Incubation Center</span>
@endsection

@section('box-body')
    <div class="box">
        @if($incubationcenter)
            @php($route = ['school.incubationcenters.update', $incubationcenter->id])
            @php($id = "updateIncubation")
        @else
            @php($route = 'school.incubationcenters.store')
            @php($id = "createIncubation")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
            {!! Form::hidden('school_id', school(true)->id) !!}
        @if($incubationcenter)
            @method("PATCH")
            {!! Form::hidden('id', $incubationcenter->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('club_name', "Club Name *") !!}
                        {!! Form::text('club_name', $incubationcenter ? $incubationcenter->club_name: "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('club_facilitator', "Club Facilitator *") !!}
                        {!! Form::text('club_facilitator', $incubationcenter ? $incubationcenter->club_facilitator: "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('incubatees_male', "Incubatees Male *") !!}
                        {!! Form::text('incubatees_male', $incubationcenter ? $incubationcenter->incubatees_male: "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('incubatees_female', "Incubatees Female *") !!}
                        {!! Form::text('incubatees_female', $incubationcenter ? $incubationcenter->incubatees_female: "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('average_age', "Average Age *") !!}
                        {!! Form::text('average_age', $incubationcenter ? $incubationcenter->average_age: "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('business_areas', "Business Areas *") !!}
                        {!! Form::text('business_areas', $incubationcenter ? $incubationcenter->average_age: "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('annual_turn_over_francs', "Annual Turn Over Francs *") !!}
                        {!! Form::text('annual_turn_over_francs', $incubationcenter ? $incubationcenter->annual_turn_over_francs: "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('funds_source', "Funds Source *") !!}
                        {!! Form::text('funds_source',$incubationcenter ? $incubationcenter->annual_turn_over_francs: "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('rdb_registration_number', "RDB Registration Number *") !!}
                        {!! Form::text('rdb_registration_number', $incubationcenter ? $incubationcenter->rdb_registration_number: "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('number_incubated_clubs', "Number Incubated Clubs *") !!}
                        {!! Form::text('number_incubated_clubs', $incubationcenter ? $incubationcenter->number_incubated_clubs: "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4 pull-right">
                    <div class="form-group">
                        {!! Form::label('attachments', "Attachments *") !!}
                        {!! Form::file('attachments', ['class' => 'filer_docs_input form-control']) !!}
                    </div>
                </div>
            </div>


        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $incubationcenter ? "Update Incubation" : "Save Incubation" }}
                    </button>
                    <a href="{{ route('school.incubationcenters.index') }}"
                       class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('school.requests.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($incubationcenter)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateIncubationCenterRequest', '#updateIncubation'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateIncubationCenterRequest', '#createIncubation'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $("#qualification").on("change", function () {
                var dep = $(this).val();
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@endsection