@extends('school.incubationcenter.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Incubation Centers</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th>Club Name</th>
                <th>Club Facilitator</th>
                <th>Incubatees Male</th>
                <th>Incubatees Female</th>
                <th>Average Age</th>
                <th>Business Area</th>
                <th>Annual Turn Over(RWF)</th>
                <th>Source of Funds</th>
                <th>RDB registration Number</th>
                <th>Number Of Incubated Clubs</th>
                <th>Attachment</th>
                <th>Created At</th>
                <th></th>
                </thead>
                <tbody>
                @if($incubationcenters->count() > 0)
                    @foreach($incubationcenters as $incubationcenter)
                        <tr>
                            <td>
                                {{ $incubationcenter->club_name }}
                            </td>
                            <td>
                                {{ $incubationcenter->club_facilitator }}
                            </td>
                            <td>
                                {{ $incubationcenter->incubatees_male }}
                            </td>
                            <td>
                                {{ $incubationcenter->incubatees_female }}
                            </td>
                            <td>
                                {{ $incubationcenter->average_age }}
                            </td>
                            <td>
                                {{ $incubationcenter->business_areas }}
                            </td>
                            <td>
                                {{ $incubationcenter->annual_turn_over_francs }}
                            </td>
                            <td>
                                {{ $incubationcenter->funds_source }}
                            </td>
                            <td>
                                {{ $incubationcenter->rdb_registration_number }}
                            </td>
                            <td>
                                {{ $incubationcenter->number_incubated_clubs }}
                            </td>
                            <td>
                                <a href="{{ asset('storage/'.$incubationcenter->attachments)}}">view attachment</a>
                            </td>
                            <td>
                                {{ $incubationcenter->created_at->diffForHumans() }}
                            </td>
                            <th>
                                <a href="{{ route('school.incubationcenters.edit', $incubationcenter->id) }}" class="btn btn-info btn-sm">Edit</a>
                                <button type="button" data-url="{{ route('school.incubationcenters.destroy', $incubationcenter->id) }}"
                                        data-names="{{ $incubationcenter->id }}"
                                        class="btn btn-danger btn-sm btn-delete">Delete
                                </button>
                            </th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('school.incubationcenter.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false,
                "responsive": true,
                "scrollX": true
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection