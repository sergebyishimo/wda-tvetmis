<div class="list-group list-group-horizontal">
    {{--@if( activeMenu('school.students.index') == "" )--}}
        <a href="{{ route('school.incubationcenters.index') }}" class="list-group-item {{ activeMenu('school.incubationcenters.index') }}">View Incubation centers</a>
    {{--@endif--}}
    <a href="{{ route('school.incubationcenters.create') }}" class="list-group-item {{ activeMenu('school.incubationcenters.create') }}">
        Add Incubation center</a>
</div>