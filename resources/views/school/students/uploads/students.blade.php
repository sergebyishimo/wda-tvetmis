@extends('school.students.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Students Management</span>
@endsection

@section('box-body')
    <div class="box box-default">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ asset('files/students_upload_format.csv') }}" target="_blank" class="box-title">
                        <span class="text-info">
                            <small class="pull-right">Download Format</small>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        {!! Form::open(['route' => 'school.students.upload.store', 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => 'uploadStudents']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('qualification', "Qualification *") !!}
                        {!! Form::select('qualification', getDepartments(true), null,
                        ['class' => 'form-control select2 class-q', 'placeholder' => 'Choose department']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('level_id', "Level *") !!}
                        {!! Form::select('level_id', [], null,
                        ['class' => 'form-control select2', 'placeholder' => 'Choose class']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('acad_year', "Academic Year *") !!}
                        {!! Form::select('acad_year', academicYear(), date('Y'),
                        ['class' => 'form-control select2', 'placeholder' => 'Choose class']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('status', "Students Status *") !!}
                        {!! Form::select('status', availableStatus(), null,
                        ['class' => 'form-control select2', 'placeholder' => 'Choose status']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('file', "CSV File *") !!}
                        {!! Form::file('file', ['class' => 'filer_input form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary pull-left btn-md" type="submit">Upload</button>
                    <a class="btn btn-warning pull-right btn-md" href="{{ route('school.students.upload') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @if(session()->has('students'))
        <div class="box box-default">
            <div class="box-heading">
                <div class="box-title">All Students {{ count(session()->get('students')) }} </div>
            </div>
            <div class="box-body">
                <table class="table" id="dataTable">
                    <thead>
                    <tr>
                        <th>Students Names</th>
                        <th>Gender</th>
                        <th>Mode</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count(session()->get('students')) > 0)
                        @foreach(session()->get('students') as $student)
                            <tr>
                                <td>{{ ucwords($student['fname']." ".$student['lname']) }}</td>
                                <td>{{ ucwords($student['gender']) }}</td>
                                <td>{{ ucwords($student['mode']) }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif
    @if(session()->has('leftovers'))
        <div class="box box-warning">
            <div class="box-heading">
                <div class="box-title">Not Imported Students {{ count(session()->get('leftovers')) }} </div>
            </div>
            <div class="box-body">
                <table class="table" id="dataTable">
                    <thead>
                    <tr>
                        <th>Row</th>
                        <th>Students Names</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Mode</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count(session()->get('leftovers')) > 0)
                        @foreach(session()->get('leftovers') as $row => $student)
                            <tr>
                                <td>{{ $row }}</td>
                                <td>{{ ucwords($student['name']) }}</td>
                                <td>{{ ucwords($student['gender']) }}</td>
                                <td><label class="label label-danger">{{ ucwords($student['email']) }}</label></td>
                                <td>{{ ucwords($student['mode']) }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("* #dataTable").dataTable({
                "ordering": false
            });
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filerCsv.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UploadStudentsRequest', '#uploadStudents'); !!}
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: csv");

            $(".class-q").on("change", function () {
                var dep = $(this).val();
                console.log(dep);
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });
        });
    </script>
@endsection