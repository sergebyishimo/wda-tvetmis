@extends('school.students.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Students Management</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th></th>
                <th>Student Number</th>
                <th>Student Names</th>
                <th>Course Offered</th>
                <th>Class</th>
                <th>Student Gender</th>
                @can('isSchoolManager')
                <th></th>
                @endcan
                </thead>
                <tbody>
                @if(school(true)->students->count() > 0)
                    @foreach(school(true)->students as $student)
                        <tr>
                            <td>
                                @if(getStudentPhoto($student) != "")
                                    <img src="{{ getStudentPhoto($student) }}" alt="{{ $student->fname }}" width="50px"
                                         height="50px">
                                @endif
                            </td>
                            <td>
                                <label class="label label-primary">{{ $student->reg_no }}</label>
                            </td>
                            <td>
                                <h5>
                                    {{ $student->fname." ".$student->lname }}
                                </h5>
                            </td>
                            <td>
                                {{ $student->level->department->qualification->qualification_title }}
                            </td>
                            <td>
                                {{ $student->level->rtqf->level_name }}
                            </td>
                            <td>
                                {{ $student->gender }}
                            </td>
                            @can('isSchoolManager')
                            <th>
                                <a href="{{ route('school.students.edit', $student->id) }}" class="btn btn-info btn-sm">Edit</a>
                                <button type="button" data-url="{{ route('school.students.destroy', $student->id) }}"
                                        data-names="{{ $student->names }}"
                                        class="btn btn-danger btn-sm btn-delete">Delete
                                </button>
                            </th>
                            @endcan
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('school.students.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection