<div class="list-group list-group-horizontal">
    @if(Gate::check('isSchoolManager') || Gate::check('isTeachOrStaff'))
        <a href="{{ route('school.students.index') }}" class="list-group-item {{ activeMenu('school.students.index') }}">View Students</a>
    @endif
        @can('isSchoolManager')
    <a href="{{ route('school.students.create') }}" class="list-group-item {{ activeMenu('school.students.create') }}">
        Create a Student</a>

    <a href="{{ route('school.students.upload') }}"
       class="list-group-item {{ activeMenu('school.students.upload') }}">Upload Students</a>
    <a href="{{ route('school.students.upload.pictures') }}"
       class="list-group-item {{ activeMenu('school.students.upload.pictures') }}">Upload Pictures</a>
    <a href="#" class="list-group-item {{ activeMenu('school.students.view.class') }}" data-toggle="modal"
       data-target="#studentList">Student Card</a>
    <a href="#" class="list-group-item {{ activeMenu('school.p.students.view.class') }}" data-toggle="modal"
       data-target="#pstudentList">Parent Card</a>
        @endcan
</div>