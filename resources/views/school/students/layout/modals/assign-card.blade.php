<!-- ----------------------- ASSIGN CARD ------------------ -->
<div class="modal fade" id="assign_card" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-new-window"></span>
                    Assing card to Student
                </h4>
            </div>
            <form id="frm_s_card" method="post" action="{{ route('school.students.assign.card') }}">
                @csrf()
                <div class="modal-body" style="height: 400px;">
                    <div style="width: 350px;margin: auto;display: block;">
                        <div>
                            <input type="hidden" name="level" value="{{ $level }}"/>
                            <input type="hidden" name="student" value=""/>
                            <input type="hidden" name="card"/>
                            <input type="hidden" name="who" value="s">
                        </div>
                        <div style="width: 300px;height: 300px;">
                            <img src="{{ asset('img/card.png') }}" style="width: 100%;height: auto;"/>
                        </div>
                        <p style="text-align: center">Waiting for a new card...</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-left btnsave" name="btnsave" disabled><span
                                class="fa fa-save"></span> Save Operation
                    </button>

                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"> Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>