<div class="modal fade" tabindex="-1" role="dialog" id="pstudentList">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-info">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Select Class</h4>
            </div>
            {!! Form::open(['route' => 'school.p.students.view.class', 'method' => 'post']) !!}
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="qualification">Qualification <sup>(Department)</sup></label>
                                    <select name="qualification" id="qualification" class="form-control select2 class-q"
                                            style="width: 100%"
                                            required>
                                        <option value="" selected disabled>Select Qualification</option>
                                        @foreach(getQualifications(school(true)->id) as $item)
                                            <option value="{{ $item->id }}" @isset($request) {{ $request->input('qualification') == $item->id ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group level-group">
                                    <label for="level">Level <sup>(Class)</sup></label>
                                    <select name="level" id="level" class="form-control select2 class-l"
                                            style="width: 100%"
                                            required>
                                        @if(isset($request))
                                            <option value="{{ $request->input('level') }}"
                                                    selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                                        @else
                                            <option value="" selected disabled>Select Level</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-md pull-left" type="submit">View</button>
                        <button type="button" class="btn btn-default btn-md pull-right" data-dismiss="modal">Cancel
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('l-scripts')
    @parent
@endsection