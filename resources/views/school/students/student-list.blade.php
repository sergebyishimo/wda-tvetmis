@extends('school.students.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Students Management</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th></th>
                <th>Student Number</th>
                <th>Student Names</th>
                <th>Course Offered</th>
                <th>Class</th>
                <th>Student Gender</th>
                <th>Action</th>
                </thead>
                <tbody>
                @foreach($students as $student)
                    <tr>
                        <td>
                            @if(getStudentPhoto($student) != "")
                                <img src="{{ getStudentPhoto($student) }}" alt="{{ $student->fname }}" width="50px"
                                     height="50px">
                            @endif
                        </td>
                        <td>
                            <label class="label label-primary">{{ $student->reg_no }}</label>
                        </td>
                        <td>
                            <h5>
                                {{ $student->fname." ".$student->lname }}
                            </h5>
                        </td>
                        <td>
                            {{ $student->level->department->qualification->qualification_title }}
                        </td>
                        <td>
                            {{ $student->level->rtqf->level_name }}
                        </td>
                        <td>
                            {{ $student->gender }}
                        </td>
                        <td>
                            @if(strlen($student->card) > 0)
                                <a href="{{ route('school.students.remove.card', [$student->id, 's']) }}" class="btn btn-warning" onclick="confirming()">
                                    Remove Card
                                </a>
                            @else
                                <button type="button" class="btn btn-primary" data-student="{{ $student->id }}"
                                        data-toggle='modal' data-target='#assign_card'>
                                    Assign
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('school.students.layout.modals.assign-card')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
        });

        function confirming() {
            var op = confirm("Are you sure\nYou want to remove the card !!");
            if (op === true)
                return true;
            else
                return false;
        }
    </script>
    <script>
        jQuery('#assign_card').on('shown.bs.modal', function (e) {
            var invoker = $(e.relatedTarget);
            var counts = 0;
            $.post("{{ route('school.students.delete.card', 's') }}", {
                _token: "{{ csrf_token() }}",
                'who': 's'
            }, function () {
            }); //delete recent card and wait for new one

            interval = setInterval(function () {
                $.post("{{ route('school.students.get.card', 's') }}", {
                    _token: "{{ csrf_token() }}"
                }, function (data) {
                    var card = data;
                    if (card.length > 5) {
                        //card found
                        $("[name=card]").val(card);
                        $("[name=student]").val(invoker.data("student"));
                        $(".btnsave").prop("disabled", "");
                        $("#frm_s_card").submit();
                        //alert(card);
                        clearInterval(interval);
                    }
                    if (counts == 10) {
                        alert("Timeout please retry after setup all requirements");
                        clearInterval(interval);
                        $('#assign_card').modal('hide');
                    }
                    counts++;
                });
            }, 2000);
        });
        jQuery('#assign_card').on('hide.bs.modal', function (e) {
            clearInterval(interval)
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection