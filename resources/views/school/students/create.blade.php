@extends('school.students.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Students Management</span>
@endsection

@section('box-body')
    <div class="box">
        @if($student)
            @php($route = ['school.students.update', $student->id])
            @php($id = "updateStudent")
        @else
            @php($route = 'school.students.store')
            @php($id = "createStudent")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
        @if($student)
            @method("PATCH")
            {!! Form::hidden('id', $student->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('reg_no', "Student Number") !!}
                        {!! Form::text('reg_no', $student ? $student->reg_no : generateReg(), ['class' => 'form-control', 'readonly' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('qualification', "Qualification *") !!}
                        {!! Form::select('qualification', getDepartments(true), $student ? $student->level->department->id : null,
                        ['class' => 'form-control select2', 'placeholder' => 'Choose department']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('level_id', "Level *") !!}
                        @if($student)
                            {!! Form::select('level_id', [$student->level_id => $student->level->rtqf->level_name],
                            $student->level_id,
                            ['class' => 'form-control select2', 'placeholder' => 'Choose class']) !!}
                        @else
                            {!! Form::select('level_id', [], null,
                            ['class' => 'form-control select2', 'placeholder' => 'Choose class']) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-ggroup">
                        {!! Form::label('fname', "First Name *") !!}
                        {!! Form::text('fname', $student ? $student->fname : "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('lname', "Other Names *") !!}
                        {!! Form::text('lname', $student ? $student->lname : "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('email', "Email *") !!}
                        {!! Form::email('email', $student ? $student->email : "", ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('gender', "Gender *") !!}
                        {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], $student ? $student->gender : null,
                            ['placeholder' => 'Pick a gender...', 'class' => 'form-control select2', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('bdate', "Birth Day *") !!}
                        {!! Form::text('bdate', $student ? $student->bdate : null, ['class' => 'datepicker form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('image', "Photo") !!}
                        {!! Form::file('image', ['class' => 'filer_input form-control']) !!}
                        @if($student)
                            @if(getStudentPhoto($student))
                                <div class="img-responsive">
                                    <img src="{{ getStudentPhoto($student) }}" alt="{{ $student->names }}"
                                         style="width: 9em; height: 9em;">
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('orphan', "Social status *") !!}
                        {!! Form::select('orphan', [
                            'Father'    =>  'Has Mother Only',
                            'Mother'    =>  'Has Father Only',
                            'Both'      =>  'Orphan both parents',
                            'None'      =>  'Not Orphan'
                        ], $student ? $student->orphan : "None", ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('ft_name', "Father Names") !!}
                        {!! Form::text('ft_name', $student ? $student->ft_name : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('mt_name', "Mother Names") !!}
                        {!! Form::text('mt_name', $student ? $student->mt_name : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('sponsor', "Sponsor *") !!}
                        {!! Form::select('sponsor', getSponsors(), $student ? $student->sponsor : null, ['class' => 'form-control select2', 'placeholder' => 'Pick sponsor ...']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('insurance', "Insurance") !!}
                        {!! Form::select('insurance', getInsurance(), $student ? $student->insurance : null, ['class' => 'form-control select2', 'placeholder' => 'Pick insurance ...']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('insurance_number', "Insurance Number") !!}
                        {!! Form::text('insurance_number', $student ? $student->insurance_number : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('gd_name', "Guardian Name") !!}
                        {!! Form::text("gd_name", $student ? $student->gd_name : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('gd_phone', "Guardian Telephone") !!}
                        {!! Form::text("gd_phone", $student ? $student->gd_phone : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('ft_phone', "Father's Telephone") !!}
                        {!! Form::text("ft_phone", $student ? $student->ft_phone : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('mt_phone', "Mother's Telephone") !!}
                        {!! Form::text("mt_phone", $student ? $student->mt_phone : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('mode', "Study Mode") !!}
                        @if(school('studying_mode') != 'Both' && false)
                            {!! Form::text('mode', $student ? $student->mode : school('studying_mode'), [
                            'class' =>  'form-control',
                            'readonly'  =>  true
                            ]) !!}
                        @else
                            {!! Form::select('mode', ['Boarding' => 'Boarding', 'Day' => 'Day'], $student ? $student->mode : 'Day', ['class' => 'form-control select2']) !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('ubudehe', "Ubudehe") !!}
                        {!! Form::select('ubudehe', getUbudehe(), $student ? $student->ubudehe : null, ['class' => 'form-control select2', 'placeholder' => 'Pick ubudehe ...']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('telephone', "Telephone Number") !!}
                        {!! Form::text('telephone', $student ? $student->telephone : null, [
                        'class' =>  'form-control',
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('nationality', "Nationality") !!}
                        {!! Form::text('nationality', $student ? $student->nationality : null, [
                        'class' =>  'form-control',
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('nationalID', "National ID") !!}
                        {!! Form::text('nationalID', $student ? $student->nationalID : null, [
                        'class' =>  'form-control',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('Province', "Province *") !!}
                        {{--{!! dd(getProvince(true)) !!}--}}
                        {!! Form::select('Province', getProvince(true), $student ? $student->province : null, ['placeholder' => 'Pick Province ...',
                        'class' => 'form-control p select2', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('District', "District *") !!}
                        {!! Form::select('District', $student ? [ $student->district => $student->district ] : [], $student ? $student->district : null, ['placeholder' => 'Pick District ...',
                        'class' => 'form-control d select2', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('Sector', "Sector *") !!}
                        {!! Form::select('Sector', $student ? [ $student->sector => $student->sector ] : [], $student ? $student->sector : null, ['placeholder' => 'Pick Sector ...',
                        'class' => 'form-control s select2', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('Cell', "Cell *") !!}
                        {!! Form::select('Cell', $student ? [ $student->cell => $student->cell ] : [], $student ? $student->cell : null, ['placeholder' => 'Pick Cell ...',
                        'class' => 'form-control c select2', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('Village', "Village *") !!}
                        {!! Form::select('Village', $student ? [ $student->village => $student->village ] : [], $student ? $student->village : null, ['placeholder' => 'Pick Village ...',
                        'class' => 'form-control v select2', 'required' => true]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('index_number', "Index Number") !!}
                        {!! Form::text('index_number', $student ? $student->index_number : null, [
                        'class' =>  'form-control',
                        ]) !!}
                    </div>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $student ? "Update Student" : "Save Student" }}
                    </button>
                    <a href="{{ route('school.students.index') }}"
                       class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('school.students.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($student)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateStudentRequest', '#updateStudenta'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateStudentRequest', '#createStudent'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $("#qualification").on("change", function () {
                var dep = $(this).val();
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@endsection