@extends('school.marks.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>Marks Management</span>
@endsection

@section('panel-body')
    <div class="box box-default">
        <div class="box-header">
            <h4 class="box-title">View Marks for Period {{ school(true)->settings->active_period }}</h4>
        </div>
{{--        {!! Form::open(['route' => 'school.discipline.store', 'method' => 'post', 'id' => 'searchQForm']) !!}--}}
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="qualification">Qualification <sup>(Department)</sup></label>
                        <select name="qualification" id="qualification" class="form-control select2 class-q"
                                style="width: 100%"
                                required>
                            <option value="" selected disabled>Select Qualification</option>
                            @foreach(getQualifications(school(true)->id) as $item)
                                <option value="{{ $item->uuid }}" @isset($request) {{ $request->input('qualification') == $item->uuid ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group level-group">
                        <label for="level">Level <sup>(Class)</sup></label>
                        <select name="level" id="level" class="form-control select2 class-l"
                                style="width: 100%"
                                onchange="levelList(this.value, 'studentList2')"
                                required>
                            @if(isset($request))
                                <option value="{{ $request->input('level') }}"
                                        selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                            @else
                                <option value="" selected disabled>Select Level</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group module-group">
                        <label for="module">Module <sup>(Cource)</sup></label>
                        <select name="module" id="module" class="form-control select2 class-m"
                                style="width: 100%"
                                required>
                            @if(isset($request))
                                <option value="{{ $request->input('level') }}"
                                        selected>{{ \App\Level::find($request->input('level'))->rtqf->level_name }}</option>
                            @else
                                <option value="" selected disabled>Select Level</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="list">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr style="background-color: #f0f0f0;color: #383838;font-size:13px">
                                <th><input type="checkbox" id="main_check" name="class_com" checked
                                           onClick="toggle(this)"/></th>
                                <th>Student Code</th>
                                <th>Student Name</th>
                                <th>Entry Marks</th>
                            </tr>
                            </thead>
                            <tbody id="studentList2"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="box-footer">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<button class="btn btn-primary btn-md pull-left" id="submitP" type="submit">Submit</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--{!! Form::close() !!}--}}
    </div>
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $("button[type='submit']").hide();
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('school.get.course.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = '<option value="" selected disabled>Select ...</option>';
                        $.each(JSON.parse(data), function (index, level) {
                            h += '<option value="' + level.id + '">' + level.name + '</option>';
                        });
                        $(".level-group select").html(h);
                    });
            });
        });

    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreDepartmentRequest', '#addQual'); !!}
    {!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}
@endsection