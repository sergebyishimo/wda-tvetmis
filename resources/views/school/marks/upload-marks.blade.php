@extends('school.marks.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>Marks Management</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h5 class="box-title text-right">
                <a href="#" data-toggle="modal" data-target="#uploadMarks" data-dismiss="modal">Download Excel File</a>
            </h5>
        </div>
        {!! Form::open(['route' => 'school.uploading.marks', 'method' => 'post', 'files' => true]) !!}
        <div class="box-body">
            <input type="hidden" name="acad_year" value="{{ date('Y') }}">
            <input type="hidden" name="term" value="{{ settings('active_term') }}">
            @if(session('uploadErrors'))
                <p class="alert alert-danger">
                    {!! session('uploadErrors') !!}
                    <br>
                    They were given 0 by default. you can correct those marks using the below link!!
                </p>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('acad_year', "Academic Year") !!}
                        {!! Form::text('acad_year', date('Y'), ['class' => 'form-control', 'readonly' => true]) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('term', "Current Term") !!}
                        {!! Form::select('term', getTerm(), getTerm(true), ['class' => 'form-control', 'readonly' => true]) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="file" name="file" style="line-height: 1">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-success"><i class="fa fa-tasks"></i> Import Excel
                File
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('school.get.course.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = '<option value="" selected disabled>Select ...</option>';
                        $.each(JSON.parse(data), function (index, level) {
                            h += '<option value="' + level.id + '">' + level.name + '</option>';
                        });
                        $(".level-group select").html(h);
                    });
            });
        });

    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreDepartmentRequest', '#addQual'); !!}
    {!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}
@endsection