@extends('school.marks.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('panel-title')
    <span>Marks Management</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-body">
            @if(!empty($students))
                <form method="POST" action="{{ route('school.marks.save.exam') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="course_id" value="{{ $course_id }}">
                    <div>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Term</th>
                                <th>Given Date</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $term }}</td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" class="form-control date-picker" name="date"
                                               placeholder="Date" style="" required>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="total" id="total" maxlength="3"
                                           style="font-size: 16px;font-weight: bold;"
                                           onkeyup="abc()" onkeypress="return isNumber(event)" required>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                    <table class="table table-bordered"
                           style="">
                        <thead>
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th style="width: 100px;">Reg. No</th>
                            <th>Student Name</th>
                            <th align="right">Marks</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($students as $student)
                            <tr>
                                <td style="text-align: center;">{{ $i++ }}</td>
                                <td><input type="hidden" name="reg_no[]"
                                           value="{{ $student['reg_no'] }}">{{ $student['reg_no'] }}</td>
                                <td>{{ $student['fname'] . ' ' . $student['lname']  }}</td>
                                <td>
                                    <span name="warning[]"></span>
                                    <input type="text" class="form-control"
                                           name="my_new_marks[]" maxlength="3"
                                           style="width: 80px;float: right;font-size: 16px;font-weight: bold"
                                           onkeyup="validateNewMarks()"
                                           onkeypress="return isNumber(event)" disabled>
                                </td>
                            </tr>
                        @endforeach
                        @if(count($students) == 0)
                            <tr>
                                <td colspan="4" style="text-align:  center;">No Students In The Selected Level.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    <center>
                        <button id="submit" class="btn btn-success" disabled style="">
                            <i class="fa fa-save"></i> Save Marks
                        </button>
                    </center>
                </form>
            @endif

            @if(!empty($u_students))
                {!! Form::open(['route' => 'school.marks.update.exam']) !!}
                <input type="hidden" name="course_id" value="{{ $course_id }}">
                <div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Term</th>
                            <th>Given Date</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $term }}</td>
                            <td>
                                <div class="form-group">
                                    <input type="text" class="form-control date-picker" name="date"
                                           value="{{ date('m/d/Y', strtotime($check->given_date)) }}" style=""
                                           required>
                                </div>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="u_total" id="u_total"
                                       maxlength="7"
                                       style="font-size: 16px;font-weight: bold;"
                                       onkeyup="abcUpdate()" onkeypress="return isNumber(event)"
                                       value="{{ $check->total }}" required>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <table class="table table-bordered">
                    <thead style="">
                    <tr>
                        <th style="text-align: center;">#</th>
                        <th style="width: 100px;">Reg. No</th>
                        <th>Student Name</th>
                        <th class="text-right">Marks</th>
                    </tr>
                    </thead>

                    <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($u_students as $student)
                        @php
                            $student->course_id = $course_id;
                            $student->term      = $term;
                            $student->acad_year = $acad_year;
                        @endphp
                        <input type="hidden" name="u_marks_id[]" value="{{ $student->exam_mark_id }}">
                        <tr>
                            <td style="text-align: center;">{{ $i++ }}</td>
                            <td><input type="hidden" name="u_reg_no[]"
                                       value="{{ $student['reg_no'] }}">{{ $student['reg_no'] }}</td>
                            <td>{{ $student['fname'] . ' ' . $student['lname']  }}</td>
                            <td>
                                <span name="u_warning[]"></span>
                                <input type="text"
                                       class="form-control"
                                       name="my_marks[]" maxlength="7"
                                       style="width: 80px;float: right;font-size: 16px;font-weight: bold"
                                       onkeyup="validateUpdatedMarks()"
                                       onkeypress="return isNumber(event)"
                                       value="{{ $student->exam_marks }}">
                            </td>
                        </tr>
                    @endforeach
                    @if(count($u_students) == 0)
                        <tr>
                            <td colspan="5" style="text-align:  center;">No Students In The Selected Level.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <center>
                    <button id="update" class="btn btn-success"
                            style="width: 50%;margin-left: 140px;"><i class="fa fa-save"></i> Update Marks
                    </button>
                </center>
                {!! Form::close() !!}
            @endif

            @if(!empty($unable))
                <br>
                <p style="background: #cc0000;color: #fff;padding: 10px;margin: auto;width: 50%;text-align: center;"><i
                            class="fa fa-warning"></i> You are not allowed to enter new marks!! You are only allowed to
                    update them.</p>
            @endif

            @if(!empty($timeout))
                <br>
                <p style="background: #cc0000;color: #fff;padding: 10px;margin: auto;width: 50%;text-align: center;"><i
                            class="fa fa-warning"></i> You are allowed to edit marks in 24 hourse after you insert them.
                </p>
            @endif

        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $("button[type='submit']").hide();
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('school.get.course.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = '<option value="" selected disabled>Select ...</option>';
                        $.each(JSON.parse(data), function (index, level) {
                            h += '<option value="' + level.id + '">' + level.name + '</option>';
                        });
                        $(".level-group select").html(h);
                    });
            });
        });

    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreDepartmentRequest', '#addQual'); !!}
    {!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}
@endsection