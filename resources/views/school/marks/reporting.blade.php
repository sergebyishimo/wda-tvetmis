@extends('school.marks.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>Marks Management</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <div class="row">
                @if(isset($students))
                    <div class="col-md-6">
                        <a href="/school/marks/level/report?level_id={{ $level_id }}&year={{ $acad_year }}&term={{ $term }}&download=1"
                           target="_blank" class="btn btn-primary btn-sm">View All Reports</a>
                    </div>
                    <div class="col-md-6">
                        <a href="/school/marks/reports?acad_year={{ $acad_year }}&term={{ $term }}&level_id={{ $level_id }}&download=1"
                           target="_blank" class="btn btn-sm btn-link pull-right">
                            <i class="fa fa-download"></i> Export in PDF</a>
                    </div>
                @endif
            </div>
        </div>
        <div class="box-body">
            @if(isset($students))
                <div class="main-content">
                    <table class="table table-bordered" id="dataTable">
                        <thead class="bg-primary" style="text-align: center;color: #fff">
                        <tr>
                            <th style="text-align: center;">Position</th>
                            <th>Student Number</th>
                            <th>Student Names</th>
                            <th style="width: 100px">%</th>
                            <th>Grade</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach($students as $student)
                            @php
                                $student->position = $i;
                            @endphp
                            <tr>
                                <td style="text-align: center;">{{ $i }}</td>
                                <td>{{ $student['reg_no'] }}</td>
                                <td><a target="_blank"
                                       href="/school/marks/report?reg_no={{$student['reg_no']}}&qed={{ $i }}&year={{ $acad_year }}&term={{ $term }}">{{ $student['fname'] . ' ' . $student['lname'] }}</a>
                                </td>
                                <th style="text-align: center;">{{  number_format($student['term_perc'], 1) . ' %' }} </th>
                                <th style="text-align: center;">
                                    {{ getGrade(number_format($student['term_perc'], 1)) }}
                                </th>
                            </tr>
                            @php($i++)
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable();
        });

    </script>
@endsection