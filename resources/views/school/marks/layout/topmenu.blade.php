<div class="list-group list-group-horizontal">

    <a href="#" class="list-group-item {{ activeMenu([
    'school.marks.period.entry',
    ]) }}" data-toggle="modal"
       data-target="#periodMarksEntry">Period Entry</a>

    <a href="#" class="list-group-item {{ activeMenu([
    'school.marks.view.exam',
    ]) }}" data-toggle="modal"
       data-target="#examMarksEntry">Exam Entry</a>

    @if(isAllowed() || isAllowed(1) || isAllowed(5))
        <a href="#" class="list-group-item {{ activeMenu(['school.marks.term.period.exam']) }}" data-toggle="modal"
           data-target="#examPeriodTermMarksEntry">Period and Exam</a>
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(5))
        <a href="{{ route('school.marks.file.exam') }}" class="list-group-item {{ activeMenu([
    'school.marks.file.exam'
    ]) }}">Upload Marks</a>
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5))
        <a href="#" class="list-group-item {{ activeMenu(['school.marks.periodic.results']) }}" data-toggle="modal"
           data-target="#periodResultsMarksEntry">Parmares Report</a>
    @endif
    @if(isAllowed() || isAllowed(1) || isAllowed(3) || isAllowed(5))
        <a href="#" class="list-group-item {{ activeMenu([
    'school.reporting.marks'
    ]) }}" data-toggle="modal" data-target="#reportView">Class Reports</a>
    @endif
</div>