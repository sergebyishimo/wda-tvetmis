<!-- Modal -->
<div class="modal fade" id="verifyPaermissions" tabindex="-1" role="dialog" aria-labelledby="verifyPaermissionsModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Marking Marks</h4>
            </div>
            <form method="post">
                <div class="modal-body">

                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>