<!-- Modal -->
<div class="modal fade" id="uploadMarks" tabindex="-1" role="dialog" aria-labelledby="uploadMarksEntryModal">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Downloading Class List</h4>
            </div>
            @if(school(true)->settings->active_term && school(true)->settings->active_period)
                {!! Form::open(['method' => 'post', 'route' => 'school.download.marks.excel']) !!}
                <div class="modal-body">
                    {!! Form::hidden('term', getTerm(true), ['class' => 'form-control', 'readonly' => true]) !!}
                    {!! Form::hidden('acad_year', date('Y'), ['class' => 'form-control', 'readonly' => true]) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="qualification">Qualification <sup>(Department)</sup></label>
                                <select name="qualification" id="qualification" class="form-control select2 class-q"
                                        style="width: 100%"
                                        required>
                                    <option value="" selected disabled>Select Qualification</option>
                                    @foreach(getQualifications(school(true)->id) as $item)
                                        <option value="{{ $item->uuid }}" @isset($request) {{ $request->input('qualification') == $item->uuid ? 'selected' : '' }} @endisset >{{ $item->qualification_title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 period">
                            <div class="form-group level-group">
                                <label for="levelCourses">Level <sup>(Class)</sup></label>
                                <select name="level_id" id="levelCoursesExam" class="form-control select2 class-l"
                                        style="width: 100%"
                                        required>
                                    @if(isset($request))
                                        <option value="{{ $request->input('level_id') }}"
                                                selected>{{ \App\Level::find($request->input('level_id'))->rtqf->level_name }}</option>
                                    @else
                                        <option value="" selected disabled>Select Level</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="myCoursesExam">Modules <sup>Courses</sup></label>
                            <select name="course_id" id="myCoursesExam" class="form-control select2 module-l"
                                    style="width: 100%" required>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-left">
                        Download
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
                {!! Form::close() !!}
            @else
                <div class="modal-body">
                    <div class="alert alert-danger">
                        Please set active term and period<br>
                        <a href="" data-toggle="modal" data-target="#schoolSettings" data-dismiss="modal">Click here
                            ...</a>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>