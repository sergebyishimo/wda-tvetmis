@extends('school.marks.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('panel-title')
    <span>Marks Management</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-body">
            @if(!empty($students) && empty($periods))
                <center>
                    <form method="POST" action="{{ route('school.marks.entry.period') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="marks_type" value="Whole">
                        <input type="hidden" name="course_id" value="{{ $course_info['id'] }}">
                        <input type="hidden" id="total" name="total" value="{{ $course_info['max_point'] }}">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th style="width: 100px;">Reg. No</th>
                                <th>Student Name</th>
                                <th style="width: 140px;">CAT <sub> / {{$course_info['max_point']}} Pts.</sub></th>
                                <th style="width: 140px;">Exam <sub> / {{$course_info['max_point']}} Pts.</sub></th>
                            </tr>
                            </thead>

                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($students as $student)
                                <tr>
                                    <td style="text-align: center;">{{ $i++ }}</td>
                                    <td><input type="hidden" name="reg_no[]"
                                               value="{{ $student['reg_no'] }}">{{ $student['reg_no'] }}</td>
                                    <td>{{ $student['fname'] . ' ' . $student['lname']  }}</td>
                                    <td>
                                        <span name="cat_warning[]"></span>
                                        <input type="text" class="form-control"
                                               name="my_new_cat[]" maxlength="7"
                                               style="width: 80px;float: right;font-size: 16px;font-weight: bold"
                                               onkeyup="validateNewMarksTerm()"
                                               autocomplete="off">
                                    </td>
                                    <td>
                                        <span name="ex_warning[]"></span>
                                        <input type="text" class="form-control"
                                               name="my_new_ex[]" maxlength="7"
                                               style="width: 80px;float: right;font-size: 16px;font-weight: bold"
                                               onkeyup="validateNewMarksTerm()"
                                               autocomplete="off">
                                    </td>
                                </tr>
                            @endforeach
                            @if(count($students) == 0)
                                <tr>
                                    <td colspan="5" style="text-align:  center;">No Students In The Selected Level.</td>
                                </tr>
                            @endif
                            </tbody>

                        </table>

                        <button id="submit" class="btn btn-success" style="width: 50%;">
                            <i class="fa fa-save"></i> Save Marks
                        </button>

                    </form>
                </center>

                <br clear="left"><br><br>
            @endif

            @if(!empty($students) && !empty($periods))
                <center>
                    <form method="POST" action="{{ route('school.marks.entry.period') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="marks_type" value="Whole">
                        <input type="hidden" name="course_id" value="{{ $course_info['id'] }}">
                        <input type="hidden" id="total" name="total" value="{{ $course_info['max_point'] }}">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th style="width: 100px;">Reg. No</th>
                                <th>Student Name</th>
                                <th style="width: 140px;">CAT <sub> / {{$course_info['max_point']}} Pts.</sub></th>
                                <th style="width: 140px;">Exam <sub> / {{$course_info['max_point']}} Pts.</sub></th>
                            </tr>
                            </thead>

                            <tbody>
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($students as $student)
                                @php
                                    $student->course_id = $course_id;
                                    $student->marks_type = "Whole";
                                    $student->term      = $term;
                                    $student->acad_year = $acad_year;
                                @endphp
                                <tr>
                                    <td style="text-align: center;">{{ ++$i }}</td>
                                    <td><input type="hidden" name="reg_no[]"
                                               value="{{ $student['reg_no'] }}">{{ $student['reg_no'] }}</td>
                                    <td>{{ $student['fname'] . ' ' . $student['lname']  }}</td>
                                    <td>
                                        <span name="cat_warning[]"></span>
                                        <input type="text" class="form-control"
                                               name="my_new_cat[]" maxlength="7"
                                               style="width: 80px;float: right;font-size: 16px;font-weight: bold"
                                               onkeyup="validateNewMarksTerm()"
                                               value="{{ $student->marks }}"
                                               autocomplete="off"></td>
                                    <td>
                                        <span name="ex_warning[]"></span>
                                        <input type="text" class="form-control"
                                               name="my_new_ex[]" maxlength="7"
                                               style="width: 80px;float: right;font-size: 16px;font-weight: bold"
                                               onkeyup="validateNewMarksTerm()"
                                               {{--#$exams[$i++]['obt_marks']--}}
                                               value="{{ $student->exam_marks }}"
                                               autocomplete="off">
                                    </td>
                                </tr>
                            @endforeach
                            @if(count($students) == 0)
                                <tr>
                                    <td colspan="5" style="text-align:  center;">No Students In The Selected Level.</td>
                                </tr>
                            @endif
                            </tbody>

                        </table>

                        <button id="submit" class="btn btn-success" style="width: 50%;">
                            <i class="fa fa-save"></i> Save Marks
                        </button>

                    </form>
                </center>
            @endif
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $("button[type='submit']").hide();
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('school.get.course.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = '<option value="" selected disabled>Select ...</option>';
                        $.each(JSON.parse(data), function (index, level) {
                            h += '<option value="' + level.id + '">' + level.name + '</option>';
                        });
                        $(".level-group select").html(h);
                    });
            });
        });

    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreDepartmentRequest', '#addQual'); !!}
    {!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}
@endsection