@extends('school.marks.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        th.rotate {
            /* Something you can count on */
            height: 80px;
            white-space: nowrap;
        }

        th.rotate > div {
            transform: /* Magic Numbers */ translate(25px, 51px) /* 45 is really 360 - 45 */ rotate(315deg);
            width: 30px;
            margin: -75px 0px 0px -29px;
        }

        th.rotate > div > span {
            border-bottom: 1px solid #ccc;
            padding: 5px 10px;
        }
    </style>
@overwrite

@section('panel-title')
    <span>Marks Management</span>
@endsection

@section('panel-body')
    <div class="box box-default">
        <div class="box-header">
            <div class="row">
                <form action="/school/periodic/results/print/"
                      method="get"
                      target="_blank">

                    <input type="hidden" name="acad_year" value="{{ $acad_year }}">
                    <input type="hidden" name="term" value="{{ $term }}">
                    <input type="hidden" name="period" value="{{ $period }}">
                    <input type="hidden" name="level_id" value="{{ $level_id }}">
                    <input type="hidden" name="course_id" value="{{ $course_id }}">
                    <input type="hidden" name="download" value="1">

                    <div class="col-md-6">
                        <label for="send_sms">Report To Parents</label>
                        <input type="checkbox" name="send_sms" value="1" id="send_sms" class="checkbox-ui">
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-link btn-sm pull-right" style="">
                            <i class="fa fa-file-excel-o"></i> Export in Excel
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="box-body">
            <div class="main-content">
                @if(!empty($courses) && !empty($students))

                    <table class="table table-striped table-hover" cellspacing="0"
                           style="background: #fff">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Code</th>
                            <th>Student Names</th>
                            @if(isset($periods))
                                @foreach($periods as $ped)
                                    <th class='rotate'>
                                        <div>
                                            <span>{{ $ped['marks_type'] }} / {{ $ped['total']}} Pts</span>
                                        </div>
                                    </th>
                                @endforeach
                            @else
                                @foreach($courses as $course)
                                    @php
                                        $a = strtolower($course['module']['module_title']).'_cat';
                                        $a = str_replace(' ', '_', $a);
                                        $a = str_replace('+', '', $a);
                                        $$a = 0;

                                        $a = strtolower($course['module']['module_title']).'_ex';
                                        $a = str_replace(' ', '_', $a);
                                        $a = str_replace('+', '', $a);
                                        $$a = 0;
                                    @endphp
                                    <th @if($period == 7) colspan="3" @endif class='rotate'>
                                        <div>
                                            <span>
                                                {{ $course['module']['module_title'] }}/
                                                &nbsp;&nbsp;
                                                @if($course['max_point'] == '' || $course['max_point'] == 0)
                                                    <b style="color: #cc0000">
                                                        <i class="fa fa-warning"></i>
                                                        No Total Found
                                                    </b>
                                                @else {{ $course['max_point'] }}
                                                Pts @endif  </span>
                                        </div>
                                    </th>
                                @endforeach
                            @endif


                            <th class="rotate">
                                <div><span>The Sum / {{ $courses_total or $periods_total }} Pts  </span></div>
                            </th>
                            @if(isset($periods_total))
                                <th class="rotate">
                                    <div><span>Total / {{ $course_info['max_point'] }} Pts  </span></div>
                                </th>
                            @endif
                            <th class="rotate">
                                <div><span>% Percentage</span></div>
                            </th>
                        </tr>

                        @if($period == 7)
                            <tr>
                                <th colspan="3"></th>
                                @foreach($courses as $course)
                                    <th>CAT</th>
                                    <th>EX</th>
                                    <th>TOT</th>
                                @endforeach
                            </tr>

                        @endif
                        </thead>

                        <tbody>
                        @php
                            $i = 1;
                            $total = 0;
                            $i_sec = 1;
                            $course_total_total = 0;

                            $msg_total = count($students);
                        @endphp
                        @foreach($students as $student)
                            @php
                                $student->acad_year = $acad_year;
                                $student->term      = $term;
                                $student->period    = $period;

                                if (isset($periods)) {
                                    $student->course_id = $course_id;

                                } else {
                                    if ($courses_total == 0) {
                                        $perc = 0;
                                    } else {
                                        $perc  = ($student['total_obt_marks'] * 100) / $courses_total;
                                    }
                                }


                                $perc_total = 0;
                                $real_total = 0;
                                $msg_pos = $i;

                            @endphp
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $student['reg_no'] }}</td>
                                <td>{{ $student['fname'] . ' ' . $student['lname'] }}</td>

                                @if(isset($periods))
                                    @foreach($periods as $ped)
                                        @php
                                            $student->marks_type = $ped['marks_type'];
                                            $student->course_id  = $course_info['id'];
                                            if ($i_sec == 1) {
                                                $a = strtolower($ped['marks_type']);
                                                $a = str_replace(' ', '_', $a);
                                                $$a = $student['period_obt_marks'];
                                            } else {
                                                $a = strtolower($ped['marks_type']);
                                                $a = str_replace(' ', '_', $a);
                                                $$a += $student['period_obt_marks'];
                                            }
                                        @endphp
                                        <td>
                                            @if( $student['period_obt_marks'] < ($ped['total'] / 2) )
                                                <span style="color: #cc0000">{{ $student['period_obt_marks'] }}</span>
                                            @else
                                                {{ $student['period_obt_marks'] }}
                                            @endif
                                        </td>
                                    @endforeach
                                @else
                                    @foreach($courses as $course)
                                        @php
                                            $student->course_id = $course['id'];
                                            $student->course_max = $course['max_point'];

                                            if ($period <=5 ) {
                                                if ($student['period_course_total'] > 0) {
                                                    $real_marks = ($student['sum_course_obt_marks'] * $course['max_point']) / $student['period_course_total'] ;
                                                    //$real_marks = $student['period_course_total'];
                                                } else {
                                                    $real_marks = 0;
                                                }

                                                if ($i_sec == 1) {
                                                    $a = strtolower($course['module']['module_title']);
                                                    $a = str_replace(' ', '_', $a);
                                                    $a = str_replace('+', '', $a);
                                                    $$a = $real_marks;

                                                } else {
                                                    $a = strtolower($course['module']['module_title']);
                                                    $a = str_replace(' ', '_', $a);
                                                    $a = str_replace('+', '', $a);
                                                    $$a += $real_marks;


                                                }

                                                $real_total += $real_marks;
                                            }



                                        @endphp
                                    <!-- Obtained Marks -->

                                        @if($period == 7)
                                            @php
                                                $term_cat = $student['term_cat'];
                                                $term_ex = $student['term_ex'];

                                                $real_total += $term_cat + $term_ex;

                                                $a = strtolower($course['module']['module_title']).'_cat';
                                                $a = str_replace(' ', '_', $a);
                                                $a = str_replace('+', '', $a);
                                                $$a += $term_cat;

                                                $a = strtolower($course['module']['module_title']).'_ex';
                                                $a = str_replace(' ', '_', $a);
                                                $a = str_replace('+', '', $a);
                                                $$a += $term_ex;
                                            @endphp
                                            <td style="background: #85c0f5"
                                                title="{{ $student['fname'] . ' ' . $student['lname'] . ' Course : ' . $course['module']['module_title']  }}">
                                                @if($term_cat < ($course['max_point'] / 2))
                                                    <span style="color: #cc0000">{{ number_format($term_cat, 1) }}</span>
                                                @else
                                                    {{ number_format($term_cat, 1) }}
                                                @endif
                                            </td>
                                            <td style="background: #79d479"
                                                title="{{ $student['fname'] . ' ' . $student['lname'] . ' Course : ' . $course['module']['module_title']  }}">
                                                @if($term_ex < ($course['max_point'] / 2))
                                                    <span style="color: #cc0000">{{ number_format($term_ex, 1) }}</span>
                                                @else
                                                    {{ number_format($term_ex, 1) }}
                                                @endif
                                            </td>
                                            <td style="background: #d4d4d4"
                                                title="{{ $student['fname'] . ' ' . $student['lname'] . ' Course : ' . $course['module']['module_title']  }}">
                                                @if($term_cat + $term_ex < $course['max_point'])
                                                    <span style="color: #cc0000">{{ number_format($term_cat + $term_ex, 1) }}</span>
                                                @else
                                                    {{ number_format($term_cat + $term_ex, 1) }}
                                                @endif
                                            </td>

                                        @else
                                            <td style=""
                                                title="{{ $student['fname'] . ' ' . $student['lname'] . ' Course : ' . $course['module']['module_title']  }}">
                                                @if($real_marks < ($course['max_point'] / 2))
                                                    <span style="color: #cc0000">{{ number_format($real_marks, 1) }}</span>
                                                @else
                                                    {{ number_format($real_marks, 1) }}
                                                @endif
                                            </td>
                                        @endif
                                    @endforeach
                                @endif
                            <!-- Total -->
                                <th style="background: #cccccc">
                                    @if(isset($periods))
                                        @if($student['total_obt_marks_in_course'] < ($periods_total / 2))
                                            <span style="color: #cc0000">{{ number_format($student['total_obt_marks_in_course'], 1) }}</span>
                                        @else
                                            {{ number_format($student['total_obt_marks_in_course'], 1) }}
                                        @endif
                                    @else
                                        @if($real_total < ($courses_total / 2))
                                            <span style="color: #cc0000">{{ number_format($real_total, 1) }}</span>
                                        @else
                                            {{ number_format($real_total, 1) }}
                                        @endif
                                    @endif
                                </th>
                                <!-- Course Total -->
                                @if(isset($periods_total))
                                    @php
                                        if ($periods_total == 0) {
                                            $course_total = 0;
                                        } else {
                                            $course_total = ($student['total_obt_marks_in_course'] * $course_info['max_point']) / $periods_total;
                                        }

                                        $course_total_total += $course_total;
                                    @endphp
                                    <th style="background: #cccccc"> {{ number_format($course_total, 1) }} </th>
                            @endif
                            <!-- Percentage -->
                                <th style="background: #cccccc">
                                    @php
                                        if (isset($periods_total)) {
                                            if ($periods_total == 0) {
                                                $perc = 0;
                                            } else {
                                                $perc  = ($student['total_obt_marks_in_course'] * 100) / $periods_total;
                                            }

                                        } else {
                                            if ($courses_total == 0) {
                                                $perc = 0;
                                            } else {
                                                $perc  = ($real_total * 100) / $courses_total;
                                            }


                                            // Send sms

                                            if (isset($send_sms) && $send_sms == 'yes') {

                                                $msg = 'Babyeyi Dufatanyije Kurera, ' . $student['fname'] . ' ' . $student['lname'] . ' turabamenyesha ko yabaye uwa ' . $msg_pos . '/' . $msg_total . ' agira ' .  number_format($perc, 1) . ' %. Igihembwe cya ' . $term . '.';

                                                if ($period == 7) {
                                                    if (count($student['ft_phone']) > 0) {
                                                        sendSMS($student['ft_phone'], $msg, "marks");
                                                    } elseif(count($student['mt_phone']) > 0) {
                                                        sendSMS($student['mt_phone'], $msg, "marks");
                                                    } elseif (count($student['gd_phone'])) {
                                                        sendSMS($student['gd_phone'], $msg, "marks");
                                                    }
                                                } else {
                                                        if (count($student['ft_phone']) > 0) {
                                                        /*$response = $client->request('POST', 'https://www.intouchsms.co.rw/api/sendsms/.json', [
                                                                        'form_params' => [
                                                                            'username' => 'sergebyishimo',
                                                                            'password' => '2018@Serge',
                                                                            'sender'  => 'GSSB Save',
                                                                            'recipients' => $student['parent_phone'],
                                                                            'message' => 'Babyeyi Dufatanyije Kurera, ' . $student['fname'] . ' ' . $student['lname'] . ' yagize amanota ' . number_format($real_total, 1) . ' / ' . $courses_total . ' Kwijanisha ' . number_format($perc, 1) . ' %. Igihembwe : ' . $term . ' Period : ' . $period . ' Umwaka : ' . $acad_year
                                                                        ]
                                                        ]); */
                                                    }
                                                }


                                            }
                                        }
                                    @endphp
                                    @if($perc < 50)
                                        <span style="color: #cc0000">{{ number_format($perc, 1) . ' %' }}</span>
                                    @else
                                        {{ number_format($perc, 1) . ' %' }}
                                    @endif
                                </th>
                            </tr>
                            @php
                                $total += $student['total_obt_marks'] ;
                                $perc_total += $perc;
                                $i_sec++;
                            @endphp
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <th style="">Average</th>
                            @php
                                $avg_total = 0.00;

                            @endphp
                            @if(isset($periods))
                                @foreach($periods as $ped)
                                    @php

                                        $a = strtolower($ped['marks_type']);
                                        $a = str_replace(' ', '_', $a);
                                        $avg = ${$a} / count($students);
                                        $avg_total += $avg;
                                    @endphp
                                    <th style="">
                                        @if($avg < ($ped['total'] / 2))
                                            <span style="color: #cc0000">{{ number_format($avg, 1) }}</span>
                                        @else
                                            {{ number_format($avg, 1) }}
                                        @endif
                                    </th>
                                @endforeach

                            @else

                                @foreach($courses as $course)
                                    @php

                                        if ($period == 7) {
                                            $a = strtolower($course['module']['module_title']) . '_cat';
                                            $a = str_replace(' ', '_', $a);
                                            $a = str_replace('+', '', $a);
                                            $cat_avg = ${$a} / count($students);


                                            $e = strtolower($course['module']['module_title']) . '_ex';
                                            $e = str_replace(' ', '_', $e);
                                            $e = str_replace('+', '', $e);
                                            $ex_avg = ${$e} / count($students);


                                            $avg_total += $cat_avg + $ex_avg;
                                        } else {
                                            //$student->course_id = $course['id'];
                                            $a = strtolower($course['module']['module_title']);
                                            $a = str_replace(' ', '_', $a);
                                            $a = str_replace('+', '', $a);
                                            $avg = ${$a} / count($students);
                                            $avg_total += $avg;
                                        }
                                    @endphp

                                    @if($period == 7)
                                        <th style="">
                                            @if($cat_avg < ($course['max_point'] / 2))
                                                <span style="color: #cc0000">{{ number_format($cat_avg, 1) }}</span>
                                            @else
                                                {{ number_format($cat_avg, 1) }}
                                            @endif
                                        </th>
                                        <th style="">
                                            @if($ex_avg < ($course['max_point'] / 2))
                                                <span style="color: #cc0000">{{ number_format($ex_avg, 1) }}</span>
                                            @else
                                                {{ number_format($ex_avg, 1) }}
                                            @endif
                                        </th>
                                        <th>{{ number_format($cat_avg + $ex_avg, 1) }}</th>
                                    @else
                                        <th style="">
                                            @if($avg < ($course['max_point'] / 2))
                                                <span style="color: #cc0000">{{ number_format($avg, 1) }}</span>
                                            @else
                                                {{ number_format($avg, 1) }}
                                            @endif
                                        </th>
                                    @endif

                                @endforeach

                            @endif

                            <th style="background: #cccccc">
                                @if(isset($periods))
                                    @if($avg_total < ($periods_total / 2))
                                        <span style="color: #cc0000">{{ number_format($avg_total, 1) }}</span>
                                    @else
                                        {{ number_format($avg_total, 1) }}
                                    @endif
                                @else
                                    @if($avg_total < ($courses_total / 2))
                                        <span style="color: #cc0000">{{ number_format($avg_total, 1) }}</span>
                                    @else
                                        {{ number_format($avg_total, 1) }}
                                    @endif
                                @endif

                            </th>
                            @if(isset($periods_total))
                                @php
                                    $course_total_total = $course_total_total / count($students);
                                @endphp
                                <th style="background: #cccccc"> {{ number_format($course_total_total, 1) }}</th>
                            @endif
                            @php
                                if (isset($periods)) {
                                    if ($course_info['max_point'] == 0) {
                                        $avg_perc = 0;
                                    } else {
                                        $avg_perc = ($course_total_total * 100) / $course_info['max_point'];
                                    }

                                } else {
                                    if ($courses_total == 0) {
                                        $avg_perc = 0;
                                    } else {
                                        $avg_perc = ($avg_total * 100) / $courses_total;
                                    }

                                }

                            @endphp
                            <th style="background: #cccccc">
                                @if($avg_perc < 50)
                                    <span style="color: #cc0000">{{ number_format( $avg_perc, 1) . ' %' }}</span>
                                @else
                                    {{ number_format( $avg_perc, 1) . ' %' }}
                                @endif
                            </th>
                        </tr>
                        </tbody>
                    </table>
                @else
                    @if(isset($error))
                        <p style="background: #cc0000;width: 500px;color: #fff;padding: 8px;padding-left: 20px">
                            <i class="fa fa-warning"></i> This report is not available!!.
                        </p>
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('school.get.course.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = '<option value="" selected disabled>Select ...</option>';
                        $.each(JSON.parse(data), function (index, level) {
                            h += '<option value="' + level.id + '">' + level.name + '</option>';
                        });
                        $(".level-group select").html(h);
                    });
            });
        });

    </script>
@endsection
