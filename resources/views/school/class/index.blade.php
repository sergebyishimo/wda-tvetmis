@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Class
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
@overwrite

@section('contentheader_title')
    <div class="container-fluid">
        <span>School Qualification</span>
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        <center>@include('school.class.layout.topmenu')</center>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <table class="table table-striped">
                            <thead align="center">
                            <th>Qualifications <sup>(Departments)</sup></th>
                            <th>RTQF <sup>Class</sup></th>
                            <th>Capacity Students</th>
                            <th>Capacity Rooms</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @if(getDepartments()->count() > 0)
                                @foreach(getDepartments()->get() as $department)
                                    <tr>
                                        <td>
                                            <ul class="list-group">
                                                <li class="list-group-item list-group-item-success">
                                                    <div>
                                                        <span class="glyphicon glyphicon-ok"></span> &nbsp;
                                                        <b>{{ $department->qualification->qualification_title }}</b>
                                                    </div>
                                                    <div class="text-info" style="margin-left: 22px;">
                                                        @if($department->teacher)
                                                            Head of Department:
                                                            <u>{{ ucwords(trim($department->teacher->first_name." ".$department->teacher->last_name)) }}</u>
                                                        @else
                                                            Head of Department: <span
                                                                    class="text-danger">Not set !!</span>
                                                        @endif
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-group">
                                                @if($department->levels)
                                                    @if($department->levels->count() > 0)
                                                        @foreach($department->levels()->orderby('created_at', 'ASC')->get() as $level)
                                                            <li class="list-group-item">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <b>{{ ucwords($level->rtqf->level_name) }}</b>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <button class="btn btn-sm btn-warning pull-right remove-level"
                                                                                data-toggle="modal"
                                                                                data-target="#removeLevel"
                                                                                data-form="{{ $level->id }}"
                                                                                data-action="{{ route('school.class.destroy', $level->id) }}"
                                                                                data-level="{{ $level->id }}">
                                                                            <span class="glyphicon glyphicon-remove"></span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        <li class="list-group-item list-group-item-warning text-danger">
                                                            Not
                                                            set !!
                                                        </li>
                                                    @endif
                                                @else
                                                    <li class="list-group-item list-group-item-warning text-danger">
                                                        Not set !!
                                                    </li>
                                                @endif
                                            </ul>
                                        </td>
                                        <td style="text-align: center;font-weight: bold;font-size: medium;">{{ $department->capacity_students or 0 }}
                                            <br>
                                            <a href="#" class="btn btn-sm btn-primary" style="margin-top: 10px;"
                                               data-toggle="modal" data-dept-id="{{ $department->id }}"
                                               data-target="#studentsModal">Update
                                                <sup>(students)</sup></a>
                                        </td>
                                        <td style="text-align: center;font-weight: bold;font-size: medium;">{{ $department->capacity_rooms or 0 }}
                                            <br>
                                            <a href="#" class="btn btn-sm btn-primary" style="margin-top: 10px;"
                                               data-toggle="modal" data-dept-id="{{ $department->id }}"
                                               data-target="#roomsModal">Update
                                                <sup>(rooms)</sup></a>
                                        </td>
                                        <td>
                                            <button class="btn btn-md btn-block disable-dept {{ $department->status == 1 ? 'btn-danger' : 'btn-success' }}"
                                                    data-toggle="modal" data-target="#disableDepartment"
                                                    data-action="{{ route('school.destroy.department', $department->id) }}"
                                                    data-department="{{ $department->id }}">
                                                {{ $department->status == 1 ? 'Disable' : 'Enable' }}
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td align="center" colspan="2">
                                        <h5 class="text-danger">No qualification <sup>(department)</sup> registered to
                                            this
                                            school
                                            !!</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('school.class.layout.modals.add-department')
    @include('school.class.layout.modals.add-class')
    @include('school.class.layout.modals.warnings')
    @include('school.class.layout.modals.update-rooms-cap')
    @include('school.class.layout.modals.update-students-cap')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            //triggered when modal is about to be shown
            $('#studentsModal').on('show.bs.modal', function (e) {

                //get data-id attribute of the clicked element
                var deptId = $(e.relatedTarget).data('dept-id');

                //populate the textbox
                let action_url = $('#updateStudentsCap').attr('action');
                let arr = action_url.split('/');
                arr.splice(6, 1, deptId);
                $('#updateStudentsCap').attr('action', arr.join('/'));
                // $(e.currentTarget).find('form[name="capacity_students"]').val(deptId);
            });


            $('#roomsModal').on('show.bs.modal', function (e) {

                //get data-id attribute of the clicked element
                var deptId = $(e.relatedTarget).data('dept-id');

                //populate the textbox
                let action_url = $('#updateRoomsCap').attr('action');
                let arr = action_url.split('/');
                arr.splice(6, 1, deptId);
                $('#updateRoomsCap').attr('action', arr.join('/'));
            });

            $(".level-group").hide();
            $(".class-q").on("change", function (e) {
                $(".level-group").show();
                var op = $(this).val();
                $.get("{{ route('school.get.levels') }}",
                    {
                        id: op
                    }, function (data) {
                        var h = "";
                        $.each(JSON.parse(data), function (index, level) {
                            var se = level.checked == true ? "checked disabled" : "";
                            var nm = level.checked == false ? 'name="levels[]"' : "";
                            h += '<div class="form-check"><input class="form-check-input" ' + nm + ' type="checkbox" id="' + level.id + '" value="' + level.id + ' " ' + se + '>' +
                                '&nbsp;<label class="form-check-label" for="' + level.id + '">' + level.name + '</label></div>';
                        });
                        console.log(h);
                        $(".levels-list").html(h);
                    });
            });
            $(".remove-level").on('click', function () {
                var modal = $("#removeLevel");
                modal.find('form .level').val($(this).data('level'));
                modal.find('form').attr("action", $(this).data('action'));
                modal.modal();
            });
            $(".disable-dept").on('click', function () {
                var modal = $("#disableDepartment");
                modal.find('form .department').val($(this).data('department'));
                modal.find('form').attr("action", $(this).data('action'));
                modal.modal();
            });
        });

    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StoreDepartmentRequest', '#addQual'); !!}
    {!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}
    {!! JsValidator::formRequest('App\Http\Requests\UpdateStudentsCapRequest', '#updateStudentsCap'); !!}
    {!! JsValidator::formRequest('App\Http\Requests\UpdateRoomsCapRequest', '#updateRoomsCap'); !!}

@endsection