<!-- Modal -->
<div class="modal fade" id="qualificationModal" tabindex="-1" role="dialog" aria-labelledby="qualificationModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Qualification <sup>(Department)</sup></h4>
            </div>
            <form action="{{ route('school.store.department') }}" method="post" class="form" id="addQual">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="qualificationD">Qualification <sup>(Department)</sup></label><br/>
                        <select name="qualification" id="qualificationD" class="form-control select2" style="width: 100%" required>
                            <option value="" selected disabled>Select Qualification</option>
                            @foreach(getQualifications() as $item)
                                <option value="{{ $item->id }}">{{ $item->qualification_title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="head">Head of Department</label><br>
                        <select name="head" id="head" class="form-control select2" style="width: 100%">
                            <option value="" selected disabled>Select...</option>
                            @foreach(getSchoolTeach() as $item)
                                <option value="{{ $item->id }}">{{ ucwords(trim($item->first_name." ".$item->last_name)) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary pull-left">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>