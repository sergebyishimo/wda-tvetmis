<!-- Modal -->
<div class="modal fade" id="rtqfModal" tabindex="-1" role="dialog" aria-labelledby="rtqfModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add RTQF <sup>(Class)</sup></h4>
            </div>
            <form action="{{ route('school.class.store') }}" method="post" class="form" id="addClass">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="qualification">Qualification <sup>(Department)</sup></label><br/>
                        <select name="qualification" id="qualification" class="form-control select2 class-q"
                                style="width: 100%" required>
                            <option value="" selected disabled>Select Qualification</option>
                            @foreach(getQualifications(school(true)->id) as $item)
                                <option value="{{ $item->id }}">{{ $item->qualification_title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group level-group">
                        <label for="levelList">Pick Level(s)</label>
                        <div class="levels-list" id="levelList"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary pull-left">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>