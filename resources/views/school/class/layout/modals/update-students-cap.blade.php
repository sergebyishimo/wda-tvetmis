<!-- Modal -->
<div class="modal fade" id="studentsModal" tabindex="-1" role="dialog" aria-labelledby="studentsModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Update Students Capacity</h4>
            </div>
            <form action="{{ route('school.updade.students.capacity.department',['id' =>$department->id]) }}"
                  method="post" class="form" id="updateStudentsCap">
                @method("PATCH")
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="capacity_students">Students capacity</label><br/>
                        {!! Form::text('capacity_students', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary pull-left">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>