@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Staff
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('contentheader_title')
    <div class="container-fluid">
        School Gallery : {{ $description }}
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-2">
                <a href="{{ url()->previous()  }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</a>
            </div>
            <br><br>
            <div class="col-md-12">
                <div class="row">
                    @foreach($photos as $photo)
                        <div class="col-md-4">
                            <div class="box">
                                <img src="/storage/{{ $photo->photo  }}" alt="" style="max-width: 100%">

                                    <form method="post" action="{{ route('wda.gallery.delete', $photo->id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <p class="text-center">{{ str_replace('_', ' ', substr($photo->photo, 18))   }}<br>
                                            <button type="submit" class="btn btn-danger btn-xs " onclick="return window.confirm('Are you sure you want to delete this image?!')">Delete</button>
                                        </p>
                                    </form>


                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection