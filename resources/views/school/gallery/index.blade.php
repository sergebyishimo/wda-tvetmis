@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Staff
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    <style>
        .action-btn a {
            line-height: inherit;
            margin-bottom: 3px;
            margin-left: 3px;
            float: right;
        }
    </style>

@overwrite
@section('contentheader_title')
    <div class="container-fluid">
        School Gallery
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        @if (auth()->guard('wda')->check())
                            <form action="{{ route('wda.gallery.store') }}" method="post" enctype="multipart/form-data">
                        @endif
                        @if (auth()->guard('school')->check())
                            <form action="{{ route('school.gallery.store') }}" method="post" enctype="multipart/form-data">
                        @endif
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">School</label>
                                    @if (auth()->guard('school')->check())
                                    <select name="school_id" id="" class="form-control select2" disabled>
                                            <option value="{{  auth()->guard('school')->user()->id }}">{{ auth()->guard('school')->user()->name }}</option>
                                    </select>
                                    @endif
                                    @if (auth()->guard('wda')->check())
                                        <select name="school_id" id="" class="form-control select2">
                                            @foreach ($schools as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label for="">Choose Files</label>
                                    <input type="file" name="photos[]" multiple class="form-control-file" required>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Description</label>
                                    <textarea name="description" id="" class="form-control"></textarea>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        @if (auth()->guard('wda')->check())
                            <form action="{{ route('wda.gallery') }}" method="get" enctype="multipart/form-data">
                                @endif
                                @if (auth()->guard('school')->check())
                                    <form action="{{ route('school.gallery') }}" method="get" enctype="multipart/form-data">
                                @endif
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label for="">School</label>
                                                @if (auth()->guard('school')->check())
                                                    <select name="school_id" id="" class="form-control select2" disabled>
                                                        <option value="{{  auth()->guard('school')->user()->id }}">{{ auth()->guard('school')->user()->name }}</option>
                                                    </select>
                                                @endif
                                                @if (auth()->guard('wda')->check())
                                                    <select name="school_id" id="" class="form-control select2">
                                                        @foreach ($schools as $key => $value)
                                                            <option value="{{ $key }}">{{ $value }}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                            </div>
                                            <div class="col-md-4">
                                                <br>
                                                <button type="submit" class="btn btn-primary">View</button>
                                            </div>
                                        </div>
                                    </form>
                    </div>
                </div>
            </div>
        </div>
        @if (isset($galleries))
            <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Gallery</th>
                                            <th>Upload Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                        @forelse($galleries as $gallery)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $gallery->description  }}</td>
                                                <td>{{ date('d/m/Y', strtotime($gallery->created_at))  }}</td>
                                                <td>
                                                    @if(auth()->guard('wda')->check())
                                                        <form action="{{ route('wda.gallery.destroy', $gallery->description) }}" method="POST">
                                                            @method('DELETE')
                                                            @csrf
                                                            <a href="{{ route('wda.gallery.show', $gallery->description) }}?school_id={{ $school_id }}" class="btn btn-success btn-sm ">Open</a>
                                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                        </form>
                                                    @endif
                                                    @if(auth()->guard('school')->check())
                                                        <form action="{{ route('school.gallery.destroy', $gallery->description) }}" method="POST">
                                                            @method('DELETE')
                                                            @csrf
                                                            <a href="{{ route('school.gallery.show', $gallery->description) }}" class="btn btn-success btn-sm ">Open</a>
                                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" class="text-center">No Uploaded Pictures Yet</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection