@extends('school.desks.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Desks Management</span>
@endsection

@section('box-body')
    <div class="box">
        @if($schooldesk)
            @php($route = ['school.desks.update', $schooldesk->id])
            @php($id = "updateDeskRequest")
        @else
            @php($route = 'school.desks.store')
            @php($id = "createDeskRequest")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
            {!! Form::hidden('school_id', school(true)->id) !!}
        @if($schooldesk)
            @method("PATCH")
            {!! Form::hidden('id', $schooldesk->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('rtqf_level', "RTQF Level *") !!}
                        {!! Form::select('rtqf_level', getRtqfLevels(), $schooldesk?($schooldesk->level? $schooldesk->level->id : null):null,['class' => 'form-control select2', 'placeholder' => 'Choose RTQF Level']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('classrooms', "Number Of Classrooms *") !!}
                        {!! Form::text('classrooms', $schooldesk ? $schooldesk->classrooms: null, ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    {!! Form::label('desks', "Number Of Desks *") !!}
                    {!! Form::text('desks', $schooldesk ? $schooldesk->desks: null, ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $schooldesk ? "Update" : "Save" }}
                    </button>
                    <a href="{{ route('school.desks.index') }}"
                       class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('school.desks.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($schooldesk)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateSchoolDeskRequest', '#updateDeskRequest'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateSchoolDeskRequest', '#createDeskRequest'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $("#qualification").on("change", function () {
                var dep = $(this).val();
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@endsection