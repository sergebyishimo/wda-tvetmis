@extends('admin.layout.main')

@section('panel-title', "Dashboard")

@section('htmlheader_title', "District Dashboard")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <b><i class="fa fa-check-circle"></i> School Indicators</b>
            <div class="clear-fix">&nbsp;</div>
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{ $schools_nber }}</h3>
                            <p>Schools</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-institution"></i>
                        </div>
                        <a href="{{ route('district.schools') }}" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-md-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-gray">
                        <div class="inner">
                            <h3>{{ $have_internet }}</h3>
                            <p>Have Internet</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-rss"></i>
                        </div>
                        <a href="{{ route('district.school.internet') }}" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{ $have_electricity }}</h3>
                            <p>Have Electricity</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-lightbulb-o"></i>
                        </div>
                        <a href="{{ route('district.school.electricity') }}" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{{ $have_water }}</h3>
                            <p>Have Water</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shower"></i>
                        </div>
                        <a href="{{ route('district.school.water') }}" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <b><i class="fa fa-check-circle"></i> Student and Staff Indicators</b>
            <div class="clear-fix">&nbsp;</div>
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>{{ \App\Student::count() }}</h3>
                            <p>Students</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                            More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-md-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-black">
                        <div class="inner">
                            <h3>0</h3>

                            <p>Female</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-female"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-orange">
                        <div class="inner">
                            <h3>0</h3>

                            <p>Staff Members</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user-circle-o"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3>0</h3>

                            <p>Academic Staff</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user-secret"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
