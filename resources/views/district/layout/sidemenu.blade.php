@if(auth()->guard('district')->check())
    <li class="{{ activeMenu('district.home') }}">
        <a href="{{ route('district.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>
    {{-- User Management --}}

    <li class="treeview {{ activeMenu([
                'district.schools',  'district.edit.school', 'district.view.school',
                'district.school.internet','district.school.electricity', 'district.school.water',
                ]) }}">
        <a href="#">
            <i class="fa fa-table"></i>
            <span>Manage School</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="{{ activeMenu([
                            'district.schools',  'district.edit.school', 'district.view.school',
                            'district.school.internet','district.school.electricity', 'district.school.water',
                        ]) }}">
                <a href="{{ route('district.schools') }}">
                    <i class="fa fa-graduation-cap"></i><span>&nbsp;View Schools</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="treeview {{ activeMenu([
                'district.accr.application', 'district.view.accr.application',
                'district.school.assessment', 'district.view.school.assessment', 'district.sorting.school.assessment'
                ]) }}">
        <a href="#">
            <i class="fa fa-question-circle"></i>
            <span>Assessment</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="{{ activeMenu([ 'district.accr.application', 'district.view.accr.application' ])}}">
                <a href="{{ route('district.accr.application') }}"><i
                            class="fa fa-link"></i><span>School Application</span></a>
            </li>
            <li class="{{ activeMenu([ 'district.school.assessment', 'district.view.school.assessment', 'district.sorting.school.assessment' ])}}">
                <a href="{{ route('district.school.assessment') }}">
                    <i class="fa fa-link"></i><span>School Assessment</span></a>
            </li>
        </ul>
    </li>

@endif