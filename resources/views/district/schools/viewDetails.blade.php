@extends('district.layout.main')

@section('panel-title', "View School Information")

@section('htmlheader_title', "View School Information")


@section('panel-body')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    @if($school->school_logo)
                        <img src="{{ asset("storage/app/$school->school_logo") }}">
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>School Code</label>
                        <p>{{ $school->school_code }}</p>
                    </div>
                    <div class="form-group">
                        <label>School Name</label>
                        <p>{{ $school->school_name }}</p>
                    </div>
                    <div class="form-group">
                        <label>Province</label>
                        <p>{{ $school->province }}</p>
                    </div>
                    <div class="form-group">
                        <label>District</label>
                        <p>{{ $school->district }}</p>
                    </div>
                    <div class="form-group">
                        <label>Sector</label>
                        <p>{{ $school->sector }}</p>
                    </div>
                    <div class="form-group">
                        <label>Cell</label>
                        <p>{{ $school->cell }}</p>
                    </div>
                    <div class="form-group">
                        <label>Village</label>
                        <p>{{ $school->village }}</p>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <p>{{ $school->phone }}</p>
                    </div>
                    <div class="form-group">
                        <label>Date of Establishment</label>
                        <p>{{ $school->date_of_establishment }}</p>
                    </div>
                    <div class="form-group">
                        <label>Has Electricity</label>
                        <p>{{ ($school->has_electricity == 1) ? 'No' : 'Yes' }}</p>
                    </div>
                    <div class="form-group">
                        <label>Has Water</label>
                        <p>{{ ($school->has_water == 1) ? 'No' : 'Yes' }}</p>
                    </div>
                    <div class="form-group">
                        <label>Number of Desktops</label>
                        <p>{{ $school->number_of_desktops }}</p>
                    </div>
                    <div class="form-group">
                        <label>Has Computer Lab</label>
                        <p>{{ ($school->has_computer_lab == 1) ? 'No' : 'Yes' }}</p>
                    </div>


                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Number of workshops</label>
                        <p>{{ $school->number_of_workshops }}</p>
                    </div>
                    <div class="form-group">
                        <label>Number of Classrooms</label>
                        <p>{{ $school->number_of_classrooms }}</p>
                    </div>
                    <div class="form-group">
                        <label>Male Students </label>
                        <p>{{ $school->students_males }}</p>
                    </div>
                    <div class="form-group">
                        <label>Female Students</label>
                        <p>{{ $school->students_female }}</p>
                    </div>
                    <div class="form-group">
                        <label>Male Staff Members</label>
                        <p>{{ $school->staff_male }}</p>
                    </div>
                    <div class="form-group">
                        <label>Female Staff Members</label>
                        <p>{{ $school->staff_female }}</p>
                    </div>
                    <div class="form-group">
                        <label>Has Internet</label>
                        <p>{{ ($school->source_Internet == 1) ? 'No' : 'Yes' }}</p>
                    </div>
                    <div class="form-group">
                        <label>Number of Generators</label>
                        <p>{{ $school->number_of_generators }}</p>
                    </div>
                    <div class="form-group">
                        <label>Has Library</label>
                        <p>{{ ($school->has_library == 1) ? 'No' : 'Yes' }}</p>
                    </div>
                    <div class="form-group">
                        <label>Has Business or Strategic Plan</label>
                        <p>{{ ($school->has_business_or_strategic_plan == 1) ? 'No' : 'Yes' }}</p>
                    </div>
                    <div class="form-group">
                        <label>Number of Students Fed at School</label>
                        <p>{{ $school->number_of_students_fed_at_school  }}</p>
                    </div>
                    <div class="form-group">
                        <label>Has Feeding Program</label>
                        <p>{{ ($school->has_feeding_program == 1) ? 'No' : 'Yes' }}</p>
                    </div>


                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Latitude</label>
                        <p>{{ $school->latitude }}</p>
                    </div>
                    <div class="form-group">
                        <label>Longitude</label>
                        <p>{{ $school->longitude }}</p>
                    </div>
                    <div class="form-group">
                        <label>Manager Name</label>
                        <p>{{ $school->manager_name }}</p>
                    </div>
                    <div class="form-group">
                        <label>School Status</label>
                        <p>{{ $school->school_status }}</p>
                    </div>
                    <div class="form-group">
                        <label>Website</label>
                        <p>{{ $school->website }}</p>
                    </div>
                    <div class="form-group">
                        <label>Boarding / Day</label>
                        <p>{{ $school->boarding_or_day }}</p>
                    </div>
                    <div class="form-group">
                        <label>Accreditation Number</label>
                        <p>{{ $school->accreditation_number }}</p>
                    </div>
                    <div class="form-group">
                        <label>Accreditation Date</label>
                        <p>{{ $school->accreditation_date }}</p>
                    </div>
                    <div class="form-group">
                        <label>Discipline Total Marks</label>
                        <p>{{ $school->discipline_totalmarks }}</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection