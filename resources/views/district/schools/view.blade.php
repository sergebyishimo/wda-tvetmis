@extends('district.layout.main')

@section('panel-title', "School Management")

@section('htmlheader_title', "School Management")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped" id="myTable">
                            <thead>
                            <tr>
                                <th class="text-center" width="50px">#</th>
                                <th>School Name</th>
                                <th>Province</th>
                                <th>District</th>
                                <th>Accreditation</th>
                                <th>Phone Number</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($schools as $school)
                                <tr>
                                    <td class="text-center">{{ $i++ }}</td>
                                    <td>{{ $school->school_name }}</td>
                                    <td>{{ $school->province }}</td>
                                    <td>{{ $school->district }}</td>
                                    <td>{{ ($school->accreditation_status == 1) ? 'Accredited' : 'Not Accredited' }}</td>
                                    <td>{{ $school->phone }}</td>
                                    <td>
                                        <form method="POST">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="delete_school" value="{{ $school->id }}">

                                            <a href="{{ route('district.view.school', $school->id) }}"
                                               class="btn btn-warning btn-block btn-sm btn-flat"><i class="fa fa-list"></i> View
                                                More</a>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                dom: 'Blfrtip',
                ordering: false,
                searching: true,
                pager: true,
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $('#filterSearchremove').click(function (e) {
                e.preventDefault();
                $('#filterSearch').hide();
            });
            $('#columnremove').click(function (e) {
                e.preventDefault();
                $('#filtercolumns').hide();
            });
        });
    </script>
@endsection