@extends('admin.layout.main')

@section('panel-title', $school->school_name)

@section('htmlheader_title', $school->school_name)

@section('l-style')
    <div class="box">
        <div class="box-body">
            <table class="table table-bordered">
                @php($c_indicators=0)
                @php($c_answers=0)
                @foreach($qualities as $quality)
                    <thead class="bg-gray">
                    <tr>
                        <th>Quality Area</th>
                        <th class="text-left" width="200px">Criteria</th>
                        <th class="text-left">Indicator</th>
                        <th class="text-right pr-4">Answers</th>
                    </tr>
                    </thead>
                    {!! Form::open(['route' => ['district.store.school.assessment', $funct->id, $school->id]]) !!}
                    {!! Form::hidden('school_id', $school->id) !!}
                    {!! Form::hidden('function_id', $funct->id) !!}
                    {!! Form::hidden('quality_id', $quality->id) !!}
                    <tbody>
                    <tr>
                        <td class="bg-gray-light">
                            <div>{{ ucwords($quality->name) }}</div>
                            <div class="p-1 label text-sm label-info rounded shadow-sm">Scores:
                                {{ getQualityAreaScores($quality->criteria , $school) }}
                                / {{ getTotalWeightForQualityAreas($quality->criteria) }}</div>
                        </td>
                        <td colspan="3">
                            <table class="table">

                                @foreach ($quality->criteria as $criteria_section)
                                    <tr>
                                        <td class="bg-gray-light"
                                            width="188px">
                                            <div>{{ ucwords($criteria_section->criteria_section)  }}</div>
                                            <div class="p-1 label text-sm label-info rounded shadow-sm">
                                                Scores:
                                                {{ getCriteriaScores($criteria_section['criterias'], $school) }}
                                                / {{ $criteria_section->criterias->sum('weight') }}</div>
                                        </td>
                                        <td>
                                            <table class="table">
                                                @foreach ($criteria_section['criterias'] as $criteria)
                                                    @php($c_indicators += 1)
                                                    <thead>
                                                    <th class="text-right bg-gray-light p-0 pr-2"
                                                        colspan="3">
                                                        Verified
                                                    </th>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td width="430px">
                                                            @php($weight= strtolower($criteria->getSchoolAnswer($school)) == 'yes'? $criteria->weight : 0 )
                                                            <div>{{ ucwords($criteria->criteria) }}</div>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="p-1 label text-sm label-info rounded shadow-sm">
                                                                        Scores: {{ $weight }}
                                                                        / {{ $criteria->weight }}</div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td width="50px"
                                                            class="text-center {{ $criteria->getSchoolAnswer($school) ? '' : 'bg-danger' }}">
                                                            @if($criteria->getSchoolAnswer($school))
                                                                @php($c_answers += 1)
                                                                <div>{{ $criteria->getSchoolAnswer($school) }}</div>
                                                            @else
                                                                ---
                                                            @endif
                                                        </td>
                                                        <td align="right">
                                                            <div class="form-group">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio"
                                                                           id="customRadioY{{ $criteria->id }}"
                                                                           name="{{ 'district['.$criteria->id.']' }}"
                                                                           value="YES"
                                                                           required
                                                                           @if(old('district['.$criteria->id.']'))
                                                                           {{ old('district['.$criteria->id.']') == 'YES' ? 'checked' : '' }}
                                                                           @else
                                                                           {{ $criteria->getDistrictComment($school->id) == 'YES' ? 'checked' : '' }}
                                                                           @endif
                                                                           class="custom-control-input">

                                                                    <label class="custom-control-label"
                                                                           style="cursor: pointer"
                                                                           for="customRadioY{{ $criteria->id }}">YES</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio"
                                                                           id="customRadioN{{ $criteria->id }}"
                                                                           name="{{ 'district['.$criteria->id.']' }}"
                                                                           value="NO"
                                                                           @if(old('district['.$criteria->id.']'))
                                                                           {{ old('district['.$criteria->id.']') == 'NO' ? 'checked' : '' }}
                                                                           @else
                                                                           {{ $criteria->getDistrictComment($school->id) == 'NO' ? 'checked' : '' }}
                                                                           @endif
                                                                           required
                                                                           class="custom-control-input">

                                                                    <label class="custom-control-label"
                                                                           style="cursor: pointer"
                                                                           for="customRadioN{{ $criteria->id }}">NO</label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>
                    <tr class="bg-dark">
                        <td colspan="4">
                            <div>
                                <b>{{ ucwords($quality->name) }}</b>
                                <span class="pull-right p-1 text-sm label-info rounded shadow-sm">
                                            <i>
                                                <b>{{ getQualityAreaScores($quality->criteria , $school) }}
                                                    /{{ getTotalWeightForQualityAreas($quality->criteria) }}</b>
                                            </i>
                                        </span>
                            </div>
                        </td>
                    </tr>
                    <tr class="bg-dark">
                        <td>
                            <div class="form-group">
                                {!! Form::label('strength'.$quality->id, "Strength", ['class' => 'label-control']) !!}
                                {!! Form::textarea('strength', getCommentsDistrict($school->id,$funct->id,$quality->id, 'strength'), [
                                'class' => 'form-control', 'id' => 'strength',
                                'required' => true, 'rows' => 3 ]) !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! Form::label('weakness'.$quality->id, "Weakness", ['class' => 'label-control']) !!}
                                {!! Form::textarea('weakness', getCommentsDistrict($school->id,$funct->id,$quality->id, 'weakness'), [
                                'class' => 'form-control', 'id' => 'weakness'.$quality->id,
                                'required' => true,
                                 'rows' => '3']) !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! Form::label('recommendation'.$quality->id, "Recommendation", ['class' => 'label-control']) !!}
                                {!! Form::textarea('recommendation', getCommentsDistrict($school->id,$funct->id,$quality->id, 'recommendation'), [
                                'class' => 'form-control', 'id' => 'recommendation'.$quality->id,
                                'required' => true, 'rows' => 3 ]) !!}
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! Form::label('timeline'.$quality->id, "Time Line", ['class' => 'label-control']) !!}
                                {!! Form::textarea('timeline', getCommentsDistrict($school->id, $funct->id, $quality->id, 'timeline'), ['id' => 'timeline'.$quality->id,'class' => 'form-control', 'required' => true, 'rows' => '3' ]) !!}
                            </div>
                        </td>
                    </tr>
                    <tr class="bg-dark">
                        <td colspan="4">
                            {!! Form::submit('Save Decision', ['class' => 'btn btn-block btn-primary btn-md']) !!}
                        </td>
                    </tr>
                    </tbody>
                    {!! Form::close() !!}
                @endforeach
            </table>
        </div>
    </div>
@endsection