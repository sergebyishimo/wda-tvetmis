@extends('admin.layout.main')

@section('panel-title', "Assessment Provisional")

@section('htmlheader_title', "Assessment Provisional")

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection
@section('panel-body')
    <div class="box">
        <div class="box-header">
            <ul class="nav nav-tabs" id="nav-tab" role="tablist">
                <li role="presentation" class="active">
                    <a href="#input" aria-controls="INPUT" role="tab" data-toggle="tab">INPUT</a>
                </li>
                <li role="presentation">
                    <a href="#process" aria-controls="PROCESS" role="tab" data-toggle="tab">PROCESS</a>
                </li>
            </ul>
        </div>
        <div class="box-body" style="overflow-x: hidden;">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane active" id="input">
                    <table class="table table-bordered table-condensed text-center" id="dataTables"
                           style="width: 100%;">
                        <thead>
                        <tr>
                            @php
                                $c = 9;
                                $hideI = "";
                            @endphp
                            <th data-priority="1">School Name</th>
                            <th>District</th>
                            <th>Function</th>
                            <th>Scores</th>
                            <th>Rating</th>
                            <th>Stage/Status</th>
                            <th>Accreditation</th>
                            <th data-priority="2">Button</th>
                            @if($qualityInput)
                                @foreach($qualityInput as $quality)
                                    @php
                                        $hideI .= $c.',';
                                        $c++;
                                    @endphp
                                    <th class="text-primary">{{ $quality->name }}</th>
                                    @foreach ($quality->criteria as $criteria_section)
                                        @php
                                            $hideI .= $c.',';
                                            $c++;
                                        @endphp
                                        <th class="bg-gray-light">{{ ucwords($criteria_section->criteria_section) }}</th>
                                    @endforeach
                                @endforeach
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if($submitedInputs->count() > 0)
                            @foreach($submitedInputs as $assessment)
                                <tr>
                                    <td>{{ $assessment->school->school_name }}</td>
                                    <td>{{ $assessment->school->district }}</td>
                                    <td>{{ $assessment->functions->name }}</td>
                                    <td width="50px">
                                        @php
                                            $tt = getFunctionScores($assessment->functions->qualityAreas, $assessment->school, true);
                                            $hv = getFunctionScores($assessment->functions->qualityAreas, $assessment->school);
                                            $pp = ($hv * 100) / $tt;
                                            $pp = ceil($pp);
                                        @endphp
                                        {{ $pp ." %" }}
                                    </td>
                                    <td>
                                        <label class="label label-success text-sm p-1 rounded shadow-sm">{{ getRattingScore($pp) }}</label>
                                    </td>
                                    <td>
                                        @if($assessment->district == null)
                                            <label class="label label-info p-1 rounded shadow-sm">At
                                                district</label>
                                        @elseif($assessment->wda == null)
                                            <label class="label label-info p-1 rounded shadow-sm">At
                                                WDA</label>
                                        @else
                                            <label class="label label-info p-1 rounded shadow-sm">Viewed</label>
                                        @endif
                                    </td>
                                    <td>
                                        @if($assessment->district == 1 && $assessment->wda == 1 )
                                        @else
                                            <label class="label label-warning p-1 rounded shadow-sm">In
                                                Process</label>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('district.view.school.assessment', [$assessment->function_id, $assessment->school_id]) }}"
                                           class="btn btn-sm btn-dark btn-block">View</a>
                                    </td>
                                    @if($qualityInput)
                                        @foreach($qualityInput as $quality)
                                            <td>{{ getQualityAreaScores($quality->criteria , $assessment->school) }}</td>
                                            @foreach ($quality->criteria as $criteria_section)
                                                <td class="bg-gray-light">{{ getCriteriaScores($criteria_section['criterias'], $assessment->school) }}</td>
                                            @endforeach
                                        @endforeach
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="process">
                    <table class="table table-bordered text-center" id="dataTableProcess"
                           style="width: 100%;">
                        <thead>
                        <tr>
                            <th>School Name</th>
                            <th>District</th>
                            <th>Function</th>
                            <th>Scores</th>
                            <th>Rating</th>
                            <th>Stage/Status</th>
                            <th>Accreditation</th>
                            <th>Button</th>
                            @if($qualityProcess)
                                @php
                                    $c = 9;
                                    $hideP = "";
                                @endphp
                                @foreach($qualityProcess as $quality)
                                    @php
                                        $hideP .= $c.',';
                                        $c++;
                                    @endphp
                                    <th class="text-primary">{{ $quality->name }}</th>
                                    @foreach ($quality->criteria as $criteria_section)
                                        @php
                                            $hideP .= $c.',';
                                            $c++;
                                        @endphp
                                        <th class="bg-gray-light">{{ ucwords($criteria_section->criteria_section) }}</th>
                                    @endforeach
                                @endforeach
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if($submitedProcess->count() > 0)
                            @foreach($submitedProcess as $assessment)
                                <tr>
                                    <td>{{ $assessment->school->school_name }}</td>
                                    <td>{{ $assessment->school->district }}</td>
                                    <td>{{ $assessment->functions->name }}</td>
                                    <td width="50px">
                                        @php
                                            $tt = getFunctionScores($assessment->functions->qualityAreas, $assessment->school, true);
                                            $hv = getFunctionScores($assessment->functions->qualityAreas, $assessment->school);
                                            $pp = ($hv * 100) / $tt;
                                            $pp = ceil($pp);
                                        @endphp
                                        {{ $pp ." %" }}
                                    </td>
                                    <td>
                                        <label class="label label-success text-sm p-1 rounded shadow-sm">{{ getRattingScore($pp) }}</label>
                                    </td>
                                    <td>
                                        @if($assessment->district == null)
                                            <label class="label label-info p-1 rounded shadow-sm">At
                                                district</label>
                                        @elseif($assessment->wda == null)
                                            <label class="label label-info p-1 rounded shadow-sm">At
                                                WDA</label>
                                        @else
                                            <label class="label label-info p-1 rounded shadow-sm">Viewed</label>
                                        @endif
                                    </td>
                                    <td>
                                        @if($assessment->district == 1 && $assessment->wda == 1 )
                                        @else
                                            <label class="label label-warning p-1 rounded shadow-sm">In
                                                Process</label>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('wda.view.school.assessment', [$assessment->function_id, $assessment->school_id]) }}"
                                           class="btn btn-sm btn-dark btn-block">View</a>
                                    </td>
                                    @if($qualityProcess)
                                        @foreach($qualityProcess as $quality)
                                            <td>{{ getQualityAreaScores($quality->criteria , $assessment->school) }}</td>
                                            @foreach ($quality->criteria as $criteria_section)
                                                <td class="bg-gray-light">{{ getCriteriaScores($criteria_section['criterias'], $assessment->school) }}</td>
                                            @endforeach
                                        @endforeach
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#dataTables").dataTable({
                dom: 'Blfrtip',
                pager: true,
                responsive: true,
                scrollX: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
            $("#dataTableProcess").dataTable({
                dom: 'Blfrtip',
                pager: true,
                responsive: true,
                scrollX: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
        });
    </script>
@endsection