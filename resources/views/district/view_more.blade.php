@extends('admin.layout.main')

@section('panel-title', "Provisional AQA : ".$school_info->school_name)

@section('htmlheader_title', "Provisional AQA : ".$school_info->school_name)

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <link href="{{ asset('css/smart_wizard.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/smart_wizard_theme_arrows.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/smart_wizard_theme_dots.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('panel-body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{--Start Section 1 : Input--}}
                <div class="card  card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Section 1 : Input</h3>
                        <a class="pull-right btnPrint" style="margin-top: -27px;"
                           href="/school/accreditation/print/{{ $application_id }}"><i class="fa fa-print"></i>
                            Print</a>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div id="myForm" role="form" accept-charset="utf-8">

                            <!-- SmartWizard html -->
                            <div id="smartwizard">
                                <ul>
                                    <li><a href="#step-1-1">Step 1<br/>
                                            <small>School Information</small>
                                        </a></li>
                                    <li><a href="#step-1-2">Step 2<br/>
                                            <small>Attachments</small>
                                        </a></li>
                                    <li><a href="#step-1-3">Step 3<br/>
                                            <small>Materials & Infrastructure</small>
                                        </a></li>
                                    <li><a href="#step-1-4">Step 4<br/>
                                            <small>Programs Offered</small>
                                        </a></li>
                                    <li><a href="#step-1-5">Step 5<br/>
                                            <small>Trainers / Teachers</small>
                                        </a></li>
                                    <li><a href="#step-1-6">Step 6<br/>
                                            <small>Admin Staff</small>
                                        </a></li>
                                    <li><a href="#step-1-7">Step 7<br/>
                                            <small>Applying For</small>
                                        </a></li>
                                    <li><a href="#step-1-8">Step 8<br/>
                                            <small>General Recommendation</small>
                                        </a></li>
                                </ul>
                                <div style="padding: 10px 20px;background: #fff">
                                    <div id="step-1-1">
                                        <h5><i class="fa fa-check-circle"></i> School Information</h5>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Name:</label>
                                                    <p>{{ $school_info['school_name'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Province:</label>
                                                    <p>{{ ucwords($school_info['province']) }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">District:</label>
                                                    <p>{{ $school_info['district'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Sector:</label>
                                                    <p>{{ $school_info['Sector'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Cell:</label>
                                                    <p>{{ $school_info['cell'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Village:</label>
                                                    <p>{{ $school_info['village'] }}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Manager Name:</label>
                                                    <p>{{ $school_info['manager_name'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Manager Phone:</label>
                                                    <p>{{ $school_info['manager_phone'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Manager E-mail:</label>
                                                    <p>{{ $school_info['manager_'] }}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Latitude:</label>
                                                    {{ $school_info['latitude'] }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Longitude:</label>
                                                    <p>{{ $school_info['longitude'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Phone:</label>
                                                    <p>{{ $school_info['phone'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">E-mail:</label>
                                                    <p>{{ $school_info['email'] }}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">School Status:</label>
                                                    <p>{{ $school_info['school_status'] }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Owner Name:</label>
                                                    <p>{{ $school_info['owner_name'] }}</p>

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Owner Phone:</label>
                                                    <p>{{ $school_info['owner_phone'] }}</p>

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Owner Type:</label>
                                                    <p>{{ $school_info['owner_type'] }}</p>

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Owner E-mail:</label>
                                                    <p>{{ $school_info['owner_email'] }}</p>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="submit"
                                                        class="btn btn-primary btn-block btn-flat"><i
                                                            class="fa fa-save"></i> Save & Continue <i
                                                            class="fa fa-arrow-right"></i></button>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#step-1-2"
                                                           class="btn btn-default btn-block btn-flat"> Next <i
                                                                    class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="step-1-2">
                                        <h5><i class="fa fa-check-circle"></i> Uploaded Attachments</h5>

                                        <form method="POST" action="/district/accreditation/save">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="form" value="attachments">
                                            <input type="hidden" name="application_id"
                                                   value="{{ $application_id}}">

                                            <input type="hidden" name="section" value="1">
                                            <input type="hidden" name="step" value="2">
                                            <input type="hidden" name="class" value="App\Model\Accr\AccrAttachment">

                                            @php
                                                //$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 2)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 2)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th style="width: 15%"></th>
                                                    <th>Attachment Name</th>
                                                    <th style="width: 25%">Download File</th>
                                                    <th>Verified</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($attachments_data as $ad)
                                                    <input type="hidden" name="attachment_id[]"
                                                           value="{{ $ad->id }}">
                                                    <tr>
                                                        <td class="text-center">{{$i++ }}</td>
                                                        <td>{{ $ad->source }}</td>
                                                        <td>
                                                            <a href="{{ Storage::url("$ad->attachment")}}/{{ $school_info['school_name'] . ' ' . str_replace('/', ' ', $ad->source ) }}">Download
                                                                File</a></td>
                                                        <td>
                                                            <select class="form-control flat" name="verified[]">
                                                                <option @if($ad->district_verified == 'Yes') selected @endif>
                                                                    Yes
                                                                </option>
                                                                <option @if($ad->district_verified == 'No') selected @endif>
                                                                    No
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                            <div>
                                                @include('district.form')
                                            </div>

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="#step-1-1"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-5">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue <i
                                                                class="fa fa-arrow-right"></i></button>
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="#step-1-3"
                                                       class="btn btn-default btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="step-1-3">
                                        <h5><i class="fa fa-check-circle"></i> Materials &amp; Infrastructure
                                        </h5>

                                        @php
                                            //$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 3)->first();
                                            $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 3)->first();
                                        @endphp

                                        <form method="POST" action="/district/accreditation/save">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="form" value="attachments">

                                            <input type="hidden" name="application_id"
                                                   value="{{ $application_id}}">

                                            <input type="hidden" name="section" value="1">
                                            <input type="hidden" name="step" value="3">
                                            <input type="hidden" name="class" value="App\Model\Accr\AccrBuildingsAndPlots">

                                            <table class="table table-bordered table-hover">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Building</th>
                                                    <th>Infrastructure</th>
                                                    <th>Purpose</th>
                                                    <th>Size</th>
                                                    <th>Capacity</th>
                                                    <th style="width: 120px;">Action</th>
                                                    <th>Verified</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($buildings_plots as $bp)
                                                    <input type="hidden" name="attachment_id[]"
                                                           value="{{ $bp->id }}">
                                                    <tr>
                                                        <td></td>
                                                        <td>{{ $bp->name }}</td>
                                                        <td>

                                                            @foreach ($bp['infras'] as $infra)
                                                                {{ $infra->quantity. ' ' .$infra->name .  ', ' }}
                                                            @endforeach

                                                        </td>
                                                        <td>{{ $bp->purpose }}</td>
                                                        <td>{{ $bp->size }}</td>
                                                        <td>{{ $bp->capacity }}</td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-warning btn-flat btn-sm"
                                                                    data-toggle="modal"
                                                                    data-target="#view_{{ $bp->id }}"><i
                                                                        class="fa fa-tasks"></i> View More
                                                            </button>

                                                            <div class="modal fade" id="view_{{ $bp->id }}">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                            <h4 class="modal-title"><i
                                                                                        class="fa fa-home"></i>
                                                                                View Building Information</h4>
                                                                        </div>

                                                                        <div class="modal-body"
                                                                             style="padding: 30px;height: 450px;overflow-y: scroll;divbackground: #f0f1f3">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Building / Plot
                                                                                            Name</label>
                                                                                        <p>{{ $bp->name }}</p>
                                                                                    </div>


                                                                                    <div class="form-group">
                                                                                        <label>Building / Plot
                                                                                            Class</label>
                                                                                        <p>{{ $bp->class }}</p>
                                                                                    </div>

                                                                                    <div class="form-group row"
                                                                                         id="addBuilding2">
                                                                                        <label class="col-md-12 col-form-label">Infrastructure</label>
                                                                                        @foreach ($bp['infras'] as $infra)
                                                                                            <div class="col-md-6">
                                                                                                <p>{{ $infra->name }}</p>
                                                                                            </div>
                                                                                            <div class="col-md-5">
                                                                                                <p>{{ $infra->quantity }}</p>
                                                                                            </div>
                                                                                        @endforeach

                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Purpose</label>
                                                                                        <p>{{ $bp->purpose }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Size</label>
                                                                                        <p>{{ $bp->size }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Capacity</label>
                                                                                        <p>{{ $bp->capacity }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Construction
                                                                                            Materials</label>
                                                                                        <p>{{ $bp->construction_materials }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Roofing
                                                                                            Materials</label>
                                                                                        <p>{{ $bp->roofing_materials }}</p>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label for=""
                                                                                               class="col-md-5 col-form-label">Harvests
                                                                                            Rain Water</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->harvests_rain_water }}</p>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Water</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_water }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Electricity</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_electricity }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Internet</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_internet }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Extinguisher</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_extinguisher }}</p>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Lightening
                                                                                            Arrestor</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_lightening_arrestor }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            External
                                                                                            Lighting</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_external_lighting }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Furniture</label>
                                                                                        <p>{{ $bp->furniture }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Equipment</label>
                                                                                        <p>{{ $bp->equipment }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer"
                                                                             style="text-align: center;">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <button type="button"
                                                                                            class="btn btn-default btn-block btn-flat"
                                                                                            data-dismiss="modal">
                                                                                        <i class="fa fa-close"></i>
                                                                                        Cancel
                                                                                    </button>
                                                                                </div>
                                                                            </div>


                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <select class="form-control flat" name="verified[]">
                                                                <option @if($bp->district_verified == 'Yes') selected @endif>
                                                                    Yes
                                                                </option>
                                                                <option @if($bp->district_verified == 'No') selected @endif>
                                                                    No
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                                </tbody>
                                            </table>

                                            <div>
                                                @include('district.form')
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="#step-1-2"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-5">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue <i
                                                                class="fa fa-arrow-right"></i></button>
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="#step-1-4"
                                                       class="btn btn-default btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>
                                        </form>

                                    </div> <!--  Step 3 -->

                                    <div id="step-1-4">
                                        <h5><i class="fa fa-check-circle"></i> Programs Offered</h5>

                                        <form method="POST" action="/district/accreditation/save">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="form" value="attachments">
                                            <input type="hidden" name="application_id"
                                                   value="{{ $application_id}}">

                                            <input type="hidden" name="section" value="1">
                                            <input type="hidden" name="step" value="4">
                                            <input type="hidden" name="class" value="App\Model\Accr\ProgramsOffered">

                                            @php
                                                //	$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                                                    $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Sector</th>
                                                    <th style="width: 15%">Sub-Sector</th>
                                                    <th style="width: 15%">Qualification</th>
                                                    <th style="width: 15%">RTQF Level</th>
                                                    <th style="width: 10%">Male Trainees</th>
                                                    <th style="width: 10%">Female Trainees</th>
                                                    <th>Curriculum</th>
                                                    <th>Verified</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($programs_offered as $pd)
                                                    <input type="hidden" name="attachment_id[]"
                                                           value="{{ $pd->id }}">
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $pd->subsector->sector->tvet_field }}</td>
                                                        <td>{{ $pd->subsector->sub_field_name }}</td>
                                                        <td>{{ $pd->rtqf_level->level_name }}</td>
                                                        <td>{{ $pd->qualification->qualification_title }}</td>
                                                        <td>{{ $pd->male_trainees }}</td>
                                                        <td>{{ $pd->female_trainees }}</td>
                                                        <td>{{ $pd->curriculum }}</td>
                                                        <td>
                                                            <select class="form-control" name="verified[]">
                                                                <option @if($pd->district_verified == 'Yes') selected @endif>
                                                                    Yes
                                                                </option>
                                                                <option @if($pd->district_verified == 'No') selected @endif>
                                                                    No
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>

                                            <div>
                                                @include('district.form')
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="#step-1-3"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-5">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue <i
                                                                class="fa fa-arrow-right"></i></button>
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="#step-1-5"
                                                       class="btn btn-default btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="step-1-5">
                                        <h5><i class="fa fa-check-circle"></i> Trainer / Teachers</h5>

                                        <form method="POST" action="/district/accreditation/save">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="form" value="attachments">
                                            <input type="hidden" name="application_id"
                                                   value="{{ $application_id}}">

                                            <input type="hidden" name="section" value="1">
                                            <input type="hidden" name="step" value="5">
                                            <input type="hidden" name="class" value="App\Model\Accr\Trainer">

                                            @php
                                                //$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th style="width: 10%">Gender</th>
                                                    <th>Level / Degree</th>
                                                    <th>Qualification</th>
                                                    <th>Certification</th>
                                                    <th>Experience</th>
                                                    <th>Modules Taught</th>
                                                    <th>Verified</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($trainers as $td)
                                                    <input type="hidden" name="attachment_id[]"
                                                           value="{{ $td->id }}">
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $td->names }}</td>
                                                        <td>{{ $td->gender }}</td>
                                                        <td>{{ $td->q_level->name }}</td>
                                                        <td>{{ $td->qualification }}</td>
                                                        <td>{{ $td->certification }}</td>
                                                        <td>{{ $td->experience }}</td>
                                                        <td>{{ $td->modules_taught }}</td>
                                                        <td>
                                                            <select class="form-control flat" name="verified[]">
                                                                <option @if($td->district_verified == 'Yes') selected @endif>
                                                                    Yes
                                                                </option>
                                                                <option @if($td->district_verified == 'No') selected @endif>
                                                                    No
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach


                                                </tbody>
                                            </table>
                                            <div>
                                                @include('district.form')
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="#step-1-4"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-5">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue <i
                                                                class="fa fa-arrow-right"></i></button>
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="#step-1-6"
                                                       class="btn btn-default btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="step-1-6">
                                        <h5><i class="fa fa-check-circle"></i> Administration Staff</h5>

                                        <form method="POST" action="/district/accreditation/save">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="form" value="attachments">
                                            <input type="hidden" name="application_id"
                                                   value="{{ $application_id}}">

                                            <input type="hidden" name="section" value="1">
                                            <input type="hidden" name="step" value="6">
                                            <input type="hidden" name="class" value="App\Model\Accr\StaffAdministrator">

                                            @php
                                                //$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Gender</th>
                                                    <th>Position</th>
                                                    <th>Qualification</th>
                                                    <th>Institution Studied</th>
                                                    <th>Qualification Level</th>
                                                    <th>Teaching Experience</th>
                                                    <th>Assessor Qualification</th>
                                                    <th>Verified</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($staffs as $sd)
                                                    <input type="hidden" name="attachment_id[]"
                                                           value="{{ $sd->id }}">
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $sd->names }}</td>
                                                        <td>{{ $sd->gender }}</td>
                                                        <td>{{ $sd->position }}</td>
                                                        <td>{{ $sd->academic_qualification }}</td>
                                                        <td>{{ $sd->institution_studied }}</td>
                                                        <td>{{ $sd->q_level->name }}</td>
                                                        <td>{{ $sd->teaching_experience }}</td>
                                                        <td>{{ $sd->assessor_qualification }}</td>
                                                        <td>
                                                            <select class="form-control flat" name="verified[]">
                                                                <option @if($sd->district_verified == 'Yes') selected @endif>
                                                                    Yes
                                                                </option>
                                                                <option @if($sd->district_verified == 'No') selected @endif>
                                                                    No
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach


                                                </tbody>
                                            </table>
                                            <div>
                                                @include('district.form')
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="#step-1-5"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-5">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue <i
                                                                class="fa fa-arrow-right"></i></button>
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="#step-1-7"
                                                       class="btn btn-default btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="step-1-7">
                                        <h5><i class="fa fa-check-circle"></i> Qualification Applied For</h5>

                                        <form method="POST" action="/district/accreditation/save">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="form" value="attachments">
                                            <input type="hidden" name="application_id"
                                                   value="{{ $application_id}}">

                                            <input type="hidden" name="section" value="1">
                                            <input type="hidden" name="step" value="7">
                                            <input type="hidden" name="class"
                                                   value="App\Model\Accr\AccrQualificationsApplied">

                                            @php
                                                //	$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                                    $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();

                                            @endphp

                                            <table class="table table-bordered table-hover">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th></th>
                                                    <th style="width: 30%">Sector</th>
                                                    <th style="width: 20%">Sub Sector</th>
                                                    <th style="width: 20%">Curriculum Qualification</th>
                                                    <th>RTQF Level</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($application_data as $ad)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $ad->curr->subsector->sector->tvet_field }}</td>
                                                        <td>{{ $ad->curr->subsector->sub_field_name }}</td>
                                                        <td>{{ $ad->curr->qualification_title }}</td>
                                                        <td>{{ $ad->rtqf->level_name }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div>
                                                @include('district.form')
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="#step-1-6"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-5">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue <i
                                                                class="fa fa-arrow-right"></i></button>
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="#step-1-8"
                                                       class="btn btn-default btn-block btn-flat"> Next <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="step-1-8">
                                        <h5><i class="fa fa-check-circle"></i> General Recommendation</h5>

                                        <form method="POST" action="/district/accreditation/save">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="form" value="general_recommendation">

                                            <input type="hidden" name="application_id"
                                                   value="{{ $application_id }}">

                                            @php
                                                //$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();

                                            @endphp

                                            <div class="row">

                                                <div class="form-group col-md-12">
                                                    <div class="mb-3">
														<textarea class="textarea" name="recommendation"
                                                                  placeholder="General Recommendation"
                                                                  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                                  required>{{ $comments->recommendation or "" }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Average Marks</label>
                                                    <select class="form-control flat" name="marks">
                                                        <option {{ (isset($comments) && $comments->marks == 1) ? 'selected' : '' }}>
                                                            1
                                                        </option>
                                                        <option {{ (isset($comments) && $comments->marks == 2) ? 'selected' : '' }}>
                                                            2
                                                        </option>
                                                        <option {{ (isset($comments) && $comments->marks == 3) ? 'selected' : '' }}>
                                                            3
                                                        </option>
                                                        <option {{ (isset($comments) && $comments->marks == 4) ? 'selected' : '' }}>
                                                            4
                                                        </option>
                                                        <option {{ (isset($comments) && $comments->marks == 5) ? 'selected' : '' }}>
                                                            5
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a href="#step-7"
                                                       class="btn btn-default btn-block btn-flat"> <i
                                                                class="fa fa-arrow-left"></i> Previous </a>
                                                </div>
                                                <div class="col-md-8">
                                                    <button type="submit"
                                                            class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue To The
                                                        Next Section <i class="fa fa-arrow-right"></i></button>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
                {{--End Section 1 : Input--}}
                {{--Start Section 2 : Input--}}
                {{--<div class="card">--}}
                {{--<div class="card-header">--}}
                {{--<h3 class="card-title">Section 2 : Process</h3>--}}
                {{--</div>--}}
                {{--<div class="card-body">--}}
                {{--<div id="smartwizard2">--}}
                {{--<ul>--}}
                {{--@php--}}
                {{--$i = 1;--}}
                {{--$j = 1;--}}
                {{--@endphp--}}
                {{--@foreach ($criteria_sections as $criteria_section)--}}
                {{--<li><a href="#step-2-{{ $i }}">Step {{ $i++ }}<br/>--}}
                {{--<small>{{ $criteria_section->criteria_section }}</small>--}}
                {{--</a></li>--}}

                {{--@endforeach--}}
                {{--</ul>--}}

                {{--<div style="padding: 10px 20px;background: #fff">--}}


                {{--@php--}}
                {{--$i = 1;--}}
                {{--@endphp--}}
                {{--@foreach ($criteria_sections as $criteria_section)--}}
                {{--<div id="step-2-{{ $i }}">--}}

                {{--<h5>--}}
                {{--<i class="fa fa-check-circle"></i> {{ $criteria_section->criteria_section }}--}}
                {{--</h5>--}}

                {{--<form method="POST" action="/district/accreditation/save">--}}
                {{--{{ csrf_field() }}--}}

                {{--<input type="hidden" name="form" value="attachments">--}}
                {{--<input type="hidden" name="application_id"--}}
                {{--value="{{ $application_id}}">--}}

                {{--<input type="hidden" name="section" value="2">--}}
                {{--<input type="hidden" name="step" value="{{ $i}}">--}}

                {{--@php--}}
                {{--//$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 2)->where('step', $i)->first();--}}
                {{--$comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 2)->where('step', $i)->first();--}}
                {{--@endphp--}}


                {{--@foreach ($criteria_section['criterias'] as $criteria)--}}
                {{--<input type="hidden" name="criteria_id[]"--}}
                {{--value="{{ $criteria->id }}">--}}

                {{--@php--}}
                {{--$answer = App\Model\Accr\AccrCriteriaAnswer::where('school_id', $school_info->id)->where('criteria_id', $criteria->id)->first();--}}
                {{--@endphp--}}

                {{--<div class="form-group row">--}}
                {{--<label class="col-md-8 col-form-label">{{ $criteria->criteria }}</label>--}}
                {{--<div class="col-md-2">--}}
                {{--																	{{ $answer->answer }}--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--@endforeach--}}



                {{--@if (count($criteria_section['criterias']) == 0)--}}
                {{--<p class="text-center">No Questions set for this section.</p>--}}
                {{--@else--}}
                {{--<div>--}}
                {{--<hr>--}}
                {{--@include('district.form')--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-2">--}}
                {{--<a href="#step-2-{{ ($i - 1) }}" class="btn btn-default btn-block btn-flat"> <i class="fa fa-arrow-left"></i> Previous </a>--}}
                {{--</div>--}}
                {{--<div class="col-md-5">--}}
                {{--<button type="submit"--}}
                {{--class="btn btn-primary btn-block btn-flat">--}}
                {{--<i class="fa fa-save"></i> Save & Continue <i--}}
                {{--class="fa fa-arrow-right"></i></button>--}}
                {{--</div>--}}
                {{--<div class="col-md-5">--}}
                {{--<a href="#step-2-{{ ($i + 1) }}" class="btn btn-default btn-block btn-flat"> Next <i class="fa fa-arrow-right"></i></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--@endif--}}

                {{--</form>--}}
                {{--<br>--}}


                {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-6">--}}

                {{--@if ($i == 1)--}}

                {{--@else--}}
                {{--<a href="#step-2-{{ ($i - 1) }}"--}}
                {{--class="btn btn-default btn-block btn-flat"> <i--}}
                {{--class="fa fa-arrow-left"></i>--}}
                {{--Previous--}}
                {{--</a>--}}
                {{--@endif--}}


                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                {{--<a href="#step-2-{{ ($i + 1) }}" class="btn btn-default btn-block btn-flat"> Next <i class="fa fa-arrow-right"></i></a>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--@php--}}
                {{--$i++;--}}
                {{--@endphp--}}
                {{--@endforeach--}}

                {{--@if (count($criteria_sections) == 0)--}}
                {{--<p class="text-center">No Criteria Sections Found</p>--}}
                {{--@endif--}}

                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<br clear="left">--}}
                {{--<div class="card-footer"></div>--}}
                {{--</div>--}}
                {{--End Section 2 : Input--}}
                {{--Start Section 3 : Input--}}
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Section 2 : Output</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Graduation Year</label>
                                <div class="col-md-2">
                                    <select class="form-control flat" name="view_year">
                                        @for ($i = date('Y'); $i > date('Y')  - 5 ; $i--)
                                            <option>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-primary btn-flat btn-block"><i
                                                class="fa fa-list"></i> View
                                    </button>
                                </div>

                            </div>
                        </form>

                        <form method="POST" action="/district/accreditation/save">
                            {{ csrf_field() }}

                            <input type="hidden" name="form" value="attachments">
                            <input type="hidden" name="application_id" value="{{ $application_id}}">

                            <input type="hidden" name="section" value="3">
                            <input type="hidden" name="step" value="1">

                            @php
                                //$comments = App\Model\Accr\DistrictProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 3)->where('step', 1)->first();
                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 3)->where('step', 1)->first();
                            @endphp


                            <table class="table table-bordered table-hover">
                                <thead style="background: #eaeaea">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Trade</th>
                                    <th>RTQF Achieved</th>
                                    <th>Phone</th>
                                    <th>Employment Status</th>
                                    <th>Employment Timing</th>
                                    <th>Monthly Salary</th>
                                    <th>Name of Employment Company</th>
                                    <th>Sector</th>
                                    <th>Sub Sector</th>
                                    <th>Employer Contact</th>
                                    <th>Knowledge level</th>
                                    <th>Attitude level</th>
                                    <th>Skills level</th>

                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($graduates as $graduate)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $graduate->names }}</td>
                                        <td>{{ $graduate->gender }}</td>
                                        <td>{{ $graduate->trade }}</td>
                                        <td>{{ $graduate->rtqf_achieved }}</td>
                                        <td>{{ $graduate->phone_number }}</td>
                                        <td>{{ $graduate->employment_status }}</td>
                                        <td>{{ $graduate->employment_timing }}</td>
                                        <td>{{ $graduate->monthly_salary }}</td>
                                        <td>{{ $graduate->name_of_employment_company }}</td>
                                        <td>{{ $graduate->sector }}</td>
                                        <td>{{ $graduate->sub_sector }}</td>
                                        <td>{{ $graduate->employer_contact }}</td>
                                        <td>{{ $graduate->knowledge_level }}</td>
                                        <td>{{ $graduate->attitude_level }}</td>
                                        <td>{{ $graduate->skill_level }}</td>
                                    </tr>
                                @endforeach

                                @if (count($graduates) == 0)
                                    <tr>
                                        <td colspan="20" class="text-center">No Graduates Recorded in The
                                            Selected Period
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <br>
                            <hr>
                            <br>
                            @include('district.form')
                            <div class="row">

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat"><i
                                                class="fa fa-save"></i> Save & Continue <i
                                                class="fa fa-arrow-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br clear="left">
                    <div class="card-footer"></div>
                </div>
                {{--End Section 3 : Input--}}
            </div>
        </div>
    </div>

@endsection
@section('l-scripts')
    <script src="{{ asset('js/jquery.printPage.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.smartWizard.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            // Toolbar extra buttons


            $('#smartwizard2').smartWizard({
                selected: 0,
                theme: 'default',
                keyNavigation: false,

                transitionEffect: 'fade',
                toolbarSettings: {
                    toolbarPosition: 'bottom',
                    showNextButton: true,
                    showPreviousButton: true
                },
                anchorSettings: {
                    anchorClickable: true,
                    enableAllAnchors: true,
                    markDoneStep: true, // add done css
                    markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                    removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                    enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                }
            });

            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'default',
                keyNavigation: false,

                transitionEffect: 'fade',
                toolbarSettings: {
                    toolbarPosition: 'bottom',
                    showNextButton: true,
                    showPreviousButton: true
                },
                anchorSettings: {
                    anchorClickable: true,
                    enableAllAnchors: true,
                    markDoneStep: true, // add done css
                    markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                    removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                    enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                }
            });


        });

        $("#finish").click();
    </script>
    <script>
        $(document).ready(function () {
            $(".btnPrint").printPage();
        });
    </script>
@endsection