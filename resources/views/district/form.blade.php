<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Strength</label>
            <div class="mb-3">
<textarea class="textarea" name="strength" placeholder="Strength"
          style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>{{ $comments->strength or "" }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label>Weakness</label>
            <div class="mb-3">
    <textarea class="textarea" name="weakness" placeholder="Weakness"
              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>
            {{ $comments->weakness or "" }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label>Opportunity</label>
            <div class="mb-3">
    <textarea class="textarea" name="opportunity" placeholder="opportunity"
              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>
                                                                                    {{ $comments->opportunity or "" }}</textarea>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Threat</label>
            <div class="mb-3">
        <textarea class="textarea" name="threat" placeholder="Threat"
                  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>
                                                                                    {{ $comments->threat or "" }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label>Recommendation</label>
            <div class="mb-3">
    <textarea class="textarea" name="recommendation" placeholder="Recommendation"
              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>
                                                                                    {{ $comments->recommendation or "" }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label>Marks</label>
            <select class="form-control flat" name="marks">
                <option {{ (isset($comments) && $comments->marks == 1) ? 'selected' : '' }}>1</option>
                <option {{ (isset($comments) && $comments->marks == 2) ? 'selected' : '' }}>2</option>
                <option {{ (isset($comments) && $comments->marks == 3) ? 'selected' : '' }}>3</option>
                <option {{ (isset($comments) && $comments->marks == 4) ? 'selected' : '' }}>4</option>
                <option {{ (isset($comments) && $comments->marks == 5) ? 'selected' : '' }}>5</option>
            </select>
        </div>
    </div>

</div>