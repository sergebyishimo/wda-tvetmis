@extends('admin.layout.main')

@section('panel-title', "Provisional AQA")

@section('htmlheader_title', "Provisional AQA")

@section('panel-body')
	<div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered" style="background: #fff" id="dataType">
						<thead style="background: #e8e8e8">
						<tr>
							<td class="text-center">#</td>
							<th title="Accreditation Type">Accr Type</th>
							<th>School Name</th>
							<th>Sector</th>
							<th>Sub Sector</th>
							<th>Qualification</th>
							<th>Submitted</th>
							<th>Button</th>
						</tr>
						</thead>

						<tbody>
						@php
							$i = 1;
						@endphp
						@foreach ($apps as $app)
							<tr>
								<td class="text-center">{{ $i++ }}</td>
								<td>{{ $app->accreditationType ? $app->accreditationType->type : '' }}</td>
								<td>{{ $app->school_name }}</td>
								<td>
									@foreach ($app->applied_for as $applied_for)
										{{ $applied_for->curr->subsector->sector->tvet_field }} ,
									@endforeach
								</td>
								<td>
									@foreach ($app->applied_for as $applied_for)
										{{ $applied_for->curr->subsector->sub_field_name }} ,
									@endforeach
								</td>
								<td>
									@foreach ($app->applied_for as $applied_for)
										{{ $applied_for->curr->qualification_title }} ,
									@endforeach
								</td>
								<td>{{ date('F d, Y ', strtotime($app->updated_at)) }}</td>
								<td>
									<a href="{{ route('view.accr.application', $app->id) }}" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> View More</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
@endsection
@section('l-scripts')
	@parent
	<script type="text/javascript">
        $(function () {
            $("#dataType").dataTable({
                dom: 'Blfrtip',
                pager: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
        });
	</script>
@endsection