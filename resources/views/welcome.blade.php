<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126663778-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-126663778-1');
    </script>

</head>
<body>

<nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144"
     style="background-color: rgb(255, 255, 255);">
    <div class="container">
        <div class="row bg-white p-2" style="width: inherit !important;">
            <div class="col-md-10">
                <div class="navbar-toggleable-sm">
                    <span class="navbar-logo"><a href="/"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                    <span><a class="navbar-brand" href="/">{{ config('app.name') }}</a></span>
                    <!-- Example single danger button -->
                    <a class="btn btn-sm btn-primary shadow-sm" href="{{ route('home.dashboard') }}">
                        <span>Dashboard</span> </a>
                    <a class="btn btn-sm btn-dark shadow-sm"
                       href="{{ route('curricula.index') }}">
                        <span>Curriculum</span>
                    </a>

                    <a class="btn btn-sm btn-info shadow-sm"
                       href="{{ route('home.schools') }}">
                        <span>Schools</span>
                    </a>

                    <span class="dropdown">
                        <button class="btn btn-sm btn-warning shadow-sm btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Support
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               target="_blank"
                               href="{{ url('https://solutions.rp.ac.rw') }}">Help Desk</a>
                            <a class="dropdown-item"
                               href="{{ route('contact.us') }}">Contact Us</a>
                        </div>
                    </span>

                    <a class="btn btn-sm btn-success shadow-sm" href="{{ route('home.attachments') }}">
                        <span>Downloads</span>
                    </a>

                    <a class="btn btn-sm btn-success shadow-sm" href="{{ url('https://gis.rp.ac.rw/') }}">
                        <span>GIS</span>
                    </a>

                    <a class="btn btn-sm btn-primary shadow-sm" href="{{ url('results') }}">
                        <span>Senior Six Results</span>
                    </a>

                </div>
            </div>
            <div class="col-md-2">
                <div class="btn-group" style="float: right !important;">
                    <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        General Login
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('school.login') }}">Schools / Staffs Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('/college/login') }}">Colleges
                            Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('rp.login') }}">RP Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('district.login') }}">District Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('wda.login') }}">WDA Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/register') }}">REB | Create Account</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/login') }}">REB | Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
         style="background-image: url({{ asset('img/wlc_bg_n_4.jpg') }});background-attachment: fixed;">
    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="mbr-section-title display-5 text-dark bg-white shadow-md rounded p-2 pt-0">
                        <div class="card">
                            <div class="card-header">
                                Check Admission Decision
                            </div>
                            <div class="card-body">
                                {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => "verify"]) !!}
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            {!! Form::label('Registration Number') !!}
                                            {!! Form::text('reg_no', isset($feedback)? $reg : null, ['class' => 'form-control', 'required' => true]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">&nbsp;</label>
                                        {!! Form::submit('Verify', ['class' => 'btn btn-sm btn-primary btn-block']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                @isset($feedback)
                                    <div class="">
                                        @if($feedback[1] == '2')
                                            <div class="alert alert-warning alert-dismissible fade show"
                                                 role="alert">
                                                <strong>Eeeh !!</strong>
                                                <br>
                                                {!! $feedback[0] !!}
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @else
                                            <div class="alert alert-{{ $feedback[1] == '0' ? 'danger' : 'success' }} alert-dismissible fade show"
                                                 role="alert">
                                                <strong>{{ $feedback[1] == '1' ? 'Yes, Admitted' : 'Oops Not Admitted' }}</strong>
                                                <br>
                                                {!! $feedback[0] !!}
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                @endisset
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="mbr-buttons--left col-md-4">
                            <div class="dropdown">
                                <button class="btn btn-secondary btn-lg btn-warning shadow-sm dropdown-toggle"
                                        type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    Students
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ url('/student/login') }}">Students Login</a>
                                    <a class="dropdown-item" href="{{ route('student.signup.private') }}">
                                        Private Applicants (Account Creation && First Year Only)
                                    </a>
                                    <a class="dropdown-item" href="{{ route('student.signup.continuing') }}">
                                        Continuing Students (Account Creation Only)
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="mbr-buttons--left col-md-4">
                            <a class="btn btn-lg btn-primary shadow-sm" href="{{ url('http://elearning.rp.ac.rw/') }}">eLearning
                                Login</a>
                        </div>
                        <div class="mbr-buttons--left col-md-4">
                            <a class="btn btn-lg btn-info shadow-sm" href="{{ url('http://application.rp.ac.rw/') }}">Admission
                                Login</a>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                </div>
                <div class="col-md-5">
                    <div class="card shadow-lg">
                        <div class="card-body">
                            <img src="{{ asset('img/app_logo.jpg') }}" style="width: 405px;"
                                 alt="">
                        </div>
                    </div>
                    @if(getNoticeBoard())
                        <div class="clearfix">&nbsp;</div>
                        <div class="card shadow-lg">
                            <div class="bg-warning card-header">{!! getNoticeBoard('title') !!}</div>
                            <div class="card-body bg-info">
                                {!! getNoticeBoard('message') !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>

<section class="mbr-section mbr-section-small mbr-footer" id="contacts1-m" data-rv-view="178"
         style="background-color: rgb(55, 56, 62); padding-top: 4.5rem; padding-bottom: 4.5rem;">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="img-responsive">
                    <img src="{{ asset('img/app_logo.jpg') }}" width="200px">
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>
                    <strong>Address</strong><br>
                    Kicukiro Niboye<br>
                    KK 15 Rd, Kigali
                </p>
            </div>
            <div class="col-xs-12 col-md-3">
                <p><strong>Contacts</strong><br>
                    Email: rp@gmail.com<br>
                    Phone: +250 788 4564<br>
                </p>
            </div>
            <div class="col-xs-12 col-md-3"><strong>Links</strong>
                <ul>
                    <li><a href="#">RP Website</a></li>
                    <li><a href="#">WDA Website</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>

</body>
</html>
