@extends('wda.layout.main')

@section('panel-title', "Dashboard")

@section('htmlheader_title', "WDA Portal")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <b><i class="fa fa-check-circle"></i> School Indicators</b>
            <div class="clear-fix">&nbsp;</div>
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{ $schools_nber }}</h3>
                            <p>Schools</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-institution"></i>
                        </div>
                        <a href="{{ route('wda.schools') }}" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{{ $private }}</h3>
                            <p>Private Schools</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <a href="{{ route('wda.school.water') }}" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-gray">
                        <div class="inner">
                            <h3>{{ $have_internet }}</h3>
                            <p>Have Internet</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-rss"></i>
                        </div>
                        <a href="{{ route('wda.school.internet') }}" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{ $have_electricity }}</h3>
                            <p>Have Electricity</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-lightbulb-o"></i>
                        </div>
                        <a href="{{ route('wda.school.electricity') }}" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                
            </div>
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <h3>Schools per province per school status</h3>
                    <canvas id="chartjs-0"></canvas>
                </div>
                <div class="col-md-12">
                    <br><br>
                    <h3>Trainers per Province by gender</h3>
                    <canvas id="chartjs-3"></canvas>
                </div>
                <div class="col-md-12">
                    <br><br>
                    <h3>Students by Province by Gender</h3>
                    <canvas id="chartjs-1"></canvas>
                </div>
                <div class="col-md-12">
                    <br><br>
                    <h3>Count of Students by TVET Sector</h3>
                    <canvas id="chartjs-4"></canvas>
                </div>
                <div class="col-md-12">
                    <br><br>
                    <h3>Schools Per District</h3>
                    <canvas id="chartjs-5"></canvas>
                </div>

                <div class="col-md-12">
                    <br><br>
                    <h3>Staffs Per District</h3>
                    <canvas id="chartjs-6"></canvas>
                </div>

                <div class="col-md-12">
                    <br><br>
                    <h3>Trades / Sub Sector Statistics</h3>
                    <div class="row">
                        <form method="get" action="{{ route('wda.home') }}">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Sector</label>
                                    <select name="sector_id" id="" class="form-control">
                                        @foreach ($db_sectors as $sector)
                                            <option value="{{ $sector->id }}" @if(request('sector_id') == $sector->id) selected @endif>{{ $sector->tvet_field }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="btn btn-primary">View</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>



                <div class="col-md-12">
                    <h3>Staffs Per Sub Sector</h3>
                    <canvas id="chartjs-7"></canvas>
                </div>

                <div class="col-md-12">
                    <br clear="left">
                    <h3>Students Per Sub Sector</h3>
                    <canvas id="chartjs-8"></canvas>
                </div>

            </div>
            <br><br>
            <b><i class="fa fa-check-circle"></i> Student and Staff Indicators</b>
            <div class="clear-fix">&nbsp;</div>
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>{{ \App\Student::count() }}</h3>
                            <p>Students</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                            More info
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-md-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-black">
                        <div class="inner">
                            <h3>{{ App\Student::where('gender', 'Female')->count() }}</h3>

                            <p>Female</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-female"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-orange">
                        <div class="inner">
                            <h3>{{ App\StaffsInfo::count() }}</h3>

                            <p>Staff Members</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user-circle-o"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3>{{ App\StaffsInfo::where('privilege', 7)->count() }}</h3>

                            <p>Academic Staff</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user-secret"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("l-scripts")
    @parent
    @include('jsview')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
        var publicSchools = {
            label: 'Public Schools',
            data: publics,
            backgroundColor: 'rgba(99, 132, 0, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-public-schools"
        };

        var govSchools = {
            label: 'Govt Schools',
            data: gov,
            backgroundColor: 'rgba(0, 99, 132, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-gov-schools"
        };

        var privateSchools = {
            label: 'Private Schools',
            data: privates,
            backgroundColor: '#f39c12',
            borderWidth: 0,
            yAxisID: "y-axis-private-schools"
        };

        var data = {
            labels: provinces,
            datasets: [publicSchools, govSchools, privateSchools]
        };

        var chartOptions = {
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6
                }],
                yAxes: [{
                    id: "y-axis-public-schools"
                }, {
                    id: "y-axis-gov-schools"
                }, {
                    id: "y-axis-private-schools"
                }]
            },
            events: false,
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 0
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        };

        var barChart = new Chart(document.getElementById("chartjs-0"), {
            type: 'bar',
            data: data,
            options: chartOptions
        });

        maleStaffs = {
            label: 'Male Staffs',
            data: staff_males,
            backgroundColor: 'rgba(99, 132, 0, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-staff-males"
        };

        femaleStaffs = {
            label: 'Female Staffs',
            data: staff_females,
            backgroundColor: 'rgba(0, 99, 132, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-staff-females"
        };

        data = {
            labels: provinces,
            datasets: [maleStaffs, femaleStaffs]
        };

        chartOptions = {
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6
                }],
                yAxes: [{
                    id: "y-axis-staff-males"
                }, {
                    id: "y-axis-staff-females"
                }]
            },
            events: false,
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 0
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        };

        barChart = new Chart(document.getElementById("chartjs-3"), {
            type: 'bar',
            data: data,
            options: chartOptions
        });


        maleStudents = {
            label: 'Male Students',
            data: students_male,
            backgroundColor: 'rgba(99, 132, 0, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-students-males"
        };

        femaleStudents = {
            label: 'Female Students',
            data: students_female,
            backgroundColor: 'rgba(0, 99, 132, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-students-females"
        };

        data = {
            labels: provinces,
            datasets: [maleStudents, femaleStudents]
        };

        chartOptions = {
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6
                }],
                yAxes: [{
                    id: "y-axis-students-males"
                }, {
                    id: "y-axis-students-females"
                }]
            },
            events: false,
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 0
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        };

        barChart = new Chart(document.getElementById("chartjs-1"), {
            type: 'bar',
            data: data,
            options: chartOptions
        });

        new Chart(document.getElementById("chartjs-4"),
            {"type":"bar","data":
                    {
                        "labels":sectors,
                        "datasets":[
                            {"label":"Count of Students by TVET Sector","data":sectors_statistics,
                                "fill":false,
                                "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                                "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});

        // Schools Per District

        new Chart(document.getElementById("chartjs-5"),
            {"type":"bar","data":
                    {
                        "labels":districts,
                        "datasets":[
                            {"label":"Schools Per District","data":districts_stats,
                                "fill":false,
                                "backgroundColor":["#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a","#00a65a"],
                                }]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});

        // Staff Per District

        new Chart(document.getElementById("chartjs-6"),
            {"type":"bar","data":
                    {
                        "labels":districts,
                        "datasets":[
                            {"label":"Staff Per District","data":staff_districts_stats,
                                "fill":false,
                                "backgroundColor":["#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7"],
                            }]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});


        // Staff Per Trade

        new Chart(document.getElementById("chartjs-7"),
            {"type":"bar","data":
                    {
                        "labels":trades_arr,
                        "datasets":[
                            {"label":"Saff Per Trade","data":staffs_trades_stats,
                                "fill":false,
                                "backgroundColor":["#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7"],
                            }]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});

        // Students Per Trade

        new Chart(document.getElementById("chartjs-8"),
            {"type":"bar","data":
                    {
                        "labels":trades_arr,
                        "datasets":[
                            {"label":"Students Per Trade","data":students_trades_stats,
                                "fill":false,
                                "backgroundColor":["#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7","#0073b7"],
                            }]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});


    </script>
@endsection