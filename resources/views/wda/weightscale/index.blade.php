@extends('wda.layout.main')

@section('htmlheader_title')
        Weight Scale
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Weight Scale
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @if($manageweightscale)
                        @php($route = ['wda.weightscale.update', $manageweightscale->id])
                        @php($id = "updateWeightScale")
                    @else
                        @php($route = 'wda.weightscale.store')
                        @php($id = "createWeightScale")
                    @endif
                    {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                    @if($manageweightscale && $weightscale)
                        @method("PATCH")
                        {!! Form::hidden('id', $weightscale->id) !!}
                    @endif
                    <div class="box-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('grade_id', "Choose Grade * ") !!}
                                    {!! Form::select('grade_id', $grades->pluck('grade_name','id') , null , ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('weightscale_from', "Weight Scale From *") !!}
                                    {!! Form::text('weightscale_from', $weightscale ? $weightscale->weightscale_from: null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('weightscale_to', "Weight Scale To *") !!}
                                    {!! Form::text('weightscale_to',$weightscale ? $weightscale->weightscale_to: null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-md btn-primary pull-left">
                                    {{ $manageweightscale ? "Update Weight Scale" : "Add Weight Scale" }}
                                </button>
                                <a href="{{ route('wda.weightscale.index') }}"
                                   class="btn btn-md btn-warning pull-right">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

            <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>Grade</th>
                                <th>Weight Scale(%)</th>
                                <th>Weight Name(%)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($weightscales as $weightscale)
                                <tr>
                                    <td class="col-md-6">{{$weightscale->grade->grade_name or ""}}</td>
                                    <td class="col-md-6">{{$weightscale->weightscale_from or ""}}</td>
                                    <td class="col-md-6">{{$weightscale->weightscale_to or ""}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($manageweightscale)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateWeightScale', '#updateWeightScale'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateWeightScale', '#createWeightScale'); !!}
    @endif
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@show