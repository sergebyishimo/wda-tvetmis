@extends('wda.layout.main')

@section('panel-title', "Calendar")

@section('htmlheader_title', "WDA Portal")
@section('l-style')
    @parent
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection
@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            {!! $calendar->calendar() !!}
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {!! $calendar->script() !!}
@endsection