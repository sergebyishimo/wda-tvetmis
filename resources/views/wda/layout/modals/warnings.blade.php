<div class="modal fade " tabindex="-1" role="dialog" id="removeLevel">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Are Sure To Remove This Level ?</h4>
            </div>
            <form action=""
                  method="post">
                @method("DELETE")
                {{ csrf_field() }}
                <input type="hidden" class="level" name="level" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger pull-left">Yes, Continue</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade " tabindex="-1" role="dialog" id="disableDepartment">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Are Sure To Remove This Qualification ?</h4>
            </div>
            <form action=""
                  method="post">
                @method("DELETE")
                {{ csrf_field() }}
                <input type="hidden" class="department" name="department" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger pull-left">Yes, Continue</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade " tabindex="-1" id="assignQualifications" role="dialog" aria-labelledby="assignQualifications">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Assign Qualifications</h4>
            </div>
            <form action="{{ route('wda.school.assign.qualification') }}"
                  class="form"
                  role="form"
                  method="get">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="sector" class="control-label">Sector</label>
                        <select name="r_i" data-url="{{ url('wda/get/trades') }}" class="form-control select2"
                                style="width: 100%;"
                                id="sector">
                            <option value="" disabled selected>Choose Here</option>
                            @isset($sectors)
                                @foreach ($sectors as $sector)
                                    <option value="{{ $sector->id }}"
                                            @if( isset($curr_info) && $curr_info->subsector->sector->id == $sector->id) selected @endif >{{ $sector->tvet_field }}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="trades" class="control-label">Sub Sector</label>
                        <select name="d_d" class="form-control select2" id="trades" style="width: 100%;" required>
                            <option value="" disabled selected>Choose Here</option>
                            @isset($sub_sectors)
                                @foreach ($sub_sectors as $subtrade)
                                    <option value="{{ $subtrade->id }}"
                                            @if( isset($curr_info) && $curr_info->sub_sector_id == $subtrade->id) selected @endif >{{  $subtrade->sub_field_name }}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="schoolModalList" class="control-label">School</label>
                        <select name="o_o" class="form-control" id="schoolModalList" style="width: 100%;" required>
                            <option value="" disabled selected>Choose Here </option>
                                @isset($schools)
                                    @foreach($schools as $school)
                                        <option value="{{ $school->id }}">{{ $school->school_name }}</option>
                                    @endforeach
                                @endisset
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning pull-right" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary pull-left">Yes, Continue</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade " tabindex="-1" id="uploadSchools" role="dialog" aria-labelledby="assignQualifications">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading"
                    style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Upload Schools</h4>
            </div>
            <form method="POST" action="{{ route('wda.school.upload') }}" class="form" role="form" method="get" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <ul>
                        <li>Click Here To Download The Format:</li>
                        <li style="list-style-type: none"><a href="/files/Schools Upload Format.xlsx">Schools Upload Format</a></li>
                    </ul>
                    <br>
                    <b>Choose The File To Be Uploaded Here:</b>
                    <input type="file" name="file" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning pull-right" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary pull-left">Yes, Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="advancedSearch" aria-labelledby="advancedSearch">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-info">
            <div class="modal-header panel-heading"
                 style="border-top-left-radius: inherit;border-top-right-radius: inherit;">
                <h4 class="modal-title">Make Search</h4>
            </div>
            <form action=""
                  method="get">
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Based on Curricula</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sector" class="control-label">Sector</label>
                                        <select name="r_i" data-url="{{ url('wda/get/trades') }}"
                                                class="form-control select2"
                                                style="width: 100%;"
                                                id="sector">
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($sectors)
                                                @foreach ($sectors as $sector)
                                                    <option value="{{ $sector->id }}"
                                                            @if( isset($curr_info) && $curr_info->subsector->sector->id == $sector->id) selected @endif >{{ $sector->tvet_field }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="trades" class="control-label">Sub Sector</label>
                                        <select name="d_d" class="form-control select2"
                                                data-url="{{ url('wda/get/quafication') }}" id="trades" style="width: 100%;">
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($sub_sectors)
                                                @foreach ($sub_sectors as $subtrade)
                                                    <option value="{{ $subtrade->id }}"
                                                            @if( isset($curr_info) && $curr_info->sub_sector_id == $subtrade->id) selected @endif >{{  $subtrade->sub_field_name }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="qua" class="control-label">Qualification</label>
                                        <select name="qualification_id" class="form-control select2" id="qua"
                                                style="width: 100%;" required>
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($currs)
                                                @foreach ($currs as $curr)
                                                    <option value="{{ $curr->uuid }}" {{ isset($mod) ? $curr->uuid == $mod->qualification_id ? 'selected' : '' : "" }} >
                                                        {{  $curr->qualification_title }}
                                                    </option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning pull-right" data-dismiss="modal">Dismiss</button>
                    <button type="submit" class="btn btn-info pull-left">Yes, Search</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->