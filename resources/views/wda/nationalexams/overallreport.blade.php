@extends('wda.layout.main')

@section('htmlheader_title')
    National Examination Statistics
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Overall Report
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-body" style="overflow-x: auto;">
                        <div style="text-align: center;">National Examinations Overview Statistics: Academic
                            Year 2018
                        </div>
                        <table class="table table-bordered table-responsive table-condensed table-hover"
                               style="table-border-color-dark: #0c0c0c;border-width: thin;" id="mdtable0">
                            <thead>
                            <tr>
                                <th>Year</th>
                                <th>Province</th>
                                <th width="120px" class="text-center">District</th>
                                <th width="100px" class="text-center">#Schools</th>
                                <th width="100px" class="text-center">Male</th>
                                <th width="100px" class="text-center">Female</th>
                                <th width="100px" class="text-center">Total</th>
                                <th width="100px" class="text-center">%Pass</th>
                                <th width="100px" class="text-center">%Fail</th>
                                <th width="100px" class="text-center">Rank</th>
                                <th width="100px" class="text-center">%Special Needs</th>
                                <th width="100px" class="text-center">%Overall</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--<tr>--}}
                            @php
                                $year = null;
                                $province = null;
                            @endphp
                            @foreach($basedDistrict as $item)
                                <tr>
                                    @if($year != $item->created_at->year)
                                        <td valign="middle"
                                            rowspan="{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'year', $item->created_at->year) }}">
                                            {{ $year != $item->created_at->year ? $item->created_at->year : '' }}
                                        </td>
                                    @endif
                                    @if($province != $item->province)
                                        <td valign="middle"
                                            rowspan="{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'province', $item->province) }}">{{  $item->province }}</td>
                                    @endif
                                    <td width="120px" class="text-center">{{ $item->district }}</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'schools') }}</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'male') }}</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'female') }}</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'total') }}</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'pass') }} %</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'fail') }} %</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'rank') }} </td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'special') }}</td>
                                    <td width="98px"
                                        class="text-center">{{ getNumbersForDistrictFromCollection($fullData, $item->district, 'overall') }}</td>
                                </tr>
                                @php
                                    $year = $item->created_at->year;
                                    $province = $item->province;
                                @endphp
                            @endforeach
                            {{--</tr>--}}
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--Datables--}}
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(function () {
            $("#mdtable0").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: false,
                processing: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
            });
        });
    </script>
@show