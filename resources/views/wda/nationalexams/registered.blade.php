@extends('wda.layout.main')
@section('htmlheader_title')
    National Examination Statistics
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Registered Candidates
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    @if(!$details)
        <div class="container-fluid spark-screen">
            <div class="row">
                <div class="col-md-12">
                    <!-- List box -->
                    <div class="box">
                        <div class="box-body" style="overflow-x: auto;">
                            <table class="table table-bordered table-responsive table-condensed table-hover"
                                   style="table-border-color-dark: #0c0c0c;border-width: thin;"
                                   id="dataTableBtnListServer">
                                <thead>
                                <tr>
                                    <td colspan="13">
                                        <div style="text-align: center;">Databases of registred candidates academic
                                            Year {{date('Y')}}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>School Name</th>
                                    <th>Province</th>
                                    <th>District</th>
                                    <th>School Code</th>
                                    <th>Tot cand</th>
                                    @php
                                        $cols = [];
                                        $columnsjs = [];
                                    @endphp
                                    @foreach($columns as $item)
                                        <th width="120px" class="text-center">{{ $item['combination_code'] }}</th>
                                        @php
                                            $cols[] = $item['combination_code'];
                                            $columnsjs[] = [
                                            'name' => strtolower($item['combination_code']),
                                            'data' => strtolower($item['combination_code']),
                                            'orderable' => false,
                                            'searchable'  => false
                                            ];
                                        @endphp
                                    @endforeach
                                    @php
                                        unset($columns)
                                    @endphp
                                </tr>
                                </thead>
                                <tbody>
                                {{--@foreach($schools as $school)--}}
                                {{--<tr>--}}
                                {{--<td>{{$school->school_name}}</td>--}}
                                {{--<td>{{$school->province}}</td>--}}
                                {{--<td>{{$school->district}}</td>--}}
                                {{--<td>{{$school->school_code}}</td>--}}
                                {{--<td class="text-center">--}}
                                {{--<a href="{{ route('wda.nationalexams.registered',['details'=> true,'level_id'=> $levels_5_id,'school_id'=>$school->id]) }}">--}}
                                {{-- {{solveTotalCanditate($level_5_students,$levels_5_id,$school->id)}}--}}
                                {{--0--}}
                                {{--</a>--}}
                                {{--</td>--}}
                                {{--@foreach($cols as $d)--}}
                                {{--@if($loop->index > 4)--}}
                                {{--<td class="text-center">--}}
                                {{--<a href="#">--}}
                                {{--{{solveCandidatePerCourse($level_5_students,$levels_5_id,$school->id,$d)}}--}}
                                {{--0--}}
                                {{--</a>--}}
                                {{--</td>--}}
                                {{--@endif--}}
                                {{--@endforeach--}}
                                {{--</tr>--}}
                                {{--@endforeach--}}
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
            </div>
        </div>
    @else
        <div class="container-fluid spark-screen">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-responsive table-condensed table-hover"
                           style="table-border-color-dark: #0c0c0c;border-width: thin;" id="mydt">
                        <thead>
                        <tr>
                            <td colspan="13">
                                <div style="text-align: center;">Candidates in:
                                    <label>{{$school_name}}</label> @if($program_code) in :
                                    <label>{{$program_code}}@endif</label></div>
                            </td>
                        </tr>
                        <tr>
                            <td>Student Names</td>
                            <td>Gender</td>
                            @if($school_name && $program_code)
                                <td>{{ $program_code }}</td>
                            @else
                                <td>Program Code</td>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $datum)
                            <tr>
                                <td>{{ $datum->names }}</td>
                                <td>{{ $datum->gender }}</td>
                                <td>{{ resolveProgramFromIndex($datum->index_number) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('l-scripts')
    @parent
    {{--Datables--}}
    <script>
        $(function () {
            let staticColms = [
                {'data' : 'school_name', 'name' : 'accr_schools_information.school_name', 'orderable': true , 'searchable' : true},
                {'data' : 'province', 'name' : 'accr_schools_information.province', 'orderable': true , 'searchable' : true},
                {'data' : 'district', 'name' : 'accr_schools_information.district', 'orderable': true , 'searchable' : true},
                {'data' : 'school_code', 'name' : 'accr_schools_information.school_code', 'orderable': true , 'searchable' : true}
            ];
            let columnsLet = '{!! json_encode($columnsjs) !!}';
            columnsLet = staticColms.concat(columnsLet);
            console.log(columns);

            $("#dataTableBtnListServer").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: true,
                processing: true,
                ajax: "{{ route('wda.get.nationalexams.registered.data') }}",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columns: columnsLet
            });
        });
    </script>
    <script>
        $(function () {
            $("#mydt").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: false,
                processing: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
            });
            $("#dataTableBtnListServerTop").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: false,
                processing: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                "order": [[0, 'desc'], [6, 'desc']]
            });
            // $("#mdtable").dataTable({
            //     dom: 'Blfrtip',
            //     pager: true,
            //     serverSide: false,
            //     processing: true,
            // });
        });
    </script>
@show