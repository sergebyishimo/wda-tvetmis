@if(isset($html))
    <table class="table table-responsive table-condensed table-bordered" id="dataTableBtnList">
        <thead>
        <tr>
            <th class="text-center">Student Name</th>
            <th class="text-center">School Name</th>
            <th class="text-center">Province</th>
            <th class="text-center">District</th>
            <th class="text-center">School Status</th>
            <th class="text-center">Option</th>
            <th class="text-center">Sex</th>
            <th class="text-center">Aggregate</th>
            <th class="text-center">Decision</th>
            <th class="text-center">Position</th>
            @if(isset($courses))
                @php
                    $courss=[];
                @endphp
                @foreach($courses as $cours)
                    @php
                        $courss[] = $cours->id;
                    @endphp
                    @if($course  != null)
                        @if($course == $cours->id)
                            <th class="text-center">{{ $cours->course_name }}</th>
                            @php
                                break;
                            @endphp
                        @endif
                    @else
                        <th class="text-center">{{ $cours->course_name }}</th>
                    @endif
                @endforeach
            @endif
            {{--<th></th>--}}
        </tr>
        </thead>
        <tbody>
        @if($students)
            @php
                $x = 1;
            @endphp
            @foreach($students as $rstudent)
                <tr>
                    <td class="text-center">{{ ucwords($rstudent->fname ." " .$rstudent->fname) }}</td>
                    <td class="text-center">{{ $rstudent->school_name }}</td>
                    <td class="text-center">{{ $rstudent->province }}</td>
                    <td class="text-center">{{ $rstudent->district }}</td>
                    <td class="text-center">{{ $rstudent->school_status }}</td>
                    <td class="text-center">{{ $rstudent->combination_name }}</td>
                    <td class="text-center">{{ $rstudent->gender }}</td>
                    <td class="text-center">
                        @php
                            $agt = getStudentAggregate($rstudent->student_index)
                        @endphp
                        {{ $agt }}
                    </td>
                    <td class="text-center">
                        {{ calculateRemark($agt) }}
                    </td>
                    <td class="text-center">{{ $x++ }}</td>
                    {{--@foreach($presults as $result)--}}
                    @foreach($courss as $coursss)
                        {{--@if($result->course_id == $coursss)--}}
                        @php
                            $marks = getStudentMarksByCourse($rstudent->student_index, $coursss);
                        @endphp
                        <td class="text-center">
                            <b>{{ getNationalExamGrade($marks) }}</b><sub>{{ $marks }}</sub>
                        </td>
                        {{--@endif--}}
                    @endforeach
                    {{--@endforeach--}}
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endif