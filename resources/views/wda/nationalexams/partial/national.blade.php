@if(isset($html))
    <table class="table table-striped table-bordered" id="dataTableBtnListServerNational">
        <thead>
        <tr>
            <th>Student Name</th>
            <th>School Name</th>
            <th>Province</th>
            <th>District</th>
            <th>Sector</th>
            <th>School Status</th>
            <th>Option</th>
            <th>Sex</th>
            <th>Aggregate</th>
            <th>Total Marks</th>
            <th>Decision</th>
            <th>Position</th>
            {{--<th></th>--}}
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endif

@if(isset($js))
    <script>
        $(function () {

            let url = "{{ $url }}";
            function replaceAll(str, find, replace) {
                return str.replace(new RegExp(find, 'g'), replace);
            }
            let furl = replaceAll(url,"&amp;", "&");
            console.log(furl);
            $("#dataTableBtnListServerNational").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: true,
                processing: true,
                ajax: furl,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columns: [
                    {
                        data: 'names',
                        name: 'names',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'school_name',
                        name: 'accr_schools_information.school_name',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'province',
                        name: 'accr_schools_information.province',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'district',
                        name: 'accr_schools_information.district',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'sector',
                        name: 'accr_schools_information.sector',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'school_status',
                        name: 'accr_schools_information.school_status',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'combination_name',
                        name: 'combinations.combination_name',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'gender',
                        name: 'students.gender',
                        orderable: false,
                        searchable: false
                    },
                    {data: 'aggregate', name: 'aggregate', orderable: false, searchable: false},
                    {data: 'total_marks', name: 'total_marks', orderable: false, searchable: false},
                    {data: 'decision', name: 'decision', orderable: false, searchable: false},
                    {data: 'DT_Row_Index', name: 'DT_Row_Index', orderable: false, searchable: false}
                ],
                'columnDefs': [
                    {
                        "targets": 8, // your case first column
                        "className": "text-center",
                    },
                    {
                        "targets": 9, // your case first column
                        "className": "text-center",
                    },
                    {
                        "targets": 10, // your case first column
                        "className": "text-center",
                    }

                ]
            });
        });
    </script>
@endif