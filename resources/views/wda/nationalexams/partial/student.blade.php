<div style="display: flow;">
    <label>REPUBLIC OF RWANDA</label><br>
    <label>MINISTRIY OF EDUCATION</label><br>
    <label>RWANDA POLYTECHNIC</label><br>
    <label>WORKFORCE DEVELOPMENT AUTHORITY</label><br>
    @if($student && $student->mschool)
        <label>SCHOOL: {{$student->mschool->school_name }}</label><br>
    @endif
    <label>NAMES: {{ $student->names or ""}}</label><br>
</div>
<table class="table table-responsive">
    <thead>
    <th class="mb">Course Name</th>
    <th>Aggregate</th>
    </thead>
    <tbody>
        @foreach($results as $result)
            <tr>
            <td>{{$result->course->course_name or ""}}</td>
            <td>{{getNationalExamGrade($result->marks)}}</td>
            <tr>
        @endforeach
    </tbody>
</table>
<div class="row">
    <dv class="col-md-4">
        @if($student)
            <label>TOTAL AGGREGATE: </label> <label>{{getStudentAggregate($student->index_number)}}</label>
        @endif
    </dv>
</div>