@extends('wda.layout.main')

@section('htmlheader_title')
    National Examination Statistics
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Per program Report
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-header">
                        <div style="text-align: center;">Table showing Student performance by Province and
                            District: Academic 2018
                        </div>
                    </div>
                    <div class="box-body" style="overflow-x: auto;">
                        <table class="table table-striped table-bordered" id="mdtable">
                            <thead>
                            <tr>
                                <th>Year</th>
                                <th>Province</th>
                                <th>District</th>
                                <th>Student</th>
                                <th>Sex</th>
                                <th>School</th>
                                <th>OPTION</th>
                                <th>TOT</th>
                                <th>AGG</th>
                                <th>MENTION</th>
                                <th>RANK</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ppd as $line)
                                <tr>
                                    <td>{{$line->created_at->year}}</td>
                                    <td>{{$line->school->province or ""}}</td>
                                    <td>{{$line->school->district or ""}}</td>
                                    <td>{{$line->student->names or ""}}</td>
                                    <td>{{$line->student->gender or ""}}</td>
                                    <td>{{$line->school->school_name or ""}}</td>
                                    <td>{{$line->combination->combination_name or ""}}</td>
                                    <td>{{$line->computedmarks->total_marks or ""}}</td>
                                    <td>{{$line->computedmarks->total_aggregates or ""}}</td>
                                    <td>{{ $line->computedmarks ? calculateRemark($line->computedmarks->total_aggregates) : "" }}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--Datables--}}
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(function () {
            $("#mdtable0").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: false,
                processing: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
            });
            $("#mdtable").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: false,
                processing: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                "order": [[0, 'desc'], [6, 'desc']]
            });
        });
    </script>
@show