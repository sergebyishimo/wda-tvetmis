@extends('wda.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>Search</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h5 class="box-title text-right">Search student</h5>
        </div>
        {!! Form::open(['id'=>'search','route' => 'wda.nationalexams.search', 'method' => 'POST']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Based on PROVINCE</label>
                        <select class="form-control flat" id="nprovince" name="province">
                            <option value="">Choose Here</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Based on DISTRICT</label>
                        <select class="form-control flat" id="ndistrict" name="district">
                            <option value="">Choose Here</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('school_id', "Based on SCHOOL") !!}
                        {!! Form::select('school_id', $all_schools , null , ['class' => 'form-control select2 ', 'placeholder' => 'select school']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('combination_id', "Based on PROGRAM") !!}
                        {!! Form::select('combination_id', $combinations, null, [
                                   'class' => 'form-control select2 class-q',
                                   'placeholder' => 'Choose Program'
                                   ]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('course', "Based on COURSE") !!}
                        {!! Form::select('course_id',[], null, [
                                   'class' => 'form-control select2 class-c',
                                   'placeholder' => 'Choose course'
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('gender', "Based on GENDER") !!}
                        {!! Form::select('gender',[
                            'Male' => 'Male',
                            'Female' => 'Female'
                        ], null, [
                                   'class' => 'form-control select2',
                                   'placeholder' => 'Choose gender'
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('student_index', "Student Index Number") !!}
                        {!! Form::text('student_index', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" name="search" value="search" />
                    </div>
                </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    @if($result)
            <div class="box-body">
                <div style="display: flow;">
                    <label>REPUBLIC OF RWANDA</label><br>
                    <label>MINISTRIY OF EDUCATION</label><br>
                    <label>RWANDA POLYTECHNIC</label><br>
                    <label>WORKFORCE DEVELOPMENT AUTHORITY</label><br>
                    <label>SCHOOL: {{$result[0]->school->school_name or ""}}</label><br>
                    <label>NAMES: {{ $result[0]->student->fname." ".$result[0]->student->lname}}</label><br>
                </div>
                <table class="table table-responsive">
                    <thead>
                    @foreach($result[0]->combination->courses as $course)
                        <th>{{$course->course_name}}</th>
                    @endforeach
                    <th>DECISION</th>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($result as $rs)
                            <td>{{$rs->marks}}</td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
    @endif
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('wda.get.program.courses') }}",
                    {
                        id: op
                    }, function (data) {
                        console.log(data);
                        var h = '<option value="" selected disabled>Select Course</option>';
                        $.each(JSON.parse(data), function (index, course) {
                            h += '<option value="' + course.id + '">' + course.course_name + '</option>';
                        });
                        $(".class-c").html(h);
                    });
            });
        });

    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\WdaNationalExamMarksCreate', '#upload'); !!}
@endsection