@extends('wda.layout.main')

@section('htmlheader_title')
    Uploaded Marks
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Uploaded Marks
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-body" style="overflow-x: auto;">
                        <table class="table table-striped table-bordered" id="dataTableBtnListServer">
                            <thead>
                            <tr>
                                <th>School Name</th>
                                <th>Student Index</th>
                                <th>Option(Program)</th>
                                <th>Course</th>
                                <th>Marks</th>
                                <th>Grade</th>
                                <th>Decision</th>
                                {{--<th></th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--Datables--}}
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(function () {
            $("#dataTableBtnListServer").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: true,
                processing: true,
                ajax: "{{ route('wda.get.data.examination.results') }}",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columns: [
                    {data : 'school_name', name : 'accr_schools_information.school_name', orderable: true, searchable: true},
                    {data : 'student_index', name : 'national_exam_result.student_index', orderable: true, searchable: true},
                    {data : 'combination_name', name : 'combinations.combination_name', orderable: true, searchable: true},
                    {data : 'course_name', name : 'old_courses.course_name', orderable: true, searchable: true},
                    {data : 'marks', name : 'national_exam_result.marks', orderable: true, searchable: true},
                    {data : 'calc_grade', name : 'calc_grade', orderable: false, searchable: false},
                    {data : 'action', name : 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@show