@extends('wda.layout.main')

@section('l-style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>
    @parent
@overwrite

@section('panel-title')
    <span>Uploads National Exam Result</span>
@endsection

@section('panel-body')
    <div class="box">
        <div class="box-header">
            <h5 class="box-title text-right">
                <a href="{{ asset('files/upload_national_exam_marks_format.xlsx') }}" target="_blank">Download Excel Format</a>
            </h5>
        </div>
        {!! Form::open(['id'=>'upload','route' => 'wda.nationalexams.store', 'method' => 'post', 'files' => true]) !!}
        <div class="box-body">
            <input type="hidden" name="acad_year" value="{{ date('Y') }}">
            <input type="hidden" name="term" value="">
            @if(session('uploadErrors'))
                <p class="alert alert-danger">
                    {!! session('uploadErrors') !!}
                    <br>
                    They were given 0 by default. you can correct those marks using the below link!!
                </p>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('acad_year', "Academic Year") !!}
                        {!! Form::text('acad_year', date('Y'), ['class' => 'form-control', 'readonly' => true]) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('combination_id', "Choose Option(Program)") !!}
                        {!! Form::select('combination_id', $combinations->pluck('combination_name','id'), null, [
                                   'class' => 'form-control select2 class-q',
                                   'placeholder' => 'Choose Program'
                                   ]) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('course', "Choose Subject(course)") !!}
                        {!! Form::select('course_id',[], null, [
                                   'class' => 'form-control select2 class-c',
                                   'placeholder' => 'Choose course'
                        ]) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('attachment', "File", ['class' => 'control-label']) !!}
                        {!! Form::file('attachment',['class' => 'filer_docs_input_excel']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-success"><i class="fa fa-tasks"></i> Import Excel
                File
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "order": [[1, 'asc']],
                "columnDefs": [
                    {orderable: false, targets: -1},
                    {orderable: false, targets: 0}
                ]
            });
            $(".class-q").on("change", function (e) {
                var op = $(this).val();
                $("#classQ").attr("checked", true);
                $.get("{{ route('wda.get.program.courses') }}",
                    {
                        id: op
                    }, function (data) {
                        console.log(data);
                        var h = '<option value="" selected disabled>Select Course</option>';
                        $.each(JSON.parse(data), function (index, course) {
                            h += '<option value="' + course.id + '">' + course.course_name + '</option>';
                        });
                        $(".class-c").html(h);
                    });
            });
        });

    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\WdaNationalExamMarksCreate', '#upload'); !!}
@endsection