<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700" rel="stylesheet">
    <title>Certificate</title>
    <style>
        body, p, div, li {
            font-family: 'Montserrat', sans-serif !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <img alt="Image result for logo wda" src="{{ asset('img/wda-logo.png') }}"/>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <p class="text-center" style="font-size: 3em;">
                <strong>Certificate of Accreditation</strong>
            </p>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <p class="text-center" style="font-size: 1.8em;">
                This is to certify that
            </p>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <p class="text-center" style="font-size: 1.5em; font-weight: bold;text-decoration: underline">
                <strong>{{ $school->school_name }}</strong>
            </p>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <p class="text-center" style="font-size: 1.2em;">
                Located in {{ $school->sector }} Sector, {{ $school->district }} District in the {{ $school->province }}
                is accredited and recognized by the Workforce Development Authority; complying with the established
                professional and educational standards and criteria; with the following details
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 centered">
            <ul type="none" style="text-align: center;">
                <li>School Name: <strong>{{ $school->school_name }}</strong></li>
                <li>School Status: <strong>{{ $school->school_status }}</strong></li>
                <li>School Code: <strong>{{ $school->school_code }}</strong></li>
                <li>Type of accreditation: <strong>{{ $school->accreditation_status }}</strong></li>
            </ul>
            <p class="text-center">Accreditation Validity: <strong>{{ getCurrentAcademicYear() }}</strong></p>
            <p class="text-center">List of Accredited trades</p>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr style="background-color: orange;font-weight: bold;">
                    <td><p><strong>Qualification Name</strong></p></td>
                    <td><p><strong>Qualification Code</strong></p></td>
                </tr>
                </thead>
                <tbody>
                @foreach($school->departments as $qualification)
                    <tr>
                        <td>{{ $qualification->qualification->qualification_title or "" }}</td>
                        <td>{{ $qualification->qualification->qualification_code or "" }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-center">WDA {{ $school->school_name }} confirms that meets quality of education
                responsibility to the students and to the
                education profession.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-condensed">
                <tr>
                    <td><p>Signature</p></td>
                    <td><p>Signature</p></td>
                </tr>
                <tr>
                    <td><p>Director of Qualification, Licensing and Accreditation Unit</p></td>
                    <td><p>Director General</p>
                        <p>WDA</p></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<br><br>
<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<script type="text/javascript">
    $(function () {
        window.print();
    });
</script>
</body>
</html>