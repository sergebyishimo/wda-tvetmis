@extends('wda.layout.main')

@section('htmlheader_title')
    Combinations
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Combinations
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if(isset($combination))
                    @php($route = ['wda.combinations.update', $combination->id])
                    @php($id = "updateCombination")
                @else
                    @php($route = 'wda.combinations.store')
                    @php($id = "createCombination")
                @endif
                {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                @if(isset($combination))
                    @method("PATCH")
                    {!! Form::hidden('id', $combination->id) !!}
                @endif
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('combination_name', 'Combination Name *', ['class' => 'label-control']) !!}
                                    {!! Form::text('combination_name', $combination ? $combination->combination_name : null,['class' => 'form-control']) !!}
                                    @if ($errors->has('combination_name'))
                                        <span class="help-block text-red">
                                            {{ $errors->first('combination_name') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('combination_code', 'Combination Code *', ['class' => 'label-control']) !!}
                                    {!! Form::text('combination_code', $combination ? $combination->combination_code : null,['class' => 'form-control']) !!}
                                    @if ($errors->has('combination_code'))
                                        <span class="help-block text-red">
                                            {{ $errors->first('combination_code') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-md btn-primary pull-left">
                            {{ $combination ? "Update Combination" : "Save Combination" }}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($combination)
        {!! JsValidator::formRequest('App\Http\Requests\WdaCombinationUpdate', '#updateCombination'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\WdaCombinationCreate', '#createCombination'); !!}
    @endif
@show