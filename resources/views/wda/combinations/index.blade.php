@extends('wda.layout.main')

@section('htmlheader_title')
    Options(Programs)
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Options(Programs)
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
            <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3> <span class="pull-right"><a
                                    href="{{ route('wda.combinations.create') }}" class="btn btn-primary btn-block">Add Option(Progam)</a></span>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Options(program) Name</th>
                                <th>Options Code</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($combinations as $combination)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>
                                        <a href="{{ route('wda.combinations.index',['o_o'=>$combination->id]) }}">{{ $combination->combination_name }}</a>
                                    </td>
                                    <td>{{ $combination->combination_code }}</td>
                                    <td>
                                        <a href="{{ route('wda.combinations.edit', $combination->id) }}"
                                           class="btn btn-primary btn-block " style="margin-bottom: 5px;">Edit</a>
                                        <form action="{{ route('wda.combinations.destroy', $combination->id) }}"
                                              method="post">
                                            @method("DELETE")
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@show