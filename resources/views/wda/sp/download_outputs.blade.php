
<head>
	<title>Program Indicators PDF</title>
	<style>
		.page-break {
		    page-break-after: always;
		}
	</style>
</head>

<div>
	<h3>Program</h3>  
	<p>{{ $outcome->program->name }}</p>
</div>			
<div>
	<h3>Outcome</h3>
	<p>{{ $outcome->name }}</p>
</div>			

@foreach ($outcome['outputs'] as $output)
	{{-- expr --}}

<div class="page-break">

<div>
	<h3>Output</h3>
	<p>{{ $output->name }}</p>
</div>

<table border="1" cellspacing="0" cellpadding="5">
	<thead style="background: #f1f1f1">
		<tr>
			<th width="10px">#</th>	
			<th style="width: 100px">Indicator</th>
			<th>Baseline {{ date('Y', strtotime($output->outcome->goal->created_at)) }}</th>
			<th>Year / Target / Actual</th>
			<th>Verification means</th>
			<th>Responsible Agency</th>
			<th>Stakeholders</th>
			<th>Source of Funds</th>
			<th>Comment</th>
		</tr>
	</thead>
	<tbody>
		@php
			$i = 1;
		@endphp
		@foreach($output['indicators'] as $indicator)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $indicator->name }}</td>
				<td>{{ $indicator->baseline }}</td>
				<td>
					<table class="table" border="1" cellspacing="0" cellpadding="5" style="width: 100%">
						@foreach ($indicator->plans as $plan)
							<tr>
								<td>{{ $plan->ind_year }}</td>
								<td>{{ $plan->target }}</td>
								<td>{{ $plan->actual }}</td>
							</tr>
							<tr id="details_{{$plan->id}}" style="display: none">
								<td colspan="4">
									<table border="1" cellspacing="0" cellpadding="5" >
										<thead style="font-size: 12px;">
											<tr>
												<th>Quarter 1 Target</th>
												<th>Quarter 1 Actual</th>
												<th>Quarter 2 Target</th>
												<th>Quarter 2 Actual</th>
												<th>Quarter 3 Target</th>
												<th>Quarter 3 Actual</th>
												<th>Quarter 4 Target</th>
												<th>Quarter 4 Actual</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>{{ $plan->quarter_1_target }}</td>
												<td>{{ $plan->quarter_1_actual }}</td>
												<td>{{ $plan->quarter_2_target }}</td>
												<td>{{ $plan->quarter_2_actual }}</td>
												<td>{{ $plan->quarter_3_target }}</td>
												<td>{{ $plan->quarter_3_actual }}</td>
												<td>{{ $plan->quarter_4_target }}</td>
												<td>{{ $plan->quarter_4_actual }}</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						@endforeach
					</table>	
				</td>
				<td>{{ $indicator->verification_means }}</td>
				<td>{{ $indicator->responsible_agency }}</td>
				<td>{{ $indicator->stakeholders }}</td>
				<td>{{ $indicator->source_of_funds }}</td>
				<td>{{ $indicator->comment }}</td>
			</tr>
		@endforeach
		@if(count($output['indicators']) == 0)
			<tr>
				<td colspan="10" class="text-center"> No Indicators Found!!</td>
			</tr>
		@endif
	</tbody>
	</table>
	</div>
@endforeach