@extends('wda.layout.main')

@section('panel-title', "Strategic Plan Programs")

@section('htmlheader_title', "Strategic Plan Programs")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">

            @if (isset($program_info))
                <form method="POST" action="{{ route('wda.sp.programs.index') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="program_id" value="{{ $program_info->id }}">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Program</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label>Reporting Period</label>
                                <p>{{ $program_info->rp->period }}</p>
                            </div>
                            <div class="form-group">
                                <label>Program</label>
                                <input type="text" class="form-control flat" name="u_name"
                                       value="{{ $program_info->name }}" placeholder="Program">
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info btn-flat btn-block"><i
                                        class="fa fa-save"></i>
                                Save Strategic Plan Program
                            </button>
                        </div>
                    </div>
                </form>
            @else
                <div class="box">
                    <div class="box-header">
                        Add New Program
                    </div>
                    <div class="box-body">
                        <form method="POST" id="form" action="{{ route('wda.sp.programs.index') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control flat" name="reporting_period">
                                            @foreach ($rps as $rp)
                                                <option value="{{ $rp->id }}">{{ $rp->period }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="number" class="form-control flat" name="number"
                                               placeholder="Number" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control flat" name="name" placeholder="Program"
                                           required>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success btn-md btn-flat btn-block"><i
                                                class="fa fa-check"></i> Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif

            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th style="width: 50px" class="text-center">#</th>
                    <th>Reporting Period</th>
                    <th style="width: 100px">Nber</th>
                    <th>Program</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i =1;
                @endphp
                @forelse($programs as $program)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $program->rp->period }}</td>
                        <td>{{ $program->number }}</td>
                        <td>{{ $program->name }}</td>
                        <td>


                            <form method="POST" action="{{ route('wda.sp.programs.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_pro" value="{{ $program->id }}">

                                <a href="{{ route('wda.sp.programs.edit', $program->id) }}"
                                   class="btn btn-primary btn-sm btn-flat">
                                    <i class="fa fa-edit"></i> Edit </a>
                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this program : \n\n {{ $program->name }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>

                            </form>

                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No Data Available !</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection