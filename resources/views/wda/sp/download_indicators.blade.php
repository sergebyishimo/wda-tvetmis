
<head>
	<title>Program Indicators PDF</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<style>
		.page-break {
		    page-break-after: always;
		}
	</style>
</head>

<div>
	<h3>Programs</h3>  
	<p>{{ $outcome->programName }}</p>
</div>			
<div>
	<h3>Result</h3>
	<p>{{ $outcome->resultName }}</p>
</div>			

{{--@foreach ($outcome['outputs'] as $output)--}}
	{{-- expr --}}

<div class="page-break">

{{--<div>--}}
	{{--<h3>Output</h3>--}}
	{{--<p>{{ $output->name }}</p>--}}
{{--</div>--}}

<table border="1" cellspacing="0" cellpadding="5" style="width: 100%">
	<thead style="background: #f1f1f1">
		<tr>
			<th>#</th>	
			<th style="width: 100px">Indicator</th>
			<th>Baseline {{ date('Y', strtotime($outcome->created_at)) }}</th>
			<th>Year / Target / Actual</th>
		</tr>
	</thead>
	<tbody>
@php $indicator =$outcome; @endphp
		{{--@foreach($output['indicators'] as $indicator)--}}
			<tr>
				<td>{{ $indicator->result_id }}</td>
				<td>{{ $indicator->indicatorName }}</td>
				<td>{{ $indicator->baseline }}</td>
				<td>
					<table border="1" cellspacing="0" cellpadding="5" style="width: 100%">
						
							<tr>
								<td>{{ $indicator->year_one }}</td>
								<td>{{ $indicator->year_one_target }}</td>
								<td>{{ $indicator->year_one_actual }}</td>
							</tr>
							<tr>
								<td>{{ $indicator->year_two }}</td>
								<td>{{ $indicator->year_two_target }}</td>
								<td>{{ $indicator->year_two_actual }}</td>
							</tr>
							<tr>
								<td>{{ $indicator->year_three }}</td>
								<td>{{ $indicator->year_three_target }}</td>
								<td>{{ $indicator->year_three_actual }}</td>
							</tr>
							<tr>
								<td>{{ $indicator->year_four }}</td>
								<td>{{ $indicator->year_four_target }}</td>
								<td>{{ $indicator->year_four_actual }}</td>
							</tr>
							<tr>
								<td>{{ $indicator->year_five }}</td>
								<td>{{ $indicator->year_five_target }}</td>
								<td>{{ $indicator->year_five_actual }}</td>
							</tr>
							<tr>
								<td>{{ $indicator->year_six }}</td>
								<td>{{ $indicator->year_six_target }}</td>
								<td>{{ $indicator->year_six_actual }}</td>
							</tr>
							<tr>
								<td>{{ $indicator->year_seven }}</td>
								<td>{{ $indicator->year_seven_target }}</td>
								<td>{{ $indicator->year_seven_actual }}</td>
							</tr>
					</table>	
				</td>
			</tr>
		{{--@endforeach--}}
		@if(!$indicator->indicatorName)
			<tr>
				<td colspan="10" class="text-center"> No Indicators Found!!</td>
			</tr>
		@endif
	</tbody>
	</table>

	<canvas id="barChart_{{ $indicator->id }}" style="height:230px"></canvas>

	@php
    	$labels = [];
    	$targets = [];
    	$actuals = [];

    	if(substr($indicator->year_one_target, -1) == "%") {
            $targets[] = substr($indicator->year_one_target, 0, strlen($indicator->year_one_target) - 1);
        } else {
            $targets[] = $indicator->year_one_target;
        }

        if(substr($indicator->year_two_target, -1) == "%") {
            $targets[] = substr($indicator->year_two_target, 0, strlen($indicator->year_two_target) - 1);
        } else {
            $targets[] = $indicator->year_two_target;
        }

        if(substr($indicator->year_three_target, -1) == "%") {
            $targets[] = substr($indicator->year_three_target, 0, strlen($indicator->year_three_target) - 1);
        } else {
            $targets[] = $indicator->year_three_target;
        }

        if(substr($indicator->year_four_target, -1) == "%") {
            $targets[] = substr($indicator->year_four_target, 0, strlen($indicator->year_four_target) - 1);
        } else {
            $targets[] = $indicator->year_four_target;
        }

        if(substr($indicator->year_five_target, -1) == "%") {
            $targets[] = substr($indicator->year_five_target, 0, strlen($indicator->year_five_target) - 1);
        } else {
            $targets[] = $indicator->year_five_target;
        }

        if(substr($indicator->year_six_target, -1) == "%") {
            $targets[] = substr($indicator->year_six_target, 0, strlen($indicator->year_six_target) - 1);
        } else {
            $targets[] = $indicator->year_six_target;
        }

        if(substr($indicator->year_seven_target, -1) == "%") {
            $targets[] = substr($indicator->year_seven_target, 0, strlen($indicator->year_seven_target) - 1);
        } else {
            $targets[] = $indicator->year_seven_target;
        }


        if(substr($indicator->year_one_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_one_actual, 0, strlen($indicator->year_one_actual) - 1);
        } else {
            $actuals[] = $indicator->year_one_actual;
        }


        if(substr($indicator->year_two_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_two_actual, 0, strlen($indicator->year_two_actual) - 1);
        } else {
            $actuals[] = $indicator->year_two_actual;
        }

        if(substr($indicator->year_three_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_three_actual, 0, strlen($indicator->year_three_actual) - 1);
        } else {
            $actuals[] = $indicator->year_three_actual;
        }

        if(substr($indicator->year_four_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_four_actual, 0, strlen($indicator->year_four_actual) - 1);
        } else {
            $actuals[] = $indicator->year_four_actual;
        }

        if(substr($indicator->year_five_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_five_actual, 0, strlen($indicator->year_five_actual) - 1);
        } else {
            $actuals[] = $indicator->year_five_actual;
        }

        if(substr($indicator->year_six_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_six_actual, 0, strlen($indicator->year_six_actual) - 1);
        } else {
            $actuals[] = $indicator->year_six_actual;
        }

        if(substr($indicator->year_seven_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_seven_actual, 0, strlen($indicator->year_seven_actual) - 1);
        } else {
            $actuals[] = $indicator->year_seven_actual;
        }

    @endphp

    <script type="text/javascript">
		//-------------
    //- BAR CHART -
    //-------------

    var areaChartData = {
      labels  : ['Year zero', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5', 'Year 6'],
      datasets: [
        {
			label               : 'Target',
			fillColor           : 'rgba(210, 214, 222, 1)',
			strokeColor         : 'rgba(210, 214, 222, 1)',
			pointColor          : 'rgba(210, 214, 222, 1)',
			pointStrokeColor    : '#c1c7d1',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(220,220,220,1)',
			data                : [{{ implode(', ', $targets) }}],
			backgroundColor		: 'rgba(54, 162, 235, 1)',
        },
        {
			label               : 'Actual',
			fillColor           : 'rgba(60,141,188,0.9)',
			strokeColor         : 'rgba(60,141,188,0.8)',
			pointColor          : '#3b8bba',
			pointStrokeColor    : 'rgba(60,141,188,1)',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(60,141,188,1)',
			data                : [{{ implode(', ', $actuals) }}],
			backgroundColor		: 'rgba(255,99,132,1)',
        }
      ]
    }

    var ctx = document.getElementById("barChart_{{ $indicator->id }}").getContext('2d');

    var myChart = new Chart(ctx, {
            type: 'bar',
            data: areaChartData,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
	</script>

	</div>
{{--@endforeach--}}

<script src="{{ asset('js/adminlte.js') }}"></script>