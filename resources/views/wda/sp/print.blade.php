@extends('layouts.master')

@section('content')

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Strategic Plan Results</h1>
					</div>
					<div class="col-md-12">
						@if (isset($programs))
							<div style="overflow-y: auto;">
								<table class="table table-bordered">
									<tr>

										<th> STRATEGIC PRIORITIES</th>
										<th >OUTCOMES</th>
										<th>INDICATORS</th>
										<th>BASELINE</th>
										<th>Year 1 / Target / Actual</th>
										<th>Year 2 / Target / Actual</th>
										<th>Year 3 / Target / Actual</th>
										<th>Year 4 / Target / Actual</th>
										<th>Year 5 / Target / Actual</th>
										<th>Year 6 / Target / Actual</th>
										<th>Year 7 / Target / Actual</th>

									</tr>
								@foreach ($programs as $program)
										<tr>
											<td>{{$program->programName}}</td>
											<td> {{$program->resultName}}</td>
											<td>{{$program->indicatorName}}</td>
											<td>{{$program->baseline}}</td>
											<td>{{$program->year_one.' / '.$program->year_one_target.' / '.$program->year_one_actual}}</td>
											<td>{{$program->year_two.' / '.$program->year_two_target.' / '.$program->year_two_actual}}</td>
											<td>{{$program->year_three.' / '.$program->year_three_target.' / '.$program->year_three_actual}}</td>
											<td>{{$program->year_four.' / '.$program->year_four_target.' / '.$program->year_four_actual}}</td>
											<td>{{$program->year_five.' / '.$program->year_five_target.' / '.$program->year_five_actual}}</td>
											<td>{{$program->year_six.' / '.$program->year_six_target.' / '.$program->year_six_actual}}</td>
											<td>{{$program->year_seven.' / '.$program->year_seven_target.' / '.$program->year_seven_actual}}</td>
											</td>
										</tr>
									@endforeach
								</table>
							</div>
						@endif

					</div>
				</div>
			</div>
		</section>
	</div>

@endsection