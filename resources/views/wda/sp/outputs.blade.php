@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Strategic Plan Outputs</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	Strategic Plan / Data Input / Outputs</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
							<table style="background: #fff">
								<tr>
									<th style="width: 60px">#</th>
									<th style="width: 400px"> Program</th>
									<th style="width: 490px">Outcome</th>
									<th style="width: 200px">Output</th>
								</tr>
							</table>
							<table class="table" style="background: #fff">
								
								<tbody>

									@foreach ($programs as $program)
										<tr>
											<td></td>
											<td style="width: 400px">{{ $program->name }}</td>
											<td>
												<table class="table">
													@foreach ($program['outcomes'] as $outcome)
														<tr>
															<td style="width: 300px">{{ $outcome->name }}</td>
															
															<td>
																<table class="table">
																	@foreach ($outcome['outputs'] as $output)
																		<tr>
																			<td>{{ $output->name }}</td>
																			<td>
																				<form method="POST">
																					{{ csrf_field() }}
																					<input type="hidden" name="_method" value="delete">
																					<input type="hidden" name="delete_output" value="{{ $output->id }}">
																					
																					<a href="/sp/input/outputs/{{ $output->id }}" class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit </a>

																					<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to remove this program : \n\n {{ $output->name }} ?')"><i class="fa fa-trash"></i> Delete</button>

																				</form>

																			</td>
																		</tr>

																		
																	@endforeach
																</table>
															</td>
														</tr>
													@endforeach
												</table>
											</td>
											
											
										</tr>
									@endforeach

									@if (count($outputs) == 0)
										<tr>
											<td colspan="5" class="text-center"> No Output </td>
										</tr>
									@endif
									
											
								</tbody>
							</table>

						@if (isset($output_info))
							
							<form method="POST" action="/sp/input/outputs">
								{{ csrf_field() }}
								
								<input type="hidden" name="output_id" value="{{ $output_info->id }}">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title"><i class="fa fa-edit"></i> Edit Output</h3>
									</div>
								
									<div class="card-body">
										
										<div class="form-group">
											<label>Program</label>
											<p>{{ $output_info->outcome->program->name }}</p>
										</div>
										<div class="form-group">
											<label>Outcome</label>
											<p>{{ $output_info->outcome->name }}</p>
										</div>
										<div class="form-group">
											<label>Output</label>
											<input type="text" class="form-control flat" name="u_name" value="{{ $output_info->name }}" placeholder="Outome">
										</div>
										
									</div>
									<div class="card-footer">
										<button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i> Save Output</button>
									</div>
								</div>
							</form>
						@endif

						<form method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="form" value="sp_outcome">

							<div class="card card-success">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Outputs</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-success btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" name="program_id" id="sp_program">
											<option value="">Choose Here</option>
											@foreach ($programs as $program)
												<option value="{{ $program->id }}">{{ $program->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Outcome</label>
										<select class="form-control flat" name="outcome_id" id="outcomes">
											
										</select>
									</div>
									<div class="form-group">
										<label>Output</label>
										<input type="text" class="form-control flat" name="name">
									</div>
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i> Save Output</button>
								</div>
							</div>
						</form>

						

					</div>
				</div>
			</div>
		</section>
	</div>

@endsection