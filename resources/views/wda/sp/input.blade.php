@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 26px;padding-left: 5px;"> <i class="fa fa-edit"></i> Strategic Plan Data Input</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	  Strategic Plan / Data Input</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<form method="POST">
								{{ csrf_field() }}
							<input type="hidden" name="form" value="sp_program">
							<div class="card card-info collapsed-card">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Program</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<input type="text" class="form-control flat" name="name" placeholder="Program">
									</div>
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save Strategic Plan Program</button>
								</div>
							</div>
						</form>

						<form method="POST">
							{{ csrf_field() }}

							<input type="hidden" name="form" value="sp_outcome">
						
							<div class="card card-danger collapsed-card">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Outcome</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-danger btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" name="program_id">
											@foreach ($programs as $program)
												<option value="{{ $program->id }}">{{ $program->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Outcome</label>
										<input type="text" class="form-control flat" name="name">
									</div>
									<div class="form-group">
										<label>Means of verification</label>
										<input type="text" class="form-control" name="verification_means" placeholder="Means of verification">
									</div>

									<div class="form-group">
										<label>Assumptions</label>
										<input type="text" class="form-control" name="assumptions" placeholder="Assumptions">
									</div>
									
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-danger btn-flat btn-block"><i class="fa fa-save"></i> Save Strategic Plan Outcome</button>
								</div>
							</div>
						</form>
						<form method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="form" value="sp_indicator">
						
							<div class="card card-success collapsed-card" >
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Indicator</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-success btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" id="sp_program">
											<option value="">Choose Here</option>
											@foreach ($programs as $program)
												<option value="{{ $program->id }}">{{ $program->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Outcome</label>
										<select class="form-control flat" name="outcome_id" id="outcomes">
											
										</select>
									</div>
									<div class="form-group">
										<label>Indicator</label>
										<input type="text" class="form-control" name="name" placeholder="Indicator">
									</div>
									<div class="form-group">
										<label>Baseline</label>
										<input type="text" class="form-control" name="baseline" placeholder="Baseline">
									</div>
									<div class="form-group">
										<label>Year / Target / Actual</label>
										<div class="row">
											<div class="col-md-6" style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
												<div class="row" >
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_one" placeholder="Year 1">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_one_target" placeholder="Target">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_one_actual" placeholder="Actual">		
													</div>	
												</div>
											</div>
											<div class="col-md-6" style="background: #e8e8e8;padding: 5px;border-left: 4px solid #fff">
												<div class="row">
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_two" placeholder="Year 2">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_two_target" placeholder="Target">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_two_actual" placeholder="Actual">		
													</div>	
												</div>
											</div>
										</div>
										
									</div>
									<div class="form-group">
										<label>Year / Target / Actual</label>
										<div class="row">
											<div class="col-md-6" style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
												<div class="row" >
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_three" placeholder="Year 3">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_three_target" placeholder="Target">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_three_actual" placeholder="Actual">		
													</div>	
												</div>
											</div>
											<div class="col-md-6" style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
												<div class="row">
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_four" placeholder="Year 4">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_four_target" placeholder="Target">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_four_actual" placeholder="Actual">		
													</div>	
												</div>
											</div>
										</div>
										
									</div>
									<div class="form-group">
										<label>Year / Target / Actual</label>
										<div class="row">
											<div class="col-md-6" style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
												<div class="row" >
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_five" placeholder="Year 5">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_five_target" placeholder="Target">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_five_actual" placeholder="Actual">		
													</div>	
												</div>
											</div>
											<div class="col-md-6" style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
												<div class="row">
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_six" placeholder="Year 6">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_six_target" placeholder="Target">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_six_actual" placeholder="Actual">		
													</div>	
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Year / Target / Actual</label>
										<div class="row">
											<div class="col-md-6" style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
												<div class="row" >
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_seven" placeholder="Year 7">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_seven_target" placeholder="Target">		
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="year_seven_actual" placeholder="Actual">		
													</div>	
												</div>
											</div>
										</div>
									</div>
									
									
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i> Save Strategic Plan Indicator</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>

	</div>
@endsection