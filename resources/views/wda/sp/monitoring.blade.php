@extends('wda.layout.main')

@section('panel-title', "Monitoring Overview")

@section('htmlheader_title', "Monitoring Overview")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="info-box mb-3 bg-warning-gradient">
                <span class="info-box-icon"><i class="fa fa-check"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><b>Welcome</b></span>
                    @foreach ($welcomes as $welcome)
                        <p>{!! $welcome->message !!}</p>
                    @endforeach
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box mb-3 bg-primary">
                <span class="info-box-icon"><i class="fa fa-bell"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><b>Notifcations</b></span>
                    @foreach ($notifications as $notification)
                        <p>* {{ $notification->message }}</p>
                    @endforeach
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>

    </div>
    <br><br>
    <div class="row">

        <div class="col-md-4 col-sm-6 col-12">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Strategic Plan</span>
                    <span class="info-box-number">{{ $srps }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('wda.sp.report.index') }}">View Strategic Plan</a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-12">
            <div class="info-box bg-success">
                <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Action Plan</span>
                    <span class="info-box-number">{{ $rps }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
					    <a href="{{ route('wda.ap.report.index') }}">View Action Plan</a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-12">
            <div class="info-box bg-warning">
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Project</span>
                    <span class="info-box-number">{{ $projects }}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('wda.pr.report.index') }}">View Projects</a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
@endsection