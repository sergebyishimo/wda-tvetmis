@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 26px;padding-left: 5px;"> <i class="fa fa-pie-chart"></i> Strategic Plan Charts</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/">Home</a></li>
							<li class="breadcrumb-item active">Stategic Plan / Charts</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
					
						<div class="card card-success">
							<div class="card-header">
								<h3 class="card-title">View Outcome Charts</h3>
							</div>
							<div class="card-body">
								<div class="form-group">
									<label><i class="fa fa-check-circle"></i> Program</label>
									<p>{{ $outcome->program->name }}</p>
								</div>
								<div class="form-group">
									<label><i class="fa fa-check-circle"></i> Outcome</label>
									<p>{{ $outcome->name }}</p>
								</div>
								<div class="form-group">
									<label>Indicators Charts</label>
									{{ count($outcome['indicators']) }}
								</div>

								@foreach($outcome['indicators'] as $indicator)
									<br>
									<div class="form-group">
										<label><i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp; {{ $indicator->name }}</label>
									</div>

									<canvas id="barChart_{{ $indicator->id }}" style="height:230px"></canvas>
									{{ var_dump($indicator['year_one']) }}
									<script type="text/javascript">
										//-------------
									    //- BAR CHART -
									    //-------------

									    @php
									    	$labels = [];
									    	$targets = [];
									    	$actuals = [];

									    	if ($indicator['year_one']) {
									    		$labels[] = $indicator['year_one'];
									    		$target[] = $indicator['year_one_target'];
									    		$actual[] = $indicator['year_one_actual'];
									    	}

									    	if ($indicator['year_two']) {
									    		$labels[] = $indicator['year_two'];
									    		$target[] = $indicator['year_two_target'];
									    		$actual[] = $indicator['year_two_actual'];
									    	}

									    	
									    @endphp


									    var areaChartData = {
									      labels  : [{{ json_encode($labels) }}],
									      datasets: [
									        {
												label               : 'Target',
												fillColor           : 'rgba(210, 214, 222, 1)',
												strokeColor         : 'rgba(210, 214, 222, 1)',
												pointColor          : 'rgba(210, 214, 222, 1)',
												pointStrokeColor    : '#c1c7d1',
												pointHighlightFill  : '#fff',
												pointHighlightStroke: 'rgba(220,220,220,1)',
												data                : [{{ implode(', ', $targets)}}],
												backgroundColor		: 'rgba(54, 162, 235, 1)',
									        },
									        {
												label               : 'Actual',
												fillColor           : 'rgba(60,141,188,0.9)',
												strokeColor         : 'rgba(60,141,188,0.8)',
												pointColor          : '#3b8bba',
												pointStrokeColor    : 'rgba(60,141,188,1)',
												pointHighlightFill  : '#fff',
												pointHighlightStroke: 'rgba(60,141,188,1)',
												data                : [{{ implode(', ', $actuals)}}],
												backgroundColor		: 'rgba(255,99,132,1)',
									        }
									      ]
									    }

									    var ctx = document.getElementById("barChart_{{ $indicator->id }}").getContext('2d');

									    var myChart = new Chart(ctx, {
									            type: 'bar',
									            data: areaChartData,
									            options: {
									                scales: {
									                    yAxes: [{
									                        ticks: {
									                            beginAtZero:true
									                        }
									                    }]
									                }
									            }
									        });
										</script>
								@endforeach
							</div>
							<div class="card-footer">
								
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
	</div>
@endsection