@extends('wda.layout.main')

@section('panel-title', "Searching")

@section('htmlheader_title', "Searching")

@section('l-style')
    @parent
@endsection

@section('panel-body')
    <div class="row">
        <form action=""
              method="post">
            @csrf
            <div class="col-md-12">
                <div class="box-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Based on Schema</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="model" class="control-label">Model</label>
                                        <select name="model"
                                                class="form-control"
                                                style="width: 100%;" id="model">
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach (getSearchModel() as $key => $models)
                                                <option value="{{ $key }}">{{ $models }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="related" class="control-label">Qualification</label>
                                        <select name="qualification" class="form-control" id="qualification" style="width: 100%;">
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach ($qualifications as $qualification)
                                                @if($qualification->uuid)
                                                <option value="{{ $qualification->uuid }}"
                                                        @if( request()->has('qualification') && request()->get('qualification') == $qualification->uuid) selected @endif >{{ $qualification->qualification_title }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="column" class="control-label">Columns</label>
                                        <select name="column" class="form-control" id="column" style="width: 100%;">
                                            <option value="" disabled selected>Choose Here</option>
                                            @if(request()->has('column'))
                                                <option value="{{ request()->get('column') }}"
                                                        selected>{{ ucwords(request()->get('column')) }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="condition" class="control-label">Condition</label>
                                        <select name="condition" class="form-control" id="condition"
                                                style="width: 100%;" required>
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach (getConditionSearch() as $key => $value)
                                                <option value="{{ $key }}"
                                                        @if( request()->has('condition') && request()->get('condition') == $key) selected @endif >{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="value" class="control-label">Value</label>
                                        <select name="value" class="form-control" id="value"
                                                style="width: 100%;">
                                            <option value="" disabled selected>Choose Here</option>
                                            @if(request()->has('value'))
                                                <option value="{{ request()->get('value') }}"
                                                        selected>{{ ucwords(request()->get('value')) }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 cols-display icheck">
                                    <div class="row pl-1">
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <button type="submit" class="btn btn-info pull-left">Yes, Search</button>
                                    <a href="{{ route('wda.search.index') }}"
                                       class="btn btn-warning pull-right">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @if(!is_null($results))
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include($view)
                </div>
            </div>
        </div>
    @endif
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $("#dataTableSearchBtn").dataTable({
            dom: 'Blfrtip',
            pager: true,
            "ordering": false,
            // "scrollX": true,
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed two-column'
                }
            ],
            columnDefs: [
                {
                    targets: [],
                    visible: false,
                }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var select = $('<select><option value="">Select ...</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });
    </script>
    <script type="text/javascript">
        $("#model").change(function () {
            let m = $(this).val();
            let url = "{{ url('/wda/search/box') }}/" + m;
            let p = $("#column");
            let d = $(".cols-display .row");
            let r = $("#related");
            $.get(url, {}, function (data) {
                let obj = jQuery.parseJSON(data);
                let html = "<option value='' selected disabled>Select Columns</option>";
                let htmlR = "<option value='' selected disabled>Select Related</option>";
                let ckc = "";
                $.each(obj, function (k, dd) {
                    if (k == 'table') {
                        $.each(dd, function (k, v) {
                            let vv = v;
                            v = ucwords(v.replace("_", " "));
                            html += "<option value='" + vv + "'>" + v + "</option>";
                            if (vv == 'school_type')
                                vv = "school_type_name";
                            ckc += "<div class='col-md-2 checkbox'>" +
                                "<label class='label text-dark' for='" + vv + "'>" +
                                "<input type='checkbox' id='" + vv + "' name='colms[]' value='" + vv + "'> " + v + "</label>" +
                                "</div>";
                        });
                    }
                    if (k == 'related') {
                        $.each(dd, function (kk, vv) {
                            kk = ucwords(kk.replace("_", " "));
                            htmlR += "<option value='" + vv + "'>" + kk + "</option>";
                        });
                    }
                });
                r.html(htmlR);
                p.html(html);
                d.html(ckc);
            });
        });
        $("#column").change(function () {
            let m = $("#model").val();
            let c = $(this).val();
            let url = "{{ url('/wda/search/box/') }}/" + m + "/" + c + "/";
            let p = $("#value");
            $.get(url, {}, function (data) {
                let obj = jQuery.parseJSON(data);
                let html = "<option value='' selected disabled>Select Value</option>";
                $.each(obj, function (k, v) {
                    html += "<option value='" + v + "'>" + ucwords(v) + "</option>";
                });
                p.html(html);
            });
        });

        function ucwords(str) {
            return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                return $1.toUpperCase();
            });
        }

        $(function () {
            $('.icheck input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
@endsection
