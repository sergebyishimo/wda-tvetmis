<div class="box-header with-border">
    Results Fund: <b>{{ $results->count() }}</b>
</div>
<div class="box-body" style="overflow-x: auto;">
    <table class="table table-responsive table-bordered" id="dataTableSearchBtn">
        <thead>
        <tr>
            <th width="20px" class="text-center">#</th>
            @forelse($colms as $colm)
                <th>{{ ucwords(str_replace('_', ' ', $colm)) }}</th>
            @empty
                <th>School Name</th>
                <th>Province</th>
                <th>District</th>
                <th>Sector</th>
            @endforelse
        </tr>
        </thead>
        <tbody>
        @php
            $x=1;
            $c=0;
        @endphp
        @forelse($results as $result)
            <tr>
                <td class="text-center">{{ $x++ }}</td>
                @forelse($colms as $colm)
                    <td>{{ $result->{$colm} or "" }}</td>
                @empty
                    <td>{{ $result->school_name }}</td>
                    <td>{{ $result->province }}</td>
                    <td>{{ $result->district }}</td>
                    <td>{{ $result->sector }}</td>
                @endforelse
            </tr>
        @empty
            <tr>
                <td colspan="6">No results find ...</td>
            </tr>
        @endforelse
        </tbody>
        <tfoot>
        <tr>
            <th width="20px" class="text-center">#</th>
            @forelse($colms as $colm)
                <th>{{ ucwords(str_replace('_', ' ', $colm)) }}</th>
            @empty
                <th>School Name</th>
                <th>Province</th>
                <th>District</th>
                <th>Sector</th>
            @endforelse
        </tr>
        </tfoot>
    </table>
</div>