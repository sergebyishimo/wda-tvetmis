@extends('wda.layout.main')

@section('panel-title', "Searching With Filters")

@section('htmlheader_title', "Searching")

@section('l-style')
    @parent
@endsection

@section('panel-body')
    <div class="row">
        <form action="{{url('/wda/school')}}"
              method="get">

            <div class="col-md-12">
                <div class="box-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Filters</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Province</label>
                                        <select class="form-control flat" id="fprovince" name="province">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>District</label>
                                        <select class="form-control flat" id="fdistrict" name="district">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Sector:</label>
                                        <select class="form-control flat" id="fsector" name="sector">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="">School Type:</label>
                                        <select class="form-control flat"  id="fcell" name="schooltype">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Accreditetion:</label>
                                        <select class="form-control flat" id="accreditation" name="accreditation">
                                            <option value="">Choose Here</option>
                                            <option value="Accredited">Accredited</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 mt-3">
                                            <button type="submit" class="btn btn-info pull-left">Yes, Search</button>
                                            <a href="{{url('/wda/filter')}}" class="btn btn-warning pull-right">Reset</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @if(!is_null($results))
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include($view)
                </div>
            </div>
        </div>
    @endif
@endsection
