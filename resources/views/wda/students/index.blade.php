@extends('wda.layout.main')
@section('htmlheader_title')
    Students Managements
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Student managements
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-header">
                        {!! Form::open(['id'=>'search','route' => 'wda.studentmanagement.search', 'method' => 'POST']) !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Based on PROVINCE</label>
                                        <select class="form-control flat" id="nprovince" name="province">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Based on DISTRICT</label>
                                        <select class="form-control flat" id="ndistrict" name="district">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('school_id', "Based on SCHOOL") !!}
                                        {!! Form::select('school_id', $all_schools , null , ['class' => 'form-control select2 ', 'placeholder' => 'select school']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('combination_id', "Based on Qualification") !!}
                                        {!! Form::select('combination_id', $qualifications, null, [
                                                   'class' => 'form-control select2 class-q',
                                                   'placeholder' => 'Choose Program'
                                                   ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('course', "Based on COURSE") !!}
                                        {!! Form::select('course_id',[], null, [
                                                   'class' => 'form-control select2 class-c',
                                                   'placeholder' => 'Choose course'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('gender', "Based on GENDER") !!}
                                        {!! Form::select('gender',[
                                            'Male' => 'Male',
                                            'Female' => 'Female'
                                        ], null, [
                                                   'class' => 'form-control select2',
                                                   'placeholder' => 'Choose gender'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('acad_year', "Academic Year") !!}
                                        {!! Form::select('acad_year',academicYear(null,null,true), null, [
                                                   'class' => 'form-control select2',
                                                   'placeholder' => 'Choose Academic Year'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                @if(!$search)
                    <div class="box-body" style="overflow-x: auto;">
                        <table class="table table-bordered table-responsive table-condensed table-hover"
                               style="table-border-color-dark: #0c0c0c;border-width: thin;" id="mystudenttable">
                            <thead>
                            <tr>
                                <th width="120px" class="text-center">Photo</th>
                                <th width="120px" class="text-center">Student number</th>
                                <th width="120px" class="text-center">Student names</th>
                                <th width="120px" class="text-center">Sex</th>
                                <th width="120px" class="text-center">Qualification</th>
                                <th width="120px" class="text-center">Level</th>
                                <th width="120px" class="text-center">School</th>
                                <th width="120px" class="text-center">Province</th>
                                <th width="120px" class="text-center">District</th>
                                <th width="120px" class="text-center">Academic year</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="box-body" style="overflow-x: auto;">
                        <table class="table table-bordered table-responsive table-condensed table-hover"
                               style="table-border-color-dark: #0c0c0c;border-width: thin;" id="mydt">
                            <thead>
                            <tr>
                                <th width="120px" class="text-center">Photo</th>
                                <th width="120px" class="text-center">Student number</th>
                                <th width="120px" class="text-center">Student names</th>
                                <th width="120px" class="text-center">Sex</th>
                                <th width="120px" class="text-center">Qualification</th>
                                <th width="120px" class="text-center">Level</th>
                                <th width="120px" class="text-center">School</th>
                                <th width="120px" class="text-center">Province</th>
                                <th width="120px" class="text-center">District</th>
                                <th width="120px" class="text-center">Academic year</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td class="text-center">
                                    </td>
                                    <td>{{ $student->reg_no or ""}}</td>
                                    <td>{{ $student->names or ""}}</td>
                                    <td>{{ $student->gender or "" }}</td>
                                    <td>{{ $student->level->department->qualification->qualification_title or ""  }}</td>
                                    <td>{{ $student->level->rtqf->level_name or ""}}</td>
                                    <td>{{$student->mschool->school_name or ""}}</td>
                                    <td>{{$student->province  or ""}}</td>
                                    <td>{{$student->district or ""}}</td>
                                    <td>{{$student->acad_year or ""}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
            @endif
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--Datables--}}
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(function () {
            let url = "{!! $url !!}";
            $("#mystudenttable").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: true,
                processing: true,
                ajax: url,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columns: [
                    {
                        data: 'photo',
                        name: 'photo',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'reg_no',
                        name: 'students.reg_no',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'names',
                        name: 'names',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'gender',
                        name: 'students.gender',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'qualification_title',
                        name: 'qualifications.qualification_title',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'level_name',
                        name: 'rtqfs.level_name',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'school_name',
                        name: 'accr_schools_information.school_name',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'province',
                        name: 'accr_schools_information.province',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'district',
                        name: 'accr_schools_information.district',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'acad_year',
                        name: 'students.acad_year',
                        orderable: false,
                        searchable: false
                    }
                ]
            });


            $("#mydt").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: false,
                processing: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
            });
        });
    </script>
@show