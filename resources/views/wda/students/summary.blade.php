@extends('wda.layout.main')
@section('htmlheader_title')
    Students Summary
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Students Summary
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row">
            <form method="GET" action="{{ route('wda.studentmanagement.summary') }}">
                <div class="col-md-2 col-md-offset-4">
                    <div class="form-group">
                        <label for="">Acad. Year</label>
                        <select name="chart_acad_year" id="" class="form-control">
                            @for ($i = 2018; $i >= 2016; $i--)
                                <option @if($chart_acad_year == $i) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <br>
                        <button type="submit" class="btn btn-primary">View</button>
                    </div>
                </div>
            </form>
            <div class="col-md-12">
                <h4>Students per Province per Gender</h4>

                <canvas id="chartjs-0"></canvas>
            </div>
            <div class="col-md-12">
                <h4>Students per RTQF per Gender</h4>
                <canvas id="chartjs-1"></canvas>
            </div>
            <div class="col-md-12">
                <h4>Students per Sector per Gender</h4>
                <canvas id="chartjs-2"></canvas>
            </div>
            <br clear="left"><br>
            <div class="col-md-12">
                <div class="row">
                    <form method="GET" action="{{ route('wda.studentmanagement.summary') }}">
                        <div class="col-md-2 col-md-offset-4">
                            <div class="form-group">
                                <label for="">Acad. Year</label>
                                <select name="acad_year" id="" class="form-control">
                                    @for ($i = 2018; $i >= 2016; $i--)
                                        <option @if($acad_year == $i) selected @endif>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">View</button>
                            </div>
                        </div>
                    </form>
                </div>

                <table class="table table-striped table-hover" id="schoolsTable">
                    <thead>
                        <tr>
                            <th>School</th>
                            <th>Status</th>
                            <th>Type</th>
                            <th>Province</th>
                            <th>District</th>
                            <th>Total</th>
                            <th>Male</th>
                            <th>Female</th>
                            <th>Level 1</th>
                            <th>Level 2</th>
                            <th>Level 3</th>
                            <th>Level 4</th>
                            <th>Level 5</th>
                            <th>Level 6</th>
                            <th>Level 7</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($schools as $school)
                            <tr>
                                <th>{{ $school->school_name }}</th>
                                <th>{{ $school->school_status }}</th>
                                <th>{{ $school->schooltype->school_type }}</th>
                                <th>{{ $school->province }}</th>
                                <th>{{ $school->district }}</th>
                                <th>{{ $school->students()->count() }}</th>
                                <th>{{ $school->students()->where('gender', 'Male')->count() }}</th>
                                <th>{{ $school->students()->where('gender', 'Female')->count() }}</th>
                                <th>{{ $school->students()->join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.uuid', 'levels.rtqf_id')->where('accr_rtqf_source.qualification_type', 'Level 1')->where('students.acad_year', $acad_year)->count() }}</th>
                                <th>{{ $school->students()->join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.uuid', 'levels.rtqf_id')->where('accr_rtqf_source.qualification_type', 'Level 2')->where('students.acad_year', $acad_year)->count() }}</th>
                                <th>{{ $school->students()->join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.uuid', 'levels.rtqf_id')->where('accr_rtqf_source.qualification_type', 'Level 3')->where('students.acad_year', $acad_year)->count() }}</th>
                                <th>{{ $school->students()->join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.uuid', 'levels.rtqf_id')->where('accr_rtqf_source.qualification_type', 'Level 4')->where('students.acad_year', $acad_year)->count() }}</th>
                                <th>{{ $school->students()->join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.uuid', 'levels.rtqf_id')->where('accr_rtqf_source.qualification_type', 'Level 5')->where('students.acad_year', $acad_year)->count() }}</th>
                                <th>{{ $school->students()->join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.uuid', 'levels.rtqf_id')->where('accr_rtqf_source.qualification_type', 'Level 6')->where('students.acad_year', $acad_year)->count() }}</th>
                                <th>{{ $school->students()->join('levels', 'levels.id', 'students.level_id')->join('accr_rtqf_source', 'accr_rtqf_source.uuid', 'levels.rtqf_id')->where('accr_rtqf_source.qualification_type', 'Level 7')->where('students.acad_year', $acad_year)->count() }}</th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section("l-scripts")
    @parent
    @include('jsview')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
        var maleStudents = {
            label: 'Male Students',
            data: provinces_males,
            backgroundColor: 'rgba(99, 132, 0, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-male"
        };

        var femaleStudents = {
            label: 'Female Students',
            data: provinces_females,
            backgroundColor: 'rgba(0, 99, 132, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-female"
        };

        var data = {
            labels: provinces,
            datasets: [maleStudents, femaleStudents]
        };

        var chartOptions = {
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6
                }],
                yAxes: [{
                    id: "y-axis-male"
                }, {
                    id: "y-axis-female"
                }]
            },
            events: false,
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 0
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        };

        var barChart = new Chart(document.getElementById("chartjs-0"), {
            type: 'bar',
            data: data,
            options: chartOptions
        });

        maleStudents = {
            label: 'Male Students',
            data: levels_males,
            backgroundColor: 'rgba(99, 132, 0, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-levels_males"
        };

        femaleStudents = {
            label: 'Female Students',
            data: levels_females,
            backgroundColor: 'rgba(0, 99, 132, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-levels_females"
        };

        data = {
            labels: levels,
            datasets: [maleStudents, femaleStudents]
        };

        chartOptions = {
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6
                }],
                yAxes: [{
                    id: "y-axis-levels_males"
                }, {
                    id: "y-axis-levels_females"
                }]
            },
            events: false,
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 0
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        };

        barChart = new Chart(document.getElementById("chartjs-1"), {
            type: 'bar',
            data: data,
            options: chartOptions
        });


        maleStudents = {
            label: 'Male Students',
            data: sectors_males,
            backgroundColor: 'rgba(99, 132, 0, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-sectors_males"
        };

        femaleStudents = {
            label: 'Female Students',
            data: sectors_females,
            backgroundColor: 'rgba(0, 99, 132, 0.6)',
            borderWidth: 0,
            yAxisID: "y-axis-sectors_females"
        };

        data = {
            labels: sectors,
            datasets: [maleStudents, femaleStudents]
        };

        chartOptions = {
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6
                }],
                yAxes: [{
                    id: "y-axis-sectors_males"
                }, {
                    id: "y-axis-sectors_females"
                }]
            },
            events: false,
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 0
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        };

        barChart = new Chart(document.getElementById("chartjs-2"), {
            type: 'bar',
            data: data,
            options: chartOptions
        });

        // new Chart(document.getElementById("chartjs-0"),
        //     {"type":"bar","data":
        //             {
        //                 "labels":provinces,
        //                 "datasets":[
        //                     {"label":"Students per province","data":statistics,
        //                         "fill":false,
        //                         "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
        //                         "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
        //             },
        //         "options":
        //             {"scales":
        //                     {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});
    </script>
    <script>
        $("#schoolsTable").DataTable({
            dom: 'Blfrtip',
            "lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
            "responsive": true,
            "scrollX": true,
            buttons: [
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed two-column',
                    postfixButtons: ['colvisRestore']
                },
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
        });
    </script>

@endsection