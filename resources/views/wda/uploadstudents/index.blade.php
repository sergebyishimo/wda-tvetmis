@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        Mass Upload Students
    </div>
@endsection
@section('htmlheader_title', "Mass Upload Students")
@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
          integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
            @include('feedback.feedback')

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ asset('files/mass_upload_students_by_school.xlsx') }}" target="_blank">Download
                            Excel
                            Format</a>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['id' => 'upload', 'route' => 'wda.uploadstudents.store', 'files' => true]) !!}
                        @method('POST')
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('school_id', "School Name *", ['class' => 'control-label']) !!}
                                    {!! Form::select('school_id', $schools->pluck('school_name','id'), null, [
                                    'class' => 'form-control department select2 class-sc',
                                    'id' => 'aDepartment',
                                    'placeholder' => 'Choose School Name'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-md-9">
                                {{--<div class="form-group">--}}
                                    {{--{!! Form::label('department_id', "Department", ['class' => 'control-label']) !!}--}}
                                    {{--{!! Form::select('department_id', [], null, [--}}
                                  {{--'class' => 'form-control department select2 class-q',--}}
                                  {{--'id' => 'aDepartment',--}}
                                  {{--'placeholder' => 'Choose Department Name'--}}
                                  {{--]) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2">--}}
                                {{--<div class="form-group">--}}
                                    {{--{!! Form::label('level_id', "Level *") !!}--}}
                                    {{--{!! Form::select('level_id', [], null,--}}
                                    {{--['class' => 'form-control select2', 'placeholder' => 'Choose class']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('academic_year', "Academic Year *") !!}
                                    {!! Form::select('academic_year', academicYear(null, null, true), null,
                                    ['class' => 'form-control select2', 'placeholder' => 'Choose year']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('attachment', "File", ['class' => 'control-label']) !!}
                                    {!! Form::file('attachment',['class' => 'filer_docs_input_excel']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Upload',['class' => 'btn btn-md btn-primary pull-right']) !!}
                                </div>
                            </div>

                        </div>
                        <div class="row">

                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@include('wda.layout.modals.warnings')
@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
            $(".class-sc").on("change", function () {
                var dep = $(this).val();
                var school_id = $('.class-sc').val();
                console.log('school_id =>' + school_id);
                if (school_id.length > 0) {
                    $.get("{{ route('wda.get.dept') }}",
                        {
                            school_id: school_id,
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, dept) {
                                h += '<option value="' + dept.id + '">' + dept.name + '</option>';
                                console.log(dept.name);
                            });
                            // console.log(data);
                            // console.log(JSON.parse(data));
                            $(".class-q").html(h);
                        });
                }
            });

            $(".class-q").on("change", function () {
                var dep = $(this).val();
                var school_id = $('.class-sc').val();
                console.log('school_id =>' + school_id);
                console.log('dep Id =>' + dep);
                if (dep.length > 0) {
                    $.get("{{ route('wda.get.levels') }}",
                        {
                            id: dep,
                            where: 'id',
                            school_id: school_id,
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });
        });
    </script>
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: xlsx");
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\WdaMassUploadStudentRequest', '#upload'); !!}
@show