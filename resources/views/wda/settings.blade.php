@extends('wda.layout.main')

@section('panel-title', "General Settings")

@section('htmlheader_title', "General Settings")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('wda.settings.store') }}">
                {{ csrf_field() }}
                <input type="hidden" name="form" value="welcome">
                <input type="hidden" name="welcome_id" value="{{ $welcome->id or "" }}">
                <div class="box box-primary box-outline">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-edit"></i> Edit Welcome Message </h3>
                    </div>
                    <div class="box-body pad">
                        <div class="mb-3">
				                <textarea class="textarea" name="message" placeholder="Place some text here"
                                          style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                          required>{{ $welcome->message or "" }}</textarea>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Save
                            Welcome Message
                        </button>
                    </div>
                </div>
            </form>


            <div class="box box-primary box-outline">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> Edit Notifications</h3>
                </div>
                <div class="box-body pad">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 60px;">#</th>
                            <th>Notification Message</th>
                            <th>Last Update</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @forelse ($notifications as $notification)
                            <tr>
                                <td class="text-center">{{ $i++ }}</td>
                                <td>{{ $notification->message }}</td>
                                <td>{{ date('d/m/Y', strtotime($notification->updated_at)) }}</td>
                                <td>
                                    <form method="POST" action="{{ route('wda.settings.destroy') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">

                                        <input type="hidden" name="form" value="notification">
                                        <input type="hidden" name="delete_noti" value="{{ $notification->id }}">

                                        <a href="{{ route('wda.settings.edit', $notification->id) }}"
                                           class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit </a>

                                        <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                                onclick="return window.confirm('Are you sure you want to remove this notification :\n{{ $notification->message }} ? ')">
                                            <i class="fa fa-trash "></i> Delete
                                        </button>

                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">
                                    No Data Available !
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <form method="POST" action="{{ route('wda.settings.store') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="form" value="notification">
                        <br clear="left"><br>

                        @if (isset($notification_info))
                            <input type="hidden" name="noti_id" value="{{ $notification_info->id }}">
                            <div class="form-group">
                                <label style="color: green"><i class="fa fa-edit"></i> Edit Notification</label>
                                <textarea class="form-control flat" name="u_message" placeholder="Notification Message"
                                          required>{{ $notification_info->message }}</textarea>
                            </div>
                        @else
                            <div class="form-group">
                                <label><i class="fa fa-bell"></i> Add Notification</label>
                                <textarea class="form-control flat" name="message" placeholder="Notification Message"
                                          required></textarea>
                            </div>
                        @endif
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i>
                                Save
                                Notification
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection