@extends('wda.layout.main')

@section('panel-title')
    Assign Qualification to <u><i>{{ $school->school_name }}</i></u>
@endsection

@section('htmlheader_title', "Assign Qualification to ".$school->school_name)

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-md-2">
                <a class="btn btn-primary btn-md" href="{{ route('wda.schools') }}">
                    <i class="fa fa-list"></i>&nbsp;<span>Return to list</span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('wda.school.assign.qualification') }}"
                      class="form"
                      role="form"
                      method="get">
                    {!! Form::hidden('o_o', $school->id) !!}
                    {!! Form::hidden('inside', true) !!}
                    <table class="table table-bordered">
                        <tr>
                            <td width="100px" class="bg-gray-active">
                                <label for="sector" class="control-label">Sector</label>
                            </td>
                            <td class="bg-gray-light">
                                <div class="form-group">
                                    <select name="r_i" data-url="{{ url('wda/get/trades') }}"
                                            class="form-control select2"
                                            style="width: 100%;"
                                            id="sector">
                                        <option value="" disabled selected>Choose Here</option>
                                        @isset($sectors)
                                            @foreach ($sectors as $ssector)
                                                <option value="{{ $ssector->id }}"
                                                        {{ getUUIDFrom('se', $ssector->id) == $sector->id ?  "selected" : "" }} >{{ $ssector->tvet_field }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </td>
                            <td width="100px" class="bg-gray-active">
                                <label for="trades" class="control-label">Sub Sector</label>
                            </td>
                            <td class="bg-gray-light">
                                <div class="form-group">
                                    <select name="d_d" class="form-control select2" id="trades"
                                            style="width: 100%;" required>
                                        <option value="" disabled selected>Choose Here</option>
                                        @isset($sub_sectors)
                                            @foreach ($sub_sectors as $subtrade)
                                                <option value="{{ $subtrade->id }}"
                                                        @if( $subsector->id == $subtrade->uuid) selected @endif >{{  $subtrade->sub_field_name }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </td>
                            <td class="bg-gray-light">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-search"></i>&nbsp;&nbsp;
                                    <span>Search</span>
                                </button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="box">
            {!! Form::open(['route' => 'wda.school.save.department']) !!}
            {!! Form::hidden('school', $school->id) !!}
            <div class="box-body">
                <table class="table table-bordered table-condensed table-responsive">
                    <thead align="center" class="bg-gray-light">
                    <th>Qualifications <sup>(Departments)</sup></th>
                    <th>RTQF <sup>Class</sup></th>
                    </thead>
                    <tbody>
                    @if($qualifications->count() > 0)
                        @foreach($qualifications as $department)
                            <tr>
                                <td class="bg-white" width="60%">
                                    <ul class="list-group">
                                        <li class="list-group-item {{ hasQualification($school->id, $department->id) ? 'list-group-item-success' : 'list-group-item-default' }} p-1 pl-3">
                                            <div>
                                                <div class="checkbox icheck">
                                                    <label>
                                                        <input style="display:none;" type="checkbox"
                                                               class=""
                                                               @if(hasQualification($school->id, $department->id)) checked
                                                               @endif
                                                               name="qualification[]" value="{{ $department->id }}">&nbsp;
                                                        <b>{{ $department->qualification_title }}</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </td>
                                <td class="bg-white">
                                    <ul class="row">
                                        @if($department->rtqfs->count() > 0)
                                            @foreach($department->rtqfs as $rtqf)
                                                {{--<div class="row p-1 pl-3">--}}
                                                {{--<div class="row">--}}
                                                <div class="col-md-4 mr-1 mb-1 {{ hasRqtf($school->id, getDepartmentFrom($school->id, $department->id, 'id'), $rtqf->id) ? 'bg-success' : '' }}"
                                                     style="box-shadow:inset 0px 0px 3px #e2e3e5;">
                                                    <div class="checkbox icheck">
                                                        <label>
                                                            <input style="display:none;" type="checkbox"
                                                                   class=""
                                                                   @if(hasRqtf($school->id, getDepartmentFrom($school->id, $department->id, 'id'), $rtqf->id)) checked
                                                                   @endif
                                                                   name="rqtf[{{$department->id}}][]"
                                                                   value="{{ $rtqf->id }}">&nbsp;
                                                            <b>{{ ucwords($rtqf->level_name) }}</b>
                                                        </label>
                                                    </div>
                                                </div>
                                                {{--</div>--}}
                                                {{--</div>--}}
                                            @endforeach
                                        @else
                                            <li class="list-group-item list-group-item-warning text-danger">Not
                                                set !!
                                            </li>
                                        @endif
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td align="center" colspan="2">
                                <h5 class="text-danger">No qualification <sup>(department)</sup> !!</h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            @if($qualifications->count() > 0)
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Yes, Save</button>
                </div>
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
@endsection