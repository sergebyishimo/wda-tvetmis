@extends('wda.layout.main')

@section('panel-title', "Edit Schools Information")

@section('htmlheader_title', "Edit Schools Information")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
    <style>
    
    .select2-container--default .select2-selection--single {
        border-radius: 0;
        
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 20px;
    }

    </style>
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2" style="width: 10.66666667%">
                <form method="POST" action="{{ route('wda.editAllSchools') }}" id="paginateForm">
                    @csrf
                    <div class="form-group">
                        <select name="paginate" id="" class="form-control" onchange="document.getElementById('paginateForm').submit()">
                            <option @if($paginate == 20) selected @endif >20</option>
                            <option @if($paginate == 50) selected @endif >50</option>
                            <option @if($paginate == 100) selected @endif >100</option>
                        </select>    
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                
                <form method="POST" action="{{ route('wda.editAllSchoolsSave') }}">
                    @csrf
                    
                    <table class="table table-bordered table-striped table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th class="text-center" width="10px">#</th>
                                <th style="width:100px">School Code</th>
                                <th style="width:100px">School Activity</th>
                                <th style="width:200px">School Name</th>
                                <th style="width:100px">School Status</th>
                                <th style="width:150px">Manager Name</th>
                                <th style="width:100px">School Type</th>
                                <th style="width:100px">Phone</th>
                                <th style="width:180px">E-mail</th>
                                <th style="width:100px">Accred. Status</th>
                                <th style="width:100px">Province</th>
                                <th style="width:100px">District</th>
                                <th style="width:100px">Sector</th>
                                <th style="width:100px">Latitude</th>
                                <th style="width:100px">Longitude</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($schools as $school)
                                <tr>
                                    <td><input type="hidden" name="school_id[]" value="{{ $school->id }}"> {{ $i }}</td>
                                    <td><input type="text" class="form-control" name="school_code[]" value="{{ $school->school_code }}"></td>
                                    <td>
                                        <select class="form-control" name="school_activity[]" style="border-radius:0 !important" required>
                                            <option @if($school->school_activity == 'Active') selected @endif>Active</option>
                                            <option @if($school->school_activity == 'Not Active') selected @endif>Not Active</option>
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="school_name[]" value="{{ $school->school_name }}" required></td>
                                    <td>
                                        <select class="form-control flat" required name="school_status[]" required>
                                            <option @if($school->school_status == 'Private') selected @endif>Private</option>
                                            <option @if($school->school_status == 'Public') selected @endif>Public</option>
                                            <option @if($school->school_status == 'Government Aid') selected @endif>Government Aid</option>
                                            
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="manager_name[]" value="{{ $school->manager_name }}" required></td>
                                    <td>
                                        <select name="school_type[]" class="form-control flat select2" required>
                                            @foreach ($schoolTypes as $type)
                                                <option name="{{ $type->id  }}" @if($school->school_type == $type->id) selected @endif> {{ $type->school_type }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="phone[]" value="{{ $school->phone }}"></td>
                                    <td><input type="text" class="form-control" name="email[]" value="{{ $school->email }}" required></td>
                                    <td>
                                        <select class="form-control flat" name="accreditation_status[]">
                                            <option value="1" @if($school->accreditation_status == 1) selected @endif >Accredited</option>
                                            <option value="2" @if($school->accreditation_status == 2) selected @endif >Not Accredited</option>
                                            <option value="3" @if($school->accreditation_status == 3) selected @endif >In Process</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control flat" id="nprovince" class="province" data-i="{{ $i }}" name="province[]">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control flat ndistrict_{{ $i }}" id="ndistrict" data-i="{{ $i }}" name="district[]">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control flat nsector_{{ $i }}" id="nsector" name="sector[]">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="latitude[]" value="{{ $school->latitude }}"></td>
                                    <td><input type="text" class="form-control" name="longitude[]" value="{{ $school->longitude }}"></td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>

                    {{ $schools->links() }}
                
                    <br>
                    <button type="submit" class="btn btn-primary btn-block"> <i class="fa fa-check"></i> Save Information</button>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                dom: 'Blfrtip',
                ordering: false,
                searching: true,
                pager: true,
                scrollX:true,
                paging: false,
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    }
                ]
            });

           
           $("*#nprovince").change(function() {
                
                var p = $(this).val();
                var def = $(this).data('i');
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".ndistrict_" + def).html(html);
                });
           });

           $("*#ndistrict").change(function () {
                var p = $(this).val();
                var def = $(this).data('i');
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".nsector_" + def).html(html);
                });
            });
           
        });
    </script>
@endsection