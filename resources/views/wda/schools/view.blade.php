@extends('wda.layout.main')

@section('panel-title', "School Management")

@section('htmlheader_title', "School Management")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row mb-1">
            <div class="col-md-2">
                <a class="btn btn-primary" href="{{ route('wda.school.create') }}">
                    <i class="fa fa-plus-square"></i>
                    &nbsp;<span>Add New School</span>
                </a>
            </div>
            <div class="col-md-2">
                <button class="btn btn-success" type="button"
                data-toggle="modal" data-target="#uploadSchools">
                    <i class="fa fa-upload"></i>
                    &nbsp;<span>Upload Schools</span>
                </button>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" type="button"
                        data-toggle="modal" data-target="#assignQualifications">
                    <i class="fa fa-anchor"></i>
                    &nbsp;<span>Assign Qualifications</span>
                </button>
            </div>
            <div class="col-md-2">
                <a class="btn btn-info" href="{{ route('wda.search.index') }}">
                    <i class="fa fa-search-plus"></i>
                    &nbsp;<span>Advanced Search</span>
                </a>
            </div>
             <div class="col-md-2">
                <a class="btn btn-danger" href="{{ route('wda.search.filter') }}">
                    <i class="fa fa-search-plus"></i>
                    &nbsp;<span>Search Filters</span>
                </a>
            </div>
            <div class="col-md-2 pull-right">
                <a class="btn btn-warning" href="{{ route('wda.school.archive') }}">
                    <i class="fa fa-archive"></i>
                    &nbsp;<span>Deleted School</span>
                </a>
            </div>
        </div>
        <div class="row" style="overflow-y: scroll;">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" id="nav-tab">
                            <li role="presentation" class="{{ request()->get('tb') == 'i' || ! request()->has('tb') ? 'active' : '' }}">
                                <a href="?tb=i">Active <span class="badge badge-primary">{{ count($schools) }}</span></a>
                            </li>
                            <li role="presentation" class="{{ request()->get('tb') == 'p'? 'active' : '' }}">
                                <a href="?tb=p">Not active <span class="badge badge-success">{{ count($notactiveschools) }}</span></a>
                            </li>
                            <li role="presentation" class="{{ request()->get('tb') == 'm'? 'active' : '' }}">
                                <a href="?tb=m">Suspended <span class="badge badge-success">{{ count($suspendedschools) }}</span></a>
                            </li>
                            <li role="presentation" class="{{ request()->get('tb') == 'd'? 'active' : '' }}">
                                <a href="?tb=d">Qualifications <span class="badge badge-success">{{ $deps }}</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body" style="overflow-x: auto">
                        <div class="tab-content" id="myTabContent">
                            @if(request()->get('tb') == 'i' || ! request()->has('tb'))
                                <table class="table table-bordered table-striped" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>School Name</th>
                                        <th>Province</th>
                                        <th>District</th>
                                        <th>School Type</th>
                                        <th>Accreditation</th>
                                        <th>School Activity</th>
                                        <th>School Status</th>
                                        <th>Phone Number</th>
                                        <th>E-Mail</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($schools as $school)
                                        <tr>
                                            <td><a href="/schools/{{ $school->id }}" target="_blank" style="color: #333">{{ $school->school_name }}</a></td>
                                            <td>{{ $school->province }}</td>
                                            <td>{{ $school->district }}</td>
                                            <td>{{ ($school->accreditation_status == 1) ? 'Accredited' : 'Not Accredited' }}</td>
                                            <td>{{ $school->phone }}</td>
                                            <td>
                                                <form method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                                    <input type="hidden" name="delete_school" value="{{ $school->id }}">

                                                    <a href="{{ route('wda.view.school', $school->id) }}"
                                                    title="View More"
                                                    class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i></a>
                                                    <a href="{{ route('wda.edit.school', $school->id) }}"
                                                    title="Edit"
                                                    class="btn btn-primary btn-sm btn-flat"><i
                                                                class="fa fa-edit"></i></a>
                                                    <button type="button" class="btn btn-primary btn-sm"
                                                            data-school="{{ $school->id }}"
                                                            data-school-name="{{ $school->school_name }}"
                                                            data-toggle="modal" data-target="#assignQualifications">
                                                        <i class="fa fa-anchor"></i>
                                                    </button>
                                                    <button type="submit" class="btn btn-danger btn-sm pull-right btn-flat"
                                                            title="Delete"
                                                            onclick="return window.confirm('Are you sure you want to remove this school:\n{{ $school->school_name }}?')">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>

                                        </tr>
                                    @endforeach --}}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>School Name</th>
                                        <th>Province</th>
                                        <th>District</th>
                                        <th>School Type</th>
                                        <th>Accreditation</th>
                                        <th>School Activity</th>
                                        <th>School Status</th>
                                        <th>Phone Number</th>
                                        <th>E-Mail</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            @endif
                            @if(request()->get('tb') == 'p')
                                <table class="table table-bordered table-striped" id="myTable2">
                                        <thead>
                                        <tr>
                                            <th>School Name</th>
                                            <th>Province</th>
                                            <th>District</th>
                                            <th>School Type</th>
                                            <th>Accreditation</th>
                                            <th>School Activity</th>
                                            <th>School Status</th>
                                            <th>Phone Number</th>
                                            <th>E-Mail</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($notactiveschools as $school)
                                            <tr>
                                                <td><a href="/schools/{{ $school->id }}" target="_blank" style="color: #333">{{ $school->school_name }}</a></td>
                                                <td>{{ $school->province }}</td>
                                                <td>{{ $school->district }}</td>
                                                <td>{{ $school->school_type }}</td>
                                                <td>{{ ($school->accreditation_status == 1) ? 'Accredited' : 'Not Accredited' }}</td>
                                                <td>{{ $school->school_activity }}</td>
                                                <td>{{ $school->school_status }}</td>
                                                <td>{{ $school->phone }}</td>
                                                <td>{{ $school->email }}</td>
                                                <td>
                                                    <form method="POST">{{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="delete">
                                                        <input type="hidden" name="delete_school" value="{{ $school->id }}">
                                                        <a href="{{ route('wda.view.school', $school->id) }}" title="View More" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i>
                                                        </a>
                                                        <a title="Browsing" href="/schools/{{ $school->school_acronym }}" target="_blank" style="color: #333" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-server"></i>
                                                        </a>
                                                        <a href="{{ route('wda.edit.school', $school->id) }}" title="Edit" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a> <button type="button" class="btn btn-primary btn-sm" data-school="' . $school->id . '" data-school-name="{{ $school->school_name }}" data-toggle="modal" data-target="#assignQualifications"> <i class="fa fa-anchor"></i>
                                                        <button type="submit" class="btn btn-danger btn-sm pull-right btn-flat" title="Delete" onclick="return window.confirm("Are you sure you want to remove this school:\n {{ $school->school_name }}?)">
                                                        <i class="fa fa-trash"></i> </button>
                                                        <a href="{{ route('wda.staff.upload', [ 'id' => $school->id]) }}" class="btn btn-success btn-sm btn-flat"><i class="fa fa-upload"></i></a>
                                                    </form>
                                                </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                </table>
                            @endif
                            @if(request()->get('tb') == 'm')
                                    <table class="table table-bordered table-striped" id="myTable2">
                                        <thead>
                                        <tr>
                                            <th>School Name</th>
                                            <th>Province</th>
                                            <th>District</th>
                                            <th>School Type</th>
                                            <th>Accreditation</th>
                                            <th>School Activity</th>
                                            <th>School Status</th>
                                            <th>Phone Number</th>
                                            <th>E-Mail</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($suspendedschools as $school)
                                            <tr>
                                                <td><a href="/schools/{{ $school->id }}" target="_blank" style="color: #333">{{ $school->school_name }}</a></td>
                                                <td>{{ $school->province }}</td>
                                                <td>{{ $school->district }}</td>
                                                <td>{{ $school->school_type }}</td>
                                                <td>{{ ($school->accreditation_status == 1) ? 'Accredited' : 'Not Accredited' }}</td>
                                                <td>{{ $school->school_activity }}</td>
                                                <td>{{ $school->school_status }}</td>
                                                <td>{{ $school->phone }}</td>
                                                <td>{{ $school->email }}</td>
                                                <td>
                                                    <form method="POST">{{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="delete">
                                                        <input type="hidden" name="delete_school" value="{{ $school->id }}">
                                                        <a href="{{ route('wda.view.school', $school->id) }}" title="View More" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i>
                                                        </a>
                                                        <a title="Browsing" href="/schools/{{ $school->school_acronym }}" target="_blank" style="color: #333" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-server"></i>
                                                        </a>
                                                        <a href="{{ route('wda.edit.school', $school->id) }}" title="Edit" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a> <button type="button" class="btn btn-primary btn-sm" data-school="' . $school->id . '" data-school-name="{{ $school->school_name }}" data-toggle="modal" data-target="#assignQualifications"> <i class="fa fa-anchor"></i>
                                                            <button type="submit" class="btn btn-danger btn-sm pull-right btn-flat" title="Delete" onclick="return window.confirm("Are you sure you want to remove this school:\n {{ $school->school_name }}?)">
                                                            <i class="fa fa-trash"></i> </button>
                                                        <a href="{{ route('wda.staff.upload', [ 'id' => $school->id]) }}" class="btn btn-success btn-sm btn-flat"><i class="fa fa-upload"></i></a>
                                                    </form>
                                                </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                            @endif
                            @if(request()->get('tb') == 'd')
                                    <table class="table table-bordered table-striped" id="myTable3">
                                        <thead>
                                        <tr>
                                            <th>School Name</th>
                                            <th>School Type</th>
                                            <th>School Status</th>
                                            <th>Province</th>
                                            <th>District</th>
                                            <th>Sector</th>
                                            <th>TVET Sector</th>
                                            <th>TVET SubSector</th>
                                            <th>TVET Qualification</th>
                                            <th>RTQF</th>
                                            <th>Contact</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>

                
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function () {
            let url = "{!! $url !!}";
            $('#myTable').DataTable({
                dom: 'Blfrtip',
                serverSide: true,
                processing: true,
                pager: true,
                colReorder: true,
                "columnDefs": [
                    { "width": "200px", "targets": 0 },
                    { "width": "90px", "targets": 8 }
                ],
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                ajax: url,
                "oSearch": {"bSmart": false},
                columns: [
                    {data: 'school_name', name: 'accr_schools_information.school_name'},
                    {data: 'province', name: 'accr_schools_information.province', orderable:true, searchable:true},
                    {data: 'district', name: 'accr_schools_information.district', orderable:true, searchable:true},
                    {data: 'school_type', name: 'accr_source_school_type.school_type', orderable:true, searchable:true},
                    {data: 'accreditation_status', name: 'accr_schools_information.accreditation_status', orderable:true, searchable:true},
                    {data: 'school_activity', name: 'accr_schools_information.school_activity', orderable:true, searchable:true},
                    {data: 'school_status', name: 'accr_schools_information.school_status', orderable:true, searchable:true},
                    {data: 'phone', name: 'accr_schools_information.phone', orderable:true, searchable:true},
                    {data: 'email', name: 'accr_schools_information.email', orderable:true, searchable:true},
                    {data: 'action', orderable: false, searchable: false}
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                initComplete: function () {
                    var x = 0;
                    var lenC = this.api().columns().length;
                    this.api().columns().every(function () {
                        if (x > 0 ){
                            var column = this;
                            var select = $('<select class="select2"><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        x++;
                    });
                }
            });

            $('#myTable2').DataTable({
                dom: 'Blfrtip',
                "lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
                "responsive": true,
                "scrollX": true,
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
            });

            $("#myTable3").DataTable({
                dom: 'Blfrtip',
                serverSide: true,
                processing: true,
                pager: true,
                colReorder: true,
                ajax: '/wda/datatable/object-data/schools_qualifications',
                columns: [
                    {data: 'school_name', name:'school_name'},
                    {data: 'school_type', name:'accr_source_school_type.school_type'},
                    {data: 'school_status', name:'school_status'},
                    {data: 'province', name:'province'},
                    {data: 'district', name:'district'},
                    {data:  'sector', name: 'sector'},
                    {data: 'tvet_field', name:'accr_training_sectors_source.tvet_field'},
                    {data:  'sub_field_name', name: 'accr_training_trades_source.sub_field_name'},
                    {data:  'qualification_title', name: 'accr_curriculum_qualifications.qualification_title'},
                    {data:  'qualification_type', name: 'accr_rtqf_source.qualification_type'},
                    {data: 'manager_phone', defaultContent: '-'}
                ],

                "lengthMenu": [[100,200,300,500,-1], [100,200,300,500,"All"]]
            });

            $('#filterSearchremove').click(function (e) {
                e.preventDefault();
                $('#filterSearch').hide();
            });

            $('#columnremove').click(function (e) {
                e.preventDefault();
                $('#filtercolumns').hide();
            });
        });
    </script>
@endsection