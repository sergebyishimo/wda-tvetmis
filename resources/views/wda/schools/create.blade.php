@extends('wda.layout.main')

@section('panel-title', "Create School")

@section('htmlheader_title', "Create School")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-md-2">
                <a class="btn btn-primary btn-md" href="{{ route('wda.schools') }}">
                    <i class="fa fa-list"></i>&nbsp;<span>Return to list</span></a>
            </div>
        </div>
        {!! Form::open(['route' => 'wda.store.school', 'files' => true, 'id' => 'createSchool']) !!}
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Primary information</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label>School Code</label>
                            <input type="text" class="form-control flat" name="school_code"
                                   placeholder="School Code">
                        </div>
                        <div class="form-group">
                            <label>School Name</label>
                            <input type="text" class="form-control flat" name="school_name"
                                   placeholder="School Name">
                        </div>
                        <div class="form-group">
                            <label>School Acronym</label>
                            <input type="text" class="form-control flat" name="school_acronym"
                                   placeholder="School Acronym">
                        </div>
                        <div class="form-group">
                            <label for="">E-mail:</label>
                            <input type="text" class="form-control flat" name="email"
                                   value="{{ $school_info['email'] or '' }}">

                        </div>
                        <div class="form-group">
                            <label for="">Phone:</label>
                            <input type="text" class="form-control flat" name="phone"
                                   value="{{ $school_info['phone'] or '' }}">

                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 col-form-label">School Type</label>
                            <div class="col-md-12">
                                <select name="school_type" class="form-control flat select2">
                                    <option>Choose School Type ...</option>
                                    @foreach ($schoolTypes as $type)
                                        <option value="{{ $type->id  }}">{{ $type->school_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">School Status:</label>
                            <select class="form-control flat" required name="school_status">
                                <option value="">Choose Here</option>
                                <option @if( isset($school_info) && $school_info['school_status'] == "Public") selected @endif>
                                    Public
                                </option>
                                <option @if( isset($school_info) && $school_info['school_status'] == "Government Aid") selected @endif>
                                    Government Aid
                                </option>
                                <option @if( isset($school_info) && $school_info['school_status'] == "Private") selected @endif>
                                    Private
                                </option>
                            </select>

                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">School Activity</label>
                            <select class="form-control" name="school_activity">
                                <option>Active</option>
                                <option>Not Active</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Manager Name:</label>
                            <input type="text" class="form-control flat" name="manager_name"
                                   value="{{  $school_info['manager_name'] or '' }}">

                        </div>
                        <div class="form-group">
                            <label for="">Manager Phone:</label>
                            <input type="text" class="form-control flat" name="manager_phone"
                                   value="{{  $school_info['manager_phone'] or '' }}">

                        </div>
                        <div class="form-group">
                            <label for="">Manager E-mail:</label>
                            <input type="text" class="form-control flat" name="manager_email"
                                   value="{{  $school_info['manager_email'] or '' }}">

                        </div>
                        <div class="form-group">
                            <label for="">Accreditation Status</label>
                            <select class="form-control flat" name="accreditation_status">
                                <option>Accredited</option>
                                <option>Not Accredited</option>
                                <option>In Process</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Location information</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label>Province</label>
                            <select class="form-control flat" id="nprovince" name="province">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>District</label>
                            <select class="form-control flat" id="ndistrict" name="district">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Sector:</label>
                            <select class="form-control flat" id="nsector" name="sector">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Cell:</label>
                            <select class="form-control flat" id="ncell" name="cell">
                                <option value="">Choose Here</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Village:</label>
                            <select class="form-control flat" id="nvillage" name="village">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Latitude:</label>
                            <input type="text" class="form-control flat" name="latitude"
                                   value="{{ $school_info['latitude'] or '' }}">

                        </div>
                        <div class="form-group">
                            <label for="">Longitude:</label>
                            <input type="text" class="form-control flat" name="longitude"
                                   value="{{ $school_info['longitude'] or '' }}">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Key Infrastructure</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has Electricity</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="has_electricity">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has Water</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="has_water">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has Computer Lab</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="has_computer_lab">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has Internet</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="has_internet">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Type of internet connection</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="type_of_internet_connection">
                                    <option value=""></option>
                                    <option value="4G">4G</option>
                                    <option value="3G">3G</option>
                                    <option value="2G">2G</option>
                                    <option value="Fiber">Fiber</option>
                                    <option value="Others">Others</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has Library</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="has_library">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has Business or Strategic
                                Plan</label>
                            <div class="col-md-3">
                                <select class="form-control flat"
                                        name="has_business_or_strategic_plan">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has Feeding Program</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="has_feeding_program">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Has three phase electricity</label>
                            <div class="col-md-3">
                                <select class="form-control flat" name="has_three_phase_electricity">
                                    <option value="2">Yes</option>
                                    <option value="1">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of incubation centers</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_incubation_centers">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Key Statistics</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Desktops</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_desktops">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Classrooms</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_classrooms">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Male Students</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="students_males">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Female Students</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="students_female">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Computer Labs</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_computer_labs">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of positivo Laptops</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_positivo_laptops">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Male Staff
                                Members</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat" name="staff_male">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Female Staff
                                Members</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat" name="staff_female">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Generators</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_generators">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Smart Classrooms</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_smart_classrooms">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Number of Laptops</label>
                            <div class="col-md-3">
                                <input type="number" class="form-control flat"
                                       name="number_of_laptops">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Other Information</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Accreditation Number</label>
                            <input type="text" class="form-control flat" name="accreditation_number"
                                   placeholder="Accreditation Number">
                        </div>
                        <div class="form-group">
                            <label for="">Website</label>
                            <input type="text" class="form-control flat" name="website"
                                   placeholder="Website">
                        </div>
                        <div class="form-group">
                            <label>Date of Establishment</label>
                            <input type="text" class="form-control datepicker-year flat"
                                   name="date_of_establishment">
                        </div>
                        <div class="form-group">
                            <label for="">Boarding / Day</label>
                            <select class="form-control flat" name="boarding_or_day">
                                <option>Boarding</option>
                                <option>Day</option>
                                <option>Both</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">School Logo</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="school_logo"
                                           id="exampleInputFile">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">


                        <div class="form-group">
                            <label for="">Owner Name:</label>
                            <input type="text" class="form-control flat" name="owner_name"
                                   value="{{ $school_info['owner_name'] or '' }}">

                        </div>
                        <div class="form-group">
                            <label for="">Owner Phone:</label>
                            <input type="text" class="form-control flat" name="owner_phone"
                                   value="{{ $school_info['owner_phone'] or '' }}">

                        </div>
                        <div class="form-group">
                            <label for="">Owner Type:</label>
                            <select class="form-control" name="owner_type">
                                @foreach ($ownerships as $ownership)
                                    <option value="{{ $ownership->id }}">{{ $ownership->owner }}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="">Owner E-mail:</label>
                            <input type="text" class="form-control flat" name="owner_email"
                                   value="{{ $school_info['owner_email'] or '' }}">

                        </div>
                        {{--<div class="form-group row">--}}
                            {{--<label class="col-md-12 col-form-label">School Rating</label>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<select name="school_rating" class="form-control flat select2">--}}
                                    {{--<option>Choose School Rate ...</option>--}}
                                    {{--@foreach ($schoolRating as $rating)--}}
                                        {{--<option name="{{ $rating->id  }}">{{ $rating->rating }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Cost Of Trainings</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('cost_of_consumables_per_year', "Estimated Consumable Budget Required per Year *") !!}
                            {!! Form::text('cost_of_consumables_per_year', $costoftraining ? $costoftraining->cost_of_consumables_per_year: "", ['class' => 'form-control currency']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('budget_actually_available_for_salaries_per_year', "Budget Actually Available for Salaries per Year *") !!}
                            {!! Form::text('budget_actually_available_for_salaries_per_year', $costoftraining ? $costoftraining->budget_actually_available_for_salaries_per_year: "", ['class' => 'form-control currency']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('estimated_operational_budget_required_per_year', "Estimated Operational Budget Required per Year *") !!}
                            {!! Form::text('estimated_operational_budget_required_per_year', $costoftraining ? $costoftraining->estimated_operational_budget_required_per_year: "", ['class' => 'form-control currency']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('estimated_total_salaries_year', "Estimated Total Salaries Required per Year *") !!}
                            {!! Form::text('estimated_total_salaries_year', $costoftraining ? $costoftraining->estimated_total_salaries_year: "", ['class' => 'form-control currency']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('budget_available_for_consumables', "Budget Actually Available for Consumables per Year *") !!}
                            {!! Form::text('budget_available_for_consumables', $costoftraining ? $costoftraining->budget_available_for_consumables: "", ['class' => 'form-control currency']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('operational_funds_available_per_year', "Budget Actually Available for Operational Funds per Year *") !!}
                            {!! Form::text('operational_funds_available_per_year', $costoftraining ? $costoftraining->operational_funds_available_per_year: "", ['class' => 'form-control currency', 'required' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('academic_year', "Academic Year *") !!}
                            {!! Form::select('academic_year', academicYear(null, null, true), $costoftraining ? $costoftraining->academic_year: null, ['class' => 'form-control select2', 'required' => true] ) !!}
                            {{--{!! Form::text('academic_year', $costoftraining ? $costoftraining->academic_year: "", ['class' => 'form-control', 'required' => true]) !!}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save School Information
        </button>
        {!! Form::close() !!}
    </div>
@endsection
@section("l-scripts")
    @parent
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CreateNewSchoolRequest', '#createSchool'); !!}
@endsection