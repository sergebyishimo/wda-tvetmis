@extends('wda.layout.main')

@section('panel-title', "Deleted Schools")

@section('htmlheader_title', "Deleted Schools")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-2">
                <a class="btn btn-primary" href="{{ route('wda.school.create') }}">
                    <i class="fa fa-plus-square"></i>
                    &nbsp;<span>Add New School</span>
                </a>
            </div>
            <div class="col-md-2">
                <a class="btn btn-primary btn-md" href="{{ route('wda.schools') }}">
                    <i class="fa fa-list"></i>&nbsp;<span>Return to list</span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped" id="myTable">
                    <thead>
                    <tr>
                        <th class="text-center" width="50px">#</th>
                        <th>School Name</th>
                        <th>Province</th>
                        <th>District</th>
                        <th>Accreditation</th>
                        <th>Phone Number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($schools as $school)
                        <tr>
                            <td class="text-center">{{ $i++ }}</td>
                            <td>{{ $school->school_name }}</td>
                            <td>{{ $school->province }}</td>
                            <td>{{ $school->district }}</td>
                            <td>{{ ($school->accreditation_status == 1) ? 'Accredited' : 'Not Accredited' }}</td>
                            <td>{{ $school->phone }}</td>
                            <td>
                                <form method="POST" action="{{ route('wda.school.restore') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="restore" value="{{ $school->id }}">

                                    <button type="submit" class="btn btn-warning btn-sm pull-right btn-flat"
                                            title="Restore"
                                            onclick="return window.confirm('Are you sure you want to restore this school:\n{{ $school->school_name }}?')">
                                        <i class="fa fa-ils"></i> &nbsp; <span>Restore</span>
                                    </button>
                                </form>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                dom: 'Blfrtip',
                ordering: false,
                searching: true,
                pager: true,
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $('#filterSearchremove').click(function (e) {
                e.preventDefault();
                $('#filterSearch').hide();
            });
            $('#columnremove').click(function (e) {
                e.preventDefault();
                $('#filtercolumns').hide();
            });
        });
    </script>
@endsection