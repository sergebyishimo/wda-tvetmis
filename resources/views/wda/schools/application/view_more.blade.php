@extends('wda.layout.main')

@section('panel-title', "Final AQA : ".$school_info->school_name)

@section('htmlheader_title', "Final AQA : ".$school_info->school_name)


@section('panel-body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{--Start Section 1 : Input--}}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="card-title">Section 1 : Input</h3>
                        <a class="pull-right btnPrint" style="margin-top: -27px;"
                           href="{{ route('wda.print.accr.application', $application_id) }}"><i class="fa fa-print"></i>
                            Print</a>

                    </div>
                    <!-- /.card-header -->
                    <div class="panel-body" style="overflow: hidden;">
                        <div id="myForm" role="form" accept-charset="utf-8">

                            <!-- Twitter-bootstrap-wizard html -->
                            <div id="rootwizard">
                                <div class="navbar">
                                    <div class="navbar-inner">
                                        <div class="container">
                                            @php
                                                $tab = request()->get('tab') ? request()->get('tab') : 1;
                                            @endphp
                                            <ul>
                                                <li><a href="?tab=1"
                                                       class="{{ $tab == '1' ? 'active' : '' }}">Step
                                                        1<br/>
                                                        <small>School Info</small>
                                                    </a></li>
                                                <li><a href="?tab=2"
                                                       class="{{ $tab == '2' ? 'active' : '' }}">Step
                                                        2<br/>
                                                        <small>Attachments</small>
                                                    </a></li>
                                                <li><a href="?tab=3"
                                                       class="{{ $tab == '3' ? 'active' : '' }}">Step
                                                        3<br/>
                                                        <small>Infrastructure</small>
                                                    </a></li>
                                                <li><a href="?tab=4"
                                                       class="{{ $tab == '4' ? 'active' : '' }}">Step
                                                        4<br/>
                                                        <small>Programs Offered</small>
                                                    </a></li>
                                                <li><a href="?tab=5"
                                                       class="{{ $tab == '5' ? 'active' : '' }}">Step
                                                        5<br/>
                                                        <small>Trainers / Teachers</small>
                                                    </a></li>
                                                <li><a href="?tab=6"
                                                       class="{{ $tab == '6' ? 'active' : '' }}">Step
                                                        6<br/>
                                                        <small>Admin Staff</small>
                                                    </a></li>
                                                <li><a href="?tab=7"
                                                       class="{{ $tab == '7' ? 'active' : '' }}">Step
                                                        7<br/>
                                                        <small>Applying For</small>
                                                    </a></li>
                                                <li><a href="?tab=8"
                                                       class="{{ $tab == '8' ? 'active' : '' }}">Step
                                                        8<br/>
                                                        <small>General Recommendation</small>
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    @if(request()->get('tab') == '1' || ! request()->get('tab'))
                                        <div class="">
                                            <h5><i class="fa fa-check-circle"></i> School Information</h5>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Name:</label>
                                                        <p>{{ $school_info['school_name'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Province:</label>
                                                        <p>{{ ucwords($school_info['province']) }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">District:</label>
                                                        <p>{{ $school_info['district'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Sector:</label>
                                                        <p>{{ $school_info['sector'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Cell:</label>
                                                        <p>{{ $school_info['cell'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Village:</label>
                                                        <p>{{ $school_info['village'] }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Manager Name:</label>
                                                        <p>{{ $school_info['manager_name'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Manager Phone:</label>
                                                        <p>{{ $school_info['manager_phone'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Manager E-mail:</label>
                                                        <p>{{ $school_info['manager_email'] }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Latitude:</label>
                                                        {{ $school_info['latitude'] }}
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Longitude:</label>
                                                        <p>{{ $school_info['longitude'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Phone:</label>
                                                        <p>{{ $school_info['phone'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">E-mail:</label>
                                                        <p>{{ $school_info['email'] }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">School Status:</label>
                                                        <p>{{ $school_info['school_status'] }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Owner Name:</label>
                                                        <p>{{ $school_info['owner_name'] }}</p>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Owner Phone:</label>
                                                        <p>{{ $school_info['owner_phone'] }}</p>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Owner Type:</label>
                                                        <p>{{ $school_info->ownership ? $school_info->ownership->owner : '' }}</p>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Owner E-mail:</label>
                                                        <p>{{ $school_info['owner_email'] }}</p>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    {{--<button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Save & Continue <i class="fa fa-arrow-right"></i></button>		--}}
                                                    <a href="?tab={{ $tab++ }}"
                                                       class="btn btn-primary btn-block btn-flat"><i
                                                                class="fa fa-save"></i> Save & Continue <i
                                                                class="fa fa-arrow-right"></i></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a href="?tab={{ $tab++ }}"
                                                               class="btn btn-default btn-block btn-flat">
                                                                Next <i class="fa fa-arrow-right"></i></a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @elseif(request()->get('tab') == '2')
                                        <div class="">
                                            <h5><i class="fa fa-check-circle"></i> Uploaded Attachments</h5>

                                            @php
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 2)->first();
                                            @endphp

                                            <div style="overflow: auto;">
                                                <table class="table table-bordered table-hover">
                                                    <thead style="background: #efefef">
                                                    <tr>
                                                        <th style="width: 15%"></th>
                                                        <th>Attachment Name</th>
                                                        <th style="width: 25%">Download File</th>
                                                        <th>Verified</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach($attachments_data as $ad)

                                                        <tr>
                                                            <td class="text-center">{{$i++ }}</td>
                                                            <td>{{ $ad->source }}</td>
                                                            <td>
                                                                <a href="{{ Storage::url("$ad->attachment")}}?{{ $school_info['school_name'] . ' ' . str_replace('/', ' ', $ad->source ) }}">Download
                                                                    File</a></td>
                                                            <td>
                                                                {{ $ad->district_verified }}
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>

                                            <h2>District Provisional AQA</h2>

                                            <div>
                                                @include('wda.schools.application.display')
                                            </div>

                                            <h2>WDA Final AQA</h2>

                                            <form method="POST" action="{{ route('wda.save.accr.application') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="form" value="attachments">
                                                <input type="hidden" name="application_id" value="{{ $application_id}}">

                                                <input type="hidden" name="section" value="1">
                                                <input type="hidden" name="step" value="2">


                                                @php
                                                    $comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 2)->first();
                                                @endphp

                                                <div>
                                                    @include('wda.schools.application.form');
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            <i
                                                                    class="fa fa-arrow-left"></i> Previous </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-block btn-flat"><i
                                                                    class="fa fa-save"></i> Save & Continue <i
                                                                    class="fa fa-arrow-right"></i></button>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>

                                            </form>

                                        </div>
                                    @elseif(request()->get('tab') == '3')
                                        <div class="">
                                            <h5><i class="fa fa-check-circle"></i> Materials &amp; Infrastructure</h5>

                                            @php
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 3)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Building</th>
                                                    <th>Infrastructure</th>
                                                    <th>Purpose</th>
                                                    <th>Size</th>
                                                    <th>Capacity</th>
                                                    <th style="width: 120px;">Action</th>
                                                    <th>Verified</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($buildings_plots as $bp)

                                                    <tr>
                                                        <td></td>
                                                        <td>{{ $bp->name }}</td>
                                                        <td>

                                                            @foreach ($bp['infras'] as $infra)
                                                                {{ $infra->quantity. ' ' .$infra->name .  ', ' }}
                                                            @endforeach

                                                        </td>
                                                        <td>{{ $bp->purpose }}</td>
                                                        <td>{{ $bp->size }}</td>
                                                        <td>{{ $bp->capacity }}</td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-warning btn-flat btn-sm"
                                                                    data-toggle="modal"
                                                                    data-target="#view_{{ $bp->id }}"><i
                                                                        class="fa fa-tasks"></i> View More
                                                            </button>

                                                            <div class="modal fade" id="view_{{ $bp->id }}">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                            <h4 class="modal-title"><i
                                                                                        class="fa fa-home"></i> View
                                                                                Building Information</h4>
                                                                        </div>

                                                                        <div class="modal-body"
                                                                             style="padding: 30px;height: 450px;overflow-y: scroll;divbackground: #f0f1f3">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Building / Plot
                                                                                            Name</label>
                                                                                        <p>{{ $bp->name }}</p>
                                                                                    </div>


                                                                                    <div class="form-group">
                                                                                        <label>Building / Plot
                                                                                            Class</label>
                                                                                        <p>{{ $bp->class }}</p>
                                                                                    </div>

                                                                                    <div class="form-group row"
                                                                                         id="addBuilding2">
                                                                                        <label class="col-md-12 col-form-label">Infrastructure</label>
                                                                                        @foreach ($bp['infras'] as $infra)
                                                                                            <div class="col-md-6">
                                                                                                <p>{{ $infra->name }}</p>
                                                                                            </div>
                                                                                            <div class="col-md-5">
                                                                                                <p>{{ $infra->quantity }}</p>
                                                                                            </div>
                                                                                        @endforeach

                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Purpose</label>
                                                                                        <p>{{ $bp->purpose }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Size</label>
                                                                                        <p>{{ $bp->size }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Capacity</label>
                                                                                        <p>{{ $bp->capacity }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Construction
                                                                                            Materials</label>
                                                                                        <p>{{ $bp->construction_materials }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Roofing Materials</label>
                                                                                        <p>{{ $bp->roofing_materials }}</p>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label for=""
                                                                                               class="col-md-5 col-form-label">Harvests
                                                                                            Rain Water</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->harvests_rain_water }}</p>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Water</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_water }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Electricity</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_electricity }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Internet</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_internet }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Extinguisher</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_extinguisher }}</p>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            Lightening Arrestor</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_lightening_arrestor }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-5 col-form-label">Has
                                                                                            External Lighting</label>
                                                                                        <div class="col-md-3">
                                                                                            <p class="col-form-label">{{ $bp->has_external_lighting }}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Furniture</label>
                                                                                        <p>{{ $bp->furniture }}</p>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label>Equipment</label>
                                                                                        <p>{{ $bp->equipment }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer"
                                                                             style="text-align: center;">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <button type="button"
                                                                                            class="btn btn-default btn-block btn-flat"
                                                                                            data-dismiss="modal"><i
                                                                                                class="fa fa-close"></i>
                                                                                        Cancel
                                                                                    </button>
                                                                                </div>
                                                                            </div>


                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <select class="form-control flat" name="verified[]">
                                                                <option @if($bp->district_verified == 'Yes') selected @endif>
                                                                    Yes
                                                                </option>
                                                                <option @if($bp->district_verified == 'No') selected @endif>
                                                                    No
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                                </tbody>
                                            </table>

                                            <h2>District Provisional AQA</h2>

                                            <div>
                                                @include('wda.schools.application.display')
                                            </div>
                                            <h2>WDA Final AQA</h2>

                                            <form method="POST" action="{{ route('wda.save.accr.application') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="form" value="attachments">
                                                <input type="hidden" name="application_id" value="{{ $application_id}}">

                                                <input type="hidden" name="section" value="1">
                                                <input type="hidden" name="step" value="3">


                                                @php
                                                    //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'WDA')->where('application_id',$application_id)->where('section', 1)->where('step', 3)->first();
                                                    $comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 3)->first();
                                                @endphp
                                                <div>
                                                    @include('wda.schools.application.form');
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            <i class="fa fa-arrow-left"></i> Previous </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-block btn-flat"><i
                                                                    class="fa fa-save"></i> Save & Continue <i
                                                                    class="fa fa-arrow-right"></i></button>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    @elseif(request()->get('tab') == '4')
                                        <div class="">
                                            <h5><i class="fa fa-check-circle"></i> Programs Offered</h5>

                                            @php
                                                //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover" id="dataTableBtn">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th>#</th>

                                                    <th>Sector</th>
                                                    <th style="width: 15%">Sub-Sector</th>
                                                    <th>Qualification</th>
                                                    <th style="width: 15%">RTQF Level</th>
                                                    <th style="width: 10%">Male Trainees</th>
                                                    <th style="width: 10%">Female Trainees</th>
                                                    {{--<th>Curriculum</th>--}}
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($programs_offered as $pd)

                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $pd->qualification->subsector->sector->tvet_field or "" }}</td>
                                                        <td>{{ $pd->qualification->subsector->sub_field_name or ""  }}</td>
                                                        <td>{{ $pd->qualification->qualification_title or "" }}</td>
                                                        <td>{{ $pd->qualification->rtqf->level_name or "" }}</td>
                                                        <td>{{ $pd->qualification ? getStudentsNber($pd->qualification->rtqf->id, $pd->qualification_id, $pd->school_id, [['key' => 'gender', 'operator' => '=', 'needed' => ucwords('male')]]) : "" }}</td>
                                                        <td>{{ $pd->qualification ? getStudentsNber($pd->qualification->rtqf->id, $pd->qualification_id, $pd->school_id, [['key' => 'gender', 'operator' => '=', 'needed' => ucwords('female')]]) : "" }}</td>
                                                        {{--                                                <td>{{ $pd->curriculum }}</td>--}}
                                                        {{-- <td width="80px">
                                                            <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                                {{ csrf_field() }}

                                                                <input type="hidden" name="_method" value="delete">

                                                                <input type="hidden" name="form" value="programs">
                                                                <input type="hidden" name="delete_program"
                                                                       value="{{ $pd->id }}">

                                                                <a href="/school/accreditation/input/programs/edit/{{ $pd->id }}/?step=4"
                                                                   class="btn btn-primary btn-sm btn-flat"><i
                                                                            class="fa fa-edit"></i></a>

                                                                <button type="submit"
                                                                        class="btn btn-danger btn-sm btn-flat"
                                                                        onclick="return window.confirm('Are you sure you want to remove this program?')">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>

                                                            </form>
                                                        </td> --}}
                                                    </tr>
                                                @endforeach


                                                </tbody>
                                            </table>

                                            <h2>District Provisional AQA</h2>

                                            <div>
                                                @include('wda.schools.application.display')
                                            </div>

                                            <h2>WDA Final AQA</h2>

                                            <form method="POST" action="{{ route('wda.save.accr.application') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="form" value="attachments">
                                                <input type="hidden" name="application_id" value="{{ $application_id}}">

                                                <input type="hidden" name="section" value="1">
                                                <input type="hidden" name="step" value="4">


                                                @php
                                                    //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'WDA')->where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                                                    $comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 4)->first();
                                                @endphp
                                                <div>
                                                    @include('wda.schools.application.form');
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            <i class="fa fa-arrow-left"></i> Previous </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-block btn-flat"><i
                                                                    class="fa fa-save"></i> Save & Continue <i
                                                                    class="fa fa-arrow-right"></i></button>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @elseif(request()->get('tab') == '5')
                                        <div class="">
                                            <h5><i class="fa fa-check-circle"></i> Trainer / Teachers</h5>


                                            @php
                                                //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover" id="dataTableBtn">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th></th>
                                                    <th>Photo</th>
                                                    <th>Name</th>
                                                    <th style="width: 10%">Gender</th>
                                                    {{--<th>Level / Degree</th>--}}
                                                    <th>Qualification</th>
                                                    <th>Institution</th>
                                                    <th>Graduated Year</th>
                                                    <th>Attended RTTI</th>
                                                    {{--<th>Certification</th>--}}
                                                    {{--<th>Experience</th>--}}
                                                    {{--<th>Modules Taught</th>--}}
                                                    {{-- <th></th> --}}
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($trainers as $td)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>
                                                            <img src="{{ getStaffPhoto($td->photo) }}" alt=""
                                                                 class="img-responsive img-rounded"
                                                                 style="width: 90px;">
                                                        </td>
                                                        <td>{{ $td->names }}</td>
                                                        <td>{{ $td->gender }}</td>
                                                        {{--<td>--}}
                                                        {{--@if($td->qualifications)--}}
                                                        {{--@php--}}
                                                        {{--$degree = "";--}}
                                                        {{--@endphp--}}
                                                        {{--@foreach($td->qualifications as $quall)--}}
                                                        {{--@php--}}
                                                        {{--$degree .= ",".$quall->qualification_level;--}}
                                                        {{--@endphp--}}
                                                        {{--@endforeach--}}
                                                        {{--@endif--}}
                                                        {{--</td>--}}
                                                        <td>
                                                            {{ $td->qualification }}
                                                        </td>
                                                        <td>
                                                            {{ $td->institution }}
                                                        </td>
                                                        <td>{{ $td->graduated_year }}</td>
                                                        <td>{{ $td->attended_rtti }}</td>
                                                        {{--<td>{{ $td->certiName }}</td>--}}
                                                        {{--<td>{{ $td->experience }}</td>--}}
                                                        {{--<td>{{ $td->modules_taught }}</td>--}}
                                                        {{-- <td>
                                                            <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                                {{ csrf_field() }}

                                                                <input type="hidden" name="_method" value="delete">
                                                                <input type="hidden" name="form" value="teachers">
                                                                <input type="hidden" name="delete_trainer"
                                                                       value="{{ $td->id }}">

                                                                <button type="submit"
                                                                        class="btn btn-danger btn-sm btn-flat"
                                                                        onclick="return window.confirm('Are you sure you want to remove this trainer : \n\n {{ $td->names }} ?')">
                                                                    <i class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </form>
                                                        </td> --}}
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>

                                            <h2>District Provisional AQA</h2>

                                            <div>
                                                @include('wda.schools.application.display')
                                            </div>

                                            <h2>WDA Final AQA</h2>

                                            <form method="POST" action="{{ route('wda.save.accr.application') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="form" value="attachments">
                                                <input type="hidden" name="application_id" value="{{ $application_id}}">

                                                <input type="hidden" name="section" value="1">
                                                <input type="hidden" name="step" value="5">

                                                @php
                                                    //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'WDA')->where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                                                    $comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 5)->first();
                                                @endphp
                                                <div>
                                                    @include('wda.schools.application.form');
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            <i class="fa fa-arrow-left"></i> Previous </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-block btn-flat"><i
                                                                    class="fa fa-save"></i> Save & Continue <i
                                                                    class="fa fa-arrow-right"></i></button>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @elseif(request()->get('tab') == '6')
                                        <div class="">
                                            <h5><i class="fa fa-check-circle"></i> Administration Staff</h5>
                                            @php
                                                //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                                            @endphp

                                            <table class="table table-bordered table-hover" id="dataTableBtn">
                                                <thead style="background: #efefef">
                                                <tr>
                                                    <th></th>
                                                    <th>Photo</th>
                                                    <th>Name</th>
                                                    <th style="width: 10%">Gender</th>
                                                    {{--<th>Level / Degree</th>--}}
                                                    <th>Qualification</th>
                                                    <th>Institution</th>
                                                    <th>Graduated Year</th>
                                                    <th>Attended RTTI</th>
                                                    {{-- <th>Teaching Experience</th>
                                                    <th>Assessor Qualification</th> --}}
                                                    {{-- <th></th> --}}
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @forelse($staffs as $sd)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>
                                                            <img src="{{ getStaffPhoto($sd->photo) }}" alt=""
                                                                 class="img-responsive img-rounded"
                                                                 style="width: 90px;">
                                                        </td>
                                                        <td>{{ $sd->names }}</td>
                                                        <td>{{ $sd->gender }}</td>
                                                        {{--<td>--}}
                                                        {{--@if($td->qualifications)--}}
                                                        {{--@php--}}
                                                        {{--$degree = "";--}}
                                                        {{--@endphp--}}
                                                        {{--@foreach($td->qualifications as $quall)--}}
                                                        {{--@php--}}
                                                        {{--$degree .= ",".$quall->qualification_level;--}}
                                                        {{--@endphp--}}
                                                        {{--@endforeach--}}
                                                        {{--@endif--}}
                                                        {{--</td>--}}
                                                        <td>
                                                            {{ $sd->qualification }}
                                                        </td>
                                                        <td>
                                                            {{ $sd->institution }}
                                                        </td>
                                                        <td>{{ $sd->graduated_year }}</td>
                                                        <td>{{ $sd->attended_rtti }}</td>
                                                        {{--<td>{{ $td->certiName }}</td>--}}
                                                        {{--<td>{{ $td->experience }}</td>--}}
                                                        {{--<td>{{ $td->modules_taught }}</td>--}}
                                                        {{-- <td>
                                                            <form method="POST" action="{{ route('school.accr.input.save') }}">
                                                                {{ csrf_field() }}

                                                                <input type="hidden" name="_method" value="delete">
                                                                <input type="hidden" name="form" value="teachers">
                                                                <input type="hidden" name="delete_trainer"
                                                                       value="{{ $td->id }}">

                                                                <button type="submit"
                                                                        class="btn btn-danger btn-sm btn-flat"
                                                                        onclick="return window.confirm('Are you sure you want to remove this trainer : \n\n {{ $td->names }} ?')">
                                                                    <i class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </form>
                                                        </td> --}}
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="10" class="text-center">Empty !!</td>
                                                    </tr>
                                                @endforelse
                                                </tbody>
                                            </table>

                                            <h2>District Provisional AQA</h2>

                                            <div>
                                                @include('wda.schools.application.display')
                                            </div>

                                            <h2>WDA Final AQA</h2>

                                            <form method="POST" action="{{ route('wda.save.accr.application') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="form" value="attachments">
                                                <input type="hidden" name="application_id" value="{{ $application_id}}">

                                                <input type="hidden" name="section" value="1">
                                                <input type="hidden" name="step" value="6">

                                                @php
                                                    //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'WDA')->where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                                                    $comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 6)->first();
                                                @endphp

                                                <div>
                                                    @include('wda.schools.application.form');
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            <i
                                                                    class="fa fa-arrow-left"></i> Previous </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-block btn-flat"><i
                                                                    class="fa fa-save"></i> Save & Continue <i
                                                                    class="fa fa-arrow-right"></i></button>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @elseif(request()->get('tab') == '7')
                                        <div>
                                            <h5><i class="fa fa-check-circle"></i> Qualification Applied For</h5>


                                            @php
                                                //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                            @endphp
                                            <div style="overflow-x: auto;">
                                                <table class="table table-bordered table-hover">
                                                    <thead style="background: #efefef">
                                                    <tr>
                                                        <th></th>
                                                        <th style="width: 30%">Sector</th>
                                                        <th style="width: 20%">Sub Sector</th>
                                                        <th style="width: 20%">Curriculum Qualification</th>
                                                        <th>RTQF Level</th>
                                                        <th class="text-center">Decision</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach($application_data as $ad)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>{{ $ad->curr->subsector->sector->tvet_field or "" }}</td>
                                                            <td>{{ $ad->curr->subsector->sub_field_name or "" }}</td>
                                                            <td>{{ $ad->curr->qualification_title or "" }}</td>
                                                            <td>{{ $ad->rtqf->level_name or "" }}</td>
                                                            <td>
                                                                <div class="row">
                                                                    {!! Form::open(['route' => 'wda.accreditation.make.decision']) !!}
                                                                    {!! Form::hidden('qualification_id', $ad->curriculum_qualification_id) !!}
                                                                    {!! Form::hidden('school', $school_info->id) !!}
                                                                    {!! Form::hidden('application_qualification', $ad->id) !!}
                                                                    {!! Form::hidden('application_id', $application_id) !!}
                                                                    <div class="col-md-6">
                                                                        <input type="submit"
                                                                               name="reject"
                                                                               value="Not Accredited"
                                                                               class="btn btn-warning btn-sm">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <input type="submit"
                                                                               name="accepted"
                                                                               value="Accredited"
                                                                               class="btn btn-success btn-sm">
                                                                    </div>
                                                                    {!! Form::close() !!}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h2>District Provisional AQA</h2>

                                            <div>
                                                @include('wda.schools.application.display')
                                            </div>

                                            <h2>WDA Final AQA</h2>

                                            <form method="POST" action="{{ route('wda.save.accr.application') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="form" value="attachments">
                                                <input type="hidden" name="application_id" value="{{ $application_id}}">

                                                <input type="hidden" name="section" value="1">
                                                <input type="hidden" name="step" value="7">

                                                @php
                                                    //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'WDA')->where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                                    $comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                                @endphp
                                                <div>
                                                    @include('wda.schools.application.form');
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            <i
                                                                    class="fa fa-arrow-left"></i> Previous </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-block btn-flat"><i
                                                                    class="fa fa-save"></i> Save & Continue <i
                                                                    class="fa fa-arrow-right"></i></button>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="?tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat">
                                                            Next
                                                            <i class="fa fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @elseif(request()->get('tab') == '8')
                                        <div class="">
                                            <h5><i class="fa fa-check-circle"></i> General Recommendation</h5>

                                            <h2>District Provisional AQA</h2>


                                            @php
                                                $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                            @endphp

                                            <div class="row">

                                                <div class="form-group col-md-12">
                                                    <label>General Recommendation</label>
                                                    <p> {!!  $comments->recommendation or ''  !!} </p>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>Average Marks :</label>
                                                    <p> {{$comments->marks or '' }} </p>
                                                </div>
                                            </div>

                                            <h2>WDA Final AQA</h2>
                                            <form method="POST" action="{{ route('wda.save.accr.application') }}">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="form" value="general_recommendation">

                                                <input type="hidden" name="application_id"
                                                       value="{{ $application_id }}">

                                                @php
                                                    //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'WDA')->where('application_id',$application_id)->where('section', 1)->where('step', 7)->first();
                                                    $wdacomments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 1)->where('step', 8)->first();

                                                @endphp

                                                <div class="row">

                                                    <div class="form-group col-md-12">
                                                        <div class="mb-3">
														<textarea class="textarea" name="recommendation"
                                                                  placeholder="General Recommendation"
                                                                  style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                                  required>
															{{ $wdacomments->recommendation or "" }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>Average Marks</label>
                                                        <select class="form-control flat" name="marks">
                                                            <option {{ (isset($wdacomments) && $wdacomments->marks == 1) ? 'selected' : '' }}>
                                                                1
                                                            </option>
                                                            <option {{ (isset($wdacomments) && $wdacomments->marks == 2) ? 'selected' : '' }}>
                                                                2
                                                            </option>
                                                            <option {{ (isset($wdacomments) && $wdacomments->marks == 3) ? 'selected' : '' }}>
                                                                3
                                                            </option>
                                                            <option {{ (isset($wdacomments) && $wdacomments->marks == 4) ? 'selected' : '' }}>
                                                                4
                                                            </option>
                                                            <option {{ (isset($wdacomments) && $wdacomments->marks == 5) ? 'selected' : '' }}>
                                                                5
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <a href="?tab=tab={{ $tab++ }}"
                                                           class="btn btn-default btn-block btn-flat"> <i
                                                                    class="fa fa-arrow-left"></i> Previous </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-block btn-flat"><i
                                                                    class="fa fa-save"></i> Save & Continue To The Next
                                                            Section <i class="fa fa-arrow-right"></i></button>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-header">
                        <h3 class="card-title">Section 2 : Output</h3>
                    </div>
                    <div class="panel-body" style="overflow-x: auto;">
                        <form method="POST">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Graduation Year</label>
                                <div class="col-md-2">
                                    <select class="form-control flat" name="view_year">
                                        @for ($i = date('Y'); $i > date('Y')  - 5 ; $i--)
                                            <option>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-primary btn-flat btn-block"><i class="fa fa-list"></i> View
                                    </button>
                                </div>

                            </div>
                        </form>


                        @php
                            //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'District')->where('application_id',$application_id)->where('section', 3)->where('step', 1)->first();
                            $comments = App\Model\Accr\DistrictProvisionalAQA::where('application_id',$application_id)->where('section', 3)->where('step', 1)->first();

                        @endphp


                        <table class="table table-bordered table-hover">
                            <thead style="background: #eaeaea">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Trade</th>
                                <th>RTQF Achieved</th>
                                <th>Phone</th>
                                <th>Employment Status</th>
                                <th>Employment Timing</th>
                                <th>Monthly Salary</th>
                                <th>Name of Employment Company</th>
                                <th>Sector</th>
                                <th>Sub Sector</th>
                                <th>Employer Contact</th>
                                <th>Knowledge level</th>
                                <th>Attitude level</th>
                                <th>Skills level</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($graduates as $graduate)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $graduate->names }}</td>
                                    <td>{{ $graduate->gender }}</td>
                                    <td>{{ $graduate->trade }}</td>
                                    <td>{{ $graduate->rtqf_achieved }}</td>
                                    <td>{{ $graduate->phone_number }}</td>
                                    <td>{{ $graduate->employment_status }}</td>
                                    <td>{{ $graduate->employment_timing }}</td>
                                    <td>{{ $graduate->monthly_salary }}</td>
                                    <td>{{ $graduate->name_of_employment_company }}</td>
                                    <td>{{ $graduate->sector }}</td>
                                    <td>{{ $graduate->sub_sector }}</td>
                                    <td>{{ $graduate->employer_contact }}</td>
                                    <td>{{ $graduate->knowledge_level }}</td>
                                    <td>{{ $graduate->attitude_level }}</td>
                                    <td>{{ $graduate->skill_level }}</td>
                                </tr>
                            @endforeach

                            @if (count($graduates) == 0)
                                <tr>
                                    <td colspan="20" class="text-center">No Graduates Recorded in The Selected Period
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <br>
                        <h2>District Provisional AQA</h2>
                        @include('wda.schools.application.display')
                        <br>
                        <h2>WDA Final AQA</h2>
                        <form method="POST" action="{{ route('wda.save.accr.application') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="form" value="attachments">
                            <input type="hidden" name="application_id" value="{{ $application_id}}">

                            <input type="hidden" name="section" value="3">
                            <input type="hidden" name="step" value="1">

                            @php
                                //$comments = App\Model\Accr\WdaProvisionalAQA::where('level', 'WDA')->where('application_id',$application_id)->where('section', 3)->where('step', 1)->first();
                                $comments = App\Model\Accr\WdaProvisionalAQA::where('application_id',$application_id)->where('section', 3)->where('step', 1)->first();
                            @endphp


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Strength</label>
                                        <div class="mb-3">
																					<textarea class="textarea"
                                                                                              name="strength"
                                                                                              placeholder="Place some text here"
                                                                                              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                                                              required>{{ $comments->strength or "" }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Weakness</label>
                                        <div class="mb-3">
																					<textarea class="textarea"
                                                                                              name="weakness"
                                                                                              placeholder="Place some text here"
                                                                                              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                                                              required>
																						{{ $comments->weakness or "" }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Opportunity</label>
                                        <div class="mb-3">
																					<textarea class="textarea"
                                                                                              name="opportunity"
                                                                                              placeholder="Place some text here"
                                                                                              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                                                              required>
																						{{ $comments->opportunity or "" }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Threat</label>
                                        <div class="mb-3">
																					<textarea class="textarea"
                                                                                              name="threat"
                                                                                              placeholder="Place some text here"
                                                                                              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                                                              required>
																						{{ $comments->threat or "" }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Recommendation</label>
                                        <div class="mb-3">
																					<textarea class="textarea"
                                                                                              name="recommendation"
                                                                                              placeholder="Place some text here"
                                                                                              style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                                                              required>
																						{{ $comments->recommendation or "" }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Marks</label>
                                        <select class="form-control flat" name="marks">
                                            <option {{ (isset($comments) && $comments->marks == 1) ? 'selected' : '' }}>
                                                1
                                            </option>
                                            <option {{ (isset($comments) && $comments->marks == 2) ? 'selected' : '' }}>
                                                2
                                            </option>
                                            <option {{ (isset($comments) && $comments->marks == 3) ? 'selected' : '' }}>
                                                3
                                            </option>
                                            <option {{ (isset($comments) && $comments->marks == 4) ? 'selected' : '' }}>
                                                4
                                            </option>
                                            <option {{ (isset($comments) && $comments->marks == 5) ? 'selected' : '' }}>
                                                5
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info btn-block btn-flat"><i
                                                class="fa fa-save"></i>
                                        Save & Continue <i class="fa fa-arrow-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{--End Section 3 : Input--}}
        </div>
    </div>
    </div>

@endsection
@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".btnPrint").printPage();
        });
    </script>
@endsection