@extends('wda.layout.main')

@section('panel-title', "Schools Self Assessment")

@section('htmlheader_title', "School Self Assessment")

@section('l-style')
    @parent
    <script type="text/javascript">
        function confirmDelete(msg) {

        }
    </script>
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="panel panel-body">
            @php
                $ro = route('wda.school.assessment')."#resultListPanel";
            @endphp
            {!! Form::open(['method' => 'GET', 'url' => $ro]) !!}
            {!! Form::hidden('tb', request()->input('tb') ?:'i') !!}
            @if(isset($request) && count($request) >= 1)
                <div class="row">
                    <div class="col-md-12">
                        Query:
                        @php
                            if (isset($request['tb']))
                                unset($request['tb']);
                        @endphp
                        @foreach($request as $key => $item)
                            @if($item)
                                <label class="label label-success">{{ str_replace('_', ' ', ucwords($key)) }}
                                    : {{ $item }}</label>&nbsp;
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Year of Assessment</label>
                        {!! Form::select('year', academicYear(null, null, true) , null ) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Schools</label>
                        <select class="form-control flat" id="nschools" name="school">
                            <option value="">Choose Here</option>
                        </select>
                    </div>
                </div>
                <div class="when-school-null">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Province</label>
                            <select class="form-control flat" id="nprovince" name="province">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>District</label>
                            <select class="form-control flat" id="ndistrict" name="district">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">School Type:</label>
                            <select name="school_type" class="form-control flat select2">
                                <option value="">Choose Here</option>
                                @foreach ($schoolTypes as $type)
                                    <option value="{{ $type->id  }}">{{ $type->school_type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">School Status:</label>
                            <select class="form-control flat" name="school_status">
                                <option value="">Choose Here</option>
                                <option>Public</option>
                                <option>Government Aid</option>
                                <option>Private</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @if(request()->get('tb') != 'm')
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('quality_area', 'Quality Areas', ['classs' => 'control-label']) !!}
                            {!! Form::select('quality_area', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('criteria', 'Criteria Sections', ['classs' => 'control-label']) !!}
                            {!! Form::select('criteria', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('all_criteria', 'Include All Criteria Sections', ['classs' => 'control-label']) !!}
                            {!! Form::checkbox('all_criteria', 'y', request()->input('all_criteria') == 'y' ?:false) !!}
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-2">
                    {!! Form::submit('Search', ['class' => 'btn btn-sm btn-warning']) !!}
                </div>
                <div class="col-md-2">
                    <a href="{{ route('wda.school.assessment') . '?tb=' . request()->input('tb') ?:'i' }}"
                       class="btn btn-sm btn-success">Reset</a>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
        <div class="panel" id="resultListPanel">
            <div class="panel-heading">
                <ul class="nav nav-tabs" id="nav-tab">
                    <li role="presentation"
                        class="{{ request()->get('tb') == 'i' || ! request()->has('tb') ? 'active' : '' }}">
                        <a href="?tb=i">INPUT <span
                                    class="badge badge-primary">{{ $functionInput ? $functionInput->selfAssessmentStatus->count() : 0 }}</span>
                        </a>
                    </li>
                    <li role="presentation" class="{{ request()->get('tb') == 'p'? 'active' : '' }}">
                        <a href="?tb=p">PROCESS <span
                                    class="badge badge-success">{{ $functionProcess ? $functionProcess->selfAssessmentStatus->count() : 0 }}</span>
                        </a>
                    </li>
                    <li role="presentation" class="{{ request()->get('tb') == 'm'? 'active' : '' }}">
                        <a href="?tb=m">SCHOOL STATUS</a>
                    </li>
                </ul>
            </div>
            <div class="panel-body" style="overflow-x: auto">
                <div class="tab-content" id="myTabContent">
                    @if(request()->get('tb') == 'i' || ! request()->has('tb'))
                        <div id="input">
                            <table class="table table-bordered text-center" id="dataTableInput"
                                   style="width: 100%;">
                                <thead>
                                <tr>
                                    {{--<th>#</th>--}}
                                    <th data-priority="1">School Name</th>
                                    <th>Province</th>
                                    <th>District</th>
                                    <th>School Status</th>
                                    <th>School Type</th>
                                    <th>Function</th>
                                    {{--<th>School Scores</th>--}}
                                    <th>WDA Scores</th>
                                    {{--<th>District Scores</th>--}}
                                    {{--<th>Rating</th>--}}
                                    {{--<th>Stage/Status</th>--}}
                                    {{--<th>Accreditation</th>--}}
                                    <th data-priority="2">Button</th>
                                    @if($qualityInput)
                                        @foreach($qualityInput as $quality)
                                            @if (request()->input('quality_area'))
                                                @if (request()->input('quality_area') != $quality->id)
                                                    @continue
                                                @endif
                                            @endif
                                            <th class="text-primary">{{ $quality->name }}</th>
                                            @foreach ($quality->criteria as $criteria_section)
                                                @if(request()->input('all_criteria') != 'y' && !request()->input('criteria'))
                                                    @break
                                                @endif
                                                @if (request()->input('criteria'))
                                                    @if (request()->input('criteria') != $criteria_section->id)
                                                        @continue
                                                    @endif
                                                @endif
                                                <th class="bg-gray-light">{{ ucwords($criteria_section->criteria_section) }}</th>
                                            @endforeach
                                        @endforeach
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    @endif
                    @if(request()->get('tb') == 'p')
                        <div id="process">
                            <table class="table table-bordered text-center" id="dataTableProcess"
                                   style="width: 100%;">
                                <thead>
                                <tr>
                                    <th data-priority="1">School Name</th>
                                    <th>Province</th>
                                    <th>District</th>
                                    <th>School Status</th>
                                    <th>School Type</th>
                                    <th>Function</th>
                                    {{--<th>School Scores</th>--}}
                                    <th>WDA Scores</th>
                                    {{--<th>District Scores</th>--}}
                                    {{--<th>Rating</th>--}}
                                    {{--<th>Stage/Status</th>--}}
                                    {{--<th>Accreditation</th>--}}
                                    <th data-priority="2">Button</th>
                                    @if($qualityProcess)
                                        @foreach($qualityProcess as $quality)
                                            @if (request()->input('quality_area'))
                                                @if (request()->input('quality_area') != $quality->id)
                                                    @continue
                                                @endif
                                            @endif
                                            <th class="text-primary">{{ $quality->name }}</th>
                                            @foreach ($quality->criteria as $criteria_section)
                                                @if(request()->input('all_criteria') != 'y' && !request()->input('criteria'))
                                                    @break
                                                @endif
                                                @if (request()->input('criteria'))
                                                    @if (request()->input('criteria') != $criteria_section->id)
                                                        @continue
                                                    @endif
                                                @endif
                                                <th class="bg-gray-light">{{ ucwords($criteria_section->criteria_section) }}</th>
                                            @endforeach
                                        @endforeach
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    @endif
                    @if(request()->get('tb') == 'm')
                        <div id="status">
                            <table class="table table-bordered table-striped" id="dataTableSchoolsStatus" width="100%">
                                <thead>
                                <tr>
                                    <th>School</th>
                                    <th>Province</th>
                                    <th>District</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Input</th>
                                    <th>Process</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $(document).on('submit', '* #formDeleteAssessment', function (e) {
                $(this).append('{!! csrf_field() !!}');
                let msg = $(this).data('msg');
                if (confirm(msg) === true)
                    return true;
                return false;
            });
            $.get(
                "{{ route('wda.ajax.get.schools') }}",
                function (data) {
                    var html = "<option value='' selected>Select..</option>";
                    $.each(data, function (key, value) {
                        html += "<option value='" + key + "'>" + value + "</option>";
                    });
                    $("#nschools").html(html);
                }
            );
            $("#nschools").on('change', function () {
                let v = $(this).val();
                if (v.length > 0) {
                    $(".when-school-null")
                        .fadeOut(555);
                } else {
                    $(".when-school-null")
                        .fadeIn(555);
                }
            });
            $.get(
                "{{ route('wda.ajax.get.quality.area') }}?f={{ $ctb }}",
                function (data) {
                    var html = "<option value='' selected>Select..</option>";
                    $.each(data, function (key, value) {
                        html += "<option value='" + key + "'>" + value + "</option>";
                    });
                    $("#quality_area").html(html);
                }
            );
            $("#quality_area").on('change', function () {
                let f = $(this).val();
                $.get(
                    "{{ route('wda.get.manage.quality.ajax') }}?q=" + f,
                    function (data) {
                        var html = "<option value='' selected>Select..</option>";
                        $.each(data, function (key, value) {
                            html += "<option value='" + key + "'>" + value + "</option>";
                        });
                        $("#criteria").html(html);
                    }
                );
            });
            $("#criteria").on('change', function () {
                let f = $(this).val();
                $.get(
                    "{{ route('wda.get.manage.quality.ajax') }}?c=" + f,
                    function (data) {
                        var html = "<option value='' selected>Select..</option>";
                        $.each(data, function (key, value) {
                            html += "<option value='" + key + "'>" + value + "</option>";
                        });
                        $("#indicator").html(html);
                    }
                );
            });
        });
    </script>
    @if(request()->get('tb') == 'i' || ! request()->has('tb'))
        <script type="text/javascript">
            $(function () {
                let columns = [
                    // {
                    //     data: 'DT_Row_Index',
                    //     name: 'DT_Row_Index',
                    //     orderable: true,
                    //     searchable: true
                    // },
                    {
                        data: 'school_name',
                        name: 'accr_schools_information.school_name',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'province',
                        name: 'accr_schools_information.province',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'district',
                        name: 'accr_schools_information.district',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'school_status',
                        name: 'accr_schools_information.school_status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'school_type',
                        name: 'accr_source_school_type.school_type',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'name',
                        name: 'accr_functions.name',
                        orderable: true,
                        searchable: true
                    },
                    // {
                    //     data: 'school_scores',
                    //     name: 'school_scores',
                    //     orderable: true,
                    //     searchable: true
                    // },
                    {
                        data: 'wda_scores',
                        name: 'wda_scores',
                        orderable: true,
                        searchable: true
                    },
                    // {
                    //     data: 'district_scores',
                    //     name: 'district_scores',
                    //     orderable: true,
                    //     searchable: true
                    // },
                    // {
                    //     data: 'rating',
                    //     name: 'rating',
                    //     orderable: false,
                    //     searchable: false
                    // },
                    // {
                    //     data: 'stage',
                    //     name: 'stage',
                    //     orderable: false,
                    //     searchable: false
                    // },
                    // {
                    //     data: 'accreditation',
                    //     name: 'accreditation',
                    //     orderable: false,
                    //     searchable: false
                    // },
                    {
                        data: 'button',
                        name: 'button',
                        orderable: false,
                        searchable: false
                    },
                ];
                let urlI = "{!! $url !!}";
                let cols = [
                        @if($qualityInput)
                        @foreach($qualityInput as $quality)
                        @if (request()->input('quality_area'))
                        @if (request()->input('quality_area') != $quality->id)
                        @continue
                        @endif
                        @endif
                    {
                        data: "{{ strtolower(str_replace(' ', '_', $quality->name)) }}",
                        name: "{{ strtolower(str_replace(' ', '_', $quality->name)) }}",
                        orderable: false, searchable: false
                    }
                    ,
                        @foreach ($quality->criteria as $criteria_section)
                        @if(request()->input('all_criteria') != 'y' && !request()->input('criteria'))
                        @break
                        @endif
                        @if (request()->input('criteria'))
                        @if (request()->input('criteria') != $criteria_section->id)
                        @continue
                        @endif
                        @endif
                    {
                        data: "{{ strtolower(str_replace(' ', '_', $criteria_section->criteria_section)) }}",
                        name:
                            "{{ strtolower(str_replace(' ', '_', $criteria_section->criteria_section)) }}",
                        orderable:
                            false,
                        searchable:
                            false
                    }
                    ,
                    @endforeach
                    @endforeach
                    @endif
                ];
                let loading = "{{ asset('img/ajaxload.gif') }}";
                columns = columns.concat(cols);

                $("#dataTableInput").dataTable({
                    dom: '<"toolbar">Blfrtip',
                    serverSide: true,
                    processing: true,
                    ajax: {
                        "url": urlI,
                        data: function (data) {
                            for (var i = 0, len = data.columns.length; i < len; i++) {
                                if (!data.columns[i].search.value) delete data.columns[i].search;
                                if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                                if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                                if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                            }
                            delete data.search.regex;
                        }
                    },
                    pager: true,
                    "lengthMenu": [[100, 300, 600, -1], [100, 300, 600, "All"]],
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column'
                        }
                    ],
                    columnDefs: [
                        {
                            targets: [],
                            visible: false,
                        }
                    ],
                    columns: columns,
                    language: {
                        processing: '<span style="width:100%;"><img src="' + loading + '"></span>'
                    }
                });
                $("div.toolbar").html('<a href="{!! route('wda.school.export.assessment') . ($parms? '?' . $parms : '') !!}" target="_blank" class="btn btn-sm btn-warning mb-1"><i class="fa fa-download"></i>Export</a>');
            });
        </script>
    @elseif(request()->get('tb') == 'p')
        <script type="text/javascript">
            $(function () {
                let columns = [
                    // {
                    //     data: 'DT_Row_Index',
                    //     name: 'DT_Row_Index',
                    //     orderable: true,
                    //     searchable: true
                    // },
                    {
                        data: 'school_name',
                        name: 'accr_schools_information.school_name',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'province',
                        name: 'accr_schools_information.province',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'district',
                        name: 'accr_schools_information.district',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'school_status',
                        name: 'accr_schools_information.school_status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'school_type',
                        name: 'accr_source_school_type.school_type',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'name',
                        name: 'accr_functions.name',
                        orderable: true,
                        searchable: true
                    },
                    // {
                    //     data: 'school_scores',
                    //     name: 'school_scores',
                    //     orderable: true,
                    //     searchable: true
                    // },
                    {
                        data: 'wda_scores',
                        name: 'wda_scores',
                        orderable: true,
                        searchable: true
                    },
                    // {
                    //     data: 'district_scores',
                    //     name: 'district_scores',
                    //     orderable: true,
                    //     searchable: true
                    // },
                    // {
                    //     data: 'rating',
                    //     name: 'rating',
                    //     orderable: false,
                    //     searchable: false
                    // },
                    // {
                    //     data: 'stage',
                    //     name: 'stage',
                    //     orderable: false,
                    //     searchable: false
                    // },
                    // {
                    //     data: 'accreditation',
                    //     name: 'accreditation',
                    //     orderable: false,
                    //     searchable: false
                    // },
                    {
                        data: 'button',
                        name: 'button',
                        orderable: false,
                        searchable: false
                    },
                ];
                let urlI = "{!! $url !!}";
                let cols = [
                        @if($qualityProcess)
                        @foreach($qualityProcess as $quality)
                        @if (request()->input('quality_area'))
                        @if (request()->input('quality_area') != $quality->id)
                        @continue
                        @endif
                        @endif
                    {
                        data: "{{ strtolower(str_replace(' ', '_', $quality->name)) }}",
                        name: "{{ strtolower(str_replace(' ', '_', $quality->name)) }}",
                        orderable: false,
                        searchable: false
                    },
                        @foreach ($quality->criteria as $criteria_section)
                        @if(request()->input('all_criteria') != 'y' && !request()->input('criteria'))
                        @break
                        @endif
                        @if (request()->input('criteria'))
                        @if (request()->input('criteria') != $criteria_section->id)
                        @continue
                        @endif
                        @endif
                    {
                        data: "{{ strtolower(str_replace(' ', '_', $criteria_section->criteria_section)) }}",
                        name: "{{ strtolower(str_replace(' ', '_', $criteria_section->criteria_section)) }}",
                        orderable: false,
                        searchable: false
                    },
                    @endforeach
                    @endforeach
                    @endif
                ];
                let loading = "{{ asset('img/ajaxload.gif') }}";
                columns = columns.concat(cols);
                $("#dataTableProcess").dataTable({
                    dom: '<"toolbar">Blfrtip',
                    serverSide: true,
                    processing: true,
                    ajax: {
                        "url": urlI,
                        data: function (data) {
                            for (var i = 0, len = data.columns.length; i < len; i++) {
                                if (!data.columns[i].search.value) delete data.columns[i].search;
                                if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                                if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                                if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                            }
                            delete data.search.regex;
                        }
                    },
                    pager: true,
                    "lengthMenu": [[100, 300, 600, -1], [100, 300, 600, "All"]],
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column'
                        }
                    ],
                    columnDefs: [
                        {
                            targets: [],
                            visible: false,
                        }
                    ],
                    columns: columns,
                    language: {
                        processing: '<span style="width:100%;"><img src="' + loading + '"></span>'
                    }
                });
                $("div.toolbar").html('<a href="{!! route('wda.school.export.assessment') . ($parms? '?' . $parms : '') !!}" target="_blank" class="btn btn-sm btn-warning mb-1"><i class="fa fa-download"></i>Export</a>');
            });
        </script>
    @elseif(request()->get('tb') == 'm')
        <script type="text/javascript">
            $(function () {
                let columns = [
                    {
                        data: 'school_name',
                        name: 'accr_schools_information.school_name',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'province',
                        name: 'accr_schools_information.province',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'district',
                        name: 'accr_schools_information.district',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'school_status',
                        name: 'accr_schools_information.school_status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'school_type',
                        name: 'accr_source_school_type.school_type',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'function_input',
                        name: 'function_input',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'function_process',
                        name: 'function_process',
                        orderable: true,
                        searchable: true
                    }
                ];
                let urlI = "{!! $url !!}";
                let loading = "{{ asset('img/ajaxload.gif') }}";
                $("#dataTableSchoolsStatus").dataTable({
                    dom: 'Blfrtip',
                    serverSide: true,
                    processing: true,
                    ajax: {
                        "url": urlI,
                        data: function (data) {
                            for (var i = 0, len = data.columns.length; i < len; i++) {
                                if (!data.columns[i].search.value) delete data.columns[i].search;
                                if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                                if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                                if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                            }
                            delete data.search.regex;
                        }
                    },
                    pager: true,
                    "lengthMenu": [[100, 300, 600, -1], [100, 300, 600, "All"]],
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            collectionLayout: 'fixed two-column'
                        }
                    ],
                    columnDefs: [
                        {
                            targets: [],
                            visible: false,
                        }
                    ],
                    columns: columns,
                    language: {
                        processing: '<span style="width:100%;"><img src="' + loading + '"></span>'
                    }
                });
            });
        </script>
    @endif
@endsection