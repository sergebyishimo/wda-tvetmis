@extends('wda.layout.main')

@section('panel-title', ucwords($school->school_name))

@section('htmlheader_title', ucwords($school->school_name))

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-2">
                            {!! Form::open([ 'class' => 'form-horizontal']) !!}
                            {!! Form::label('submit_id', 'Select Date', ['class' => 'label-control']) !!}
                            {!! Form::select('submit_id', $selectDate, $firstNumberId, ['class' => 'form-control submit-date select2']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 pull-right">
                            <a href="{{ url()->previous() }}" class="btn btn-warning pull-right">
                                <i class="fa fa-list"></i>&nbsp;Return Back
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @php($c_indicators=0)
                    @php($c_answers=0)
                    @foreach($qualities as $quality)
                        {!! Form::open(['route' => ['wda.store.school.assessment', $funct->id, $school->id]]) !!}
                        {!! Form::hidden('school_id', $school->id) !!}
                        {!! Form::hidden('function_id', $funct->id) !!}
                        {!! Form::hidden('quality_id', $quality->id) !!}
                        <table class="table table-bordered">
                            <thead class="bg-gray">
                            <tr>
                                <th>Quality Area</th>
                                <th class="text-left" width="200px">Criteria</th>
                                <th class="text-left">Indicator</th>
                                <th class="text-right pr-4">Answers</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="bg-gray-light">
                                    <div>{{ ucwords($quality->name) }}</div>
                                    <div class="p-1 label text-sm label-info rounded shadow-sm">Scores:
                                        {{ getQualityAreaScores($quality->criteria , $school, $sub) }}
                                        / {{ getTotalWeightForQualityAreas($quality->criteria) }}</div>
                                </td>
                                <td colspan="3">
                                    <table class="table">

                                        @foreach ($quality->criteria as $criteria_section)
                                            <tr>
                                                <td class="bg-gray-light"
                                                    width="188px">
                                                    <div>{{ ucwords($criteria_section->criteria_section)  }}</div>
                                                    <div class="p-1 label text-sm label-info rounded shadow-sm">
                                                        Scores:
                                                        {{ getCriteriaScores($criteria_section['criterias'], $school, $sub) }}
                                                        / {{ $criteria_section->criterias->sum('weight') }}</div>
                                                </td>
                                                <td>
                                                    <table class="table table-bordered">
                                                        @foreach ($criteria_section['criterias'] as $criteria)
                                                            @php($c_indicators += 1)
                                                            <thead>
                                                            <th class="text-right bg-gray-light p-0 pr-1"
                                                                colspan="3">
                                                                District
                                                            </th>
                                                            <th class="text-right bg-gray-light p-0 pr-2"
                                                                colspan="3">
                                                                Verified
                                                            </th>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td width="430px">
                                                                    @php($weight= strtolower($criteria->getSchoolAnswer($school, $sub)) == 'yes'? $criteria->weight : 0 )
                                                                    <div>{{ ucwords($criteria->criteria) }}</div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="p-1 label text-sm label-info rounded shadow-sm">
                                                                                Scores: {{ $weight }}
                                                                                / {{ $criteria->weight }}</div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="50px"
                                                                    class="text-center {{ $criteria->getSchoolAnswer($school, $sub) ? $criteria->getSchoolAnswer($school, $sub) == 'YES' ? 'bg-success' : 'bg-danger' : 'bg-warnings' }}">
                                                                    @if($criteria->getSchoolAnswer($school, $sub))
                                                                        @php($c_answers += 1)
                                                                        <div>{{ $criteria->getSchoolAnswer($school, $sub) }}</div>
                                                                    @else
                                                                        ---
                                                                    @endif
                                                                </td>
                                                                <td width="50px"
                                                                    class="text-center {{ $criteria->getDistrictComment($school->id, $sub) ? $criteria->getDistrictComment($school->id, $sub) == 'YES' ? 'bg-success' : 'bg-danger' : 'bg-warnings' }}">
                                                                    {{ $criteria->getDistrictComment($school->id, $sub) }}
                                                                </td>
                                                                <td align="right"
                                                                    class="{{ $criteria->getWdaComment($school->id, $sub) ? $criteria->getWdaComment($school->id, $sub) == 'YES' ? 'bg-success' : 'bg-danger' : 'bg-warnings' }}">
                                                                    <div class="form-group">
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio"
                                                                                   id="customRadioY{{ $criteria->id }}"
                                                                                   name="{{ 'wda['.$criteria->id.']' }}"
                                                                                   value="YES"
                                                                                   required
                                                                                   @if(old('wda['.$criteria->id.']'))
                                                                                   {{ old('wda['.$criteria->id.']') == 'YES' ? 'checked' : '' }}
                                                                                   @else
                                                                                   {{ $criteria->getWdaComment($school->id, $sub) == 'YES' ? 'checked' : '' }}
                                                                                   @endif
                                                                                   class="custom-control-input">

                                                                            <label class="custom-control-label"
                                                                                   style="cursor: pointer"
                                                                                   for="customRadioY{{ $criteria->id }}">YES</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio"
                                                                                   id="customRadioN{{ $criteria->id }}"
                                                                                   name="{{ 'wda['.$criteria->id.']' }}"
                                                                                   value="NO"
                                                                                   @if(old('wda['.$criteria->id.']'))
                                                                                   {{ old('wda['.$criteria->id.']') == 'NO' ? 'checked' : '' }}
                                                                                   @else
                                                                                   {{ $criteria->getWdaComment($school->id, $sub) == 'NO' ? 'checked' : '' }}
                                                                                   @endif
                                                                                   required
                                                                                   class="custom-control-input">

                                                                            <label class="custom-control-label"
                                                                                   style="cursor: pointer"
                                                                                   for="customRadioN{{ $criteria->id }}">NO</label>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        @endforeach
                                                    </table>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                            <tr class="bg-dark">
                                <td colspan="4">
                                    <div>
                                        <b>{{ ucwords($quality->name) }}</b>
                                        <span class="pull-right p-1 text-sm label-info rounded shadow-sm">
                                            <i>
                                                <b>{{ getQualityAreaScores($quality->criteria , $school, $sub) }}
                                                    /{{ getTotalWeightForQualityAreas($quality->criteria) }}</b>
                                            </i>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="p-0 bg-gray ">
                                <td colspan="4" class="p-0 pl-2 text-center">District Comments</td>
                            </tr>
                            <tr class="p-0 bg-gray ">
                                <td class="p-0 pl-2">Strength</td>
                                <td class="p-0 pl-2">Weakness</td>
                                <td class="p-0 pl-2">Recommendation</td>
                                <td class="p-0 pl-2">Time Line</td>
                            </tr>
                            <tr class="bg-gray-light">
                                <td>
                                    <div class="form-group">
                                        {!! getCommentsDistrict($school->id,$funct->id,$quality->id, 'strength', false, $sub) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! getCommentsDistrict($school->id,$funct->id,$quality->id, 'weakness', false, $sub) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! getCommentsDistrict($school->id,$funct->id,$quality->id, 'recommendation', false, $sub) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! getCommentsDistrict($school->id, $funct->id, $quality->id, 'timeline', false, $sub) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="p-0 bg-dark-gradient ">
                                <td colspan="4" class="p-0 pl-2 text-center">WDA Comments</td>
                            </tr>
                            <tr class="bg-dark">
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('strength'.$quality->id, "Strength", ['class' => 'label-control']) !!}
                                        {!! Form::textarea('strength', getCommentsWda($school->id,$funct->id,$quality->id, 'strength', false, $sub), [
                                        'class' => 'form-control', 'id' => 'strength',
                                        'required' => true, 'rows' => 3 ]) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('weakness'.$quality->id, "Weakness", ['class' => 'label-control']) !!}
                                        {!! Form::textarea('weakness', getCommentsWda($school->id,$funct->id,$quality->id, 'weakness', false, $sub), [
                                        'class' => 'form-control', 'id' => 'weakness'.$quality->id,
                                        'required' => true,
                                         'rows' => '3']) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('recommendation'.$quality->id, "Recommendation", ['class' => 'label-control']) !!}
                                        {!! Form::textarea('recommendation', getCommentsWda($school->id,$funct->id,$quality->id, 'recommendation', false, $sub), [
                                        'class' => 'form-control', 'id' => 'recommendation'.$quality->id,
                                        'required' => true, 'rows' => 3 ]) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('timeline'.$quality->id, "Time Line", ['class' => 'label-control']) !!}
                                        {!! Form::textarea('timeline', getCommentsWda($school->id, $funct->id, $quality->id, 'timeline', false, $sub), ['id' => 'timeline'.$quality->id,'class' => 'form-control', 'required' => true, 'rows' => '3' ]) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr class="bg-dark">
                                <td colspan="4">
                                    {!! Form::submit('Save Decision', ['class' => 'btn btn-block btn-primary btn-md']) !!}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                    @endforeach
                </div>
                <div class="card-footer">
                    @if($funct->name == ucwords('Process'))
                        <div class="row">
                            {!! Form::open(['method' => 'GET']) !!}
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('analyzed_implications', "Analyzed Implications", ['class' => 'label-control']) !!}
                                    {!! Form::textarea('analyzed_implications', null, [
                                    'class' => 'form-control',
                                    'required' => true, 'rows' => 3 ]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('strategic_recommendations', "Strategic Recommendations", ['class' => 'label-control']) !!}
                                    {!! Form::textarea('strategic_recommendations', null, [
                                    'class' => 'form-control',
                                    'required' => true, 'rows' => 3 ]) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="button"
                                                   name="fail"
                                                   value="Fail"
                                                   class="btn btn-warning pull-left">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="button"
                                                   name="pass"
                                                   value="Pass"
                                                   class="btn btn-success pull-right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    @elseif($funct->name == ucwords('Input'))
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['method' => 'GET']) !!}
                                <div class="form-group">
                                    {!! Form::label('general', "General Recommendation", ['class' => 'label-control']) !!}
                                    {!! Form::textarea('general', null, [
                                    'class' => 'form-control',
                                    'required' => true, 'rows' => 3 ]) !!}
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="button"
                                                   name="reject"
                                                   value="Not Accredited"
                                                   class="btn btn-warning pull-left">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="button"
                                                   name="accepted"
                                                   value="Accredited"
                                                   class="btn btn-success pull-right">
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $(".submit-date").on('change', function () {
                let v = $(this).val();
                window.location = "/qm-manual/wda/school/assessment/{{ $funct->id }}/{{ $school->id }}/" + v;
            });
        });
    </script>
@endsection