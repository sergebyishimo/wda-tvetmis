<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Strength</label>
            <p>{!!  $comments->strength or "" !!}</p>
        </div>
        <div class="form-group">
            <label>Weakness</label>
            <p>{!! $comments->weakness or ""  !!}</p>
        </div>
        <div class="form-group">
            <label>Opportunity</label>
            <p>{!! $comments->opportunity or ""  !!}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Threat</label>
            <p>{!! $comments->threat or ""  !!}</p>
        </div>
        <div class="form-group">
            <label>Recommendation</label>
            <p>{!! $comments->recommendation or "" !!} </p>
        </div>
        <div class="form-group">
            <label>Marks</label>
            <p>
                {!! $comments->marks  or ""  !!}
            </p>
        </div>
    </div>

</div>