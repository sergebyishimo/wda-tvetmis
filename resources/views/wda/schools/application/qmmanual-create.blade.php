@extends('wda.layout.main')

@section('panel-title', "QM-Manual Source")

@section('htmlheader_title', "QM-Manual Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Procedures Entry</h3>
                </div>
                {!! Form::open(['route' => 'wda.procedures.store', 'files' => true]) !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::textarea('description', $procedure ? $procedure->description : "", ['class' => 'form-control summernote', 'required' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::file('attachment', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-md']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--END Procedures--}}
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Accreditation Types Entry</h3>
                </div>
                {!! Form::open(['route' => 'wda.accreditation-types.store', 'files' => true ]) !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::textarea('description', $type ? $type->description : "", ['class' => 'form-control summernote', 'required' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::file('attachment', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-md']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--END Accreditation Types--}}
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Responsibilities Entry</h3>
                </div>
                {!! Form::open(['route' => 'wda.responsibilities.store']) !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::textarea('description', $responsibility ? $responsibility->description : "", ['class' => 'form-control summernote', 'required' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::file('attachment', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-md']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--END Responsibilities--}}
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Timelines Entry</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open(['route' => 'wda.timelines.store']) !!}
                            @php($autofoucs = false)
                            @isset($rekod)
                                @php($autofoucs = true)
                                {!! Form::hidden('id', $rekod->id) !!}
                            @endisset
                            <table class="table table-bordered table-condensed">
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            {!! Form::label('activity', "Activity", ['class' => 'label-control']) !!}
                                            {!! Form::text('activity', $rekod ? $rekod->activity : null, ['class' => 'form-control', 'autofocus' => $autofoucs]) !!}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            {!! Form::label('date_from', "Date From", ['class' => 'label-control']) !!}
                                            {!! Form::text('date_from', $rekod ? $rekod->date_from : null, ['class' => 'form-control datepicker']) !!}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            {!! Form::label('date_to', "Date To", ['class' => 'label-control']) !!}
                                            {!! Form::text('date_to', $rekod ? $rekod->date_to : null, ['class' => 'form-control datepicker']) !!}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            {!! Form::label('responsible', "Responsible", ['class' => 'label-control']) !!}
                                            {!! Form::text('responsible', $rekod ? $rekod->responsible : null, ['class' => 'form-control']) !!}
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::submit('Save', ['class' => 'btn btn-block btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped" id="dataTable">
                                <thead>
                                <th class="text-center">Activity</th>
                                <th class="text-center">Date From</th>
                                <th class="text-center">Date To</th>
                                <th class="text-center">Responsible</th>
                                <th></th>
                                </thead>
                                <tbody>
                                @if($timelines)
                                    @foreach($timelines as $timeline)
                                        <tr>
                                            <td class="text-center">{!! $timeline->activity !!}</td>
                                            <td class="text-center">{!! date("F d, Y", strtotime($timeline->date_from)) !!}</td>
                                            <td class="text-center">{!! date("F d, Y", strtotime($timeline->date_to)) !!}</td>
                                            <td class="text-center">{!! $timeline->responsible !!}</td>
                                            <td>
                                                <a href="{{ route('wda.edit.timeline', $timeline->id) }}"
                                                   class="btn btn-sm btn-success pull-right">Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--END Timelines--}}
@endsection

@section('l-style')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Text Goes here ...',
            tabsize: 2,
            height: 200
        });
    </script>
@endsection