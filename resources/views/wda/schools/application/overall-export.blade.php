<table border="1" cellspacing="0" cellpadding="3">
    <thead class="bg-light">
    <tr>
        <th>Quality Area</th>
        <th>Criteria Section</th>
        <th>Indicator</th>
        <th colspan="2">Overall</th>
        <th colspan="6">School Status</th>
        @if($provinces->count())
            @foreach($provinces as $key => $province)
                <th colspan="{{ $province->count() * 2 }}">{{ ucwords($key) }}</th>
            @endforeach
        @endif
    </tr>
    <tr>
        <th align="center">&nbsp;</th>
        <th align="center">&nbsp;</th>
        <th align="center">&nbsp;</th>
        <th align="center">&nbsp;</th>
        <th align="center">&nbsp;</th>
        <th align="center" colspan="2">GOV</th>
        <th align="center" colspan="2">PRV</th>
        <th align="center" colspan="2">PUB</th>
        @if($provinces->count())
            @foreach($provinces as $key => $province)
                @foreach($province as $district)
                    <th align="center" colspan="2">{{ ucwords($district->District) }}</th>
                @endforeach
            @endforeach
        @endif
    </tr>
    <tr>
        <th align="center">&nbsp;</th>
        <th align="center">&nbsp;</th>
        <th align="center">&nbsp;</th>
        <th align="center">YES</th>
        <th align="center">NO</th>
        <th align="center">YES</th>
        <th align="center">NO</th>
        <th align="center">YES</th>
        <th align="center">NO</th>
        <th align="center">YES</th>
        <th align="center">NO</th>
        @if($provinces->count())
            @foreach($provinces as $key => $province)
                @foreach($province as $district)
                    <th align="center">YES</th>
                    <th align="center">NO</th>
                @endforeach
            @endforeach
        @endif
    </tr>
    </thead>
    <tbody>
    @php
        $f = 0;
        #$q = $_function->qualityAreas->count() + 1;
        $q = 1000;
        $temp = "";
        if (request()->input('quality_area'))
            $qualityArea = $_function->qualityAreas()->where('id', request()->input('quality_area'))->get();
        else
            $qualityArea = $_function->qualityAreas;
    @endphp
    @foreach($qualityArea as $quality)
        @php
            $ctemp = "";
            if (request()->input('criteria'))
                $criterias = $quality->criteria()->where('id', request()->input('criteria'))->get();
            else
                $criterias = $quality->criteria;
        @endphp
        @foreach($criterias as $criteria)
            @php
                if (request()->input('indicator'))
                    $criterials = $criteria->criterias()->where('id', request()->input('indicator'))->get();
                else
                    $criterials = $criteria->criterias;
            @endphp
            @foreach($criterials as $criteria_)
                <tr>
                    <td valign="middle"
                        align="center">
                        @if($temp != $quality->id)
                            {{ $quality->name }}
                        @endif
                    </td>
                    <td>
                        @if($ctemp != $criteria->id)
                            {{ $criteria->criteria_section }}
                        @endif
                    </td>
                    <td valign="middle"
                        align="center">
                        {{ $criteria_->criteria }}
                    </td>
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "all", "YES") !!}
                    </td>
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "all", "NO") !!}
                    </td>
                    {{--//--}}
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "gov", "YES") !!}
                    </td>
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "gov", "NO") !!}
                    </td>
                    {{--//--}}
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "prv", "YES") !!}
                    </td>
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "prv", "NO") !!}
                    </td>
                    {{--//--}}
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "pub", "YES") !!}
                    </td>
                    <td width="10px">
                        {!! getPercentageForIndicator($criteria_->id, "pub", "NO") !!}
                    </td>
                    @if($provinces->count())
                        @foreach($provinces as $key => $province)
                            @foreach($province as $district)
                                <td width="10px">
                                    {!! getPercentageForIndicator($criteria_->id, $district->District, "YES", $district->District) !!}
                                </td>
                                <td width="10px">
                                    {!! getPercentageForIndicator($criteria_->id, $district->District, "NO", $district->District) !!}
                                </td>
                            @endforeach
                        @endforeach
                    @endif
                </tr>
                @php
                    $ctemp = $criteria->id;
                    $temp = $quality->id;
                @endphp
            @endforeach
        @endforeach
    @endforeach
    </tbody>
</table>