@extends('wda.layout.main')

@section('panel-title', "Quality Audit Indicatorwise Overall report")

@section('htmlheader_title', "Quality Audit Indicatorwise Overall report")
@section('l-style')
    @parent
    <style>
        #displayResults #loading {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            display: block;
            opacity: 0.7;
            background-color: #fff;
            z-index: 99;
            text-align: center;
        }

        #loading-image {
            position: absolute;
            top: 300px;
            left: 680px;
            z-index: 100;
        }
    </style>
@endsection
@section('panel-body')
    <div class="container-fluid">
        <div class="panel panel-body">
            {!! Form::open(['method' => 'GET']) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Province</label>
                        <select class="form-control flat" id="nprovince" name="province">
                            <option value="">Choose Here</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('function', 'Function', ['classs' => 'control-label']) !!}
                        {!! Form::select('function', $functions, null, ['class' => 'form-control select2', 'placeholder' => 'Select here ..', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('quality_area', 'Quality Areas', ['classs' => 'control-label']) !!}
                        {!! Form::select('quality_area', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('criteria', 'Criteria Sections', ['classs' => 'control-label']) !!}
                        {!! Form::select('criteria', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('indicator', 'Indicator', ['classs' => 'control-label']) !!}
                        {!! Form::select('indicator', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {!! Form::submit('Search', ['class' => 'btn btn-sm btn-warning']) !!}
                </div>
                <div class="col-md-2 pull-right">
                    <a href="{{ route('wda.audit.overall.report') }}"
                       class="btn btn-sm btn-danger pull-right">
                        <i class="fa fa-crosshairs"></i>&nbsp;Reset
                    </a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @isset($_function)
            <div class="panel" id="displayResults">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-md btn-danger" type="submit">Print</button>
                            <a class="btn btn-md btn-warning"
                               href="{{ route('wda.audit.overall.report.export') }}{!! $parms !!}"
                               target="_blank">Export</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="overflow-x: auto;">
                    <table class="table table-bordered" border="1" id="resultTable">
                        <thead class="bg-light">
                        <tr>
                            <th rowspan="3" style="vertical-align: middle;" class="text-center">Function</th>
                            <th rowspan="3" style="vertical-align: middle;" class="text-center">Quality Area</th>
                            <th rowspan="3" style="vertical-align: middle;" class="text-center">Criteria Section</th>
                            <th rowspan="3" style="vertical-align: middle;" class="text-center" width="400px">
                                Indicator
                            </th>
                            <th rowspan="2" colspan="2" class="text-center" style="vertical-align: middle;">Overall</th>
                            <th colspan="6" class="text-center" style="vertical-align: middle;">School Status</th>
                            @if($provinces->count())
                                @foreach($provinces as $key => $province)
                                    <th colspan="{{ $province->count() * 2 }}" class="text-center"
                                        style="vertical-align: middle;">{{ ucwords($key) }}</th>
                                @endforeach
                            @endif
                        </tr>
                        <tr>
                            <th class="text-center" colspan="2">GOV</th>
                            <th class="text-center" colspan="2">PRV</th>
                            <th class="text-center" colspan="2">PUB</th>
                            @if($provinces->count())
                                @foreach($provinces as $key => $province)
                                    @foreach($province as $district)
                                        <th class="text-center" colspan="2">{{ ucwords($district->District) }}</th>
                                    @endforeach
                                @endforeach
                            @endif
                        </tr>
                        <tr>
                            <th class="text-center">YES</th>
                            <th class="text-center">NO</th>
                            <th class="text-center">YES</th>
                            <th class="text-center">NO</th>
                            <th class="text-center">YES</th>
                            <th class="text-center">NO</th>
                            <th class="text-center">YES</th>
                            <th class="text-center">NO</th>
                            @if($provinces->count())
                                @foreach($provinces as $key => $province)
                                    @foreach($province as $district)
                                        <th class="text-center">YES</th>
                                        <th class="text-center">NO</th>
                                    @endforeach
                                @endforeach
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $f = 0;
                            #$q = $_function->qualityAreas->count() + 1;
                            $q = 1000;
                            $temp = "";
                            if (request()->input('quality_area'))
                                $qualityArea = $_function->qualityAreas()->where('id', request()->input('quality_area'))->get();
                            else
                                $qualityArea = $_function->qualityAreas;
                        @endphp

                        <tr>
                            <td rowspan="{{ $q }}" style="vertical-align: middle;"
                                class="text-center">{{ $_function->name }}</td>
                        </tr>
                        @foreach($qualityArea as $quality)
                            @php
                                $ctemp = "";
                                $s = countCriteriaFromQualityArea($quality->id) + 10;
                                if (request()->input('criteria'))
                                    $criterias = $quality->criteria()->where('id', request()->input('criteria'))->get();
                                else
                                    $criterias = $quality->criteria;
                            @endphp
                            <tr>
                                {{--@if($temp != $quality->id)--}}
                                <td rowspan="{{ $s }}" style="vertical-align: middle;"
                                    class="text-center">{{ $quality->name }}</td>
                                {{--@endif--}}
                            </tr>
                            @foreach($criterias as $criteria)
                                @php
                                    $ctemp = "";
                                    $s = countCriteriaFromQualityArea($quality->id) + 1;
                                    if (request()->input('indicator'))
                                        $criterials = $criteria->criterias()->where('id', request()->input('indicator'))->get();
                                    else
                                        $criterials = $criteria->criterias;
                                @endphp
                                <tr>
                                    {{--@if($ctemp != $criteria->id)--}}
                                    <td style="vertical-align: middle;"
                                        rowspan="{{ countCriteriaFromCriteriaSection($criteria->id) + 1 }}"
                                        class="text-center">{{ $criteria->criteria_section }}</td>
                                    {{--@endif--}}
                                </tr>
                                @foreach($criterials as $criteria_)
                                    <tr>
                                        <td style="vertical-align: middle;"
                                            class="text-center">
                                            {{ $criteria_->criteria }}
                                        </td>
                                        <td width="100px"
                                            data-indicator="{{ $criteria_->id }}" data-condition="all"
                                            data-needed="YES"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-all-YES"></td>
                                        <td width="100px"
                                            data-indicator="{{ $criteria_->id }}" data-condition="all"
                                            data-needed="NO"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-all-NO"></td>
                                        {{--//--}}
                                        <td width="100px"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-gov-YES"
                                            data-indicator="{{ $criteria_->id }}" data-condition="gov"
                                            data-needed="YES"></td>
                                        <td width="100px"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-gov-NO"
                                            data-indicator="{{ $criteria_->id }}" data-condition="gov"
                                            data-needed="NO"></td>
                                        {{--//--}}
                                        <td width="100px"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-prv-YES"
                                            data-indicator="{{ $criteria_->id }}" data-condition="prv"
                                            data-needed="YES"></td>
                                        <td width="100px"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-prv-NO"
                                            data-indicator="{{ $criteria_->id }}" data-condition="prv"
                                            data-needed="NO"></td>
                                        {{--//--}}
                                        <td width="100px"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-pub-YES"
                                            data-indicator="{{ $criteria_->id }}" data-condition="pub"
                                            data-needed="YES"></td>
                                        <td width="100px"
                                            class="text-center cell-status cell-status-{{ $criteria_->id }}-pub-NO"
                                            data-indicator="{{ $criteria_->id }}" data-condition="pub"
                                            data-needed="NO"></td>
                                        @if($provinces->count())
                                            @foreach($provinces as $key => $province)
                                                @foreach($province as $district)
                                                    <td class="text-center cell-status cell-status-{{ $criteria_->id }}-{{ $district->District }}-YES"
                                                        data-indicator="{{ $criteria_->id }}"
                                                        data-condition="{{ $district->District }}"
                                                        data-needed="YES"
                                                        data-district="{{ $district->District }}"></td>
                                                    <td class="text-center cell-status cell-status-{{ $criteria_->id }}-{{ $district->District }}-NO"
                                                        data-indicator="{{ $criteria_->id }}"
                                                        data-condition="{{ $district->District }}"
                                                        data-needed="NO" data-district="{{ $district->District }}"></td>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </tr>
                                @endforeach
                                @php
                                    $ctemp = $criteria->id;
                                @endphp
                            @endforeach
                            @php
                                $temp = $quality->id;
                            @endphp
                        @endforeach
                        </tbody>
                    </table>
                    <div id="loading">
                        <img id="loading-image" src="{{ asset('img/ajaxload.gif') }}" alt="Loading..."/>
                    </div>
                </div>
            </div>
        @endisset
    </div>
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $("#function").on('change', function () {
                let f = $(this).val();
                $.get(
                    "{{ route('wda.get.manage.quality.ajax') }}?f=" + f,
                    function (data) {
                        var html = "<option value='' selected>Select..</option>";
                        $.each(data, function (key, value) {
                            html += "<option value='" + key + "'>" + value + "</option>";
                        });
                        $("#quality_area").html(html);
                    }
                );
            });
            $("#quality_area").on('change', function () {
                let f = $(this).val();
                $.get(
                    "{{ route('wda.get.manage.quality.ajax') }}?q=" + f,
                    function (data) {
                        var html = "<option value='' selected>Select..</option>";
                        $.each(data, function (key, value) {
                            html += "<option value='" + key + "'>" + value + "</option>";
                        });
                        $("#criteria").html(html);
                    }
                );
            });
            $("#criteria").on('change', function () {
                let f = $(this).val();
                $.get(
                    "{{ route('wda.get.manage.quality.ajax') }}?c=" + f,
                    function (data) {
                        var html = "<option value='' selected>Select..</option>";
                        $.each(data, function (key, value) {
                            html += "<option value='" + key + "'>" + value + "</option>";
                        });
                        $("#indicator").html(html);
                    }
                );
            });
            let cells = $("* .cell-status");
            let Ccount = cells.length;
            let o = 0;
            cells.each(function (i, e) {
                // if (i === 8) {
                let ele = document.getElementsByClassName("cell-status")[i];
                var x = ele.getAttribute("data-indicator");
                var y = ele.getAttribute("data-condition");
                var z = ele.getAttribute("data-needed");
                var d = ele.getAttribute('data-district');
                if (d === z)
                    var p = $(".cell-status-" + x + "-" + d + "-" + z);
                else
                    var p = $(".cell-status-" + x + "-" + y + "-" + z);
                $.get("{{ route('wda.audit.overall.report.get') }}", {
                    indicator: x,
                    cond: y,
                    needed: z,
                    district: d
                }, function (data) {
                    o += 1;
                    p.html(data);
                    if (o === Ccount)
                        $('#loading').hide();
                });
                // }
            });
            $('button[type="submit"]').click(function () {
                var pageTitle = 'Quality Audit Indicatorwise Overall report',
                    stylesheet = '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',
                    win = window.open('', 'Print', 'width=500,height=300');
                win.document.write('<html><head><title>' + pageTitle + '</title>' +
                    '<link rel="stylesheet" href="' + stylesheet + '">' +
                    '</head><body>' + $('.table')[0].outerHTML + '</body></html>');
                win.document.close();
                win.print();
                win.close();
                return false;
            });
        });
    </script>
@endsection