@extends('wda.layout.main')

@section('panel-title', "Query Answers")

@section('htmlheader_title', "Query Answers")

@section('panel-body')
    <div class="container-fluid">
        <div class="panel panel-body">
            {!! Form::open(['method' => 'GET']) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Year of Assessment</label>
                        {!! Form::select('year', academicYear(null, null, true) , null ) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Schools</label>
                        <select class="form-control flat" id="nschools" name="school">
                            <option value="">Choose Here</option>
                        </select>
                    </div>
                </div>
                <div class="when-school-null">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Province</label>
                            <select class="form-control flat" id="nprovince" name="province">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>District</label>
                            <select class="form-control flat" id="ndistrict" name="district">
                                <option value="">Choose Here</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">School Type:</label>
                            <select name="school_type" class="form-control flat select2">
                                <option value="">Choose Here</option>
                                @foreach ($schoolTypes as $type)
                                    <option value="{{ $type->id  }}">{{ $type->school_type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">School Status:</label>
                            <select class="form-control flat" name="school_status">
                                <option value="">Choose Here</option>
                                <option>Public</option>
                                <option>Government Aid</option>
                                <option>Private</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('functions', 'Function', ['classs' => 'control-label']) !!}
                        {!! Form::select('functions', $functions, null, ['class' => 'form-control select2', 'placeholder' => 'Select here ..', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('quality_area', 'Quality Areas', ['classs' => 'control-label']) !!}
                        {!! Form::select('quality_area', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('criteria', 'Criteria Sections', ['classs' => 'control-label']) !!}
                        {!! Form::select('criteria', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('indicator', 'Indicator', ['classs' => 'control-label']) !!}
                        {!! Form::select('indicator', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    {!! Form::submit('Search', ['class' => 'btn btn-sm btn-warning']) !!}
                </div>
                <div class="col-md-2 pull-right">
                    <a href="{{ route('wda.schools.assessment.answers') }}"
                       class="btn btn-sm btn-danger pull-right">
                        <i class="fa fa-crosshairs"></i>&nbsp;Reset
                    </a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @if(isset($schools) && isset($indicators))
            <div class="panel">
                <div class="panel-body" style="overflow-x: auto">
                    <table class="table table-bordered table-responsive table-striped" id="dataTableBtnAnswers">
                        <thead class="bg-gray">
                        <tr>
                            <th>Function</th>
                            <th>Quality Area</th>
                            <th>Criteria Sections</th>
                            <th>Indicator</th>
                            @if($schools->count() > 0)
                                @foreach($schools as $school)
                                    <th class="text-center">{{ ucwords($school->school_name) }}</th>
                                @endforeach
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if($indicators->count() > 0)
                            @php
                                $tmpF = '';
                                $tmpQ = '';
                                $tmpI = '';
                            @endphp
                            @foreach($indicators as $indicator)
                                <tr>
                                    <td class="bg-gray-light"
                                        width="100px">
                                        {{ $tmpF != $indicator->function_name || $tmpF == '' ? $indicator->function_name : "" }}
                                    </td>
                                    <td class="bg-gray-light"
                                        style="border-left: solid 2px #fff;
                                        @if($tmpQ != $indicator->quality_areas_name)
                                                border-top: solid 2px green;
                                        @endif"
                                        width="150px">
                                        {{ $tmpQ != $indicator->quality_areas_name || $tmpQ == '' ? $indicator->quality_areas_name : "" }}
                                    </td>
                                    <td class="bg-gray-light"
                                        style="border-left: solid 2px green;
                                        @if($tmpI != $indicator->criteria_section)
                                                border-top: solid 2px green;
                                        @endif"
                                        width="150px">
                                        @if($tmpI != $indicator->criteria_section)
                                            {!! $indicator->criteria_section !!}
                                        @endif
                                    </td>
                                    <td width="350px"
                                        style="@if($tmpI != $indicator->criteria_section)
                                                border-top: solid 2px green;
                                        @endif">
                                        {{ $indicator->criteria }}
                                    </td>
                                    @if($schools->count() > 0)
                                        @foreach($schools as $school)
                                            <td style="@if($tmpI != $indicator->criteria_section)
                                                    border-top: solid 2px green;
                                            @endif">
                                                @php
                                                    $s = $indicator->getSchoolAnswer($school->id, null, request()->input('year'));
                                                    $d = $indicator->getDistrictAnswer($school->id, null, request()->input('year'));
                                                    $w = $indicator->getWdaAnswer($school->id, null, request()->input('year'));
                                                @endphp
                                                <table class="table table-condensed table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th class="@if(strtolower($s) == 'yes') bg-success @elseif(strtolower($s) == 'no') bg-danger @endif text-white text-center">
                                                            SCHOOL
                                                        </th>
                                                        <th class="@if(strtolower($d) == 'yes') bg-success @elseif(strtolower($s) == 'no') bg-danger @endif text-white text-center">
                                                            DISTRICT
                                                        </th>
                                                        <th class="@if(strtolower($w) == 'yes') bg-success @elseif(strtolower($s) == 'no') bg-danger @endif text-white text-center">
                                                            WDA
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="text-center text-white @if(strtolower($s) == 'yes') bg-success @elseif(strtolower($s) == 'no') bg-danger @endif">
                                                            {{ $s }}
                                                        </td>
                                                        <td class="text-center text-white @if(strtolower($d) == 'yes') bg-success @elseif(strtolower($s) == 'no') bg-danger @endif">
                                                            {{ $d }}
                                                        </td>
                                                        <td class="text-center text-white @if(strtolower($w) == 'yes') bg-success @elseif(strtolower($s) == 'no') bg-danger @endif">
                                                            {{ $w }}
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        @endforeach
                                    @endif
                                </tr>
                                @php
                                    $tmpF = $indicator->function_name;
                                    $tmpQ = $indicator->quality_areas_name;
                                    $tmpI = $indicator->criteria_section;
                                @endphp
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $.get(
            "{{ route('wda.ajax.get.schools') }}",
            function (data) {
                var html = "<option value='' selected>Select..</option>";
                $.each(data, function (key, value) {
                    html += "<option value='" + key + "'>" + value + "</option>";
                });
                $("#nschools").html(html);
            }
        );
        $("#nschools").on('change', function () {
            let v = $(this).val();
            if (v.length > 0) {
                $(".when-school-null")
                    .fadeOut(555);
            } else {
                $(".when-school-null")
                    .fadeIn(555);
            }
        });
        $("#functions").on('change', function () {
            let f = $(this).val();
            $.get(
                "{{ route('wda.get.manage.quality.ajax') }}?f=" + f,
                function (data) {
                    var html = "<option value='' selected>Select..</option>";
                    $.each(data, function (key, value) {
                        html += "<option value='" + key + "'>" + value + "</option>";
                    });
                    $("#quality_area").html(html);
                }
            );
        });
        $("#quality_area").on('change', function () {
            let f = $(this).val();
            $.get(
                "{{ route('wda.get.manage.quality.ajax') }}?q=" + f,
                function (data) {
                    var html = "<option value='' selected>Select..</option>";
                    $.each(data, function (key, value) {
                        html += "<option value='" + key + "'>" + value + "</option>";
                    });
                    $("#criteria").html(html);
                }
            );
        });
        $("#criteria").on('change', function () {
            let f = $(this).val();
            $.get(
                "{{ route('wda.get.manage.quality.ajax') }}?c=" + f,
                function (data) {
                    var html = "<option value='' selected>Select..</option>";
                    $.each(data, function (key, value) {
                        html += "<option value='" + key + "'>" + value + "</option>";
                    });
                    $("#indicator").html(html);
                }
            );
        });
        $("#dataTableBtnAnswers").dataTable({
            dom: 'Blfrtip',
            pager: true,
            "ordering": true,
            "aaSorting": [],
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columnDefs: [
                {
                    targets: [],
                    visible: false,
                }
            ]
        });
    </script>
@endsection