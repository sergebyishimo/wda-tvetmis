@extends('wda.layout.main')

@section('panel-title', "Final AQA")

@section('htmlheader_title', "Final AQA")

@section('panel-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-body">
                    <table class="table" id="dataType">
                        <thead style="background: #e8e8e8">
                        <tr>
                            <td class="text-center">#</td>
                            <th title="Accreditation Type">Accr Type</th>
                            <th>School Name</th>
                            <th>Sector</th>
                            <th>Sub Sector</th>
                            <th>Qualification</th>
                            <th>Submitted</th>
                            <th>Button</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($apps as $app)
                            <tr class="text-center">
                                <td class="text-center">{{ $i++ }}</td>
                                <td title="Accreditation Type">{{ ucwords($app->type) }}</td>
                                <td>{{ $app->school_name }}</td>
                                <td>
                                    @foreach ($app->applied_for as $applied_for)
                                        {{ $applied_for->curr->subsector->sector->tvet_field or "" }} ,
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($app->applied_for as $applied_for)
                                        {{ $applied_for->curr->subsector->sub_field_name or "" }} ,
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($app->applied_for as $applied_for)
                                        {{ $applied_for->curr->qualification_title or "" }} ,
                                    @endforeach
                                </td>
                                <td>{{ date('F d, Y ', strtotime($app->updated_at)) }}</td>
                                <td>
                                    <a href="{{ route('wda.view.accr.application', [$app->id, $app->school_id]) }}"
                                       class="btn btn-primary btn-flat"><i class="fa fa-list"></i> View More</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $("#dataType").dataTable({
                dom: 'Blfrtip',
                pager: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
        });
    </script>
@endsection