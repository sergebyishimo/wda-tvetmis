<h1>School Summary</h1>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th colspan="2">School Summary Information</th>
        </tr>
        <tr>
            <td>School Name</td>
            <th>{{ $school->school_name }}</th>
        </tr>
        <tr>
            <td>Province</td>
            <th>{{ $school->province }}</th>
        </tr>
        <tr>
            <td>District</td>
            <th>{{ $school->district }}</th>
        </tr>
        <tr>
            <td>Sector</td>
            <th>{{ $school->sector }}</th>
        </tr>
        <tr>
            <td>Cell</td>
            <th>{{ $school->cell }}</th>
        </tr>
        <tr>
            <td>Village</td>
            <th>{{ $school->village }}</th>
        </tr>
        <tr>
            <td>School Manager</td>
            <th>{{ $school->manager_name }}</th>
        </tr>
        <tr>
            <td>Phone</td>
            <th>{{ $school->phone }}</th>
        </tr>
        <tr>
            <td>E-mail</td>
            <th>{{ $school->email }}</th>
        </tr>
    </thead>
</table>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th>Category</th>
            <th># Male</th>
            <th># Female</th>
            <th># Total</th>
            <th># TVET Training</th>
            <th># A0</th>
            <th># A1</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Academic Staff</td>
            <td>{{ $school->staffs()->where('staff_category', 'Academic')->where('gender', 'male')->count() }}</td>
            <td>{{ $school->staffs()->where('staff_category', 'Academic')->where('gender', 'female')->count() }}</td>
            <td>{{ $school->staffs()->where('staff_category', 'Academic')->count() }}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Administrative Staff</td>
            <td>{{ $school->staffs()->where('staff_category', 'Administrative')->where('gender', 'male')->count() }}</td>
            <td>{{ $school->staffs()->where('staff_category', 'Administrative')->where('gender', 'female')->count() }}</td>
            <td>{{ $school->staffs()->where('staff_category', 'Administrative')->count() }}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Support</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <td>Students</td>
            <td>{{ $school->students()->where('gender', 'male')->count() }}</td>
            <td>{{ $school->students()->where('gender', 'female')->count() }}</td>
            <td>{{ $school->students()->count() }}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Qualification Name</td>
            <td>Level Name</td>
            <td>Sector</td>
            <td>Sub Sector</td>
            <td># Male</td>
            <td># Female</td>
            <td># Total</td>
        </tr>
    </thead>
</table>