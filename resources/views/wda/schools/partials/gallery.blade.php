@php
    $photos = \App\SchoolGallery::where('school_id', $school->id)->take(25)->get();
@endphp

<div class="row">

    @forelse($photos as $photo)
        <div class="col-md-4">
            <img src="/storage/{{ $photo->photo  }}" alt="" style="width: 100%">
        </div>        
    @empty
        <div class="col-md-12">
            <p>No Pictures Uploaded</p>
        </div>
    @endforelse
</div>