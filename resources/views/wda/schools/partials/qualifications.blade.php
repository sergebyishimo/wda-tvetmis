<table class="table table-bordered table-condensed table-responsive" id="dataTableBtn">
    <thead align="center" class="bg-gray-light">
    <th>Qualifications <sup>(Departments)</sup></th>
    <th>RTQF <sup>Class</sup></th>
    <th style="width: 80px; !important"># of Students</th>
    </thead>
    <tbody>
    @if($school->departments->count() > 0)
        @foreach($school->departments as $department)
            @php
                $students_nber = 0;
            @endphp
            @foreach ($department->levels as $level)
                @php
                    $students_nber += count($level->students);
                @endphp
            @endforeach
            <tr>
                <td class="bg-white" width="60%">
                    <ul class="list-group">
                        <li class="list-group-item p-1 pl-3">
                            <div>
                                <div class="checkbox icheck">
                                    <label>
                                        <input style="display:none;" type="checkbox"
                                               class=""
                                               checked
                                               name="department[]" value="{{ $department->id }}">&nbsp;
                                        <b>{{ $department->qualification->qualification_title or "" }}</b>
                                    </label>
                                </div>
                                <div class="row">
                                    @if($students_nber <= 0)
                                        {!! Form::open() !!}
                                        @method('DELETE')
                                        {!! Form::hidden('department', $department->id) !!}
                                        {!! Form::hidden('school', $school->id) !!}
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-danger btn-sm pull-right">
                                                <i class="fa fa-trash-o"></i> &nbsp;
                                                Delete
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    @endif
                                </div>
                            </div>
                        </li>
                    </ul>
                </td>
                <td class="bg-white">
                    <ul class="row">
                        @if($department->levels->count() > 0)
                            @foreach($department->levels as $level)
                                {{--<div class="row p-1 pl-3">--}}
                                {{--<div class="row">--}}
                                <div class="col-md-4 mr-1 mb-1"
                                     style="box-shadow:inset 0px 0px 3px #e2e3e5;">
                                    <div class="checkbox icheck">
                                        <label>
                                            <input style="display:none;" type="checkbox"
                                                   class=""
                                                   checked
                                                   name="level[{{$department->id}}][]"
                                                   value="{{ $level->id }}">&nbsp;
                                            <b>{{ $level->rtqf ? ucwords($level->rtqf->level_name) : ''  }}</b>
                                        </label>
                                        <form method="POST" action="{{ route('wda.school.students.delete')  }}">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="level_id" value="{{ $level->id }}">
                                            <button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to delete the students in this level?!')">Delete Students</button>
                                        </form>
                                    </div>
                                </div>
                                {{--</div>--}}
                                {{--</div>--}}
                            @endforeach
                        @else
                            <li class="list-group-item list-group-item-warning text-danger">
                                No Level Assigned
                            </li>
                        @endif
                    </ul>
                </td>
                <td style="text-align:center;font-weight:bold;font-size:18px;vertical-align:middle">
                    {{ $students_nber }}
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td align="center" colspan="2">
                <h5 class="text-danger">No qualification <sup>(department)</sup> !!</h5>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    @endif
    </tbody>
</table>