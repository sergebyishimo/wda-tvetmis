<form method="POST" action="{{ route('wda.schools.students.delete.all') }}">
    @csrf
    @method('DELETE')
    <input type="hidden" name="school_id" value="{{ $school->id }}">
    <button type="submit" class="btn btn-danger" style="float: right" onclick="return window.confirm('Are you sure you want to delete all students?')">Delete All Students</button>
</form>

<form method="POST" action="{{ route('wda.schools.graduates.delete.all') }}">
    @csrf
    @method('DELETE')
    <input type="hidden" name="school_id" value="{{ $school->id }}">
    <button type="submit" class="btn btn-danger" style="float: right" onclick="return window.confirm('Are you sure you want to delete all graduates?')">Delete All Graduates</button>
</form>

<br clear="left"><br>
<table class="table table-striped" id="dataTableBtn">
    <thead>
        <tr>
            <th> Photo </th>
            <th>Student Number</th>
            <th>Student Names</th>
            <th>Student Gender</th>
            <th>Qualification</th>
            <th>Rtqf</th>
            <th> Action </th>
        </tr>
    </thead>
    <tbody>
    @if($students->count() > 0)
        @foreach($students as $student)
            <tr>
                <td>
                    @if(getStudentPhoto($student) != "")
                        <img src="{{ getStudentPhoto($student) }}" alt="{{ $student->fname }}" width="50px"
                             height="50px">
                    @endif
                </td>
                <td>
                    <label class="label label-primary">{{ $student->reg_no }}</label>
                </td>
                <td>
                    <h5>
                        {{ $student->fname." ".$student->lname }}
                    </h5>
                </td>
                <td>
                    {{ $student->gender }}
                </td>
                <td>
                    {{ $student->level->department->qualification->qualification_title or "" }}
                </td>
                <td>
                    {{ $student->level->rtqf->level_name or "" }}
                </td>
                <td></td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

{{ $students->links() }}