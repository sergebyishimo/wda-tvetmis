<br>
<div class="row">
    {{--<form method="POST" action="{{ route('wda.view.school.staff.upload') }}" enctype="multipart/form-data">--}}
        {{--@csrf--}}
        {{--<div class="col-md-4">--}}
            {{--<input type="file" name="" id="" accept=".xls,.xlsx" required>--}}
        {{--</div>--}}
        {{--<div class="col-md-2">--}}
            {{--<button type="submit" class="btn btn-primary btn-block">Upload</button>--}}
        {{--</div>--}}
    {{--</form>--}}
    <div class="col-md-3 col-md-offset-10">
        <form method="POST" action="{{ route('wda.school.staff.delete') }}">
            @csrf
            @method('DELETE')
            <input type="hidden" name="school_id" value="{{ $school->id }}">
            <button type="submit"  class="btn btn-danger" onclick="return window.confirm('Are you sure you want to delete all staff from this school?!')"><i class="fa fa-trash"></i> Delete All Staff</button>
        </form>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-12">
        <table class="table" id="dataTableBtn">
            <thead>
            <th>Photo</th>
            <th>Names</th>
            <th>Position</th>
            <th>Gender</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Category</th>
            {{-- <th>Contract</th> --}}
            </thead>
            <tbody>
            @if($staffs->count() > 0)
                @foreach($staffs as $staff)
                    <tr>
                        <td>
                            @if($staff->photo != null)
                                <img src="{{ getStaffPhoto($staff->photo) }}" alt=""
                                    class="img-responsive img-rounded" style="width: 100px;">
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('wda.view.school.staff.profile',['id'=>$school->id,'staffid'=>$staff->id]) }}">{!! $staff->names !!}</a>
                        </td>
                        <td></td>
                        {{--<td>{!! getSchoolUserPrivilege($staff->id) !!}</td>--}}
                        <td>{!! $staff->gender !!}</td>
                        <td><a href="#" class="label label-primary phone-l"
                            data-phone="{{ $staff->phone_number }}">{!! $staff->phone_number !!}</a>
                        </td>
                        <td><a href="#" class="label label-primary email-l"
                            data-email="{{ $staff->email }}">{!! $staff->email !!}</a></td>
                        {{-- <td>
                            <div>Started: <b>{!! $staff->contract_start !!}</b></div>
                            <div>Ending: <b>{!! $staff->contract_end !!}</b></div>
                        </td> --}}
                        <td>{{ $staff->staff_category }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>