@extends('wda.layout.main')

@section('panel-title', "Quality Area Source")

@section('htmlheader_title', "Quality Area Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if(!isset($quality))
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center" width="40px">#</th>
                        <th>Quality Area</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $c=1;
                    @endphp
                    @forelse($qualityAreas as $area)
                        <tr>
                            <td>{{ $c++ }}</td>
                            <td>{{ $area->name }}</td>
                            <td>
                                {!! Form::open(['route' => ['wda.audit.manage.quality.destroy', $area->id]]) !!}
                                @method('DELETE')
                                @csrf
                                {!! Form::hidden('destroy', $area->id) !!}

                                <a href="{{ route('wda.audit.manage.quality.edit', $area->id) }}"
                                   class="btn btn-primary btn-flat btn-sm"><i
                                            class="fa fa-edit"></i> Edit</a>

                                <button type="submit"
                                        class="btn btn-danger btn-flat btn-sm"
                                        onclick="return window.confirm('Are you sure you want to remove this quality area:\n{{ $area->name }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2" class="text-center">No Data ...</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            @endif
            @php($route = 'wda.audit.manage.quality.store')
            {!! Form::open(['route' => $route]) !!}
            @csrf
            @isset($quality)
                {!! Form::hidden('quality', $quality->id) !!}
            @endisset
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        {!! Form::text('name', isset($quality) ? $quality->name : null, ['class' => 'form-control', 'placeholder' => 'Quality Area', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success btn-flat btn-block">
                        <i class="fa fa-save"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection