@extends('wda.layout.main')

@section('panel-title', "School Summary")

@section('htmlheader_title', "School Summary")

@section('panel-body')
    <div class="container-fluid">
        <div class="panel panel-body">
            {!! Form::open(['method' => 'GET']) !!}
            <div class="row">
                <div class="col-md-11 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Select School</label>
                                <select class="form-control flat" id="nschools" name="school">
                                    <option value="">Choose Here</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            {!! Form::submit('Search', ['class' => 'btn btn-sm btn-warning']) !!}
                        </div>
                        <div class="col-md-2 col-md-offset-5">
                            <a href="{{ route('wda.school.summary') }}"
                               class="btn btn-sm btn-danger">
                                <i class="fa fa-crosshairs"></i>&nbsp;Reset
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @isset($school)
            <div style="overflow-x: auto;">
                {!! Form::open(['route' => 'wda.school.summary.upload.save', 'method' => 'post', 'files' => true]) !!}
                {!! Form::hidden('school_id', request()->input('school')) !!}
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('summary', 'Use docs if prepared ...', '', ['class' => 'control-label']) !!}
                            {!! Form::file('summary') !!}
                        </div>
                        <div class="col-md-12 mt-1 mb-3">
                            <button class="btn btn-success" type="submit">Submit</button>
                        </div>
                    </div>
                {!! Form::close() !!}
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="300px" class="text-center">School identification</th>
                        <th width="300px" class="text-center">Observed Strengths</th>
                        <th width="300px" class="text-center">Observed Weaknesses & Challenges</th>
                        <th width="300px" class="text-center">Recommendations</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <table class="table">
                                <tr>
                                    <td>School Name:</td>
                                    <td><b>{{ $school->school_name }}</b></td>
                                </tr>
                                <tr>
                                    <td>School Status:</td>
                                    <td><b>{{ $school->school_status }}</b></td>
                                </tr>
                                <tr>
                                    <td>School Type:</td>
                                    <td><b>{{ $school->schooltype->school_type or "" }}</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" class="bg-warning"><b>Address</b></td>
                                </tr>
                                <tr>
                                    <td>Province:</td>
                                    <td><b>{{ $school->province }}</b></td>
                                </tr>
                                <tr>
                                    <td>District:</td>
                                    <td><b>{{ $school->district }}</b></td>
                                </tr>
                                <tr>
                                    <td>Sector:</td>
                                    <td><b>{{ $school->sector }}</b></td>
                                </tr>
                                <tr>
                                    <td>Manager:</td>
                                    <td><b>{{ $school->manager_name }}</b></td>
                                </tr>
                                <tr>
                                    <td>Manager Phone:</td>
                                    <td><b>{{ $school->manager_phone }}</b></td>
                                </tr>
                            </table>
                        </td>
                        <td class="bg-gray-light">
                            @foreach($qualities as $quality)
                                <table class="table">
                                    <tr>
                                        <td><b>{{ $quality->name }}</b></td>
                                    </tr>
                                    <tr>
                                        <td><textarea name="strengths[{{$quality->id}}]" id="" cols="30"
                                                      rows="5"></textarea></td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>
                        <td>
                            @foreach($qualities as $quality)
                                <table class="table">
                                    <tr>
                                        <td><b>{{ $quality->name }}</b></td>
                                    </tr>
                                    <td><textarea name="weaknesses[{{$quality->id}}]" id="" cols="30"
                                                  rows="5"></textarea></td>
                                </table>
                            @endforeach
                        </td>
                        <td>
                            <table class="table">
                                <tr>
                                    <td><b>Accreditation of New School/Trades</b></td>
                                </tr>
                                <tr>
                                    <td><textarea name="school_trade" id="" cols="30" rows="5"></textarea></td>
                                </tr>
                                <tr>
                                    <td><b>Accreditation of new level</b></td>
                                </tr>
                                <tr>
                                    <td><textarea name="new_level" id="" cols="30" rows="5"></textarea></td>
                                </tr>
                                <tr>
                                    <td><b>School Re-accreditation/Revocation of some trades</b></td>
                                </tr>
                                <tr>
                                    <td><textarea name="new_level" id="" cols="30" rows="5"></textarea></td>
                                </tr>
                                <tr>
                                    <td><b>Conclusion/General recommendation</b></td>
                                </tr>
                                <tr>
                                    <td><textarea name="general" id="" cols="30" rows="5"></textarea></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-center bg-warning"><b>SOME PICTURES OF THE SCHOOL</b></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="file" name="image_1"></td>
                        <td><input type="file" name="image_2"></td>
                        <td><input type="file" name="image_2"></td>
                    </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::submit('Submit',['class' => 'btn btn-lg btn-primary']) !!}
                    </div>
                </div>
            </div>
        @endisset
    </div>
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $.get(
            "{{ route('wda.ajax.get.schools') }}",
            function (data) {
                var html = "<option value='' selected>Select..</option>";
                $.each(data, function (key, value) {
                    html += "<option value='" + key + "'>" + value + "</option>";
                });
                $("#nschools").html(html);
            }
        );
        $("#nschools").on('change', function () {
            let v = $(this).val();
            if (v.length > 0) {
                $(".when-school-null")
                    .fadeOut(555);
            } else {
                $(".when-school-null")
                    .fadeIn(555);
            }
        });
    </script>
@endsection