@extends('wda.layout.main')

@section('panel-title', "Indicators Source")

@section('htmlheader_title', "Indicators Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if(!isset($cindicator))
                <table class="table table-bordered" style="background: #fff" width="100%">
                <thead>
                <tr>
                    <th>Quality Area</th>
                    <th>Indicator</th>
                </tr>
                </thead>
                <tbody>
                @php($x = 1)
                @if($qualityAreas->count() > 0)
                    @foreach($qualityAreas as $quality)
                        <tr>
                            <td class="">
                                {{ $quality->name }}
                            </td>
                            <td>
                                @if($quality->indicator->count() > 0)
                                    <table class="table">
                                        @foreach($quality->indicator as $indicator)
                                            <tr>
                                                <td class="">
                                                    <h6>{{ $indicator->name }}</h6>
                                                </td>
                                                <td>
                                                    {!! Form::open(['route' => ['wda.audit.manage.indicator.destroy', $indicator->id]]) !!}
                                                    @method('DELETE')
                                                    @csrf
                                                    {!! Form::hidden('destroy', $indicator->id) !!}

                                                    <a href="{{ route('wda.audit.manage.indicator.edit', $indicator->id) }}"
                                                       class="btn btn-primary btn-flat btn-sm"><i
                                                                class="fa fa-edit"></i> Edit</a>

                                                    <button type="submit"
                                                            class="btn btn-danger btn-flat btn-sm"
                                                            onclick="return window.confirm('Are you sure you want to remove this indicator area:\n{{ $indicator->name }} ?')">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            @endif
            @php($route = 'wda.audit.manage.indicator.store')
            {!! Form::open(['route' => $route]) !!}
            @csrf
            @isset($cindicator)
                {!! Form::hidden('indicator', $cindicator->id) !!}
            @endisset
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control flat select2"
                                name="quality_id" required>
                            <option value="">Choose Quality</option>
                            @foreach($qualityAreas as $quality)
                                <option value="{{ $quality->id }}" {{ isset($cindicator) ? $cindicator->quality_id == $quality->id ? 'selected' : '' : '' }} >{{ $quality->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control flat" name="name"
                               value="{{ isset($cindicator) ? $cindicator->name : '' }}"
                               placeholder="Indicator" required>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-block btn-flat"><i
                                    class="fa fa-save"></i> Save
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection