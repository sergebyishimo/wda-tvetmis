@extends('wda.layout.main')

@section('panel-title', "Quality Area Source")

@section('htmlheader_title', "Quality Area Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center" width="40px">#</th>
                    <th>Functions</th>
                    <th>Quality Area</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($functions as $att)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $att->name }}</td>
                        <td>
                            <table class="table">
                                @php($c=1)
                                @if($att->qualityAreas->count() > 0)
                                    @foreach($att->qualityAreas as $area)
                                        <tr>
                                            <td>{{ $area->name }}</td>
                                            <td>
                                                {!! Form::open(['route' => ['wda.manage.destroy.quality.area', $area->id]]) !!}
                                                @method('DELETE')
                                                @csrf
                                                {!! Form::hidden('id', $area->id) !!}

                                                <a href="{{ route('wda.manage.edit.quality.area', $area->id) }}"
                                                   class="btn btn-primary btn-flat btn-sm"><i
                                                            class="fa fa-edit"></i> Edit</a>

                                                <button type="submit"
                                                        class="btn btn-danger btn-flat btn-sm"
                                                        onclick="return window.confirm('Are you sure you want to remove this quality area:\n{{ $area->name }} ?')">
                                                    <i class="fa fa-trash"></i> Delete
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @php($route = 'wda.manage.store.quality.area')
            @isset($quality)
                @php($route = ['wda.manage.update.quality.area', $quality->id])
            @endisset
            {!! Form::open(['route' => $route]) !!}
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::select('function_id', getFunctionsForSelect(), isset($quality) ? $quality->function_id : null , ['class' => 'form-control', 'placeholder' => 'Select Function', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::text('name', isset($quality) ? $quality->name : null, ['class' => 'form-control', 'placeholder' => 'Quality Area', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success btn-flat btn-block">
                        <i class="fa fa-save"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection