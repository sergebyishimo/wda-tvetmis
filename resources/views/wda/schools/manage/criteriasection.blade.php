@extends('wda.layout.main')

@section('panel-title', "Criteria Source")

@section('htmlheader_title', "Criteria Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">

            @if (isset($att_info))

                <form method="POST" action="{{ route('wda.manage.criteria') }}">
                    @csrf
                    <input type="hidden" name="att_id" value="{{ $att_info->id }}">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Attachment</h3>
                        </div>


                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Quality Area</label>
                                    <select name="quality_area_id"
                                            id="quality_area_id"
                                            class="form-control"
                                            required=true>
                                        <option value="">Select Quality Area</option>
                                        @foreach($atts as $quality)
                                            <option value="{{ $quality->id }}" {{ $att_info->quality_area_id == $quality->id ? 'selected' : '' }} >{{ $quality->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Criteria Section</label>
                                        <input type="text" class="form-control flat" name="criteria"
                                               value="{{ $att_info->criteria_section }}"
                                               placeholder="Program">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info btn-flat btn-block"><i
                                        class="fa fa-save"></i> Save Criteria Section
                            </button>
                        </div>
                    </div>
                </form>
            @endif

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center" width="40px">#</th>
                    <th>Quality Area</th>
                    <th colspan="2">Criteria Section</th>
                </tr>
                </thead>
                <tbody>
                @php($ii = 1)
                @forelse ($atts as $att)
                    <tr>
                        <td>{{ $ii++ }}</td>
                        <td>{{ $att->name }}</td>
                        <td>
                            <table class="table">
                                @if($att->criteria->count() > 0)
                                    @php($i = 1)

                                    @foreach($att->criteria as $criteria)
                                        <tr>
                                            <td class="text-center">{{ $i++ }}</td>
                                            <td>{{ $criteria->criteria_section }}</td>
                                            <td class="text-center">{{ $criteria->criterias->sum('weight') }}</td>
                                            <td>
                                                <form method="POST" action="{{ route('wda.manage.criteria') }}">
                                                    {{ csrf_field() }}

                                                    <input type="hidden" name="_method" value="delete">
                                                    <input type="hidden" name="delete_att"
                                                           value="{{ $criteria->id }}">

                                                    <a href="{{ route('wda.manage.edit.criteria', $criteria->id) }}"
                                                       class="btn btn-primary btn-flat btn-sm">
                                                        <i class="fa fa-edit"></i> Edit</a>
                                                    <button type="submit"
                                                            class="btn btn-danger btn-flat btn-sm"
                                                            onclick="return window.confirm('Are you sure you want to remove this criteria: \n\n {{ $criteria->criteria_section }} ?')">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center">
                            No Data Available !
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <form method="POST">
                @csrf
                <div class="row mb-4">
                    <div class="col-md-6">
                        <select name="quality_area_id"
                                id="quality_area_id"
                                class="form-control"
                                required=true>
                            <option value="">Select Quality Area</option>
                            @foreach($atts as $quality)
                                <option value="{{ $quality->id }}">{{ $quality->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control flat" name="criteria"
                               placeholder="Criteria" required>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-flat btn-block"><i
                                    class="fa fa-save"></i> Save
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection