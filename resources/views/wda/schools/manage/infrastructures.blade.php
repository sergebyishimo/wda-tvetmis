@extends('wda.layout.main')

@section('panel-title', "Infrastructure Source")

@section('htmlheader_title', "Infrastructure Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th class="text-center" width="80px">#</th>
                    <th>Infrastructure</th>
                    <th class="text-center" style="width: 135px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @forelse($atts as $att)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $att->name }}</td>
                        <td>
                            {!! Form::open(['route' => 'wda.manage.destroy.infrastructure']) !!}
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="delete_att" value="{{ $att->id }}">

                            <button type="submit"
                                    class="btn btn-warning btn-outline-danger pull-right btn-flat btn-sm"
                                    onclick="return window.confirm('Are you sure you want to remove : {{ $att->name }} ?')">
                                <i class="fa fa-trash"></i> Delete
                            </button>
                            {!! Form::close() !!}
                            <a href="{{ route('wda.manage.edit.infrastructure', $att->id) }}"
                               style="margin-right: 10px;"
                               class="btn btn-outline-success pull-right btn-flat btn-sm"><i class="fa fa-edit"></i>
                                Edit</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center">No Data Available !</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! Form::open(['route' => 'wda.manage.store.infrastructure']) !!}
            @if (isset($att_info))
                <input type="hidden" name="att_id" value="{{ $att_info->id }}">
            @endif
            <div class="row" style="margin-top: 5px">
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="text" class="form-control flat"
                               @if (isset($att_info))
                               value="{{ $att_info->name }}"
                               @endif
                               name="infrastructure" placeholder="Infrastructure"
                               required>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success btn-flat btn-block">
                        <i class="fa fa-save"></i> Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection