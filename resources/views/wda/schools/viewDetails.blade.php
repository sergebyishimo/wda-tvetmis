@extends('wda.layout.main')

@section('panel-title' )
    Viewing School Information For <u> {{ $school->school_name }}</u>
@endsection

@section('htmlheader_title', "Viewing School Information For " . $school->school_name)


@section('panel-body')
    <div>
        <ul class="nav nav-tabs" role="tablist">
            {{-- <li role="presentation" class="active"><a href="#information" aria-controls="home" role="tab" data-toggle="tab">Information</a></li> --}}
            <li role="presentation {{ request()->get('tab') == 'd' ? 'active' : '' }}">
                <a href="?tab=d">Department</a>
            </li>
            <li role="presentation {{ request()->get('tab') == 'sf' ? 'active' : '' }}">
                <a href="?tab=sf">Staffs</a>
            </li>
            <li role="presentation {{ request()->get('tab') == 'st' ? 'active' : '' }}">
                <a href="?tab=st">Students</a>
            </li>
            <li role="presentation {{ request()->get('tab') == 'vs' ? 'active' : '' }}">
                <a href="?tab=vs">View Summary</a>
            </li>
            <li role="presentation {{ request()->get('tab') == 'gallery' ? 'active' : '' }}">
                <a href="?tab=gallery">Gallery</a>
            </li>
        </ul>
        <div class="tab-content p-2">
            {{-- <div role="tabpanel" class="tab-pane active" id="information">
                @include('wda.schools.partials.informations')
            </div> --}}
            @if(request()->get('tab') == 'd')
                <div role="tabpanel" class="tab-pane active" id="qualifications">
                    @include('wda.schools.partials.qualifications')
                </div>
            @endif
            @if(request()->get('tab') == 'sf')
                <div role="tabpanel" class="tab-pane active" id="staffs">
                    @include('wda.schools.partials.staffs')
                </div>
            @endif
            @if(request()->get('tab') == 'st')
                <div role="tabpanel" class="tab-pane active" id="students">
                    @include('wda.schools.partials.students')
                </div>
            @endif
            @if(request()->get('tab') == 'vs' || ! request()->has('tab'))
                <div role="tabpanel" class="tab-pane active" id="students">
                    @include('wda.schools.partials.summary')
                </div>
            @endif
            @if(request()->get('tab') == 'gallery' || ! request()->has('tab'))
                <div role="tabpanel" class="tab-pane active" id="gallery">
                    @include('wda.schools.partials.gallery')
                </div>
            @endif
        </div>
    </div>
@endsection