@extends('wda.layout.main')

@section('panel-title', "Accreditation General Settings")

@section('htmlheader_title', "Accreditation General Settings")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('wda.accr.settings') }}">
                {{ csrf_field() }}

                <input type="hidden" name="form" value="welcome">
                <input type="hidden" name="welcome_id" value="{{ $welcome->id or "" }}">
                <div class="box box-primary box-outline">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-edit"></i> Edit Welcome Message </h3>
                        <div class="box-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body pad">
                        <div class="mb-3">
				                <textarea class="form-group summernote" name="message"
                                          placeholder="Place some text here"
                                          style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                          required>{{ $welcome->message or "" }}</textarea>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Save
                            Welcome Message
                        </button>
                    </div>
                </div>
            </form>


            <div class="box box-primary box-outline">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> Edit Notifications</h3>
                    <div class="box-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <form method="POST" action="{{ route('wda.accr.settings') }}">
                    {{ csrf_field() }}
                    <div class="box-body pad">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Notification Message</th>
                                <th>Last Update</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($notifications as $notification)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $notification->message }}</td>
                                    <td>{{ date('d/m/Y', strtotime($notification->updated_at)) }}</td>
                                    <td>
                                        <form method="POST" action="{{ route('wda.accr.destroy.settings') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">

                                            <input type="hidden" name="form" value="notification">
                                            <input type="hidden" name="delete_noti" value="{{ $notification->id }}">

                                            <a href="{{ route('wda.accr.edit.settings', $notification->id) }}"
                                               class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit
                                            </a>

                                            <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                                    onclick="return window.confirm('Are you sure you want to remove this notification :\n{{ $notification->message }} ')">
                                                <i class="fa fa-trash "></i> Delete
                                            </button>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" name="form" value="notification">
                        <br clear="left"><br>

                        @if (isset($notification_info))
                            <input type="hidden" name="noti_id" value="{{ $notification_info->id }}">
                            <div class="form-group">
                                <label style="color: green"><i class="fa fa-edit"></i> Edit Notification</label>
                                <textarea class="form-control flat" name="u_message" placeholder="Notification Message"
                                          autofocus
                                          required>{{ $notification_info->message }}</textarea>
                            </div>
                        @else
                            <div class="form-group">
                                <label><i class="fa fa-bell"></i> Add Notification</label>
                                <textarea class="form-control flat" name="message" placeholder="Notification Message"
                                          required></textarea>
                            </div>
                        @endif


                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            <i class="fa fa-save"></i> Save Notification
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('l-style')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Text Goes here ...',
            tabsize: 2,
            height: 200
        });
    </script>
@endsection