<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    @if(env('APP_ENV') != "local")
        {{ Html::style('css/adminlte.css') }}
    @else
        <link rel="stylesheet" href="{{ public_path('css/adminlte.css') }}">
    @endif
</head>
<body>
<table class="table table-bordered" border='1'>
    <thead class="bg-gray">
    <tr>
        <th class="text-center bg-gray" width="10px" align="center">identifier</th>
        <th class="text-center bg-gray">QUALITY AREA</th>
        <th class="text-center bg-gray">SECTIONS</th>
        <th class="text-left bg-gray" width="100px">CRITERIA</th>
        <th class="text-left bg-gray" width="15px">SCHOOL</th>
        <th class="text-left bg-gray" width="15px">DISTRICT</th>
        <th class="text-left bg-gray" width="10px">WDA</th>
    </tr>
    </thead>
    @php
        $tmpQ=1;
        $tmpS=1;
    @endphp
    <tbody>
    @foreach($qualities as $quality)
        @if($quality->criteria()->count() > 0)
            @foreach ($quality->criteria as $criteria_section)
                @if($criteria_section->criterias()->count() > 0)
                    @foreach ($criteria_section['criterias'] as $criteria)
                        @if($quality->id != $tmpQ)
                            @php
                                $csQ = 0;
                            @endphp
                        @endif
                        <tr>
                            <td class="bg-gray" align="center">{{ $criteria->id }}</td>
                            <td class="bg-gray-light">
                                @if($quality->id != $tmpQ)
                                    {{ $quality->name }}
                                @endif
                            </td>
                            <td class="bg-gray-light">
                                @if($criteria_section->id != $tmpS)
                                    {{ $criteria_section->criteria_section }}
                                @endif
                            </td>
                            <td>
                                {{ ucwords($criteria->criteria) }}
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        @php
                            $tmpQ = $quality->id;
                            $tmpS = $criteria_section->id;
                        @endphp
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
    </tbody>
</table>
</body>
</html>