@extends('wda.layout.main')

@section('panel-title', "Export & Import")

@section('htmlheader_title', "Export & Import")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header bg-primary with-border text-white">
                    <i class="fa fa-download"></i>&nbsp;Exporting
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => 'wda.export.format']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('function', "Function", ['class' => 'control-label']) !!}
                            {!! Form::select('function', $functs->pluck('name', 'id'), null, ['class' => 'form-control select2', 'placeholder' => 'Select Function', 'required' => true]) !!}
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary" style="margin-top: 23px;">
                                <i class="fa fa-download"></i>&nbsp;Export Excel
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border bg-info text-white">
                    <i class="fa fa-upload"></i>&nbsp;Importing
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => 'wda.import.format', 'files' => true]) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('school', "School", ['class' => 'control-label']) !!}
                                {!! Form::select('school', $schools->pluck('school_name', 'id'), null, ['class' => 'form-control select2', 'placeholder' => 'Select School', 'required' => true]) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('function', "Function", ['class' => 'control-label']) !!}
                            {!! Form::select('function', $functs->pluck('name', 'id'), null, ['class' => 'form-control select2', 'placeholder' => 'Select Function', 'required' => true]) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('file', "Excel Format") !!}
                                {!! Form::file('file',['class' => 'filer_docs_input_excel', 'required' => true]) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary mt-5">
                                <i class="fa fa-upload"></i>&nbsp;
                                Upload
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('l-style')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Text Goes here ...',
            tabsize: 2,
            height: 200
        });
    </script>
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: xlsx");
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
@endsection