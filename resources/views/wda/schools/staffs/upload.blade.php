@extends('wda.layout.main')

@section('panel-title', "Upload Staffs")

@section('htmlheader_title', "Upload  Staffs")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
    <style>
        div.dataTables_wrapper {
            width: 87.5%;
            margin: 0 auto;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <a href="{{ route('wda.schools') }}" class="btn btn-primary">Go Back</a>
                <div class="box">
                <div class="box-header">
                    <form method="POST" action="{{ route('wda.staff.upload.save')  }}" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">Choose School</label>
                                <select name="school_id" id="" class="form-control" required>
                                    @if (isset($school))
                                        <option value="{{ $school->id  }}">{{ $school->school_name  }}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Choose File</label>
                                <input type="file" name="uploadedFile" accept=".xls, .xlsx" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <br>
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            </div>
        </div>
    </div>
@endsection