@extends('wda.layout.main')

@section('panel-title', "List of all Teachers")

@section('htmlheader_title', "List of all Teachers")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
    <style>
        div.dataTables_wrapper {
            width: 87.5%;
            margin: 0 auto;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-header">
                        {!! Form::open(['id'=>'search','route' => 'wda.studentmanagement.search', 'method' => 'GET']) !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Based on PROVINCE</label>
                                        <select class="form-control flat" id="nprovince" name="province">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Based on DISTRICT</label>
                                        <select class="form-control flat" id="ndistrict" name="district">
                                            <option value="">Choose Here</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('school_id', "Based on SCHOOL") !!}
                                        {!! Form::select('school_id', $all_schools , null , ['class' => 'form-control select2 ', 'placeholder' => 'select school']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('gender', "Based on GENDER") !!}
                                        {!! Form::select('gender',[
                                            'Male' => 'Male',
                                            'Female' => 'Female'
                                        ], null, [
                                                   'class' => 'form-control select2',
                                                   'placeholder' => 'Choose gender'
                                        ]) !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <table class="table table-bordered" id="dataTableBtnList">
                    <thead>
                    <th>Photo</th>
                    <th>Names</th>
                    <th>School</th>
                    <th>Gender</th>
                    <th>NationID</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Province</th>
                    <th>District</th>
                    <th>TVET Trainings</th>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <th>Photo</th>
                    <th>Names</th>
                    <th>School</th>
                    <th>Gender</th>
                    <th>NationID</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Province</th>
                    <th>District</th>
                    <th>TVET Trainings</th>
                    </tfoot>
                </table>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#dataTableBtnList').DataTable({
                serverSide: true,
                processing: true,
                searching: true,
                pager: true,
                colReorder: true,
                responsive: true,
                "scrollX": true,
                ajax: '/wda/datatable/object-data/s',
                lengthMenu: [[25, 100, 1000, -1], [25, 100, 1000, "All"]],
                columns: [
                    {data: 'photo', orderable: false, searchable: false},
                    {data: 'names', name: 'staffs_info.first_name'},
                    {data: 'school', name: 'accr_schools_information.school_name'},
                    {data: 'gender', name: 'staffs_info.gender'},
                    {data: 'nationID', name: 'staffs_info.national_id_number'},
                    {data: 'phone_number', name: 'staffs_info.phone_number'},
                    {data: 'email', name: 'staffs_info.email'},
                    {data: 'province', name: 'accr_schools_information.province'},
                    {data: 'district', name: 'accr_schools_information.district'},
                    {data: 'tvet_trainings', name: 'staffs_info.attended_rtti'},
                    // {data: 'actions', orderable: false, searchable: false}
                ],
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            // jQuery selector to add a border
                            $('row c[r*="10"]', sheet).attr('s', '25');
                        },
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        postfixButtons: ['colvisRestore'],
                        collectionLayout: 'fixed two-column'
                    },


                    // 'columnsToggle'
                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select class="select2"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });

            $('#filterSearchremove').click(function (e) {
                e.preventDefault();
                $('#filterSearch').hide();
            });
            $('#columnremove').click(function (e) {
                e.preventDefault();
                $('#filtercolumns').hide();
            });
        });
    </script>
@endsection