@extends('wda.layout.main')

@section('panel-title', "Staffs Summary")

@section('htmlheader_title', "Staffs Summary")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
    <style>
        div.dataTables_wrapper {
            width: 87.5%;
            margin: 0 auto;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h4>Teachers per Province</h4>
                <canvas id="chartjs-0"></canvas>
            </div>
            <div class="col-md-4">
                <h4>Teachers by Category</h4>
                <canvas id="chartjs-1"></canvas>
            </div>
            <div class="col-md-4">
                <h4>Teachers by Qualification Level</h4>
                <canvas id="chartjs-2"></canvas>
            </div>
            <br clear="left"><br><br><br>
            <div class="col-md-12">
                <table class="table table-striped table-hover" id="schoolsTable">
                    <thead>
                        <tr>
                            <th>School</th>
                            <th>Status</th>
                            <th>Type</th>
                            <th>Province</th>
                            <th>District</th>
                            <th>Total</th>
                            <th>Academic Male</th>
                            <th>Academic Female</th>
                            <th>Admin Male</th>
                            <th>Admin Female</th>
                            <th>A0</th>
                            <th>A1</th>
                            <th>A2</th>
                            <th>A3</th>
                            <th>Masters</th>
                            <th>Other</th>
                            <th>Training</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($schools as $school)
                        <tr>
                            <th>{{ $school->school_name }}</th>
                            <th>{{ $school->school_status }}</th>
                            <th>{{ $school->schooltype->school_type }}</th>
                            <th>{{ $school->province }}</th>
                            <th>{{ $school->district }}</th>
                            <th>{{ $school->staffs()->count() }}</th>
                            <th>{{ $school->staffs()->where('staff_category', 'Academic')->where('gender', 'Male')->count() }}</th>
                            <th>{{ $school->staffs()->where('staff_category', 'Academic')->where('gender', 'Female')->count() }}</th>
                            <th>{{ $school->staffs()->where('staff_category', 'Administrative')->where('gender', 'Male')->count() }}</th>
                            <th>{{ $school->staffs()->where('staff_category', 'Administrative')->where('gender', 'Female')->count() }}</th>
                            <th>{{ $school->staffs()->join('accr_qualification_levels', 'accr_qualification_levels.id', 'staffs_info.qualification_level')->where('accr_qualification_levels.name', 'A0')->count() }}</th>
                            <th>{{ $school->staffs()->join('accr_qualification_levels', 'accr_qualification_levels.id', 'staffs_info.qualification_level')->where('accr_qualification_levels.name', 'A1')->count() }}</th>
                            <th>{{ $school->staffs()->join('accr_qualification_levels', 'accr_qualification_levels.id', 'staffs_info.qualification_level')->where('accr_qualification_levels.name', 'A2')->count() }}</th>
                            <th>{{ $school->staffs()->join('accr_qualification_levels', 'accr_qualification_levels.id', 'staffs_info.qualification_level')->where('accr_qualification_levels.name', 'A3')->count() }}</th>
                            <th>{{ $school->staffs()->join('accr_qualification_levels', 'accr_qualification_levels.id', 'staffs_info.qualification_level')->where('accr_qualification_levels.name', 'Masters')->count() }}</th>
                            <th>{{ $school->staffs()->join('accr_qualification_levels', 'accr_qualification_levels.id', 'staffs_info.qualification_level')->where('accr_qualification_levels.name', 'Other')->count() }}</th>
                            <th>{{ $school->staffs()->where('attended_rtti', 'yes')->count() }}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section("l-scripts")
    @parent
    @include('jsview')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
        new Chart(document.getElementById("chartjs-0"),
            {"type":"bar","data":
                    {
                        "labels":provinces,
                        "datasets":[
                            {"label":"Teachers per province","data":statistics,
                                "fill":false,
                                "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                                "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});
    </script>
    <script>
        new Chart(document.getElementById("chartjs-1"),
            {"type":"bar","data":
                    {
                        "labels":['Administrative' ,'Academic'],
                        "datasets":[
                            {"label":"Teachers by category","data":category,
                                "fill":false,
                                "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                                "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});
    </script>
    <script>
        new Chart(document.getElementById("chartjs-2"),
            {"type":"bar","data":
                    {
                        "labels":levels,
                        "datasets":[
                            {"label":"Teachers by qualification level","data":level_stats   ,
                                "fill":false,
                                "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                                "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
                    },
                "options":
                    {"scales":
                            {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});
    </script>
    <script>
        $("#schoolsTable").DataTable({
            dom: 'Blfrtip',
            "lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
            "responsive": true,
            "scrollX": true,
            buttons: [
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed two-column',
                    postfixButtons: ['colvisRestore']
                },
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
        });
    </script>

@endsection