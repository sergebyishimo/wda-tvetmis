@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        Attachments
    </div>
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
          integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
            @include('feedback.feedback')
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ route('wda.attachments.upload') }}" class="btn btn-primary pull-right"> Add Attachment </a>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped table-hover" id="dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Download </th>
                                    <th>Upload Time</th>
                                    <th>Added By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($attachments as $att)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $att->title }}</td>
                                        <td>{!! $att->description !!}</td>
                                        <td><a href="/storage/{{ $att->file }}">Download</a></td>
                                        <td>{{ date('d/m/Y - H:i', strtotime($att->created_at)) }}</td>
                                        <td>{{ App\Wda::find($att->added_by)->name }}</td>
                                        <td><a href="{{ route('wda.attachments.remove', $att->id) }}" class="btn btn-danger" onclick="return window.confirm('Are you sure you want to remove this attachment: \n {{ $att->title }} ?')"><i class="fa fa-trash"></i> Remove</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show