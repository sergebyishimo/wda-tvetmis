@extends('layouts.master')

@section('content')

	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						@if(isset($output))
							<div class="card card-primary">
								<div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
									<h3 class="card-title p-3">
										<i class="fa fa-pie-chart mr-1"></i>
										Project Output Details
									</h3>
									<ul class="nav nav-pills ml-auto p-2">
										<li class="nav-item">
											<a class="nav-link" href="/projects/report/{{ $output->outcome->program->project->id }}"><i class="fa fa-arrow-left"></i> Go Back</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/projects/download/indicators/{{ $output->outcome->id }}"><i class="fa fa-file-pdf-o"></i> PDF</a>
										</li>
									</ul>
								</div>
								<div class="card-body" 	>
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Project</label>
										<p>{{ $output->outcome->program->project->project_name }}</p>
									</div>
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i>  Program</label>
										<p>{{ $output->outcome->program->name }}</p>
									</div>			
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Outcome</label>
										<p>{{ $output->outcome->name }}</p>
									</div>			
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Output</label>
										<p>{{ $output->name }}</p>
									</div>

									<div class="form-group">
										<label><i class="fa fa-table"></i> Indicators Table</label>
									</div>

									<div style="overflow-y: scroll;">
										<table class="table table-bordered table-hover" >
											<thead style="background: #f1f1f1">
												<tr>
													<th>#</th>	
													<th style="width: 100px">Indicator</th>
													<th>Baseline {{ date('Y', strtotime($output->outcome->program->created_at)) }}</th>
													<th>Year / Target / Actual</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@php
													$i = 1;
												@endphp
												@foreach($output['indicators'] as $indicator)
													<tr>
														<td>{{ $i++ }}</td>
														<td>{{ $indicator->name }}</td>
														<td>{{ $indicator->baseline }}</td>
														<td style="padding: 0;">
															<table class="table table-bordered table-hover table-striped" style="margin-bottom: -2px;">
																<tr>
																	<td>{{ $indicator->year_one }}</td>
																	<td>{{ $indicator->year_one_target }}</td>
																	<td>{{ $indicator->year_one_actual }}</td>
																</tr>
																<tr>
																	<td>{{ $indicator->year_two }}</td>
																	<td>{{ $indicator->year_two_target }}</td>
																	<td>{{ $indicator->year_two_actual }}</td>
																</tr>
																<tr>
																	<td>{{ $indicator->year_three }}</td>
																	<td>{{ $indicator->year_three_target }}</td>
																	<td>{{ $indicator->year_three_actual }}</td>
																</tr>
																<tr>
																	<td>{{ $indicator->year_four }}</td>
																	<td>{{ $indicator->year_four_target }}</td>
																	<td>{{ $indicator->year_four_actual }}</td>
																</tr>
																<tr>
																	<td>{{ $indicator->year_five }}</td>
																	<td>{{ $indicator->year_five_target }}</td>
																	<td>{{ $indicator->year_five_actual }}</td>
																</tr>
																<tr>
																	<td>{{ $indicator->year_six }}</td>
																	<td>{{ $indicator->year_six_target }}</td>
																	<td>{{ $indicator->year_six_actual }}</td>
																</tr>
																<tr>
																	<td>{{ $indicator->year_seven }}</td>
																	<td>{{ $indicator->year_seven_target }}</td>
																	<td>{{ $indicator->year_seven_actual }}</td>
																</tr>
															</table>	
														</td>
														<td>
															<a href="/projects/report/indicator/{{ $indicator->id }}" class="btn btn-warning btn-flat btn-sm"> <i class="fa fa-tasks"></i> View Indicator Chart</a>
															<br>
															<a href="/projects/input/indicators/{{ $indicator->id }}" class="btn btn-primary btn-flat btn-sm"> <i class="fa fa-pencil"></i> Edit Indicator</a>
															<form method="POST" action="/projects/input/indicators">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<input type="hidden" name="indicator_id" value="{{ $indicator->id }}">
																<button type="submit" class="btn btn-danger btn-flat btn-sm" onclick="return window.confirm('Are you sure you want to remove this indicator ?')"> <i class="fa fa-trash"></i> Delete Indicator</button>
															</form>
															
														</td>
													</tr>
												@endforeach
												@if(count($output['indicators']) == 0)
													<tr>
														<td colspan="10" class="text-center"> No Indicators Found!!  <a href="/projects/input/indicators">  Add them here </a>  </td>
													</tr>
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</section>

	</div>
@endsection