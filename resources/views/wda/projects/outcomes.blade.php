@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Projects Outcomes</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	Strategic Plan / Data Input / Outcomes</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

					
							<table class="table table-bordered" style="background: #fff">
								<thead>
									<tr>
										<th></th>
										<th>Program</th>
										<th>Outcome</th>
										<th style="width: 200px">Action</th>
									</tr>
								</thead>
								<tbody>

									@foreach ($outcomes as $outcome)
										<tr>
											<td></td>
											<td>{{ $outcome->program->name }}</td>
											<td>{{ $outcome->name }}</td>
											<td>
												

												<form method="POST">
													{{ csrf_field() }}
													<input type="hidden" name="_method" value="delete">
													<input type="hidden" name="delete_outcome" value="{{ $outcome->id }}">
													
													<a href="/projects/input/outcomes/{{ $outcome->id }}" class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit </a>

													<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to remove this program : \n\n {{ $outcome->name }} ?')"><i class="fa fa-trash"></i> Delete</button>

												</form>

											</td>
										</tr>
									@endforeach

									@if (count($outcomes) == 0)
										<tr>
											<td colspan="4" class="text-center"> No Outcome </td>
										</tr>
									@endif
									
											
								</tbody>
							</table>

						@if (isset($outcome_info))
							
							<form method="POST" action="/projects/input/outcomes">
								{{ csrf_field() }}
								
								<input type="hidden" name="outcome_id" value="{{ $outcome_info->id }}">
								<div class="card card-success">
									<div class="card-header">
										<h3 class="card-title"><i class="fa fa-edit"></i> Edit Outcome</h3>
									</div>
								
									<div class="card-body">
										
										<div class="form-group">
											<label>Program</label>
											<p>{{ $outcome_info->program->name }}</p>
										</div>
										<div class="form-group">
											<label>Outcome</label>
											<input type="text" class="form-control flat" name="u_name" value="{{ $outcome_info->name }}" placeholder="Outome">
										</div>
										
									</div>
									<div class="card-footer">
										<button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i> Save Outcome</button>
									</div>
								</div>
							</form>
						@endif

						@if (!isset($outcome_info))

						<form method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="form" value="outcome">

							<div class="card card-info">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Outcome</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" name="program_id" id="program" required>
											<option value="">Choose Here</option>
											@foreach ($programs as $program)
												<option value="{{ $program->id }}">{{ $program->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Outcome</label>
										<input type="text" class="form-control flat" name="name">
									</div>
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save Outcome</button>
								</div>
							</div>
						</form>

						@endif

					</div>
				</div>
			</div>
		</section>
	</div>

@endsection