@extends('layouts.master')

@section('content')

	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						<div class="col-md-12">
							
							<div class="card card-info">
								<div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
									<h3 class="card-title p-3">
										<i class="fa fa-pie-chart mr-1"></i>
										View Indicator Information
									</h3>
									<ul class="nav nav-pills ml-auto p-2">
										<li class="nav-item">
											<a class="nav-link" href="/projects/report/more/{{ $indicator->output->id }}"><i class="fa fa-arrow-left"></i> Go Back</a>
										</li>
									</ul>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Program</label>
										<p>{{ $indicator->output->outcome->program->name }}</p>
									</div>			
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Outcome</label>
										<p>{{ $indicator->output->outcome->name }}</p>
									</div>			
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Output</label>
										<p>{{ $indicator->output->name }}</p>
									</div>
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Indicator</label>
										<p>{{ $indicator->name }}</p>
									</div>

									<canvas id="barChart" style="height:230px"></canvas>
									<br>
									<table class="table">
										<tr>
											<th colspan="2">Year zero</th>
											<th colspan="2">Year 1</th>
											<th colspan="2">Year 2</th>
											<th colspan="2">Year 3</th>
											<th colspan="2">Year 4</th>
											<th colspan="2">Year 5</th>
											<th colspan="2">Year 6</th>
										</tr>
										<tr>
											<th colspan="2">{{ $indicator->year_one }}</th>
											<th colspan="2">{{ $indicator->year_two }}</th>
											<th colspan="2">{{ $indicator->year_three }}</th>
											<th colspan="2">{{ $indicator->year_four }}</th>
											<th colspan="2">{{ $indicator->year_five }}</th>
											<th colspan="2">{{ $indicator->year_six }}</th>
											<th colspan="2">{{ $indicator->year_seven }}</th>
										</tr>
										<tr>
											<td>{{ $indicator->year_one_target }}</td>
											<td>{{ $indicator->year_one_actual }}</td>
											<td>{{ $indicator->year_two_target }}</td>
											<td>{{ $indicator->year_two_actual }}</td>
											<td>{{ $indicator->year_three_target }}</td>
											<td>{{ $indicator->year_three_actual }}</td>
											<td>{{ $indicator->year_four_target }}</td>
											<td>{{ $indicator->year_four_actual }}</td>
											<td>{{ $indicator->year_five_target }}</td>
											<td>{{ $indicator->year_five_actual }}</td>
											<td>{{ $indicator->year_six_target }}</td>
											<td>{{ $indicator->year_six_actual }}</td>
											<td>{{ $indicator->year_seven_target }}</td>
											<td>{{ $indicator->year_seven_actual }}</td>
										</tr>
									</table>
								</div>
								<div class="card-footer">
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	

    @php
    	$labels = [];
    	$targets = [];
    	$actuals = [];

    	if(substr($indicator->year_one_target, -1) == "%") {
            $targets[] = substr($indicator->year_one_target, 0, strlen($indicator->year_one_target) - 1); 
        } else {
            $targets[] = $indicator->year_one_target;
        }

        if(substr($indicator->year_two_target, -1) == "%") {
            $targets[] = substr($indicator->year_two_target, 0, strlen($indicator->year_two_target) - 1); 
        } else {
            $targets[] = $indicator->year_two_target;
        }

        if(substr($indicator->year_three_target, -1) == "%") {
            $targets[] = substr($indicator->year_three_target, 0, strlen($indicator->year_three_target) - 1); 
        } else {
            $targets[] = $indicator->year_three_target;
        }

        if(substr($indicator->year_four_target, -1) == "%") {
            $targets[] = substr($indicator->year_four_target, 0, strlen($indicator->year_four_target) - 1); 
        } else {
            $targets[] = $indicator->year_four_target;
        }

        if(substr($indicator->year_five_target, -1) == "%") {
            $targets[] = substr($indicator->year_five_target, 0, strlen($indicator->year_five_target) - 1); 
        } else {
            $targets[] = $indicator->year_five_target;
        }

        if(substr($indicator->year_six_target, -1) == "%") {
            $targets[] = substr($indicator->year_six_target, 0, strlen($indicator->year_six_target) - 1); 
        } else {
            $targets[] = $indicator->year_six_target;
        }

        if(substr($indicator->year_seven_target, -1) == "%") {
            $targets[] = substr($indicator->year_seven_target, 0, strlen($indicator->year_seven_target) - 1); 
        } else {
            $targets[] = $indicator->year_seven_target;
        }


        if(substr($indicator->year_one_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_one_actual, 0, strlen($indicator->year_one_actual) - 1); 
        } else {
            $actuals[] = $indicator->year_one_actual;
        }


        if(substr($indicator->year_two_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_two_actual, 0, strlen($indicator->year_two_actual) - 1); 
        } else {
            $actuals[] = $indicator->year_two_actual;
        }

        if(substr($indicator->year_three_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_three_actual, 0, strlen($indicator->year_three_actual) - 1); 
        } else {
            $actuals[] = $indicator->year_three_actual;
        }

        if(substr($indicator->year_four_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_four_actual, 0, strlen($indicator->year_four_actual) - 1); 
        } else {
            $actuals[] = $indicator->year_four_actual;
        }

        if(substr($indicator->year_five_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_five_actual, 0, strlen($indicator->year_five_actual) - 1); 
        } else {
            $actuals[] = $indicator->year_five_actual;
        }

        if(substr($indicator->year_six_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_six_actual, 0, strlen($indicator->year_six_actual) - 1); 
        } else {
            $actuals[] = $indicator->year_six_actual;
        }

        if(substr($indicator->year_seven_actual, -1) == "%") {
            $actuals[] = substr($indicator->year_seven_actual, 0, strlen($indicator->year_seven_actual) - 1); 
        } else {
            $actuals[] = $indicator->year_seven_actual;
        }
    	
    @endphp

    <script type="text/javascript">
		//-------------
    //- BAR CHART -
    //-------------

    var areaChartData = {
      labels  : ['Year zero', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5', 'Year 6'],
      datasets: [
        {
			label               : 'Target',
			fillColor           : 'rgba(210, 214, 222, 1)',
			strokeColor         : 'rgba(210, 214, 222, 1)',
			pointColor          : 'rgba(210, 214, 222, 1)',
			pointStrokeColor    : '#c1c7d1',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(220,220,220,1)',
			data                : [{{ implode(', ', $targets) }}],
			backgroundColor		: 'rgba(54, 162, 235, 1)',
        },
        {
			label               : 'Actual',
			fillColor           : 'rgba(60,141,188,0.9)',
			strokeColor         : 'rgba(60,141,188,0.8)',
			pointColor          : '#3b8bba',
			pointStrokeColor    : 'rgba(60,141,188,1)',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(60,141,188,1)',
			data                : [{{ implode(', ', $actuals) }}],
			backgroundColor		: 'rgba(255,99,132,1)',
        }
      ]
    }

    var ctx = document.getElementById("barChart").getContext('2d');

    var myChart = new Chart(ctx, {
            type: 'bar',
            data: areaChartData,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
	</script>
@endsection