@extends('wda.layout.main')

@section('panel-title', "Program / Project Results")

@section('htmlheader_title', "Program / Project Results")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($result_info))
                <form method="POST" action="{{ route('wda.pr.results.index') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="result_id" value="{{ $result_info->id }}">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Result</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Program / Project</label>
                                <select name="program_id" class="form-control flat">
                                    @foreach ($programs as $program)
                                        <option value="{{ $program->id }}"
                                                @if($result_info->program_id == $program->id) selected @endif >{{ $program->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Result</label>
                                <input type="text" class="form-control flat" name="name"
                                       value="{{ $result_info->name }}" placeholder="Outome">
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i>
                                Save Result
                            </button>
                        </div>
                    </div>
                </form>
            @else
                <form method="POST" action="{{ route('wda.pr.results.store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="form" value="sp_result">

                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-plus"></i> Add Result</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Project</label>
                                <select class="form-control flat" name="program_id" id="project" required>
                                    <option value="">Choose Here</option>
                                    @foreach ($projects as $project)
                                        <option value="{{ $project->id }}">{{ $project->project_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Intervention</label>
                                <select class="form-control flat" name="program_id" id="project_program" required>
                                    <option value="">Choose Here</option>
                                    @foreach ($programs as $program)
                                        <option value="{{ $program->id }}">{{ $program->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Result</label>
                                <input type="text" class="form-control flat" name="name" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save
                                Result
                            </button>
                        </div>
                    </div>
                </form>
            @endif
            <table class="table table-bordered" style="background: #fff">
                <thead>
                <tr>
                    <th style="width: 50px" class="text-center">#</th>
                    <th>Project Name</th>
                    <th style="width: 500px">Intervention</th>
                    <th>Result</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($results as $result)
                    <tr>
                        <td class="text-center">{{ $i }}</td>
                        <td>{{ $result->program->project->project_name or "" }}</td>
                        <td>{{ $result->program->name or "" }}</td>
                        <td>{{ $result->name }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.pr.results.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_result" value="{{ $result->id }}">

                                <a href="{{ route('wda.pr.results.edit',$result->id) }}"
                                   class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit </a>

                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this result :\n{{ $result->name }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                @if (count($results) == 0)
                    <tr>
                        <td colspan="5" class="text-center"> No Result Found!! Add them below</td>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>
@endsection