@extends('wda.layout.main')

@section('panel-title', "Programs / Projects Report")

@section('htmlheader_title', "Programs / Projects Report")

@section('l-style')
	@parent
	<link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<form method="POST" action="{{ route('wda.pr.report.index') }}">
						{{ csrf_field() }}
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Projects</label>
							<div class="col-md-8">
								<select class="form-control flat" name="project_id">
									@foreach ($projects as $project)
										<option value="{{ $project->id }}">{{ $project->project_name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-1">
								<button type="submit" class="btn btn-primary btn-group-sm btn-flat"><i class="fa fa-list"></i> View </button>
							</div>
						</div>
					</form>

					{{--Start search content--}}
					<div id="filterSearch" class="box" style="display: none">
						<div class="box-header">
							Advanced Search
						</div>
						<div class="box-body">
							<div class="panel panel-default">
								<div class="panel-body">
									<form class="form-inline" action="{{ route('wda.pr.report.index') }}"  method="get" role="form">
										<div class="form-group row">
											<label class="col-md-4 col-form-label">
												Programs </label>
											<div class="col-md-2">
												<select class="form-control flat" name="search_programid">
													<option></option>
													@foreach ($datas['programs'] as $program)
														<option value="{{ $program->id }}">{{ $program->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-6 col-form-label">
												Result </label>
											<div class="col-md-2">
												<select class="form-control flat" name="search_result">
													<option></option>
													@foreach ($datas['results'] as $result)
														<option value="{{ $result->id }}">{{ $result->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row" style="margin-left: 57px">
											<label class="col-md-3 col-form-label">
												Indicator </label>
											<div class="col-md-2">
												<select class="form-control flat" name="search_indicator">
													<option></option>
													@foreach ($datas['indicators'] as $indicator)
														<option value="{{ $indicator->id }}">{{ $indicator->name }}</option>
													@endforeach
												</select>
											</div>
										</div>

										<div class="form-group"  style="margin-left: 12px;">
											<button type="submit" class="btn btn-primary filter-col">
												<span class="fa fa-search-plus"></span> Search
											</button>
										</div>
										<div class="form-group" style="margin-left: 12px;">
											<button id="filterSearchremove"  type="button" class="btn btn-warning filter-col">
												<span class="fa fa-remove"></span> Cancel
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					{{--End search content--}}
					{{--Start Columns content--}}
					<div id="filtercolumns" class="box"  style="display: none">
						<div class="box-body">

							<form action="{{ route('wda.pr.report.index') }}" class="form-inline" role="form" method="get">
								<div class="container">
									<div class="row">
										<div class="col-md-10">
											<div class="form-group">
												<select multiple="multiple" class="form-control flat" id="my-select" name="columns_select[]">
													@foreach($columns['indicators'] as $column)
														<option value="{{'projects_indicators.'.$column->Field}}">{{$column->Field}} (Indicators)</option>
													@endforeach
													@foreach($columns['programs'] as $program)
														<option value="{{'project_programs.'.$program->Field}}">{{$program->Field}} (Programs)</option>
													@endforeach
													@foreach($columns['results'] as $program)
														<option value="{{'project_results.'.$program->Field}}">{{$program->Field}} (Results)</option>
													@endforeach
												</select> <br/>

											</div>
										</div>
									</div><br/>
									<div class="form-group row">
										<label class="col-md-2 col-form-label">Project </label>
										<div class="col-md-2">
											<select class="form-control flat" name="mySelectId">
												@foreach ($projects as $project)
													<option value="{{ $project->id }}">{{ $project->project_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group" style="margin-top: 12px; ">
												<button type="submit" class="form-control flat btn btn-primary filter-col">
													<span class="fa fa-search-plush"></span>Search Now
												</button>
											</div>
										</div>
										<div class="col-md-2" style="margin-top: 12px; ">
											<div class="form-group">
												<button type="button" id="columnremove" class="form-control flat btn btn-warning filter-col">
													<span class="fa fa-remove"></span> Cancel
												</button>
											</div>
										</div>
									</div>
								</div>

							</form>
						</div>
					</div>
					{{--End Columns content--}}
				</div>

				<div class="box-body">
					@if (isset($programs))
						<div style="overflow-y: auto;">
							<table  class="table table-bordered table-striped" id="dataTable">
								<thead style="background: #fff">
								<tr>
									<th style="width: 40px">#</th>
									<th style="width: 250px">Intervention</th>
									<th style="width: 250px">Result</th>
									<th style="width: 200px">Indicator</th>
									<th style="width: 200px">Baseline</th>
									<th>
										<table class="table">
											<tr style="border: none;background-color: white">
												<td>Year</td>
												<td>Target</td>
												<td>Actual</td>
											</tr></table></th>
								</tr>
								</thead>
								<tbody>
								@php
									$checkObject=array();
								@endphp
								@foreach ($programs as $program)
									<tr>
										@if(in_array($program->programName,$checkObject))
											<td><a href="{{ route('wda.pr.report.result', $program->id )}}" class="btn btn-warning btn-sm btn-flat">	<i class="fa fa-bar-chart"></i></a>
											<td></td>
											<td> {{$program->resultName}}</td>
										@else
											<td><a href="{{ route('wda.pr.report.result', $program->id )}}" class="btn btn-warning btn-sm btn-flat">	<i class="fa fa-bar-chart"></i></a>
											</td>
											<td>{{$program->programName}}</td>
											<td> {{$program->resultName}}</td>

										@endif
										<td><span class="badge bg-red">{{$program->indicatorName}}</span></td>
										<td><span class="badge bg-red">{{$program->baseline}}</span></td>
										<td>
											<table class="table">
												<tr>
													<td>Y1 :{{$program->year_one}}</td>
													<td style="background-color: orange">{{ $program->year_one_target }}</td>
													@if($program->year_one_actual < $program->year_one_target)
														<td style="background-color: red">{{ $program->year_one_actual }}</td>
													@elseif($program->year_one_actual > $program->year_one_target)
														<td style="background-color: green">{{ $program->year_one_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_one_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y2 :{{$program->year_two}}</td>
													<td style="background-color: orange">{{ $program->year_two_target }}</td>
													@if($program->year_two_actual < $program->year_two_target)
														<td style="background-color: red">{{ $program->year_two_actual }}</td>
													@elseif($program->year_two_actual > $program->year_two_target)
														<td style="background-color: green">{{ $program->year_two_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_two_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y3 :{{$program->year_three}}</td>
													<td style="background-color: orange">{{ $program->year_three_target }}</td>
													@if($program->year_three_actual < $program->year_three_target)
														<td style="background-color: red">{{ $program->year_three_actual }}</td>
													@elseif($program->year_three_actual > $program->year_three_target)
														<td style="background-color: green">{{ $program->year_three_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_three_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y4 :{{$program->year_four}}</td>
													<td style="background-color: orange">{{ $program->year_four_target }}</td>
													@if($program->year_four_actual < $program->year_four_target)
														<td style="background-color: red">{{ $program->year_four_actual }}</td>
													@elseif($program->year_four_actual > $program->year_four_target)
														<td style="background-color: green">{{ $program->year_four_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_four_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y5 :{{$program->year_five}}</td>
													<td style="background-color: orange">{{ $program->year_five_target }}</td>
													@if($program->year_five_actual < $program->year_five_target)
														<td style="background-color: red">{{ $program->year_five_actual }}</td>
													@elseif($program->year_five_actual > $program->year_five_target)
														<td style="background-color: green">{{ $program->year_five_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_five_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y6 :{{$program->year_six}}</td>
													<td style="background-color: orange">{{ $program->year_six_target }}</td>
													@if($program->year_six_actual < $program->year_six_target)
														<td style="background-color: red">{{ $program->year_six_actual }}</td>
													@elseif($program->year_six_actual > $program->year_six_target)
														<td style="background-color: green">{{ $program->year_six_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_six_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y7 :{{$program->year_seven}}</td>
													<td style="background-color: orange">{{ $program->year_seven_target }}</td>
													@if($program->year_seven_actual < $program->year_seven_target)
														<td style="background-color: red">{{ $program->year_seven_actual }}</td>
													@elseif($program->year_seven_actual > $program->year_seven_target)
														<td style="background-color: green">{{ $program->year_seven_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_seven_actual }}</td>
													@endif
												</tr>
											</table>
										</td>

									</tr>
									@php array_push($checkObject,$program->id) @endphp
									@php array_push($checkObject,$program->programName) @endphp
								@endforeach
								</tbody>
							</table>
						</div>


					@endif

					{{--search and columns Looking--}}
					@if (isset($searchprograms))
						<div style="overflow-y: auto;">
							<table  class="table table-bordered table-striped" id="dataTable">
								<thead style="background: #fff">
								<tr>
									<th style="width: 40px">#</th>
									<th style="width: 250px">Intervention</th>
									<th style="width: 250px">Result</th>
									<th style="width: 200px">Indicator</th>
									<th style="width: 200px">Baseline</th>
									<th>
										<table class="table">
											<tr style="border: none;background-color: white">
												<td>Year</td>
												<td>Target</td>
												<td>Actual</td>
											</tr></table></th>
								</tr>
								</thead>
								<tbody>
								@php
									$checkObject=array();
                                 $programs =$searchprograms;
								@endphp
								@foreach ($programs as $program)

									<tr>
										@if(in_array($program->programName,$checkObject))
											<td><a href="{{ route('wda.pr.report.result', $program->id )}}" class="btn btn-warning btn-sm btn-flat">	<i class="fa fa-bar-chart"></i></a>
											<td></td>
											<td> {{$program->resultName}}</td>
										@else
											<td><a href="{{ route('wda.pr.report.result', $program->id )}}" class="btn btn-warning btn-sm btn-flat">	<i class="fa fa-bar-chart"></i></a>
											</td>
											<td>{{$program->programName}}</td>
											<td> {{$program->resultName}}</td>

										@endif
										<td><span class="badge bg-red">{{$program->indicatorName}}</span></td>
										<td><span class="badge bg-red">{{$program->baseline}}</span></td>
										<td>
											<table class="table">
												<tr>
													<td>Y1 :{{$program->year_one}}</td>
													<td style="background-color: orange">{{ $program->year_one_target }}</td>
													@if($program->year_one_actual < $program->year_one_target)
														<td style="background-color: red">{{ $program->year_one_actual }}</td>
													@elseif($program->year_one_actual > $program->year_one_target)
														<td style="background-color: green">{{ $program->year_one_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_one_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y2 :{{$program->year_two}}</td>
													<td style="background-color: orange">{{ $program->year_two_target }}</td>
													@if($program->year_two_actual < $program->year_two_target)
														<td style="background-color: red">{{ $program->year_two_actual }}</td>
													@elseif($program->year_two_actual > $program->year_two_target)
														<td style="background-color: green">{{ $program->year_two_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_two_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y3 :{{$program->year_three}}</td>
													<td style="background-color: orange">{{ $program->year_three_target }}</td>
													@if($program->year_three_actual < $program->year_three_target)
														<td style="background-color: red">{{ $program->year_three_actual }}</td>
													@elseif($program->year_three_actual > $program->year_three_target)
														<td style="background-color: green">{{ $program->year_three_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_three_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y4 :{{$program->year_four}}</td>
													<td style="background-color: orange">{{ $program->year_four_target }}</td>
													@if($program->year_four_actual < $program->year_four_target)
														<td style="background-color: red">{{ $program->year_four_actual }}</td>
													@elseif($program->year_four_actual > $program->year_four_target)
														<td style="background-color: green">{{ $program->year_four_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_four_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y5 :{{$program->year_five}}</td>
													<td style="background-color: orange">{{ $program->year_five_target }}</td>
													@if($program->year_five_actual < $program->year_five_target)
														<td style="background-color: red">{{ $program->year_five_actual }}</td>
													@elseif($program->year_five_actual > $program->year_five_target)
														<td style="background-color: green">{{ $program->year_five_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_five_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y6 :{{$program->year_six}}</td>
													<td style="background-color: orange">{{ $program->year_six_target }}</td>
													@if($program->year_six_actual < $program->year_six_target)
														<td style="background-color: red">{{ $program->year_six_actual }}</td>
													@elseif($program->year_six_actual > $program->year_six_target)
														<td style="background-color: green">{{ $program->year_six_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_six_actual }}</td>
													@endif
												</tr>
												<tr>
													<td>Y7 :{{$program->year_seven}}</td>
													<td style="background-color: orange">{{ $program->year_seven_target }}</td>
													@if($program->year_seven_actual < $program->year_seven_target)
														<td style="background-color: red">{{ $program->year_seven_actual }}</td>
													@elseif($program->year_seven_actual > $program->year_seven_target)
														<td style="background-color: green">{{ $program->year_seven_actual }}</td>
													@else
														<td style="background-color: yellow">{{ $program->year_seven_actual }}</td>
													@endif
												</tr>
											</table>
										</td>

									</tr>
									@php array_push($checkObject,$program->id) @endphp
									@php array_push($checkObject,$program->programName) @endphp
								@endforeach
								</tbody>
							</table>
						</div>

					@endif

					@if (isset($DataColumnsprograms))

						<div style="overflow-y: auto;">
							<table class="table table-bordered table-striped" id="dataTable">
								<thead>
								<tr>
									@php
										$columns =explode(',',$columnsDisplays);
									@endphp
									<th>#</th>
									@foreach($columns as $columndata)
										@php
											$columntodisplay =strtoupper(str_replace_last('_',' ',$columndata));
										@endphp
										<th>{{str_replace_last('_',' ',$columntodisplay)}}</th>
									@endforeach

								</tr>
								</thead>
								<tbody>
								@foreach ($DataColumnsprograms as $program)
									<tr>
										@php
											$i=0;
                                              echo '<td>
                                                <a href="'.route("wda.pr.report.result", $program->programId ).'" class="btn btn-warning btn-sm btn-flat">	<i class="fa fa-bar-chart"></i></a>
                                            </td>';
                                        foreach($columns as $columndata){
                                        $value=trim($columndata);
                                         if($i < count ($columns)) {

                                         echo '<td>'.$program->$value.'</td>
                                            ';

                                         }
                                                $i++;
                                        }

										@endphp


									</tr>

								@endforeach
								</tbody>
							</table>
						</div>
					@endif

				</div>
			</div>
		</div>
	</div>
@endsection
@section('l-scripts')
	@parent
	<script src="{{ asset('js/jquery.multi-select.js') }}"></script>
	<script>
        $(document).ready(function() {
            $('#my-select').multiSelect({
                selectableHeader: "<div class='custom-header'>Selectable Columns</div>",
                selectionHeader: "<div class='custom-header'>Selection Columns</div>"
            });
        } );
	</script>
@endsection