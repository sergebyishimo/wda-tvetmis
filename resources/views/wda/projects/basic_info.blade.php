@extends('wda.layout.main')

@section('panel-title', "Program / Project Information")

@section('htmlheader_title', "Program / Project Information")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($project_info))

                <form method="POST" enctype="multipart/form-data" action="{{ route('wda.pr.info.store') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="project_id" value="{{ $project_info->id }}">
                    <div class="box box-success">
                        <div class="box-header">
                            <h4 class="box-title"><i class="fa fa-edit"></i> Edit Project</h4>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Project Name</label>
                                <input type="text" class="form-control flat" name="project_name"
                                       placeholder="Project Name" required value="{{ $project_info->project_name }}">
                            </div>
                            <div class="form-group">
                                <label>Project Manager</label>
                                <input type="text" class="form-control flat" name="project_manager"
                                       value="{{ $project_info->project_manager }}" required>
                            </div>
                            <div class="form-group">
                                <label>Project Duration</label>
                                <input type="text" class="form-control flat" name="duration"
                                       placeholder="Project Duration" value="{{ $project_info->duration }}" required>
                            </div>
                            <div class="form-group">
                                <label>Brief Description</label>
                                <textarea class="form-control flat" name="brief_description"
                                          placeholder="Brief Description">{{ $project_info->brief_description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Attachment</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="upload_file"
                                               id="exampleInputFile">
                                        <label class="custom-file-label flat" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Finance Type</label>
                                <select class="form-control flat" name="finance_type[]" multiple="-1">
                                    <option value="">Choose Here</option>
                                    @php
                                        $finance_types = explode(', ', $project_info->finance_type);
                                    @endphp
                                    @foreach ($fins as $fin)
                                        <option value="{{ $fin->id }}"
                                                @if(array_search($fin->id, $finance_types)  !== false) selected @endif>{{ $fin->finance_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Development Partner</label>
                                <select class="form-control flat" name="development_partner[]" multiple="-1">
                                    <option value="">Choose Here</option>
                                    @php
                                        $development_partners = explode(', ', $project_info->development_partner);
                                    @endphp
                                    @foreach ($devs as $dev)
                                        <option value="{{ $dev->id }}"
                                                @if(array_search($dev->id, $development_partners) !== false) selected @endif>{{ $dev->development_partner }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Counterpart</label>
                                <input type="text" class="form-control flat" name="budget_agency_name"
                                       placeholder="Counterpart" value="{{ $project_info->budget_agency_name }}">
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-save"></i>
                                Save Project
                            </button>
                        </div>
                    </div>
                </form>

            @endif
            @if (!isset($project_info))
                <div class="mb-4">
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample">
                        Add New Project
                    </button>
                    <div class="collapse" id="collapseExample">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('wda.pr.info.store') }}">
                            {{ csrf_field() }}

                            <div class="box box-info mt-3">
                                <div class="box-header">
                                    <h4 class="box-title"><i class="fa fa-plus"></i> Add Project</h4>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Program / Project Name</label>
                                        <input type="text" class="form-control flat" name="project_name"
                                               placeholder="Project Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Project Manager</label>
                                        <input type="text" class="form-control flat" name="project_manager"
                                               placeholder="Project Manager" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Program / Project Duration</label>
                                        <input type="text" class="form-control flat" name="duration"
                                               placeholder="Project Duration">
                                    </div>
                                    <div class="form-group">
                                        <label>Brief Description</label>
                                        <textarea class="form-control flat" name="brief_description"
                                                  placeholder="Brief Description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Attachment</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="upload_file"
                                                       id="exampleInputFile" required>
                                                <label class="custom-file-label flat" for="exampleInputFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Finance Type</label>
                                        <select class="form-control flat" name="finance_type[]" multiple="-1">
                                            <option value="">Choose Here</option>
                                            @foreach ($fins as $fin)
                                                <option value="{{ $fin->id }}">{{ $fin->finance_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Development Partner</label>
                                        <select class="form-control flat" name="development_partner[]" multiple="-1">
                                            <option value="">Choose Here</option>
                                            @foreach ($devs as $dev)
                                                <option value="{{ $dev->id }}">{{ $dev->development_partner }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Counterpart</label>
                                        <input type="text" class="form-control flat" name="budget_agency_name"
                                               placeholder="Counterpart">
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info btn-block btn-flat"><i
                                                class="fa fa-save"></i>
                                        Save Project
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            <table class="table table-bordered mb-3" id="dataTableBtn">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Program / Project Name</th>
                    <th>Project Manager</th>
                    <th>Duration</th>
                    <th style="width: 150px">Brief Description</th>
                    <th>Finance Type</th>
                    <th>Development Partner</th>
                    <th>Counterpart</th>
                    <th>Attachment</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($projects as $project)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $project->project_name }}</td>
                        <td>{{ $project->project_manager }}</td>
                        <td>{{ $project->duration }}</td>
                        <td>{{ $project->brief_description }}</td>

                        @php
                            $finance_types = explode(', ', $project->finance_type);
                            $fin = [];
                        @endphp
                        @foreach ($finance_types as $finance_type)
                            @php
                                $model = \App\Model\Accr\ProjectFinanceTypeSource::find($finance_type);
                                if($model) {
                                    $fin[] = \App\Model\Accr\ProjectFinanceTypeSource::find($finance_type)->finance_type;
                                }
                                
                            @endphp
                        @endforeach
                        <td>{{ implode(', ', $fin) }}</td>
                        @php
                            $development_partners = explode(', ', $project->development_partner);
                            $dev = [];
                        @endphp
                        @foreach ($development_partners as $development_partner)
                            @php
                                $model_sec = \App\Model\Accr\DevelopmentPartner::find($development_partner);
                                if($model_sec) {
                                    $dev[] = \App\Model\Accr\DevelopmentPartner::find($development_partner)->development_partner;
                                }
                            @endphp
                        @endforeach
                        <td>{{ implode(', ', $dev) }}</td>
                        <td>{{ $project->budget_agency_name }}</td>
                        <td><a href="{{ asset("storage/".$project->attachment_name) }}"
                               target="_blank">View File</a></td>
                        <td>
                            <form method="POST" action="{{ route('wda.pr.info.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_project" value="{{ $project->id }}">

                                <a href="{{ route('wda.pr.info.edit', $project->id) }}"
                                   class="btn btn-primary btn-sm btn-flat"><i
                                            class="fa fa-edit"></i> Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure yo want to remove this project :\n{{ $project->project_name }}?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection