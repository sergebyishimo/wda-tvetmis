@extends('wda.layout.main')

@section('panel-title', "Program / Project Interventions")

@section('htmlheader_title', "Program / Project Interventions")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Program / Project</th>
                    <th>Intervention</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @forelse ($programs as $program)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $program->project->project_name or "" }}</td>
                        <td>{{ $program->name }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.pr.programs.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_pro" value="{{ $program->id }}">

                                <a href="{{ route('wda.pr.programs.edit', $program->id) }}"
                                   class="btn btn-primary btn-sm btn-flat">
                                    <i class="fa fa-edit"></i> Edit </a>
                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this program :\n{{ $program->name }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center">No Data Available !</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            @if ( ! isset($program_info))
                <form method="POST" action="{{ route('wda.pr.programs.store') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-5">
                            <select class="form-control flat" name="project_id">
                                @foreach ($projects as $project)
                                    <option value="{{ $project->id }}">{{ $project->project_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            <input type="text" class="form-control flat" name="name" placeholder="Intervention">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-save"></i>
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            @endif

            @if (isset($program_info))

                <form method="POST" action="{{ route('wda.pr.programs.store') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="program_id" value="{{ $program_info->id }}">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Program</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label>Program / Project Name</label>
                                <p>{{ $program_info->project->project_name }}</p>
                            </div>
                            <div class="form-group">
                                <label>Intervention</label>
                                <input type="text" class="form-control flat" name="u_name"
                                       value="{{ $program_info->name }}" placeholder="Intervention">
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i>
                                Save Strategic Plan Program
                            </button>
                        </div>
                    </div>
                </form>
            @endif

        </div>
    </div>
@endsection