@extends('wda.layout.main')

@section('panel-title', "Program / Project Indicators")

@section('htmlheader_title', "Program / Project Indicators")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($indicator_info))
                {{-- expr --}}
                <form method="POST" action="{{ route('wda.pr.indicators.store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="indicator" value="indicator">
                    <input type="hidden" name="indicator_id" value="{{ $indicator_info->id }}">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Indicator</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Intervention</label>
                                <p>{{ $indicator_info->result->program->name }}</p>
                            </div>
                            <div class="form-group">
                                <label>Result</label>
                                <p>{{ $indicator_info->result->name }}</p>
                            </div>
                            <div class="form-group">
                                <label>Indicator</label>
                                <input type="text" class="form-control flat" name="name" placeholder="Indicator"
                                       value="{{ $indicator_info->name }}" required>
                            </div>
                            <div class="form-group">
                                <label>Baseline</label>
                                <input type="text" class="form-control flat" name="baseline" placeholder="Baseline"
                                       value="{{ $indicator_info->baseline }}">
                            </div>
                            <div class="form-group">
                                <label>Year / Target / Actual</label>
                                <div class="row">
                                    <div class="col-md-6"
                                         style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_one"
                                                       placeholder="Year 1" value="{{ $indicator_info->year_one }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_one_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->year_one_target }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_one_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->year_one_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"
                                         style="background: #e8e8e8;padding: 5px;border-left: 4px solid #fff">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_two"
                                                       placeholder="Year 2" value="{{ $indicator_info->year_two }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_two_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->year_two_target }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_two_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->year_two_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label>Year / Target / Actual</label>
                                <div class="row">
                                    <div class="col-md-6"
                                         style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_three"
                                                       placeholder="Year 3" value="{{ $indicator_info->year_three }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_three_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->year_three_target }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_three_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->year_three_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"
                                         style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_four"
                                                       placeholder="Year 4" value="{{ $indicator_info->year_four }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_four_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->year_four_target }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_four_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->year_four_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label>Year / Target / Actual</label>
                                <div class="row">
                                    <div class="col-md-6"
                                         style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_five"
                                                       placeholder="Year 5" value="{{ $indicator_info->year_five }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_five_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->year_five_target }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_five_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->year_five_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"
                                         style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_six"
                                                       placeholder="Year 6" value="{{ $indicator_info->year_six }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_six_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->year_six_target }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_six_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->year_six_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Year / Target / Actual</label>
                                <div class="row">
                                    <div class="col-md-6"
                                         style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_seven"
                                                       placeholder="Year 7" value="{{ $indicator_info->year_seven }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_seven_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->year_seven_target }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control flat" name="year_seven_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->year_seven_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Activities to Deliver Result </label>
                                <textarea class="form-control flat" name="activities_to_deliver_output"
                                          placeholder="Activities To Deliver Result">{{ $indicator_info->activities_to_deliver_output }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Stakeholders </label>
                                <input type="text" class="form-control flat" name="stakeholders"
                                       placeholder="Stakeholders" value="{{ $indicator_info->stakeholders }}">
                            </div>
                            <div class="form-group">
                                <label>Budget </label>
                                <input type="number" class="form-control flat" name="budget" placeholder="Available"
                                       value="{{ $indicator_info->budget }}">
                            </div>
                            <div class="form-group">
                                <label>Budget Spent </label>
                                <input type="number" class="form-control flat" name="budget_spent"
                                       placeholder="Budget Spent" value="{{ $indicator_info->budget_spent }}">
                            </div>
                            <div class="form-group">
                                <label>Narrative Progress </label>
                                <textarea class="form-control flat" name="narrative_progress"
                                          placeholder="Narrative Progress">{{ $indicator_info->narrative_progress }}</textarea>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i>
                                Save Strategic Plan Indicator
                            </button>
                        </div>
                    </div>
                </form>
            @endif

            @if (!isset($indicator_info))
                {{-- expr --}}
                <div class="mb-4">
                    <a role="button" class="btn btn-primary"
                       @if(request()->has('o') && request()->get('o') != 'l')
                       href="?o=l"
                       @else
                       href="?o=in"
                            @endif>
                        Add Indicator
                    </a>
                    <div class="collapse {{ request()->get('o') }}" id="collapseExample">
                        <form method="POST" action="{{ route('wda.pr.indicators.store') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="indicator" value="indicator">
                            <div class="box box-info mt-3">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-plus"></i> Add Indicator</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Project</label>
                                        <select class="form-control flat" id="project">
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach ($projects as $project)
                                                <option value="{{ $project->id }}">{{ $project->project_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Intervention</label>
                                        <select class="form-control flat" id="project_program">
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach ($programs as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Result</label>
                                        <select class="form-control flat" name="result_id" id="project_results"
                                                required>

                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label>Indicator</label>
                                        <input type="text" class="form-control flat" name="name" placeholder="Indicator"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <label>Baseline</label>
                                        <input type="text" class="form-control flat" name="baseline"
                                               placeholder="Baseline">
                                    </div>
                                    <div class="form-group">
                                        <label>Year / Target / Actual</label>
                                        <div class="row">
                                            <div class="col-md-6"
                                                 style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat" name="year_one"
                                                               placeholder="Year 1">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_one_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_one_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"
                                                 style="background: #e8e8e8;padding: 5px;border-left: 4px solid #fff">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat" name="year_two"
                                                               placeholder="Year 2">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_two_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_two_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label>Year / Target / Actual</label>
                                        <div class="row">
                                            <div class="col-md-6"
                                                 style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat" name="year_three"
                                                               placeholder="Year 3">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_three_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_three_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"
                                                 style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat" name="year_four"
                                                               placeholder="Year 4">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_four_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_four_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label>Year / Target / Actual</label>
                                        <div class="row">
                                            <div class="col-md-6"
                                                 style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat" name="year_five"
                                                               placeholder="Year 5">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_five_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_five_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"
                                                 style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat" name="year_six"
                                                               placeholder="Year 6">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_six_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_six_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Year / Target / Actual</label>
                                        <div class="row">
                                            <div class="col-md-6"
                                                 style="background: #e8e8e8;padding: 5px;border-right: 4px solid #fff">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat" name="year_seven"
                                                               placeholder="Year 7">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_seven_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control flat"
                                                               name="year_seven_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Activities to Deliver Result </label>
                                        <textarea class="form-control flat" name="activities_to_deliver_output"
                                                  placeholder="Activities To Deliver Result"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Stakeholders </label>
                                        <input type="text" class="form-control flat" name="stakeholders"
                                               placeholder="Stakeholders">
                                    </div>
                                    <div class="form-group">
                                        <label>Budget </label>
                                        <input type="number" class="form-control flat" name="budget"
                                               placeholder="Available">
                                    </div>
                                    <div class="form-group">
                                        <label>Budget Spent </label>
                                        <input type="number" class="form-control flat" name="budget_spent"
                                               placeholder="Budget Spent">
                                    </div>
                                    <div class="form-group">
                                        <label>Narrative Progress </label>
                                        <textarea class="form-control flat" name="narrative_progress"
                                                  placeholder="Narrative Progress"></textarea>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info btn-flat btn-block"><i
                                                class="fa fa-save"></i>
                                        Save Strategic Plan Indicator
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Project</th>
                    <th>Intervention</th>
                    <th>Result</th>
                    <th>Indicator</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($indicators as $indicator)

                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $indicator->result->program->project->project_name or "" }}</td>
                        <td>{{ $indicator->result->program->name or "" }}</td>
                        <td>{{ $indicator->result->name or "" }}</td>
                        <td>{{ $indicator->name }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.pr.indicators.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_indicator" value="{{ $indicator->id }}">

                                <a href="{{ route('wda.pr.indicators.edit',$indicator->id) }}"
                                   class="btn btn-primary btn-sm btn-flat">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this program :\n{{ $indicator->name }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                @if (count($indicators) == 0)
                    <tr>
                        <td colspan="10" class="text-center">No Indicators</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection