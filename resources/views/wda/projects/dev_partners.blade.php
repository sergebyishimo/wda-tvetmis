@extends('wda.layout.main')

@section('panel-title', "Development Partners")

@section('htmlheader_title', "Development Partners")

@section('panel-body')
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped table-hover">
				<thead style="background: #fff">
				<tr>
					<th>#</th>
					<th>Development Partner</th>
					<th>Area of intervention</th>
					<th>Contact Person</th>
					<th>E-mail Address</th>
					<th style="width: 200px">Action</th>
				</tr>
				</thead>
				<tbody>
				@php
					$i = 1;
				@endphp
				@foreach ($devs as $dev)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $dev->development_partner }}</td>
						<td>{{ $dev->area_of_operation }}</td>
						<td>{{ $dev->contact_person }}</td>
						<td>{{ $dev->address_details }}</td>
						<td>
							<form method="POST" action="{{ route('wda.pr.dev.partners.destroy') }}">
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="delete">
								<input type="hidden" name="delete_dev" value="{{ $dev->id }}">

								<a href="{{ route('wda.pr.dev.partners.edit', $dev->id) }}" class="btn btn-primary btn-sm btn-flat">
									<i class="fa fa-edit"></i> Edit
								</a>
								<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to remove this Development Partner :\n{{ $dev->development_partner }}?')">
									<i class="fa fa-trash"></i> Delete
								</button>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>

			@if (isset($dev_info))

				<form method="POST" action="{{ route('wda.pr.dev.partners.store') }}">
					{{ csrf_field()   }}
					<input type="hidden" name="dev_id" value="{{ $dev_info->id }}">
					<div class="box box-success">
						<div class="box-header">
							<h4 class="box-title"><i class="fa fa-plus"></i> Edit Development Partner</h4>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Development Partner</label>
								<input type="text" class="form-control flat" name="development_partner" placeholder="Development Partner"  value="{{ $dev_info->development_partner }}" required>
							</div>
							<div class="form-group">
								<label>Area of intervention</label>
								<input type="text" class="form-control flat" name="area_of_operation" placeholder="Area of Operation" value="{{ $dev_info->area_of_operation }}" required>
							</div>
							<div class="form-group">
								<label>Contact Person</label>
								<input type="text" class="form-control flat" name="contact_person" placeholder="Contact Person" value="{{ $dev_info->contact_person }}" required>
							</div>
							<div class="form-group">
								<label>E-mail Address</label>
								<input type="email" class="form-control flat" name="address_details" placeholder="E-mail Address" value="{{ $dev_info->address_details }}" required>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-success btn-flat btn-block"> <i class="fa fa-save"></i> Save Development Partner </button>
						</div>
					</div>
				</form>
			@endif

			@if (!isset($dev_info))

				<form method="POST" action="{{ route('wda.pr.dev.partners.store') }}">
					{{ csrf_field()   }}

					<div class="box box-info">
						<div class="box-header">
							<h4 class="box-title"><i class="fa fa-plus"></i> Add Development Partner</h4>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Development Partner</label>
								<input type="text" class="form-control flat" name="development_partner" placeholder="Development Partner" required>
							</div>
							<div class="form-group">
								<label>Area of intervention</label>
								<input type="text" class="form-control flat" name="area_of_operation" placeholder="Area of Operation" required>
							</div>
							<div class="form-group">
								<label>Contact Person</label>
								<input type="text" class="form-control flat" name="contact_person" placeholder="Contact Person" required>
							</div>
							<div class="form-group">
								<label>E-mail Address</label>
								<input type="email" class="form-control flat" name="address_details" placeholder="E-mail Address" required>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-info btn-flat btn-block"> <i class="fa fa-save"></i> Save Development Partner </button>
						</div>
					</div>
				</form>
			@endif
		</div>
	</div>
@endsection