@extends('wda.layout.main')

@section('panel-title', "Program / Project Details")

@section('htmlheader_title', "Program / Project Details")

@section('l-style')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="{{ asset('js/chart.js/Chart.min.js') }}"></script>
@endsection

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if(isset($result))
                <div class="box box-primary">
                    <div class="box-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
                        <h3 class="box-title p-3">
                            <i class="fa fa-pie-chart mr-1"></i>
                            Program / Project Details
                        </h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('wda.pr.report.edit', $result->program->id) }}"><i
                                            class="fa fa-arrow-left"></i> Go Back</a>
                            </li>
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="/projects/download/indicators/{{ $result->id }}"><i class="fa fa-file-pdf-o"></i> PDF</a>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label><i class="fa fa-check-circle"></i> Program / Project</label>
                            <p>{{ $result->program->name }}</p>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-check-circle"></i> Result</label>
                            <p>{{ $result->name }}</p>
                        </div>

                        <div class="form-group">
                            <label><i class="fa fa-table"></i> Indicators Table</label>
                        </div>

                        <div style="overflow-y: scroll;">
                            <table class="table table-bordered table-hover" style="width: 2000px">
                                <thead style="background: #f1f1f1">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th style="width: 300px">Indicator</th>
                                    <th>Baseline</th>
                                    <th>Year / Target / Actual</th>
                                    <th style="width: 350px">Chart</th>
                                    <th style="width: 300px">Activities to Deliver Result</th>
                                    <th style="width: 200px">Narrative Progress</th>
                                    <th style="width: 150px">Stakeholders</th>
                                    <th style="width: 200px">Budget</th>
                                    <th style="width: 200px">Budget Spent</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($result['indicators'] as $indicator)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $indicator->name }}</td>
                                        <td>{{ $indicator->baseline }}</td>
                                        <td style="padding: 0;width: 200px">
                                            <table class="table table-bordered table-hover table-striped"
                                                   style="margin-bottom: -2px;width: 200px">
                                                <tr>
                                                    <td>Y1:</td>
                                                    <td>{{$indicator->year_one}}</td>
                                                    <td style="background-color: orange">{{ $indicator->year_one_target }}</td>
                                                    @if($indicator->year_one_actual < $indicator->year_one_target)
                                                        <td style="background-color: red">{{ $indicator->year_one_actual }}</td>
                                                    @elseif($indicator->year_one_actual > $indicator->year_one_target)
                                                        <td style="background-color: green">{{ $indicator->year_one_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $indicator->year_one_actual }}</td>
                                                    @endif

                                                </tr>
                                                <tr>
                                                    <td>Y2:</td>
                                                    <td>{{$indicator->year_two}}</td>
                                                    <td style="background-color: orange">{{ $indicator->year_two_target }}</td>
                                                    @if($indicator->year_two_actual < $indicator->year_two_target)
                                                        <td style="background-color: red">{{ $indicator->year_two_actual }}</td>
                                                    @elseif($indicator->year_two_actual > $indicator->year_two_target)
                                                        <td style="background-color: green">{{ $indicator->year_two_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $indicator->year_two_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Y3:</td>
                                                    <td>{{$indicator->year_three}}</td>
                                                    <td style="background-color: orange">{{ $indicator->year_three_target }}</td>
                                                    @if($indicator->year_three_actual < $indicator->year_three_target)
                                                        <td style="background-color: red">{{ $indicator->year_three_actual }}</td>
                                                    @elseif($indicator->year_three_actual > $indicator->year_three_target)
                                                        <td style="background-color: green">{{ $indicator->year_three_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $indicator->year_three_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Y4:</td>
                                                    <td>{{$indicator->year_four}}</td>
                                                    <td style="background-color: orange">{{ $indicator->year_four_target }}</td>
                                                    @if($indicator->year_four_actual < $indicator->year_four_target)
                                                        <td style="background-color: red">{{ $indicator->year_four_actual }}</td>
                                                    @elseif($indicator->year_four_actual > $indicator->year_four_target)
                                                        <td style="background-color: green">{{ $indicator->year_four_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $indicator->year_four_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Y5:</td>
                                                    <td>{{$indicator->year_five}}</td>
                                                    <td style="background-color: orange">{{ $indicator->year_five_target }}</td>
                                                    @if($indicator->year_five_actual < $indicator->year_five_target)
                                                        <td style="background-color: red">{{ $indicator->year_five_actual }}</td>
                                                    @elseif($indicator->year_five_actual > $indicator->year_five_target)
                                                        <td style="background-color: green">{{ $indicator->year_five_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $indicator->year_five_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Y6:</td>
                                                    <td>{{$indicator->year_six}}</td>
                                                    <td style="background-color: orange">{{ $indicator->year_six_target }}</td>
                                                    @if($indicator->year_six_actual < $indicator->year_six_target)
                                                        <td style="background-color: red">{{ $indicator->year_six_actual }}</td>
                                                    @elseif($indicator->year_six_actual > $indicator->year_six_target)
                                                        <td style="background-color: green">{{ $indicator->year_six_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $indicator->year_six_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Y7:</td>
                                                    <td>{{$indicator->year_seven}}</td>
                                                    <td style="background-color: orange">{{ $indicator->year_seven_target }}</td>
                                                    @if($indicator->year_seven_actual < $indicator->year_seven_target)
                                                        <td style="background-color: red">{{ $indicator->year_seven_actual }}</td>
                                                    @elseif($indicator->year_seven_actual > $indicator->year_seven_target)
                                                        <td style="background-color: green">{{ $indicator->year_seven_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $indicator->year_seven_actual }}</td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <canvas id="barChart_{{ $indicator->id }}" style="height: 30px;"></canvas>
                                        </td>
                                        <td>{{ $indicator->activities_to_deliver_output }}</td>
                                        <td>{{ $indicator->narrative_progress }}</td>
                                        <td>{{ $indicator->stakeholders }}</td>
                                        <td>{{ $indicator->budget }}</td>
                                        <td>{{ $indicator->budget_spent }}</td>

                                    </tr>
                                    @php
                                        $labels = [];
                                        $targets = [];
                                        $actuals = [];

                                        $labels[] = $indicator->year_one;
                                        $labels[] = $indicator->year_two;
                                        $labels[] = $indicator->year_three;
                                        $labels[] = $indicator->year_four;
                                        $labels[] = $indicator->year_five;

                                        if(substr($indicator->year_one_target, -1) == "%") {
                                            $targets[] = substr($indicator->year_one_target, 0, strlen($indicator->year_one_target) - 1); 
                                        } else {
                                            $targets[] = $indicator->year_one_target;
                                        }

                                        if(substr($indicator->year_two_target, -1) == "%") {
                                            $targets[] = substr($indicator->year_two_target, 0, strlen($indicator->year_two_target) - 1); 
                                        } else {
                                            $targets[] = $indicator->year_two_target;
                                        }

                                        if(substr($indicator->year_three_target, -1) == "%") {
                                            $targets[] = substr($indicator->year_three_target, 0, strlen($indicator->year_three_target) - 1); 
                                        } else {
                                            $targets[] = $indicator->year_three_target;
                                        }

                                        if(substr($indicator->year_four_target, -1) == "%") {
                                            $targets[] = substr($indicator->year_four_target, 0, strlen($indicator->year_four_target) - 1); 
                                        } else {
                                            $targets[] = $indicator->year_four_target;
                                        }
                                        
                                        if(substr($indicator->year_five_target, -1) == "%") {
                                            $targets[] = substr($indicator->year_five_target, 0, strlen($indicator->year_five_target) - 1); 
                                        } else {
                                            $targets[] = $indicator->year_five_target;
                                        }

                                        if(substr($indicator->year_six_target, -1) == "%") {
                                            $targets[] = substr($indicator->year_six_target, 0, strlen($indicator->year_six_target) - 1); 
                                        } else {
                                            $targets[] = $indicator->year_six_target;
                                        }

                                        if(substr($indicator->year_seven_target, -1) == "%") {
                                            $targets[] = substr($indicator->year_seven_target, 0, strlen($indicator->year_seven_target) - 1); 
                                        } else {
                                            $targets[] = $indicator->year_seven_target;
                                        }
                                        
                                        

                                        if(substr($indicator->year_one_actual, -1) == "%") {
                                            $actuals[] = substr($indicator->year_one_actual, 0, strlen($indicator->year_one_actual) - 1); 
                                        } else {
                                            $actuals[] = $indicator->year_one_actual;
                                        }

                                        if(substr($indicator->year_two_actual, -1) == "%") {
                                            $actuals[] = substr($indicator->year_two_actual, 0, strlen($indicator->year_two_actual) - 1); 
                                        } else {
                                            $actuals[] = $indicator->year_two_actual;
                                        }

                                        if(substr($indicator->year_three_actual, -1) == "%") {
                                            $actuals[] = substr($indicator->year_three_actual, 0, strlen($indicator->year_three_actual) - 1); 
                                        } else {
                                            $actuals[] = $indicator->year_three_actual;
                                        }

                                        if(substr($indicator->year_four_actual, -1) == "%") {
                                            $actuals[] = substr($indicator->year_four_actual, 0, strlen($indicator->year_four_actual) - 1); 
                                        } else {
                                            $actuals[] = $indicator->year_four_actual;
                                        }

                                        if(substr($indicator->year_five_actual, -1) == "%") {
                                            $actuals[] = substr($indicator->year_five_actual, 0, strlen($indicator->year_five_actual) - 1); 
                                        } else {
                                            $actuals[] = $indicator->year_five_actual;
                                        }

                                        if(substr($indicator->year_six_actual, -1) == "%") {
                                            $actuals[] = substr($indicator->year_six_actual, 0, strlen($indicator->year_six_actual) - 1); 
                                        } else {
                                            $actuals[] = $indicator->year_six_actual;
                                        }

                                        if(substr($indicator->year_seven_actual, -1) == "%") {
                                            $actuals[] = substr($indicator->year_seven_actual, 0, strlen($indicator->year_seven_actual) - 1); 
                                        } else {
                                            $actuals[] = $indicator->year_seven_actual;
                                        }


                                    @endphp
                                {{--@section('loop_st')--}}
                                    <script type="text/javascript">
                                        //-------------
                                        //- BAR CHART -
                                        //-------------

                                        var areaChartData = {
                                            labels: ['Year 0', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5', 'Year 6'],
                                            datasets: [
                                                {
                                                    label: 'Target',
                                                    fillColor: 'rgba(210, 214, 222, 1)',
                                                    strokeColor: 'rgba(210, 214, 222, 1)',
                                                    pointColor: 'rgba(210, 214, 222, 1)',
                                                    pointStrokeColor: '#c1c7d1',
                                                    pointHighlightFill: '#fff',
                                                    pointHighlightStroke: 'rgba(220,220,220,1)',
                                                    data: [{{ implode(', ', $targets) }}],
                                                    backgroundColor: 'rgba(54, 162, 235, 1)',
                                                },
                                                {
                                                    label: 'Actual',
                                                    fillColor: 'rgba(60,141,188,0.9)',
                                                    strokeColor: 'rgba(60,141,188,0.8)',
                                                    pointColor: '#3b8bba',
                                                    pointStrokeColor: 'rgba(60,141,188,1)',
                                                    pointHighlightFill: '#fff',
                                                    pointHighlightStroke: 'rgba(60,141,188,1)',
                                                    data: [{{ implode(', ', $actuals) }}],
                                                    backgroundColor: 'rgba(255,99,132,1)',
                                                }
                                            ]
                                        }

                                        var ctx = document.getElementById("barChart_{{ $indicator->id }}").getContext('2d');

                                        var myChart = new Chart(ctx, {
                                            type: 'bar',
                                            data: areaChartData,
                                            options: {
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                }
                                            }
                                        });
                                    </script>
                                {{--@endsection--}}
                                @endforeach
                                @if(count($result['indicators']) == 0)
                                    <tr>
                                        <td colspan="10" class="text-center"> No Indicators Found!! <a
                                                    href="{{ route('wda.pr.indicators.index') }}"> Add them here </a>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection