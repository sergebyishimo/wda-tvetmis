@extends('wda.layout.main')

@section('panel-title', "Project Finance Types Source")

@section('htmlheader_title', "Project Finance Types Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th style="width: 60px;" class="text-center">#</th>
                    <th>Finance Types</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($fins as $fin)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $fin->finance_type }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.pr.fin.types.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_fin" value="{{ $fin->id }}">
                                <a href="{{ route('wda.pr.fin.types.edit', $fin->id) }}"
                                   class="btn btn-primary btn-sm btn-flat">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this Finance Type :\n{{ $fin->finance_type }} ?')">
                                    <i class="fa fa-trash"> Delete</i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <form method="POST" action="{{ route('wda.pr.fin.types.store') }}">
                {{ csrf_field() }}
                @if($finstype != null)
                    <input type="hidden" name="id" value="{{ $finstype->id or "" }}">
                @endif
                <div class="row">
                    <div class="col-md-10">
                        <input type="text" class="form-control flat" value="{{ $finstype->finance_type or "" }}"
                               name="finance_type">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-save"> Save</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection