@extends('adminlte::layouts.app')

@section('contentheader_title')
    <div class="container-fluid">
        Audit Log
    </div>
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
          integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
            @include('feedback.feedback')
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ route('wda.attachments.upload') }}" class="btn btn-primary pull-right"> Add Attachment </a>
                    </div>
                    <div class="box-body" style="overflow: auto">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Type</th>
                                    <th>User Id</th>
                                    <th>Event / Action </th>
                                    <th>Auditable Type</th>
                                    <th>Old Values</th>
                                    <th>New Values</th>
                                    <th>Ip Address</th>
                                    <th>Tags</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($audits as $audit)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ substr($audit->user_type, 4, 9) }}</td>
                                        <td>{{ $audit->user_type ? $audit->user_type::find($audit->user_id)->name : '' }}</td>
                                        <td>{{ $audit->event }}</td>
                                        <td>{{ substr($audit->auditable_type, 4, 20) }}</td>
                                        <td>{{ json_encode($audit->old_values) }}</td>
                                        <td>{{ json_encode($audit->new_values) }}</td>
                                        <td>{{ $audit->ip_address }}</td>
                                        <td>{{ $audit->tags }}</td>
                                        <td>{{ date('d/m/Y - H:i',strtotime($audit->created_at)) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $audits->links()  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection