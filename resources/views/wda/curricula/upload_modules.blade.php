@extends('wda.layout.main')

@section('panel-title', "Modules")

@section('htmlheader_title', "Modules")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" href="{{ route('wda.modules.index') }}" class="btn btn-sm btn-info">
                                <i class="fa fa-plus-square-o"></i>&nbsp;
                                <span>Go Back</span>
                            </a>
                        </h4>
                    </div>
                    <div>
                        <div class="panel-body">
                            {!! Form::open(['files' => true,'route' => 'wda.modules.post.uploading', 'method' => 'POST']) !!}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sector" class="control-label">Sector</label>
                                        <select name="r_i" data-url="{{ url('wda/get/trades') }}"
                                                class="form-control select2"
                                                style="width: 100%;"
                                                id="sector">
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($sectors)
                                                @foreach ($sectors as $sector)
                                                    <option value="{{ $sector->id }}"
                                                            @if( (isset($curr_info) && $curr_info->subsector->sector->id == $sector->id) || old('r_i') == $sector->id) selected @endif >{{ $sector->tvet_field }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="trades" class="control-label">Sub Sector</label>
                                        <select name="d_d" class="form-control select2"
                                                data-url="{{ url('wda/get/quafication') }}" id="trades"
                                                style="width: 100%;">
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($sub_sectors)
                                                @foreach ($sub_sectors as $subtrade)
                                                    <option value="{{ $subtrade->id }}"
                                                            @if((isset($curr_info) && $curr_info->sub_sector_id == $subtrade->id) || old('d_d') == $subtrade->id) selected @endif >{{  $subtrade->sub_field_name }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="qua" class="control-label">Qualification</label>
                                        <select name="qualification_id" class="form-control select2" id="qua"
                                                style="width: 100%;" required>
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($currs)
                                                @foreach ($currs as $curr)
                                                    <option value="{{ $curr->uuid }}" {{ isset($mod) ? ($curr->uuid == $mod->qualification_id ) == $curr->uuid ? 'selected' :  old('qualification_id') == $curr->uuid ? 'selected' : '' : "" }} >
                                                        {{  $curr->qualification_title }}
                                                    </option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('module_type_id', "Module Category", ['class' => 'control-label']) !!}
                                        <select name="module_type_id" id="module_type_id"
                                                style="width: 100%;"
                                                class="form-control select2">
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($categories)
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}"
                                                            @if(isset($mod))
                                                            @if($mod->module_type_id == $category->id)
                                                            selected
                                                            @elseif(old('module_type_id') == $category->id)
                                                            selected
                                                            @endif
                                                            @endif >{{ $category->title }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('modules', "File (excel please!)") !!}
                                        {!! Form::file('modules') !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label for="">&nbsp;</label>
                                    {!! Form::submit('Uploading', ['class' => 'btn btn-primary btn-block pull-right']) !!}
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ asset('files/format_modules.xlsx') }}"
                                       class="btn btn-sm btn-warning mt-5"
                                       target="_blank">Download Excel Format</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection