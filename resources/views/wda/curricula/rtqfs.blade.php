@extends('wda.layout.main')

@section('panel-title', "RTQF Levels")

@section('htmlheader_title', "RTQF Levels")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">

            @if (isset($rtqf))

                <form method="POST" action="{{ route('wda.rtqfs.index') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="rtqf_id" value="{{ $rtqf->id }}">

                    <div class="box box-success">
                        <div class="box-header">
                            <h4 class="box-title"><i class="fa fa-edit"></i> Edit RTQF Level</h4>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Level Name</label>
                                <input type="text" class="form-control flat" name="level" placeholder="Level Name"
                                       required value="{{ $rtqf->level_name }}">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control flat"
                                          placeholder="Description">{{ $rtqf->level_description }}</textarea>
                            </div>
                            <dvi class="form-group">
                                <label for="code">Level Qualification Name</label>
                                <input type="text" class="form-control flat" name="qualification_type"
                                       placeholder="Qualification Type" value="{{ $rtqf->qualification_type }}">
                            </dvi>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-check"></i>
                                Save RTQF Level
                            </button>
                        </div>
                    </div>
                </form>

            @endif


            <table class="table table-bordered table-hover table-striped" id="myTable">
                <thead>
                <tr>
                    <th style="width:10px">#</th>
                    <th style="width:25%">Qualification Name</th>
                    <th style="width:15%">Level Name</th>
                    <th>Description</th>
                    <th style="width:20%">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i =  1;
                @endphp
                @forelse ($rtqfs as $rtqf)
                    <tr>
                        <td>
                            @if(checkIfSync('rt', $rtqf->uuid))
                                <label class="label label-default">{{ $i++ }}</label>
                            @else
                                <label class="label label-warning" title="Not Synced for schools uses !!!">{{ $i++ }}</label>
                            @endif
                        </td>
                        <td>{{ $rtqf->qualification_type }}</td>
                        <td>{{ $rtqf->level_name }} &nbsp;&nbsp;&nbsp; <span class="badge">{{ getStudentsNber($rtqf->id) }}</span> </td>
                        <td>{{ $rtqf->level_description }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.rtqfs.destroy') }}">
                                {{ csrf_field() }}

                                <input type="hidden" name="delete_rtqf" value="{{ $rtqf->id }}">
                                <input type="hidden" name="_method" value="delete">

                                <a href="{{ route('wda.rtqfs.edit', $rtqf->id) }}"
                                   class="btn btn-primary btn-sm btn-flat"><i
                                            class="fa fa-edit"></i> Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                    @if (count($rtqf->curriculumQualification) <= 0)
                                        onclick="return window.confirm('Are you sure you want to remove this RTQF Level:\n{{ $rtqf->level_name }} ?')">
                                    @else
                                        onclick="alert('You Cant Remove This Level Because It Has Curriculum');return false;">
                                    @endif
                                        
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No RTQF Levels Found!! Add them Below</td>
                    </tr>
                @endforelse
                </tbody>
            </table>


            <form method="POST" action="{{ route('wda.rtqfs.index') }}">
                {{ csrf_field() }}

                <div class="box box-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fa fa-plus"></i> Add RTQF Level</h4>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Level Name</label>
                            <input type="text" class="form-control flat" name="level" placeholder="Level Name" required>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control flat" placeholder="Description"></textarea>
                        </div>
                        <dvi class="form-group">
                            <label for="code">Level Qualification Name</label>
                            <input type="text" class="form-control flat" name="qualification_type"
                                   placeholder="Qualification Type">
                        </dvi>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info btn-block btn-flat"><i class="fa fa-check"></i> Save
                            RTQF Level
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection