@extends('wda.layout.main')

@section('panel-title', "Qualification's Schools")

@section('htmlheader_title', "Qualification's Schools")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url()->previous() }}" class="btn btn-warning">
                <i class="fa fa-list"></i>
                <span>Back</span>
            </a>
            <table class="table table-hover">
                <tr>
                    <td>Sector</td>
                    <td><b>{{ $curr->subsector->sector->tvet_field }}</b></td>
                </tr>
                <tr>
                    <td>Sub Sector</td>
                    <td><b>{{ $curr->subsector->sub_field_name }}</b></td>
                </tr>
                <tr>
                    <td>Qualification Name</td>
                    <td><b>{{ $curr->qualification_title }}</b></td>
                </tr>
                @if($school)
                    <tr>
                        <td>School Name</td>
                        <td><b>{{ $school->school_name }}</b></td>
                    </tr>
                @endif
            </table>
            <table class="table table-bordered table-striped table-hover" id="dataTableBtnServer">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Photo</th>
                    <th>Student Names</th>
                    <th>Student Number</th>
                    <th>School</th>
                    <th>Sex</th>
                    <th>Province</th>
                    <th>District</th>
                    <th>Level</th>
                </tr>
                </thead>
                <tbody>
                {{--@php--}}
                    {{--$i = 1;--}}
                {{--@endphp--}}
                {{--@foreach ($students as $student)--}}
                    {{--<tr>--}}
                        {{--<td>{{ $i++ }}</td>--}}
                        {{--<td>--}}
                            {{--@if(getStudentPhoto($student) != "")--}}
                                {{--<img src="{{ getStudentPhoto($student) }}" alt="{{ $student->fname }}" width="50px"--}}
                                     {{--height="50px" class="img img-rounded">--}}
                            {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>--}}
                            {{--<h5>--}}
                                {{--{{ $student->fname." ".$student->lname }}--}}
                            {{--</h5>--}}
                        {{--</td>--}}
                        {{--<td>--}}
                            {{--<label class="label label-primary">{{ $student->reg_no }}</label>--}}
                        {{--</td>--}}
                        {{--<td>{{ $student->mschool->school_name or "" }}</td>--}}
                        {{--<td>{{ $student->gender }}</td>--}}
                        {{--<td>{{ $student->province }}</td>--}}
                        {{--<td>{{ $student->district }}</td>--}}
                        {{--<td>--}}
                            {{--{{ $student->level->rtqf->level_name or ""}}--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $("#dataTableBtnServer").dataTable({
                dom: 'Blfrtip',
                pager: true,
                serverSide: true,
                processing: true,
                ajax: '{{ $url }}',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columns: [
                    {data: 'DT_Row_Index', name: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'photo', name: 'students.photo', orderable: false, searchable: false},
                    {data: 'names', name: 'names', orderable: false, searchable: false},
                    {data: 'reg_no', name: 'students.reg_no', orderable: true, searchable: true},
                    {data: 'school_name', name: 'accr_schools_information.school_name', orderable: true, searchable: true},
                    {data: 'gender', name: 'students.gender', orderable: true, searchable: true},
                    {data: 'province', name: 'students.province', orderable: true, searchable: true},
                    {data: 'district', name: 'students.district', orderable: true, searchable: true},
                    {data: 'level_name', name: 'rtqfs.level_name', orderable: true, searchable: true},
                ],
            });
        });
    </script>
@endsection