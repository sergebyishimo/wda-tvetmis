@extends('wda.layout.main')

@section('panel-title', "Qualification's Schools")

@section('htmlheader_title', "Qualification's Schools")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url()->previous() }}" class="btn btn-warning">
                <i class="fa fa-list"></i>
                <span>Back</span>
            </a>
            <table class="table table-hover">
                <tr>
                    <td>Sector</td>
                    <td><b>{{ $curr->subsector->sector->tvet_field }}</b></td>
                </tr>
                <tr>
                    <td>Sub Sector</td>
                    <td><b>{{ $curr->subsector->sub_field_name }}</b></td>
                </tr>
                <tr>
                    <td>Qualification Name</td>
                    <td><b>{{ $curr->qualification_title }}</b></td>
                </tr>
            </table>
            <table class="table table-bordered table-striped table-hover" id="dataTableBtn">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>School</th>
                    <th>Province</th>
                    <th>District</th>
                    <th>Sector</th>
                    <th>Students</th>
                    <th>School Status</th>
                    <th>School Phone</th>
                    <th>School Emails</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($schools as $school)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $school->school_name }}</td>
                        <td>{{ $school->province }}</td>
                        <td>{{ $school->district }}</td>
                        <td>{{ $school->sector }}</td>
                        <td>
                            <a href="{{ route('wda.curricula.view.school.students', [$curr->id, $school->id]) }}">
                                {{ getQualificationStudents($curr->id, true, $school->id) }}
                            </a>
                        </td>
                        <td>{{ $school->school_status }}</td>
                        <td>{{ $school->phone }}</td>
                        <td>{{ $school->email }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection