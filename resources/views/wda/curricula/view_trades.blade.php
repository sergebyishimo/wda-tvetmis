@extends('wda.layout.main')

@section('panel-title', "Curricula")

@section('htmlheader_title', "Curricula")

@section('panel-body')
    <div class="row text-center">
        <div class="col-md-9" style="float: inherit;margin: auto;">
            <div class="row">
                @foreach ($sector_info->subsectors as $trade)
                    <div class="col-md-3">
                        <a href="{{ route('wda.curricula.trade.view.details', [$trade->sector_id, $trade->id]) }}"
                           style="text-decoration: none;">
                            <p class="p-3 text-center text-white bg-success shadow-sm rounded">
                                {{ $trade->sub_field_name }} &nbsp;
                                <span class="badge bg-warning"
                                      title="Qualifications"
                                      style="color:#333">{{ count($trade->curriculumQualification) }}</span>
                            </p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection