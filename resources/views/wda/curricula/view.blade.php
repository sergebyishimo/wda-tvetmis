@extends('wda.layout.main')

@section('panel-title', "Curricula")

@section('htmlheader_title', "Curricula")

@section('panel-body')
    <div class="row text-center bg-blue">
        <div class="col-md-9" style="float: inherit;margin: auto;">
            <div class="row">
                @foreach ($sectors as $sector)
                    <div class="col-md-4" style="margin-top: 10px">
                        <a href="{{ route('wda.curricula.edit',$sector->id) }}">
                            <p style="background: #fff;color: #000;border-radius: 10px;border: 2px solid #000;width: 100%;padding: 18px 10px">{{ $sector->tvet_field }}
                                <br>
                                <span class="badge bg-orange"><small>Sub Sector</small>: {{ $sector->subsectors()->count() }}</span>
                                <br>
                                <span class="badge bg-orange"><small>Qualification</small>: {{ getQualificationFormSubsector($sector->subsectors, true) }}</span>
                            </p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection