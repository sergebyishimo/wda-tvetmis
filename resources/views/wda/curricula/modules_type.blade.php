@extends('wda.layout.main')

@section('panel-title', "Module Categories")

@section('htmlheader_title', "Module Categories")

@section('panel-body')
    <div class="box">
        <div class="box-body bg-gray-light">
            {!! Form::open(['route' => 'wda.modules.category.index']) !!}
            @isset($cat)
                {!! Form::hidden('category_id', $cat->id) !!}
            @endisset
            <div class="form-group">
                <div class="col-md-2">
                    {!! Form::label('title', "Category Title", ['class' => 'control-label']) !!}
                </div>
                <div class="col-md-9">
                    {!! Form::text('title', isset($cat) ? $cat->title : null, ['class' => 'form-control col-md-8']) !!}
                </div>
            </div>
            <div class="col-md-1">
                {!! Form::submit('Save', ['class' => 'btn btn-primary btn-block']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box">
                <div class="box-body">
                    <table class="table table-responsive" id="dataTable">
                        <thead>
                        <th width="40px" class="text-center">#</th>
                        <th>Name</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @php
                            $x = 1;
                        @endphp
                        @forelse($categories as $category)
                            <tr>
                                <td class="text-center">{{ $x++ }}</td>
                                <td>{{ $category->title }}</td>
                                <td width="150px">
                                    <form method="POST" action="{{ route('wda.modules.category.destroy') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="delete" value="{{ $category->id }}">
                                        <input type="hidden" name="_method" value="delete">
                                        <a href="{{ route('wda.modules.category.edit',$category->id) }}"
                                           class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i> Edit</a>
                                        <button type="submit" class="btn btn-danger btn-sm pull-right btn-flat"
                                                onclick="return window.confirm('Are you sure you want to remove this category:\n{{ $category->title }} ?')">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection