@extends('wda.layout.main')

@section('panel-title', "Sectors")

@section('htmlheader_title', "Sectors")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($sector))
                <form method="POST" action="{{ route('wda.sectors.store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="sector_id" value="{{ $sector->id }}">
                    <div class="box box-success">
                        <div class="box-header">
                            <h4 class="box-title"><i class="fa fa-edit"></i> Edit Sector</h4>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="sector">Sector / Field</label>
                                <input type="text" class="form-control flat" name="sector" placeholder="Sector / Field"
                                       required value="{{ $sector->tvet_field }}">
                            </div>
                            <dvi class="form-group">
                                <label for="code">Sector Code</label>
                                <input type="text" class="form-control flat" name="code" placeholder="Sector code"
                                       value="{{ $sector->field_code }}">
                            </dvi>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success btn-block btn-flat"><i
                                                class="fa fa-check"></i> Save Sector
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('wda.sectors.index') }}"
                                       class="btn btn-default btn-block btn-flat"><i class="fa fa-close"></i> Cancel</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            @endif

            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th style="width:10px">#</th>
                    <th style="width:40%">Sector</th>
                    <th>Code</th>
                    <th style="width:20%">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i =  1;
                @endphp
                @foreach ($sectors as $sector)
                    <tr>
                        <td>
                            @if(checkIfSync('se', $sector->uuid))
                                <label class="label label-default">{{ $i++ }}</label>
                            @else
                                <label class="label label-warning"
                                       title="Not Synced for schools uses !!!">{{ $i++ }}</label>
                            @endif
                        </td>
                        <td>{{ $sector->tvet_field }}</td>
                        <td>{{ $sector->field_code }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.sectors.destroy') }}">
                                {{ csrf_field() }}

                                <input type="hidden" name="delete_sector" value="{{ $sector->id }}">
                                <input type="hidden" name="_method" value="delete">

                                <a href="{{ route('wda.sectors.edit',$sector->id) }}"
                                   class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i> Edit</a>
                                @if(!$sector->subsectors()->count() <= 0)
                                    <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                            @if(count($sector->subsectors) <= 0)
                                            onclick="return window.confirm('Are you sure you want to remove this Sector: \n\n {{ $sector->tvet_field }} ?')">
                                        @else
                                            onclick="alert('You Cant Delete This Sector Because It Has Sub
                                            Sectors.');return false">
                                        @endif
                                        <i class="fa fa-trash"></i> Delete
                                    </button>
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach

                @if (count($sectors) == 0)
                    <tr>
                        <td colspan="5" class="text-center">No Sectors Found!! Add them Below</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <form method="POST" action="{{ route('wda.sectors.store') }}">
                {{ csrf_field() }}

                <div class="box box-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fa fa-plus"></i> Add Sector</h4>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="sector">Sector / Field</label>
                            <input type="text" class="form-control flat" name="sector" placeholder="Sector / Field"
                                   required>
                        </div>
                        <dvi class="form-group">
                            <label for="code">Sector Code</label>
                            <input type="text" class="form-control flat" name="code" placeholder="Sector code">
                        </dvi>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info btn-block btn-flat"><i class="fa fa-check"></i> Save
                            Sector
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection