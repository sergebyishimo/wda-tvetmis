@extends('wda.layout.main')

@section('panel-title', "Modules")

@section('htmlheader_title', "Modules")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <div class="row">
                                <div class="col-md-6">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        Add New Module
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('wda.modules.post.uploading') }}"
                                       class="btn btn-warning btn-md pull-right">
                                        <i class="fa fa-upload"></i> &nbsp;
                                        <span>Use Upload Form</span>
                                    </a>
                                </div>
                            </div>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse {{ isset($mod) ? 'in' : '' }}" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            {!! Form::open(['files' => true,'route' => 'wda.modules.store']) !!}
                            @isset($mod)
                                {!! Form::hidden('module_id', $mod->id) !!}
                            @endisset
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sector" class="control-label">Sector</label>
                                        @php
                                            $uSD = null;
                                        @endphp
                                        @if(isset($mod))
                                            @if($mod->qualification)
                                                @php
                                                    $uSD = $mod->qualification ? $mod->qualification->subsector->sector_id : null;
                                                @endphp
                                            @endif
                                        @endif
                                        {{ Form::select('r_i', $sectors->pluck('tvet_field', 'id'), null, ['class' => 'form-control select2', 'id' => 'sector', 'data-url' =>  url('wda/get/trades'),'placeholder' => 'Select ...']) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="trades" class="control-label">Sub Sector</label>
                                        @php
                                            $uS = null;
                                        @endphp
                                        @if(isset($mod))
                                            @if($mod->qualification)
                                                @php
                                                    $uS = $mod->qualification ? $mod->qualification->sub_sector_id : null
                                                @endphp
                                            @endif
                                        @endif
                                        {{ Form::select('d_d', $sub_sectors->pluck('sub_field_name', 'id'), null, ['class' => 'form-control select2', 'id' => 'trades', 'data-url' =>  url('wda/get/quafication'),'placeholder' => 'Select ...']) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="qua" class="control-label">Qualification</label>
                                        <select name="qualification_id" class="form-control select2" id="qua"
                                                style="width: 100%;" required>
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($currs)
                                                @foreach ($currs as $curr)
                                                    <option value="{{ $curr->uuid }}" {{ isset($mod) ? $curr->uuid == $mod->qualification_id ? 'selected' : '' : "" }} >
                                                        {{  $curr->qualification_title }}
                                                    </option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('module_type_id', "Module Category", ['class' => 'control-label']) !!}
                                        <select name="module_type_id" id="module_type_id"
                                                style="width: 100%;"
                                                class="form-control select2">
                                            <option value="" disabled selected>Choose Here</option>
                                            @isset($categories)
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}"
                                                            @if(isset($mod))
                                                            @if($mod->module_type_id == $category->id)
                                                            selected
                                                            @endif
                                                            @endif >{{ $category->title }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('module_code', "Module Code", ['class' => 'control-label']) !!}
                                        {!! Form::text('module_code', isset($mod)? $mod->module_code : null, ['class' => 'form-control', 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('module_title', "Module Title", ['class' => 'control-label']) !!}
                                        {!! Form::text('module_title', isset($mod)? $mod->module_title : null, ['class' => 'form-control', 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('credits', "Module Credits", ['class' => 'control-label']) !!}
                                        {!! Form::text('credits', isset($mod)? $mod->credits : null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('learning_hours', "Learning Hours", ['class' => 'control-label']) !!}
                                        {!! Form::text('learning_hours', isset($mod)? $mod->learning_hours : null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::label('competence_class', "Competence Class", ['class' => 'control-label']) !!}
                                    {!! Form::select('competence_class', getModuleCompetenceClass(), isset($mod)? $mod->competence_class : null, ['class' => 'form-control select2', 'placeholder' => 'Select Here ...']) !!}
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('competence_type', "Competence Type", ['class' => 'control-label']) !!}
                                    {!! Form::select('competence_type', getModuleCompetenceType(), isset($mod)? $mod->competence_type : null, ['class' => 'form-control select2', 'placeholder' => 'Select Here ...']) !!}
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('attachment', "Attachment", ['class' => 'control-label']) !!}
                                    {!! Form::file('attachment') !!}
                                    @isset($mod)
                                        @if($mod->attachment)
                                            @if(\Storage::disk(config('voyager.storage.disk'))->exists($mod->attachment))
                                                <a href="{{ asset('storage/'.$mod->attachment) }}"
                                                   class="btn btn-warning btn-outline-warning"
                                                   target="_blank">Download</a>
                                            @else
                                                <a class="btn btn-danger text-white">No Documents</a>
                                            @endif
                                        @else
                                            <a class="btn btn-danger text-white">No Documents</a>
                                        @endif
                                    @endisset
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-2">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-block']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <table class="table table-responsive" id="dataTable">
                <thead>
                <tr>
                    <th width="35px" class="text-center">#</th>
                    <th>Module Code</th>
                    <th>Module Title</th>
                    <th>Qualification</th>
                    <th>Credits</th>
                    <th>Learning Hours</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @php $x =1; @endphp
                @forelse($modules as $module)
                    <tr>
                        <td class="text-center">{{ $x++ }}</td>
                        <td>{{ $module->module_code }}</td>
                        <td>{{ $module->module_title }}</td>
                        <td>{{ $module->qualification->qualification_title or "" }}</td>
                        <td>{{ $module->credits }}</td>
                        <td>{{ $module->learning_hours }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.modules.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="delete" value="{{ $module->id }}">
                                <input type="hidden" name="_method" value="delete">

                                <a href="{{ route('wda.modules.edit', $module->id) }}"
                                   title="Edit"
                                   class="btn btn-primary btn-sm btn-block">
                                    <i class="fa fa-edit"></i> Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm btn-block"
                                        title="Delete"
                                        onclick="return window.confirm('Are you sure you want to remove this module:\n{{ $module->module_title }} ?')">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
    {!! Form::close() !!}
@endsection