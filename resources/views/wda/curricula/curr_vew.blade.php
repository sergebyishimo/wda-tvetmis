@extends('wda.layout.main')

@section('panel-title', "Qualifications : " . $curr->qualification_title)

@section('htmlheader_title', "Qualifications : " . $curr->qualification_title)

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url()->previous() }}" class="btn btn-warning">
                <i class="fa fa-list"></i>
                <span>Go Back</span>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <table class="table table-bordered table-hover">
                <tr>
                    <th style="width: 300px;background: #030363;color: #fff">Code</th>
                    <td>{{ $curr->qualification_code }}</td>
                </tr>
                <th style="background: #030363;color: #fff">Qualification Title</th>
                <td>{{ $curr->qualification_title }}</td>
                </tr>
                <th style="background: #030363;color: #fff">Field</th>
                <td>{{ $curr->subsector->sector->tvet_field or "" }}</td>
                </tr>
                <th style="background: #030363;color: #fff">Trade</th>
                <td>{{ $curr->subsector->sub_field_name or "" }}</td>
                </tr>
                <th style="background: #030363;color: #fff">RTQF</th>
                <td>{{ $curr->rtqf->level_name or "" }}</td>
                </tr>
                <th style="background: #030363;color: #fff">Credits</th>
                <td>{{ $curr->credits }}</td>
                </tr>
                <th style="background: #030363;color: #fff">Release Date</th>
                <td>{{ $curr->release_date }}</td>
                </tr>
                <th style="background: #030363;color: #fff">Status</th>
                <td>{{ $curr->status == '1' ? "Active" : "Inactive" }}</td>
                </tr>
                <th style="background: #030363;color: #fff">Description</th>
                <td>{!! $curr->description !!}</td>
                </tr>
                <tr>
                    <th style="background: #030363;color: #fff">Attachment</th>
                    <td> @if($curr->attachment) <a href="{{  Storage::url($curr->attachment) }}">Download
                            File</a> @endif</td>
                </tr>
                <th style="background: #030363;color: #fff">Occupation Profile</th>
                <td>{{ $curr->occupation_profile }}</td>
                </tr>
                {{--<th style="background: #030363;color: #fff">Job Related Information</th>--}}
                {{--<td>{{ $curr->job_related_information }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Entry Requirements</th>--}}
                {{--<td>{{ $curr->entry_requirements }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Information About Pathways</th>--}}
                {{--<td>{{ $curr->information_about_pathways }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Employability And Life Skills</th>--}}
                {{--<td>{{ $curr->employability_and_life_skills }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Qualification Arrangment</th>--}}
                {{--<td>{{ $curr->qualification_arrangement }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Competency Standards</th>--}}
                {{--<td>{{ $curr->competency_standards }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Training Manual</th>--}}
                {{--<td>{{ $curr->training_manual }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Trainees Manual</th>--}}
                {{--<td>{{ $curr->trainees_manual }}</td>--}}
                {{--</tr>--}}
                {{--<th style="background: #030363;color: #fff">Trainers Manual</th>--}}
                {{--<td>{{ $curr->trainers_manual }}</td>--}}
                </tr>
            </table>
        </div>
        <div class="col-md-3">
            <table class="table table-bordered p-1">
                <tr class="bg-warning">
                    <td>Number of competencies</td>
                    <td class="text-right">{{ $modules ? $modules->count() : 0 }}</td>
                </tr>
                @foreach(getModuleCompetenceClass() as $item)
                    <tr class="bg-warning">
                        <td>{{ ucwords($item) }}</td>
                        <td class="text-right">
                            {{ $modules ? number_format($modules->where('competence_class', 'LIKE', $item)->count()) : 0 }}
                        </td>
                    </tr>
                @endforeach
                <tr class="bg-warning">
                    <td>Total Number Of Credit</td>
                    <td class="text-right">{{ $modules ? number_format($modules->sum('credits')) : 0 }}</td>
                </tr>
            </table>
        </div>
    </div>
    @if($modules)
        <div class="row" id="currModules">
            <div class="col-md-12">
                <h3>Modules</h3>
                <table class="table table-responsive" id="dataTable">
                    <thead>
                    <tr>
                        <th width="35px" class="text-center">#</th>
                        <th>Module Code</th>
                        <th>Module Title</th>
                        <th>Credits</th>
                        <th>Learning Hours</th>
                        <th>Document</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $x =1; @endphp
                    @forelse($modules as $module)
                        <tr>
                            <td class="text-center">{{ $x++ }}</td>
                            <td>{{ $module->module_code }}</td>
                            <td>{{ $module->module_title }}</td>
                            <td>{{ $module->credits }}</td>
                            <td>{{ $module->learning_hours }}</td>
                            <td>
                                @if($module->attachment)
                                    @if(\Storage::disk(config('voyager.storage.disk'))->exists($module->attachment))
                                        <a href="{{ asset('storage/'.$module->attachment) }}"
                                           class="btn btn-warning btn-outline-warning"
                                           target="_blank">Download</a>
                                    @else
                                        <a class="btn btn-danger text-white">No Documents</a>
                                    @endif
                                @else
                                    <a class="btn btn-danger text-white">No Documents</a>
                                @endif
                            </td>
                            <td>
                                <form method="POST" action="{{ route('wda.modules.destroy') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="delete" value="{{ $module->id }}">
                                    <input type="hidden" name="_method" value="delete">

                                    <a href="{{ route('wda.modules.edit', $module->id) }}"
                                       title="Edit"
                                       class="btn btn-primary btn-sm btn-block">
                                        <i class="fa fa-edit"></i> Edit</a>
                                    <button type="submit" class="btn btn-danger btn-sm btn-block"
                                            title="Delete"
                                            onclick="return window.confirm('Are you sure you want to remove this module:\n{{ $module->module_title }} ?')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection