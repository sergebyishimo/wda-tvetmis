@extends('wda.layout.main')

@section('panel-title', "Sub Sectors")

@section('htmlheader_title', "Sub Sectors")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($trade))
                <form method="POST" action="{{ route('wda.trades.index') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="trade_id" value="{{ $trade->id }}">
                    <div class="box box-success">
                        <div class="box-header">
                            <h4 class="box-title"><i class="fa fa-edit"></i> Edit Sub Sector</h4>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Sector</label>
                                <select name="sector_id" class="form-control flat">
                                    @foreach ($sectors as $sector)
                                        <option value="{{ $sector->id }}"
                                                @if($sector->id == $trade->sector_id) selected @endif>{{ $sector->tvet_field }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Sub Sector</label>
                                <input type="text" class="form-control flat" name="trade" placeholder="Sub Sector"
                                       required value="{{ $trade->sub_field_name }}">
                            </div>
                            <dvi class="form-group">
                                <label for="code">Sub Sector Code</label>
                                <input type="text" class="form-control flat" name="code" placeholder="Sub Sector code"
                                       value="{{ $trade->sub_field_code }}">
                            </dvi>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-check"></i>
                                Save Sub Sector
                            </button>
                        </div>
                    </div>
                </form>

            @endif


            <table class="table table-bordered table-hover table-striped" id="dataTableTrades">
                <thead>
                <tr>
                    <th style="width:10px">#</th>
                    <th style="width:30%">Sector</th>
                    <th style="width:30%">Sub Sector</th>
                    <th>Code</th>
                    <th style="width:20%">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i =  1;
                @endphp
                @foreach ($trades as $trade)
                    <tr>
                        <td>
                            @if(checkIfSync('sb', $trade->uuid))
                                <label class="label label-default">{{ $i++ }}</label>
                            @else
                                <label class="label label-warning"
                                       title="Not Synced for schools uses !!!">{{ $i++ }}</label>
                            @endif
                        </td>
                        <td>{{ $trade->sector->tvet_field }}</td>
                        <td>{{ $trade->sub_field_name }}</td>
                        <td>{{ $trade->sub_field_code }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.trades.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="delete_trade" value="{{ $trade->id }}">
                                <input type="hidden" name="_method" value="delete">

                                <a href="{{ route('wda.trades.edit', $trade->id) }}"
                                   class="btn btn-primary btn-sm btn-flat">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                @if($trade->curriculumQualification()->count() == 0)
                                    <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                            @if (count($trade->curriculumQualification) <= 0)
                                            onclick="return window.confirm('Are you sure you want to remove this Sub Sector:\n{{ $trade->sub_field_name }} ?')">
                                        @else
                                            onclick="alert('You Cant Remove This Trade Because It Has
                                            Curriculum');return false;">
                                        @endif

                                        <i class="fa fa-trash"></i> Delete
                                    </button>
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach

                @if (count($trades) == 0)
                    <tr>
                        <td colspan="5" class="text-center">No Sub Sectors Found!! Add them Below</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="clear-fix">&nbsp;</div>
            <form method="POST" action="{{ route('wda.trades.index') }}">
                {{ csrf_field() }}

                <div class="box box-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fa fa-plus"></i> Add Sub Sector</h4>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Sector</label>
                            <select name="sector_id" class="form-control flat">
                                @foreach ($sectors as $sector)
                                    <option value="{{ $sector->id }}">{{ $sector->tvet_field }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Sub Sector</label>
                            <input type="text" class="form-control flat" name="trade" placeholder="Sub Sector" required>
                        </div>
                        <dvi class="form-group">
                            <label for="code">Sub Sector Code</label>
                            <input type="text" class="form-control flat" name="code" placeholder="Sub Sector code">
                        </dvi>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info btn-block btn-flat"><i class="fa fa-check"></i> Save
                            Sub Sector
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            $("#dataTableTrades").dataTable({
                dom: 'Blfrtip',
                pager: true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
        });
    </script>
@endsection