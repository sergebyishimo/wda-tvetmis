@extends('wda.layout.main')

@section('panel-title', "Curricula: ".$sector_info->tvet_field)

@section('htmlheader_title', "Curricula: ".$sector_info->tvet_field)

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (count($sector_info->subsectors) == 0)
                <p style="font-size: 18px"><i class="fa fa-warning"></i> No trades founds in this sector</p>
            @endif
            <div style="overflow: auto">
                <table class="table table-bordered table-striped table-hover"
                       style="background: #fff;" id="dataTableListingQ">
                    <thead class="text-center">
                    <tr style="background: #cc0000;color: #fff">
                        @foreach ($sector_info->subsectors as $trade)
                            @if($trade->id == $trad)
                                <th colspan="3" style="width: 250px">{{ $trade->sub_field_name }}</th>
                                @break;
                            @endif
                        @endforeach
                    </tr>
                    <tr style="background: darkblue;color: #fff">
                        <th style="width: 100px">RTQF Levels</th>
                        @foreach ($sector_info->subsectors as $trade)
                            @if($trade->id == $trad)
                                <th style="width: 250px">Curricula</th>
                                <th style="width: 300px">Occupations/ Job titles</th>
                                @break
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($rtqfs as $rtqf)
                        <tr>
                            <td>{{ $rtqf->level_name }}</td>
                            @foreach ($sector_info->subsectors as $trade)
                                @if($trade->id == $trad)
                                    @php
                                        $trade->real_rtqf_id = $rtqf->id;
                                    @endphp
                                    <td>
                                        <a href="{{ route('curriculum.view', $trade->real_curr_id) }}">{{ $trade->real_title }}</a>
                                    </td>
                                    <td>
                                        <ol>
                                            @php
                                                $jobs = explode(', ', $trade->real_jobs);
                                            @endphp
                                            @foreach ($jobs as $job)
                                                @if ($job != '')
                                                    <li>{{ $job }}</li>
                                                @endif
                                            @endforeach
                                        </ol>
                                    </td>
                                    @break
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script>
        $(document).ready(function () {
            $('#dataTableListingQ').DataTable({
                dom: 'Bfrtip',
                "scrollX": false,
                // // responsive: true,
                // fixedHeader: {
                //     headerOffset: 100
                // },
                "aaSorting": [],
                buttons: [
                    'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection