@extends('wda.layout.main')

@section('panel-title', "Qualifications")

@section('htmlheader_title', "Qualifications")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box-body">
                {!! Form::open(['method' => 'get', 'class' => 'form', 'id' => 'search']) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sectors</label>
                            {!! Form::select('s_sector', $sectors->pluck('tvet_field','id') , null , ['class' => 'form-control select2 flat','placeholder'=>'Choose Here']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sub-Sector</label>
                            {!! Form::select('s_subsector', $Sub_sectors->pluck('sub_field_name','id') , null , ['class' => 'form-control select2 flat','placeholder'=>'Choose Here']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">RTQF:</label>
                            {!! Form::select('s_rtqf', $rtqfs->pluck('level_name','id') , null , ['class' => 'form-control select2 flat','placeholder'=>'Choose Here']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-12 mt-3">
                                <button type="submit" class="btn btn-info pull-left">Yes, Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped table-hover" id="dataTableBtn">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Code</th>
                    <th>Title</th>
                    <th>Sector</th>
                    <th>Sub Sector</th>
                    <th>RTQF</th>
                    <th>Modules</th>
                    <th>Schools</th>
                    <th>Students</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($currs as $curr)
                    <tr>
                        <td>
                            @if(checkIfSync('qu', $curr->uuid))
                                <label class="label label-default">{{ $i++ }}</label>
                            @else
                                <label class="label label-warning"
                                       title="Not Synced for schools uses !!!">{{ $i++ }}</label>
                            @endif
                        </td>
                        <td>{{ $curr->qualification_code }}</td>
                        <td>{{ $curr->qualification_title }}</td>
                        <td>{{ $curr->subsector->sector->tvet_field or "" }}</td>
                        <td>{{ $curr->subsector->sub_field_name or "" }}</td>
                        <td>{{ $curr->rtqf->level_name or "" }}</td>
                        <td>
                            @if($curr->modules)
                                <a href="{{ route('wda.curriculum.view', $curr->id) }}#currModules"
                                   title="View"
                                   class="btn btn-warning btn-sm btn-flat">{{ $curr->modules->count() }}</a>
                            @else
                                0
                            @endif
                        </td>
                        <th style="vertical-align:middle;text-align:center;font-size:18px">
                            <a href="{{ route('wda.curricula.view.schools', $curr->id) }}">{{ getQualificationSchools($curr->id) }}</a>
                        </th>
                        <th style="vertical-align:middle;text-align:center;font-size:18px">
                            <a href="{{ route('wda.curricula.view.students', $curr->id) }}">{{ \App\Student::join('levels', 'levels.id', 'students.level_id')->join('departments', 'departments.id', 'levels.department_id')->join('accr_curriculum_qualifications', 'accr_curriculum_qualifications.uuid', 'departments.qualification_id')->where('accr_curriculum_qualifications.uuid', $curr->uuid)->count() }}</a>
                        </th>
                        <td>
                            <form method="POST" action="{{ route('wda.curriculum.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_curriculum" value="{{ $curr->id }}">
                                <a href="{{ route('wda.curriculum.view', $curr->id) }}"
                                   title="View"
                                   class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i></a>
                                <a href="{{ route('wda.curriculum.edit', $curr->id) }}#edit"
                                   title="Edit"
                                   class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i> </a>
                                @if($curr->modules()->count() <= 0)
                                    <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                            title="Delete"

                                            @if (count($curr->departments) <= 0)
                                            onclick="return window.confirm('Are you sure you want to remove this curriculum:\n{{ $curr->qualification_title }}?')">
                                        @else
                                            onclick="alert('You Cant Remove This Level Because It Has
                                            Curriculum');return
                                            false;">
                                        @endif
                                        <i class="fa fa-trash"></i>
                                    </button>
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="clear-fix" id="edit">&nbsp;</div>
            <form method="POST" action="{{ route('wda.curriculum.index') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if (isset($curr_info))
                    <div class="box box-success">
                        <div class="box-header">
                            <h4 class="box-title"><i class="fa fa-edit"></i> Edit Curriculum Information</h4>
                            <input type="hidden" name="curriculum_id" value="{{ $curr_info->id }}">
                            @else
                                <div class="box box-info">
                                    <div class="box-header">
                                        <h4 class="box-title"><i class="fa fa-plus"></i> Add Curriculum</h4>
                                        @endif


                                    </div>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="">Sector</label>
                                            <select name="sector_id" data-url="{{ url('wda/get/trades') }}"
                                                    class="form-control flat" id="sector">
                                                <option value="" disabled selected>Choose Here</option>
                                                @foreach ($sectors as $sector)
                                                    <option value="{{ $sector->id }}"
                                                            @if( isset($curr_info) && $curr_info->subsector->sector->id == $sector->id) selected @endif >{{ $sector->tvet_field }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Sub Sector</label>
                                            @php //dd($curr_info->subsector) @endphp
                                            <select name="trade_id" class="form-control flat" id="trades" required>
                                                <option value="" disabled selected>Choose Here</option>
                                                @if (isset($Sub_sectors))
                                                    @foreach ($Sub_sectors as $subtrade)
                                                        <option value="{{ $subtrade->id }}"
                                                                @if( isset($curr_info) && $curr_info->sub_sector_id == $subtrade->id) selected @endif >{{  $subtrade->sub_field_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="">RTQF Level <sup>SM</sup></label>
                                                    <select name="rtqf_id[]" class="form-control flat" multiple
                                                            required>
                                                        <option value="" disabled>Choose Here</option>
                                                        @foreach ($rtqfs as $rtqf)
                                                            <option value="{{ $rtqf->id }}"
                                                                    @if( isset($curr_info) && in_array($rtqf->uuid, getMyRqtf($curr_info->uuid))) selected @endif >{{ $rtqf->level_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="">RTQF Level <sup>CBT</sup></label>
                                                    <select name="rtqf_level_id" class="form-control flat"
                                                            required>
                                                        <option value="" disabled>Choose Here</option>
                                                        @foreach ($rtqfs as $rtqf)
                                                            <option value="{{ $rtqf->id }}"
                                                                    @if( isset($curr_info) )
                                                                    @if($rtqf->id == $curr_info->rtqf_level_id)
                                                                    selected
                                                                    @endif
                                                                    @endif >{{ $rtqf->level_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="">Is CBT</label>
                                                    <select name="is_cbt" class="form-control flat"
                                                            required>
                                                        <option value="" disabled>Choose Here</option>
                                                        <option value="yes" {{ isset($curr_info)? $curr_info->is_cbt == 'yes' ? 'selected': '' : '' }}>
                                                            YES
                                                        </option>
                                                        <option value="no" {{ isset($curr_info)? $curr_info->is_cbt == 'no' ? 'selected': '' : '' }}>
                                                            NO
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Curriculum Code</label>
                                            <input type="text" class="form-control flat" name="code"
                                                   placeholder="Curriculum Code"
                                                   value="{{ $curr_info->qualification_code or '' }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Curriculum Title</label>
                                            <input type="text" class="form-control flat" name="title"
                                                   placeholder="Curriculum Title"
                                                   value="{{ $curr_info->qualification_title or '' }}" required>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-12 col-form-label">Occupation / Job Titles</label>
                                            <div class="col-md-12">
                                                <div class="row" id="jobs">
                                                    @if(isset($curr_info->job_titles))
                                                        @php $jobs =explode(',',$curr_info->job_titles) @endphp
                                                        @foreach($jobs as $job)
                                                            <div class="col-md-5">
                                                                <input type="text" name="jobs[]"
                                                                       class="form-control flat" value="{{$job}}">
                                                            </div><br>
                                                        @endforeach

                                                    @else
                                                        <div class="col-md-5">
                                                            <input type="text" name="jobs[]" class="form-control flat"
                                                                   placeholder="Occupation / Job Title">
                                                        </div>
                                                        <div class="col-md-5">
                                                            <input type="text" name="jobs[]" class="form-control flat"
                                                                   placeholder="Occupation / Job Title">
                                                        </div>
                                                    @endif
                                                    <div class="col-md-1">
                                                        <button type="button" class="btn btn-primary btn-flat"
                                                                onclick="addJob()"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Credits</label>
                                            <input type="number" class="form-control flat" name="credits"
                                                   placeholder="Credits" value="{{ $curr_info->credits or '' }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Release Date</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><i
                                                            class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control datepicker" name="release_date"
                                                       aria-describedby="basic-addon1"
                                                       value="{{ $curr_info->release_date or '' }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" class="form-control flat" id="status" required>
                                                <option value="1" {{ isset($curr_info)? $curr_info->status == '1' ? 'selected': '' : '' }}>
                                                    Active
                                                </option>
                                                <option value="0" {{ isset($curr_info)? $curr_info->status == '0' ? 'selected': '' : '' }}>
                                                    Inactive
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control summernote-sm flat" name="description"
                                                      placeholder="Description">{{ $curr_info->description or '' }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Occupation Profile</label>
                                            <textarea class="form-control flat" name="occupation_profile"
                                                      placeholder="Occupation Profile">{{ $curr_info->occupation_profile or '' }}</textarea>
                                        </div>
                                        {{--<div class="form-group">--}}
                                        {{--<label>Job Related Information</label>--}}
                                        {{--<textarea class="form-control flat" name="job_related_information" placeholder="Job Related Information">{{ $curr_info->job_related_information or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Entry Requirements</label>--}}
                                        {{--<textarea class="form-control flat" name="entry_requirements" placeholder="Entry Requirements">{{ $curr_info->entry_requirements or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Information About Pathways</label>--}}
                                        {{--<textarea class="form-control flat" name="information_about_pathways" placeholder="Information About Pathways">{{ $curr_info->information_about_pathways or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Employability And Life Skills</label>--}}
                                        {{--<textarea class="form-control flat" name="employability_and_life_skills" placeholder="Employability And Life Skills">{{ $curr_info->employability_and_life_skills or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Qualification Arrangement</label>--}}
                                        {{--<textarea class="form-control flat" name="qualification_arrangement" placeholder="Qualification Arrangement">{{ $curr_info->qualification_arrangement or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Competency Standards</label>--}}
                                        {{--<textarea class="form-control flat" name="competency_standards" placeholder="Competency Standards">{{ $curr_info->competency_standards or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Training Manual</label>--}}
                                        {{--<textarea class="form-control flat" name="training_manual" placeholder="Training Manual">{{ $curr_info->training_manual or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Trainees Manual</label>--}}
                                        {{--<textarea class="form-control flat" name="trainees_manual" placeholder="Trainees Manual">{{ $curr_info->trainees_manual or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Trainers Manual</label>--}}
                                        {{--<textarea class="form-control flat" name="trainers_manual" placeholder="Trainers Manual">{{ $curr_info->trainers_manual or '' }}</textarea>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <label>Attachment</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="attachment"
                                                           id="exampleInputFile">
                                                </div>
                                            </div>
                                            @if (isset($curr_info) && $curr_info->attachment)
                                                <a href="/storage/{{ $curr_info->attachment}}">Download Uploaded
                                                    Attachment</a>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        @if (isset($curr_info))
                                            <button type="submit" class="btn btn-success btn-block btn-flat"><i
                                                        class="fa fa-check"></i> Save Curriculum Information
                                            </button>
                                        @else
                                            <button type="submit" class="btn btn-info btn-block btn-flat"><i
                                                        class="fa fa-check"></i> Save Curriculum
                                            </button>
                                        @endif

                                    </div>
                                </div>
            </form>

        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script>
        $("#dataTableBtn").DataTable({
            dom: 'Blfrtip',
            "lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
            buttons: [
                {
                    extend: 'excel',
                    className:' btn btn-primary'
                }
            ]
        });
    </script>
@endsection