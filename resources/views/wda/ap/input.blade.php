@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 26px;padding-left: 5px;"> <i class="fa fa-edit"></i> Action Plan Data Input</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	  Action Plan / Data Input</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<form method="POST">
								{{ csrf_field() }}
							<input type="hidden" name="form" value="ap_program">
							<div class="card card-info collapsed-card">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Program</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
							

								
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<input type="text" class="form-control flat" name="name" placeholder="Program">
									</div>
									<div class="form-group">
										<label>Outome</label>
										<input type="text" class="form-control flat" name="outcome" placeholder="Outome">
									</div>
									<div class="form-group">
										<label>Reporting Period</label>
										<input type="text" class="form-control flat" name="reporting_period" placeholder="Reporting Period">
									</div>
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save Action Plan Program</button>
								</div>
							</div>
						</form>
						<form method="POST">
							{{ csrf_field() }}

							<input type="hidden" name="form" value="ap_sub_program">
						
							<div class="card card-danger collapsed-card">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Sub Program</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-danger btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" name="program_id">
											@foreach ($programs as $program)
												<option value="{{ $program->id }}">{{ $program->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Sub Program</label>
										<input type="text" class="form-control flat" name="name">
									</div>
									
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-danger btn-flat btn-block"><i class="fa fa-save"></i> Save Action Plan Sub Program</button>
								</div>
							</div>
						</form>
						<form method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="form" value="ap_output">

							<div class="card card-primary collapsed-card">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Output</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" name="program_id" id="program">
											<option value="">Choose Here</option>
											@foreach ($programs as $program)
												<option value="{{ $program->id }}">{{ $program->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Sub Program</label>
										<select class="form-control flat" name="sub_program_id" id="subprograms">
											
										</select>
									</div>
									<div class="form-group">
										<label>Output</label>
										<input type="text" class="form-control flat" name="name">
									</div>
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i> Save Action Plan Output</button>
								</div>
							</div>
						</form>
						<form method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="form" value="ap_indicator">
						
							<div class="card card-success collapsed-card" >
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Indicator</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-success btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" id="program2">
											<option value="">Choose Here</option>
											@foreach ($programs as $program)
												<option value="{{ $program->id }}">{{ $program->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Sub Program</label>
										<select class="form-control flat" id="subprograms2">
											
										</select>
									</div>
									<div class="form-group">
										<label>Output</label>
										<select class="form-control flat" name="output_id" id="outputs" required>
											
										</select>
									</div>

									<div class="form-group">
										<label>Indicator</label>
										<input type="text" class="form-control flat" name="indicator" placeholder="Indicator" required=>
									</div>
									<div class="form-group">
										<label>Baseline</label>
										<input type="text" class="form-control flat" name="baseline" placeholder="Baseline" required>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label>Quarter 1</label>
												<div class="row">
													<div class="col-md-8">
														<input type="text" class="form-control flat" name="q_one" placeholder="Quarter 1" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_one_target" placeholder="Target" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_one_actual" placeholder="Actual">		
													</div>	
												</div>		
											</div>
											<div class="col-md-6">
												<label>Quarter 2</label>
												<div class="row">
													<div class="col-md-8">
														<input type="text" class="form-control flat" name="q_two" placeholder="Quarter 2" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_two_target" placeholder="Target" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_two_actual" placeholder="Actual">		
													</div>	
												</div>	
											</div>
										</div>
										
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<label>Quarter 3</label>
												<div class="row">
													<div class="col-md-8">
														<input type="text" class="form-control flat" name="q_three" placeholder="Quarter 3" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_three_target" placeholder="Target" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_three_actual" placeholder="Actual">		
													</div>	
												</div>		
											</div>
											<div class="col-md-6">
												<label>Quarter 4</label>
												<div class="row">
													<div class="col-md-8">
														<input type="text" class="form-control flat" name="q_four" placeholder="Quarter 4" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_four_target" placeholder="Target" required>		
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control flat" name="q_four_actual" placeholder="Actual">		
													</div>	
												</div>	
											</div>
										</div>
										
									</div>
									
									<div class="form-group">
										<label>Activities to Deliver Output </label>
										<input type="text" class="form-control flat" name="activities_to_deliver_output" placeholder="Activities To Deliver Output">
									</div>
									<div class="form-group">
										<label>Stakeholders </label>
										<input type="text" class="form-control flat" name="stakeholders" placeholder="Stakeholders">
									</div>
									<div class="form-group">
										<label>Available </label>
										<input type="text" class="form-control flat" name="available" placeholder="Available">
									</div>
									<div class="form-group">
										<label>Amount Spent </label>
										<input type="text" class="form-control flat" name="amount_spent" placeholder="Amount Spent">
									</div>
									<div class="form-group">
										<label>Balance </label>
										<input type="text" class="form-control flat" name="balance" placeholder="Amount Spent">
									</div>
									<div class="form-group">
										<label>Narrative Report </label>
										<input type="text" class="form-control flat" name="narrative_support" placeholder="Amount Spent">
									</div>
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i> Save Action Plan Indicator</button>
								</div>
							</div>
						</form>						
						
						
					</div>
				</div>
			</div>
		</section>

	</div>
@endsection