@extends('wda.layout.main')

@section('panel-title', "Action Plan Program Details")
@section('htmlheader_title', "Action Plan Program Details")

@section('l-style')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="{{ asset('js/chart.js/Chart.min.js') }}"></script>
@endsection

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if(isset($result))
                <div class="box box-primary">
                    <div class="box-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
                        <h3 class="box-title p-3">
                            <i class="fa fa-pie-chart mr-1"></i>
                            Action Plan Program Details
                        </h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <li class="nav-item">
                                <a class="nav-link" href="/wda/ap/report/{{ $result->program->rp->id }}"><i
                                            class="fa fa-arrow-left"></i> Go Back</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/wda/ap/download/indicators/{{ $result->id }}"><i
                                            class="fa fa-file-pdf-o"></i> PDF</a>
                            </li>
                        </ul>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label><i class="fa fa-check-circle"></i> Reporting Period</label>
                            <p>{{ $result->program->rp->period }}</p>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-check-circle"></i> Program</label>
                            <p>{{ $result->program->name }}</p>
                        </div>

                        @foreach ($result->program->results as $result)


                            <div class="form-group">
                                <label><i class="fa fa-check-circle"></i> Result</label>
                                <p>{{ $result->name }}</p>
                            </div>


                            {{-- expr --}}

                            <div class="form-group">
                                <label><i class="fa fa-table"></i> Indicators Table</label>
                            </div>

                            <div style="overflow-y: scroll;">
                                <table class="table table-bordered table-hover" style="width: 2000px">
                                    <thead style="background: #f1f1f1">
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th style="width: 250px">Indicator</th>
                                        <th style="width: 100px">Baseline</th>
                                        <th>Quarter / Target / Actual</th>
                                        <th style="width: 350px">Chart</th>
                                        <th style="width: 300px">Activities to Deliver Result</th>
                                        <th style="width: 200px">Narrative Progress</th>
                                        <th style="width: 150px">Stakeholders</th>
                                        <th style="width: 200px">Budget</th>
                                        <th style="width: 200px">Budget Spent</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($result['indicators'] as $indicator)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $indicator->name }}</td>
                                            <td>{{ $indicator->baseline }}</td>
                                            <td style="padding: 0;width: 300px">
                                                <table class="table">
                                                    <tr>
                                                        <td>Q1 :</td>
                                                        <td style="background-color: orange">{{ $indicator->q_one_target }}</td>
                                                        @if($indicator->q_one_actual < $indicator->q_one_target)
                                                            <td style="background-color: red">{{ $indicator->q_one_actual }}</td>
                                                        @elseif($indicator->q_one_actual > $indicator->q_one_target)
                                                            <td style="background-color: green">{{ $indicator->q_one_actual }}</td>
                                                        @else
                                                            <td style="background-color: yellow">{{ $indicator->q_one_actual }}</td>
                                                        @endif
                                                    </tr>
                                                    <tr>
                                                        <td>Q2 :</td>
                                                        <td style="background-color: orange">{{ $indicator->q_two_target }}</td>
                                                        @if($indicator->q_two_actual < $indicator->q_two_target)
                                                            <td style="background-color: red">{{ $indicator->q_two_actual }}</td>
                                                        @elseif($indicator->q_two_actual > $indicator->q_two_target)
                                                            <td style="background-color: green">{{ $indicator->q_two_actual }}</td>
                                                        @else
                                                            <td style="background-color: yellow">{{ $indicator->q_two_actual }}</td>
                                                        @endif
                                                    </tr>
                                                    <tr>
                                                        <td>Q3 :</td>
                                                        <td style="background-color: orange">{{ $indicator->q_three_target }}</td>
                                                        @if($indicator->q_three_actual < $indicator->q_three_target)
                                                            <td style="background-color: red">{{ $indicator->q_three_actual }}</td>
                                                        @elseif($indicator->q_three_actual > $indicator->q_three_target)
                                                            <td style="background-color: green">{{ $indicator->q_three_actual }}</td>
                                                        @else
                                                            <td style="background-color: yellow">{{ $indicator->q_three_actual }}</td>
                                                        @endif
                                                    </tr>
                                                    <tr>
                                                        <td>Q4 :</td>
                                                        <td style="background-color: orange">{{ $indicator->q_four_target }}</td>
                                                        @if($indicator->q_four_actual < $indicator->q_four_target)
                                                            <td style="background-color: red">{{ $indicator->q_four_actual }}</td>
                                                        @elseif($indicator->q_four_actual > $indicator->q_four_target)
                                                            <td style="background-color: green">{{ $indicator->q_four_actual }}</td>
                                                        @else
                                                            <td style="background-color: yellow">{{ $indicator->q_four_actual }}</td>
                                                        @endif
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <div class="chart">
                                                    <canvas id="barChart_{{ $indicator->id }}"
                                                            style="height: 250px;"></canvas>
                                                </div>
                                            </td>
                                            <td>{{ $indicator->activities_to_deliver_output }}</td>
                                            <td>{{ $indicator->narrative_report }}</td>
                                            <td>{{ $indicator->stakeholders }}</td>
                                            <td>{{ $indicator->amount }}</td>
                                            <td>{{ $indicator->amount_spent }}</td>
                                        </tr>
                                        @php
                                            $labels = [];
                                            $targets = [];
                                            $actuals = [];

                                            $labels[] = $indicator->q_one;
                                            $labels[] = $indicator->q_two;
                                            $labels[] = $indicator->q_three;
                                            $labels[] = $indicator->q_four;
                                            $labels[] = $indicator->q_five;

                                            if(substr($indicator->q_one_target, -1) == "%") {
                                                $targets[] = substr($indicator->q_one_target, 0, strlen($indicator->q_one_target) - 1);
                                            } else {
                                                $targets[] = $indicator->q_one_target;
                                            }

                                            if(substr($indicator->q_two_target, -1) == "%") {
                                                $targets[] = substr($indicator->q_two_target, 0, strlen($indicator->q_two_target) - 1);
                                            } else {
                                                $targets[] = $indicator->q_two_target;
                                            }

                                            if(substr($indicator->q_three_target, -1) == "%") {
                                                $targets[] = substr($indicator->q_three_target, 0, strlen($indicator->q_three_target) - 1);
                                            } else {
                                                $targets[] = $indicator->q_three_target;
                                            }

                                            if(substr($indicator->q_four_target, -1) == "%") {
                                                $targets[] = substr($indicator->q_four_target, 0, strlen($indicator->q_four_target) - 1);
                                            } else {
                                                $targets[] = $indicator->q_four_target;
                                            }



                                            if(substr($indicator->q_one_actual, -1) == "%") {
                                                $actuals[] = substr($indicator->q_one_actual, 0, strlen($indicator->q_one_actual) - 1);
                                            } else {
                                                $actuals[] = $indicator->q_one_actual;
                                            }

                                            if(substr($indicator->q_two_actual, -1) == "%") {
                                                $actuals[] = substr($indicator->q_two_actual, 0, strlen($indicator->q_two_actual) - 1);
                                            } else {
                                                $actuals[] = $indicator->q_two_actual;
                                            }

                                            if(substr($indicator->q_three_actual, -1) == "%") {
                                                $actuals[] = substr($indicator->q_three_actual, 0, strlen($indicator->q_three_actual) - 1);
                                            } else {
                                                $actuals[] = $indicator->q_three_actual;
                                            }

                                            if(substr($indicator->q_four_actual, -1) == "%") {
                                                $actuals[] = substr($indicator->q_four_actual, 0, strlen($indicator->q_four_actual) - 1);
                                            } else {
                                                $actuals[] = $indicator->q_four_actual;
                                            }


                                        @endphp
                                    {{--@section('loop_st')--}}
                                        <script type="text/javascript">
                                            //-------------
                                            //- BAR CHART -
                                            //-------------

                                            var areaChartData = {
                                                labels  : ['Quarter 1', 'Quarter 2', 'Quarter 3', 'Quarter 4'],
                                                datasets: [
                                                    {
                                                        label               : 'Target',
                                                        fillColor           : 'rgba(210, 214, 222, 1)',
                                                        strokeColor         : 'rgba(210, 214, 222, 1)',
                                                        pointColor          : 'rgba(210, 214, 222, 1)',
                                                        pointStrokeColor    : '#c1c7d1',
                                                        pointHighlightFill  : '#fff',
                                                        pointHighlightStroke: 'rgba(220,220,220,1)',
                                                        data                : [{{ implode(', ', $targets) }}],
                                                        backgroundColor		: 'rgba(54, 162, 235, 1)',
                                                    },
                                                    {
                                                        label               : 'Actual',
                                                        fillColor           : 'rgba(60,141,188,0.9)',
                                                        strokeColor         : 'rgba(60,141,188,0.8)',
                                                        pointColor          : '#3b8bba',
                                                        pointStrokeColor    : 'rgba(60,141,188,1)',
                                                        pointHighlightFill  : '#fff',
                                                        pointHighlightStroke: 'rgba(60,141,188,1)',
                                                        data                : [{{ implode(', ', $actuals) }}],
                                                        backgroundColor		: 'rgba(255,99,132,1)',
                                                    }
                                                ]
                                            }

                                            var ctx = document.getElementById("barChart_{{ $indicator->id }}").getContext('2d');

                                            var myChart = new Chart(ctx, {
                                                type: 'bar',
                                                data: areaChartData,
                                                options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero:true
                                                            }
                                                        }]
                                                    }
                                                }
                                            });
                                        </script>
                                    {{--@endsection--}}
                                    @endforeach
                                    @if(count($result['indicators']) == 0)
                                        <tr>
                                            <td colspan="10" class="text-center"> No Indicators Found!! <a
                                                        href="/wda/ap/indicators"> Add them here </a></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection