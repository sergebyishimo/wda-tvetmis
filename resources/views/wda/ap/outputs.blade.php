@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Action Plan Outputs</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	  Action Plan / Data Input / Outputs</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						
						<table class="table table-bordered" style="background: #fff">
							<thead>
								<tr>
									<th></th>
									<th>Reporting Period</th>
									<th>Program</th>
									<th>Results</th>
									<th style="width: 200px">Action</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($outputs as $output)
									<tr>
										<td></td>
										<td>{{ $output->subprogram->program->rp->period }}</td>
										<td>{{ $output->subprogram->name }}</td>
										<td>{{ $output->name }}</td>
										<td>
											

											<form method="POST">
												{{ csrf_field() }}
												<input type="hidden" name="_method" value="delete">
												<input type="hidden" name="delete_output" value="{{ $output->id }}">
												
												<a href="/ap/input/outputs/{{ $output->id }}" class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit </a>

												<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to remove this program : \n\n {{ $output->name }} ?')"><i class="fa fa-trash"></i> Delete</button>

											</form>

										</td>
									</tr>
								@endforeach

								
									
							</tbody>
						</table>
						

						@if (isset($output_info))
							
							<form method="POST" action="/ap/input/outputs">
								{{ csrf_field() }}
								
								<input type="hidden" name="output_id" value="{{ $output_info->id }}">
								<div class="card card-info">
									<div class="card-header">
										<h3 class="card-title"><i class="fa fa-edit"></i> Edit Output</h3>
									</div>
								
									<div class="card-body">
										
										<div class="form-group">
											<label>Reporting Period</label>
											<p>{{ $output_info->subprogram->program->name }}</p>
										</div>
										<div class="form-group">
											<label>Program</label>
											<p>{{ $output_info->subprogram->name }}</p>
										</div>
										<div class="form-group">
											<label>Results</label>
											<input type="text" class="form-control flat" name="u_name" value="{{ $output_info->name }}" placeholder="Outome">
										</div>
										
									</div>
									<div class="card-footer">
										<button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save Action Plan Output</button>
									</div>
								</div>
							</form>
						@endif

						<form method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="form" value="ap_output">

							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title"><i class="fa fa-plus"></i> Add Output</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Rporting Period</label>
										<select class="form-control flat" name="program_id" id="program">
											<option value="">Choose Here</option>
											@foreach ($rps as $rp)
												<option value="{{ $rp->id }}">{{ $rp->period }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Program</label>
										<select class="form-control flat" name="subprogram_id" id="subprograms">
											
										</select>
									</div>
									<div class="form-group">
										<label>Results</label>
										<input type="text" class="form-control flat" name="name">
									</div>
								</div>
								<div class="card-footer">
									<button type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i> Save Action Plan Output</button>
								</div>
							</div>
						</form>

						

					</div>
				</div>
			</div>
		</section>
	</div>

@endsection