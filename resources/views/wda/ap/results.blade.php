@extends('wda.layout.main')

@section('panel-title', "Action Plan Results")
@section('htmlheader_title', "Action Plan Results")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($result_info))
                <form method="POST" action="{{ route('wda.ap.results.store') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="result_id" value="{{ $result_info->id }}">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Result</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program_id" class="form-control flat">
                                    @foreach ($programs as $program)
                                        <option value="{{ $program->id }}"
                                                @if($result_info->program_id == $program->id) selected @endif >{{ $program->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Result</label>
                                <input type="text" class="form-control flat" name="name"
                                       value="{{ $result_info->name }}" placeholder="Outome">
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i>
                                Save Result
                            </button>
                        </div>
                    </div>
                </form>
            @else
                <div class="mb-4">
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample">
                        Add New Result
                    </button>
                    <div class="collapse" id="collapseExample">
                        <form method="POST" action="{{ route('wda.ap.results.store') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="form" value="sp_result">
                            <div class="box box-info mt-3">
                                <div class="box-header"><i class="fa fa-plus"></i> Add Result</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Reporting Period</label>
                                        <select class="form-control flat" id="ap_rp" required>
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach ($rps as $rp)
                                                <option value="{{ $rp->id }}">{{ $rp->period }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Program</label>
                                        <select class="form-control flat" name="program_id" id="ap_programs"
                                                required>
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach ($programs as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Result</label>
                                        <input type="text" class="form-control flat" name="name" required>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info btn-flat btn-block"><i
                                                class="fa fa-save"></i>
                                        Save
                                        Result
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            @endif

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 60px" class="text-center">#</th>
                    <th>Reporting Period</th>
                    <th style="width: 500px">Program</th>
                    <th>Result</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($results as $result)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $result->program->rp->period }}</td>
                        <td>{{ $result->program->name }}</td>
                        <td>{{ $result->name }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.ap.results.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_result" value="{{ $result->id }}">

                                <a href="{{ route('wda.ap.results.edit', $result->id) }}"
                                   class="btn btn-primary btn-sm btn-flat"> <i
                                            class="fa fa-edit"></i> Edit </a>

                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this result :\n{{ $result->name }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                @if (count($results) == 0)
                    <tr>
                        <td colspan="5" class="text-center"> No Result Found!! Add them below</td>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>
@endsection