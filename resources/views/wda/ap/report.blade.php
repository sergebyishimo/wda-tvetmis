@extends('wda.layout.main')

@section('panel-title', "Action Plan")
@section('htmlheader_title', "Action Plan")

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('css/multi-select.css') }}">
@endsection

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <form method="get" action="{{ route('wda.ap.report.index') }}">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Reporting Period </label>
                            <div class="col-md-8">
                                <select class="form-control flat" name="period_id">
                                    @foreach ($periods as $reporting_period)
                                        <option value="{{ $reporting_period->id }}">{{ $reporting_period->period }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-primary btn-group-sm btn-flat"><i
                                            class="fa fa-list"></i> View
                                </button>
                            </div>
                        </div>
                    </form>

                    {{--Start search content--}}
                    <div id="filterSearch" class="box" style="display: none">
                        <div class="box-header">
                            Advanced Search
                        </div>
                        <div class="box-body">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form class="form-inline" action="{{ route('wda.ap.report.index') }}" method="get"
                                          role="form">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label">
                                                Programs </label>
                                            <div class="col-md-2">
                                                <select class="form-control flat" name="search_programid">
                                                    <option></option>
                                                    @foreach ($datas['programs'] as $program)
                                                        <option value="{{ $program->id }}">{{ $program->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-5 col-form-label">
                                                Result </label>
                                            <div class="col-md-2 pull-right">
                                                <select class="form-control flat" name="search_result">
                                                    <option></option>
                                                    @foreach ($datas['results'] as $result)
                                                        <option value="{{ $result->id }}">{{ $result->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" style="margin-left: 20px;">
                                                Indicator </label>
                                            <div class="col-md-2 pull-right">
                                                <select class="form-control flat" name="search_indicator">
                                                    <option></option>
                                                    @foreach ($datas['indicators'] as $indicator)
                                                        <option value="{{ $indicator->id }}">{{ $indicator->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 70px;">
                                            <button type="submit" class="btn btn-primary filter-col">
                                                <span class="fa fa-search-plus"></span> Search
                                            </button>
                                        </div>
                                        <div class="form-group" style="margin-left: 12px;">
                                            <button id="filterSearchremove" type="button"
                                                    class="btn btn-warning filter-col">
                                                <span class="fa fa-remove"></span> Cancel
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--End search content--}}
                    {{--Start Columns content--}}
                    <div id="filtercolumns" class="box" style="display: none">
                        <div class="box-body">

                            <form action="{{ route('wda.ap.report.index') }}" class="form-inline" role="form"
                                  method="get">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <select multiple="multiple" class="form-control flat" id="my-select"
                                                        name="columns_select[]">
                                                    @foreach($columns['indicators'] as $column)
                                                        <option value="{{'ap_indicators.'.$column->Field}}">{{$column->Field}}
                                                            (Indicators)
                                                        </option>
                                                    @endforeach
                                                    @foreach($columns['programs'] as $program)
                                                        <option value="{{'ap_programs.'.$program->Field}}">{{$program->Field}}
                                                            (Programs)
                                                        </option>
                                                    @endforeach
                                                    @foreach($columns['results'] as $program)
                                                        <option value="{{'ap_results.'.$program->Field}}">{{$program->Field}}
                                                            (Results)
                                                        </option>
                                                    @endforeach
                                                </select> <br/>

                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Reporting Period </label>
                                        <div class="col-md-2">
                                            <select class="form-control flat" name="mySelectId">
                                                @foreach ($periods as $reporting_period)
                                                    <option value="{{ $reporting_period->id }}">{{ $reporting_period->period }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group" style="margin-top: 12px; ">
                                                <button type="submit"
                                                        class="form-control flat btn btn-primary filter-col">
                                                    <span class="fa fa-search-plush"></span>Search Now
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-top: 12px; ">
                                            <div class="form-group">
                                                <button type="button" id="columnremove"
                                                        class="form-control flat btn btn-warning filter-col">
                                                    <span class="fa fa-remove"></span> Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                    {{--End Columns content--}}
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (isset($programs))
                        <div style="overflow-y: auto;">
                            <table class="table table-bordered" id="dataTableBtn">
                                <thead style="background: #fff">
                                <tr>
                                    <th>#</th>
                                    <th style="width: 250px;">Program</th>
                                    <th style="width: 250px;">Results</th>
                                    <th style="width: 280px;">Indicators</th>
                                    <th style="width: 280px;">Baseline</th>
                                    <th>
                                        <table class="table">
                                            <tr style="border: none;background-color: white">
                                                <td>Quarter</td>
                                                <td>Target</td>
                                                <td>Actual</td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $checkObject=array();
                                    $ctp = null;
                                    $ctr = null;
                                    $p = 1;
                                    $class = "bg-gray-light";
                                @endphp
                                @foreach ($programs as $program)
                                    @php
                                        $tmpClass = $class;
                                    @endphp
                                    @if($p % 2 == 0)
                                        @php $class = "bg-gray-light"; @endphp
                                    @else
                                        @php $class = "bg-gray-light"; @endphp
                                    @endif
                                    @php
                                        if ($ctp == $program->programName) {
                                            $class = $tmpClass;
                                        }
                                        if ($ctr == $program->programName) {
                                            $class = $tmpClass;
                                        }
                                    @endphp
                                    @if($p > 1)
                                        @if(! in_array($program->programName,$checkObject) && ! in_array($program->resultName,$checkObject))
                                            <tr class="bg-white">
                                                <td colspan="6">&nbsp;</td>
                                            </tr>
                                        @endif
                                    @endif
                                    <tr>
                                        @if(in_array($program->programName,$checkObject) && in_array($program->resultName,$checkObject))
                                            <td class="{{ $class }}"></td>
                                            <td class="{{ $class }}"></td>
                                            <td class="{{ $class }}"></td>
                                        @else
                                            <td class="{{ $class }}"><a
                                                        href="{{ route('wda.ap.report.result', $program->program_id) }}"
                                                        class="btn btn-warning btn-sm btn-flat"> <i
                                                            class="fa fa-bar-chart"></i></a>
                                            </td>
                                            <td class="{{ $class }}">{{$program->programName}}</td>
                                            <td class="{{ $class }}"> {{$program->resultName}}</td>

                                        @endif
                                        <td><p class="bg-red rounded p-1"
                                               data-toggle="tooltip" data-placement="left"
                                               title="{{ $program->indicatorName }}">
                                                {{ strlen($program->indicatorName) > 61 ?  substr($program->indicatorName, 0, 60)." ..." : $program->indicatorName }}
                                            </p>
                                        </td>
                                        <td><p class="p-2 bg-red">{{$program->baseline}}</p></td>
                                        <td>
                                            <table class="table">
                                                <tr>
                                                    <td>Q1 :</td>
                                                    <td style="background-color: orange">{{ $program->q_one_target }}</td>
                                                    @if($program->q_one_actual < $program->q_one_target)
                                                        <td style="background-color: red">{{ $program->q_one_actual }}</td>
                                                    @elseif($program->q_one_actual > $program->q_one_target)
                                                        <td style="background-color: green">{{ $program->q_one_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_one_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Q2 :</td>
                                                    <td style="background-color: orange">{{ $program->q_two_target }}</td>
                                                    @if($program->q_two_actual < $program->q_two_target)
                                                        <td style="background-color: red">{{ $program->q_two_actual }}</td>
                                                    @elseif($program->q_two_actual > $program->q_two_target)
                                                        <td style="background-color: green">{{ $program->q_two_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_two_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Q3 :</td>
                                                    <td style="background-color: orange">{{ $program->q_three_target }}</td>
                                                    @if($program->q_three_actual < $program->q_three_target)
                                                        <td style="background-color: red">{{ $program->q_three_actual }}</td>
                                                    @elseif($program->q_three_actual > $program->q_three_target)
                                                        <td style="background-color: green">{{ $program->q_three_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_three_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Q4 :</td>
                                                    <td style="background-color: orange">{{ $program->q_four_target }}</td>
                                                    @if($program->q_four_actual < $program->q_four_target)
                                                        <td style="background-color: red">{{ $program->q_four_actual }}</td>
                                                    @elseif($program->q_four_actual > $program->q_four_target)
                                                        <td style="background-color: green">{{ $program->q_four_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_four_actual }}</td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    @php array_push($checkObject,$program->program_id) @endphp
                                    @php array_push($checkObject,$program->programName) @endphp
                                    @php array_push($checkObject,$program->resultName);
                                    @endphp
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    @endif
                    {{--search and columns Looking--}}
                    @if (isset($searchprograms))
                        <div style="overflow-y: auto;">
                            <table class="table table-bordered table-striped" id="dataTableBtn">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 250px;">Program</th>
                                    <th style="width: 250px;">Results</th>
                                    <th style="width: 280px;">Indicators</th>
                                    <th>Baseline</th>
                                    <th>
                                        <table class="table">
                                            <tr style="border: none;background-color: white">
                                                <td>Quarter</td>
                                                <td>Target</td>
                                                <td>Actual</td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $checkObject=array();
                                @endphp
                                @foreach ($searchprograms as $program)

                                    <tr>
                                        @if(in_array($program->programName,$checkObject) && in_array($program->resultName,$checkObject))
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @else
                                            <td><a href="{{ route('wda.ap.report.result', $program->program_id) }}"
                                                   class="btn btn-warning btn-sm btn-flat"> <i
                                                            class="fa fa-bar-chart"></i></a>
                                            </td>
                                            <td>{{$program->programName}}</td>
                                            <td> {{$program->resultName}}</td>

                                        @endif
                                        <td><span class="badge bg-red">{{$program->indicatorName}}</span></td>
                                        <td><span class="badge bg-red">{{$program->baseline}}</span></td>
                                        <td>
                                            <table class="table">
                                                <tr style="border: none;background-color: white">
                                                    <td>Q1 :</td>
                                                    <td style="background-color: orange">{{ $program->q_one_target }}</td>
                                                    @if($program->q_one_actual < $program->q_one_target)
                                                        <td style="background-color: red">{{ $program->q_one_actual }}</td>
                                                    @elseif($program->q_one_actual > $program->q_one_target)
                                                        <td style="background-color: green">{{ $program->q_one_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_one_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Q2 :</td>
                                                    <td style="background-color: orange">{{ $program->q_two_target }}</td>
                                                    @if($program->q_two_actual < $program->q_two_target)
                                                        <td style="background-color: red">{{ $program->q_two_actual }}</td>
                                                    @elseif($program->q_two_actual > $program->q_two_target)
                                                        <td style="background-color: green">{{ $program->q_two_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_two_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Q3 :</td>
                                                    <td style="background-color: orange">{{ $program->q_three_target }}</td>
                                                    @if($program->q_three_actual < $program->q_three_target)
                                                        <td style="background-color: red">{{ $program->q_three_actual }}</td>
                                                    @elseif($program->q_three_actual > $program->q_three_target)
                                                        <td style="background-color: green">{{ $program->q_three_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_three_actual }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Q4 :</td>
                                                    <td style="background-color: orange">{{ $program->q_four_target }}</td>
                                                    @if($program->q_four_actual < $program->q_four_target)
                                                        <td style="background-color: red">{{ $program->q_four_actual }}</td>
                                                    @elseif($program->q_four_actual > $program->q_four_target)
                                                        <td style="background-color: green">{{ $program->q_four_actual }}</td>
                                                    @else
                                                        <td style="background-color: yellow">{{ $program->q_four_actual }}</td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    @php array_push($checkObject,$program->program_id) @endphp
                                    @php array_push($checkObject,$program->programName) @endphp
                                    @php array_push($checkObject,$program->resultName);
                                    @endphp
                                @endforeach
                                </tbody>

                            </table>

                        </div>


                    @endif

                    @if (isset($DataColumnsprograms))

                        <div style="overflow-y: auto;">
                            <table class="table table-bordered table-striped" id="myTable">
                                <thead>
                                <tr>
                                    @php
                                        $columns =explode(',',$columnsDisplays);
                                    @endphp
                                    <th>#</th>
                                    @foreach($columns as $columndata)
                                        @php
                                            $columntodisplay =strtoupper(str_replace_last('_',' ',$columndata));
                                        @endphp
                                        <th>{{str_replace_last('_',' ',$columntodisplay)}}</th>
                                    @endforeach

                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($DataColumnsprograms as $program)
                                    <tr>
                                        @php
                                            $i=0;
                                             echo '<td>
                                                <a href="'.route('wda.ap.report.result', $program->programId).'" class="btn btn-warning btn-sm btn-flat">	<i class="fa fa-bar-chart"></i></a>
                                            </td>';
                                        foreach($columns as $columndata){
                                        $value=trim($columndata);
                                         if($i < count ($columns)) {

                                         echo '<td>'.$program->$value.'</td>
                                            ';

                                         }
                                                $i++;
                                        }

                                        @endphp


                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('js/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#my-select').multiSelect({
                selectableHeader: "<div class='custom-header'>Selectable Columns</div>",
                selectionHeader: "<div class='custom-header'>Selection Columns</div>"
            });
            $(".btnPrint").printPage();
        });
    </script>
@endsection