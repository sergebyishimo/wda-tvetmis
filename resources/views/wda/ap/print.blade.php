@extends('layouts.master')

@section('content')

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Action Plan Results</h1>
					</div>
					<div class="col-md-12">
						@if (isset($programs))
							<div style="overflow-y: auto;">
								<table class="table table-bordered">
									<tr>
										<th style="width: 250px;">Program</th>
										<th style="width: 250px;">Results</th>
										<th style="width: 280px;">Indicators</th>
										<th>Baseline</th>
										<th>
											<table class="table">
												<tr>
													<td>Quarter</td>
													<td>Target</td>
													<td>Actual</td>
												</tr></table></th>
									</tr>
									@php
										$checkObject=array();
									@endphp
									@foreach ($programs as $program)

										<tr>
											@if(in_array($program->programName,$checkObject) && in_array($program->resultName,$checkObject))
												<td></td>
												<td></td>
											@else
												<td>{{$program->programName}}</td>
												<td> {{$program->resultName}}</td>

											@endif
											<td>{{$program->indicatorName}}</td>
											<td>{{$program->baseline}}</td>
											<td>
												<table class="table">
													<tr>
														<td>Q1 :</td>
														<td style="background-color: orange">{{ $program->q_one_target }}</td>
														@if($program->q_one_actual < $program->q_one_target)
															<td style="background-color: red">{{ $program->q_one_actual }}</td>
														@elseif($program->q_one_actual > $program->q_one_target)
															<td style="background-color: green">{{ $program->q_one_actual }}</td>
														@else
															<td style="background-color: yellow">{{ $program->q_one_actual }}</td>
														@endif
													</tr>
													<tr>
														<td>Q2 :</td>
														<td style="background-color: orange">{{ $program->q_two_target }}</td>
														@if($program->q_two_actual < $program->q_two_target)
															<td style="background-color: red">{{ $program->q_two_actual }}</td>
														@elseif($program->q_two_actual > $program->q_two_target)
															<td style="background-color: green">{{ $program->q_two_actual }}</td>
														@else
															<td style="background-color: yellow">{{ $program->q_two_actual }}</td>
														@endif
													</tr>
													<tr>
														<td>Q3 :</td>
														<td style="background-color: orange">{{ $program->q_three_target }}</td>
														@if($program->q_three_actual < $program->q_three_target)
															<td style="background-color: red">{{ $program->q_three_actual }}</td>
														@elseif($program->q_three_actual > $program->q_three_target)
															<td style="background-color: green">{{ $program->q_three_actual }}</td>
														@else
															<td style="background-color: yellow">{{ $program->q_three_actual }}</td>
														@endif
													</tr>
													<tr>
														<td>Q4 :</td>
														<td style="background-color: orange">{{ $program->q_four_target }}</td>
														@if($program->q_four_actual < $program->q_four_target)
															<td style="background-color: red">{{ $program->q_four_actual }}</td>
														@elseif($program->q_four_actual > $program->q_four_target)
															<td style="background-color: green">{{ $program->q_four_actual }}</td>
														@else
															<td style="background-color: yellow">{{ $program->q_four_actual }}</td>
														@endif
													</tr>
												</table>
											</td>

										</tr>
										@php array_push($checkObject,$program->program_id) @endphp
										@php array_push($checkObject,$program->programName) @endphp
										@php array_push($checkObject,$program->resultName);
										@endphp
									@endforeach
								</table>
							</div>
						@endif

					</div>
				</div>
			</div>
		</section>
	</div>

@endsection