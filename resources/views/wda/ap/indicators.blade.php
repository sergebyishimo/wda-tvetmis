@extends('wda.layout.main')

@section('panel-title', "Action Plan Indicators")
@section('htmlheader_title', "Action Plan Indicators")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($indicator_info))
                <form method="POST" action="{{ route('wda.ap.indicators.store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="indicator_id" value="{{ $indicator_info->id }}">
                    <div class="box box-success" id="edit">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Indicator</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="progam_id" id="ap_programs" class="form-control flat">
                                    @foreach ($indicator_info->result->program->rp->programs as $program)
                                        <option value="{{ $program->id }}"
                                                @if($indicator_info->result->program->id == $program->id) selected @endif >{{ $program->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Result</label>
                                <select name="result_id" id="ap_results" class="form-control flat" required>
                                    @foreach ($indicator->result->program->results as $result)
                                        <option value="{{ $result->id }}"
                                                @if($indicator_info->result->id == $result->id) selected @endif >{{ $result->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Indicator</label>
                                <input type="text" class="form-control flat" name="indicator" placeholder="Indicator"
                                       value="{{ $indicator_info->name }}" required=>
                            </div>
                            <div class="form-group">
                                <label>Baseline</label>
                                <input type="text" class="form-control flat" name="baseline" placeholder="Baseline"
                                       value="{{ $indicator_info->baseline }}">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Quarter 1</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_one_target"
                                                       placeholder="Target" value="{{ $indicator_info->q_one_target }}">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_one_actual"
                                                       value="{{ $indicator_info->q_one_actual }}" placeholder="Actual">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Quarter 2</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_two_target"
                                                       placeholder="Target" value="{{ $indicator_info->q_two_target }}">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_two_actual"
                                                       placeholder="Actual" value="{{ $indicator_info->q_two_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Quarter 3</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_three_target"
                                                       placeholder="Target"
                                                       value="{{ $indicator_info->q_three_target }}">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_three_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->q_three_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Quarter 4</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_four_target"
                                                       placeholder="Target "
                                                       value="{{ $indicator_info->q_four_target }}">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control flat" name="q_four_actual"
                                                       placeholder="Actual"
                                                       value="{{ $indicator_info->q_four_actual }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label>Activities to Deliver Output </label>
                                <textarea name="activities_to_deliver_output" style="height: 100px"
                                          class="form-control flat"
                                          placeholder="Activities To Deliver Output">{{ $indicator_info->activities_to_deliver_output }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Stakeholders </label>
                                <input type="text" class="form-control flat" name="stakeholders"
                                       placeholder="Stakeholders" value="{{ $indicator_info->stakeholders }}">
                            </div>
                            <div class="form-group">
                                <label>Budget </label>
                                <input type="text" class="form-control flat" name="available" placeholder="Budget"
                                       value="{{ $indicator_info->available }}">
                            </div>
                            <div class="form-group">
                                <label>Budget Spent </label>
                                <input type="text" class="form-control flat" name="amount_spent"
                                       placeholder="Budget Spent" value="{{ $indicator_info->amount_spent }}">
                            </div>
                            <div class="form-group">
                                <label>Narrative Progress </label>
                                <textarea class="form-control flat" style="height: 100px" name="narrative_support"
                                          placeholder="Narrative Progress"> {{ $indicator_info->narrative_support }} </textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-save"></i>
                                Save Action Plan Indicator
                            </button>
                        </div>
                    </div>
                </form>

            @endif

            @if (!isset($indicator_info))
                <div class="mb-4">
                    <a role="button" class="btn btn-primary"
                       @if(request()->has('o') && request()->get('o') != 'l')
                       href="?o=l"
                       @else
                       href="?o=in"
                            @endif>
                        Add Indicator
                    </a>
                    <div class="collapse {{ request()->get('o') }}" id="collapseExample">
                        <form method="POST" action="{{ route('wda.ap.indicators.store') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="form" value="ap_indicator">

                            <div class="box box-info mt-3">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-plus"></i> Add Indicator</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Reporting Period</label>
                                        <select class="form-control flat" id="ap_rp">
                                            <option value="" disabled selected>Choose Here</option>
                                            @foreach ($rps as $rp)
                                                <option value="{{ $rp->id }}">{{ $rp->period }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Program</label>
                                        <select class="form-control flat" id="ap_programs">

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Results</label>
                                        <select class="form-control flat" name="result_id" id="ap_results" required>

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Indicator</label>
                                        <input type="text" class="form-control flat" name="indicator"
                                               placeholder="Indicator"
                                               required=>
                                    </div>
                                    <div class="form-group">
                                        <label>Baseline</label>
                                        <input type="text" class="form-control flat" name="baseline" placeholder="Baseline"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Quarter 1</label>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_one_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_one_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Quarter 2</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_two_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_two_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Quarter 3</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_three_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_three_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Quarter 4</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_four_target"
                                                               placeholder="Target">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control flat" name="q_four_actual"
                                                               placeholder="Actual">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Activities to Deliver Output </label>
                                        <textarea class="form-control flat" name="activities_to_deliver_output"
                                                  placeholder="Activities To Deliver Output"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Stakeholders </label>
                                        <input type="text" class="form-control flat" name="stakeholders"
                                               placeholder="Stakeholders">
                                    </div>
                                    <div class="form-group">
                                        <label>Budget </label>
                                        <input type="text" class="form-control flat" name="available"
                                               placeholder="Available">
                                    </div>
                                    <div class="form-group">
                                        <label>Budget Spent </label>
                                        <input type="text" class="form-control flat" name="amount_spent"
                                               placeholder="Amount Spent">
                                    </div>
                                    <div class="form-group">
                                        <label>Narrative Progress </label>
                                        <textarea class="form-control flat" name="narrative_support"
                                                  placeholder="Narrative Progress"></textarea>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i>
                                        Save Action Plan Indicator
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 60px" class="text-center">#</th>
                    <th>Reporting Period</th>
                    <th>Program</th>
                    <th style="width: 280px">Result</th>
                    <th>Indicator</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @forelse ($indicators as $indicator)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $indicator->result->program->rp->period }}</td>
                        <td>{{ $indicator->result->program->name }}</td>
                        <td>{{ $indicator->result->name }}</td>
                        <td>{{ $indicator->name }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.ap.indicators.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_indicator" value="{{ $indicator->id }}">

                                <a href="{{ route('wda.ap.indicators.edit',$indicator->id) }}#edit"
                                   class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit </a>

                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this program :\n{{ $indicator->name }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>

                            </form>

                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="6">No Data Available !</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection