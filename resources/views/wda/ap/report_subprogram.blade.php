@extends('layouts.master')

@section('content')

	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						@if(isset($subprogram))
							<div class="card card-primary">
								<div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
									<h3 class="card-title p-3">
										<i class="fa fa-pie-chart mr-1"></i>
										Action Plan Program Details
									</h3>
									<ul class="nav nav-pills ml-auto p-2">
										<li class="nav-item">
											<a class="nav-link" href="/ap/report/{{ $subprogram->program->rp->id }}"><i class="fa fa-arrow-left"></i> Go Back</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/ap/download/indicators/{{ $subprogram->id }}"><i class="fa fa-file-pdf-o"></i> PDF</a>
										</li>
									</ul>
								</div>
								<div class="card-body" 	>
									
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Program</label>
										<p>{{ $subprogram->name }}</p>
									</div>			

									@foreach ($subprogram['outputs'] as $output)
										{{-- expr --}}
									
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Results</label>
										<p>{{ $output->name }}</p>
									</div>

									<div class="form-group">
										<label><i class="fa fa-table"></i> Indicators Table</label>
									</div>

									<div style="overflow-y: scroll;">
										<table class="table table-bordered table-hover" style="width: 2000px" >
											<thead style="background: #f1f1f1">
												<tr>
													<th>#</th>	
													<th style="width: 100px">Indicator</th>
													<th>Baseline</th>
													<th style="width: 300px">Quarter / Target / Actual</th>
													<th>Graph</th>
													<th style="width: 250px">Activites</th>
													<th style="width: 250px">Narrative Progress</th>
													<th style="width: 150px">Stakeholders</th>
													<th style="width: 150px">Budget</th>
													<th style="width: 150px">Budget Spent</th>
													<th style="width: 10%"></th>
												</tr>
											</thead>
											<tbody>
												@php
													$i = 1;
												@endphp
												@foreach($output['indicators'] as $indicator)
													<tr>
														<td>{{ $i++ }}</td>
														<td>{{ $indicator->name }}</td>
														<td>{{ $indicator->baseline }}</td>

														<td style="padding: 0;">
															<table class="table table-bordered table-hover table-striped" style="margin-bottom: -2px;">
																<tr>
																	<td>Q. 1</td>
																	
																	
																		@if ($indicator->q_one_target > $indicator->q_one_actual)
																		<td style="background: green;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_one_target }}</td>
																		<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">
																			<span >
																				{{ $indicator->q_one_actual }}
																			</span>
																		</td>
																		@else
																			<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_one_target }}</td>
																			<td style="background: green;color: #fff;font-weight: bold" class="text-center">
																				{{ $indicator->q_one_actual }}
																			</td>
																		@endif 
																	
																</tr>
																<tr>
																	<td>Q. 2</td>
																	
																		@if ($indicator->q_two_target > $indicator->q_two_actual)
																		<td style="background: green;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_two_target }}</td>
																		<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">
																			<span >
																				{{ $indicator->q_two_actual }}
																			</span>
																		</td>
																		@else
																			<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_two_target }}</td>
																			<td style="background: green;color: #fff;font-weight: bold" class="text-center">
																				{{ $indicator->q_two_actual }}
																			</td>
																		@endif
																	
																</tr>
																<tr>
																	<td>Q. 3</td>
																	
																		@if ($indicator->q_three_target > $indicator->q_three_actual)
																		<td style="background: green;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_three_target }}</td>
																		<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">
																			<span >
																				{{ $indicator->q_three_actual }}
																			</span>
																		</td>
																		@else
																			<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_three_target }}</td>
																			<td style="background: green;color: #fff;font-weight: bold" class="text-center">
																				{{ $indicator->q_three_actual }}
																			</td>
																		@endif
																	
																</tr>
																<tr>
																	<td>Q. 4</td>
																	
																		@if ($indicator->q_four_target > $indicator->q_four_actual)
																		<td style="background: green;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_four_target }}</td>
																		<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">
																			<span >
																				{{ $indicator->q_four_actual }}
																			</span>
																		</td>
																		@else
																			<td style="background: #cc0000;color: #fff;font-weight: bold" class="text-center">{{ $indicator->q_four_target }}</td>
																			<td style="background: green;color: #fff;font-weight: bold" class="text-center">
																				{{ $indicator->q_four_actual }}
																			</td>
																		@endif
																	
																</tr>
															</table>	
														</td>
														<td><canvas id="barChart" height="200"  ></canvas>	</td>
														<td>{{ $indicator->activities_to_deliver_output }}</td>
														<td>{{ $indicator->narrative_report }}</td>
														<td>{{ $indicator->stakeholders }}</td>
														<td>{{ $indicator->amount }}</td>
														<td>{{ $indicator->amount_spent }}</td>
														<td>
															<a href="/ap/input/indicators/{{ $indicator->id }}" class="btn btn-primary btn-flat btn-sm"> <i class="fa fa-pencil"></i> Edit Indicator</a>
															<form method="POST" action="/ap/input/indicators">
																{{ csrf_field() }}
																<input type="hidden" name="_method" value="delete">
																<input type="hidden" name="indicator_id" value="{{ $indicator->id }}">
																<button type="submit" class="btn btn-danger btn-flat btn-sm" onclick="return window.confirm('Are you sure you want to remove this indicator ?')"> <i class="fa fa-trash"></i> Delete Indicator</button>
															</form>
															
														</td>
													</tr>
															@php
														    	$labels = [];
														    	$targets = [];
														    	$actuals = [];

														    	if(substr($indicator->q_one_target, -1) == "%") {
														            $targets[] = substr($indicator->q_one_target, 0, strlen($indicator->q_one_target) - 1); 
														        } else {
														            $targets[] = $indicator->q_one_target;
														        }

														        if(substr($indicator->q_two_target, -1) == "%") {
														            $targets[] = substr($indicator->q_two_target, 0, strlen($indicator->q_two_target) - 1); 
														        } else {
														            $targets[] = $indicator->q_two_target;
														        }

														        if(substr($indicator->q_three_target, -1) == "%") {
														            $targets[] = substr($indicator->q_three_target, 0, strlen($indicator->q_three_target) - 1); 
														        } else {
														            $targets[] = $indicator->q_three_target;
														        }

														        if(substr($indicator->q_four_target, -1) == "%") {
														            $targets[] = substr($indicator->q_four_target, 0, strlen($indicator->q_four_target) - 1); 
														        } else {
														            $targets[] = $indicator->q_four_target;
														        }	

														        


														        if(substr($indicator->q_one_actual, -1) == "%") {
														            $actuals[] = substr($indicator->q_one_actual, 0, strlen($indicator->q_one_actual) - 1); 
														        } else {
														            $actuals[] = $indicator->q_one_actual;
														        }

														        if(substr($indicator->q_two_actual, -1) == "%") {
														            $actuals[] = substr($indicator->q_two_actual, 0, strlen($indicator->q_two_actual) - 1); 
														        } else {
														            $actuals[] = $indicator->q_two_actual;
														        }

														        if(substr($indicator->q_three_actual, -1) == "%") {
														            $actuals[] = substr($indicator->q_three_actual, 0, strlen($indicator->q_three_actual) - 1); 
														        } else {
														            $actuals[] = $indicator->q_three_actual;
														        }

														        if(substr($indicator->q_four_actual, -1) == "%") {
														            $actuals[] = substr($indicator->q_four_actual, 0, strlen($indicator->q_four_actual) - 1); 
														        } else {
														            $actuals[] = $indicator->q_four_actual;
														        }


														        
														    	
														    @endphp

														    <script type="text/javascript">
																//-------------
														    //- BAR CHART -
														    //-------------

														    var areaChartData = {
														      labels  : ['Quarter 1', 'Quarter 2', 'Quarter 3', 'Quarter 4'],
														      datasets: [
														        {
																	label               : 'Target',
																	fillColor           : 'rgba(210, 214, 222, 1)',
																	strokeColor         : 'rgba(210, 214, 222, 1)',
																	pointColor          : 'rgba(210, 214, 222, 1)',
																	pointStrokeColor    : '#c1c7d1',
																	pointHighlightFill  : '#fff',
																	pointHighlightStroke: 'rgba(220,220,220,1)',
																	data                : [{{ implode(', ', $targets) }}],
																	backgroundColor		: 'rgba(54, 162, 235, 1)',
														        },
														        {
																	label               : 'Actual',
																	fillColor           : 'rgba(60,141,188,0.9)',
																	strokeColor         : 'rgba(60,141,188,0.8)',
																	pointColor          : '#3b8bba',
																	pointStrokeColor    : 'rgba(60,141,188,1)',
																	pointHighlightFill  : '#fff',
																	pointHighlightStroke: 'rgba(60,141,188,1)',
																	data                : [{{ implode(', ', $actuals) }}],
																	backgroundColor		: 'rgba(255,99,132,1)',
														        }
														      ]
														    }

														    var ctx = document.getElementById("barChart").getContext('2d');

														    var myChart = new Chart(ctx, {
														            type: 'bar',
														            data: areaChartData,
														            options: {
														                scales: {
														                    yAxes: [{
														                        ticks: {
														                            beginAtZero:true
														                        }
														                    }]
														                }
														            }
														        });
															</script>
														
												@endforeach

												@if(count($output['indicators']) == 0)
													<tr>
														<td colspan="10" class="text-center"> No Indicators Found!!  <a href="/ap/input/indicators">  Add them here </a>  </td>
													</tr>
												@endif
											</tbody>
										</table>
									</div>
									@endforeach
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</section>

	</div>
@endsection