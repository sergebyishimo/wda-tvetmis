@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 22px;padding-left: 5px;"> <i class="fa fa-edit"></i> Action Plan Programs</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">	  Action Plan / Data Input / Programs</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						
						<table class="table table-bordered" style="background: #fff">
							<thead>
								<tr>
									<th></th>
									<th>Reporting Period</th>
									<th style="width: 150px">Program  Number</th>
									<th>Program</th>
									<th style="width: 200px">Action</th>
								</tr>
							</thead>
							<tbody>

								@foreach ($subprograms as $subprogram)
									<tr>
										<td></td>
										<td>{{ $subprogram->program->rp->period }}</td>
										<td>{{ $subprogram->program_number }}</td>
										<td>{{ $subprogram->name }}</td>
										<td>
											

											<form method="POST">
												{{ csrf_field() }}
												<input type="hidden" name="_method" value="delete">
												<input type="hidden" name="delete_subpro" value="{{ $subprogram->id }}">
												
												<a href="/ap/input/subprograms/{{ $subprogram->id }}" class="btn btn-primary btn-sm btn-flat"> <i class="fa fa-edit"></i> Edit </a>
												<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return window.confirm('Are you sure you want to remove this program : \n\n {{ $subprogram->name }} ?')"><i class="fa fa-trash"></i> Delete</button>

											</form>

										</td>
									</tr>
								@endforeach

								<form method="POST" id="form">
									{{ csrf_field() }}
									<tr>
										<td></td>
										<td>
											<select class="form-control flat" name="program_id">
												@foreach ($rps as $rp)
													<option value="{{ $rp->id }}">{{ $rp->period }}</option>
												@endforeach
											</select>
										</td>
										<td>
											<input type="text" class="form-control flat" name="number" placeholder="Number">
										</td>
										<td>
											<input type="text" class="form-control flat" name="name" placeholder="Program">
										</td>
										<td class="text-center"><i class="fa fa-check" style="font-size: 26px;color: green;cursor: pointer" onclick='$("#form").trigger("submit")'></i> </td>
									</tr>
								</form>
									
							</tbody>
						</table>
						
						@if (isset($subprogram_info))
							
							<form method="POST" action="/ap/input/subprograms">
								{{ csrf_field() }}
								
								<input type="hidden" name="subprogram_id" value="{{ $subprogram_info->id }}">
								<div class="card card-info">
									<div class="card-header">
										<h3 class="card-title"><i class="fa fa-edit"></i> Edit Program</h3>
									</div>
								
									<div class="card-body">
										
										<div class="form-group">
											<label>Reporting Period</label>
											<p>{{ $subprogram_info->program->rp->peior }}</p>
										</div>
										<div class="form-group">
											<label>Program Number</label>
											<input type="text" class="form-control flat" name="u_program_number" value="{{ $subprogram_info->program_number }}" placeholder="Program Number">
										</div>
										<div class="form-group">
											<label>Program</label>
											<input type="text" class="form-control flat" name="u_name" value="{{ $subprogram_info->name }}" placeholder="Program">
										</div>
										
									</div>
									<div class="card-footer">
										<button type="submit" class="btn btn-info btn-flat btn-block"><i class="fa fa-save"></i> Save Action Plan Sub Program</button>
									</div>
								</div>
							</form>
						@endif

					</div>
				</div>
			</div>
		</section>
	</div>

@endsection