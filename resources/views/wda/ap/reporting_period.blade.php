@extends('wda.layout.main')

@section('panel-title', "Reporting Period")
@section('htmlheader_title', "Reporting Period")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            @if (isset($rp_info))
                <form method="POST" action="{{ route('wda.ap.rp.index') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="rp_id" value="{{ $rp_info->id }}">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-edit"></i> Edit Reporting Period</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label>Reporting Period</label>
                                <input type="text" class="form-control flat" name="u_reporting_period"
                                       value="{{ $rp_info->period }}" placeholder="Program">
                            </div>

                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-info btn-flat btn-block"><i
                                                class="fa fa-save"></i> Save Action Plan Program
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('wda.ap.rp.index') }}" class="btn btn-default btn-flat btn-block"><i
                                                class="fa fa-close"></i> Cancel</a>
                                </div>
                            </div>


                        </div>
                    </div>
                </form>
            @endif

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 60px" class="text-center">#</th>
                    <th>Reporting Period</th>
                    <th style="width: 200px">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i =1;
                @endphp
                @forelse ($rps as $rp)
                    <tr>
                        <td class="text-center">{{ $i++ }}</td>
                        <td>{{ $rp->period }}</td>
                        <td>
                            <form method="POST" action="{{ route('wda.ap.rp.destroy') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="delete_rp" value="{{ $rp->id }}">

                                <a href="{{ route('wda.ap.rp.edit', $rp->id) }}"
                                   class="btn btn-primary btn-sm btn-flat"> <i
                                            class="fa fa-edit"></i> Edit </a>

                                <button type="submit" class="btn btn-danger btn-sm btn-flat"
                                        onclick="return window.confirm('Are you sure you want to remove this program :\n{{ $rp->period }} ?')">
                                    <i class="fa fa-trash"></i> Delete
                                </button>

                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="3">No Data Available !</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <form method="POST" id="form" action="{{ route('wda.ap.rp.index') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <input type="text" class="form-control flat" name="reporting_period"
                                   placeholder="Reporting Period">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-md btn-block btn-flat"><i
                                    class="fa fa-check"></i> Save
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection