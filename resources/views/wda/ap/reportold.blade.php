@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 26px;padding-left: 5px;"> <i class="fa fa-pie-chart"></i> Action Plan</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-rfight">
							<li class="breadcrumb-item"><a href="/home">Home</a></li>
							<li class="breadcrumb-item active">Action Plan / Report</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>


		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						<div class="card">
							<div class="card-header">
								<form method="POST" action="{{url('/ap/report/older')}}">
									{{ csrf_field() }}
								
									<div class="form-group row">
										<label class="col-md-2 col-form-label">Reporting Period </label>
										<div class="col-md-2">
											<select class="form-control flat" name="period_id">
												@foreach ($periods as $reporting_period)
													<option value="{{ $reporting_period->id }}">{{ $reporting_period->period }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-list"></i> View </button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								@if (isset($programs))
								
								<div style="overflow-y: auto">

								<table class="table">
									<tr>
										<th style="width: 10px">#</th>
										<th style="width: 250px;">Program</th>
										<th style="width: 250px;">Results</th>
										<th style="width: 280px;">Indicators</th>
										<th>Baseline</th>
										<th>Quarter / Target / Actual</th>
									</tr>
								</table>
										<table class="table">
											@foreach ($programs as $program)
												<tr>
													<td></td>
													<td style="width: 250px;">{{ $program->name }}
															<br class="left"> <a href="/ap/report/result/{{ $program->id }}" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-list"></i> View More Details</a>
													</td>
													<td>
														<table class="table">
															@foreach ($program['results'] as $result)
																<tr>
																	<td style="width: 250px;">{{ $result->name }}
																		
																	</td>
																	<td>
																		<table class="table">
																			@foreach ($result['indicators'] as $indicator)
																				<tr>
																					<td>{{ $indicator->name }}</td>
																					<td>{{ $indicator->baseline }}</td>
																					<td>
																						<table class="table">
																							<tr>
																								<td>Q1 :</td>
																								<td>{{ $indicator->q_one_target }}</td>
																								<td>{{ $indicator->q_one_actual }}</td>
																							</tr>
																							<tr>
																								<td>Q2 :</td>
																								<td>{{ $indicator->q_two_target }}</td>
																								<td>{{ $indicator->q_two_actual }}</td>
																							</tr>
																							<tr>
																								<td>Q3 :</td>
																								<td>{{ $indicator->q_three_target }}</td>
																								<td>{{ $indicator->q_three_actual }}</td>
																							</tr>
																							<tr>
																								<td>Q4 :</td>
																								<td>{{ $indicator->q_four_target }}</td>
																								<td>{{ $indicator->q_four_actual }}</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			@endforeach
																		</table>
																	</td>
																</tr>
															@endforeach
														</table>
													</td>
												</tr>
											@endforeach
										</table>

								@endif
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>
@endsection