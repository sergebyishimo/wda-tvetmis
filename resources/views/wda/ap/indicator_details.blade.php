@extends('layouts.master')

@section('content')

	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						<div class="col-md-12">
							
							<div class="card card-info">
								<div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
									<h3 class="card-title p-3">
										<i class="fa fa-pie-chart mr-1"></i>
										View Indicator Information
									</h3>
									<ul class="nav nav-pills ml-auto p-2">
										<li class="nav-item">
											<a class="nav-link" href="/ap/report/more/{{ $indicator->output->id }}"><i class="fa fa-arrow-left"></i> Go Back</a>
										</li>
									</ul>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Program</label>
										<p>{{ $indicator->output->subprogram->program->name }}</p>
									</div>			
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Sub program</label>
										<p>{{ $indicator->output->subprogram->name }}</p>
									</div>			
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Output</label>
										<p>{{ $indicator->output->name }}</p>
									</div>
									<div class="form-group">
										<label><i class="fa fa-check-circle"></i> Indicator</label>
										<p>{{ $indicator->name }}</p>
									</div>

									<canvas id="barChart" style="height:230px"></canvas>
									<br>
									<table class="table table-bordered">
										<tr class="text-center">
											<th colspan="2">Quarter 1</th>
											<th colspan="2">Quarter 2</th>
											<th colspan="2">Quarter 3</th>
											<th colspan="2">Quarter 4</th>
										</tr>
										<tr>
											<td>{{ $indicator->q_one_target }}</td>
											<td>{{ $indicator->q_one_actual }}</td>
											<td>{{ $indicator->q_two_target }}</td>
											<td>{{ $indicator->q_two_actual }}</td>
											<td>{{ $indicator->q_three_target }}</td>
											<td>{{ $indicator->q_three_actual }}</td>
											<td>{{ $indicator->q_four_target }}</td>
											<td>{{ $indicator->q_four_actual }}</td>
										</tr>
									</table>
								</div>
								<div class="card-footer">
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	

    @php
    	$labels = [];
    	$targets = [];
    	$actuals = [];

    	if(substr($indicator->q_one_target, -1) == "%") {
            $targets[] = substr($indicator->q_one_target, 0, strlen($indicator->q_one_target) - 1); 
        } else {
            $targets[] = $indicator->q_one_target;
        }

        if(substr($indicator->q_two_target, -1) == "%") {
            $targets[] = substr($indicator->q_two_target, 0, strlen($indicator->q_two_target) - 1); 
        } else {
            $targets[] = $indicator->q_two_target;
        }

        if(substr($indicator->q_three_target, -1) == "%") {
            $targets[] = substr($indicator->q_three_target, 0, strlen($indicator->q_three_target) - 1); 
        } else {
            $targets[] = $indicator->q_three_target;
        }

        if(substr($indicator->q_four_target, -1) == "%") {
            $targets[] = substr($indicator->q_four_target, 0, strlen($indicator->q_four_target) - 1); 
        } else {
            $targets[] = $indicator->q_four_target;
        }	

        


        if(substr($indicator->q_one_actual, -1) == "%") {
            $actuals[] = substr($indicator->q_one_actual, 0, strlen($indicator->q_one_actual) - 1); 
        } else {
            $actuals[] = $indicator->q_one_actual;
        }

        if(substr($indicator->q_two_actual, -1) == "%") {
            $actuals[] = substr($indicator->q_two_actual, 0, strlen($indicator->q_two_actual) - 1); 
        } else {
            $actuals[] = $indicator->q_two_actual;
        }

        if(substr($indicator->q_three_actual, -1) == "%") {
            $actuals[] = substr($indicator->q_three_actual, 0, strlen($indicator->q_three_actual) - 1); 
        } else {
            $actuals[] = $indicator->q_three_actual;
        }

        if(substr($indicator->q_four_actual, -1) == "%") {
            $actuals[] = substr($indicator->q_four_actual, 0, strlen($indicator->q_four_actual) - 1); 
        } else {
            $actuals[] = $indicator->q_four_actual;
        }


        
    	
    @endphp

    <script type="text/javascript">
		//-------------
    //- BAR CHART -
    //-------------

    var areaChartData = {
      labels  : ['Quarter 1', 'Quarter 2', 'Quarter 3', 'Quarter 4'],
      datasets: [
        {
			label               : 'Target',
			fillColor           : 'rgba(210, 214, 222, 1)',
			strokeColor         : 'rgba(210, 214, 222, 1)',
			pointColor          : 'rgba(210, 214, 222, 1)',
			pointStrokeColor    : '#c1c7d1',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(220,220,220,1)',
			data                : [{{ implode(', ', $targets) }}],
			backgroundColor		: 'rgba(54, 162, 235, 1)',
        },
        {
			label               : 'Actual',
			fillColor           : 'rgba(60,141,188,0.9)',
			strokeColor         : 'rgba(60,141,188,0.8)',
			pointColor          : '#3b8bba',
			pointStrokeColor    : 'rgba(60,141,188,1)',
			pointHighlightFill  : '#fff',
			pointHighlightStroke: 'rgba(60,141,188,1)',
			data                : [{{ implode(', ', $actuals) }}],
			backgroundColor		: 'rgba(255,99,132,1)',
        }
      ]
    }

    var ctx = document.getElementById("barChart").getContext('2d');

    var myChart = new Chart(ctx, {
            type: 'bar',
            data: areaChartData,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
	</script>
@endsection