@extends('layouts.master')

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 style="font-size: 26px;padding-left: 5px;"> <i class="fa fa-pie-chart"></i> Action Plan Charts</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/">Home</a></li>
							<li class="breadcrumb-item active">Action Plan / Charts</li>
						</ol>
					</div>
				</div>
				@if (session('successMessage'))
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fa fa-check"></i> Operation Done!</h5>
						{!! session('successMessage') !!}
					</div>

				@endif
			</div><!-- /.container-fluid -->
		</section>

		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
					
						<div class="card card-success">
							<div class="card-header d-flex p-0 ui-sortable-handle">
								<h3 class="card-title p-3">View Output Charts</h3>
								<ul class="nav nav-pills ml-auto p-2">
									<li class="nav-item">
										<a class="nav-link" href="/ap/report/{{ $output->subprogram->program->id }}"><i class="fa fa-arrow-left"></i> Go Back</a>
									</li>
								</ul>
							</div>
							<div class="card-body">
								<div class="form-group">
									<label><i class="fa fa-check-circle"></i> Program</label>
									<p>{{ $output->subprogram->program->name }}</p>
								</div>
								<div class="form-group">
									<label><i class="fa fa-check-circle"></i> Sub Program</label>
									<p>{{ $output->subprogram->name }}</p>
								</div>
								<div class="form-group">
									<label><i class="fa fa-check-circle"></i> Output</label>
									<p>{{ $output->name }}</p>
								</div>
								<div class="form-group">
									<label><i class="fa fa-check-circle"></i> Output Charts</label>
								</div>

								@foreach ($output['indicators'] as $indicator)
									<br>
									<div class="form-group">
										<label><i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp; {{ $indicator->name }}</label>
									</div>

									<canvas id="barChart_{{ $indicator->id }}" style="height:230px"></canvas>

									@php

										$targets = [];
										$actuals = [];

										if(substr($indicator['q_one_target'], -1) == "%") {
							                $targets[] = substr($indicator['q_one_target'], 0, strlen($indicator['q_one_target']) - 1); 
							            } else {
							                $targets[] = $indicator['q_one_target'];
							            }

							            if(substr($indicator['q_two_target'], -1) == "%") {
							                $targets[] = substr($indicator['q_two_target'], 0, strlen($indicator['q_one_target']) - 1); 
							            } else {
							                $targets[] = $indicator['q_two_target'];
							            }
							            
							            if(substr($indicator['q_three_target'], -1) == "%") {
							                $targets[] = substr($indicator['q_three_target'], 0, strlen($indicator['q_one_target']) - 1); 
							            } else {
							                $targets[] = $indicator['q_three_target'];
							            }
							            
							            if(substr($indicator['q_four_target'], -1) == "%") {
							                $targets[] = substr($indicator['q_four_target'], 0, strlen($indicator['q_one_target']) - 1); 
							            } else {
							                $targets[] = $indicator['q_four_target'];
							            }



							            if(substr($indicator['q_one_actual'], -1) == "%") {
							                $actuals[] = substr($indicator['q_one_target'], 0, strlen($indicator['q_one_actual']) - 1); 
							            } else {
							                $actuals[] = $indicator['q_one_actual'];
							            }

							            if(substr($indicator['q_two_actual'], -1) == "%") {
							                $actuals[] = substr($indicator['q_two_target'], 0, strlen($indicator['q_two_actual']) - 1); 
							            } else {
							                $actuals[] = $indicator['q_two_actual'];
							            }

							            if(substr($indicator['q_two_actual'], -1) == "%") {
							                $actuals[] = substr($indicator['q_two_target'], 0, strlen($indicator['q_two_actual']) - 1); 
							            } else {
							                $actuals[] = $indicator['q_two_actual'];
							            }

							            if(substr($indicator['q_one_actual'], -1) == "%") {
							                $actuals[] = substr($indicator['q_two_target'], 0, strlen($indicator['q_two_actual']) - 1); 
							            } else {
							                $actuals[] = $indicator['q_two_actual'];
							            }

									@endphp


									

									<script type="text/javascript">

										var areaChartData = {
									      labels  : ['Q1 Target / Actual', 'Q2 Target / Actual', 'Q3 Target / Actual', 'Q4 Target / Actual'],
									      datasets: [
									        {
												label               : 'Target',
												fillColor           : 'rgba(210, 214, 222, 1)',
												strokeColor         : 'rgba(210, 214, 222, 1)',
												pointColor          : 'rgba(210, 214, 222, 1)',
												pointStrokeColor    : '#c1c7d1',
												pointHighlightFill  : '#fff',
												pointHighlightStroke: 'rgba(220,220,220,1)',
												data                : [{{ implode(', ', $targets)}}],
												backgroundColor		: 'rgba(54, 162, 235, 1)',
									        },
									        {
												label               : 'Actual',
												fillColor           : 'rgba(60,141,188,0.9)',
												strokeColor         : 'rgba(60,141,188,0.8)',
												pointColor          : '#3b8bba',
												pointStrokeColor    : 'rgba(60,141,188,1)',
												pointHighlightFill  : '#fff',
												pointHighlightStroke: 'rgba(60,141,188,1)',
												data                : [{{ implode(', ', $actuals)}}],
												backgroundColor		: 'rgba(255,99,132,1)',
									        }
									      ]
									    }

									    var ctx = document.getElementById("barChart_{{ $indicator->id }}").getContext('2d');

									    var myChart = new Chart(ctx, {
									            type: 'bar',
									            data: areaChartData,
									            options: {
									                scales: {
									                    yAxes: [{
									                        ticks: {
									                            beginAtZero:true
									                        }
									                    }]
									                }
									            }
									        });
									</script>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection