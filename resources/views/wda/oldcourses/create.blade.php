@extends('wda.layout.main')

@section('htmlheader_title')
    Old Courses
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Old Courses
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if(isset($oldcourse))
                    @php($route = ['wda.oldcourses.update', $oldcourse->id])
                    @php($id = "updateOldCourse")
                @else
                    @php($route = 'wda.oldcourses.store')
                    @php($id = "createOldCourse")
                @endif
                {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                @if(isset($oldcourse))
                    @method("PATCH")
                    {!! Form::hidden('id', $oldcourse->id) !!}
                @endif
                <div class="box">
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('combination_id', 'Combination Name *', ['class' => 'label-control']) !!}
                                {!! Form::select('combination_id', $combinations->pluck('combination_name','id')  , "", ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('course_name', 'Course Name *', ['class' => 'label-control']) !!}
                                    {!! Form::text('course_name', $oldcourse ? $oldcourse->course_name : null,['class' => 'form-control']) !!}
                                    @if ($errors->has('course_name'))
                                        <span class="help-block text-red">
                                            {{ $errors->first('course_name') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('course_code', 'Course Code *', ['class' => 'label-control']) !!}
                                    {!! Form::text('course_code', $oldcourse ? $oldcourse->course_code : null,['class' => 'form-control']) !!}
                                    @if ($errors->has('course_code'))
                                        <span class="help-block text-red">
                                            {{ $errors->first('course_code') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-md btn-primary pull-left">
                            {{ $oldcourse ? "Update Course" : "Save Course" }}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($oldcourse)
        {!! JsValidator::formRequest('App\Http\Requests\WdaOldCourseUpdate', '#updateOldCourse'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\WdaOldCourseCreate', '#createOldCourse'); !!}
    @endif
@show