@extends('rp.layout.main')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        College List
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>College Names</th>
                                <th>College Acronym</th>
                                <th>Applicant Number</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach(\App\Rp\Polytechnic::all() as $polytechnic)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ ucwords($polytechnic->polytechnic) }}</td>
                                    <td>{{ ucwords($polytechnic->short_name) }}</td>
                                    <td>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>First Choice</td>
                                                <td>{{ applicant()->firstChoice($polytechnic->id) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Second Choice</td>
                                                <td>{{ applicant()->secondChoice($polytechnic->id) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Third Choice</td>
                                                <td>{{ applicant()->thirdChoice($polytechnic->id) }}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        @if (rpAllowed(2))
                                            <a href="{{ route('rp.colleges.show', $polytechnic->id) }}"
                                               class="btn btn-primary btn-md">View</a>
                                        @endif
                                        @if( $polytechnic->user->count() <= 0 && rpAllowed(7))
                                            <a href="{{ route('rp.college.account', $polytechnic->id) }}"
                                               class="btn btn-info btn-md">Account</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show