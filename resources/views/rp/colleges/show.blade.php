@extends('rp.layout.main')

@section('htmlheader_title')
    {{ "View More - ".$college->polytechnic }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        {{ $college->polytechnic }}
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="panel">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Student Admitted</th>
                                        <th class="text-center">Student Paid</th>
                                        <th class="text-center">Student Registered</th>
                                        <th class="text-center">Student Remain</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr align="center">
                                        <td>{{ college($college->id, 'students')->count() }}</td>
                                        <td>0</td>
                                        <td>{{ college($college->id, 'myStudents')->count() }}</td>
                                        <td>{{ getNotFinishedStudents($college->id)->count() }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="clearfix">&nbsp;</div>
                                <div class="row">
                                    <div class="col-md-12">
                                        @if($errors->count() > 0)
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                                @foreach($errors->all() as $error)
                                                    {!! $error !!}
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6 class="panel-title">Student</h6>
                            </div>
                            <div class="panel-body">
                                <table class="table" id="dataTable">
                                    <thead>
                                    <tr>
                                        <th width="30px">#</th>
                                        <th>Student Names</th>
                                        <th>State</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($x=1)
                                    @foreach(college($college->id, 'allStudents') as $student)
                                        <tr>
                                            <td>{{ $x++ }}</td>
                                            <td>{{ $student->names }}</td>
                                            <td>
                                                <label class="label label-{{ applied($student->std_id) ? 'primary' : 'warning' }}">{{ applied($student->std_id) ? "Registered" : "Not Registered Yet" }}</label>
                                            </td>
                                            <th>
                                                <a href="{{ route('rp.college.show.student',[$college->id, $student->std_id]) }}"
                                                   class="btn btn-primary btn-sm">See More</a>
                                            </th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show