@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Transfer Out Student
@endsection

@section('head_css')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Transfer Out Student
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row mb-1">
            <div class="col-md-2">
                <a href="{{ route('college.student.transfer.going') }}"
                   class="btn btn-block btn-warning {{ activeMenu('college.student.transfer.going') }}">Request Out</a>
            </div>
            <div class="col-md-2">
                <a href="{{ route('college.student.transfer.coming') }}"
                   class="btn btn-block btn-info {{ activeMenu('college.student.transfer.coming') }}">Request In</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        @include('flash_msg.flash-message')
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Student name</th>
                                <th>Requested College</th>
                                <th>Requested Department</th>
                                <th>Requested Option</th>
                                <th>Letter</th>
                                <th>Current Department</th>
                                <th>Current Option</th>
                                <th></th>
                            </tr>
                            </thead>
                            @php($i = 1 )
                            @foreach($requests as $request)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ getStudentInfo($request->student_id, 'names') }}</td>
                                    <td>{!! $request->college !!}</td>
                                    <td>{!! $request->department? $request->department->department_name : "" !!}</td>
                                    <td>{!! $request->course ? $request->course->program_name : "" !!}</td>
                                    <td>
                                        <a href="{{ asset('storage/'.$request->letter) }}" target="_blank"
                                           class="btn btn-link">Read</a>
                                    </td>
                                    <td>{!! getDepartment($request->from_department_id, 'department_name') !!}</td>
                                    <td>{!! getCourse($request->from_course_id, 'program_name') !!}</td>
                                    <td>
                                        @if(auth()->guard('rp')->check())
                                            <a href="{{ route('rp.college.show.student',[getStudentInfo($request->student_id, 'college_id', true), $request->student_id]) }}"
                                               class="btn btn-primary btn-sm">See More</a>
                                        @elseif(auth()->guard('college')->check())
                                            <a href="{{ route('college.show.student',[getStudentInfo($request->student_id, 'college_id', true), $request->student_id]) }}"
                                               class="btn btn-primary btn-sm">See More</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@endsection