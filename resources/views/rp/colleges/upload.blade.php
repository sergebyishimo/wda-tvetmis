@extends('rp.layout.main')

@section('htmlheader_title')
    Upload Admitted Students
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Upload Admitted Students
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    {!! Form::open(['files' => true,'route' => 'rp.colleges.upload', 'class' => 'form', 'id' => 'uploadStudents']) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="form-group">
                                    {!!  Form::label('file', "Students Upload") !!}
                                    {{--{!! Form::file('file', [ 'required' => true]) !!}--}}
                                    <input type="file" name="file" id="file" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-sm">Upload</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ asset('files/format.csv') }}" target="_blank"
                                   class="btn btn-link pull-right">Download Format in Excel</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UploadStudent', '#uploadStudenats'); !!}
@show