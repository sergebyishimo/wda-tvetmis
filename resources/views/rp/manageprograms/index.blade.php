@extends('rp.layout.main')

@section('htmlheader_title')
    Manage Programs
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Manage Programs
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if (rpAllowed(4))
                    <div class="box">
                        @if($manageprogram)
                            @php($route = ['rp.manageprograms.update', $manageprogram->id])
                            @php($id = "updatePManage")
                        @else
                            @php($route = 'rp.manageprograms.store')
                            @php($id = "createPManage")
                        @endif
                        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                        @if($manageprogram && $program)
                            @method("PATCH")
                            {!! Form::hidden('id', $manageprogram->id) !!}
                        @endif
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('program_id', "Program Name *") !!}
                                        {!! Form::select('program_id', $programs->pluck("program_name","id"), $program ? $program->id : null,
                                            ['placeholder' => 'Choose Program Name...', 'class' => 'form-control select2', 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('college_id', "College Name *") !!}
                                        {!! Form::select('college_id', $polytechnics->pluck("short_name","id"), $manageprogram ? $manageprogram->id : null,
                                            ['placeholder' => 'Choose College Name...', 'class' => 'form-control select2', 'required' => true]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-md btn-primary pull-left">
                                        {{ $manageprogram ? "Update Assign" : "Assign program" }}
                                    </button>
                                    <a href="{{ route('rp.manageprograms.index') }}"
                                       class="btn btn-md btn-warning pull-right">Cancel</a>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
            @endif

            <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>College Name</th>
                                <th>College Programs</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($polytechnics as $polytechnic)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ ucwords($polytechnic->short_name) }}</td>
                                    <td>
                                        <table>
                                            @foreach(@$polytechnic->programs as $program)
                                                <tr>
                                                    <td class="col-md-6">{{$program->program_name or ""}}
                                                            <span class="pull-right">
                                                        @if (rpAllowed(3))
                                                                <a href="{{ route('rp.manageprograms.edit', ['id' => $polytechnic->id,'program_id'=> $program->id]) }}"
                                                           class="btn btn-primary btn-md" style="margin-bottom: 2px;">Edit</a>
                                                                @endif
                                                                @if (rpAllowed(5))
                                                                    <form action="{{ route('rp.manageprograms.destroy', ['id' => $polytechnic->id,'program_id'=> $program->id]) }}"
                                                                          method="post">
                                                            @method("DELETE")
                                                                        {{ csrf_field() }}
                                                                        <button type="submit"
                                                                                class="btn btn-danger btn-md">Delete</button>
                                                        </form>
                                                                @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($manageprogram)
        {!! JsValidator::formRequest('App\Http\Requests\UpdatePManageRequest', '#updatePManage'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreatePManageRequest', '#createPManage'); !!}
    @endif
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@show