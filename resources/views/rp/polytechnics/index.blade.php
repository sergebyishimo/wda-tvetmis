@extends('rp.layout.main')

@section('htmlheader_title')
    Polytechnics
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Polytechnics
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if (rpAllowed(3) || rpAllowed(4))

                    {!! Form::open(['route' => 'rp.polytechnics.store', 'class' => 'form']) !!}
                    @if(!is_null($editting))
                        {!! Form::hidden('id', $editting) !!}
                    @endif
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('polytechnic', 'Name', ['class' => 'label-control']) !!}
                                        {!! Form::text('polytechnic', getCollege($editting, 'polytechnic', null, false),['class' => 'form-control']) !!}
                                        @if ($errors->has('polytechnic'))
                                            <span class="help-block text-red">
                                            {{ $errors->first('polytechnic') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('short_name', 'Acronym', ['class' => 'label-control']) !!}
                                        {!! Form::text('short_name', getCollege($editting, 'short_name', null, false),['class' => 'form-control']) !!}
                                        @if ($errors->has('short_name'))
                                            <span class="help-block text-red">
                                            {{ $errors->first('short_name') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            {!! Form::submit('Save', ['class' => 'btn btn-md btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                @endif

            <!-- List box -->
                    @if (rpAllowed(2))
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">List</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-striped table-bordered" id="dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>College Names</th>
                                        <th>College Acronym</th>
                                        @if (rpAllowed(3))
                                            <th></th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($i = 1 )
                                    @foreach($polytechnics as $polytechnic)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ ucwords($polytechnic->polytechnic) }}</td>
                                            <td>{{ ucwords($polytechnic->short_name) }}</td>
                                            @if (rpAllowed(3))
                                                <td>
                                                    <a href="{{ route('rp.polytechnics.edit', $polytechnic->id) }}"
                                                       class="btn btn-primary btn-sm">Edit</a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                @endif
                    <!-- /.box-body -->
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show