@if(auth()->guard('rp')->check())
    <li class="{{ activeMenu('rp.home') }}">
        <a href="{{ route('rp.home') }}"><i class='fa fa-link'></i>&nbsp;<span>Dashboard</span></a>
    </li>
    @if (rpAllowed())
        <li class="{{ activeMenu(['rp.polytechnics.index', 'rp.polytechnics.edit']) }}">
            <a href="{{ route('rp.polytechnics.index') }}"><i class='fa fa-certificate'></i>&nbsp;Colleges</a>
        </li>

        <li><a href="{{ route('rp.departments.index') }}"><i class='fa fa-building'></i>&nbsp;Departments</a></li>

        <li class="treeview {{ activeMenu(['rp.programs.index', 'rp.programs.create', 'rp.programs.edit',
        'rp.manageprograms.index',
        'rp.courseunits.index',
        'rp.courseunits.create', 'rp.courseunits.edit']) }}">
            <a href="#">
                <i class='fa fa-book'></i>
                <span>Programs</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                <span class="label label-primary pull-right">{{ App\Model\Rp\CollegeProgram::count() }}</span>
            </span>

            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['rp.programs.index', 'rp.programs.create', 'rp.programs.edit',
            'rp.courseunits.index', 'rp.courseunits.create', 'rp.courseunits.edit']) }}">
                    <a href="{{ route('rp.programs.index') }}"><i class='fa fa-link'></i>&nbsp;View Programs</a>
                </li>
                <li class="{{ activeMenu(['rp.manageprograms.index']) }}"><a
                            href="{{ route('rp.manageprograms.index') }}"><i class='fa fa-bars'></i>&nbsp;Manage
                        Programs</a></li>
            </ul>
        </li>

        <li class="treeview {{ activeMenu([
        'rp.students', 'rp.students',
        'rp.student.single', 'rp.student.single', 'rp.query.execute'
        ]) }}">
            <a href="#">
                <i class='fa fa-users'></i>
                <span>Make Queries</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                {{--<li class="{{ activeMenu(['rp.students', 'rp.students']) }}">--}}
                    {{--<a href="{{ route('rp.students') }}">--}}
                        {{--<i class='fa fa-users'></i>&nbsp;--}}
                        {{--<span>Students List</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li class="{{ activeMenu(['rp.student.single', 'rp.student.single', 'rp.query.execute']) }}">
                    <a href="{{ route('rp.student.single') }}">
                        <i class='fa fa-users'></i>&nbsp;
                        <span>On Single Student</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview {{ activeMenu([
            'rp.privates.index', 'rp.privates.edit', 'rp.privates.show', 'rp.admission.privates.finished'
        ]) }}">
            <a href="#">
                <i class='fa fa-bookmark'></i>
                <span>Admission</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="treeview {{ activeMenu([
                'rp.privates.index', 'rp.privates.edit', 'rp.privates.show',
                'rp.admission.privates.finished',
            ]) }}">
                    <a href="{{ route('rp.colleges') }}">
                        <i class='fa fa-xing'></i>&nbsp;
                        Applicants
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ activeMenu() }}">
                            <a href="#"><i class='fa fa-link'></i>&nbsp;<span>Government</span></a>
                        </li>
                        <li class="{{ activeMenu(['rp.privates.index', 'rp.admission.privates.finished']) }}">
                            <a href="{{ route('rp.privates.index') }}"><i
                                        class='fa fa-link'></i>&nbsp;<span>Private</span></a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class='fa fa-money'></i>
                        <span>Payments</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('rp.colleges') }}"><i class='fa fa-link'></i>&nbsp;History</a></li>
                        <li><a href="{{ route('rp.colleges') }}"><i class='fa fa-link'></i>&nbsp;Paid Students</a></li>
                        <li><a href="{{ route('rp.colleges') }}"><i class='fa fa-link'></i>&nbsp;Not Paid Students</a>
                        </li>

                    </ul>
                </li>
                <li><a href="{{ route('rp.colleges') }}"><i class='fa fa-link'></i>&nbsp; System Activation</a></li>
                <li><a href="#"></a></li>
            </ul>
        </li>
        <li class="treeview {{ activeMenu([
            'rp.colleges', 'rp.colleges.view.upload', 'rp.student.transfer',
            'rp.college.account', 'rp.college.show.student',
            'rp.registration.index', 'rp.reg.not-finished', 'rp.reg.finished', 'rp.reg.new.student',
            'rp.students.enrol',
            'rp.social.edit', 'rp.social.view', 'rp.social.upload',
            'rp.social.index', 'rp.social.create', 'rp.social.show'
        ]) }}">
            <a href="#">
                <i class='fa fa-diamond'></i>
                <span>Registration</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['rp.colleges', 'rp.college.account', 'rp.college.show.student']) }}">
                    <a href="{{ route('rp.colleges') }}"><i class='fa fa-link'></i>&nbsp;View Colleges</a>
                </li>
                <li class="{{ activeMenu(['rp.registration.index']) }}">
                    <a href="{{ route('rp.registration.index') }}"><i class='fa fa-link'></i>&nbsp;Registered
                        Students</a></li>
                <li class="{{ activeMenu(['rp.reg.not-finished']) }}" disabled="">
                    <a href="{{ route('rp.reg.not-finished') }}"><i class='fa fa-link'></i>&nbsp;Unregistered
                        Students</a>
                </li>
                <li class="{{ activeMenu(['rp.social.index', 'rp.social.edit', 'rp.social.view', 'rp.social.create']) }}">
                    <a href="{{ route('rp.social.index') }}">
                        <i class='fa fa-link'></i>&nbsp;Social Students
                    </a>
                </li>
                <li class="{{ activeMenu(['rp.students.enrol']) }}" disabled="">
                    <a href="{{ route('rp.students.enrol') }}"><i class='fa fa-link'></i>&nbsp;Enrolled Students</a>
                </li>
                <li class="{{ activeMenu(['rp.exceptional.students']) }}" disabled="">
                    <a href="{{ route('rp.exceptional.students') }}">
                        <i class='fa fa-link'></i>&nbsp;Exceptional Students</a>
                </li>
                {{--<li class="{{ activeMenu(['rp.reg.finished']) }}">--}}
                    {{--<a href="{{ route('rp.reg.finished') }}"><i class='fa fa-link'></i>&nbsp;Finished Student</a></li>--}}

                <li class="{{ activeMenu(['rp.colleges.view.upload']) }}">
                    <a href="{{ route('rp.colleges.view.upload') }}"><i class='fa fa-link'></i>&nbsp;Upload Students</a>
                </li>
                <li class="{{ activeMenu(['rp.student.transfer']) }}">
                    <a href="{{ route('rp.student.transfer') }}"><i class='fa fa-link'></i>&nbsp;Transfer Students</a>
                </li>

                <li class="{{ activeMenu(['rp.emails']) }}">
                    <a href="{{ route('rp.emails') }}"><i class='fa fa-link'></i>&nbsp;Send Emails</a>
                </li>
                {{--<li class="{{ activeMenu(['rp.letter']) }}">--}}
                {{--<a href="{{ route('rp.letter') }}"><i class='fa fa-book'></i>&nbsp;Admission Letter</a>--}}
                {{--</li>--}}
            </ul>
        </li>

        <li class="treeview {{ activeMenu([
            'rp.payments.category', 'rp.payments.index', 'rp.reg.paid',
            'rp.payments.history', 'rp.payments.get.invoice.detail', 'rp.payments.get.invoices',
            'rp.payments.invoices', 'rp.payments.history', 'rp.payments.filtering', 'rp.payments.post.filtering',
            'rp.payments.outside', 'rp.payments.manually'
        ]) }}">
            <a href="#">
                <i class='fa fa-money'></i>
                <span>Payments</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['rp.payments.invoices', 'rp.payments.get.invoice.detail', 'rp.payments.get.invoices']) }}">
                    <a href="{{ route('rp.payments.invoices') }}">
                        <i class='fa fa-link'></i>&nbsp;Invoices
                    </a>
                </li>
                <li class="{{ activeMenu(['rp.payments.history']) }}">
                    <a href="{{ route('rp.payments.history') }}">
                        <i class='fa fa-link'></i>&nbsp;Payments History
                    </a>
                </li>
                <li class="{{ activeMenu(['rp.payments.filtering', 'rp.payments.post.filtering']) }}">
                    <a href="{{ route('rp.payments.filtering') }}">
                        <i class='fa fa-link'></i>&nbsp;Payments Filtering
                    </a>
                </li>
                <li class="{{ activeMenu(['rp.payments.outside']) }}">
                    <a href="{{ route('rp.payments.outside') }}">
                        <i class='fa fa-link'></i>&nbsp;Payments Made Outside
                    </a>
                </li>
                <li class="{{ activeMenu(['rp.payments.manually']) }}">
                    <a href="{{ route('rp.payments.manually') }}">
                        <i class='fa fa-weibo'></i>&nbsp;Manually Payments
                    </a>
                </li>
            </ul>
        </li>
        @if (rpAllowed(7))
        <li class="treeview {{ activeMenu(['rp.functionalfees.index', 'rp.functionalfeescategory.index', 'rp.managefuncfeecategory.index']) }}">
            <a href="#">
                <i class='fa fa-book'></i>
                <span>Functional Fees</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['rp.functionalfees.index']) }}">
                    <a href="{{ route('rp.functionalfees.index') }}"><i class='fa fa-link'></i>&nbsp;Functional Fees</a>
                </li>
                <li class="{{ activeMenu(['rp.functionalfeescategory.index']) }}">
                    <a href="{{ route('rp.functionalfeescategory.index') }}"><i class='fa fa-link'></i>Fees
                        Categories</a>
                </li>
                <li class="{{ activeMenu(['rp.managefuncfeecategory.index']) }}">
                    <a href="{{ route('rp.managefuncfeecategory.index') }}"><i class='fa fa-link'></i>Manage fees</a>
                </li>
                <li class="{{ activeMenu(['rp.reg.paid']) }}">
                    <a href="{{ route('rp.reg.paid') }}"><i class='fa fa-link'></i>Paid Students</a>
                </li>
            </ul>
        </li>

        <li class="treeview {{ activeMenu([
            'rp.users.index', 'rp.users.edit', 'rp.users.show', 'rp.users.create',
            'rp.rps.index', 'rp.rps.edit', 'rp.rps.create', 'rp.rps.show'
            ]) }}">
            <a href="#">
                <i class='fa fa-users'></i>
                <span>User Managements</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['rp.rps.index', 'rp.rps.edit', 'rp.rps.create', 'rp.rps.show']) }}">
                    <a href="{{ route('rp.rps.index') }}"><i class="fa fa-stumbleupon"></i><span>RP Users</span></a>
                </li>
                <li class="{{ activeMenu(['rp.users.index', 'rp.users.edit', 'rp.users.show', 'rp.users.create']) }}">
                    <a href="{{ route('rp.users.index') }}"><i class='fa fa-link'></i>&nbsp;College Users</a>
                </li>
            </ul>
        </li>

            <li class="treeview {{ activeMenu([
        'rp.settings.set.time', 'rp.settings.set.enrol', 'rp.settings.set.semester'
        ]) }}">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>Settings</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ activeMenu(['rp.settings.set.time']) }}">
                        <a href="{{ route('rp.settings.set.time') }}"><i class='fa fa-link'></i>&nbsp;System Activation</a>
                    </li>
                    <li class="{{ activeMenu(['rp.settings.set.enrol']) }}">
                        <a href="{{ route('rp.settings.set.enrol') }}"><i class='fa fa-link'></i>&nbsp;Enrolment period</a>
                    </li>
                    <li class="{{ activeMenu(['rp.settings.set.semester']) }}">
                        <a href="{{ route('rp.settings.set.semester') }}"><i class='fa fa-link'></i>&nbsp;Semester
                            Period</a>
                    </li>
                </ul>
            </li>
        @endif

        <li class="treeview {{ activeMenu([
        'notifications.index', 'notifications.edit', 'notifications.show'
        ]) }}">
            <a href="#">
                <i class="fa fa-star"></i>
                <span>Others</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ activeMenu(['notifications.index', 'notifications.edit', 'notifications.show', 'rp.instructions.index']) }}">
                    <a href="{{ route('notifications.index') }}"><i
                                class="fa fa-bell"></i><span>Students Notification</span></a>
                </li>
                <li class="{{ activeMenu(['rp.instructions.index']) }}">
                    <a href="{{ route('rp.instructions.index') }}"><i
                                class="fa fa-bell"></i><span>Instructions</span></a>
                </li>
            </ul>
        </li>
    @endif
@endif