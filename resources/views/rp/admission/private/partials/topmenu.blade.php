<div class="row mb-3">
    <div class="col-md-12">
        <div class="btn-group">
            @if(auth()->guard('rp')->check())
                <a href="{{ route('college.admission.privates.rejected') }}"
                   class="btn btn-danger {{ activeMenu(['college.admission.privates.rejected']) }}">Rejected</a>
                <a href="{{ route('rp.admission.privates.finished') }}"
                   class="btn btn-success {{ activeMenu(['rp.admission.privates.finished']) }}">Finished</a>
                <a href="{{ route('rp.privates.index') }}"
                   class="btn btn-warning {{ activeMenu(['rp.privates.index']) }}">Signed up</a>
            @elseif(auth()->guard('college')->check())
                <a href="{{ route('college.admission.privates.rejected') }}"
                   class="btn btn-danger {{ activeMenu(['college.admission.privates.rejected']) }}">Rejected</a>
                <a href="{{ route('college.admission.privates.finished') }}"
                   class="btn btn-success {{ activeMenu(['college.admission.privates.finished']) }}">Finished</a>
                <a href="{{ route('college.privates.index') }}"
                   class="btn btn-warning {{ activeMenu(['college.privates.index']) }}">Signed up</a>
            @endif
            {{--<a href="" class="btn btn-primary">Payments Records</a>--}}
            {{--<a href="" class="btn btn-dark">Not Yet Paid</a>--}}
        </div>
    </div>
</div>