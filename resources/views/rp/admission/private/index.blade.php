@extends('rp.layout.main')

@section('htmlheader_title')
    Private Admission
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Private Admission
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        @include('rp.admission.private.partials.topmenu')
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Signed Up Students</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Students Name</th>
                                <th>Registration Number</th>
                                <th>College</th>
                                <th>Department</th>
                                <th>Program</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @if($students)
                                @foreach($students as $student)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $student->names }}</td>
                                        <td>{{ $student->std_id }}</td>
                                        <td>{{ $student->college->college->polytechnic or "" }}</td>
                                        <td>{{ $student->department->department_name or "" }}</td>
                                        <td>{{ $student->course->program_name or "" }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show