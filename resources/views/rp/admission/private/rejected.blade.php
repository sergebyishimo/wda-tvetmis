@extends('rp.layout.main')

@section('htmlheader_title')
    Rejected Students
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Rejected Students
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        @include('rp.admission.private.partials.topmenu')
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Rejected Students</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Students Name</th>
                                <th>Registration Number</th>
                                @if(!auth()->guard('college')->check())
                                    <th>College</th>
                                @endif
                                <th>Department</th>
                                <th>Program</th>
                                <th>Study Mode</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @if($students)
                                @foreach($students as $student)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $student->names }}</td>
                                        <td>{{ $student->std_id }}</td>
                                        @if(!auth()->guard('college')->check())
                                            <td>{{ $student->college->college->polytechnic or "" }}</td>
                                        @endif
                                        <td>{{ $student->department->department_name or "" }}</td>
                                        <td>{{ $student->course->program_name or "" }}</td>
                                        <td>{{ $student->student_category }}</td>
                                        <th>
                                            <a href="{{ route('college.registration.show', strtolower($student->std_id)) }}"
                                               class="btn btn-primary btn-sm pull-right">See More</a>
                                        </th>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show