@extends('rp.layout.main')

@section('htmlheader_title', "Invoice Code:".$code )

@section('contentheader_title')
    <div class="container-fluid">
        Invoice Code: <b>{{ $code }}</b>
    </div>
@endsection

@section('l-style')
    @parent
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <table class="table table-condensed table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Student:</td>
                                        <td><b>{{ getStudent($invoice->student_id, "names") }}</b></td>
                                    </tr>
                                    <tr>
                                        <td>College:</td>
                                        <td>
                                            <b>{{ getCollege(getStudent($invoice->student_id, "college_id"), "polytechnic") }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Department:</td>
                                        <td>
                                            <b>{{ getDepartment(getStudent($invoice->student_id, "department_id"), "department_name") }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Program:</td>
                                        <td>
                                            <b>{{ getCourse(getStudent($invoice->student_id, "course_id"), "program_name") }}</b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Items Associated</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Function Fees</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($fees as $fee)
                                        <tr>
                                            <td>{{ ucwords($fee->fee_name) }}</td>
                                            <td>{{ number_format($fee->amount) }} Rwf</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>
                                                Oops ..., no data associated !!
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <table class="table table-condensed table-bordered">
                                    <tbody>
                                    <tr class="bg-success">
                                        <td>Invoice Category:</td>
                                        <td><b>{{ ucwords($invoice->name) }}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Total Amount:</td>
                                        <td><b>{{ number_format($invoice->amount) }} RWF</b></td>
                                    </tr>
                                    <tr>
                                        <td>Paid Amount:</td>
                                        <td><b>{{ number_format($invoice->paid) }} RWF</b></td>
                                    </tr>
                                    <tr>
                                        <td>Remain:</td>
                                        <td class=""><b>{{ number_format(($invoice->amount - $invoice->paid)) }} RWF</b></td>
                                    </tr>
                                    @php
                                        $sponsor = sponsored($invoice->student_id, $invoice->name, true, true);
                                    @endphp
                                    @if($sponsor)
                                        <tr>
                                            <td>Sponsor</td>
                                            <td>
                                                <b>{{ strtoupper($sponsor->sponsor) }}</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="table table-condensed table-bordered table-striped">
                                                    <thead class="bg-gray-light">
                                                    <tr>
                                                        <th class="text-center">Expected Amount</th>
                                                        <th class="text-center">Percentage Covered</th>
                                                        <th class="text-center">Sponsor Paid</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="bg-info">
                                                        <td class="text-center">
                                                            <b>
                                                                @php
                                                                    $expected = $fees->sum('amount');
                                                                    echo number_format($expected). " FRW";
                                                                @endphp
                                                            </b>
                                                        </td>
                                                        <td class="text-center"><b>{{ $sponsor->percentage }}%</b></td>
                                                        <td class="text-center">
                                                            <b>{{ number_format(($expected - $invoice->amount)) }}
                                                                RWF</b>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Payments Associated</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Operated Date</th>
                                        <th>Amount Paid</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $x = 1;
                                    @endphp
                                    @forelse($payments as $payment)
                                        <tr>
                                            <td class="text-center">{{ $x++ }}</td>
                                            <td>{{ ucwords($payment->op_date) }}</td>
                                            <td>{{ number_format($payment->paid_amount) }} Rwf</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3" class="text-center">
                                                Oops ..., no payment made yet !!
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
@endsection