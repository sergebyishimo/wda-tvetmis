@extends('rp.layout.main')

@section('htmlheader_title', "All Active Invoices")

@section('contentheader_title')
    <div class="container-fluid">
        All Active Invoices
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3>
                    </div>
                    <div class="box-body" style="overflow-x: auto;">
                        <table class="table table-striped table-bordered" id="dataTableBtnList">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Student</th>
                                <th>Title</th>
                                <th>Total</th>
                                <th>Paid Amount</th>
                                <th>Installment</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Details</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>


    <script>
        $(document).ready(function () {

            $('#dataTableBtnList').DataTable({
                serverSide: true,
                processing: true,
                searching: true,
                pager: true,
                responsive: true,
                ajax: '{{ route("rp.payments.get.invoices") }}',
                lengthMenu: [[25, 100, 1000, -1], [25, 100, 1000, "All"]],
                columns: [
                    {data: 'code', name: 'code', orderable: false, searchable: false},
                    {data: 'student_id', name: 'student_id'},
                    {data: 'name', name: 'name'},
                    {data: 'amount', name: 'amount', orderable: false, searchable: false},
                    {data: 'paid', name: 'paid', orderable: false, searchable: false},
                    {data: 'partial', name: 'partial', orderable: false, searchable: false},
                    {
                        data: 'created_at',
                        name: 'pending_payment_invoices.created_at',
                        orderable: true,
                        searchable: false
                    },
                    {
                        data: 'updated_at',
                        name: 'pending_payment_invoices.updated_at',
                        orderable: true,
                        searchable: false
                    },
                    {data: 'details', name: 'details', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                order: [[1, 'asc']],
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            // jQuery selector to add a border
                            $('row c[r*="10"]', sheet).attr('s', '25');
                        },
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        postfixButtons: ['colvisRestore'],
                        collectionLayout: 'fixed two-column'
                    },


                    // 'columnsToggle'
                ],
                // initComplete: function () {
                //     this.api().columns().every(function () {
                //         var column = this;
                //         var select = $('<select class="select2"><option value=""></option></select>')
                //             .appendTo($(column.footer()).empty())
                //             .on('change', function () {
                //                 var val = $.fn.dataTable.util.escapeRegex(
                //                     $(this).val()
                //                 );
                //
                //                 column
                //                     .search(val ? '^' + val + '$' : '', true, false)
                //                     .draw();
                //             });
                //
                //         column.data().unique().sort().each(function (d, j) {
                //             select.append('<option value="' + d + '">' + d + '</option>')
                //         });
                //     });
                // }
            });

        });
    </script>
@endsection