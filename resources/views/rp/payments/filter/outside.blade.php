@extends('rp.layout.main')

@section('htmlheader_title', "Results")

@section('contentheader_title')
    <div class="container-fluid">
        Results
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ auth()->guard("college")->check() ? route('college.payments.filtering') : route('rp.payments.filtering') }}"
                           class="btn btn-warning">
                            <i class="fa fa-search"></i>
                            <span>Back</span>
                        </a>
                    </div>
                    <div class="box-body" style="overflow-x: scroll;">
                        <table class="table table-striped table-bordered" id="dataTableBtn">
                            <thead>
                            <tr>
                                <th width="50px" class="text-center">#</th>
                                @if(auth()->guard('rp')->check())
                                    <th>College</th>
                                @endif
                                <th>Student Reg</th>
                                <th>Student Names</th>
                                <th>Payment Categories</th>
                                <th>Amount Paid</th>
                                <th>Type</th>
                                <th>Bank</th>
                                <th>Date Paid</th>
                                <th>Academic Year</th>
                                <th>Attachment</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($payments)
                                @php
                                    $x = 1;
                                @endphp
                                @if($category)
                                    @foreach($payments as $payment)
                                        @php
                                            $sCategory = $payment->category? $payment->category->category_name : "";
                                        @endphp
                                        @if(strtolower($category) == strtolower($sCategory))
                                            <tr>
                                                <td class="text-center">{{ $x++ }}</td>
                                                @if(auth()->guard('rp')->check())
                                                    <td>{{ getStudentInfo($payment->student_id, 'college_id') }}</td>
                                                @endif
                                                <td>{{ $payment->student_id }}</td>
                                                <td>{{ getStudentInfo($payment->student_id, 'names') }}</td>
                                                <td>{{ $sCategory }}</td>
                                                <td>{{ $payment->amount }}</td>
                                                <td class="text-center">{{ $payment->payment_type  ? getOutSidePaymentsCategories($payment->payment_type) : '-' }}</td>
                                                <td>{{ strtoupper($payment->bank) }}</td>
                                                <td>{{ $payment->date_paid }}</td>
                                                <td>{{ $payment->academic_year }}</td>
                                                <td>
                                                    <a href="{{ asset('storage/'.$payment->attachment) }}"
                                                       target="_blank"
                                                       class="btn btn-link btn-warning">VIEW ATTACHMENT</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    @foreach($payments as $payment)
                                        @php
                                            $sCategory = $payment->category? $payment->category->category_name : "";
                                        @endphp
                                        <tr>
                                            <td class="text-center">{{ $x++ }}</td>
                                            @if(auth()->guard('rp')->check())
                                                <td>{{ getStudentInfo($payment->student_id, 'college_id') }}</td>
                                            @endif
                                            <td>{{ $payment->student_id }}</td>
                                            <td>{{ getStudentInfo($payment->student_id, 'names') }}</td>
                                            <td>{{ $sCategory }}</td>
                                            <td>{{ $payment->amount }}</td>
                                            <td class="text-center">{{ $payment->payment_type  ? getOutSidePaymentsCategories($payment->payment_type) : '-' }}</td>
                                            <td>{{ strtoupper($payment->bank) }}</td>
                                            <td>{{ $payment->date_paid }}</td>
                                            <td>{{ $payment->academic_year }}</td>
                                            <td>
                                                <a href="{{ asset('storage/'.$payment->attachment) }}"
                                                   target="_blank"
                                                   class="btn btn-link btn-warning">VIEW ATTACHMENT</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#dataTableBtn').DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            // jQuery selector to add a border
                            $('row c[r*="10"]', sheet).attr('s', '25');
                        },
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        postfixButtons: ['colvisRestore'],
                        collectionLayout: 'fixed two-column'
                    },


                    // 'columnsToggle'
                ]
            });

        });
    </script>
@endsection