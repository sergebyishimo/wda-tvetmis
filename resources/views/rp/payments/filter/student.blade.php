@extends('rp.layout.main')

@section('htmlheader_title', "Student Payment History")

@section('contentheader_title')
    <div class="container-fluid">
        Student Payment History
    </div>
@endsection

@section('l-style')
    @parent
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box box-primary">
            <div class="box-header with-border">
                <button type="button" class="btn btn-sm btn-primary pull-right">
                    <i class="fa fa-print"></i>
                    <span>Print</span>
                </button>
            </div>
            <div class="box-body">
                @php
                    $x = 1;
                    $tmpCode = '';
                @endphp
                @forelse($gPayments as $payment)
                    @php
                        $invoices = getInvoicesItems($payment->invoice_code);
                        $total = getInvoice($payment->invoice_code, "amount");
                        $paid = getInvoice($payment->invoice_code, "paid");
                        $remain = $total -$paid;
                        $perc = ceil((100 * $paid) / $total);
                        $code = $payment->invoice_code;
                    @endphp
                    @if($x <= 1)
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-condensed table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="100px" class="bg-gray">Student:</td>
                                        <td><b>{{ getStudent($payment->std_id, "names") }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="100px" class="bg-gray">College:</td>
                                        <td>
                                            <b>{{ getCollege(getStudent($payment->std_id, "college_id"), "polytechnic") }}</b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed table-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="100px" class="bg-gray">Department:</td>
                                        <td>
                                            <b>{{ getDepartment(getStudent($payment->std_id, "department_id"), "department_name") }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px" class="bg-gray">Program:</td>
                                        <td>
                                            <b>{{ getCourse(getStudent($payment->std_id, "course_id"), "program_name") }}</b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border bg-gray">
                                    <div class="row">
                                        <div class="col-md-6 text-left text-danger">
                                            Invoice Number: <b>{{ $payment->invoice_code }}</b>
                                        </div>
                                        <div class="col-md-6 text-right text-primary">
                                            Fee Category: <b>{{ $payment->name }}</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    @if($invoices)
                                        <div class="box">
                                            <div class="box-header with-border"
                                                 style="background-color: rgba(0,123,255,.4)">
                                                Items Associated
                                            </div>
                                            <div class="box-body">
                                                <table class="table table-bordered table-condensed">
                                                    <thead class="bg-gray">
                                                    <tr>
                                                        @foreach($invoices as $fee)
                                                            <th style="font-size: 12px">
                                                                {{ ucwords($fee->fee ? $fee->fee->fee_name : "") }}
                                                            </th>
                                                        @endforeach
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        @php
                                                            $expected = 0;
                                                        @endphp
                                                        @foreach($invoices as $fee)
                                                            <td align="">
                                                                {{ strtoupper(number_format($fee->amount)) }}
                                                                @php
                                                                    $expected += $fee->amount;
                                                                @endphp
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-bordered" style="width: 300px;">
                                                            <thead>
                                                            <tr>
                                                                <th>Total To Pay</th>
                                                                <th>Paid</th>
                                                                <th>Remain</th>
                                                                <th class="text-center">P</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>{{ strtoupper(number_format($total)) }}</td>
                                                                <td>{{ strtoupper(number_format($paid)) }}</td>
                                                                <td>{{ strtoupper(number_format($remain)) }}</td>
                                                                <td class="text-center">{{ $perc }} %</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-6">
                                                        @php
                                                            $sponsor = sponsored($payment->student_id, $payment->name , true, true);
                                                        @endphp
                                                        @if($sponsor)
                                                            <table class="table table-condensed table-bordered table-striped">
                                                                <thead class="bg-gray-light">
                                                                <tr>
                                                                    <th class="text-center">Sponsor</th>
                                                                    <th class="text-center">Expected Amount</th>
                                                                    <th class="text-center">Percentage Covered</th>
                                                                    <th class="text-center">Sponsor Paid</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr class="bg-info">
                                                                    <td class="text-center">
                                                                        <b>
                                                                            {{ strtoupper($sponsor->sponsor) }}
                                                                        </b>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <b>
                                                                            {{ number_format($expected) }} RWF
                                                                        </b>
                                                                    </td>
                                                                    <td class="text-center"><b>{{ $sponsor->percentage }}%</b></td>
                                                                    <td class="text-center">
                                                                        <b>
                                                                            {{ number_format(($expected - $total)) }} RWF
                                                                        </b>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="box box-primary ">
                                        <div class="box-header with-header"
                                             style="background-color: rgba(0,123,255,.4)">
                                            Payments Associated
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-bordered table-condensed">
                                                <thead>
                                                <tr>
                                                    <th class="text-center" width="50px">#</th>
                                                    <th>Amount</th>
                                                    <th>Bank</th>
                                                    <th>Account<sup>No</sup></th>
                                                    <th>Operator</th>
                                                    <th>Mode</th>
                                                    <th>Transaction</th>
                                                    <th>Description</th>
                                                    <th>Time recorded</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($payments)
                                                    @php
                                                        $x = 1;
                                                    @endphp
                                                    @foreach($payments as $payment)
                                                        @if($payment->invoice_code == $code)
                                                            <tr>
                                                                <td class="text-center">{{ $x++ }}</td>
                                                                <td>{{ number_format($payment->paid_amount) }} RWF</td>
                                                                <td>{{ $payment->bank }}</td>
                                                                <td>{{ $payment->bank_account }}</td>
                                                                <td>{{ $payment->operator }}</td>
                                                                <td>{{ $payment->pay_mode }}</td>
                                                                <td>{{ $payment->trans_id }}</td>
                                                                <td>{{ $payment->description }}</td>
                                                                <td>{{ $payment->created_at }}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                        $x++;
                        $tmpCode = $payment->invoice_code;
                    @endphp
                @empty
                @endforelse
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
@endsection