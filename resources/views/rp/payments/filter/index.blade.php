@extends('rp.layout.main')

@section('htmlheader_title', "Results")

@section('contentheader_title')
    <div class="container-fluid">
        Results
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ auth()->guard("college")->check() ? route('college.payments.filtering') : route('rp.payments.filtering') }}" class="btn btn-warning">
                            <i class="fa fa-search"></i>
                            <span>Back</span>
                        </a>
                    </div>
                    <div class="box-body" style="overflow-x: scroll;">
                        <table class="table table-striped table-bordered" id="dataTableBtn">
                            <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">#</th>
                                <th>College Name</th>
                                <th>RegNumber</th>
                                <th>Student Name</th>
                                <th>Fee Category</th>
                                <th>Amount</th>
                                <th>Academic Year</th>
                                <th>Bank</th>
                                <th>Account<sup>No</sup></th>
                                <th>Operator</th>
                                <th>Mode</th>
                                <th>Transaction</th>
                                <th>Time recorded</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($payments)
                                @php
                                    $x = 1;
                                @endphp
                                @foreach($payments as $payment)
                                    <tr>
                                        <td class="text-center">{{ $x++ }}</td>
                                        <td>{{ $payment->short_name }}</td>
                                        <td>{{ $payment->std_id }}</td>
                                        <td>{{ getStudent($payment->std_id, "names") }}</td>
                                        <td>{{ $payment->name }}</td>
                                        <td>{{ $payment->paid_amount }}</td>
                                        <td>{{ $payment->academic_year }}</td>
                                        <td>{{ $payment->bank }}</td>
                                        <td>{{ $payment->bank_account }}</td>
                                        <td>{{ $payment->operator }}</td>
                                        <td>{{ $payment->pay_mode }}</td>
                                        <td>{{ $payment->trans_id }}</td>
                                        <td>{{ $payment->created_at }}</td>
                                        <td>{{ $payment->description }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#dataTableBtn').DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            // jQuery selector to add a border
                            $('row c[r*="10"]', sheet).attr('s', '25');
                        },
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        postfixButtons: ['colvisRestore'],
                        collectionLayout: 'fixed two-column'
                    },


                    // 'columnsToggle'
                ]
            });

        });
    </script>
@endsection