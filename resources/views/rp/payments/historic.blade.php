@extends('rp.layout.main')

@section('htmlheader_title', "Payments Historic")

@section('contentheader_title')
    <div class="container-fluid">
        Payments Historic In {{ getCurrentAcademicYear() }}
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3>
                    </div>
                    <div class="box-body" style="overflow-x: scroll;">
                        <table class="table table-striped table-bordered" id="dataTableBtnList">
                            <thead>
                            <tr>
                                <th>College Name</th>
                                <th>RegNumber</th>
                                <th>Fee Category</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Account<sup>No</sup></th>
                                <th>Operator</th>
                                <th>Mode</th>
                                <th>Transaction</th>
                                <th>Time recorded</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="select-filter">College Name</th>
                                <th>RegNumber</th>
                                <th class="select-filter">Fee Category</th>
                                <th>Amount</th>
                                <th class="select-filter">Bank</th>
                                <th class="select-filter">Account<sup>No</sup></th>
                                <th class="select-filter">Operator</th>
                                <th class="select-filter">Mode</th>
                                <th>Transaction</th>
                                <th>Time recorded</th>
                                <th>Description</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    {{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>

    <script>
        $(document).ready(function () {
            let loading = "{{ asset('img/ajaxload.gif') }}";
            $('#dataTableBtnList').DataTable({
                serverSide: true,
                processing: true,
                searching: true,
                pager: true,
                ajax: '{{ auth()->guard("college")->check() ? route("college.payments.get.historic") : route("rp.payments.get.historic") }}',
                lengthMenu: [[25, 100, 1000, -1], [25, 100, 1000, "All"]],
                columns: [
                    {data: 'short_name', name: 'adm_polytechnics.short_name', orderable: true, searchable: true},
                    {data: 'std_id', name: 'rp_college_payments.std_id', orderable: false, searchable: true},
                    {data: 'name', name: 'rp_pending_payment_invoices.name'},
                    {data: 'paid_amount', name: 'rp_college_payments.paid_amount', orderable: false, searchable: false},
                    {data: 'bank', name: 'rp_college_payments.bank', orderable: true, searchable: true},
                    {
                        data: 'bank_account',
                        name: 'rp_college_payments.bank_account',
                        orderable: false,
                        searchable: true
                    },
                    {data: 'operator', name: 'rp_college_payments.operator', orderable: true, searchable: true},
                    {data: 'pay_mode', name: 'rp_college_payments.pay_mode', orderable: false, searchable: false},
                    {data: 'trans_id', name: 'rp_college_payments.trans_id', orderable: false, searchable: false},
                    {data: 'created_at', name: 'rp_college_payments.created_at', orderable: true, searchable: false},
                    {data: 'description', name: 'rp_college_payments.description', orderable: true, searchable: true},
                ],
                order: [[0, 'asc']],
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: 'excelHtml5',
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            // jQuery selector to add a border
                            $('row c[r*="10"]', sheet).attr('s', '25');
                        },
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        postfixButtons: ['colvisRestore'],
                        collectionLayout: 'fixed two-column'
                    },


                    // 'columnsToggle'
                ],
                language: {
                    processing: '<span style="width:100%;"><img src="' + loading + '"></span>'
                },
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select class="select2"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });

        });
    </script>
@endsection