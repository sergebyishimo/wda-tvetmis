@extends('rp.layout.main')

@section('htmlheader_title', "View Payments")

@section('contentheader_title')
    <div class="container-fluid">
        View Payments
    </div>
@endsection

@section('l-style')
    @parent
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        {!! Form::open() !!}
        <div class="box box-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('academic_year', "Academic Year") !!}
                        {!! Form::select('academic_year', academicYear(), null, ['class' => 'form-control select2', 'placeholder' => 'select here ..']) !!}
                    </div>
                </div>
                @if(!is_integer($college))
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('college', "College") !!}
                            {!! Form::select('college', $college, null, ['class' => 'form-control select2', 'placeholder' => 'select here ..']) !!}
                        </div>
                    </div>
                @else
                    {!! Form::hidden('college', $college) !!}
                @endif
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('category', "Categories") !!}
                        {!! Form::select('category', $categories, null, ['class' => 'form-control select2', 'placeholder' => 'select here ..']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('student', "Student ID") !!}
                        {!! Form::text('student',null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('start', "Range Start") !!}
                        {!! Form::date('start',null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('end', "Range End") !!}
                        {!! Form::date('end',null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    {!! Form::label('payment_type', "Outside Payment Type") !!}
                    {!! Form::select('payment_type', array_merge(['000' => 'All'], getOutSidePaymentsCategories()), null, ['class' => 'form-control select2', 'placeholder' => 'Select type ...']) !!}
                </div>
                <div class="col-md-2">
                    <label for="bank">Bank</label>
                    <select name="bank" id="bank" class="form-control select2">
                        <option value="" selected disabled>Select Bank</option>
                        @foreach(\App\Rp\Bank::all() as $item )
                            <option value="{{ strtolower($item->bank_name) }}"
                                    {{ old('bank') == strtolower($item->bank_name) ? 'selected' : '' }}>
                                {{ $item->bank_name  }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    {!! Form::label('manually_payment', "Only Payments Uploaded Manually") !!}
                    {!! Form::checkbox('manually_payment', 'y', false) !!}
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <div class="clear-fix">&nbsp;</div>
                        {!! Form::submit('Search', ['class' => 'btn btn-primary btn-block']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('l-scripts')
    @parent
@endsection