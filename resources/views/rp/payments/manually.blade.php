@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Manual Payments
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Manual Payments
    </div>
@endsection

@section('l-style')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        @include('feedback.feedback')
        <div class="box">
            @if (rpAllowed(4))
                {{--@if(auth()->guard('college')->check())--}}
                {{--{!! Form::open(['route' => 'college.payments.outside', 'files' => true]) !!}--}}
                {{--@else--}}
                {!! Form::open(['route' => 'rp.payments.manually.post', 'files' => true]) !!}
                {{--@endif--}}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            {!! Form::label('student_id', "Associated Students *") !!}
                            {!! Form::select('student_id', [], null, ['class' => 'form-control select2']) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('payment_category', "Payment Categories *") !!}
                            {!! Form::select('payment_category', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select category ...', 'required' => true]) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('academic_year', "Academic Year *") !!}
                            {!! Form::text('academic_year', date('Y - ').date('Y', strtotime("+1 year")), ['class' => 'form-control', 'readonly' => true]) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('amount', "Amount *") !!}
                            {!! Form::text('amount', null, ['class' => 'form-control', 'readonly' => false]) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('date_paid', "Date Paid *") !!}
                            {!! Form::date('date_paid', null, ['class' => 'form-control', 'readonly' => false]) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('invoice_code', "Invoice Code *") !!}
                            {!! Form::text('invoice_code', null, ['class' => 'form-control', 'readonly' => false]) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('bank_slip_number', "Bank Slip Number *") !!}
                            {!! Form::text('bank_slip_number', null, ['class' => 'form-control', 'readonly' => false]) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('operator', "Operator *") !!}
                            {!! Form::text('operator', null, ['class' => 'form-control', 'readonly' => false]) !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('attachment', "Attachment *") !!}
                            {!! Form::file('attachment', null, ['class' => 'form-control', 'required' => true]) !!}
                        </div>
                        <div class="col-md-1">
                            <div class="clearfix">&nbsp;</div>
                            {!! Form::submit('Save', ['class' => 'btn btn-block btn-primary']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            @endif
        </div>
        <div class="box">
            <div class="box-header with-border">
                List
            </div>
            <div class="box-body" style="overflow-x: auto;">
                <table class="table table-bordered table-striped table-condensed table-responsive" id="dataTable">
                    <thead>
                    <tr>
                        <th width="50px" class="text-center">#</th>
                        @if(auth()->guard('rp')->check())
                            <th>College</th>
                        @endif
                        <th>Student Reg</th>
                        <th>Student Names</th>
                        <th>Payment Categories</th>
                        <th>Invoice Code</th>
                        <th>Amount Paid</th>
                        <th>Bank</th>
                        <th>Date Paid</th>
                        <th>Attachment</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($payments)
                        @php
                            $x = 1;
                        @endphp
                        @foreach($payments as $payment)
                            <tr>
                                <td class="text-center">{{ $x++ }}</td>
                                @if(auth()->guard('rp')->check())
                                    <td>{{ getStudentInfo($payment->std_id, 'college_id') }}</td>
                                @endif
                                <td>{{ $payment->std_id }}</td>
                                <td>{{ getStudentInfo($payment->std_id, 'names') }}</td>
                                <td>{{ $payment->invoice->name or "" }}</td>
                                <td>{{ $payment->invoice->code or '' }}</td>
                                <td>{{ number_format($payment->paid_amount)." FRW" }}</td>
                                <td>{{ strtoupper($payment->bank) }}</td>
                                <td>{{ $payment->op_date }}</td>
                                <td>
                                    <a href="{{ asset('storage/'.$payment->attachment) }}" target="_blank"
                                       class="btn btn-link btn-warning">VIEW ATTACHMENT</a>
                                </td>
                                <td>
                                    @if(!applied($payment->std_id) && !applied($payment->std_id, true) && rpAllowed(5))
                                        {!! Form::open(['route' => ['rp.payments.manually.revert', $payment->id]]) !!}
                                        @method("DELETE")
                                        {!! Form::hidden('delete', $payment->id) !!}
                                        {!! Form::hidden('student_id', $payment->std_id) !!}
                                        {!! Form::hidden('invoice_code', $payment->invoice_code) !!}
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                dom: 'Blfrtip',
                "oSearch": {"bSmart": false},
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                pager: true,
                initComplete: function () {
                    var x = 0;
                    var lenC = this.api().columns().length;
                    this.api().columns().every(function () {
                        if (x == 1 || x == 4 || x == 6 || x == 7) {
                            var column = this;
                            var select = $('<select class="select2"><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        x++;
                    });
                }
            });
            let students = $("#student_id");
            let categories = $("#payment_category");
            $.get("{{ route('rp.ajax.get.short.students') }}", {}, function (data) {
                let html = "<option value=''>Select now ....</option>";
                $.each(data, function (k, v) {
                    html += "<option value='" + k + "'>" + v + "</option>";
                });
                students.html(html);
            });
            $.get("{{ route('rp.ajax.get.payment.category') }}", {}, function (data) {
                let html = "<option value=''>Select now ....</option>";
                $.each(data, function (k, v) {
                    html += "<option value='" + k + "'>" + v + "</option>";
                });
                categories.html(html);
            });
        });
    </script>
@endsection