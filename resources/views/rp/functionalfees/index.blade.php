@extends('rp.layout.main')

@section('htmlheader_title')
    Functional Fees
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Functional Fees
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            @if (rpAllowed(4))
                <div class="col-md-12">
                    <div class="box">
                        @if($functionalfee)
                            @php($route = ['rp.functionalfees.update', $functionalfee->id])
                            @php($id = "updateFee")
                        @else
                            @php($route = 'rp.functionalfees.store')
                            @php($id = "createFee")
                        @endif
                        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                        @if($functionalfee)
                            @method("PATCH")
                            {!! Form::hidden('id', $functionalfee->id) !!}
                        @endif
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('fee_name', "Fee Name *") !!}
                                        {!! Form::text('fee_name', $functionalfee ? $functionalfee->fee_name: "", ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('govt_sponsored_amount', "Govt Sponsored Amount Code *") !!}
                                        {!! Form::text('govt_sponsored_amount', $functionalfee ? $functionalfee->govt_sponsored_amount: "", ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('private_sponsored_amount', "Private Sponsored Amount *") !!}
                                        {!! Form::text('private_sponsored_amount', $functionalfee ? $functionalfee->private_sponsored_amount: "", ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('paid_when', "Paid When? *") !!}
                                        {!! Form::select('paid_when',['each_year'=>'Each Year','first_year'=>'Only In First Year','third_year'=>'Only In Third Year', 'multiple' => 'Multiple Time'],$functionalfee ? $functionalfee->paid_when: null ,
                                            ['placeholder' => 'Choose Paid When?...', 'class' => 'form-control select2']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-md btn-primary pull-left">
                                        {{ $functionalfee ? "Update Fee" : "Save Fee" }}
                                    </button>
                                    <a href="{{ route('rp.functionalfees.index') }}"
                                       class="btn btn-md btn-warning pull-right">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                @endif

                <!-- List box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Functional Fees List</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped table-bordered" id="dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fee Name</th>
                                    <th>Govment Sponsored Amount</th>
                                    <th>Private Sponsored Amount</th>
                                    <th>Paid When</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i = 1 )
                                @foreach($functionalfees as $functionalfee)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ ucwords($functionalfee->fee_name) }}</td>
                                        <td>{{ $functionalfee->govt_sponsored_amount }}</td>
                                        <td>{{ $functionalfee->private_sponsored_amount }}</td>
                                        <td>{{ ucwords(str_replace('_', " ", $functionalfee->paid_when)) }}</td>
                                        <td>
                                            <div class="row">
                                                @if (rpAllowed(3))
                                                    <div class="col-md-6">
                                                        <a href="{{ route('rp.functionalfees.edit', $functionalfee->id) }}"
                                                           class="btn btn-primary btn-sm btn-block"
                                                           style="margin-bottom: 5px;">Edit</a>
                                                    </div>
                                                @endif
                                                @if (rpAllowed(5))
                                                    <div class="col-md-6">
                                                        <form action="{{ route('rp.functionalfees.destroy', $functionalfee->id) }}"
                                                              method="post">
                                                            @method("DELETE")
                                                            {{ csrf_field() }}
                                                            <button type="submit"
                                                                    class="btn btn-danger btn-sm btn-block">Delete
                                                            </button>
                                                        </form>
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>TOTAL FOR FIRST YEAR
                                    STUDENT {{ $functionalfees->whereIn('paid_when',['first_year','each_year'])->pluck('govt_sponsored_amount')->sum() }}</h2>
                                <h2>TOTAL FOR SECOND YEAR
                                    STUDENT {{ $functionalfees->whereIn('paid_when',['each_year'])->pluck('govt_sponsored_amount')->sum() }}</h2>
                                <h2>TOTAL FOR THIRD YEAR
                                    STUDENT {{ $functionalfees->whereIn('paid_when',['each_year','third_year'])->pluck('govt_sponsored_amount')->sum() }}</h2>
                            </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($functionalfee)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateFunctionFeeRequest', '#updateFee'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateFunctionFeeRequest', '#createFee'); !!}
    @endif
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>

    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@show