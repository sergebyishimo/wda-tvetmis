<div class="box">
    <div class="row">
        <div class="col-md-12">
            <h2>
                <center>
                    <span class="text-green">Program Name:</span>
                    @if(count($courseunit))
                        {{$courseunit->program->program_name or " "}}
                    @else
                        {{$program->program_name or " "}}
                    @endif
                </center>
            </h2>
        </div>
    </div>
</div>