@extends('rp.courseunits.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@overwrite

@section('box-title')
    <span>Course Units</span>
@endsection

@section('box-body')
    <div class="box">
        @if($courseunit)
            @php($route = ['rp.courseunits.update', $courseunit->id])
            @php($id = "updateCourseUnit")
        @else
            @php($route = 'rp.courseunits.store')
            @php($id = "createCourseUnit")
        @endif
        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
        @if($courseunit)
            @method("PATCH")
            {!! Form::hidden('id', $courseunit->id) !!}
            {!! Form::hidden('program_id', $courseunit->program_id) !!}
        @else
            {!! Form::hidden('program_id', $program->id) !!}
        @endif
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <h4>Course Units Form</h4>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('course_unit_code', "Course Code *") !!}
                        {!! Form::text('course_unit_code', $courseunit ? $courseunit->course_unit_code : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('course_unit_name', "Course Name *") !!}
                            {!! Form::text('course_unit_name', $courseunit ? $courseunit->course_unit_name : null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('credit_unit', "Credit Unit *") !!}
                        {!! Form::text('credit_unit', $courseunit ? $courseunit->credit_unit : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('level_of_study_id', "Year Of Study") !!}
                            {!! Form::select('level_of_study_id', $level_of_study->pluck('level_of_study','id'), $courseunit ? $courseunit->level_of_study_id : null,
                            ['placeholder' => 'Choose Level Of Study', 'class' => 'form-control select2', 'required' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('semester_id', "Semester *") !!}
                        {!! Form::select('semester_id', $semester->pluck('semester','id'), $courseunit ? $courseunit->semester_id : null,
                          ['placeholder' => 'Choose Semester', 'class' => 'form-control select2', 'required' => true]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('unit_class_id', "Unit Class *") !!}
                        {!! Form::select('unit_class_id', $unitclass->pluck('course_unit_class','id'), $courseunit ? $courseunit->unit_class_id : null,
                          ['placeholder' => 'Choose Unit Class', 'class' => 'form-control select2', 'required' => true]) !!}
                    </div>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-md btn-primary pull-left">
                        {{ $courseunit ? "Update Course Units" : "Save Course Units" }}
                    </button>
                    <a href="#" class="btn btn-md btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('rp.courseunits.layout.modals.warnings')
@overwrite
@section('l-scripts')
    @parent
    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>

    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($courseunit)
        {!! JsValidator::formRequest('App\Http\Requests\CreateCourseUnitRequest', '#updateCourseUnit'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\UpdateCourseUnitRequest', '#createCourseUnit'); !!}
    @endif
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: docx, xlsx,pdf or ppt");

            $("#qualification").on("change", function () {
                var dep = $(this).val();
                if (dep.length > 0) {
                    $.get("{{ route('school.get.course.levels') }}",
                        {
                            id: dep,
                            where: 'id'
                        }, function (data) {
                            var h = '<option value="" selected disabled>Select ...</option>';
                            $.each(JSON.parse(data), function (index, level) {
                                h += '<option value="' + level.id + '">' + level.name + '</option>';
                            });
                            console.log(data);
                            $("#level_id").html(h);
                        });
                }
            });

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@endsection