@extends('rp.courseunits.layout.main')

@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.material.min.css"/>

@overwrite

@section('box-title')
    <span>Couse Units</span>
@endsection

@section('box-body')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped" id="dataTable">
                <thead>
                <th>Program Name</th>
                <th>Course Unit Code</th>
                <th>Course Unit Name</th>
                <th>Credit Unit</th>
                <th>Level Of Study Id</th>
                <th>Semester</th>
                <th>Unit Class</th>
                <th></th>
                </thead>
                <tbody>
                @if($courseunits->count() > 0)
                    @foreach($courseunits as $courseunit)
                        <tr>
                            <td>
                                {{ $courseunit->program->program_name or "" }}
                            </td>
                            <td>
                                {{ $courseunit->course_unit_code }}
                            </td>
                            <td>
                                <h5>
                                    {{ $courseunit->course_unit_name }}
                                </h5>
                            </td>
                            <td>
                                {{ $courseunit->credit_unit }}
                            </td>
                            <td>
                                {{ $courseunit->levelofstudy->level_of_study or "" }}
                            </td>
                            <td>
                                {{ $courseunit->semester->semester  or ""}}
                            </td>
                            <td>
                                {{ $courseunit->unitclass->course_unit_class or ""}}
                            </td>
                            <td>
                            </td>
                            <th>
                                <a href="{{ route('rp.courseunits.edit', $courseunit->id) }}"
                                   class="btn btn-info btn-sm">Edit</a>
                                <button type="button" data-url="{{ route('rp.courseunits.destroy', $courseunit->id) }}"
                                        data-names="{{ $courseunit->id }}" class="btn btn-danger btn-sm btn-delete">
                                    Delete
                                </button>
                            </th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('rp.courseunits.layout.modals.warnings')
@overwrite

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $("#dataTable").dataTable({
                "ordering": false
            });
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{--    {!! JsValidator::formRequest('App\Http\Requests\CreateCourseRequest', '#createCourse'); !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\StoreClassRequest', '#addClass'); !!}--}}
@endsection