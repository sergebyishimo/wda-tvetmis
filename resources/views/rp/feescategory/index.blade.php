@extends('rp.layout.main')

@section('htmlheader_title')
    Fees Categories
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Fees Categories
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            @if (rpAllowed(4))
                <div class="col-md-12">
                    <div class="box">
                        @if($functionalfeecategory)
                            @php($route = ['rp.functionalfeescategory.update', $functionalfeecategory->id])
                            @php($id = "updateFeeCategory")
                        @else
                            @php($route = 'rp.functionalfeescategory.store')
                            @php($id = "createFeeCategory")
                        @endif
                        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                        @if($functionalfeecategory)
                            @method("PATCH")
                            {!! Form::hidden('id', $functionalfeecategory->id) !!}
                        @endif
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('category_name', "Fee category Name *") !!}
                                        {!! Form::text('category_name', $functionalfeecategory ? $functionalfeecategory->category_name: "", ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('mandatory', "Mandatory *") !!}
                                        <br>
                                        <input type="checkbox"
                                               {{ $functionalfeecategory ? $functionalfeecategory->mandatory == '1' ? 'checked' : ''  : '' }}
                                               name="mandatory" value="1"> Yes
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('multiple_time', "Can Be Paid Multiple Time In Single Year ?") !!}
                                        <br>
                                        <input type="checkbox"
                                               {{ $functionalfeecategory ? $functionalfeecategory->multiple_time == '1' ? 'checked' : ''  : '' }}
                                               name="multiple_time" value="1"> Yes
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('program', "Associated Program") !!}
                                        {!! Form::select('program', $programs, $functionalfeecategory ? $functionalfeecategory->program_id ?: null:null, ['class' => 'form-control select2', 'placeholder' => 'Select here ...']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-md btn-primary pull-left">
                                        {{ $functionalfeecategory ? "Update Fee Category" : "Save Fee Category" }}
                                    </button>
                                    <a href="{{ route('rp.functionalfeescategory.index') }}"
                                       class="btn btn-md btn-warning pull-right">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            @endif

            <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fees Category List</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th width="30px">#</th>
                                <th>Fee Category Name</th>
                                <th>Associated Program</th>
                                <th width="80px">Mandatory</th>
                                <th width="80px">Multiple Time</th>
                                <th width="100px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($functionalfeecategories as $feecat)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ ucwords($feecat->category_name) }}</td>
                                    <td>{{ $feecat->program ? ucwords($feecat->program->program_name) : "None" }}</td>
                                    <td class="text-center">{{ $feecat->mandatory == '1' ? 'Yes' : 'No' }}</td>
                                    <td class="text-center">{{ $feecat->multiple_time == '1' ? 'Yes' : 'No' }}</td>
                                    <td>
                                        <div class="row">
                                            @if (rpAllowed(3))
                                                <div class="col-md-6">
                                                    <a href="{{ route('rp.functionalfeescategory.edit', $feecat->id) }}"
                                                       class="btn btn-primary btn-sm btn-block" style="margin-bottom: 5px;">Edit</a>
                                                </div>
                                            @endif
                                            @if (rpAllowed(5))
                                                    <div class="col-md-6">
                                                        <form action="{{ route('rp.functionalfeescategory.destroy', $feecat->id) }}"
                                                              method="post">
                                                            @method("DELETE")
                                                            {{ csrf_field() }}
                                                            <button type="submit" class="btn btn-danger btn-sm btn-block">Delete</button>
                                                        </form>
                                                    </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($functionalfeecategory != null)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateFunctionFeeCategoryRequest', '#updateFeeCategory'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateFunctionFeeCategoryRequest', '#createFeeCategory'); !!}
    @endif
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>

    <script>
        $(function () {
            $(".datepicker").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@show