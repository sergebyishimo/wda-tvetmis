@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Profile
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="row">
                <div class="col-md-12" style="padding: 15px 25px 0px 25px;">
                    @include('rp.layout.feedback')
                </div>
            </div>

            {!! Form::open(['route' => 'college.update.profile', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
            @csrf()
            <div class="col-md-7">
                <!-- Default box -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">College Profile</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::hidden('id', college('id')) !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3q" class="col-sm-2 control-label">College Name</label>

                                <div class="col-sm-10">
                                    <input type="text" name="polytechnic" class="form-control" id="inputEmail3q"
                                           value="{{ college()->college->polytechnic }}"
                                           placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3w" class="col-sm-2 control-label">College Acronym</label>

                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" id="inputEmail3w"
                                           value="{{ college()->college->short_name }}"
                                           placeholder="Acronym">
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('photo', "College Logo", ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-5">
                                    {!! Form::file('photo') !!}
                                </div>
                                @if(college('logo'))
                                    <div class="col-sm-3 pull-right">
                                        <img src="{{ getUserGravatar() }}" alt="{{ college('name') }}"
                                             class="img-responsive img-rounded img-bordered-sm">
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::label('province', "Province", ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('province', college('province'), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('district', "District", ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('district', college('district'), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('email', "Email", ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::email('email', college('email'), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('telephone', "Telephone", ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('telephone', college('telephone'), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('po_box', "P.O Box", ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('po_box', college('po_box'), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('website', "Website", ['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('website', college('website'), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-5">
                <!-- Box two -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">College Profile</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Username</label>

                                <div class="col-sm-10">
                                    <input type="username" name="username" class="form-control" id="inputEmail3"
                                           value="{{ college('username') }}"
                                           placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                                <div class="col-sm-10">
                                    <input type="password" name="password" class="form-control" id="inputPassword3"
                                           placeholder="Password">
                                    <span class="help-block text-aqua text-sm">
                                            Leave it if no changes.
                                        </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword4" class="col-sm-2 control-label">Password Confirm</label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword4"
                                           name="password_confirmation"
                                           placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection