@extends('rp.layout.main')

@section('htmlheader_title')
    Departments
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Departments
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @if (rpAllowed(3) || rpAllowed(4))
                        @if($department)
                            @php($route = ['rp.departments.update', $department->id])
                            @php($id = "updateDepartment")
                        @else
                            @php($route = 'rp.departments.store')
                            @php($id = "createDepartment")
                        @endif
                        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                        @if($department)
                            @method("PATCH")
                            {!! Form::hidden('id', $department->id) !!}
                        @endif
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('department_name', "Department Name *") !!}
                                        {!! Form::text('department_name', $department ? $department->department_name: "", ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-md btn-primary pull-left">
                                        {{ $department ? "Update Department" : "Save Department" }}
                                    </button>
                                    <a href="{{ route('rp.departments.index') }}"
                                       class="btn btn-md btn-warning pull-right">Cancel</a>
                                </div>
                            </div>
                        </div>
                </div>
            {!! Form::close() !!}
            @endif

            <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List Of Departments</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Department Name</th>
                                <th>Department Code</th>
                                @if (rpAllowed(3) || rpAllowed(5))
                                    <th></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1 )
                            @foreach($departments as $depart)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ ucwords($depart->department_name) }}</td>
                                    <td>{{ ucwords($depart->department_code) }}</td>
                                    <td>
                                        @if (rpAllowed(3))
                                        <a href="{{ route('rp.departments.edit', $depart->id) }}"
                                           class="btn btn-primary btn-block" style="margin-bottom: 5px;">Edit</a>
                                        @endif
                                        @if (rpAllowed(5))
                                            <form action="{{ route('rp.departments.destroy', $depart->id) }}"
                                                  method="post">
                                                @method("DELETE")
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>

    @include('rp.layout.modals.warnings')
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
            $(".btn-delete").on("click", function () {
                let url = $(this).data("url");
                let names = $(this).data("names");
                // console.log(names);
                let modalDelete = $("#removeLevel");
                modalDelete.find("form").prop("action", url);
                modalDelete.find(".box-heading .names").html(names);
                modalDelete.modal();
            });
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });


        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($department)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateRpDepartmentRequest', '#updateDepartment'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateRpDepartmentRequest', '#createDepartment'); !!}
    @endif
@show