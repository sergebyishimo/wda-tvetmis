@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Enable student to do registration after system closed
@endsection

@section('contentheader_title')
    <div class="container-fluid">
        Enable student to do registration after system closed
    </div>
@endsection

@section('l-style')
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        @include('feedback.feedback')
        <div class="box">
            @if (rpAllowed(4))
                {{--@if(auth()->guard('college')->check())--}}
                {{--{!! Form::open(['route' => 'college.payments.outside', 'files' => true]) !!}--}}
                {{--@else--}}
                {!! Form::open(['route' => 'rp.exceptional.students.post', 'files' => true]) !!}
                {{--@endif--}}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::label('student_id', "Students *") !!}
                            {!! Form::select('student_id', [], null, ['class' => 'form-control select2']) !!}
                        </div>

                        <div class="col-md-2">
                            {!! Form::label('academic_year', "Academic Year *") !!}
                            {!! Form::text('academic_year', getCurrentAcademicYear(), ['class' => 'form-control', 'readonly' => true]) !!}
                        </div>

                        <div class="col-md-5">
                            {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '3', 'cols' => '2']) !!}
                        </div>

                        <div class="col-md-1">
                            <div class="clearfix">&nbsp;</div>
                            {!! Form::submit('Save', ['class' => 'btn btn-block btn-primary']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            @endif
        </div>
        <div class="box">
            <div class="box-header with-border">
                List
            </div>
            <div class="box-body" style="overflow-x: auto;">
                <table class="table table-bordered table-striped table-condensed table-responsive" id="dataTable">
                    <thead>
                    <tr>
                        <th width="50px" class="text-center">#</th>
                        @if(auth()->guard('rp')->check())
                            <th>College</th>
                        @endif
                        <th>Student Reg</th>
                        <th>Student Names</th>
                        <th>Academic Year</th>
                        <th>Description</th>
                        <th>Created At</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($students)
                        @php
                            $x = 1;
                        @endphp
                        @foreach($students as $student)
                            <tr>
                                <td class="text-center">{{ $x++ }}</td>
                                @if(auth()->guard('rp')->check())
                                    <td>{{ getStudentInfo($student->student_id, 'college_id') }}</td>
                                @endif
                                <td>{{ $student->student_id }}</td>
                                <td>{{ getStudentInfo($student->student_id, 'names') }}</td>
                                <td>{{ $student->academic_year }}</td>
                                <td>
                                    <div class="btn btn-link" data-container="body" data-toggle="popover"
                                         data-placement="top"
                                         data-content="{{ $student->description }}">
                                        {{ $student->description ? substr($student->description, 0, 10).(strlen($student->description) > 10 ? ' ...' : '') : "" }}
                                    </div>
                                </td>
                                <td>{{ $student->created_at }}</td>
                                <td>
                                    @if(!isRegistered($student->student_id) && rpAllowed(5))
                                        {!! Form::open(['route' => ['rp.exceptional.students.delete', $student->id]]) !!}
                                        @method("DELETE")
                                        {!! Form::hidden('student_id', $student->student_id) !!}
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="popover"]').popover();

            $('#dataTable').DataTable({
                dom: 'Blfrtip',
                "oSearch": {"bSmart": false},
                buttons: [
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column',
                        postfixButtons: ['colvisRestore']
                    },
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                pager: true,
                initComplete: function () {
                    var x = 0;
                    var lenC = this.api().columns().length;
                    this.api().columns().every(function () {
                        if (x == 1 || x == 4 || x == 6 || x == 7) {
                            var column = this;
                            var select = $('<select class="select2"><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        x++;
                    });
                }
            });
            let students = $("#student_id");
            $.get("{{ route('rp.ajax.get.short.students.exp') }}", {}, function (data) {
                let html = "<option value=''>Select now ....</option>";
                $.each(data, function (k, v) {
                    html += "<option value='" + k + "'>" + v + "</option>";
                });
                students.html(html);
            });
        });
    </script>
@endsection