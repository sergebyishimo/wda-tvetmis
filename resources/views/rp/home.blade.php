@extends('rp.layout.main')

@section('box-title', "Dashboard")

@section('htmlheader_title', "RP")

@section('panel-body')
    <div class="container-fluid spark-screen">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ App\Rp\CollegeStudent::count() }}</h3>

                        <p>Students</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ App\Rp\CollegeStudent::where('gender', 'female')->count() }}</h3>

                        <p>Female Students</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?gender=afemale" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3>{{ App\Rp\CollegeStudent::where('gender', 'male')->count() }}</h3>

                        <p>Male Students</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?gender=amale" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-gray">
                    <div class="inner">
                        <h3>{{ App\Rp\CollegeStudent::where('sponsorship_status', 'private')->count() }}</h3>

                        <p>Private Students</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            

            <!-- ./col -->
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Gishari')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Gishari</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=5" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Kigali')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Kigali</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=1" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Karongi')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Karongi</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=4" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Huye')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Huye</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=3" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Musanze')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Musanze</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=6" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-gray">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Kitabi')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Kitabi</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=2" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Tumba')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Tumba</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=8" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        @php
                            $college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Ngoma')->first();
                            if ($college_id) {
                                $count = App\Rp\CollegeStudent::where('college_id', $college_id->id)->count();
                            } else {
                                $count = 0;
                            }
                        @endphp
                        <h3>{{ $count }}</h3>

                        <p>IPRC Ngoma</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('rp.registration.index') }}?acollege=7" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <table class="table table-striped">
                            <thead class="text-right">
                                <tr>
                                    <th>College</th>
                                    <th>Application</th>
                                    <th>Registration</th>
                                    <th>School Fees</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $colleges = App\Rp\Polytechnic::all();
                                    $app_sum = 0;
                                    $reg_sum = 0;
                                    $fees_sum = 0;
                                    $all_sum = 0;
                                @endphp
                                @foreach ($colleges as $college)
                                    @php
                                        $application = getPaymentSum($college->id, 'Application');
                                        $registration = getPaymentSum($college->id, 'Registration');
                                        $school_fees = getPaymentSum($college->id, 'School Fees');
                                        $all = getPaymentSum($college->id);

                                        $app_sum += $application;
                                        $reg_sum += $registration;
                                        $fees_sum += $school_fees;
                                        $all_sum += $all;
                                    @endphp
                                    <tr>
                                        <th>{{ $college->short_name }}</th>
                                        <td class="text-right">{{ number_format($application) }} RWF</td>
                                        <td class="text-right">{{ number_format($registration) }} RWF</td>
                                        <td class="text-right">{{ number_format($school_fees) }} RWF</td>
                                        <td class="text-right">{{ number_format($all) }} RWF</td>
                                    </tr>
                                @endforeach
                                <tr class="bg-info">
                                    <td>Total</td>
                                    <td class="text-right">{{ number_format($app_sum) }} RWF</td>
                                    <td class="text-right">{{ number_format($reg_sum) }} RWF</td>
                                    <td class="text-right">{{ number_format($fees_sum) }} RWF</td>
                                    <td class="text-right">{{ number_format($all_sum) }} RWF</td>
                                </tr>
                                
                                {{--<tr>--}}
                                    {{--<th>IPRC Kitabi</th>--}}
                                    {{--@php($college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Kitabi')->first())--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Application')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Registration')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'School Fees')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id)) }} RWF</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>IPRC Huye</th>--}}
                                    {{--@php($college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Huye')->first())--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Application')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Registration')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'School Fees')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id)) }} RWF</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>IPRC Karongi</th>--}}
                                    {{--@php($college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Karongi')->first())--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Application')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Registration')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'School Fees')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id)) }} RWF</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>IPRC Gishari</th>--}}
                                    {{--@php($college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Gishari')->first())--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Application')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Registration')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'School Fees')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id)) }} RWF</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>IPRC Musanze</th>--}}
                                    {{--@php($college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Musanze')->first())--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Application')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Registration')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'School Fees')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id)) }} RWF</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>IPRC Ngoma</th>--}}
                                    {{--@php($college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Ngoma')->first())--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Application')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Registration')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'School Fees')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id)) }} RWF</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>IPRC Tumba</th>--}}
                                    {{--@php($college_id = App\Rp\Polytechnic::where('short_name', 'IPRC Tumba')->first())--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Application')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'Registration')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id, 'School Fees')) }} RWF</td>--}}
                                    {{--<td class="text-right">{{ number_format(getPaymentSum($college_id)) }} RWF</td>--}}
                                {{--</tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box body">
                            <canvas id="chartjs-0"></canvas>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!-- USERS LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Registered</h3>

                        <div class="box-tools pull-right">
                            <span class="label label-danger">{{ college('todayStudent') }} Today Registered</span>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">
                            @if(recentStudents()->count() > 0)
                                @foreach(recentStudents() as $student)
                                    <li>
                                        <img src="{{ regGetStudentPhoto($student) }}"
                                             alt="User Image">
                                        <a class="users-list-name"
                                           href="{{ route('rp.registration.show', $student->student_reg) }}">{{ $student->names }}</a>
                                        <span class="users-list-date">{{ $student->created_at->diffForHumans() }}</span>
                                    </li>
                                @endforeach
                            @else
                                <h3 class="text-red text-center">None this past day !!</h3>
                            @endif
                        </ul>
                        <!-- /.users-list -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="{{ route('rp.reg.finished') }}" class="uppercase">View All Registered</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->
            </div>
            <div class="col-md-6">
                <div class="box box-solid bg-black-gradient">
                    <div class="box-header">
                        <i class="fa fa-calendar"></i>

                        <h3 class="box-title">Calendar</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <!-- button with a dropdown -->
                            <div class="btn-group">
                                <button type="button" class="btn btn-success btn-sm dropdown-toggle"
                                        data-toggle="dropdown">
                                    <i class="fa fa-bars"></i></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Add new event</a></li>
                                    <li><a href="#">Clear events</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">View calendar</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <!--The calendar -->
                        <div id="calendar" style="width: 100%"></div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('jsview')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
        new Chart(document.getElementById("chartjs-0"),
            {"type":"bar","data":
                {
                    "labels":deps,
                    "datasets":[
                        {"label":"Students Per Department","data":deps_statistics,
                        "fill":false,
                        "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                        "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
                },
                "options":
                    {"scales":
                        {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});</script>
    
@endsection
