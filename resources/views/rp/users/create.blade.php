@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Create User
    </div>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        @include('feedback.feedback')
        <div class="box">
            <div class="box-header with-border">
                <a href="{{ route('rp.users.index') }}" class="btn btn-md btn-warning">
                    <i class="fa fa-list"></i>
                    <span>List of users</span>
                </a>
            </div>
            <!-- Default box -->
            <div class="box">
                @if ($college)
                    {!! Form::open(['route' => ['rp.users.update', $college->id], 'class' => 'form']) !!}
                    @method('PATCH')
                    {!! Form::hidden('col', $college->id) !!}
                @else
                    {!! Form::open(['route' => 'rp.users.store', 'class' => 'form']) !!}
                @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="form-group">
                                <label for="aCollege">Select Polytechnic</label>
                                <select name="college" class="form-control select2" required>
                                    <option value="" disabled selected>Select Polytechnic</option>
                                    @foreach($colleges as $acollege)
                                        <option value="{{ $acollege->id }}"
                                                @isset($college)
                                                    @if($college->college_id == $acollege->id) selected @endif
                                                @endisset >{{ $acollege->polytechnic }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        {!! Form::label('permission', 'Permission', ['class' => 'control-label']) !!}
                                        <select name="permission[]" multiple id="permission" required class="form-control select2">
                                            @foreach (getCollegePermission() as $key => $permission)
                                                <option value="{{ $key }}"
                                                @isset($college)
                                                    @if(strpos((string) $college->permission, (string) $key) !== false) selected @endif
                                                @endisset>{{ $permission }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('role', 'Post', ['class' => 'control-label']) !!}
                                        {!! Form::select('role', getCollegeUserRole() , isset($college) ? $college->roles : null  , ['class' => 'form-control select2', 'placeholder' => 'Select Post', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('name', "Names") !!}
                                {!! Form::text('name', isset($college) ? $college->name : null, ['class' => 'form-control', 'required' => true]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('username', "Email") !!}
                                {!! Form::email('username', isset($college) ? $college->username : null, ['class' => 'form-control', 'required' => '']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('password', "Password") !!}
                                <input class="form-control" name="password" type="password" id="password" @if(!isset($college)) required @endisset>
                            </div>
                            <div class="form-group">
                                {!! Form::label('password_confirmation', "Confirm Password") !!}
                                <input class="form-control" name="password_confirmation" type="password" id="password" @if(!isset($college)) required @endisset>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-md">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection