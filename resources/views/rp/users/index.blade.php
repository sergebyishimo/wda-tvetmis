@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        College Users
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        @include('feedback.feedback')
        <div class="box">
            @if (rpAllowed(4))
                <div class="box-header with-border">
                    <a href="{{ route('rp.users.create') }}" class="btn btn-md btn-primary">Create New</a>
                </div>
            @endif
            <div class="box-body">
                <table class="table" id="dataTable">
                    <thead>
                    <tr>
                        <th class="text-center" width="50px">#</th>
                        <th width="200px">Names</th>
                        <th width="200px">Username</th>
                        <th>College</th>
                        <th>Post</th>
                        <th width="100px"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($colleges->count() > 0)
                        @php
                            $x = 1;
                        @endphp
                        @foreach($colleges as $college)
                            <tr>
                                <td class="text-center">{{ $x++ }}</td>
                                <td>{{ ucwords($college->name) }}</td>
                                <td>{{ $college->username }}</td>
                                <td>{{ $college->college->short_name or "" }}</td>
                                <td>{{ ucwords(getCollegeUserRole($college->roles)) }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if (rpAllowed(3))
                                                <a href="{{ route('rp.users.edit', $college->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if (rpAllowed(5))
                                                <form action="{{ route('rp.users.destroy', $college->id)  }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm">Delete</button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
@show