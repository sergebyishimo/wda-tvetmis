@extends('rp.layout.main')

@section('box-title', "Email Portal")

@section('htmlheader_title', "Mailing portal")

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="box">
            <div class="box-body">
                {!! Form::open() !!}
                {!! Form::hidden('on', 'a') !!}
                {!! Form::submit('Send to all admitted students '.countRemainToSend(), ['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
