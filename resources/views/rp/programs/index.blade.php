@extends('rp.layout.main')

@section('htmlheader_title')
    Programs
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Programs
    </div>
@endsection

@section('l-style')
    @parent
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
@endsection

@if(isset($details) && $details == true)
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <a href="{{ route('rp.programs.index') }}" class="btn btn-primary btn-sm">back</a>
                        <center><h3 class="box-title text-primary">Details Information of program</h3></center>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <tr>
                                <th class="text-green col-md-4">Program Name</th>
                                <td style="font-weight: bold;" class="col-md-8">{{ $program->program_name }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Code</th>
                                <td>{{ $program->program_code }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Load</th>
                                <td>{{ $program->program_load }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Rtqf Level</th>
                                <td>{{ $program->rtqflevel->rtqf_level  or ""}}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Is It STEM ?</th>
                                <td>{{ $program->program_is_stem }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Program Status</th>
                                <td>{{ $program->program_status }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Level Applied</th>
                                <td>{{ $program->level_applied }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Tuition Fees</th>
                                <td>{{ $program->tuition_fees }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Other Course Fees</th>
                                <td>{{ $program->other_course_fees }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Admission Requirements</th>
                                <td>{{ $program->admission_requirements }}</td>
                            </tr>
                            <tr>
                                <th class="text-green">Attachment</th>
                                <td><a href="{{ asset('storage/'.$program->attachment) }}">View Attachment</a></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <center><h3 class="box-title text-primary">Course Units In <span
                                        class="text-green">{{$program->program_name}}</span></h3></center>
                        <div class="list-group list-group-horizontal pull-right">
                            <a href="{{ route('rp.courseunits.create',['p_id'=>$program->id]) }}"
                               class="list-group-item active">
                                Create a Course Unit</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-responsive" id="dataTable">
                            <tr>
                                <th>Course Unit Code</th>
                                <th>Course Unit Name</th>
                                <th>Credit Unit</th>
                                <th>Level Of Study</th>
                                <th>Semester Id</th>
                                <th>Unit Class Id</th>
                            </tr>
                            @foreach($program->courseunits as $courseunit)
                                <tr>
                                    <td>
                                        {{ $courseunit->course_unit_code }}
                                    </td>
                                    <td>
                                        <h5>
                                            {{ $courseunit->course_unit_name }}
                                        </h5>
                                    </td>
                                    <td>
                                        {{ $courseunit->credit_unit }}
                                    </td>
                                    <td>
                                        {{ $courseunit->levelofstudy->level_of_study or "" }}
                                    </td>
                                    <td>
                                        {{ $courseunit->semester->semester  or ""}}
                                    </td>
                                    <td>
                                        {{ $courseunit->unitclass->course_unit_class or ""}}
                                    </td>
                                    <td>
                                    </td>
                                    <th>
                                        @if (rpAllowed(3))
                                            <a href="{{ route('rp.courseunits.edit', $courseunit->id) }}"
                                               class="btn btn-info btn-sm">Edit</a>
                                        @endif
                                        @if (rpAllowed(5))
                                            <button type="button"
                                                    data-url="{{ route('rp.courseunits.destroy', $courseunit->id) }}"
                                                    data-names="{{ $courseunit->id }}"
                                                    class="btn btn-danger btn-sm btn-delete">
                                                Delete
                                            </button>
                                        @endif
                                    </th>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@else
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- List box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List</h3> <span class="pull-right"><a
                                    href="{{ route('rp.programs.create') }}" class="btn btn-primary btn-block">Add Program</a></span>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-responsive" id="dataTablePrograms">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Department Name</th>
                                <th>Program Name</th>
                                <th>Program Code</th>
                                <th>Tuition Fees</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                                $o = 1;
                                $tmp = "";
                            @endphp
                            @foreach($programs as $program)
                                @if($program->department_id != $tmp)
                                    @php
                                        $o += 1;
                                    @endphp
                                @endif
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td class="{{ $o % 2 == 0 ? "bg-gray" : "" }}">
                                        @if($program->department_id != $tmp)
                                            {{ $program->department ? ucwords(strtolower($program->department->department_name)) : "" }}
                                        @endif
                                    </td>
                                    <td class="{{ $o % 2 == 0 ? "bg-gray" : "" }}">
                                        <a href="{{ route('rp.programs.index',['o_o'=>$program->id]) }}">{{ $program->program_name }}</a>
                                    </td>
                                    <td class="{{ $o % 2 == 0 ? "bg-gray" : "" }}">{{ $program->program_code }}</td>
                                    <td class="{{ $o % 2 == 0 ? "bg-gray" : "" }}">{{ $program->tuition_fees }}</td>
                                    <td class="{{ $o % 2 == 0 ? "bg-gray" : "" }}">
                                        <di class="row p-0">
                                            @if (rpAllowed(3))
                                                <div class="col-md-6">
                                                    <a href="{{ route('rp.programs.edit', $program->id) }}"
                                                       class="btn btn-primary btn-sm">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </div>
                                            @endif
                                            @if (rpAllowed(5))
                                                    <div class="col-md-6">
                                                        <form action="{{ route('rp.programs.destroy', $program->id) }}"
                                                              method="post">
                                                            @method("DELETE")
                                                            {{ csrf_field() }}
                                                            <button type="submit" class="btn btn-sm btn-danger">
                                                                <i class="fa fa-trash-o"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                            @endif
                                        </di>
                                    </td>
                                </tr>
                                @php
                                    $tmp = $program->department_id;
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@endif
@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    {{--Datatable Exporting--}}
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#dataTablePrograms").dataTable({
                dom: 'Blfrtip',
                pager: true,
                "ordering": true,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'colvis',
                        collectionLayout: 'fixed two-column'
                    }
                ],
                columnDefs: [
                    {
                        targets: [],
                        visible: false,
                    }
                ]
            });
        });
    </script>
@show