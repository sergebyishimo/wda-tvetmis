@extends('rp.layout.main')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('panel-title')
    <div class="container-fluid">
        @if($type != null)
            Enrol Period
        @else
            System Activation
        @endif
    </div>
@endsection
@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css"
          integrity="sha256-9w7XtQnqRDvThmsQHfLmXdDbGasYsSjF6FSXrDh7F6g=" crossorigin="anonymous"/>
@endsection
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
            {!! Form::open(['route' => ($type != null ? 'rp.settings.set.enrol' : 'rp.settings.post.set.time'), 'method' => 'post', 'class' => 'form']) !!}
            <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('academic_year', "Academic Year") !!}
                            {!! Form::text('academic_year', reg_settings('academic_year', date('Y'), date('Y'), $type), ['class' => 'form-control', 'readonly' => '']) !!}
                        </div>
                        <div class="form-group">
                            <label for="">Choose Range</label>
                            <div class="input-group input-daterange">
                                <div class="input-group-addon" style="background-color: #f0f0f0;">From</div>
                                <input type="text" name="start" class="form-control"
                                       value="{{ reg_settings('start', date('Y'), date('Y-m-d'), $type) }}">
                                <div class="input-group-addon" style="background-color: #f0f0f0;">to</div>
                                <input type="text" name="ends" class="form-control"
                                       value="{{ reg_settings('ends', date('Y'), date('Y-m-d', strtotime("+7 days")), $type) }}">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        @php($value = 'Update')
                        @if(reg_settings() == '')
                            {!! Form::hidden('create', 'y') !!}
                            @php($value = 'Create')
                        @endif
                        <button type="submit" class="btn btn-primary">{{ $value }}</button>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"
            integrity="sha256-ncetQ5WcFxZU3YIwggfwOwmewLVX4SHLBtDYnrsxooY=" crossorigin="anonymous"></script>
    <script>
        $(function () {
            $('.datepicker').datepicker();
            $('.input-daterange input').each(function () {
                $(this).datepicker({
                    format: 'yyyy-mm-dd'
                });
            });
        });

    </script>
@endsection