@extends('rp.layout.main')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('panel-title')
    <div class="container-fluid">
        @if($type != null)
            Enrol Period
        @else
            System Activation
        @endif
    </div>
@endsection
@section('l-style')
    @parent
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css"
          integrity="sha256-9w7XtQnqRDvThmsQHfLmXdDbGasYsSjF6FSXrDh7F6g=" crossorigin="anonymous"/>
@endsection
@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
            {!! Form::open(['method' => 'post', 'class' => 'form']) !!}
            <!-- Default box -->
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('academic_year', "Active Academic Year") !!}
                            {!! Form::select('academic_year', academicYear(), config('mis.academic_year', date('Y') . ' - ' . date('Y', strtotime('+1y'))), ['class' => 'form-control', 'required' => true, 'placeholder' => 'Select here']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('semester', 'Select active semester') !!}
                            {!! Form::select('semester', [1 => 1,2 => 2], config('mis.semester', 1), ['class' => 'form-control select2', 'required' => true, 'placeholder' => 'Select here']) !!}
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"
            integrity="sha256-ncetQ5WcFxZU3YIwggfwOwmewLVX4SHLBtDYnrsxooY=" crossorigin="anonymous"></script>
    <script>
        $(function () {
            $('.datepicker').datepicker();
            $('.input-daterange input').each(function () {
                $(this).datepicker({
                    format: 'yyyy-mm-dd'
                });
            });
        });

    </script>
@endsection