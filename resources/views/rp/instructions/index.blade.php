@extends('wda.layout.main')

@section('panel-title', "Instructions Source")

@section('htmlheader_title', "Instructions Source")

@section('panel-body')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Registration Entry</h3>
                </div>
                {!! Form::open(['route' => 'rp.instructions.store']) !!}
                {!! Form::hidden('type', 'r') !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::textarea('instructions', getInstruction('r'), ['class' => 'form-control summernote', 'required' => true]) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-md']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Payments Entry</h3>
                </div>
                {!! Form::open(['route' => 'rp.instructions.store']) !!}
                {!! Form::hidden('type', 'p') !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::textarea('instructions', getInstruction('p'), ['class' => 'form-control summernote', 'required' => true]) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-md']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Accommodation Entry</h3>
                </div>
                {!! Form::open(['route' => 'rp.instructions.store']) !!}
                {!! Form::hidden('type', 'a') !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::textarea('instructions', getInstruction('a'), ['class' => 'form-control summernote', 'required' => true]) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-md']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3>Enrol Entry</h3>
                </div>
                {!! Form::open(['route' => 'rp.instructions.store']) !!}
                {!! Form::hidden('type', 'o') !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::textarea('instructions', getInstruction('o'), ['class' => 'form-control summernote', 'required' => true]) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-md']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('l-style')
    @parent
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endsection

@section('l-scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Text Goes here ...',
            tabsize: 2,
            height: 200
        });
    </script>
@endsection