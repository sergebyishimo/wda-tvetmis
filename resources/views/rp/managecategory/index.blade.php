@extends('rp.layout.main')

@section('htmlheader_title')
    Manage Fee Category
@endsection
@section('contentheader_title')
    <div class="container-fluid">
        Assign Fees To Category
    </div>
@endsection

@section('l-style')
    @parent
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .jFiler-theme-default .jFiler-input {
            width: inherit !important;
        }
    </style>
@endsection

@section('panel-body')
    <div class="container-fluid spark-screen">
        <div class="row">
            @if (rpAllowed(4) || rpAllowed(3))
                <div class="col-md-12">
                    <div class="box">
                        @if($managefcat)
                            @php($route = ['rp.managefuncfeecategory.update', $cat->id])
                            @php($id = "updateFManage")
                        @else
                            @php($route = 'rp.managefuncfeecategory.store')
                            @php($id = "createFManage")
                        @endif
                        {!! Form::open(['route' => $route, 'files' => true, 'method' => 'post', 'class' => 'form', 'id' => $id]) !!}
                        @if($managefcat && $cat)
                            @method("PATCH")
                            {!! Form::hidden('id', $cat->id) !!}
                        @endif
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('category_id', "Category Name *") !!}
                                        {!! Form::select('category_id',$cats->pluck('category_name','id'), isset($cat)? $cat->id : null,['placeholder' => 'Choose Category Name...', 'class' => 'form-control select2', 'required' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('fee_id', "Fee Name *") !!}
                                        {!! Form::select('fee_id', $fees->pluck('fee_name','id'), isset($fee_id) ? $fee_id : null,['placeholder' => 'Choose Fee Name...', 'class' => 'form-control select2', 'required' => true]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-md btn-primary pull-left">
                                        {{ $managefcat ? "Update Assign" : "Assign program" }}
                                    </button>
                                    <a href="{{ route('rp.managefuncfeecategory.index') }}"
                                       class="btn btn-md btn-warning pull-right">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                @endif

                <!-- List box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">List Of Categories With It's Fees</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped table-bordered" id="dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Category Fees</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i = 1 )
                                @foreach($cats as $cat)
                                    <tr>
                                        <td width="40px" class="text-center">{{ $i++ }}</td>
                                        <td>
                                            {{ ucwords($cat->category_name) }}
                                            <label class="label label-primary pull-right">
                                                {{$cat->fees->count()}}
                                            </label>
                                        </td>
                                        <td>
                                            <table>
                                                @foreach(@$cat->fees as $cfee)
                                                    <tr>
                                                        <td class="col-md-6">{{$cfee->fee_name or ""}}
                                                            <span class="pull-right">
                                                            <div class="row">
                                                                @if (rpAllowed(3))
                                                                    <div class="col-md-6">
                                                                  <a href="{{ route('rp.managefuncfeecategory.edit', ['id' => $cat->id,'fee_id'=> $cfee->id]) }}"
                                                                     class="btn btn-primary btn-sm"
                                                                     style="margin-bottom: 2px;">Edit</a>
                                                                </div>
                                                                @endif
                                                                @if (rpAllowed(5))
                                                                        <div class="col-md-6">
                                                                    <form action="{{ route('rp.managefuncfeecategory.destroy', ['id' => $cat->id,'fee_id'=> $cfee->id]) }}"
                                                                          method="post">
                                                                        @method("DELETE")
                                                                        {{ csrf_field() }}
                                                                        <button type="submit"
                                                                                class="btn btn-danger btn-sm">Delete</button>
                                                                    </form>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
        </div>
    </div>
@endsection
@section('l-scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if($managefcat)
        {!! JsValidator::formRequest('App\Http\Requests\CreateManageFeeCategoryRequest', '#updateFManage'); !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateManageFeeCategoryRequest', '#createFManage'); !!}
    @endif
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.js?v='.time()) }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/filer.js?v='.time()) }}"></script>

    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");

            $(".p").change(function () {
                var p = $(this).val();
                $.get("/location/d/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".d").html(html);
                });
            });

            $(".d").change(function () {
                var p = $(this).val();
                $.get("/location/s/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".s").html(html);
                });
            });

            $(".s").change(function () {
                var p = $(this).val();
                $.get("/location/c/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".c").html(html);
                });
            });

            $(".c").change(function () {
                var p = $(this).val();
                $.get("/location/v/" + p, {}, function (data) {
                    var obj = jQuery.parseJSON(data);
                    var html = "<option value='' selected disabled>Select..</option>";
                    $.each(obj, function (key, value) {
                        $.each(value, function (k, v) {
                            html += "<option value='" + v + "'>" + v + "</option>";
                        });
                    });
                    $(".v").html(html);
                });
            });
        });
    </script>
@show