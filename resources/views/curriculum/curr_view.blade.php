<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126663778-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126663778-1');
    </script>
</head>
<body>

<nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144"
     style="background-color: rgb(255, 255, 255);">
    <div class="container">
        <div class="row bg-white p-2" style="width: inherit !important;">
            <div class="col-md-9">
                <div class="navbar-toggleable-sm">
                    <span class="navbar-logo"><a href="#"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                    <span><a class="navbar-brand" href="#">{{ config('app.name') }}</a></span>
                    <!-- Example single danger button -->

                    <a class="btn btn-sm btn-dark shadow-sm"
                       href="{{ route('curricula.index') }}">
                        <span>Curriculum</span>
                    </a>

                    <a class="btn btn-sm btn-info shadow-sm"
                       href="{{ route('home.schools') }}">
                        <span>Schools</span>
                    </a>

                    <span class="dropdown">
                        <button class="btn btn-sm btn-warning shadow-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Support
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               href="{{ route('school.request.account') }}">Schools Account Request</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item"
                               href="{{ route('contact.us') }}">Contact Us</a>
                        </div>
                    </span>

                </div>
            </div>
            <div class="col-md-3">
                <div class="btn-group" style="float: right !important;">
                    <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        General Login
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('school.login') }}">Schools Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('college.login') }}">Colleges
                            Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('rp.login') }}">RP Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('district.login') }}">District Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('wda.login') }}">WDA Login</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/register') }}">REB | Create Account</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"
                           href="{{ url('/reb/login') }}">REB | Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
         style="background-image: url({{ asset('img/wlc_bg_n_4.jpg') }});background-attachment: fixed;">
    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="">
                        <table class="table table-bordered table-condensed bg-white table-hover rounded shadow-lg">
                            <tr>
                                <th class="" style="width: 300px;">Code</th>
                                <td>{{ $curr->qualification_code }}</td>
                            </tr>
                            <th class="bg-gray" style="">Qualification Title</th>
                            <td>{{ $curr->qualification_title }}</td>
                            </tr>
                            <th class="bg-gray" style="">Field</th>
                            <td>{{ $curr->subsector->sector->tvet_field or "" }}</td>
                            </tr>
                            <th class="bg-gray" style="">Trade</th>
                            <td>{{ $curr->subsector->sub_field_name or "" }}</td>
                            </tr>
                            <th class="bg-gray" style="">RTQF</th>
                            <td>{{ $curr->rtqf->level_name or "" }}</td>
                            </tr>
                            <th class="bg-gray" style="">Credits</th>
                            <td>{{ $curr->credits }}</td>
                            </tr>
                            <th class="bg-gray" style="">Release Date</th>
                            <td>{{ $curr->release_date }}</td>
                            </tr>
                            <th class="bg-gray" style="">Status</th>
                            <td>{{ $curr->status ? "Active" : "Inactive" }}</td>
                            </tr>
                            <th class="bg-gray" style="">Description</th>
                            <td>{!! $curr->description !!}</td>
                            </tr>
                            <tr>
                                <th class="bg-gray" style="">Attachment</th>
                                <td> @if($curr->attachment) <a href="{{ Storage::url($curr->attachment) }}">Download File</a> @endif</td>
                            </tr>
                            <th class="bg-gray" style="">Occupation Profile</th>
                            <td>{{ $curr->occupation_profile }}</td>
                            </tr>
                            {{--<th class="bg-gray" style="">Job Related Information</th>--}}
                            {{--<td>{{ $curr->job_related_information }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Entry Requirements</th>--}}
                            {{--<td>{{ $curr->entry_requirements }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Information About Pathways</th>--}}
                            {{--<td>{{ $curr->information_about_pathways }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Employability And Life Skills</th>--}}
                            {{--<td>{{ $curr->employability_and_life_skills }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Qualification Arrangment</th>--}}
                            {{--<td>{{ $curr->qualification_arrangement }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Competency Standards</th>--}}
                            {{--<td>{{ $curr->competency_standards }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Training Manual</th>--}}
                            {{--<td>{{ $curr->training_manual }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Trainees Manual</th>--}}
                            {{--<td>{{ $curr->trainees_manual }}</td>--}}
                            {{--</tr>--}}
                            {{--<th class="bg-gray" style="">Trainers Manual</th>--}}
                            {{--<td>{{ $curr->trainers_manual }}</td>--}}
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-3">
                    <table class="table table-bordered p-1">
                        <tr class="bg-warning">
                            <td>Number of competencies</td>
                            <td class="text-right">{{ $modules ? $modules->count() : 0 }}</td>
                        </tr>
                        @foreach(getModuleCompetenceClass() as $item)
                            <tr class="bg-warning">
                                <td>{{ ucwords($item) }}</td>
                                <td class="text-right">
                                    {{ $modules ? number_format($modules->where('competence_class', 'LIKE', $item)->count()) : 0 }}
                                </td>
                            </tr>
                        @endforeach
                        <tr class="bg-warning">
                            <td>Total Number Of Credit</td>
                            <td class="text-right">{{ $modules ? number_format($modules->sum('credits')) : 0 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            @if($modules)
                <div class="row" id="currModules">
                    <div class="col-md-12">
                        <h3 style="background: #fff;padding: 10px">Modules</h3>
                        <table class="table" id="dataTable" style="background: #fff;">
                            <thead>
                            <tr>
                                <th width="35px" class="text-center">#</th>
                                <th>Module Code</th>
                                <th>Module Title</th>
                                <th>Credits</th>
                                <th>Learning Hours</th>
                                <th>Document</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $x =1; @endphp
                            @forelse($modules as $module)
                                <tr>
                                    <td class="text-center">{{ $x++ }}</td>
                                    <td>{{ $module->module_code }}</td>
                                    <td>{{ $module->module_title }}</td>
                                    <td>{{ $module->credits }}</td>
                                    <td>{{ $module->learning_hours }}</td>
                                    <td>
                                        @if($module->attachment)
                                            @if(\Storage::disk(config('voyager.storage.disk'))->exists($module->attachment))
                                                <a href="{{ asset('storage/'.$module->attachment) }}"
                                                   class="btn btn-warning btn-outline-warning"
                                                   target="_blank">Download</a>
                                            @else
                                                <a class="btn btn-danger text-white">No Documents</a>
                                            @endif
                                        @else
                                            <a class="btn btn-danger text-white">No Documents</a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>

    </div>
</section>

<section class="mbr-section mbr-section-small mbr-footer" id="contacts1-m" data-rv-view="178"
         style="background-color: rgb(55, 56, 62); padding-top: 4.5rem; padding-bottom: 4.5rem;">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="img-responsive">
                    <img src="{{ asset('img/app_logo.jpg') }}" width="200px">
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>
                    <strong>Address</strong><br>
                    Kicukiro Niboye<br>
                    KK 15 Rd, Kigali
                </p>
            </div>
            <div class="col-xs-12 col-md-3">
                <p><strong>Contacts</strong><br>
                    Email: rp@gmail.com<br>
                    Phone: +250 788 4564<br>
                </p>
            </div>
            <div class="col-xs-12 col-md-3"><strong>Links</strong>
                <ul>
                    <li><a href="#">RP Website</a></li>
                    <li><a href="#">WDA Website</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>

</body>
</html>
