<!DOCTYPE html>
<html>

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body>

    <nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144" style="background-color: rgb(255, 255, 255);">
        <div class="container">
            <div class="row bg-white p-2" style="width: inherit !important;">
                <div class="col-md-9">
                    
                    <div class="navbar-toggleable-sm">
                        <span class="navbar-logo"><a href="/"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                        <span><a class="navbar-brand" href="/">{{ config('app.name') }}</a></span>
                        <!-- Example single danger button -->
                        <a class="btn btn-sm btn-primary shadow-sm" href="{{ route('home.dashboard') }}"> <span>Dashboard</span> </a>
                        <a class="btn btn-sm btn-dark shadow-sm"
                           href="{{ route('curricula.index') }}">
                            <span>Curriculum</span>
                        </a>
    
                        <a class="btn btn-sm btn-info shadow-sm"
                        href="{{ route('home.schools') }}">
                         <span>Schools</span>
                        </a>
    
                        <span class="dropdown">
                            <button class="btn btn-sm btn-warning shadow-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Support
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item"
                                   href="{{ route('school.request.account') }}">Schools Account Request</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item"
                                   href="{{ route('contact.us') }}">Contact Us</a>
                            </div>
                        </span>
    
                        <a class="btn btn-sm btn-success shadow-sm" href="{{ route('home.attachments') }}">
                        <span>Downloads</span>
                        </a>
                        
    
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="btn-group" style="float: right !important;">
                        <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        General Login
                    </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('school.login') }}">Schools Login</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('college.login') }}">Colleges
                            Login</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('rp.login') }}">RP Login</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('district.login') }}">District Login</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('wda.login') }}">WDA Login</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('/reb/register') }}">REB | Create Account</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('/reb/login') }}">REB | Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
        style="background-image: url({{ asset('img/wlc_bg_n_4.jpg') }});background-attachment: fixed;">
        <div class="mbr-table-cell">
            <br clear="left">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="background: #fff">
                        <img src="{{ asset('img/app_logo.jpg') }}" style="height:100px;float:left">
                        <h1 class="pull-left pt-4" style="margin-left: 130px">TVET Managment System</h1>
                        <br clear="left">
                        <br>
                        <h4 class="bg-primary p-3"><i class="fa fa-check-circle"></i> School Indicators</h4>

                        <div class="row">
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-aqua">
                                    <div class="inner">
                                        <h3>{{ $schools_nber }}</h3>
                                        <p>Schools</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-institution"></i>
                                    </div>
                                    <a href="{{ route('wda.schools') }}" class="small-box-footer">
                                            More info <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h3>{{ $private }}</h3>
                                        <p>Private Schools</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-bolt"></i>
                                    </div>
                                    <a href="{{ route('wda.school.water') }}" class="small-box-footer">More info
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-gray">
                                    <div class="inner">
                                        <h3>{{ $have_internet }}</h3>
                                        <p>Have Internet</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-rss"></i>
                                    </div>
                                    <a href="{{ route('wda.school.internet') }}" class="small-box-footer">More info
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3>{{ $have_electricity }}</h3>
                                        <p>Have Electricity</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-lightbulb-o"></i>
                                    </div>
                                    <a href="{{ route('wda.school.electricity') }}" class="small-box-footer">More info
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                </div>
                            </div>
                            <!-- ./col -->

                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-6">
                                <canvas id="chartjs-0"></canvas>
                            </div>
                            <div class="col-md-6">
                                <canvas id="chartjs-1"></canvas>
                            </div>
                        </div>
                        <br><br>
                        <h4 class="bg-primary p-3"><i class="fa fa-check-circle"></i> Student and Staff Indicators</h4>
                        <div class="row">
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-blue">
                                    <div class="inner">
                                        <h3>{{ \App\Student::count() }}</h3>
                                        <p>Students</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">
                                            More info
                                            <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-md-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-black">
                                    <div class="inner">
                                        <h3>{{ App\Student::where('gender', 'Female')->count() }}</h3>

                                        <p>Female</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-female"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-orange">
                                    <div class="inner">
                                        <h3>{{ App\StaffsInfo::count() }}</h3>

                                        <p>Staff Members</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-user-circle-o"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-purple">
                                    <div class="inner">
                                        <h3>{{ App\StaffsInfo::where('privilege', 7)->count() }}</h3>

                                        <p>Academic Staff</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-user-secret"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="mbr-section mbr-section-small mbr-footer" id="contacts1-m" data-rv-view="178" style="background-color: rgb(55, 56, 62); padding-top: 4.5rem; padding-bottom: 4.5rem;">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    <div class="img-responsive">
                        <img src="{{ asset('img/app_logo.jpg') }}" width="200px">
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <p>
                        <strong>Address</strong><br> Kicukiro Niboye<br> KK 15 Rd, Kigali
                    </p>
                </div>
                <div class="col-xs-12 col-md-3">
                    <p><strong>Contacts</strong><br> Email: rp@gmail.com<br> Phone: +250 788 4564<br>
                    </p>
                </div>
                <div class="col-xs-12 col-md-3"><strong>Links</strong>
                    <ul>
                        <li><a href="#">RP Website</a></li>
                        <li><a href="#">WDA Website</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
        new Chart(document.getElementById("chartjs-0"),
            {"type":"bar","data":
                {
                    "labels":["North","South","East","West","Kigali"],
                    "datasets":[
                        {"label":"Schools Per Province","data":[{{ implode(', ', $schools_per_province_arr) }}],
                        "fill":false,
                        "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                        "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]
                },
                "options":
                    {"scales":
                        {"yAxes":[{"ticks":{"beginAtZero":true}}]}}});
    </script>
    <script>
        new Chart(document.getElementById("chartjs-1"),
            {"type":"bar","data":
                {
                    "labels":["North","South","East","West","Kigali"],
                    "datasets":[
                        {
                            "label":"Students Per Province",
                            "data":[{{ implode(', ', $students_per_province) }}],
                            "fill":false,
                            "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                            "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],
                            "borderWidth":1}]},"options":{"scales":{"yAxes":[{"ticks":{"beginAtZero":true}}]}}});
    </script>

</body>

</html>