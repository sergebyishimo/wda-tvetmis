@extends('student.swift')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            {{-- <h2>ADVANCED FORM ELEMENTS</h2>
            <small class="text-muted">Welcome to Swift application</small> --}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header bg-blue" style="margin:0px;padding: 0;padding: 10px;">
                        <h2 style="text-transform:none">Payment History</h2>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th>Amount</th>
                                    <th>Operator</th>
                                    <th>Value Date</th>
                                    <th>Operation Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($payments as $payment)
                                    <tr>
                                        <td>{{ number_format($payment->paid_amount) }}</td>
                                        <td>{{ $payment->operator }}</td>
                                        <td>{{ $payment->year }}</td>
                                        <td>{{ $payment->op_date }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection