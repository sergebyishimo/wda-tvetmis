<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel Multi Auth Guard') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css"
          integrity="sha256-1pnzA5kM6b19fJfpvTytakbs8lMvR1zyKuWCEyN4Ibk=" crossorigin="anonymous"/>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

@yield('css')
<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body style="background-image: url({{ asset('img/bg_wood.png') }});background-attachment: fixed;">
@include('student.layout.partials.navbar')
<div class="container-fluid">
    <div class="row" style="margin-top: 70px">
        <div class="col-md-2">
            <div class="affix">
                @include('student.layout.partials.sidemenu')
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    @if(activeMenu(['student.payment.code.form', 'student.payment.pending', 'student.payment.history', 'student.payment.code.generate']) == 'active')
                        @include('student.payment.partials.top_menu')
                    @endif
                    @if(activeMenu(['student.academic.index', 'student.academic.store', 'student.academic.show']) == 'active')
                        @include('student.academic.partials.top_menu')
                    @endif
                    @if(activeMenu(['student.accommodation.index', 'student.accommodation.show']) == 'active')
                        @include('student.accommodation.partials.top_menu')
                    @endif
                    @include('student.layout.partials.instructions')
                </div>
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
{{--<script src="/js/app.js"></script>--}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

@yield('script')
<script>
    $('#affix').affix({
        offset: {
            top: 100,
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    })
</script>
</body>
</html>
