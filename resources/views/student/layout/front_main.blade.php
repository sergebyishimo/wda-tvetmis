<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/app_logo.jpg') }}" type="image/x-icon">
    <meta name="description" content="">
    <title>{{ ucwords(config('app.name')) }}</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"
          integrity="sha256-xJOZHfpxLR/uhh1BwYFS5fhmOAdIRQaiOul5F/b7v3s=" crossorigin="anonymous"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
          integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous"/>

    <link rel="stylesheet" href="{{ asset('css/socicon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @yield('styles')
</head>
<body>
<nav class="navbar navbar-light mbr-navbar navbar-fixed-top" id="ext_menu-b" data-rv-view="144"
     style="background-color: rgb(255, 255, 255);">
    <div class="container">
        <div class="row bg-white p-2" style="width: inherit !important;">
            <div class="col-md-9">
                <div class="navbar-toggleable-sm">
                    <span class="navbar-logo"><a href="#"><img src="{{ asset('img/app_logo.jpg') }}"> </a></span>
                    <span><a class="navbar-brand" href="#">{{ config('app.name') }}</a></span>
                    <!-- Example single danger button -->
                </div>
            </div>
            <div class="col-md-3">
                <div class="btn-group" style="float: right !important;">
                    <a href="{{ url('/') }}" class="btn btn-light">
                        Home
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
<section class="mbr-section mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-c" data-rv-view="146"
         style="background-image: url({{ asset('img/wlc_bg_n_4.jpg') }});background-attachment: fixed;">
    <div class="mbr-table-cell">

        <div class="container">
            @yield('content')
        </div>

    </div>
</section>

<section class="mbr-section mbr-section-small mbr-footer" id="contacts1-m" data-rv-view="178"
         style="background-color: rgb(55, 56, 62); padding-top: 4.5rem; padding-bottom: 4.5rem;">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="img-responsive">
                    <img src="{{ asset('img/app_logo.jpg') }}" width="200px">
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>
                    <strong>Address</strong><br>
                    Kicukiro Niboye<br>
                    KK 15 Rd, Kigali
                </p>
            </div>
            <div class="col-xs-12 col-md-3">
                <p><strong>Contacts</strong><br>
                    Email: rp@gmail.com<br>
                    Phone: +250 788 4564<br>
                </p>
            </div>
            <div class="col-xs-12 col-md-3"><strong>Links</strong>
                <ul>
                    <li><a href="#">RP Website</a></li>
                    <li><a href="#">WDA Website</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"
        integrity="sha256-FA14tBI8v+/1BtcH9XtJpcNbComBEpdawUZA6BPXRVw=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"
        integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function () {
        $(".select2").select2();
    });
</script>
@yield('scripts')
</body>
</html>