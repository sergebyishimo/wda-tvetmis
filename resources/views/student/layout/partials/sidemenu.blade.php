<div class="list-group">
    <a href="{{ route('student.home') }}"
       class="list-group-item {{ activeMenu(['student.home']) }}">Dashboard</a>

    <a href="{{ applied(auth()->user()->id) ? route('student.editstd', seoUrl(auth()->user()->name)) : route('student.registering') }}"
       class="list-group-item {{ activeMenu(['student.registering', 'student.editstd']) }}">
        @if(isAdmitted(get_my_info()) && true)
            Registration
        @else
            Application
        @endif
    </a>
    @if(enrol_enabled() && isAdmitted(get_my_info()))
        <a href="{{ route('student.enroll.index') }}"
           class="list-group-item {{ activeMenu(['student.enroll.index']) }}">
            Enrol
        </a>
    @endif
    @if(isAdmitted(get_my_info()) && (int) getMyYearOfStudy() <= 2 )
        <a href="{{ route('student.transfer') }}"
           class="list-group-item {{ activeMenu(['student.transfer']) }}">
            Request Transfer
        </a>
    @endif
    <a href="{{ route('student.payment.code.form') }}"
       class="list-group-item {{ activeMenu(['student.payment.code.form', 'student.payment.pending', 'student.payment.history', 'student.payment.code.generate']) }}">
        Payments
    </a>
    <a href="{{ route('student.academic.index') }}"
       class="list-group-item {{ activeMenu(['student.academic.index', 'student.academic.store', 'student.academic.show']) }}">Academic
        Results</a>
    <a href="{{ route('student.accommodation.index') }}"
       class="list-group-item {{ activeMenu(['student.accommodation.index', 'student.accommodation.show']) }}">Apply for
        Accommodation</a>
    <a href="{{ route('student.suspension.index') }}" class="list-group-item">Suspension</a>
    <a href="#" class="list-group-item">Timetable</a>
    <a href="#" class="list-group-item">RP Calendar</a>
    <a href="#" class="list-group-item">Seek help</a>
</div>