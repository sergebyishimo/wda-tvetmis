<div class="btn-group btn-group-md"
     role="group"
     aria-label="Instruction" style="margin-bottom: 10px;">
    @if(activeMenu(['student.registering', 'student.editstd']) == 'active')
        <a href="{{ route('student.instructions', 'r') }}"
           class="btn btn-danger {{ activeMenu('student.instructions') }}">Instructions</a>
    @endif

    @if(activeMenu(['student.payment.code.form', 'student.payment.pending', 'student.payment.history', 'student.payment.code.generate']) == 'active')
        <a href="{{ route('student.instructions', 'p') }}"
           class="btn btn-danger {{ activeMenu('student.instructions') }}">Instructions</a>
    @endif

    @if(activeMenu(['student.accommodation']) == 'active')
        <a href="{{ route('student.instructions', 'a') }}"
           class="btn btn-danger {{ activeMenu('student.instructions') }}">Instructions</a>
    @endif

    @if(activeMenu(['student.enrol']) == 'active')
        <a href="{{ route('student.instructions', 'o') }}"
           class="btn btn-danger {{ activeMenu('student.instructions') }}">Instructions</a>
    @endif
</div>