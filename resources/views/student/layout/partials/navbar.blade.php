<nav class="navbar navbar-inverse navbar-static-top" style="position: fixed; width: 100%;">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/student') }}">
                {{ config('app.name', 'Laravel Multi Auth Guard') }}
                @if(auth()->guard('student')->check())
                    <b style="font-family: 'Raleway', sans-serif !important;">
                        {{ get_my_info() }}
                    </b>
                @endif
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if(auth()->guard('student')->check())
                    @if( auth()->user()->payments)
                        @if(! applied(auth()->user()->id) )
                            @if(system_enabled() === true)
                                {{--<li><a href="{{ route('student.registering') }}">| Registration Form |</a></li>--}}
                            @endif
                        @endif
                    @endif
                    @php($co=null)
                    @if(get_my_info('college'))
                        @php( $co = get_my_info('college')->college->id )
                    @endif
                    @if(isContinuingStudent(get_my_info()) == false)
                        @if(isAdmitted(get_my_info()) && true)
                            <li class="btn btn-success flat" style="border-radius: 0px !important;margin-top: 2px;">
                                <a href="{{ route('student.admission.letter') }}"
                                   style="color: white; padding: 8px;"
                                   target="_blank">
                                    Admission Letter</a>
                            </li>
                        @endif
                    @endif
                    @if(isRegistered(get_my_info()))
                        <li class="btn btn-primary flat" style="border-radius: 0px !important;margin-top: 2px;">
                            <a href="{{ route('student.registration.prof') }}"
                               style="color: white; padding: 8px;"
                               target="_blank">
                                Proof of Registration</a>
                        </li>
                    @endif
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guard('student')->guest())
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('/student/login') }}">Login</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::guard('student')->user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/student/logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/student/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>