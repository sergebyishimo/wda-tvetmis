@extends('student.layout.auth')

@section('content')

    {{--<div class="alert alert-warning text-white" >--}}
    {{--Please your information is incomplete.--}}
    {{--</div>--}}
    <div class="row">
        @if(get_my_info())
            <div class="col-md-8">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>{!! __('messages.system.dashboard.list.title') !!}</h5>
                                </div>
                                <div class="col-md-6">
                                    <form action="{{ route('student.print', seoUrl(auth()->guard('student')->user()->name)) }}"
                                          method="post" target="_blank">
                                        {{ csrf_field() }}
                                        <button type="submit"
                                                class="btn btn-sm btn-default pull-right">{!! __('messages.system.dashboard.list.btn') !!}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>Names</td>
                                    <td><b>{{ ucwords(get_my_info('first_name'). " ". get_my_info('other_names')) }}</b>
                                    </td>
                                    <td rowspan="6" align="center">
                                        <img src="{{ asset("/storage/".get_my_info('photo')) }}"
                                             alt="{{ get_my_info('stdid') }}" width="200xp" height="200px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>E-Mail</td>
                                    <td><b>{{ get_my_info('email') }}</b></td>
                                </tr>
                                <tr>
                                    <td>Telephone:</td>
                                    <td><b>{{ ucwords(get_my_info('phone')) }}</b></td>
                                </tr>
                                <tr>
                                    <td>Gender:</td>
                                    <td><b>{{ ucwords(get_my_info('gender')) }}</b></td>
                                </tr>
                                <tr>
                                    <td>Ubudehe:</td>
                                    <td><b>{{ ucwords(get_my_info('ubudehe')) }}</b></td>
                                </tr>
                                <tr>
                                    <td>DOB:</td>
                                    <td><b>{{ ucwords(get_my_info('dob')) }}</b></td>
                                </tr>
                                <tr>
                                    <td>Parents Phone:</td>
                                    <td><b>{{ ucwords(get_my_info('parents_phone')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>National ID:</td>
                                    <td><b>{{ ucwords(get_my_info('national_id_number')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Province:</td>
                                    <td><b>{{ ucwords(get_my_info('province')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>District:</td>
                                    <td><b>{{ ucwords(get_my_info('district')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Sector:</td>
                                    <td><b>{{ ucwords(get_my_info('sector')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Cell:</td>
                                    <td><b>{{ ucwords(get_my_info('cell')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Village:</td>
                                    <td><b>{{ ucwords(get_my_info('village')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Your Bank:</td>
                                    <td><b>{{ ucwords(get_my_info('your_bank')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Your Bank Account:</td>
                                    <td><b>{{ ucwords(get_my_info('your_bank_account')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Disability:</td>
                                    <td><b>{{ ucwords(get_my_info('disability')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Examiner:</td>
                                    <td><b>{{ ucwords(get_my_info('examiner')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Index Number:</td>
                                    <td><b>{{ ucwords(get_my_info('index_number')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>School Attended:</td>
                                    <td><b>{{ ucwords(get_my_info('school_attended')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Graduation Year:</td>
                                    <td><b>{{ ucwords(get_my_info('graduation_year')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Option Offered:</td>
                                    <td><b>{{ ucwords(get_my_info('option_offered')) }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Aggregates Obtained:</td>
                                    <td><b>{{ get_my_info('aggregates_obtained') }}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="col-md-4 @if(get_my_info() == null) col-md-offset-2 @endif">
            <div class="row">
                <div class="col-md-12">
                    @if (count($notifications) > 0)
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Notifications</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                @foreach ($notifications as $notification)
                                    <div class="alter alter-info" style="margin-bottom: 5px;">
                                        {!! $notification->message !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @if(get_my_info()  && true)
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>College Information</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td align="right">Names</td>
                                        <td>
                                            <b>{{ ucwords(get_my_info('first_name')) . ' ' . ucwords(get_my_info('other_names')) }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">College</td>
                                        <td><b>{{ ucwords(get_my_info('college_id')) }}</b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Department</td>
                                        <td><b>{{ ucwords(get_my_info('department_id')) }}</b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Course</td>
                                        <td><b>{{ ucwords(get_my_info('course_id')) }}</b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Class</td>
                                        <td><b>Not Set Yet</b></td>
                                    </tr><!--
                                        // <tr>
                                        //     <td align="right">Sponsor</td>
                                        //     <td><b>{{ ucwords(get_my_info('sponsorship_status')) }}</b></td>
                                        // </tr> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
