@extends('student.layout.auth')

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    {!! getInstruction($type) !!}
                </div>
            </div>
        </div>
    </div>
@endsection