@extends('student.layout.auth')

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    @if(enrol_enabled() && isAdmitted(get_my_info()))
                        {!! Form::open(['route' => 'student.enroll.store']) !!}
                        {!! Form::hidden('college_id', get_my_info('college_id', true)) !!}
                        {!! Form::hidden('student_id', get_my_info()) !!}
                        @if(enrolled(get_my_info()))
                            {!! Form::hidden('cancel', get_my_info()) !!}
                        @endif
                        <div class="row">
                            <div class="col-md-3">
                                {!! Form::label('enrol_date', "Enrol Date") !!}
                                {!! Form::date('enrol_date', enrolled(get_my_info(), true, "enrol_date"), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-3">
                                <button type="submit"
                                        class="btn {{ enrolled(get_my_info()) ? 'btn-danger' : 'btn-primary' }} btn-block">
                                    @if(enrolled(get_my_info()))
                                        <span>Cancel Enroll</span>
                                    @else
                                        <span>Enroll</span>
                                    @endif
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    @else
                        <div class="alert alert-warning text-white"
                             style="background-color: #ce9300 !important;">
                            @if(isAdmitted(get_my_info()))
                                Enrol Not Open
                            @else
                                Oops, Enrol is not available to you ...
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
