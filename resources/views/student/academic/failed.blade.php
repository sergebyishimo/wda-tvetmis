@extends('student.layout.auth')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Failed Modules</h5>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Module Code</td>
                                <td>Module Name</td>
                                <td>Semester</td>
                                <td>Acad. Year</td>
                                <td>Obt. Marks</td>
                                <td>Credits</td>
                                <td>Fees</td>
                                <td>Paid</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php($i = 1)
                            @foreach ($failed as $fail)
                                @php( $module = \App\Rp\CollegeModule::find($fail->module_id) )
                                @php($marks = \App\Model\Rp\StudentMarks::where('academic_year', $fail->academic_year)->where('module_id', $fail->module_id)->where('semester', $module->semester)->where('registration_number', $fail->student_id)->where('retake', 1)->first())
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $module->module_code }}</td>
                                    <td>{{ $module->module_name }}</td>
                                    <td>Semester {{ $module->semester }}</td>
                                    <td>{{ $fail->academic_year }}</td>
                                    <td class="text-right">{{ $marks ? $marks->obtained_marks . ' / 100' : '-' }}</td>
                                    <td class="text-right">{{ $module->credits }}</td>
                                    <td class="text-right">{{ number_format($module->credits * 5000) }} Rwf</td>
                                    <td>{{ $fail->paid == null ? 'No' : 'Yes' }}</td>
                                </tr>                    
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
@endsection