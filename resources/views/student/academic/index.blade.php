@extends('student.layout.auth')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Academic Results</h5>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ route('student.academic.store') }}">
                            @csrf
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Academic Year</label>
                                    {!! Form::select('academic_year', academicYear(), null, ['class' => 'form-control select2']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Semester</label>
                                    <select name="semester" class="form-control">
                                        <option value="1">Semester 1</option>
                                        <option value="2">Semester 2</option>
                                        <option value="3">Semester 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <br>
                                <button type="submit" class="btn btn-primary btn-block mt-2">View Marks</button>
                            </div>
                        </form>
                    </div>
                    @isset($marks)
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Module Code</th>
                                            <th>Module Name</th>
                                            <th>Credits</th>
                                            <th>Obt. Marks / 100</th>
                                            <th>Decision</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($marks as $mark)
                                            @php( $module = \App\Rp\CollegeModule::find($mark->module_id) )
                                            <tr>
                                                <td>{{ $module->module_code }}</td>
                                                <td>{{ $module->module_name }}</td>
                                                <td>{{ $module->credits }}</td>
                                                <td>{{ $mark->obtained_marks }}</td>
                                                <td>{!! ($mark->obtained_marks < 50) ? '<span style="color: #cc0000">Failed</span>' : '<span style="color: green">Pass</span>' !!}</td>
                                            </tr>                    
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>    
                    @endisset
                    
                </div>
            </div>
        </div>
    </div>
@endsection