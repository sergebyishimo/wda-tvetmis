<div class="btn-group btn-group-md"
     role="group"
     aria-label="Payment Sub Menu" style="margin-bottom: 10px;">
    <a href="{{ route('student.academic.results') }}"
       class="btn btn-info {{ activeMenu('student.academic.results') }}">Results</a>
    <a href="{{ route('student.academic.index') }}"
       class="btn btn-info {{ activeMenu('student.academic.index') }}">AcademicYear Results</a>
    <a href="{{ route('student.academic.show', 0) }}"
       class="btn btn-danger {{ activeMenu('student.academic.show') }}">View Failed Modules</a>
</div>