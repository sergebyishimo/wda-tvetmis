<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>:: Swift - University Admin ::</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Favicon-->
    <link href="/swift/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css" integrity="sha256-1pnzA5kM6b19fJfpvTytakbs8lMvR1zyKuWCEyN4Ibk=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <link href="/swift/css/main.css" rel="stylesheet">
    <!-- Custom Css -->


    <link href="/swift/css/themes/all-themes.css" rel="stylesheet" />

    <style>
        .datepicker {
            width: 200px
        }
    </style>
</head>

<body class="theme-blush">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blush">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->



    <!-- Top Bar -->
    <nav class="navbar clearHeader">
        <div class="col-12">
            <div class="navbar-header"> <a href="javascript:void(0);" class="bars"></a> <a class="navbar-brand" href="index.html">Reg. #
            @if(auth()->guard('student')->check())
                    <b style="font-family: 'Raleway', sans-serif !important;">
                    : {{ get_my_info() }}
                </b>
            @endif
            </a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <!--Side menu and right menu -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="admin-image"> <img src="/storage/{{ get_my_info('photo') }}" alt=""> </div>
                <div class="admin-action-info"> <span>Welcome</span>
                    <h3>{{ ucwords(Auth::guard('student')->user()->name) }}</h3>
                    <ul>
                        {{--
                        <li><a data-placement="bottom" title="Go to Inbox" href="mail-inbox.html"><i class="zmdi zmdi-email"></i></a></li>
                        <li><a data-placement="bottom" title="Go to Profile" href="profile.html"><i class="zmdi zmdi-account"></i></a></li>
                        <li><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings"></i></a></li>
                        --}}
                        <li><a data-placement="bottom" title="Full Screen" href="sign-in.html"><i class="zmdi zmdi-sign-in"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active open"><a href="/student/new/home"><i class="zmdi zmdi-home"></i><span>Home</span></a></li>
                    <li><a href="/student/new/registration"><i class="zmdi zmdi-calendar-check"></i><span>Registration Form</span> </a></li>
                    <li><a href="{{ route('student.admission.letter') }}"><i class="zmdi zmdi-calendar-check"></i><span>My Admission Letter</span> </a></li>
                    <li><a href="/student/new/transfer"><i class="zmdi zmdi-calendar-check"></i><span>Request Transfer</span> </a></li>
                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account"></i><span>Payments</span> </a>
                        <ul class="ml-menu">
                            <li><a href="/student/new/request">Request Payment</a></li>
                            <li><a href="/student/new/pending">Pending Payments</a></li>
                            <li><a href="/student/new/history">History</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="zmdi zmdi-calendar-check"></i><span>Academic Results</span> </a></li>
                    <li><a href="#"><i class="zmdi zmdi-calendar-check"></i><span>Timetable</span> </a></li>
                    <li><a href="#"><i class="zmdi zmdi-calendar-check"></i><span>RP Calendar</span> </a></li>
                    <li><a href="#"><i class="zmdi zmdi-calendar-check"></i><span>Seek help </span> </a></li>
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->

    </section>
    <!--Side menu and right menu -->


    @yield('content')

    <!-- main content -->

    <div class="color-bg"></div>
    <!-- Jquery Core Js -->
    <script src="/swift/bundles/libscripts.bundle.js"></script>
    <!-- Lib Scripts Plugin Js -->
    <script src="/swift/bundles/vendorscripts.bundle.js"></script>
    <!-- Lib Scripts Plugin Js -->
    {{-- <script src="/swift/bundles/morphingsearchscripts.bundle.js"></script> --}}
    <!-- Main top morphing search -->

    <script src="/swift/bundles/mainscripts.bundle.js"></script>
    <!-- Custom Js -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
           minViewMode: 2,
           format: 'yyyy'
         });
    </script>

</body>

</html>