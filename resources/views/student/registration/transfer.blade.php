@extends('student.layout.auth')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"
          integrity="sha256-xJOZHfpxLR/uhh1BwYFS5fhmOAdIRQaiOul5F/b7v3s=" crossorigin="anonymous"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css"/>
@endsection

@section('content')
    @if(isAdmitted(get_my_info()) && (int) getMyYearOfStudy() <= 2)
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @include('flash_msg.flash-message')
                        @if(checkTransfer())
                            <div class="alert alert-warning"
                                 style="background-color: #ce9300 !important;color: #111111; font-weight: 600;">
                                Please your can't send more than one request.
                                {{--@if(!checkTransfer(true, 'from_confirm'))--}}
                                {!! Form::open(['route' => 'student.transfer.cancel', 'method' => 'post']) !!} Click
                                <button type="submit" class="btn btn-link">cancel request</button>
                                {!! Form::close() !!}
                                {{--@endif--}}
                            </div>
                        @else
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>Request a Transfer</h5>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::open(['files' => true, 'route' => 'student.submit.transfer.request']) !!}
                                <div class="panel-body">
                                    <div class="form-group {{ $errors->has('letter') ? 'has-error' : '' }}">
                                        {!! Form::label('letter', "Supporting Letter") !!}
                                        {!! Form::file('letter') !!}
                                        @if($errors->has('letter'))
                                            <span class="help-block">{!! $errors->first('letter') !!}</span>
                                        @endif
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h6 class="panel-title">Your Desires</h6>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group {{ $errors->has('college') ? 'has-error' : '' }}">
                                                        {!! Form::label('aCollege', "College") !!}
                                                        {!! Form::select('college', getCollegeForSelect(), null,
                                                        ['class' => 'form-control select2 college', 'placeholder' => 'Select College', 'id' => 'aCollege']) !!}
                                                        @if($errors->has('college'))
                                                            <span class="help-block">{!! $errors->first('college') !!}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group {{ $errors->has('department') ? 'has-error' : '' }}">
                                                        {!! Form::label('aDepartment', "Department") !!}
                                                        {!! Form::select('department', [], null,
                                                        ['class' => 'form-control select2 department', 'placeholder' => 'Select Department', 'id' => 'aDepartment']) !!}
                                                        @if($errors->has('department'))
                                                            <span class="help-block">{!! $errors->first('department') !!}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group {{ $errors->has('course') ? 'has-error' : '' }}">
                                                        {!! Form::label('aProgram', "Course") !!}
                                                        {!! Form::select('course', [], null,
                                                        ['class' => 'form-control select2 course', 'placeholder' => 'Select Course', 'id' => 'aProgram']) !!}
                                                        @if($errors->has('course'))
                                                            <span class="help-block">{!! $errors->first('course') !!}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <button type="submit" class="btn btn-md btn-primary">Send Request</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning"
                     style="background-color: #ce9300 !important;color: #111111; font-weight: 600;">
                    {{ isContinuingStudent(get_my_info()) ? "Eeeh, what are you trying to achieve... !!" : "Not yet admitted ..." }}
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"
            integrity="sha256-FA14tBI8v+/1BtcH9XtJpcNbComBEpdawUZA6BPXRVw=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script>
        $(function () {
            $(".select2").select2();
        })
    </script>
@endsection