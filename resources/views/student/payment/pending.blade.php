@extends('student.layout.auth')

@section('content')
    {{--<div class="alert alert-warning text-white" >--}}
    {{--Please your information is incomplete.--}}
    {{--</div>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Pending Payments</h3>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                @if($invoices)
                                    @php
                                        if ($invoices->count() == 2) {
                                            $col = 6;
                                        }elseif ($invoices->count() <= 1){
                                            $col = 12;
                                        }else{
                                            $col = 4;
                                        }

                                    @endphp
                                @endif
                                @forelse($invoices as $pinvoice)
                                    @php
                                        $h = 0;
                                        $x = 1;
                                        $amount = 0;
                                        $tmpCode = "";
                                    @endphp
                                    @if($h >= 1)
                                        <hr>
                                    @endif
                                    <div class="col-md-{{ $col }}">
                                        <div class="row">
                                            <div class="col-md-6">
                                                Date Created: {{ $pinvoice->created_at }}
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button class="btn btn-sm btn-warning print" type="button"
                                                        data-code="{{ $pinvoice->code }}">
                                                    Print
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="print{{ $pinvoice->code }}">
                                                    <table class="table table-bordered table-responsive table-striped table-condensed">
                                                        <thead>
                                                        <tr>
                                                            <th colspan="3" class="bg-info">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        {{ $pinvoice->name }}
                                                                    </div>
                                                                    <div class="col-md-6 text-right">
                                                                        Invoice Number: {{ $pinvoice->code }}
                                                                    </div>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($pinvoice->invoices as $invoice)
                                                            @php
                                                                $amount += $invoice->amount;
                                                            @endphp
                                                            <tr>
                                                                <td width="20px" class="text-center">{{ $x++ }}</td>
                                                                <td>{{ $invoice->fee->fee_name }}</td>
                                                                <td>{{ number_format($invoice->amount) }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Total</th>
                                                            <th colspan="2"
                                                                class="text-right">{{ number_format($amount) }}
                                                                FRW
                                                            </th>
                                                        </tr>
                                                        <tr class="bg-success">
                                                            <th>Paid</th>
                                                            <th colspan="2"
                                                                class="text-right">{{ getPaidAmountFromInvoice($invoice->code, get_my_info()) }}
                                                                FRW
                                                            </th>
                                                        </tr>
                                                        <tr class="bg-warning">
                                                            <th>Remain</th>
                                                            <th colspan="2"
                                                                class="text-right">{{ getRemainAmountFromInvoice($invoice->code, get_my_info()) }}
                                                                FRW
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                @if(getPaidAmountFromInvoice($invoice->code, get_my_info()) == 0)
                                                                    {!! Form::open(['route' => ['student.payment.pending.delete', $pinvoice->code]]) !!}
                                                                    @method('delete')
                                                                    {!! Form::hidden('code', $pinvoice->code) !!}
                                                                    <button class="btn btn-sm btn-danger"
                                                                            type="submit">
                                                                        Cancel Invoice
                                                                    </button>
                                                                    {!! Form::close() !!}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $h++;
                                    @endphp
                                @empty
                                    <div class="col-md-12">
                                        <div class="alert alert-warning"
                                             style="background-color: #ce9300 !important;">
                                            <h1 class="text-center"
                                                style="color: #000 !important;font-weight: 500 !important;">
                                                You don't have active payment </h1>
                                        </div>
                                    </div>
                                @endforelse
                                {{--@endif--}}
                            </div>
                            <div id="editor"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"
            integrity="sha384-THVO/sM0mFD9h7dfSndI6TS0PgAGavwKvB5hAxRRvc0o9cPLohB0wb/PTA7LdUHs"
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(function () {
            var doc = new jsPDF();

            $('*.print').click(function () {
                let code = $(this).data('code');
                PrintElem('print' + code, code);
            });

            function PrintElem(elem, title) {
                var mywindow = window.open('', 'PRINT', 'height=400,width=600');

                mywindow.document.write('<html><head><title>' + title + '</title>');
                mywindow.document.write('</head><body >');
                mywindow.document.write(document.getElementById(elem).innerHTML);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();

                return true;
            }
        });
    </script>
@endsection