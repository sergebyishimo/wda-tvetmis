@extends('student.layout.auth')

@section('content')
    <div class="row">
        <div class="col-md-{{ get_my_payment() != null  ? '8' : '12'}}">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>List of Payments Available</h5>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            @if(isAdmitted(get_my_info()))
                                @php
                                    $x = 1;
                                @endphp
                                <div class="panel-group" id="payments" role="tablist" aria-multiselectable="true">
                                    @forelse($feeCategoriesWithFees as $category)
                                        @if($category['multiple_time'] == 0 && doStudentPaidThisCategory($category['name'], get_my_info()))
                                            @php
                                                continue;
                                            @endphp
                                        @endif
                                        <div class="panel panel-{{ doHavePendingCategory($category['name'], get_my_info()) ? 'info' : 'warning' }}">
                                            <div class="panel-heading" role="tab" id="heading{{ $x }}">
                                                <h3 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#payments"
                                                       href="#payment{{ $x }}" aria-expanded="true"
                                                       aria-controls="collapse{{ $x }}">
                                                        {{ $category['name'] }}
                                                    </a>
                                                </h3>
                                            </div>
                                            {!! Form::open(['']) !!}
                                            {!! Form::hidden('col', $col) !!}
                                            {!! Form::hidden('category', $category['name']) !!}
                                            {!! Form::hidden('mandatory', $category['required']) !!}
                                            <div id="payment{{ $x }}" class="panel-collapse collapse in"
                                                 role="tabpanel"
                                                 aria-labelledby="heading{{ $x }}">
                                                <div class="panel-body">
                                                    @php
                                                        $fees = $category['fees'];
                                                        $mondatory = $category['required'];
                                                    @endphp
                                                    <table class="table table-responsive table-striped table-bordered">
                                                        @if($fees)
                                                            <thead>
                                                            <tr>
                                                                @foreach($fees as $fee)
                                                                    <th style="font-size: 12px">
                                                                        {{ ucwords($fee->fee_name) }}
                                                                    </th>
                                                                @endforeach
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                @foreach($fees as $fee)
                                                                    @if($col == "private_sponsored_amount")
                                                                        <td align="">
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    @if($mondatory == '1')
                                                                                        <input type="checkbox"
                                                                                               checked
                                                                                               disabled="">
                                                                                        {!! Form::hidden('fees[]', $fee->id) !!}
                                                                                    @else
                                                                                        <input type="checkbox"
                                                                                               value="{{ $fee->id }}"
                                                                                               name="fees[]"
                                                                                               required>
                                                                                    @endif
                                                                                    {{ strtoupper(number_format($fee->private_sponsored_amount)) }}
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    @else
                                                                        <td align="">
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    @if($mondatory == '1')
                                                                                        <input type="checkbox"
                                                                                               checked
                                                                                               disabled="">
                                                                                        {!! Form::hidden('fees[]', $fee->id) !!}
                                                                                    @else
                                                                                        <input type="checkbox"
                                                                                               value="{{ $fee->id }}"
                                                                                               name="fees[]">
                                                                                    @endif
                                                                                    {{ strtoupper(number_format($fee->govt_sponsored_amount)) }}
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        @endif
                                                    </table>
                                                </div>
                                                @if(doHavePendingCategory($category['name'], get_my_info()))
                                                    <div class="panel-footer">
                                                        <button type="submit" class="btn btn-success">Generate Code
                                                        </button>
                                                    </div>
                                                @endif
                                            </div>
                                            @php
                                                $x += 1;
                                            @endphp
                                            {!! Form::close() !!}
                                        </div>
                                    @empty
                                        <div class="alert alert-warning" style="background-color: #ce9300 !important;">
                                            <h1 class="text-center"
                                                style="color: #000 !important;font-weight: 500 !important;">No Payment
                                                Currently Available !!</h1>
                                        </div>
                                    @endforelse
                                </div>
                            @else
                                {!! Form::open(['']) !!}
                                {!! Form::hidden('col', $col) !!}
                                {!! Form::hidden('category', 'Application') !!}
                                {!! Form::hidden('mandatory', 1) !!}
                                <table class="table table-responsive table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <td>Application Fees</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" checked disabled="">
                                                {!! Form::hidden('fees[]', getApplicationFees('id')) !!}
                                                {!! getApplicationFees($col) !!}
                                            </label>
                                        </div>
                                    </td>
                                    </tbody>
                                </table>
                                @if(doHavePendingCategory("Application", get_my_info()) && !doStudentPaidThisCategory('Application', get_my_info()) )
                                    <div class="panel-footer">
                                        <button type="submit" class="btn btn-success">Generate Code
                                        </button>
                                    </div>
                                @endif
                                {!! Form::close() !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(get_my_payment() != null)
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-8">
                                <h5>Last Payment Made</h5>
                            </div>
                            {{--<div class="col-md-4">--}}
                            {{--<form action="{{ route('student.print.invoice') }}" method="post" target="_blank">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<button type="submit"--}}
                            {{--class="btn btn-sm btn-default pull-right">{!! __('messages.student.payment.btn.print') !!}</button>--}}
                            {{--</form>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>{!! __('messages.student.payment.details.created') !!}</th>
                                <td>{{ get_my_payment()->op_date }}</td>
                            </tr>
                            <tr>
                                <th>{!! __('messages.student.payment.details.mode') !!}</th>
                                <td>{{ get_my_payment()->pay_mode }}</td>
                            </tr>
                            <tr>
                                <th>{!! __('messages.student.payment.details.bank.name') !!}</th>
                                <td>{{ get_my_payment()->bank }}</td>
                            </tr>
                            <tr>
                                <th>{!! __('messages.student.payment.details.bank.account') !!}</th>
                                <td>{{ get_my_payment()->bank_account }}</td>
                            </tr>
                            <tr>
                                <th>{!! __('messages.student.payment.details.bank.slip') !!}</th>
                                <td>{{ get_my_payment()->bank_slipno }}</td>
                            </tr>
                            <tr>
                                <th>{!! __('messages.student.payment.details.amount') !!}</th>
                                <td>{{ get_my_payment()->paid_amount.' '.__('messages.student.payment.currency') }}</td>
                            </tr>
                            <tr>
                                <th>{!! __('messages.student.payment.details.status') !!}</th>
                                <td>{{ get_my_payment()->pay_status ? 'Yes' : 'Not Yet' }}</td>
                            </tr>
                            <tr>
                                <th>{!! __('messages.student.payment.details.trans') !!}</th>
                                <td>{{ get_my_payment()->trans_id }}</td>
                            </tr>
                            <tr>
                                <th colspan="2">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
