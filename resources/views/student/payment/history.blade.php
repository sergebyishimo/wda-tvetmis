@extends('student.layout.auth') 
@section('content')
    {{--
    <div class="alert alert-warning text-white">--}} {{--Please your information is incomplete.--}} {{--
    </div>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Payment History</h3>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Invoice Code</th>
                                        <th>Amount</th>
                                        <th>Operator</th>
                                        <th>Value Date</th>
                                        <th>Operation Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($payments as $payment)
                                        @php
                                            $invoice = \App\Rp\PendingPaymentInvoice::where('code', $payment->invoice_code)->first();
                                        @endphp
                                        <tr>
                                            <td>
                                                @if ($invoice)
                                                {{ $invoice->name }}
                                                @endif
                                            </td>
                                            <td>{{ $payment->invoice_code }}</td>
                                            <td class="text-right">{{ number_format($payment->paid_amount) }}</td>
                                            <td>{{ $payment->operator }}</td>
                                            <td>{{ $payment->year }}</td>
                                            <td>{{ $payment->op_date }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection