<div class="btn-group btn-group-md"
     role="group"
     aria-label="Payment Sub Menu" style="margin-bottom: 10px;">
    <a href="{{ route('student.payment.pending') }}"
       class="btn btn-info {{ activeMenu('student.payment.pending') }}">View Pending Payments</a>
    <a href="{{ route('student.payment.history') }}"
       class="btn btn-success {{ activeMenu('student.payment.history') }}">View Payments Historic</a>
</div>