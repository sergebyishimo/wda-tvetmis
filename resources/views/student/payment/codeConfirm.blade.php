@extends('student.layout.auth')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>{{ ucwords($category) }}</h4>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="text-right"><label
                                                class="label label-primary">Invoice Number: <b>{{ $code }}</b></label></h4>
                                </div>
                            </div>
                        </div>
                        {!! Form::open(['route' => 'student.payment.code.generate.submit']) !!}
                        {!! Form::hidden('col', $col) !!}
                        {!! Form::hidden('code_invoice', $code) !!}
                        {!! Form::hidden('category', $category) !!}
                        {!! Form::hidden('mandatory', $mandatory) !!}
                        <div class="panel-body">
                            <table class="table">
                                @if($fees)
                                    @foreach($fees as $fee)
                                        <tr>
                                            <td style="font-size: 12px">{{ ucwords($fee->fee_name) }}</td>
                                            @if($col == "private_sponsored_amount")
                                                <td align="right">
                                                    {{ strtoupper($fee->private_sponsored_amount) }}
                                                    <input type="hidden" name="fees[{{$fee->id}}]"
                                                           value="{{ $fee->private_sponsored_amount }}">
                                                </td>
                                            @else
                                                <td align="right">
                                                    {{ strtoupper($fee->govt_sponsored_amount) }}
                                                    <input type="hidden" name="fees[{{$fee->id}}]"
                                                           value="{{ $fee->govt_sponsored_amount }}">
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                                <tr class="bg-success">
                                    <td><span class="text-right"><b><i>Total:</i></b></span></td>
                                    <td class="text-right">
                                        <span>
                                            <b>{{ number_format($sum) }}</b>&nbsp;Rwf
                                        </span>
                                    </td>
                                </tr>
                                @if($sponsor)
                                    <tr class="bg-warning">
                                        <td>
                                            <span class="text-right"><b><i>{{ strtoupper($sponsor->sponsor) }}
                                                        &nbsp;Pay: {{ $sponsor->percentage ? "( ".$sponsor->percentage."% )" : "" }}</i></b></span>
                                        </td>
                                        <td class="text-right">
                                            @if($sponsor->percentage)
                                                @php
                                                    $covered = ($sum * $sponsor->percentage) / 100;
                                                @endphp
                                                <span><b>{{ number_format($covered) }}</b>&nbsp;Rwf</span>
                                            @else
                                                <span><b>{{ number_format($sum) }}</b>&nbsp;Rwf</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr class="bg-primary">
                                        <td>
                                            <span class="text-right"><b><i>Amount to Pay:</i></b></span>
                                        </td>
                                        <td class="text-right">
                                            @if($sponsor->percentage)
                                                @php
                                                    $covered = ($sum * $sponsor->percentage) / 100;
                                                    $pay = $sum - $covered;
                                                @endphp
                                                <span><b>{{ number_format($pay) }}</b>&nbsp;Rwf</span>
                                            @else
                                                <span><b>{{ number_format($sum) }}</b>&nbsp;Rwf</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-success">Confirm & Pay</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
