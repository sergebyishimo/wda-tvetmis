@extends('student.layout.auth')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('feedback.feedback')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Suspension</h5>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ route('student.suspension.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Attach a letter</label>
                                    <input type="file" name="letter" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Resuming Academic Year</label>
                                    {!! Form::select('academic_year', academicYear(null, 2020, false), null, ['class' => 'form-control select2']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Resuming Semester</label>
                                    <select name="semester" class="form-control">
                                        <option value="1">Semester 1</option>
                                        <option value="2">Semester 2</option>
                                        <option value="3">Semester 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <br>
                                <button type="submit" class="btn btn-primary btn-block mt-2">Submit Suspension</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Suspension Applications</h5>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Application Date</th>
                                <th>Resuming Acad. Year</th>
                                <th>Resuming Semester</th>
                                <th>Attached File</th>
                                <th>Decision</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($i = 1)
                            @forelse($suspensions as $sus)
                                <tr>
                                    <td>{{ $i++  }}</td>
                                    <td>{{ date('d/m/Y - H:i', strtotime($sus->created_at)) }}</td>
                                    <td>{{ $sus->resuming_academic_year  }}</td>
                                    <td>{{ $sus->resuming_semester  }}</td>
                                    <td><a href="/storage/{{ $sus->letter  }}" target="_blank">View Letter</a></td>
                                    <td>{{ $sus->decision == null ? 'Not Yet Replied' : $sus->decision  }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center">No Suspensions Found.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection