@extends('student.swift')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            {{-- <h2>ADVANCED FORM ELEMENTS</h2>
            <small class="text-muted">Welcome to Swift application</small> --}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header bg-blue" style="margin:0px;padding: 0;padding: 10px;">
                        <h2 style="text-transform:capitalize">Primary Information</h2>
                    </div>
                    <div class="card-body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" class="form-control" value="{{ old('first_name')?:splitNameFomStudent() }}" class="form-control" {{ admission()? 'readonly' : '' }}>
                                        <label class="form-label">First Name *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" class="form-control" value="{{ old("other_names")?:splitNameFomStudent(2)  }}" class="form-control" {{ admission()? 'readonly' : '' }}>
                                        <label class="form-label">Last Name *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="email" class="form-control" value="{{ old('email')?: auth()->guard('student')->user()->email }}" {{ admission()? 'readonly' : '' }}>
                                        <label class="form-label">E-mail *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" class="form-control" value="{{ old("phone")?:get_my_info('phone')  }}"
                                        data-container="body"
                                        data-toggle="popover" title="Allowed Telephone Number Format"
                                        data-placement="top"
                                        data-trigger="focus"
                                        data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx">
                                        <label class="form-label">Telephone *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Gender *</option>
                                            <option value="male" {{ old("gender") == 'male' || get_my_info("gender") == 'male' ? 'selected' : '' }}>Male</option>
                                            <option value="female" {{ old("gender") == 'female' || get_my_info('female') == 'female' ? 'selected' : '' }}>Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" class="form-control datepicker" value="{{ old("dob")?:get_my_info('dob')  }}" required {{ admission()? 'readonly' : '' }}>
                                        <label class="form-label">Birth Year *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Ubudehe *</option>
                                            <option value="4" {{ old('ubudehe') == '4' || get_my_info('ubudehe', true) == '4' ? 'selected' : '' }}>Category Four</option>
                                            <option value="3" {{ old('ubudehe') == '3' || get_my_info('ubudehe', true) == '3' ? 'selected' : '' }}>Category Three</option>
                                            <option value="2" {{ old('ubudehe') == '2' || get_my_info('ubudehe', true) == '2' ? 'selected' : '' }}>Category Two</option>
                                            <option value="1" {{ old('ubudehe') == '1' || get_my_info('ubudehe', true) == '1' ? 'selected' : '' }}>Category One</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="hidden" name="year_of_study" value="{{ getMyYearOfStudy() }}">
                                        <input type="text" id="year_of_study" value="{{ ucwords(getMyYearOfStudy(true))." Year" }}" readonly class="form-control">
                                        <label class="form-label">Year of Study *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="country" id="country" required class="form-control" {{ admission()? 'readonly' : '' }}  style="height: 36px;"> 
                                            <option>Country *</option>
                                            @foreach(\App\Rp\Country::where('enabled', 1)->get() as $country)
                                                <option value="{{ $country->id }}" {{ old('country') == $country->id || get_my_info('country', true) == $country->id ? 'selected' : '' }} >{{ $country->country_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" name="national_id_number"
                                            value="{{ old("national_id_number")?: get_my_info('national_id_number')  }}"
                                            title="National ID Number" {{ admission()? 'readonly' : '' }}
                                            data-toggle="popover"
                                            data-placement="top"
                                            data-trigger="focus"
                                            data-content="Please, spaces are not allowed and must be valid."
                                            class="form-control" required>
                                        <label class="form-label">National ID Number *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line focused">
                                    <select name="province" id="province" class="form-control" required {{ admission()? 'readonly' : '' }}  style="height: 36px;">
                                            <option value="">Province *</option>
                                            @foreach(getProvince() as $item )
                                                <option value="{{ $item->Province  }}" {{ old('province') == $item->Province || get_my_info('province', true) == $item->Province ? 'selected' : '' }} >{{ $item->Province  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="district" id="district" class="form-control" required {{ admission()? 'readonly' : '' }}  style="height: 36px;"> 
                                            @if(old('district') || get_my_info('district'))
                                                <option value="{{ old('district')?:get_my_info('district') }}" selected>{{ old('district')?:get_my_info('district') }}</option>
                                            @else
                                                <option value=""  selected disabled>District *</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="sector" id="sector" class="form-control" required {{ admission()? 'readonly' : '' }}  style="height: 36px;">  
                                            @if(old('sector') || get_my_info('sector'))
                                                <option value="{{ old('sector')?:get_my_info('sector') }}" selected>{{ old('sector')?:get_my_info('sector') }}</option>
                                            @else
                                                <option value="" selected disabled>Sector *</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="cell" id="cell" class="form-control" required {{ admission()? 'readonly' : '' }}  style="height: 36px;"> 
                                            @if(old('cell') || get_my_info('cell'))
                                                <option value="{{ old('cell')?:get_my_info('cell') }}" selected>{{ old('cell')?:get_my_info('cell') }}</option>
                                            @else
                                                <option value="" selected disabled>Cell *</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="village" id="village" class="form-control" required {{ admission()? 'readonly' : '' }}  style="height: 36px;"> 
                                            @if(old('village') || get_my_info('village'))
                                                <option value="{{ old('village')?:get_my_info('village') }}" selected>
                                                    {{ old('village')?:get_my_info('village') }}
                                                </option>
                                            @else
                                                <option value="" selected disabled>Village *</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="disability" id="disability" class="form-control" required {{ admission()? 'readonly' : '' }}  style="height: 36px;">  
                                            <option>Disability *</option>
                                            @foreach(\App\Rp\Disability::all() as $item )
                                                <option value="{{ $item->id  }}" {{ old('disability') == $item->type || get_my_info('disability', true) == $item->type ? 'selected' : '' }} >{{ $item->type  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="your_bank" id="bank" class="form-control" required {{ admission()? 'readonly' : '' }}  style="height: 36px;"> 
                                            <option value="">Your Bank *</option>
                                            @foreach(\App\Rp\Bank::all() as $item )
                                                <option value="{{ $item->id  }}" {{ old('your_bank') == $item->id || get_my_info('your_bank', true) == $item->id ? 'selected' : '' }}> {{ $item->bank_name  }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" name="your_bank_account" value="{{ old("your_bank_account")?:get_my_info('your_bank_account')  }}" {{ admission()? 'readonly' : '' }} class="form-control" required>
                                        <label class="form-label">Your Bank Account Number *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" id="father_name" name="father_name" value="{{ old('father_name') ?: get_my_info('father_name') }}" class="form-control" required>
                                        <label class="form-label">Father Names *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" name="father_nation_id"
                                            value="{{ old("father_nation_id")?: get_my_info('father_nation_id')  }}"
                                            title="National ID Number"
                                            data-toggle="popover"
                                            data-placement="top"
                                            data-trigger="focus"
                                            data-content="Please, spaces are not allowed and must be valid."
                                            class="form-control" required>
                                        <label class="form-label">Father National ID Number *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" id="father_phone" name="father_phone"
                                            data-container="body"
                                            data-toggle="popover" title="Allowed Telephone Number Format"
                                            data-placement="top"
                                            data-trigger="focus"
                                            data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                            value="{{ old('father_phone')?: get_my_info('father_phone') }}"
                                            class="form-control" required>
                                        <label class="form-label">Father Phone *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" id="mother_name" name="mother_name"
                                            value="{{ old('mother_name')?:get_my_info('mother_name') }}"
                                            class="form-control" required>
                                        <label class="form-label">Mother Names *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" name="mother_nation_id"
                                            value="{{ old("mother_nation_id")?: get_my_info('mother_nation_id')  }}"
                                            title="National ID Number"
                                            data-toggle="popover"
                                            data-placement="top"
                                            data-trigger="focus"
                                            data-content="Please, spaces are not allowed and must be valid."
                                            class="form-control" required>
                                        <label class="form-label">Mother National ID Number *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" id="mother_phone" name="mother_phone"
                                            data-container="body"
                                            data-toggle="popover" title="Allowed Telephone Number Format"
                                            data-placement="top"
                                            data-trigger="focus"
                                            data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                            value="{{ old('mother_phone')?: get_my_info('mother_phone') }}"
                                            class="form-control" required>
                                        <label class="form-label">Mother Phone *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" id="guardian_name" name="guardian_name"
                                            value="{{ old('guardian_name')?: get_my_info('guardian_name') }}"
                                            class="form-control" required>
                                        <label class="form-label">Guardian Names *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" name="guardian_nation_id"
                                            value="{{ old("guardian_nation_id")?: get_my_info('guardian_nation_id')  }}"
                                            title="National ID Number"
                                            data-toggle="popover"
                                            data-placement="top"
                                            data-trigger="focus"
                                            data-content="Please, spaces are not allowed and must be valid."
                                            class="form-control" required>
                                        <label class="form-label">Guardian National ID Number *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="text" id="guardian_phone" name="guardian_phone"
                                            data-container="body"
                                            data-toggle="popover" title="Allowed Telephone Number Format"
                                            data-placement="top"
                                            data-trigger="focus"
                                            data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                            value="{{ old('guardian_phone')?: get_my_info('guardian_phone') }}"
                                            class="form-control" required>
                                        <label class="form-label">Guardian Phone *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <div class="form-line focused">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Marital Status *</option>
                                            {!! Form::select('marital_status', maritalStatus(), get_my_info('marital_status'),
                                            ['class' => 'form-control select2', 'placeholder' => 'Select Marital Status']) !!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <div class="form-line focused">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Study Category *</option>
                                            {!! Form::select('student_category', studyCategory(), get_my_info('student_category'),
                                            ['class' => 'form-control select2', 'placeholder' => 'Select Study Mode']) !!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <div class="form-line focused">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Has Chronic Disease *</option>
                                            {!! Form::select('has_chronic_disease', chronicDisease(), get_my_info('has_chronic_disease'),
                                            ['class' => 'form-control select2', 'placeholder' => 'Select Disease']) !!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="header bg-blue" style="margin:0px;padding: 0;padding: 10px;">
                        <h2 style="text-transform:capitalize">Past Academic</h2>
                    </div>
                    <div class="card-body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Examiner *</option>
                                            @foreach(\App\Rp\Examiners::all() as $examiner )
                                                <option value="{{ $examiner->id  }}" {{ $examiner->id == old('examiner') || get_my_info('examiner', true) == $examiner->id ? 'selected' : '' }} >{{ $examiner->name  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">Graduation Year *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">Index Number *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">School Attended *</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Options Offered *</option>
                                            @foreach(\App\Rp\OptionOffered::all() as $item )
                                                    <option value="{{ $item->id  }}"
                                                            {{ $item->id == old('option_offered') || $item->id == get_my_info('option_offered', true) ? 'selected' : '' }}
                                                    >{{ $item->option_name  }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control">
                                        <label class="form-label">Aggregates Obtained *</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-raised g-bg-blush2 btn-block">Submit</button>
                <br><br>
            </div>
        </div>
    </div>
</section>
@endsection