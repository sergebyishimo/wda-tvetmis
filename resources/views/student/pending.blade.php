@extends('student.swift')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            {{-- <h2>ADVANCED FORM ELEMENTS</h2>
            <small class="text-muted">Welcome to Swift application</small> --}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header bg-blue" style="margin:0px;padding: 0;padding: 10px;">
                        <h2 style="text-transform:none">Pending Payments</h2>
                    </div>
                    <div class="card-body">
                        
                        <div class="alert alert-warning text-center" style="background-color: #ce9300 !important;">
                            <span style="font-size:20px;color: #000 !important;font-weight: 500 !important;"> You don't have active payment </span>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection