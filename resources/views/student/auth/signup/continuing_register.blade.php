@extends('student.layout.front_main')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header bg-info text-white">Continuing Students Sign Up</div>
                <div class="card-body">
                    <form class="form-horizontal" id="register" role="form" method="POST"
                          action="{{ route('student.post.signup.continuing') }}">
                        {{ csrf_field() }}
                        @include('flash_msg.flash-message')
                        <div class="form-group {{ $errors->has('college') ? ' has-error' : '' }}">
                            <label for="aCollege">College</label>
                            <select name="college" id="aCollege"
                                    class="form-control select2" required>
                                <option value="" disabled selected>Select Polytechnic</option>
                                @foreach(\App\Rp\Polytechnic::all() as $polytechnic )
                                    <option value="{{ $polytechnic->id  }}" {{ old('college') == $polytechnic->id ? 'selected' : '' }} >{{ $polytechnic->polytechnic  }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group {{ $errors->has('registration_number') ? ' has-error' : '' }}">
                            <label for="registration_number" class="control-label">Student Number</label>

                            <input id="registration_number" type="text" class="form-control" name="registration_number"
                                   value="{{ old('registration_number') }}">

                            @if ($errors->has('registration_number'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('registration_number') }}</strong>
                                        </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                            <input id="email" type="email" class="form-control" name="email"
                                   value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Password</label>

                            <input id="password" type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="control-label">Confirm Password</label>

                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Sign Up
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('js/register.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\SignupContinuingStudentRequest', '#register'); !!}
@endsection()