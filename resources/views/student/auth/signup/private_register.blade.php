@extends('student.layout.front_main')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header bg-info text-white">Private Students Sign Up</div>
                <div class="card-body">
                    <form class="form-horizontal" id="register" role="form" method="POST"
                          action="{{ route('student.post.signup.private') }}">
                        {{ csrf_field() }}
                        @include('flash_msg.flash-message')

                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name" class="control-label">Firstname</label>

                                    <input id="first_name" type="text" class="form-control" name="first_name"
                                           value="{{ old('first_name') }}"
                                           autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name" class="control-label">Lastname</label>

                                    <input id="last_name" type="text" class="form-control" name="last_name"
                                           value="{{ old('last_name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nation_id') ? ' has-error' : '' }} hidden">
                            <label for="nation_id" class="col-md-4 control-label">NationID</label>

                            <div class="col-md-12">
                                <input id="nation_id" type="text" class="form-control" name="nation_id"
                                       value="{{ old('nation_id') }}">

                                @if ($errors->has('nation_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nation_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }} hidden">
                            <label for="telephone" class="col-md-4 control-label">Telephone Address</label>

                            <div class="col-md-12">
                                <input id="telephone" type="text" class="form-control" name="telephone"
                                       value="{{ old('telephone') }}">

                                @if ($errors->has('telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                @endif
                                <span class="help-block text-info" style="font-size: small">Allowed formats: 078...., 073...., 072.... and please don't include +250 or 250</span>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-4{{ $errors->has('college') ? ' has-error' : '' }}">
                                    <label for="aCollege">College</label>
                                    <select name="college" id="aCollege"
                                            class="form-control select2" required>
                                        <option value="" disabled selected>Select Polytechnic</option>
                                        @foreach(\App\Rp\Polytechnic::all() as $polytechnic )
                                            <option value="{{ $polytechnic->id  }}">{{ $polytechnic->polytechnic  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4{{ $errors->has('department') ? ' has-error' : '' }}">
                                    <label for="aDepartment">Department</label>
                                    <select name="department" id="aDepartment"
                                            class="form-control select2" required>
                                        <option value="">Select Department</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4{{ $errors->has('program') ? ' has-error' : '' }}">
                                    <label for="aProgram">Program</label>
                                    <select name="program" id="aProgram"
                                            class="form-control select2" required>
                                        <option value="">Select Program</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Sign Up
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('js/register.js?v='.time())}}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\RegisterStudentRequest', '#register'); !!}

@endsection()