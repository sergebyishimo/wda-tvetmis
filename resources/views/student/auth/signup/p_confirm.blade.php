@extends('student.layout.front_main')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header bg-success text-white">Sign Up Done Successfully</div>
                <div class="card-body">
                    <h6>
                        Check your email for farther information.
                        <a href="{{ route('student.login') }}"
                           class="btn btn-link">click here to Login</a>
                    </h6>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection()