@extends('student.layout.auth')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/stylee.css?'.time())  }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

    <link rel="stylesheet" href="{{ asset('js/jQuery.filer/css/jquery.filer.css') }}">
    <style>
        .select2 {
            width: 100% !important;
        }

        #myBtn {
            display: none; /* Hidden by default */
            position: fixed; /* Fixed/sticky position */
            bottom: 20px; /* Place the button at the bottom of the page */
            right: 30px; /* Place the button 30px from the right */
            z-index: 99; /* Make sure it does not overlap */
            border: none; /* Remove borders */
            outline: none; /* Remove outline */
            background-color: red; /* Set a background color */
            color: white; /* Text color */
            cursor: pointer; /* Add a mouse pointer on hover */
            padding: 15px; /* Some padding */
            border-radius: 10px; /* Rounded corners */
            font-size: 18px; /* Increase font size */
        }

        #myBtn:hover {
            background-color: #555; /* Add a dark-grey background on hover */
        }
    </style>
@endsection
@section('content')
    @if(system_enabled() !== true && ! applied(auth()->user()->id) )
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning" style="background-color: #ce9300 !important;">
                    @if(strpos('closed', system_enabled()) === true )
                        <h6 style="color: #000 !important;font-weight: 700 !important;">{!! __('messages.system.closed') !!}</h6>
                    @else
                    @endif
                    @if(system_enabled())
                        <h6 style="color: #000 !important;font-weight: 700 !important;">{{ system_enabled() }}</h6>
                    @endif
                </div>
            </div>
        </div>
    @endif
    @php

        if (isAdmitted(get_my_info())){
            $sponsored = sponsored(get_my_info(), "Registration", true);
            $outside = paidOutSide(get_my_info(), null, getPaymentCategoryID("Registration"));
        }
        else{
            $sponsored = sponsored(get_my_info(), "Application", true);
            $outside = paidOutSide(get_my_info(), null, getPaymentCategoryID("Application"));
        }
    @endphp
    @if(auth()->user()->payments || $sponsored == 100 || $outside)
        @if(! applied(auth()->user()->id) )
            @if(system_enabled() === true)
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="POST"
                              action="{{ applied(get_my_info()) ? route('student.register_update') : route('student.register_applicant') }}"
                              id="register" enctype="multipart/form-data">
                            @include('flash_msg.flash-message')
                            @if ($errors->any())
                                <div class="alert alert-danger text-white">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                {{ csrf_field() }}
                                @if(applied(get_my_info()))
                                    {!! Form::hidden('id', get_my_info('id')) !!}
                                @endif
                                {{--// Primary Information--}}
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseOne"
                                               aria-expanded="true" aria-controls="collapseOne">
                                                Primary Information
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="form-group col-md-6" {{ $errors->has('first_name')? 'has-error' : ''  }}>
                                                    <label for="first_name">Firstname * </label>
                                                    <input type="text" name="first_name"
                                                           value="{{ old('first_name')?:splitNameFomStudent() }}"
                                                           class="form-control"
                                                           {{ admission()? 'readonly' : '' }} required>
                                                </div>
                                                <div class="form-group col-md-6 {{ $errors->has('other_names')? 'has-error' : ''  }}">
                                                    <label for="lastnames">Other Names *</label>
                                                    <input type="text" name="other_names"
                                                           value="{{ old("other_names")?:splitNameFomStudent(2)  }}"
                                                           class="form-control"
                                                           {{ admission()? 'readonly' : '' }} required>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6" {{ $errors->has('email')? 'has-error' : ''  }}>
                                                    <label for="email">E-Mail * </label>
                                                    <input type="text" name="email"
                                                           value="{{ old('email')?: auth()->guard('student')->user()->email }}"
                                                           {{ admission()? 'readonly' : '' }}
                                                           class="form-control" required>
                                                </div>
                                                <div class="form-group col-md-6 {{ $errors->has('phone')? 'has-error' : ''  }}">
                                                    <label for="phone">Telephone *</label>
                                                    <input type="text" name="phone"
                                                           value="{{ old("phone")?:ltrim(get_my_info('phone'), "25")  }}"
                                                           data-container="body"
                                                           data-toggle="popover" title="Allowed Telephone Number Format"
                                                           data-placement="top"
                                                           data-trigger="focus"
                                                           data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                           class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6" {{ $errors->has('gender')? 'has-error' : ''  }}>
                                                    <label for="gender">Gender * </label>
                                                    <select name="gender" id="gender"
                                                            class="form-control"
                                                            required>
                                                        <option value="" selected disabled>Select Gender</option>
                                                        <option value="male" {{ old("gender") == 'male' || get_my_info("gender") == 'male' ? 'selected' : '' }} >
                                                            Male
                                                        </option>
                                                        <option value="female" {{ old("gender") == 'female' || get_my_info('female') == 'female' ? 'selected' : '' }} >
                                                            Female
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 {{ $errors->has('dob')? 'has-error' : ''  }}">
                                                    <label for="phone">Date of Birth *</label>
                                                    <input type="text" name="dob"
                                                           value="{{ old("dob")?:get_my_info('dob')  }}"
                                                           class="form-control datepicker"
                                                           required {{ admission()? 'readonly' : '' }}>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6" {{ $errors->has('ubudehe')? 'has-error' : ''  }}>
                                                    <label for="ubudehe">Ubudehe * </label>
                                                    <select name="ubudehe" id="ubudehe" class="form-control"
                                                            required {{ admission()? 'readonly' : '' }}>
                                                        <option value="" selected disabled>Select Ubudehe</option>
                                                        <option value="4" {{ old('ubudehe') == '4' || get_my_info('ubudehe', true) == '4' ? 'selected' : '' }} >
                                                            Category
                                                            Four
                                                        </option>
                                                        <option value="3" {{ old('ubudehe') == '3' || get_my_info('ubudehe', true) == '3' ? 'selected' : '' }} >
                                                            Category
                                                            Three
                                                        </option>
                                                        <option value="2" {{ old('ubudehe') == '2' || get_my_info('ubudehe', true) == '2' ? 'selected' : '' }} >
                                                            Category
                                                            Two
                                                        </option>
                                                        <option value="1" {{ old('ubudehe') == '1' || get_my_info('ubudehe', true) == '1' ? 'selected' : '' }} >
                                                            Category
                                                            One
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4 {{ $errors->has('year_of_study')? 'has-error' : ''  }}">
                                                    <label for="year_of_study">Year Of Study *</label>
                                                    <input type="hidden" name="year_of_study"
                                                           value="{{ getMyYearOfStudy() }}">
                                                    <input type="text" id="year_of_study"
                                                           value="{{ ucwords(getMyYearOfStudy(true))." Year" }}"
                                                           readonly
                                                           class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-4" {{ $errors->has('country')? 'has-error' : ''  }}>
                                                    <label for="country">Country * </label>
                                                    <select name="country" id="country" required
                                                            class="form-control">
                                                        <option value="" selected disabled>Select Your Country</option>
                                                        @foreach(\App\Rp\Country::where('enabled', 1)->get() as $country)
                                                            <option value="{{ $country->id }}" {{ old('country') == $country->id || get_my_info('country', true) == $country->id ? 'selected' : '' }} >{{ $country->country_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4 {{ $errors->has('national_id_number')? 'has-error' : ''  }}">
                                                    <label for="national_id">NationalID/PassportID Number *</label>
                                                    <input type="text" name="national_id_number"
                                                           value="{{ old("national_id_number")?: get_my_info('national_id_number')  }}"
                                                           title="National ID Number" {{ admission()? 'readonly' : '' }}
                                                           data-toggle="popover"
                                                           data-placement="top"
                                                           data-trigger="focus"
                                                           data-content="Please, spaces are not allowed and must be valid."
                                                           class="form-control" required>
                                                </div>
                                                <div class="form-group col-md-4 {{ $errors->has('address')? 'has-error' : ''  }}">
                                                    <label for="address">Address</label>
                                                    <input type="text" name="address"
                                                           value="{{ old("address")?: get_my_info('address')  }}"
                                                           class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6" {{ $errors->has('province')? 'has-error' : ''  }}>
                                                    <label for="province">Province </label>
                                                    <select name="province" id="province" class="form-control"
                                                            required {{ admission()? 'readonly' : '' }}>
                                                        <option value="" selected disabled>Select Province</option>
                                                        @foreach(getProvince() as $item )
                                                            <option value="{{ $item->Province  }}"
                                                                    {{ old('province') == $item->Province || get_my_info('province', true) == $item->Province ? 'selected' : '' }}
                                                            >{{ $item->Province  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6" {{ $errors->has('district')? 'has-error' : ''  }}>
                                                    <label for="province">District</label>
                                                    <select name="district" id="district" class="form-control"
                                                            required {{ admission()? 'readonly' : '' }}>
                                                        @if(old('district') || get_my_info('district'))
                                                            <option value="{{ old('district')?:get_my_info('district') }}"
                                                                    selected>{{ old('district')?:get_my_info('district') }}</option>
                                                        @else
                                                            <option value="" selected disabled>Select District</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6" {{ $errors->has('sector')? 'has-error' : ''  }}>
                                                    <label for="province">Sector * </label>
                                                    <select name="sector" id="sector" class="form-control"
                                                            required {{ admission()? 'readonly' : '' }}>
                                                        @if(old('sector') || get_my_info('sector'))
                                                            <option value="{{ old('sector')?:get_my_info('sector') }}"
                                                                    selected>{{ old('sector')?:get_my_info('sector') }}</option>
                                                        @else
                                                            <option value="" selected disabled>Select Sector</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 {{ $errors->has('cell')? 'has-error' : ''  }}">
                                                    <label for="cell">Cell *</label>
                                                    <select name="cell" id="cell" class="form-control"
                                                            required {{ admission()? 'readonly' : '' }}>
                                                        @if(old('cell') || get_my_info('cell'))
                                                            <option value="{{ old('cell')?:get_my_info('cell') }}"
                                                                    selected>{{ old('cell')?:get_my_info('cell') }}</option>
                                                        @else
                                                            <option value="" selected disabled>Select Cell</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6" {{ $errors->has('village')? 'has-error' : ''  }}>
                                                    <label for="village">Village * </label>
                                                    <select name="village" id="village" class="form-control"
                                                            required {{ admission()? 'readonly' : '' }}>
                                                        @if(old('village') || get_my_info('village'))
                                                            <option value="{{ old('village')?:get_my_info('village') }}"
                                                                    selected>
                                                                {{ old('village')?:get_my_info('village') }}
                                                            </option>
                                                        @else
                                                            <option value="" selected disabled>Select Village</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 {{ $errors->has('disability')? 'has-error' : ''  }}">
                                                    <label for="disability">Disability *</label>
                                                    <select name="disability" id="disability" class="form-control"
                                                            required {{ admission()? 'readonly' : '' }}>
                                                        <option value="" selected disabled>Select Disability</option>
                                                        @foreach(\App\Rp\Disability::all() as $item )
                                                            <option value="{{ $item->id  }}"
                                                                    {{ old('disability') == $item->type || get_my_info('disability', true) == $item->type ? 'selected' : '' }}
                                                            >{{ $item->type  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6 {{ $errors->has('your_bank')? 'has-error' : ''  }}">
                                                    <label for="bank">Your Bank *</label>
                                                    <select name="your_bank" id="bank" class="form-control"
                                                            required>
                                                        <option value="" selected disabled>Select Your Bank</option>
                                                        @foreach(\App\Rp\Bank::all() as $item )
                                                            <option value="{{ $item->id  }}"
                                                                    {{ old('your_bank') == $item->id || get_my_info('your_bank', true) == $item->id ? 'selected' : '' }}>
                                                                {{ $item->bank_name  }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6" {{ $errors->has('your_bank_account')? 'has-error' : ''  }}>
                                                    <label for="bank_account">Your Bank Account * </label>
                                                    <input type="text" name="your_bank_account"
                                                           value="{{ old("your_bank_account")?:get_my_info('your_bank_account')  }}"
                                                           class="form-control" required>
                                                </div>
                                            </div>
                                            @if(isAdmitted(get_my_info()))
                                                <div class="row">
                                                    <div class="form-group col-md-4" {{ $errors->has('father_name')? 'has-error' : ''  }}>
                                                        <label for="father_name">Father Names</label>
                                                        <input type="text" id="father_name" name="father_name"
                                                               value="{{ old('father_name') ?: get_my_info('father_name') }}"
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group col-md-4 {{ $errors->has('father_nation_id')? 'has-error' : ''  }}">
                                                        <label for="father_nation_id">Father National ID Number</label>
                                                        <input type="text" name="father_nation_id"
                                                               value="{{ old("father_nation_id")?: get_my_info('father_nation_id')  }}"
                                                               title="National ID Number"
                                                               data-toggle="popover"
                                                               data-placement="top"
                                                               data-trigger="focus"
                                                               data-content="Please, spaces are not allowed and must be valid."
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group col-md-4" {{ $errors->has('father_phone')? 'has-error' : ''  }}>
                                                        <label for="father_phone">Father Phone </label>
                                                        <input type="text" id="father_phone" name="father_phone"
                                                               data-container="body"
                                                               data-toggle="popover"
                                                               title="Allowed Telephone Number Format"
                                                               data-placement="top"
                                                               data-trigger="focus"
                                                               data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                               value="{{ old('father_phone')?: get_my_info('father_phone') }}"
                                                               class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4" {{ $errors->has('mother_name')? 'has-error' : ''  }}>
                                                        <label for="mother_name">Mother Names</label>
                                                        <input type="text" id="mother_name" name="mother_name"
                                                               value="{{ old('mother_name')?:get_my_info('mother_name') }}"
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group col-md-4 {{ $errors->has('mother_nation_id')? 'has-error' : ''  }}">
                                                        <label for="mother_nation_id">Mother National ID Number</label>
                                                        <input type="text" name="mother_nation_id"
                                                               value="{{ old("mother_nation_id")?: get_my_info('mother_nation_id')  }}"
                                                               title="National ID Number"
                                                               data-toggle="popover"
                                                               data-placement="top"
                                                               data-trigger="focus"
                                                               data-content="Please, spaces are not allowed and must be valid."
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group col-md-4" {{ $errors->has('mother_phone')? 'has-error' : ''  }}>
                                                        <label for="mother_phone">Mother Phone </label>
                                                        <input type="text" id="mother_phone" name="mother_phone"
                                                               data-container="body"
                                                               data-toggle="popover"
                                                               title="Allowed Telephone Number Format"
                                                               data-placement="top"
                                                               data-trigger="focus"
                                                               data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                               value="{{ old('mother_phone')?: get_my_info('mother_phone') }}"
                                                               class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4" {{ $errors->has('guardian_name')? 'has-error' : ''  }}>
                                                        <label for="guardian_name">Guardian Names</label>
                                                        <input type="text" id="guardian_name" name="guardian_name"
                                                               value="{{ old('guardian_name')?: get_my_info('guardian_name') }}"
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group col-md-4 {{ $errors->has('guardian_nation_id')? 'has-error' : ''  }}">
                                                        <label for="guardian_nation_id">Guardian National ID
                                                            Number</label>
                                                        <input type="text" name="guardian_nation_id"
                                                               value="{{ old("guardian_nation_id")?: get_my_info('guardian_nation_id')  }}"
                                                               title="National ID Number"
                                                               data-toggle="popover"
                                                               data-placement="top"
                                                               data-trigger="focus"
                                                               data-content="Please, spaces are not allowed and must be valid."
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group col-md-4" {{ $errors->has('guardian_phone')? 'has-error' : ''  }}>
                                                        <label for="guardian_phone">Guardian Phone </label>
                                                        <input type="text" id="guardian_phone" name="guardian_phone"
                                                               data-container="body"
                                                               data-toggle="popover"
                                                               title="Allowed Telephone Number Format"
                                                               data-placement="top"
                                                               data-trigger="focus"
                                                               data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                               value="{{ old('guardian_phone')?: get_my_info('guardian_phone') }}"
                                                               class="form-control" required>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="row">
                                                @if(get_my_info('college_id', true) == 1)
                                                    <div class="form-group col-md-4" {{ $errors->has('student_category')? 'has-error' : ''  }}>
                                                        <label for="student_category">Study Category *</label>
                                                        {!! Form::select('student_category', studyCategory(), get_my_info('student_category'),
                                                        ['class' => 'form-control select2', 'placeholder' => 'Select Study Mode']) !!}
                                                    </div>
                                                @endif
                                                @if(isAdmitted(get_my_info()))
                                                    <div class="form-group col-md-4" {{ $errors->has('marital_status')? 'has-error' : ''  }}>
                                                        <label for="marital_status">Marital Status</label>
                                                        {!! Form::select('marital_status', maritalStatus(), get_my_info('marital_status'),
                                                        ['class' => 'form-control select2', 'placeholder' => 'Select Marital Status']) !!}
                                                    </div>
                                                    <div class="form-group col-md-4" {{ $errors->has('has_chronic_disease')? 'has-error' : ''  }}>
                                                        <label for="has_chronic_disease">Has Chronic Disease</label>
                                                        {!! Form::select('has_chronic_disease', chronicDisease(), get_my_info('has_chronic_disease'),
                                                        ['class' => 'form-control select2', 'placeholder' => 'Select Disease']) !!}
                                                    </div>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                {{--// Past Academics--}}
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion"
                                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Past Academics
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel"
                                         aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="examiner">Examiner (Senior 6) *</label>
                                                    @if(admission())
                                                        {!! Form::hidden('examiner', get_my_info('examiner', true)) !!}
                                                    @endif
                                                    <select @if(!admission()) name="examiner" @endif id="examiner"
                                                            class="form-control" required
                                                            {{ admission()? 'disabled' : '' }}>
                                                        <option value="">Select Examiner</option>
                                                        @foreach(\App\Rp\Examiners::all() as $examiner )
                                                            <option value="{{ $examiner->id  }}" {{ $examiner->id == old('examiner') || get_my_info('examiner', true) == $examiner->id ? 'selected' : '' }} >{{ $examiner->name  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    @php
                                                        $s = strtotime(date('Y-m-d', strtotime('last day of december last year')));
                                                        $i = strtotime(date('Y-m-d', strtotime('today')));
                                                        $k = $i - $s;
                                                    @endphp
                                                    <label for="graduation_year">Graduation Year(Senior 6) *</label>
                                                    <input type="text" class="form-control" name="graduation_year"
                                                           value="{{ old('graduation_year')?:get_my_info('graduation_year') }}"
                                                           {{ admission() && get_my_info('graduation_year') != '0000-' ? 'readonly' : '' }}
                                                           id="graduation_year" required>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="index_numberindex_number">Index Number (Senior 6)
                                                        *</label>
                                                    <input type="text" class="form-control" name="index_number"
                                                           value="{{ old('index_number')?:get_my_info('index_number') }}"
                                                           id="index_number"
                                                           @if(!admission())
                                                           data-token="{{ csrf_token() }}"
                                                           data-url="{{ route('student.indexing') }}"
                                                           @endif
                                                           required {{ admission()? 'readonly' : '' }}>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="school_attended">School Attended (Senior 6) *</label>
                                                    <input type="text" class="form-control" id="school_attended"
                                                           value="{{ old('school_attended')?:get_my_info('school_attended') }}"
                                                           name="school_attended"
                                                           required {{ admission()? 'readonly' : '' }}>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="option_offered">Option Offered (Senior 6) *</label>
                                                    <select name="option_offered" id="option_offered"
                                                            class="form-control"
                                                            {{ admission()? 'disabled' : '' }}>
                                                        <option value="">Select Option Offered</option>
                                                        @foreach(\App\Rp\OptionOffered::all() as $item )
                                                            <option value="{{ $item->id  }}"
                                                                    {{ $item->id == old('option_offered') || $item->id == get_my_info('option_offered', true) ? 'selected' : '' }}
                                                            >{{ $item->option_name  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="aggregates_obtained">Aggregates Obtained (Senior 6)
                                                        *</label>
                                                    <input type="text" class="form-control" name="aggregates_obtained"
                                                           value="{{ old('aggregates_obtained')?:get_my_info('aggregates_obtained') }}"
                                                           id="aggregates_obtained"
                                                           required {{ admission()? 'readonly' : '' }}>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                {{--@if(!admission() && !applied())--}}
                                    {{--// Attachments--}}
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseThree" aria-expanded="false"
                                                   aria-controls="collapseThree">
                                                    Attachments
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel"
                                             aria-labelledby="headingThree">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="photo">Photo *</label>
                                                        @if(get_my_info('photo'))
                                                            <div style="margin-bottom: 5px;">
                                                                <img src="{{ regGetStudentPhoto(get_my_info('photo')) }}"
                                                                     style="width:100px">
                                                            </div>
                                                        @endif
                                                        <input type="file" name="photo" id="photo" class="filer_input">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="certificate">Diploma / Certificate *</label>
                                                        <input type="file" name="scan_of_diploma_or_certificate"
                                                               id="certificate" class="filer_input">
                                                        <div style="margin-top: 5px;">
                                                            @if(json_decode(get_my_info('scan_of_diploma_or_certificate')))
                                                                @foreach(json_decode(get_my_info('scan_of_diploma_or_certificate')) as $file)
                                                                    <a href="{{ Storage::disk('public')->url($file->download_link) ? regGetStudentFile($file->download_link) : '' }}"
                                                                       target="_blank">
                                                                        {{ $file->original_name ?: '' }}
                                                                    </a>
                                                                    <br/>
                                                                @endforeach
                                                            @else
                                                                @if(get_my_info('scan_of_diploma_or_certificate'))
                                                                    <a href="{{ regGetStudentFile(get_my_info('scan_of_diploma_or_certificate')) }}"
                                                                       target="_blank">
                                                                        Download
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="nationID_photo">National ID / Passport *</label>
                                                        <input type="file" name="scan_national_id_passport"
                                                               id="nationID_photo"
                                                               class="filer_input">
                                                        <div style="margin-top: 5px;">
                                                            @if(json_decode(get_my_info('scan_national_id_passport')))
                                                                @foreach(json_decode(get_my_info('scan_national_id_passport')) as $file)
                                                                    <a href="{{ Storage::disk('public')->url($file->download_link) ?: '' }}"
                                                                       target="_blank">
                                                                        {{ $file->original_name ?: '' }}
                                                                    </a>
                                                                    <br/>
                                                                @endforeach
                                                            @else
                                                                @if(get_my_info('scan_national_id_passport'))
                                                                    <a href="{{ regGetStudentFile(get_my_info('scan_national_id_passport')) }}"
                                                                       target="_blank">
                                                                        Download
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {{--@endif--}}

                                <div class="clearfix">&nbsp;</div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">
                                            @if(applied())
                                                Update
                                            @else
                                                {{ __('messages.student.register.btn') }}
                                            @endif
                                        </button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" onclick="topFunction()" id="myBtn" title="Go to top">Top
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        @else
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" method="POST"
                          action="{{ applied(get_my_info()) ? route('student.register_update') : route('student.register_applicant') }}"
                          id="register" enctype="multipart/form-data">
                        @include('flash_msg.flash-message')
                        @if ($errors->any())
                            <div class="alert alert-danger text-white">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            {{ csrf_field() }}
                            @if(applied(get_my_info()))
                                {!! Form::hidden('id', get_my_info('id')) !!}
                            @endif
                            {{--// Primary Information--}}
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            Primary Information
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-md-6" {{ $errors->has('first_name')? 'has-error' : ''  }}>
                                                <label for="first_name">Firstname * </label>
                                                <input type="text" name="first_name"
                                                       value="{{ old('first_name')?:splitNameFomStudent() }}"
                                                       class="form-control"
                                                       {{ /*admission()?*/ 'readonly'/* : ''*/ }} required>
                                            </div>
                                            <div class="form-group col-md-6 {{ $errors->has('other_names')? 'has-error' : ''  }}">
                                                <label for="lastnames">Other Names *</label>
                                                <input type="text" name="other_names"
                                                       value="{{ old("other_names")?:splitNameFomStudent(2)  }}"
                                                       class="form-control"
                                                       {{ /*admission()? */'readonly' /*: ''*/ }} required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6" {{ $errors->has('email')? 'has-error' : ''  }}>
                                                <label for="email">E-Mail * </label>
                                                <input type="text" name="email"
                                                       value="{{ old('email')?: auth()->guard('student')->user()->email }}"
                                                       {{ /*admission()? */'readonly'/* : ''*/ }}
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-6 {{ $errors->has('phone')? 'has-error' : ''  }}">
                                                <label for="phone">Telephone *</label>
                                                <input type="text" name="phone"
                                                       value="{{ old("phone")?: ltrim(get_my_info('phone'), '25')  }}"
                                                       data-container="body"
                                                       data-toggle="popover" title="Allowed Telephone Number Format"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                       class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6" {{ $errors->has('gender')? 'has-error' : ''  }}>
                                                <label for="gender">Gender * </label>
                                                <select name="gender" id="gender"
                                                        class="form-control"
                                                        required>
                                                    <option value="" selected disabled>Select Gender</option>
                                                    <option value="male" {{ old("gender") == 'male' || get_my_info("gender") == 'male' ? 'selected' : '' }} >
                                                        Male
                                                    </option>
                                                    <option value="female" {{ old("gender") == 'female' || get_my_info('female') == 'female' ? 'selected' : '' }} >
                                                        Female
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 {{ $errors->has('dob')? 'has-error' : ''  }}">
                                                <label for="phone">Date of Birth *</label>
                                                <input type="text" name="dob"
                                                       value="{{ old("dob")?:get_my_info('dob')  }}"
                                                       class="form-control datepicker"
                                                       required {{ true? 'readonly' : '' }}>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6" {{ $errors->has('ubudehe')? 'has-error' : ''  }}>
                                                <label for="ubudehe">Ubudehe * </label>
                                                <select name="ubudehe" id="ubudehe" class="form-control"
                                                        required {{ true ? 'readonly' : '' }}>
                                                    <option value="" selected disabled>Select Ubudehe</option>
                                                    <option value="4" {{ old('ubudehe') == '4' || get_my_info('ubudehe', true) == '4' ? 'selected' : '' }} >
                                                        Category
                                                        Four
                                                    </option>
                                                    <option value="3" {{ old('ubudehe') == '3' || get_my_info('ubudehe', true) == '3' ? 'selected' : '' }} >
                                                        Category
                                                        Three
                                                    </option>
                                                    <option value="2" {{ old('ubudehe') == '2' || get_my_info('ubudehe', true) == '2' ? 'selected' : '' }} >
                                                        Category
                                                        Two
                                                    </option>
                                                    <option value="1" {{ old('ubudehe') == '1' || get_my_info('ubudehe', true) == '1' ? 'selected' : '' }} >
                                                        Category
                                                        One
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 {{ $errors->has('year_of_study')? 'has-error' : ''  }}">
                                                <label for="year_of_study">Year Of Study *</label>
                                                <input type="hidden" name="year_of_study"
                                                       value="{{ getMyYearOfStudy() }}">
                                                <input type="text" id="year_of_study"
                                                       value="{{ ucwords(getMyYearOfStudy(true))." Year" }}"
                                                       readonly
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-4" {{ $errors->has('country')? 'has-error' : ''  }}>
                                                <label for="country">Country * </label>
                                                <select name="country" id="country" required
                                                        class="form-control">
                                                    <option value="" selected disabled>Select Your Country</option>
                                                    @foreach(\App\Rp\Country::where('enabled', 1)->get() as $country)
                                                        <option value="{{ $country->id }}" {{ old('country') == $country->id || get_my_info('country', true) == $country->id ? 'selected' : '' }} >{{ $country->country_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 {{ $errors->has('national_id_number')? 'has-error' : ''  }}">
                                                <label for="national_id">NationalID/PassportID Number *</label>
                                                <input type="text" name="national_id_number"
                                                       value="{{ old("national_id_number")?: get_my_info('national_id_number')  }}"
                                                       title="National ID Number" {{ true ? 'readonly' : '' }}
                                                       data-toggle="popover"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="Please, spaces are not allowed and must be valid."
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4 {{ $errors->has('address')? 'has-error' : ''  }}">
                                                <label for="address">Address</label>
                                                <input type="text" name="address"
                                                       value="{{ old("address")?: get_my_info('address')  }}"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6" {{ $errors->has('province')? 'has-error' : ''  }}>
                                                <label for="province">Province </label>
                                                <select name="province" id="province" class="form-control"
                                                        required {{ true ? 'readonly' : '' }}>
                                                    <option value="" selected disabled>Select Province</option>
                                                    @foreach(getProvince() as $item )
                                                        <option value="{{ $item->Province  }}"
                                                                {{ old('province') == $item->Province || get_my_info('province', true) == $item->Province ? 'selected' : '' }}
                                                        >{{ $item->Province  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6" {{ $errors->has('district')? 'has-error' : ''  }}>
                                                <label for="province">District</label>
                                                <select name="district" id="district" class="form-control"
                                                        required {{ true ? 'readonly' : '' }}>
                                                    @if(old('district') || get_my_info('district'))
                                                        <option value="{{ old('district')?:get_my_info('district') }}"
                                                                selected>{{ old('district')?:get_my_info('district') }}</option>
                                                    @else
                                                        <option value="" selected disabled>Select District</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6" {{ $errors->has('sector')? 'has-error' : ''  }}>
                                                <label for="province">Sector </label>
                                                <select name="sector" id="sector" class="form-control"
                                                        required {{ true ? 'readonly' : '' }}>
                                                    @if(old('sector') || get_my_info('sector'))
                                                        <option value="{{ old('sector')?:get_my_info('sector') }}"
                                                                selected>{{ old('sector')?:get_my_info('sector') }}</option>
                                                    @else
                                                        <option value="" selected disabled>Select Sector</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 {{ $errors->has('cell')? 'has-error' : ''  }}">
                                                <label for="cell">Cell </label>
                                                <select name="cell" id="cell" class="form-control"
                                                        required {{ true ? 'readonly' : '' }}>
                                                    @if(old('cell') || get_my_info('cell'))
                                                        <option value="{{ old('cell')?:get_my_info('cell') }}"
                                                                selected>{{ old('cell')?:get_my_info('cell') }}</option>
                                                    @else
                                                        <option value="" selected disabled>Select Cell</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6" {{ $errors->has('village')? 'has-error' : ''  }}>
                                                <label for="village">Village </label>
                                                <select name="village" id="village" class="form-control"
                                                        required {{ true? 'readonly' : '' }}>
                                                    @if(old('village') || get_my_info('village'))
                                                        <option value="{{ old('village')?:get_my_info('village') }}"
                                                                selected>
                                                            {{ old('village')?:get_my_info('village') }}
                                                        </option>
                                                    @else
                                                        <option value="" selected disabled>Select Village</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 {{ $errors->has('disability')? 'has-error' : ''  }}">
                                                <label for="disability">Disability *</label>
                                                <select name="disability" id="disability" class="form-control"
                                                        required {{ true? 'readonly' : '' }}>
                                                    <option value="" selected disabled>Select Disability</option>
                                                    @foreach(\App\Rp\Disability::all() as $item )
                                                        <option value="{{ $item->id  }}"
                                                                {{ old('disability') == $item->type || get_my_info('disability', true) == $item->type ? 'selected' : '' }}
                                                        >{{ $item->type  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6 {{ $errors->has('your_bank')? 'has-error' : ''  }}">
                                                <label for="bank">Your Bank *</label>
                                                <select name="your_bank" id="bank" class="form-control"
                                                        required>
                                                    <option value="" selected disabled>Select Your Bank</option>
                                                    @foreach(\App\Rp\Bank::all() as $item )
                                                        <option value="{{ $item->id  }}"
                                                                {{ old('your_bank') == $item->id || get_my_info('your_bank', true) == $item->id ? 'selected' : '' }}>
                                                            {{ $item->bank_name  }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6" {{ $errors->has('your_bank_account')? 'has-error' : ''  }}>
                                                <label for="bank_account">Your Bank Account * </label>
                                                <input type="text" name="your_bank_account"
                                                       value="{{ old("your_bank_account")?:get_my_info('your_bank_account')  }}"
                                                       class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-4" {{ $errors->has('father_name')? 'has-error' : ''  }}>
                                                <label for="father_name">Father Names</label>
                                                <input type="text" id="father_name" name="father_name"
                                                       value="{{ old('father_name') ?: get_my_info('father_name') }}"
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4 {{ $errors->has('father_nation_id')? 'has-error' : ''  }}">
                                                <label for="father_nation_id">Father National ID Number</label>
                                                <input type="text" name="father_nation_id"
                                                       value="{{ old("father_nation_id")?: get_my_info('father_nation_id')  }}"
                                                       title="National ID Number"
                                                       data-toggle="popover"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="Please, spaces are not allowed and must be valid."
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4" {{ $errors->has('father_phone')? 'has-error' : ''  }}>
                                                <label for="father_phone">Father Phone </label>
                                                <input type="text" id="father_phone" name="father_phone"
                                                       data-container="body"
                                                       data-toggle="popover" title="Allowed Telephone Number Format"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                       value="{{ old('father_phone')?: get_my_info('father_phone') }}"
                                                       class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-4" {{ $errors->has('mother_name')? 'has-error' : ''  }}>
                                                <label for="mother_name">Mother Names</label>
                                                <input type="text" id="mother_name" name="mother_name"
                                                       value="{{ old('mother_name')?:get_my_info('mother_name') }}"
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4 {{ $errors->has('mother_nation_id')? 'has-error' : ''  }}">
                                                <label for="mother_nation_id">Mother National ID Number</label>
                                                <input type="text" name="mother_nation_id"
                                                       value="{{ old("mother_nation_id")?: get_my_info('mother_nation_id')  }}"
                                                       title="National ID Number"
                                                       data-toggle="popover"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="Please, spaces are not allowed and must be valid."
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4" {{ $errors->has('mother_phone')? 'has-error' : ''  }}>
                                                <label for="mother_phone">Mother Phone </label>
                                                <input type="text" id="mother_phone" name="mother_phone"
                                                       data-container="body"
                                                       data-toggle="popover" title="Allowed Telephone Number Format"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                       value="{{ old('mother_phone')?: get_my_info('mother_phone') }}"
                                                       class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-4" {{ $errors->has('guardian_name')? 'has-error' : ''  }}>
                                                <label for="guardian_name">Guardian Names</label>
                                                <input type="text" id="guardian_name" name="guardian_name"
                                                       value="{{ old('guardian_name')?: get_my_info('guardian_name') }}"
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4 {{ $errors->has('guardian_nation_id')? 'has-error' : ''  }}">
                                                <label for="guardian_nation_id">Guardian National ID Number</label>
                                                <input type="text" name="guardian_nation_id"
                                                       value="{{ old("guardian_nation_id")?: get_my_info('guardian_nation_id')  }}"
                                                       title="National ID Number"
                                                       data-toggle="popover"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="Please, spaces are not allowed and must be valid."
                                                       class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4" {{ $errors->has('guardian_phone')? 'has-error' : ''  }}>
                                                <label for="guardian_phone">Guardian Phone </label>
                                                <input type="text" id="guardian_phone" name="guardian_phone"
                                                       data-container="body"
                                                       data-toggle="popover" title="Allowed Telephone Number Format"
                                                       data-placement="top"
                                                       data-trigger="focus"
                                                       data-content="MTN:078xxxxxxx TIGO:072xxxxxxx  Airtel:073xxxxxxx"
                                                       value="{{ old('guardian_phone')?: get_my_info('guardian_phone') }}"
                                                       class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-4" {{ $errors->has('marital_status')? 'has-error' : ''  }}>
                                                <label for="marital_status">Marital Status</label>
                                                {!! Form::select('marital_status', maritalStatus(), get_my_info('marital_status'),
                                                ['class' => 'form-control select2', 'placeholder' => 'Select Marital Status']) !!}
                                            </div>
                                            <div class="form-group col-md-4" {{ $errors->has('student_category')? 'has-error' : ''  }}>
                                                <label for="student_category">Study Category *</label>
                                                {!! Form::select('student_category', studyCategory(), get_my_info('student_category'),
                                                ['class' => 'form-control select2', 'placeholder' => 'Select Study Mode']) !!}
                                            </div>
                                            <div class="form-group col-md-4" {{ $errors->has('has_chronic_disease')? 'has-error' : ''  }}>
                                                <label for="has_chronic_disease">Has Chronic Disease</label>
                                                {!! Form::select('has_chronic_disease', chronicDisease(), get_my_info('has_chronic_disease'),
                                                ['class' => 'form-control select2', 'placeholder' => 'Select Disease']) !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            {{--// Past Academics--}}
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Past Academics
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="examiner">Examiner *</label>
                                                @if(true)
                                                    {!! Form::hidden('examiner', get_my_info('examiner', true)) !!}
                                                @endif
                                                <select @if(!true) name="examiner" @endif id="examiner"
                                                        class="form-control" required
                                                        {{ true? 'disabled' : '' }}>
                                                    <option value="">Select Examiner</option>
                                                    @foreach(\App\Rp\Examiners::all() as $examiner )
                                                        <option value="{{ $examiner->id  }}" {{ $examiner->id == old('examiner') || get_my_info('examiner', true) == $examiner->id ? 'selected' : '' }} >{{ $examiner->name  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-md-6">
                                                @php
                                                    $s = strtotime(date('Y-m-d', strtotime('last day of december last year')));
                                                    $i = strtotime(date('Y-m-d', strtotime('today')));
                                                    $k = $i - $s;
                                                @endphp
                                                <label for="graduation_year">Graduation Year *</label>
                                                <input type="text" class="form-control" name="graduation_year"
                                                       value="{{ old('graduation_year')?:get_my_info('graduation_year') }}"
                                                       {{ true && get_my_info('graduation_year') != '0000-' ? 'readonly' : '' }}
                                                       id="graduation_year" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="index_numberindex_number">Index Number *</label>
                                                <input type="text" class="form-control" name="index_number"
                                                       value="{{ old('index_number')?:get_my_info('index_number') }}"
                                                       id="index_number"
                                                       @if(!true)
                                                       data-token="{{ csrf_token() }}"
                                                       data-url="{{ route('student.indexing') }}"
                                                       @endif
                                                       required {{ true? 'readonly' : '' }}>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="school_attended">School Attended *</label>
                                                <input type="text" class="form-control" id="school_attended"
                                                       value="{{ old('school_attended')?:get_my_info('school_attended') }}"
                                                       name="school_attended"
                                                       required {{ true? 'readonly' : '' }}>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="option_offered">Option Offered *</label>
                                                @if(true)
                                                    {!! Form::hidden('option_offered', get_my_info('option_offered', true)) !!}
                                                @endif
                                                <select @if(!true)
                                                        name="option_offered"
                                                        @endif id="option_offered"
                                                        class="form-control"
                                                        {{ true? 'disabled' : '' }}>
                                                    <option value="">Select Option Offered</option>
                                                    @foreach(\App\Rp\OptionOffered::all() as $item )
                                                        <option value="{{ $item->id  }}"
                                                                {{ $item->id == old('option_offered') || $item->id == get_my_info('option_offered', true) ? 'selected' : '' }}
                                                        >{{ $item->option_name  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="aggregates_obtained">Aggregates Obtained *</label>
                                                <input type="text" class="form-control" name="aggregates_obtained"
                                                       value="{{ old('aggregates_obtained')?:get_my_info('aggregates_obtained') }}"
                                                       id="aggregates_obtained"
                                                       required {{ true? 'readonly' : '' }}>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            {{--@if(!true && !applied())--}}
                                {{--// Attachments--}}
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-parent="#accordion"
                                               href="#collapseThree" aria-expanded="false"
                                               aria-controls="collapseThree">
                                                Attachments
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel"
                                         aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="photo">Photo *</label>
                                                    @if(get_my_info('photo'))
                                                        <div style="margin-bottom: 5px;">
                                                            <img src="{{ regGetStudentPhoto(get_my_info('photo')) }}"
                                                                 style="width:100px">
                                                        </div>
                                                    @endif
                                                    <input type="file" name="photo" id="photo" class="filer_input">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="certificate">Diploma / Certificate *</label>
                                                    <input type="file" name="scan_of_diploma_or_certificate"
                                                           id="certificate" class="filer_input">
                                                    <div style="margin-top: 5px;">
                                                        @if(json_decode(get_my_info('scan_of_diploma_or_certificate')))
                                                            @foreach(json_decode(get_my_info('scan_of_diploma_or_certificate')) as $file)
                                                                <a href="{{ Storage::disk('public')->url($file->download_link) ? regGetStudentFile($file->download_link) : '' }}"
                                                                   target="_blank">
                                                                    {{ $file->original_name ?: '' }}
                                                                </a>
                                                                <br/>
                                                            @endforeach
                                                        @else
                                                            @if(get_my_info('scan_of_diploma_or_certificate'))
                                                                <a href="{{ regGetStudentFile(get_my_info('scan_of_diploma_or_certificate')) }}"
                                                                   target="_blank">
                                                                    Download
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="nationID_photo">National ID / Passport *</label>
                                                    <input type="file" name="scan_national_id_passport"
                                                           id="nationID_photo"
                                                           class="filer_input">
                                                    <div style="margin-top: 5px;">
                                                        @if(json_decode(get_my_info('scan_national_id_passport')))
                                                            @foreach(json_decode(get_my_info('scan_national_id_passport')) as $file)
                                                                <a href="{{ Storage::disk('public')->url($file->download_link) ?: '' }}"
                                                                   target="_blank">
                                                                    {{ $file->original_name ?: '' }}
                                                                </a>
                                                                <br/>
                                                            @endforeach
                                                        @else
                                                            @if(get_my_info('scan_national_id_passport'))
                                                                <a href="{{ regGetStudentFile(get_my_info('scan_national_id_passport')) }}"
                                                                   target="_blank">
                                                                    Download
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{--@endif--}}

                            <div class="clearfix">&nbsp;</div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        @if(applied())
                                            Update
                                        @else
                                            {{ __('messages.student.register.btn') }}
                                        @endif
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" onclick="topFunction()" id="myBtn" title="Go to top">Top
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    @else
        <div class="alert alert-warning text-white"
             style="background-color: #ce9300 !important;">
            @if(isAdmitted(get_my_info()))
                <p style="color: #000 !important;font-weight: 700 !important;">{!! __('messages.student.payment.notpaid', ['stdid' => get_my_info()]) !!}</p>
            @else
                <p style="color: #000 !important;font-weight: 700 !important;">{!! __('messages.student.payment.notapplied', ['stdid' => get_my_info()]) !!}</p>
            @endif
        </div>
        @if(isAdmitted(get_my_info()))
            @if($sponsored != 0 || $sponsored != false)
                <div class="alert alert-info text-white">
                    <p style="font-weight: 700 !important;">
                        You Been Sponsored by
                        <b>{{ strtoupper(sponsored(get_my_info(), "Registration", true, "sponsor")) }}</b>
                        and covered amount is <b>{{ strtoupper(sponsored(get_my_info(), "Registration", true)) }}%</b>
                        of Registration
                    </p>
                    <p>Go to <a href="{{ route('student.payment.code.form') }}" style="color: #fff;">payments</a>.</p>
                </div>
            @endif
        @endif
    @endif

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <!-- Laravel Javascript Validation -->
    <script src="{{ asset('js/register.js') }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/jquery.filer.min.js') }}"></script>
    <script src="{{ asset('js/jQuery.filer/js/custom.js') }}"></script>
    <script>
        $(function () {
            $(".jFiler-input").attr('data-toggle', "tooltip")
                .attr('data-placement', "top")
                .attr('title', "Allowed formats: JPEG, JPG or PNG");
            $('[data-toggle="popover"]').popover();
            $('[data-toggle="tooltip"]').tooltip();
            $("#examiner").change(function () {
                var e = $(this).val();
                var g = $("#graduation_year").val();
                if ((e == 1 && g == 2017)) {
                    $("#aggregates_obtained, #school_attended").attr("readonly", true);
                    $("#certificate").hide(555);
                }
                else {
                    $("#aggregates_obtained, #school_attended").attr("readonly", false);
                    $("#certificate").show(555).attr("required", true);
                }

            });
        });
    </script>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @if(applied(get_my_info()) == true)
        {!! JsValidator::formRequest('App\Http\Requests\UpdateRegistrationFormRequest', '#register') !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\SubmitRegistrationFormRequest', '#register') !!}
    @endif
@endsection

