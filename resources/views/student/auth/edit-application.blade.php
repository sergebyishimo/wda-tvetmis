@extends('student.layout.auth')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/style.css?'.time())  }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <style>
        .select2 {
            width: 100% !important;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="POST"
                      @if($system)
                      action="{{ route('student.update', seoUrl(auth()->guard('student')->user()->name)) }}"
                      @endif
                      id="register" enctype="multipart/form-data">
                    @include('flash_msg.flash-message')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $applicant->getinfo('id') }}">
                        {{--// Primary Information--}}
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseOne"
                                               aria-expanded="true" aria-controls="collapseOne">
                                                Primary Information
                                            </a>
                                        </h4>
                                    </div>
                                    <div class='col-md-6'>
                                        <a href="{{ url('student/home') }}"
                                           class="btn btn-sm btn-warning pull-right"> <span
                                                    class="glyphicon glyphicon-arrow-left"></span> Go To Dashboard</a></div>

                                </div>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('first_name')? 'has-error' : ''  }}>
                                            <label for="firstname">Firstname * </label>
                                            <input type="text" name="first_name"
                                                   value="{{ old('first_name')?:$applicant->getinfo('first_name') }}"
                                                   class="form-control">
                                        </div>
                                        <div class="form-group col-md-6 {{ $errors->has('other_names')? 'has-error' : ''  }}">
                                            <label for="lastnames">Other Names *</label>
                                            <input type="text" name="other_names"
                                                   value="{{ old("other_names")?:$applicant->getinfo('other_names')  }}"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('email')? 'has-error' : ''  }}>
                                            <label for="email">E-Mail * </label>
                                            <input type="text" name="email"
                                                   value="{{ old('email')?:$applicant->getinfo('email')  }}"
                                                   class="form-control">
                                        </div>
                                        <div class="form-group col-md-6 {{ $errors->has('phone')? 'has-error' : ''  }}">
                                            <label for="phone">Telephone *</label>
                                            <input type="text" name="phone"
                                                   value="{{ old("phone")?:'0'.$applicant->getinfo('phone')  }}"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('gender')? 'has-error' : ''  }}>
                                            <label for="gender">Gender * </label>
                                            <select name="gender" id="gender" class="form-control">
                                                <option value="" selected disabled>Select Gender</option>
                                                <option value="male" {{ old("gender") == 'male' || $applicant->getinfo('gender') == 'male'  ? 'selected' : '' }} >
                                                    Male
                                                </option>
                                                <option value="female" {{ old("gender") == 'female' || $applicant->getinfo('gender') == 'female' ? 'selected' : '' }} >
                                                    Female
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 {{ $errors->has('dob')? 'has-error' : ''  }}">
                                            <label for="phone">Date of Birth *</label>
                                            <input type="text" name="dob"
                                                   value="{{ old("dob")?:$applicant->getinfo('dob')  }}"
                                                   class="form-control {{ $system ? 'datepickerr' : '' }}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('ubudehe')? 'has-error' : ''  }}>
                                            <label for="ubudehe">Ubudehe * </label>
                                            <select name="ubudehe" id="ubudehe" class="form-control">
                                                <option value="" selected disabled>Select Ubudehe</option>
                                                <option value="4" {{ old('ubudehe') == '4' || $applicant->getinfo('ubudehe') == '4'  ? 'selected' : '' }} >
                                                    Category
                                                    Four
                                                </option>
                                                <option value="3" {{ old('ubudehe') == '3' || $applicant->getinfo('ubudehe') == '3' ? 'selected' : '' }} >
                                                    Category
                                                    Three
                                                </option>
                                                <option value="2" {{ old('ubudehe') == '2' || $applicant->getinfo('ubudehe') == '2' ? 'selected' : '' }} >
                                                    Category
                                                    Two
                                                </option>
                                                <option value="1" {{ old('ubudehe') == '1' || $applicant->getinfo('ubudehe') == '1' ? 'selected' : '' }} >
                                                    Category
                                                    One
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 {{ $errors->has('want_student_loan')? 'has-error' : ''  }}">
                                            <label for="loan">Want Student Loan *</label>
                                            <select name="want_student_loan" id="loan" class="form-control">
                                                <option value="1" {{ old('want_student_loan') == '1' || $applicant->getinfo('want_student_loan') == '1' ? 'select' : '' }} >
                                                    YES
                                                </option>
                                                <option value="0" {{ old('want_student_loan') == '0' || $applicant->getinfo('want_student_loan') == '0' ? 'select' : '' }} >
                                                    NO
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('parents_phone')? 'has-error' : ''  }}>
                                            <label for="parent_phone">Parents Phone * </label>
                                            <input type="text" id="parent_phone" name="parents_phone"
                                                   value="{{ old('parents_phone')?: '0'.$applicant->getinfo('parents_phone') }}"
                                                   class="form-control">
                                        </div>
                                        <div class="form-group col-md-6 {{ $errors->has('national_id_number')? 'has-error' : ''  }}">
                                            <label for="national_id">National ID Number *</label>
                                            <input type="text" name="national_id_number"
                                                   value="{{ old("national_id_number")?:$applicant->getinfo('national_id_number')  }}"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('province')? 'has-error' : ''  }}>
                                            <label for="province">Province * </label>
                                            <select name="province" id="province" class="form-control">
                                                <option value="" selected disabled>Select Province</option>
                                                @foreach(getProvince() as $item )
                                                    <option value="{{ $item->Province }}" {{ old('province') == $item->Province || $applicant->getinfo('province') == $item->Province ? 'selected' : '' }} >{{ $item->Province }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6" {{ $errors->has('district')? 'has-error' : ''  }}>
                                            <label for="province">District * </label>
                                            <select name="district" id="district" class="form-control">
                                                @if(old('district') || $update )
                                                    <option value="{{ old('district')?:$applicant->getinfo('district') }}"
                                                            selected>{{ old('district')?:$applicant->getinfo('district') }}</option>
                                                @else
                                                    <option value="" selected disabled>
                                                        Select District
                                                    </option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('sector')? 'has-error' : ''  }}>
                                            <label for="province">Sector * </label>
                                            <select name="sector" id="sector" class="form-control">
                                                @if(old('sector') || $update )
                                                    <option value="{{ old('sector')?:$applicant->getinfo('sector') }}"
                                                            selected>{{ old('sector')?:$applicant->getinfo('sector') }}</option>
                                                @else
                                                    <option value="" selected disabled>
                                                        Select Select
                                                    </option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 {{ $errors->has('cell')? 'has-error' : ''  }}">
                                            <label for="cell">Cell *</label>
                                            <select name="cell" id="cell" class="form-control">
                                                @if(old('cell') || $update )
                                                    <option value="{{ old('cell')?:$applicant->getinfo('cell') }}"
                                                            selected>{{ old('cell')?:$applicant->getinfo('cell') }}</option>
                                                @else
                                                    <option value="" selected disabled>Select Cell</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6" {{ $errors->has('village')? 'has-error' : ''  }}>
                                            <label for="village">Village * </label>
                                            <select name="village" id="village" class="form-control">
                                                @if(old('village') || $update )
                                                    <option value="{{ old('village')?:$applicant->getinfo('village') }}"
                                                            selected>{{ old('village')?:$applicant->getinfo('village') }}</option>
                                                @else
                                                    <option value="" selected disabled>Select Village</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 {{ $errors->has('disability')? 'has-error' : ''  }}">
                                            <label for="disability">Disability *</label>
                                            <select name="disability" id="disability" class="form-control">
                                                <option value="" selected disabled>Select Disability</option>
                                                @foreach(\App\Disability::all() as $item )
                                                    <option value="{{ $item->id  }}" {{ old('disability') == $item->id || $applicant->getinfo('disability') == $item->id ? 'selected' : '' }} >{{ $item->type  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 {{ $errors->has('your_bank')? 'has-error' : ''  }}">
                                            <label for="bank">Your Bank *</label>
                                            <select name="your_bank" id="bank" class="form-control">
                                                <option value="" selected disabled>Select Your Bank</option>
                                                @foreach(\App\Bank::all() as $item )
                                                    <option value="{{ $item->id  }}" {{ old('your_bank') == $item->id || $applicant->getinfo('your_bank') == $item->id ? 'selected' : '' }} >{{ $item->bank_name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6" {{ $errors->has('your_bank_account')? 'has-error' : ''  }}>
                                            <label for="bank_account">Your Bank Account * </label>
                                            <input type="text" name="your_bank_account"
                                                   value="{{ old("your_bank_account")?:$applicant->getinfo('your_bank_account')  }}"
                                                   class="form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        {{--// Past Academics--}}
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Past Academics
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="examiner">Examiner *</label>
                                            <select name="examiner" id="examiner" class="form-control">
                                                <option value="">Select Examiner</option>
                                                @foreach(\App\Examiners::all() as $examiner )
                                                    <option value="{{ $examiner->id  }}" {{ $examiner->id == old('examiner') || $applicant->getinfo('examiner') == $examiner->id ? 'selected' : '' }} >{{ $examiner->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="index_numberindex_number">Index Number *</label>
                                            <input type="text" class="form-control" name="index_number"
                                                   value="{{ old('index_number')?:$applicant->getinfo('index_number') }}"
                                                   id="index_number">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="school_attended">School Attended *</label>
                                            <input type="text" class="form-control" id="school_attended"
                                                   value="{{ old('school_attended')?:$applicant->getinfo('school_attended') }}"
                                                   name="school_attended">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="graduation_year">Graduation Year *</label>
                                            <input type="text" class="form-control datepickerA" name="graduation_year"
                                                   value="{{ old('graduation_year')?:$applicant->getinfo('graduation_year') }}"
                                                   id="graduation_year">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="option_offered">Option Offered *</label>
                                            <select name="option_offered" id="option_offered" class="form-control"
                                            >
                                                <option value="">Select Option Offered</option>
                                                @foreach(\App\OptionOffered::all() as $item )
                                                    <option value="{{ $item->id  }}" {{ $item->id == old('option_offered') || $item->id == $applicant->getinfo('option_offered')  ? 'selected' : '' }}>{{ $item->option_name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="aggregates_obtained">Aggregates Obtained *</label>
                                            <input type="text" class="form-control" name="aggregates_obtained"
                                                   value="{{ old('aggregates_obtained')?:$applicant->getinfo('aggregates_obtained') }}"
                                                   id="aggregates_obtained">
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        @if($system)
                            {{--// Attachments--}}
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Attachments
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="photo">Photo *</label>
                                                <div style="margin-bottom: 5px;">
                                                    <img src="@if( !filter_var($applicant->getinfo('photo'), FILTER_VALIDATE_URL)){{ Voyager::image( $applicant->getinfo('photo') ) }}@else{{ $applicant->getinfo('photo') }}@endif"
                                                         style="width:100px">
                                                </div>
                                                <input type="file" name="photo" id="photo">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="certificate">Diploma / Certificate *</label>
                                                <input type="file" name="scan_of_diploma_or_certificate"
                                                       id="certificate">
                                                <div style="margin-top: 5px;">
                                                    @if(json_decode($applicant->getinfo('scan_of_diploma_or_certificate')))
                                                        @foreach(json_decode($applicant->getinfo('scan_of_diploma_or_certificate')) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
                                                               target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($applicant->getinfo('scan_of_diploma_or_certificate')) }}"
                                                           target="_blank">
                                                            Download
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="nationID_photo">National ID / Passport *</label>
                                                <input type="file" name="scan_national_id_passport" id="nationID_photo">
                                                <div style="margin-top: 5px;">
                                                    @if(json_decode($applicant->getinfo('scan_national_id_passport')))
                                                        @foreach(json_decode($applicant->getinfo('scan_national_id_passport')) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
                                                               target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($applicant->getinfo('scan_national_id_passport')) }}"
                                                           target="_blank">
                                                            Download
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        {{--// Applications Choice--}}
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Application Choices
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <div class="row ">
                                        <div class="form-group col-md-4">
                                            <label for="choice_polytechnic_1">1st Choice Polytechnic *</label>
                                            <select name="first_choice_polytechnic" id="choice_polytechnic_1"
                                                    class="form-control">
                                                <option value="">Select Polytechnic</option>
                                                @foreach(\App\Polytechnic::all() as $polytechnic )
                                                    <option value="{{ $polytechnic->id  }}" {{ $applicant->getinfo('first_choice_polytechnic') == $polytechnic->id || old('first_choice_polytechnic') == $polytechnic->id ? 'selected' : '' }}>{{ $polytechnic->polytechnic  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="choice_department_1">1st Choice Department *</label>
                                            <select name="first_choice_department" id="choice_department_1"
                                                    class="form-control">
                                                <option value="">Select Department</option>
                                                @foreach(\App\Polytechnic::findOrFail($applicant->getinfo('first_choice_polytechnic'))->departments as $item )
                                                    <option value="{{ $item->id  }}" {{ $applicant->getinfo('first_choice_department') == $item->id || old('first_choice_department') == $item->id ? 'selected' : '' }} >{{ $item->department  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="choice_course_1">1st Choice Course *</label>
                                            <select name="first_choice_course" id="choice_course_1" class="form-control"
                                            >
                                                <option value="">Select Course</option>
                                                @foreach(\App\Department::findOrFail($applicant->getinfo('first_choice_department'))->courses as $item )
                                                    <option value="{{ $item->id  }}" {{ $applicant->getinfo('first_choice_course') == $item->id || old('first_choice_course') == $item->id ? 'selected' : ''  }}>{{ $item->choices  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="choice_polytechnic_2">2rd Choice Polytechnic</label>
                                            <select name="second_choice_polytechnic" id="choice_polytechnic_2"
                                                    class="form-control">
                                                <option value="">Select Polytechnic</option>
                                                @if($applicant->getinfo('second_choice_polytechnic'))
                                                    @foreach(\App\Polytechnic::all() as $polytechnic )
                                                        <option value="{{ $polytechnic->id  }}" {{ $applicant->getinfo('second_choice_polytechnic') == $polytechnic->id || old('second_choice_polytechnic') == $polytechnic->id ? 'selected' : ''  }} >{{ $polytechnic->polytechnic  }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="choice_department_2">2rd Choice Department</label>
                                            <select name="second_choice_department" id="choice_department_2"
                                                    class="form-control">
                                                <option value="">Select Department</option>
                                                @if($applicant->getinfo('second_choice_polytechnic'))
                                                    @foreach(\App\Polytechnic::findOrFail($applicant->getinfo('second_choice_polytechnic'))->departments as $item )
                                                        <option value="{{ $item->id  }}" {{ $applicant->getinfo('second_choice_department') == $item->id || old('second_choice_department') == $item->id ? 'selected' : '' }} >{{ $item->department  }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="choice_course_2">2rd Choice Course *</label>
                                            <select name="second_choice_course" id="choice_course_2"
                                                    class="form-control">
                                                <option value="">Select Course</option>
                                                @if($applicant->getinfo('second_choice_department'))
                                                    @foreach(\App\Department::findOrFail($applicant->getinfo('second_choice_department'))->courses as $item )
                                                        <option value="{{ $item->id  }}" {{ $applicant->getinfo('second_choice_course') == $item->id || old('second_choice_course') == $item->id ? 'selected' : '' }} >{{ $item->choices  }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="choice_polytechnic_3">3th Choice Polytechnic</label>
                                            <select name="third_choice_polytechnic" id="choice_polytechnic_3"
                                                    class="form-control">
                                                <option value="">Select Polytechnic</option>
                                                @if($applicant->getinfo('third_choice_polytechnic'))
                                                    @foreach(\App\Polytechnic::all() as $polytechnic )
                                                        <option value="{{ $polytechnic->id  }}"{{ $applicant->getinfo('third_choice_polytechnic') == $polytechnic->id || old('third_choice_polytechnic') == $polytechnic->id ? 'selected' : '' }}>{{ $polytechnic->polytechnic  }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="choice_department_3">3th Choice Department</label>
                                            <select name="third_choice_department" id="choice_department_3"
                                                    class="form-control">
                                                <option value="">Select Department</option>
                                                @if($applicant->getinfo('third_choice_polytechnic'))
                                                    @foreach(\App\Polytechnic::findOrFail($applicant->getinfo('third_choice_polytechnic'))->departments as $item )
                                                        <option value="{{ $item->id  }}"{{ $applicant->getinfo('third_choice_department') == $item->id || old('third_choice_department') == $item->id ? 'selected' : '' }} >{{ $item->department  }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="choice_course_3">3th Choice Course *</label>
                                            <select name="third_choice_course" id="choice_course_3"
                                                    class="form-control">
                                                <option value="">Select Course</option>
                                                @if($applicant->getinfo('third_choice_department'))
                                                    @foreach(\App\Department::findOrFail($applicant->getinfo('third_choice_department'))->courses as $item )
                                                        <option value="{{ $item->id  }}"{{ $applicant->getinfo('third_choice_course') == $item->id || old('third_choice_course') == $item->id ? 'selected' : ''  }}>{{ $item->choices  }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary" {{ $system == false ? "disabled" : "" }} >
                                    Update Your Information
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    @if($system == false)
        <script>
            var form = document.getElementById("register");
            var elements = form.elements;
            for (var i = 0, len = elements.length; i < len; ++i) {
                elements[i].readOnly = true;
            }
            $("* select ").attr("disabled", "true");
            $("* input[type=file] ").attr("disabled", "true");
        </script>
    @endif
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UpdateApplicantData', '#register'); !!}
    <script src="{{ asset('js/register.js?'.time()) }}"></script>
    <script>
        $(function () {
            $(".datepickerr").datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                assumeNearbyYear: true,
                todayHighlight: true,
            });
        });
    </script>
@endsection
