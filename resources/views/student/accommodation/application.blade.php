@extends('student.layout.auth')

@section('content')

    @if ($check > 0)

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Applications</h5>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>File</th>
                                        <th>Academic Year</th>
                                        <th>Date Submitted</th>
                                        <th>Decision</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($applications as $app)
                                        <tr>
                                            <td><a href="/storage/{{ $app->file }}" target="_blank">Download File</a></td>
                                            <td>{{ $app->academic_year }}</td>
                                            <td>{{ date('d/m/Y', strtotime($app->created_at)) }}</td>
                                            <th>{{ $app->decision == null ? 'Not Yet' : $app->decision  }}</th>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @else
        You need to be registered to view this page
    @endif

@endsection