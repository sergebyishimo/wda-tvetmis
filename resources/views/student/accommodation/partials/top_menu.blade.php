<div class="btn-group btn-group-md"
     role="group"
     aria-label="Payment Sub Menu" style="margin-bottom: 10px;">
    <a href="{{ route('student.accommodation.index') }}"
       class="btn btn-info {{ activeMenu('student.accommodation.index') }}">Apply</a>
    <a href="{{ route('student.accommodation.show', 0) }}"
       class="btn btn-danger {{ activeMenu('student.accommodation.show') }}">Applications</a>
</div>