@extends('student.layout.auth')

@section('content')

    @if ($check > 0)
    <div class="row">
        <div class="col-md-8">
            @include('feedback.feedback')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Regulations</h5>
                </div>
                <div class="panel-body">
                    <ol>
                        <li>Accommodation is granted for a period of one academic year. Once started, it should not be terminated before the end of the academic year;</li>
                        <li>Students are expected to take good care of the room and furniture assigned to them;</li>
                        <li>It is strictly prohibited for an accommodated student to move furniture or other materials from one room to another;</li>
                        <li>A student who causes damage to the room and equipment will be liable to repair them;</li>
                        <li>Two students are not allowed to sleep on a single bed.  Students found guilty of acting contrarily to this rule will be punished; </li>
                        <li>Accommodated students must not entertain visitors of the opposite sex in hostels;</li>
                        <li>Cooking (frying, roasting, baking, boiling or warming) by use of electricity is strictly prohibited in student hostels;</li>
                        <li>Unauthorized use of facilities, equipment or installations belonging to the hostel shall lead to the suspension from studies for a period of two years;</li>
                        <li>At the end of the academic year, students will hand over equipment assigned to them and must vacate the hostel with all their belongings;</li>
                        <li>Each accommodated student has the obligation to report to competent authorities any infringement of accommodation regulations which come to his/her notice;</li>
                        <li>By signing this contract, a student accepts to comply with the rules indicated above and to be punished if those rules are violated. Please find in detail disciplinary procedures found in the Student Regulations and Code of Conduct</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Accommodation Application</h5>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form method="POST" action="{{ route('student.accommodation.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Academic Year</label>
                                    {!! Form::select('academic_year', academicYear(), null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">File</label>
                                    <input type="file" name="file" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <br>
                                <button type="submit" class="btn btn-primary btn-block mt-2">Apply</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
        You need to be registered to view this page
    @endif
    

@endsection