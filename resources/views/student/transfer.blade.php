@extends('student.swift')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            {{-- <h2>ADVANCED FORM ELEMENTS</h2>
            <small class="text-muted">Welcome to Swift application</small> --}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header bg-blue" style="margin:0px;padding: 0;padding: 10px;">
                        <h2 style="text-transform:none">Request a Transfer</h2>
                    </div>
                    <div class="card-body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12"> <b>Suppoting Letter</b>
                                <div class="input-group">
                                    <div class="">
                                        <input type="file" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <b>College *</b>
                                <div class="input-group">
                                    <div class="form-line">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Select College</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <b>Department *</b>
                                <div class="input-group">
                                    <div class="form-line">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Select Department</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <b>Course *</b>
                                <div class="input-group">
                                    <div class="form-line">
                                        <select class="form-control" style="height: 36px;"> 
                                            <option>Select Course</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-raised g-bg-blush2 btn-block">Submit</button>
                <br><br>
            </div>
        </div>
    </div>
</section>

@endsection