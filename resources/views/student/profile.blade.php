@extends('student.swift')

@section('content')
<section class="content profile-page">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-md-4">
                    <h2>Students Profile</h2>   
                    <small class="text-muted">Welcome to Swift application</small>
                </div>
                <div class="col-md-8">
                    @include('flash_msg.flash-message')
                </div>
            </div>
            
        </div>        
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class=" card">
                    {{-- <img src="/swift/images/image-1.jpg" class="img-fluid" alt=""> --}}
                    <img src="/storage/{{ get_my_info('photo') }}" class="img-fluid" alt="">
                </div>
                <div class="card">
                    <div class="header bg-blue" style="margin:0px;padding: 0;padding: 10px;">
                        <h2 style="text-transform:capitalize"><i class="fa fa-info-circle"></i> Your Information</h2>
                    </div>
                    <div class="body">
                        <strong>Name</strong>
                        <p>{{ ucwords(get_my_info('first_name')) . ' ' . ucwords(get_my_info('other_names')) }}</p>
                        <strong>Colllege</strong>
                        <p>{{ ucwords(get_my_info('college_id')) }}</p>
                        <strong>Department</strong>
                        <p>{{ ucwords(get_my_info('department_id')) }}</p>
                        <strong>Department</strong>
                        <p>{{ ucwords(get_my_info('course_id')) }}</p>
                        <strong>Email ID</strong>
                        <p>{{ ucwords(get_my_info('email')) }}</p>
                        <strong>Phone</strong>
                        <p>{{ ucwords(get_my_info('phone')) }}</p>
                        <hr>
                        <strong>Address</strong>
                        <address>{{ ucwords(get_my_info('province')) . ' - ' . ucwords(get_my_info('district')) . ' - ' . ucwords(get_my_info('sector')) }}</address>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body"> 
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#report">Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#timeline">Notifications &nbsp; <span class="pull-right label-danger label">New</span></a></li>
                        </ul>
                        
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane in active" id="report">
                                <div class="wrap-reset">
                                    <div class="mypost-list">
                                        <div class="post-box">
                                            
                                                @if(system_enabled() !== true && ! applied(auth()->user()->id) )
                                                <div class="alert alert-warning" style="background-color: #FFC107 !important;">
                                                    @if(strpos('closed', system_enabled()) === true )
                                                        <h6 style="color: #000 !important;font-weight: 700 !important;">{!! __('messages.system.closed') !!}</h6>
                                                    @else
                                                    @endif
                                                    @if(system_enabled())
                                                        <h6 style="color: #000 !important;font-weight: 700 !important;">{{ system_enabled() }}</h6>
                                                    @endif
                                                </div>
                                            @endif
                                            @if( auth()->user()->payments)
                                                @if(! applied(auth()->user()->id) )
                                                    @if(system_enabled() === true)
                                                        <p>{!! __('messages.student.apply.new') !!}</p>
                                                        <a href="{{ route('student.registering') }}"
                                                        class="btn btn-primary">{!! __('messages.student.apply.btn.new') !!}</a>
                                                    @endif
                                                @else
                                                    <div class="alert alert-info text-white">
                                                        <p >{!! __('messages.student.apply.edit') !!}</p>
                                                        <a href="{{ url('/student/info/'.seoUrl(auth()->user()->name)) }}"
                                                        class="btn btn-sm btn-primary">{!! __('messages.student.apply.btn.edit') !!}</a>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="alert alert-warning text-white" style="background-color: #FFC107 !important;">
                                                    <p style="color: #000 !important;">{!! __('messages.student.payment.notpaid', ['stdid' => get_my_info()]) !!}</p>
                                                </div>
                                            @endif
                                            
                                        </div>
                                        <hr style="margin: 0px;margin-bottom:5px">
                                        <div class="row" style="margin-bottom:5px">
                                            <div class="col-md-10">
                                                <h4 style="margin-bottom:0px;margin-top:5px"> Your Payment Details</h4>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-raised g-bg-blush2" style="margin-top:0px;margin-bottom:0px;padding:10px 20px;box-shadow:0px">Print</button>
                                            </div>
                                        </div>
                                        
                                        
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>Operated Date</th>
                                                    <td>2018-06-22</td>
                                                </tr>
                                                <tr>
                                                    <th>Payment Mode</th>
                                                    <td>MOMO</td>
                                                </tr>
                                                <tr>
                                                    <th>Bank Name</th>
                                                    <td>Equity</td>
                                                </tr>
                                                <tr>
                                                    <th>Bank Account</th>
                                                    <td>39289232</td>
                                                </tr>
                                                <tr>
                                                    <th>Bank Slip N<sup>o</sup></th>
                                                    <td>32892839</td>
                                                </tr>
                                                <tr>
                                                    <th>Amount Paid</th>
                                                    <td>40000 RWF</td>
                                                </tr>
                                                <tr>
                                                    <th>Payment Status</th>
                                                    <td>Yes</td>
                                                </tr>
                                                <tr>
                                                    <th>Transaction ID</th>
                                                    <td>2191287812</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="timeline">
                                <div class="alert alert-danger alert-block">
                                    <span style="font-size:14px">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta maxime, libero rem obcaecati illum perspiciatis incidunt tempore? Non, eaque deserunt, sint repellat repudiandae consectetur iure, tenetur vero voluptates accusamus quae.</span>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
