<?php
/**
 * Created by PhpStorm.
 * User: hirwaf
 * Date: 5/12/18
 * Time: 11:48 PM
 */

return [
    'dimmer' => [
        //School
        'school' => 'school',
        'school_link_text' => 'View all school',
        'school_text' => 'You have :count :string in your database. Click on button below to view all schools.',
        //Student
        'student' => 'student',
        'student_link_text' => 'View all student',
        'student_text' => 'You have :count :string in your database. Click on button below to view all student.',
        //Stuff
        'stuff' => 'staff',
        'stuff_link_text' => 'View all staff',
        'stuff_text' => 'You have :count :string in your database. Click on button below to view all staff.'
    ],
    'school' => [
        'nav' > [
            'logout' => 'Logout',
            'settings' => 'School Settings',
            'change_password'=> 'Change Password'
        ]
    ]
];