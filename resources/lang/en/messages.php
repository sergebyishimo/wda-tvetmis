<?php
/**
 * Created by PhpStorm.
 * User: hirwaf
 * Date: 4/23/18
 * Time: 12:32 PM
 */
return [
    'student' => [
        'register' => [
            'success' => "Your account has been successfully, created. Please check your email your email for instructions on how to pay the registration and how to proceed. Please note that there is a further step of registration after payment of the registration fee",
            'fail' => '',
            'btn' => 'Submit Registration'
        ],
        'admission' => [
            'btn' => 'Admitted'
        ],
        'payment' => [
            'paid' => 'Please click here to proceed with your registration',
            'notapplied' => 'Next Steps: Submitting an Application<br>
                             <br>You have not paid or your payment for the application fee has not been verified.
                             <br>Please check your email for the instructions on how to make payment. You will only be able to complete the the application process after you have paid the application fee.
                             <br><br>Don\'t forget to pay using your ID :stdid',
            'notpaid' => 'Next Steps: Submitting Registration Information<br>
                             <br>You have not paid or your payment for registration fees has not been verified.
                             <br>Please check your email for the instructions on how to make payment. You will only be able to complete the registration after you have paid the registration fee.
                             <br><br>Don\'t forget to pay using your ID :stdid',
            'currency' => 'RWF',
            'details' => [
                'title' => 'Your Payment Details',
                'amount' => 'Amount Paid',
                'bank' => [
                    'slip' => 'Bank Slip N<sup>o</sup>',
                    'account' => 'Bank Account',
                    'name' => 'Bank Name'
                ],
                'text' => 'Payments Details',
                'admission' => 'Admission Payments Details',
                'operator' => 'Operator',
                'mode' => 'Payment Mode',
                'status' => 'Payment Status',
                'trans' => 'Transaction ID',
                'created' => 'Operated Date'
            ],
            'btn' => [
                'print' => 'Print'
            ]
        ],
        'apply' => [
            'edit' => 'Please click here to view your registration information',
            'new' => 'Please click here to proceed with registration',
            'btn' => [
                'new' => 'Proceed',
                'edit' => 'Review your information'
            ]
        ],
        'email' => [
            'applied' => 'Your application has been successfully received by Rwanda Polytechnic. Further steps shall be communicated.'
        ]
    ],
    'system' => [
        'dashboard' => [
            'title' => 'Dashboard',
            'list' => [
                'title' => 'Your Information',
                'btn' => 'Print'
            ]
        ],
        'closed' => 'The system is not enabled yet',
    ],
    'payment_sms' => "Hello :student RP received your payment :category of :amount on :date"
];