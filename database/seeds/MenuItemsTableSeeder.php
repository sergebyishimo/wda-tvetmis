<?php

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_items')->delete();
        
        \DB::table('menu_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'menu_id' => 1,
                'title' => 'Dashboard',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-boat',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 1,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 18:13:41',
                'route' => 'voyager.dashboard',
                'parameters' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'menu_id' => 1,
                'title' => 'Media',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-images',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 6,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-21 13:30:21',
                'route' => 'voyager.media.index',
                'parameters' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'menu_id' => 1,
                'title' => 'Users',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-person',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 5,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-21 13:30:21',
                'route' => 'voyager.users.index',
                'parameters' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'menu_id' => 1,
                'title' => 'Roles',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-lock',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 4,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-21 13:30:21',
                'route' => 'voyager.roles.index',
                'parameters' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'menu_id' => 1,
                'title' => 'Tools',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tools',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 7,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-21 13:30:21',
                'route' => NULL,
                'parameters' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'menu_id' => 1,
                'title' => 'Menu Builder',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 1,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 19:44:19',
                'route' => 'voyager.menus.index',
                'parameters' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'menu_id' => 1,
                'title' => 'Database',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-data',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 2,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 19:44:19',
                'route' => 'voyager.database.index',
                'parameters' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'menu_id' => 1,
                'title' => 'Compass',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-compass',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 3,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 19:44:19',
                'route' => 'voyager.compass.index',
                'parameters' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'menu_id' => 1,
                'title' => 'BREAD',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-bread',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 4,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 19:44:19',
                'route' => 'voyager.bread.index',
                'parameters' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'menu_id' => 1,
                'title' => 'Settings',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-settings',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 8,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-21 13:30:21',
                'route' => 'voyager.settings.index',
                'parameters' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'menu_id' => 1,
                'title' => 'Schools',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-bookmark',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 3,
                'created_at' => '2018-05-12 19:42:47',
                'updated_at' => '2018-05-21 13:30:21',
                'route' => 'voyager.schools.index',
                'parameters' => 'null',
            ),
            11 => 
            array (
                'id' => 12,
                'menu_id' => 1,
                'title' => 'Curriculum Management',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-pen',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2018-05-21 08:49:17',
                'updated_at' => '2018-05-21 08:50:01',
                'route' => NULL,
                'parameters' => '',
            ),
            12 => 
            array (
                'id' => 13,
                'menu_id' => 1,
                'title' => 'Sectors',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-magnet',
                'color' => NULL,
                'parent_id' => 12,
                'order' => 1,
                'created_at' => '2018-05-21 08:55:42',
                'updated_at' => '2018-05-21 09:04:23',
                'route' => 'voyager.sectors.index',
                'parameters' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'menu_id' => 1,
                'title' => 'Sub Sectors',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-belt',
                'color' => NULL,
                'parent_id' => 12,
                'order' => 2,
                'created_at' => '2018-05-21 08:59:31',
                'updated_at' => '2018-05-21 09:04:33',
                'route' => 'voyager.sub-sectors.index',
                'parameters' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'menu_id' => 1,
                'title' => 'Rtqf',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-boat',
                'color' => '#000000',
                'parent_id' => 12,
                'order' => 3,
                'created_at' => '2018-05-21 09:08:48',
                'updated_at' => '2018-05-21 09:44:23',
                'route' => 'voyager.rtqfs.index',
                'parameters' => 'null',
            ),
            15 => 
            array (
                'id' => 16,
                'menu_id' => 1,
                'title' => 'Qualifications',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-study',
                'color' => NULL,
                'parent_id' => 12,
                'order' => 4,
                'created_at' => '2018-05-21 09:25:52',
                'updated_at' => '2018-05-21 09:43:56',
                'route' => 'voyager.qualifications.index',
                'parameters' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'menu_id' => 1,
                'title' => 'Qualification Status',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-anchor',
                'color' => '#000000',
                'parent_id' => 12,
                'order' => 7,
                'created_at' => '2018-05-21 09:40:22',
                'updated_at' => '2018-05-21 13:30:25',
                'route' => 'voyager.qualification-statuses.index',
                'parameters' => 'null',
            ),
            17 => 
            array (
                'id' => 18,
                'menu_id' => 1,
                'title' => 'Modules',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-book',
                'color' => NULL,
                'parent_id' => 12,
                'order' => 6,
                'created_at' => '2018-05-21 10:04:17',
                'updated_at' => '2018-05-21 13:30:25',
                'route' => 'voyager.modules.index',
                'parameters' => NULL,
            ),
            18 => 
            array (
                'id' => 20,
                'menu_id' => 1,
                'title' => 'Module Types',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-plug',
                'color' => NULL,
                'parent_id' => 12,
                'order' => 5,
                'created_at' => '2018-05-21 13:29:54',
                'updated_at' => '2018-05-21 13:30:25',
                'route' => 'voyager.module-types.index',
                'parameters' => NULL,
            ),
        ));
        
        
    }
}