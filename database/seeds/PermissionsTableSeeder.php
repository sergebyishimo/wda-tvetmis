<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 18:13:41',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 18:13:41',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 18:13:41',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 18:13:41',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2018-05-12 18:13:41',
                'updated_at' => '2018-05-12 18:13:41',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2018-05-12 18:13:42',
                'updated_at' => '2018-05-12 18:13:42',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_schools',
                'table_name' => 'schools',
                'created_at' => '2018-05-12 19:42:47',
                'updated_at' => '2018-05-12 19:42:47',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_schools',
                'table_name' => 'schools',
                'created_at' => '2018-05-12 19:42:47',
                'updated_at' => '2018-05-12 19:42:47',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_schools',
                'table_name' => 'schools',
                'created_at' => '2018-05-12 19:42:47',
                'updated_at' => '2018-05-12 19:42:47',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_schools',
                'table_name' => 'schools',
                'created_at' => '2018-05-12 19:42:47',
                'updated_at' => '2018-05-12 19:42:47',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_schools',
                'table_name' => 'schools',
                'created_at' => '2018-05-12 19:42:47',
                'updated_at' => '2018-05-12 19:42:47',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_sectors',
                'table_name' => 'sectors',
                'created_at' => '2018-05-21 08:55:42',
                'updated_at' => '2018-05-21 08:55:42',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_sectors',
                'table_name' => 'sectors',
                'created_at' => '2018-05-21 08:55:42',
                'updated_at' => '2018-05-21 08:55:42',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_sectors',
                'table_name' => 'sectors',
                'created_at' => '2018-05-21 08:55:42',
                'updated_at' => '2018-05-21 08:55:42',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_sectors',
                'table_name' => 'sectors',
                'created_at' => '2018-05-21 08:55:42',
                'updated_at' => '2018-05-21 08:55:42',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_sectors',
                'table_name' => 'sectors',
                'created_at' => '2018-05-21 08:55:42',
                'updated_at' => '2018-05-21 08:55:42',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_sub_sectors',
                'table_name' => 'sub_sectors',
                'created_at' => '2018-05-21 08:59:31',
                'updated_at' => '2018-05-21 08:59:31',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_sub_sectors',
                'table_name' => 'sub_sectors',
                'created_at' => '2018-05-21 08:59:31',
                'updated_at' => '2018-05-21 08:59:31',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_sub_sectors',
                'table_name' => 'sub_sectors',
                'created_at' => '2018-05-21 08:59:31',
                'updated_at' => '2018-05-21 08:59:31',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_sub_sectors',
                'table_name' => 'sub_sectors',
                'created_at' => '2018-05-21 08:59:31',
                'updated_at' => '2018-05-21 08:59:31',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_sub_sectors',
                'table_name' => 'sub_sectors',
                'created_at' => '2018-05-21 08:59:31',
                'updated_at' => '2018-05-21 08:59:31',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'browse_rtqfs',
                'table_name' => 'rtqfs',
                'created_at' => '2018-05-21 09:08:48',
                'updated_at' => '2018-05-21 09:08:48',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'read_rtqfs',
                'table_name' => 'rtqfs',
                'created_at' => '2018-05-21 09:08:48',
                'updated_at' => '2018-05-21 09:08:48',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'edit_rtqfs',
                'table_name' => 'rtqfs',
                'created_at' => '2018-05-21 09:08:48',
                'updated_at' => '2018-05-21 09:08:48',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'add_rtqfs',
                'table_name' => 'rtqfs',
                'created_at' => '2018-05-21 09:08:48',
                'updated_at' => '2018-05-21 09:08:48',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'delete_rtqfs',
                'table_name' => 'rtqfs',
                'created_at' => '2018-05-21 09:08:48',
                'updated_at' => '2018-05-21 09:08:48',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'browse_qualifications',
                'table_name' => 'qualifications',
                'created_at' => '2018-05-21 09:25:52',
                'updated_at' => '2018-05-21 09:25:52',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'read_qualifications',
                'table_name' => 'qualifications',
                'created_at' => '2018-05-21 09:25:52',
                'updated_at' => '2018-05-21 09:25:52',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'edit_qualifications',
                'table_name' => 'qualifications',
                'created_at' => '2018-05-21 09:25:52',
                'updated_at' => '2018-05-21 09:25:52',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'add_qualifications',
                'table_name' => 'qualifications',
                'created_at' => '2018-05-21 09:25:52',
                'updated_at' => '2018-05-21 09:25:52',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'delete_qualifications',
                'table_name' => 'qualifications',
                'created_at' => '2018-05-21 09:25:52',
                'updated_at' => '2018-05-21 09:25:52',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'browse_qualification_statuses',
                'table_name' => 'qualification_statuses',
                'created_at' => '2018-05-21 09:40:22',
                'updated_at' => '2018-05-21 09:40:22',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'read_qualification_statuses',
                'table_name' => 'qualification_statuses',
                'created_at' => '2018-05-21 09:40:22',
                'updated_at' => '2018-05-21 09:40:22',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'edit_qualification_statuses',
                'table_name' => 'qualification_statuses',
                'created_at' => '2018-05-21 09:40:22',
                'updated_at' => '2018-05-21 09:40:22',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'add_qualification_statuses',
                'table_name' => 'qualification_statuses',
                'created_at' => '2018-05-21 09:40:22',
                'updated_at' => '2018-05-21 09:40:22',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'delete_qualification_statuses',
                'table_name' => 'qualification_statuses',
                'created_at' => '2018-05-21 09:40:22',
                'updated_at' => '2018-05-21 09:40:22',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'browse_modules',
                'table_name' => 'modules',
                'created_at' => '2018-05-21 10:04:17',
                'updated_at' => '2018-05-21 10:04:17',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'read_modules',
                'table_name' => 'modules',
                'created_at' => '2018-05-21 10:04:17',
                'updated_at' => '2018-05-21 10:04:17',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'edit_modules',
                'table_name' => 'modules',
                'created_at' => '2018-05-21 10:04:17',
                'updated_at' => '2018-05-21 10:04:17',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'add_modules',
                'table_name' => 'modules',
                'created_at' => '2018-05-21 10:04:17',
                'updated_at' => '2018-05-21 10:04:17',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'delete_modules',
                'table_name' => 'modules',
                'created_at' => '2018-05-21 10:04:17',
                'updated_at' => '2018-05-21 10:04:17',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'browse_competence_types',
                'table_name' => 'competence_types',
                'created_at' => '2018-05-21 13:11:11',
                'updated_at' => '2018-05-21 13:11:11',
            ),
            61 => 
            array (
                'id' => 62,
                'key' => 'read_competence_types',
                'table_name' => 'competence_types',
                'created_at' => '2018-05-21 13:11:11',
                'updated_at' => '2018-05-21 13:11:11',
            ),
            62 => 
            array (
                'id' => 63,
                'key' => 'edit_competence_types',
                'table_name' => 'competence_types',
                'created_at' => '2018-05-21 13:11:11',
                'updated_at' => '2018-05-21 13:11:11',
            ),
            63 => 
            array (
                'id' => 64,
                'key' => 'add_competence_types',
                'table_name' => 'competence_types',
                'created_at' => '2018-05-21 13:11:11',
                'updated_at' => '2018-05-21 13:11:11',
            ),
            64 => 
            array (
                'id' => 65,
                'key' => 'delete_competence_types',
                'table_name' => 'competence_types',
                'created_at' => '2018-05-21 13:11:11',
                'updated_at' => '2018-05-21 13:11:11',
            ),
            65 => 
            array (
                'id' => 66,
                'key' => 'browse_module_types',
                'table_name' => 'module_types',
                'created_at' => '2018-05-21 13:29:54',
                'updated_at' => '2018-05-21 13:29:54',
            ),
            66 => 
            array (
                'id' => 67,
                'key' => 'read_module_types',
                'table_name' => 'module_types',
                'created_at' => '2018-05-21 13:29:54',
                'updated_at' => '2018-05-21 13:29:54',
            ),
            67 => 
            array (
                'id' => 68,
                'key' => 'edit_module_types',
                'table_name' => 'module_types',
                'created_at' => '2018-05-21 13:29:54',
                'updated_at' => '2018-05-21 13:29:54',
            ),
            68 => 
            array (
                'id' => 69,
                'key' => 'add_module_types',
                'table_name' => 'module_types',
                'created_at' => '2018-05-21 13:29:54',
                'updated_at' => '2018-05-21 13:29:54',
            ),
            69 => 
            array (
                'id' => 70,
                'key' => 'delete_module_types',
                'table_name' => 'module_types',
                'created_at' => '2018-05-21 13:29:54',
                'updated_at' => '2018-05-21 13:29:54',
            ),
        ));
        
        
    }
}