<?php

use Illuminate\Database\Seeder;

class SourceStudentYearOfStudyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('source_student_year_of_study')->delete();
        
        \DB::table('source_student_year_of_study')->insert(array (
            0 => 
            array (
                'id' => 1,
                'year_of_study' => 'First Year',
                'school_type' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'year_of_study' => 'Second Year',
                'school_type' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'year_of_study' => 'Third Year',
                'school_type' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'year_of_study' => 'Fourth Year',
                'school_type' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'year_of_study' => 'Firth Year',
                'school_type' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'year_of_study' => 'Short Course',
                'school_type' => 1,
            ),
            6 => 
            array (
                'id' => 13,
            'year_of_study' => 'Senior Four (Level Three)',
                'school_type' => 2,
            ),
            7 => 
            array (
                'id' => 14,
            'year_of_study' => 'Senior Five (Level Four)',
                'school_type' => 2,
            ),
            8 => 
            array (
                'id' => 15,
            'year_of_study' => 'Senior Six (Level Five)',
                'school_type' => 2,
            ),
            9 => 
            array (
                'id' => 16,
                'year_of_study' => 'Level One',
                'school_type' => NULL,
            ),
            10 => 
            array (
                'id' => 17,
                'year_of_study' => 'Level Two',
                'school_type' => NULL,
            ),
            11 => 
            array (
                'id' => 18,
                'year_of_study' => 'Level Six',
                'school_type' => NULL,
            ),
            12 => 
            array (
                'id' => 19,
                'year_of_study' => 'Level Seven',
                'school_type' => NULL,
            ),
            13 => 
            array (
                'id' => 20,
                'year_of_study' => 'Level Three',
                'school_type' => NULL,
            ),
            14 => 
            array (
                'id' => 21,
                'year_of_study' => 'Level Four',
                'school_type' => NULL,
            ),
            15 => 
            array (
                'id' => 22,
                'year_of_study' => 'Level Five',
                'school_type' => NULL,
            ),
        ));
        
        
    }
}