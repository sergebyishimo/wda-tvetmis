<?php

use Illuminate\Database\Seeder;
use App\Model\Rp\MarksCategorizing;

class MarksCategorizingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MarksCategorizing::query()->truncate();
        $data = [
            [
                'title' => "Assignments",
                'max_marks' => "20",
                'academic_year' => getCurrentAcademicYear(),
            ],
            [
                'title' => "Cat 1",
                'max_marks' => "20",
                'academic_year' => getCurrentAcademicYear(),
            ],
            [
                'title' => "Cat 2",
                'max_marks' => "20",
                'academic_year' => getCurrentAcademicYear(),
            ]
        ];
        foreach ($data as $item) {
            MarksCategorizing::create($item);
        }
    }
}
