<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RwandaBoundaryTableSeeder::class);
        $this->call(UsersPrivilegeTableSeeder::class);
//        $this->call(VoyagerDatabaseSeeder::class);
        $this->call(SourceTvetSubFieldTableSeeder::class);
        $this->call(TssSubjectsTableSeeder::class);
        $this->call(SourceStaffMarkingAssessmentPositionsTableSeeder::class);
        $this->call(SourceStudentYearOfStudyTableSeeder::class);
        $this->call(AttachmentsTableSeeder::class);
        $this->call(SourceTvetFieldTableSeeder::class);
        $this->call(SourceStaffWorkStatusTableSeeder::class);
    }
}
