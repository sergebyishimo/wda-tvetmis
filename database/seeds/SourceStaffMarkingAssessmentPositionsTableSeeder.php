<?php

use Illuminate\Database\Seeder;

class SourceStaffMarkingAssessmentPositionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('source_staff_marking_assessment_positions')->delete();
        
        \DB::table('source_staff_marking_assessment_positions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'position' => 'Exam Marker',
            ),
            1 => 
            array (
                'id' => 2,
                'position' => 'Team Leader',
            ),
            2 => 
            array (
                'id' => 3,
                'position' => 'Chief Marker',
            ),
            3 => 
            array (
                'id' => 4,
                'position' => 'Exam Checker',
            ),
            4 => 
            array (
                'id' => 5,
                'position' => 'Chief Checker',
            ),
            5 => 
            array (
                'id' => 6,
                'position' => 'Chief Recorder',
            ),
        ));
        
        
    }
}