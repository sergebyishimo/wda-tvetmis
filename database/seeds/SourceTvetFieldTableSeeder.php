<?php

use Illuminate\Database\Seeder;

class SourceTvetFieldTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('source_tvet_field')->delete();
        
        \DB::table('source_tvet_field')->insert(array (
            0 => 
            array (
                'id' => 10,
                'tvet_field' => 'Technical Services',
                'field_code' => 'TCS01',
            ),
            1 => 
            array (
                'id' => 11,
                'tvet_field' => 'Agriculture and Food Processing',
                'field_code' => 'AGR01',
            ),
            2 => 
            array (
                'id' => 12,
                'tvet_field' => 'Hospitality and Tourism Services',
                'field_code' => 'HOT01',
            ),
            3 => 
            array (
                'id' => 13,
                'tvet_field' => 'Business Management and Administration Services',
                'field_code' => 'BMA01',
            ),
            4 => 
            array (
                'id' => 14,
            'tvet_field' => 'Information and Communication Technology (ICT)',
                'field_code' => 'ICT01',
            ),
            5 => 
            array (
                'id' => 15,
                'tvet_field' => 'Arts and Crafts',
                'field_code' => 'ARC01',
            ),
            6 => 
            array (
                'id' => 16,
                'tvet_field' => 'Mining and Manufacturing',
                'field_code' => 'MMS01',
            ),
            7 => 
            array (
                'id' => 17,
                'tvet_field' => 'Construction and Building Services',
                'field_code' => 'CST01',
            ),
            8 => 
            array (
                'id' => 18,
                'tvet_field' => 'Social, Sports and Health Services',
                'field_code' => 'SHS01',
            ),
            9 => 
            array (
                'id' => 19,
                'tvet_field' => 'Beauty and Aesthetic Services',
                'field_code' => 'BAS01',
            ),
            10 => 
            array (
                'id' => 20,
                'tvet_field' => 'Transport Services',
                'field_code' => 'TPS01',
            ),
            11 => 
            array (
                'id' => 21,
                'tvet_field' => 'Security Services',
                'field_code' => 'STS01',
            ),
            12 => 
            array (
                'id' => 22,
                'tvet_field' => 'Water Resources Management and Sanitation',
                'field_code' => 'WMS01',
            ),
            13 => 
            array (
                'id' => 23,
                'tvet_field' => 'CLEAN AND SUSTAINABLE ENERGY',
                'field_code' => 'ENS01',
            ),
            14 => 
            array (
                'id' => 24,
                'tvet_field' => 'Environmental Conservation and Management Services',
                'field_code' => 'ENV001',
            ),
        ));
        
        
    }
}