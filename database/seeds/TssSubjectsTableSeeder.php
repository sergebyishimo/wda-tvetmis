<?php

use Illuminate\Database\Seeder;

class TssSubjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tss_subjects')->delete();
        
        \DB::table('tss_subjects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Administraive and Business Correspondence ',
                'subject_code' => 'ABC',
            ),
            1 => 
            array (
                'id' => 2,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Cost and Computerized Accounting                                                                ',
                'subject_code' => 'CCA',
            ),
            2 => 
            array (
                'id' => 3,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Economic Environment              ',
                'subject_code' => 'EEN',
            ),
            3 => 
            array (
                'id' => 4,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Entrepreneurship A                   ',
                'subject_code' => 'ENT',
            ),
            4 => 
            array (
                'id' => 5,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Ethics and Deontology for Accountants',
                'subject_code' => 'EDA',
            ),
            5 => 
            array (
                'id' => 6,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Financial Accounting                ',
                'subject_code' => 'FAC',
            ),
            6 => 
            array (
                'id' => 7,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Law                                             ',
                'subject_code' => 'LAW',
            ),
            7 => 
            array (
                'id' => 8,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            8 => 
            array (
                'id' => 9,
                'option_trade' => 'Accountancy',
                'option_code' => 'ACC',
                'subject' => 'Principles of management and Auditing                                     ',
                'subject_code' => 'PMA',
            ),
            9 => 
            array (
                'id' => 10,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Agriculture Engineering and  Farm Machinery                                   ',
                'subject_code' => 'AEF',
            ),
            10 => 
            array (
                'id' => 11,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Agriculture Training, Ethics and Legislation                                   ',
                'subject_code' => 'AEL',
            ),
            11 => 
            array (
                'id' => 12,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Animal Husbandry                    ',
                'subject_code' => 'ANH',
            ),
            12 => 
            array (
                'id' => 13,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Crop Production, Storage and Processing                                    ',
                'subject_code' => 'CSP',
            ),
            13 => 
            array (
                'id' => 14,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Economics and Entrepreneurship                                                                    ',
                'subject_code' => 'ECE',
            ),
            14 => 
            array (
                'id' => 15,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'English B                                    ',
                'subject_code' => 'ENG',
            ),
            15 => 
            array (
                'id' => 16,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            16 => 
            array (
                'id' => 17,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Plant Biology, Ecology and Pest Management                                                                                              ',
                'subject_code' => 'PEM',
            ),
            17 => 
            array (
                'id' => 18,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            18 => 
            array (
                'id' => 19,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Rural Irrigation and Water Management                                 ',
                'subject_code' => 'RIW',
            ),
            19 => 
            array (
                'id' => 20,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Specific Crop Production          ',
                'subject_code' => 'SCP',
            ),
            20 => 
            array (
                'id' => 21,
                'option_trade' => 'Agriculture',
                'option_code' => 'AGR',
                'subject' => 'Topography, Soil Science and Fertilisation                                   ',
                'subject_code' => 'TSF',
            ),
            21 => 
            array (
                'id' => 22,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'Applied Mathematics               ',
                'subject_code' => 'APM',
            ),
            22 => 
            array (
                'id' => 23,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'Carpentry Technical Drawing ',
                'subject_code' => 'CTD',
            ),
            23 => 
            array (
                'id' => 24,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            24 => 
            array (
                'id' => 25,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            25 => 
            array (
                'id' => 26,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            26 => 
            array (
                'id' => 27,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'Materials Knowledge and Technology                                ',
                'subject_code' => 'MKT',
            ),
            27 => 
            array (
                'id' => 28,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            28 => 
            array (
                'id' => 29,
                'option_trade' => 'Carpentry',
                'option_code' => 'CAP',
                'subject' => 'Work Organization and management              ',
                'subject_code' => 'WOM',
            ),
            29 => 
            array (
                'id' => 30,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Analog and Digital Systems    ',
                'subject_code' => 'ADS',
            ),
            30 => 
            array (
                'id' => 31,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Electricity and Automation      ',
                'subject_code' => 'ELA',
            ),
            31 => 
            array (
                'id' => 32,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            32 => 
            array (
                'id' => 33,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Entrepreneurship B                    ',
                'subject_code' => 'ENT',
            ),
            33 => 
            array (
                'id' => 34,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Français C                                   ',
                'subject_code' => 'FRA',
            ),
            34 => 
            array (
                'id' => 35,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'General electronics                     ',
                'subject_code' => 'GNE',
            ),
            35 => 
            array (
                'id' => 36,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Mathematics                              ',
                'subject_code' => 'MAT',
            ),
            36 => 
            array (
                'id' => 37,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Networking                                  ',
                'subject_code' => 'NET',
            ),
            37 => 
            array (
                'id' => 38,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Operating System, Maintenance and Repair                                         ',
                'subject_code' => 'OMR',
            ),
            38 => 
            array (
                'id' => 39,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Power Electronics and Electromechanical Systems       ',
                'subject_code' => 'PES',
            ),
            39 => 
            array (
                'id' => 40,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Practical ',
                'subject_code' => 'PRA',
            ),
            40 => 
            array (
                'id' => 41,
                'option_trade' => 'Computer Electronics',
                'option_code' => 'CEL',
                'subject' => 'Technical Drawing and Knowledge of Materials                             ',
                'subject_code' => 'TKM',
            ),
            41 => 
            array (
                'id' => 42,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'Algorithm and Programming     ',
                'subject_code' => 'ALP',
            ),
            42 => 
            array (
                'id' => 43,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'Database and Web Design    ',
                'subject_code' => 'DWD',
            ),
            43 => 
            array (
                'id' => 44,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'English B                                     ',
                'subject_code' => 'ENG',
            ),
            44 => 
            array (
                'id' => 45,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'Entrepreneurship B                    ',
                'subject_code' => 'ENT',
            ),
            45 => 
            array (
                'id' => 46,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'Français B                                   ',
                'subject_code' => 'FRA',
            ),
            46 => 
            array (
                'id' => 47,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'Mathematics A                          ',
                'subject_code' => 'MAT',
            ),
            47 => 
            array (
                'id' => 48,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'MS Office and Networking      ',
                'subject_code' => 'MON',
            ),
            48 => 
            array (
                'id' => 49,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'Operating Systems and Maintenance                           ',
                'subject_code' => 'OSM',
            ),
            49 => 
            array (
                'id' => 50,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            50 => 
            array (
                'id' => 51,
                'option_trade' => 'Computer Science',
                'option_code' => 'CSC',
                'subject' => 'System Analysis                        ',
                'subject_code' => 'SAN',
            ),
            51 => 
            array (
                'id' => 52,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Algorithm and Programming     ',
                'subject_code' => 'ALP',
            ),
            52 => 
            array (
                'id' => 53,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Cost and Computerised Accounting                           ',
                'subject_code' => 'CCA',
            ),
            53 => 
            array (
                'id' => 54,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Database and Web Design    ',
                'subject_code' => 'DWD',
            ),
            54 => 
            array (
                'id' => 55,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'English B                                     ',
                'subject_code' => 'ENG',
            ),
            55 => 
            array (
                'id' => 56,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            56 => 
            array (
                'id' => 57,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Financial Accounting               ',
                'subject_code' => 'FAC',
            ),
            57 => 
            array (
                'id' => 58,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Français B                                    ',
                'subject_code' => 'FRA',
            ),
            58 => 
            array (
                'id' => 59,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Mathematics A                         ',
                'subject_code' => 'MAT',
            ),
            59 => 
            array (
                'id' => 60,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'MS Office and  Networking     ',
                'subject_code' => 'MON',
            ),
            60 => 
            array (
                'id' => 61,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            61 => 
            array (
                'id' => 62,
                'option_trade' => 'Computer Science and Management',
                'option_code' => 'CSM',
                'subject' => 'Principles of Management and Auditing                                      ',
                'subject_code' => 'PMA',
            ),
            62 => 
            array (
                'id' => 63,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Construction Surveying and Site Management                               ',
                'subject_code' => 'CSS',
            ),
            63 => 
            array (
                'id' => 64,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Construction Technology and R.C.C                                         ',
                'subject_code' => 'CTR',
            ),
            64 => 
            array (
                'id' => 65,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Cost Estimation                          ',
                'subject_code' => 'COE',
            ),
            65 => 
            array (
                'id' => 66,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Domestic Electricity and Plumbing                                                                             ',
                'subject_code' => 'DEP',
            ),
            66 => 
            array (
                'id' => 67,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'English C                                     ',
                'subject_code' => 'ENG',
            ),
            67 => 
            array (
                'id' => 68,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Entrepreneurship B                    ',
                'subject_code' => 'ENT',
            ),
            68 => 
            array (
                'id' => 69,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Finishing, Fitting and Wood Treatment                                    ',
                'subject_code' => 'FFW',
            ),
            69 => 
            array (
                'id' => 70,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Français B                                   ',
                'subject_code' => 'FRA',
            ),
            70 => 
            array (
                'id' => 71,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Mathematics B                           ',
                'subject_code' => 'MAT',
            ),
            71 => 
            array (
                'id' => 72,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            72 => 
            array (
                'id' => 73,
                'option_trade' => 'Construction',
                'option_code' => 'CST',
                'subject' => 'Technical Drawing and DCG    ',
                'subject_code' => 'TDC',
            ),
            73 => 
            array (
                'id' => 74,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Applied Electronics                   ',
                'subject_code' => 'APE',
            ),
            74 => 
            array (
                'id' => 75,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Automation                                 ',
                'subject_code' => 'AUT',
            ),
            75 => 
            array (
                'id' => 76,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Computer Skills C',
                'subject_code' => 'CSK',
            ),
            76 => 
            array (
                'id' => 77,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Electrical Drawing                     ',
                'subject_code' => 'ELD',
            ),
            77 => 
            array (
                'id' => 78,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Electrical Technology               ',
                'subject_code' => 'ELT',
            ),
            78 => 
            array (
                'id' => 79,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Electrotechnics                           ',
                'subject_code' => 'ELE',
            ),
            79 => 
            array (
                'id' => 80,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'English B                                     ',
                'subject_code' => 'ENG',
            ),
            80 => 
            array (
                'id' => 81,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Entrepreneurship B                    ',
                'subject_code' => 'ENT',
            ),
            81 => 
            array (
                'id' => 82,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Français C                                  ',
                'subject_code' => 'FRA',
            ),
            82 => 
            array (
                'id' => 83,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Mathematics B                          ',
                'subject_code' => 'MAT',
            ),
            83 => 
            array (
                'id' => 84,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            84 => 
            array (
                'id' => 85,
                'option_trade' => 'Electricity',
                'option_code' => 'ELC',
                'subject' => 'Technical Drawing and DCG   ',
                'subject_code' => 'TDE',
            ),
            85 => 
            array (
                'id' => 86,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Analog and Digital Systems     ',
                'subject_code' => 'EDS',
            ),
            86 => 
            array (
                'id' => 87,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Electricity and Automation       ',
                'subject_code' => 'ELA',
            ),
            87 => 
            array (
                'id' => 88,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            88 => 
            array (
                'id' => 89,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Entrepreneurship                        ',
                'subject_code' => 'ENT',
            ),
            89 => 
            array (
                'id' => 90,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            90 => 
            array (
                'id' => 91,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'General electronics                     ',
                'subject_code' => 'GEL',
            ),
            91 => 
            array (
                'id' => 92,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Mathematics B                           ',
                'subject_code' => 'MAT',
            ),
            92 => 
            array (
                'id' => 93,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Power Electronics                        ',
                'subject_code' => 'PEL',
            ),
            93 => 
            array (
                'id' => 94,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            94 => 
            array (
                'id' => 95,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Technical Drawing and Knowledge of Materials                                                 ',
                'subject_code' => 'TKM',
            ),
            95 => 
            array (
                'id' => 96,
                'option_trade' => 'Electronics and Telecommunication',
                'option_code' => 'ETL',
                'subject' => 'Telecommunication Systems     ',
                'subject_code' => 'TES',
            ),
            96 => 
            array (
                'id' => 97,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Business Communication and Marketing                       ',
                'subject_code' => 'BCM',
            ),
            97 => 
            array (
                'id' => 98,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Business Law                  ',
                'subject_code' => 'BUL',
            ),
            98 => 
            array (
                'id' => 99,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Business Management and Auditing                         ',
                'subject_code' => 'BMA',
            ),
            99 => 
            array (
                'id' => 100,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Computer Skills B             ',
                'subject_code' => 'CSK',
            ),
            100 => 
            array (
                'id' => 101,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Corporate Finance                                                                                                                  ',
                'subject_code' => 'COF',
            ),
            101 => 
            array (
                'id' => 102,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Customer Care, Ethics and Deontolgy                       ',
                'subject_code' => 'CED',
            ),
            102 => 
            array (
                'id' => 103,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'English B                         ',
                'subject_code' => 'ENG',
            ),
            103 => 
            array (
                'id' => 104,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Entrepreneurship C             ',
                'subject_code' => 'ENT',
            ),
            104 => 
            array (
                'id' => 105,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Micro Economics            ',
                'subject_code' => 'MEC',
            ),
            105 => 
            array (
                'id' => 106,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            106 => 
            array (
                'id' => 107,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Principles of Accounting   ',
                'subject_code' => 'PAC',
            ),
            107 => 
            array (
                'id' => 108,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Principles of Banking              ',
                'subject_code' => 'PRB',
            ),
            108 => 
            array (
                'id' => 109,
                'option_trade' => 'Finance and Banking',
                'option_code' => 'FIB',
                'subject' => 'Statistics      ',
                'subject_code' => 'STA',
            ),
            109 => 
            array (
                'id' => 110,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Economics and Entrepreneurship                                    ',
                'subject_code' => 'ECE',
            ),
            110 => 
            array (
                'id' => 111,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            111 => 
            array (
                'id' => 112,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Forest Engineering and Measurement                              ',
                'subject_code' => 'FEM',
            ),
            112 => 
            array (
                'id' => 113,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Forest Exploitation and Wood  Industry                                       ',
                'subject_code' => 'FEW',
            ),
            113 => 
            array (
                'id' => 114,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Forest Management, Extension and Legislation                                    ',
                'subject_code' => 'FEL',
            ),
            114 => 
            array (
                'id' => 115,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Forest Protection and Ecology                      ',
                'subject_code' => 'FPE',
            ),
            115 => 
            array (
                'id' => 116,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            116 => 
            array (
                'id' => 117,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Plant Biology and Dendrology                                                                                  ',
                'subject_code' => 'PBD',
            ),
            117 => 
            array (
                'id' => 118,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            118 => 
            array (
                'id' => 119,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Soil Water management and Crop Production                                  ',
                'subject_code' => 'SWC',
            ),
            119 => 
            array (
                'id' => 120,
                'option_trade' => 'Forestry',
                'option_code' => 'FOR',
                'subject' => 'Sylviculture and Agroforestry                           ',
                'subject_code' => 'SAG',
            ),
            120 => 
            array (
                'id' => 121,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'English B                                     ',
                'subject_code' => 'ENG',
            ),
            121 => 
            array (
                'id' => 122,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Entrepreneurship B                    ',
                'subject_code' => 'ENT',
            ),
            122 => 
            array (
                'id' => 123,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            123 => 
            array (
                'id' => 124,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Maintenance Technology    ',
                'subject_code' => 'MTE',
            ),
            124 => 
            array (
                'id' => 125,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Mathematics B                       ',
                'subject_code' => 'MAT',
            ),
            125 => 
            array (
                'id' => 126,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Mechanical Technology           ',
                'subject_code' => 'MET',
            ),
            126 => 
            array (
                'id' => 127,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Plumbing and Welding Technology',
                'subject_code' => 'PWT',
            ),
            127 => 
            array (
                'id' => 128,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            128 => 
            array (
                'id' => 129,
                'option_trade' => 'General Mechanics',
                'option_code' => 'GME',
                'subject' => 'Technical Drawing  and DCG   ',
                'subject_code' => 'TDG',
            ),
            129 => 
            array (
                'id' => 130,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Artistic drawing,                        ',
                'subject_code' => 'ARD',
            ),
            130 => 
            array (
                'id' => 131,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Computer Skills B                ',
                'subject_code' => 'CSK',
            ),
            131 => 
            array (
                'id' => 132,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            132 => 
            array (
                'id' => 133,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            133 => 
            array (
                'id' => 134,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Français B                                    ',
                'subject_code' => 'FRA',
            ),
            134 => 
            array (
                'id' => 135,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'History of Arts and Esthetics  ',
                'subject_code' => 'HAE',
            ),
            135 => 
            array (
                'id' => 136,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Illustration and Advertising      ',
                'subject_code' => 'ILA',
            ),
            136 => 
            array (
                'id' => 137,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Mathematics B                           ',
                'subject_code' => 'MAT',
            ),
            137 => 
            array (
                'id' => 138,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Painting and Graphic Technology      ',
                'subject_code' => 'PGT',
            ),
            138 => 
            array (
                'id' => 139,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            139 => 
            array (
                'id' => 140,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Sketch and  Printing                   ',
                'subject_code' => 'SKP',
            ),
            140 => 
            array (
                'id' => 141,
                'option_trade' => 'Graphic Arts',
                'option_code' => 'ART',
                'subject' => 'Technical Drawing  and Design            ',
                'subject_code' => 'TDD',
            ),
            141 => 
            array (
                'id' => 142,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Computer Skills C                      ',
                'subject_code' => 'CSK',
            ),
            142 => 
            array (
                'id' => 143,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Culinary Technology                ',
                'subject_code' => 'CUT',
            ),
            143 => 
            array (
                'id' => 144,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            144 => 
            array (
                'id' => 145,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            145 => 
            array (
                'id' => 146,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Français B                                    ',
                'subject_code' => 'FRA',
            ),
            146 => 
            array (
                'id' => 147,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Housekeeping and Customer Care                               ',
                'subject_code' => 'HFC',
            ),
            147 => 
            array (
                'id' => 148,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Initiation to Accountancy         ',
                'subject_code' => 'IAC',
            ),
            148 => 
            array (
                'id' => 149,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Kinyarwanda B                            ',
                'subject_code' => 'KIN',
            ),
            149 => 
            array (
                'id' => 150,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Kiswahili B                                   ',
                'subject_code' => 'KIS',
            ),
            150 => 
            array (
                'id' => 151,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Marketing and  Commercial Law            ',
                'subject_code' => 'MCL',
            ),
            151 => 
            array (
                'id' => 152,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Nutrition                                     ',
                'subject_code' => 'NUT',
            ),
            152 => 
            array (
                'id' => 153,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Practical ',
                'subject_code' => 'PRA',
            ),
            153 => 
            array (
                'id' => 154,
                'option_trade' => 'Hotel Operations',
                'option_code' => 'HOT',
                'subject' => 'Restaurant Technology  and Oenology                                    ',
                'subject_code' => 'RTO',
            ),
            154 => 
            array (
                'id' => 155,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Automotive Eletricity and Electronics                                   ',
                'subject_code' => 'AEL',
            ),
            155 => 
            array (
                'id' => 156,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            156 => 
            array (
                'id' => 157,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            157 => 
            array (
                'id' => 158,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Entreprise organisation            ',
                'subject_code' => 'ENO',
            ),
            158 => 
            array (
                'id' => 159,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Français C                                   ',
                'subject_code' => 'FRA',
            ),
            159 => 
            array (
                'id' => 160,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Hydraulic Pneumatic                 ',
                'subject_code' => 'HPN',
            ),
            160 => 
            array (
                'id' => 161,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Knowledge of Materials and Automotive Technology          ',
                'subject_code' => 'KAT',
            ),
            161 => 
            array (
                'id' => 162,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Mathematics B                          ',
                'subject_code' => 'MAT',
            ),
            162 => 
            array (
                'id' => 163,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            163 => 
            array (
                'id' => 164,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Technical Drawing and DCG   ',
                'subject_code' => 'TDM',
            ),
            164 => 
            array (
                'id' => 165,
                'option_trade' => 'Motor Vehicle Mechanics',
                'option_code' => 'MVM',
                'subject' => 'Transmission                              ',
                'subject_code' => 'TRA',
            ),
            165 => 
            array (
                'id' => 166,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Construction Technology ',
                'subject_code' => 'CTY',
            ),
            166 => 
            array (
                'id' => 167,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Domestic Electricity and Plumbing                                                                             ',
                'subject_code' => 'DEP',
            ),
            167 => 
            array (
                'id' => 168,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            168 => 
            array (
                'id' => 169,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            169 => 
            array (
                'id' => 170,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            170 => 
            array (
                'id' => 171,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Hydraulic Works  ',
                'subject_code' => 'HYW',
            ),
            171 => 
            array (
                'id' => 172,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Mathematics B                           ',
                'subject_code' => 'MAT',
            ),
            172 => 
            array (
                'id' => 173,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            173 => 
            array (
                'id' => 174,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Site Management Works        ',
                'subject_code' => 'SMW',
            ),
            174 => 
            array (
                'id' => 175,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Surveying and Cost Estimation            ',
                'subject_code' => 'SUC',
            ),
            175 => 
            array (
                'id' => 176,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Technical Drawing and DCG    ',
                'subject_code' => 'TDP',
            ),
            176 => 
            array (
                'id' => 177,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Topography and Road Construction                            ',
                'subject_code' => 'TRC',
            ),
            177 => 
            array (
                'id' => 178,
                'option_trade' => 'Public Works',
                'option_code' => 'PWO',
                'subject' => 'Water Supply and Sanitation                   ',
                'subject_code' => 'WSS',
            ),
            178 => 
            array (
                'id' => 179,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Artistic Drawing,                         ',
                'subject_code' => 'ARD',
            ),
            179 => 
            array (
                'id' => 180,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Ceramic Technology                   ',
                'subject_code' => 'CET',
            ),
            180 => 
            array (
                'id' => 181,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Computer Skills B                     ',
                'subject_code' => 'CSK',
            ),
            181 => 
            array (
                'id' => 182,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            182 => 
            array (
                'id' => 183,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            183 => 
            array (
                'id' => 184,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Français B                                        ',
                'subject_code' => 'FRA',
            ),
            184 => 
            array (
                'id' => 185,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'History of Arts and Esthetics',
                'subject_code' => 'HAE',
            ),
            185 => 
            array (
                'id' => 186,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Mathematics B                           ',
                'subject_code' => 'MAT',
            ),
            186 => 
            array (
                'id' => 187,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            187 => 
            array (
                'id' => 188,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Sculpture and Sculpture Technology',
                'subject_code' => 'SST',
            ),
            188 => 
            array (
                'id' => 189,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Sketch and Printing                    ',
                'subject_code' => 'SKP',
            ),
            189 => 
            array (
                'id' => 190,
                'option_trade' => 'Sculpture and Ceramics',
                'option_code' => 'SCE',
                'subject' => 'Technical Drawing and Design                           ',
                'subject_code' => 'TDD',
            ),
            190 => 
            array (
                'id' => 191,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Business and Administrative Correspondence       ',
                'subject_code' => 'BAC',
            ),
            191 => 
            array (
                'id' => 192,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Civil Law and Communication Skills      ',
                'subject_code' => 'CLC',
            ),
            192 => 
            array (
                'id' => 193,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Computer Skills A      ',
                'subject_code' => 'CSK',
            ),
            193 => 
            array (
                'id' => 194,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Correspondance Administrative et Commerciale',
                'subject_code' => 'CCA',
            ),
            194 => 
            array (
                'id' => 195,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'English A                                      ',
                'subject_code' => 'ENG',
            ),
            195 => 
            array (
                'id' => 196,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            196 => 
            array (
                'id' => 197,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Français A                                    ',
                'subject_code' => 'FRA',
            ),
            197 => 
            array (
                'id' => 198,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Initiation to Accountancy         ',
                'subject_code' => 'IAC',
            ),
            198 => 
            array (
                'id' => 199,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Kinyarwanda A                            ',
                'subject_code' => 'KIN',
            ),
            199 => 
            array (
                'id' => 200,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Kiswahili A                                    ',
                'subject_code' => 'KIS',
            ),
            200 => 
            array (
                'id' => 201,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Office Technique and Practice  ',
                'subject_code' => 'OTP',
            ),
            201 => 
            array (
                'id' => 202,
                'option_trade' => 'Secretarial',
                'option_code' => 'SEC',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            202 => 
            array (
                'id' => 203,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'English B                         ',
                'subject_code' => 'ENG',
            ),
            203 => 
            array (
                'id' => 204,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Entrepreneurship B      ',
                'subject_code' => 'ENT',
            ),
            204 => 
            array (
                'id' => 205,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Français C                        ',
                'subject_code' => 'FRA',
            ),
            205 => 
            array (
                'id' => 206,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'GIS and Applications          ',
                'subject_code' => 'GIS',
            ),
            206 => 
            array (
                'id' => 207,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Hydraulic Structures                ',
                'subject_code' => 'HST',
            ),
            207 => 
            array (
                'id' => 208,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Mathematics B',
                'subject_code' => 'MAT',
            ),
            208 => 
            array (
                'id' => 209,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Physics                           ',
                'subject_code' => 'PHY',
            ),
            209 => 
            array (
                'id' => 210,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Quantity Surveying                ',
                'subject_code' => 'QUS',
            ),
            210 => 
            array (
                'id' => 211,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Surveying                                ',
                'subject_code' => 'SUV',
            ),
            211 => 
            array (
                'id' => 212,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Surveying Applications           ',
                'subject_code' => 'SUA',
            ),
            212 => 
            array (
                'id' => 213,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Technical Drawing     ',
                'subject_code' => 'TDS',
            ),
            213 => 
            array (
                'id' => 214,
                'option_trade' => 'Surveying',
                'option_code' => 'SUR',
                'subject' => 'Urban Territory Planning ',
                'subject_code' => 'UTP',
            ),
            214 => 
            array (
                'id' => 215,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Artistic Drawing                         ',
                'subject_code' => 'TAD',
            ),
            215 => 
            array (
                'id' => 216,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            216 => 
            array (
                'id' => 217,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            217 => 
            array (
                'id' => 218,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            218 => 
            array (
                'id' => 219,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Mathematics B                           ',
                'subject_code' => 'MAT',
            ),
            219 => 
            array (
                'id' => 220,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Pattern Cutting Techniques        ',
                'subject_code' => 'PCT',
            ),
            220 => 
            array (
                'id' => 221,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            221 => 
            array (
                'id' => 222,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Textile Technology                    ',
                'subject_code' => 'TXT',
            ),
            222 => 
            array (
                'id' => 223,
                'option_trade' => 'Tailoring',
                'option_code' => 'TAL',
                'subject' => 'Workshop Technology            ',
                'subject_code' => 'WOT',
            ),
            223 => 
            array (
                'id' => 224,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Computer Skills C             ',
                'subject_code' => 'CSK',
            ),
            224 => 
            array (
                'id' => 225,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            225 => 
            array (
                'id' => 226,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Entrepreneurship B                     ',
                'subject_code' => 'ENT',
            ),
            226 => 
            array (
                'id' => 227,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Français B                                   ',
                'subject_code' => 'FRA',
            ),
            227 => 
            array (
                'id' => 228,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Initiation to Accountancy         ',
                'subject_code' => 'IAC',
            ),
            228 => 
            array (
                'id' => 229,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Kinyarwanda B                           ',
                'subject_code' => 'KIN',
            ),
            229 => 
            array (
                'id' => 230,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Kiswahili B                                    ',
                'subject_code' => 'KIS',
            ),
            230 => 
            array (
                'id' => 231,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Marketing and Commercial Law                                        ',
                'subject_code' => 'MCL',
            ),
            231 => 
            array (
                'id' => 232,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            232 => 
            array (
                'id' => 233,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Social Psychology and Customer Care                                                ',
                'subject_code' => 'SFC',
            ),
            233 => 
            array (
                'id' => 234,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Tourism                                       ',
                'subject_code' => 'TOU',
            ),
            234 => 
            array (
                'id' => 235,
                'option_trade' => 'Tourism',
                'option_code' => 'TOR',
                'subject' => 'Tourism  Geography                   ',
                'subject_code' => 'TOG',
            ),
            235 => 
            array (
                'id' => 236,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Anatomy and Physiology         ',
                'subject_code' => 'ANP',
            ),
            236 => 
            array (
                'id' => 237,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Animal Husbandry and Feeding                                                                                                                                          ',
                'subject_code' => 'AHF',
            ),
            237 => 
            array (
                'id' => 238,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'English B                                      ',
                'subject_code' => 'ENG',
            ),
            238 => 
            array (
                'id' => 239,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Entomology and Parasitology                                                                    ',
                'subject_code' => 'ENP',
            ),
            239 => 
            array (
                'id' => 240,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Français C                                    ',
                'subject_code' => 'FRA',
            ),
            240 => 
            array (
                'id' => 241,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'General Pathology and Semiology                                                               ',
                'subject_code' => 'GPS',
            ),
            241 => 
            array (
                'id' => 242,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Hygiene and Animal Products Industry                                        ',
                'subject_code' => 'HAI',
            ),
            242 => 
            array (
                'id' => 243,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Livestock Management and Entrepreneurship',
                'subject_code' => 'LME',
            ),
            243 => 
            array (
                'id' => 244,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Microbiology and Pharmacology                                                                                                      ',
                'subject_code' => 'MIP',
            ),
            244 => 
            array (
                'id' => 245,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Practical',
                'subject_code' => 'PRA',
            ),
            245 => 
            array (
                'id' => 246,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Surgery, Gynecology and Obstetrics                                                                                 ',
                'subject_code' => 'SGO',
            ),
            246 => 
            array (
                'id' => 247,
                'option_trade' => 'Veterinary',
                'option_code' => 'VET',
                'subject' => 'Veterinary Training and Deontology                              ',
                'subject_code' => 'VTD',
            ),
        ));
        
        
    }
}