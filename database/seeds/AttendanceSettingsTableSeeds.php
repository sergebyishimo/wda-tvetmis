<?php

use Illuminate\Database\Seeder;

class AttendanceSettingsTableSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'checkin' => '07:00',
                'checkout' => '17:00',
                'minsearly' => '20',
                'minslate' => '20',
                'minsworkday' => '420',
                'breaktime' => '2',
                'days' => 'MON,TUE,WED,THU'
            ],
            [
                'checkin' => '07:00',
                'checkout' => '14:00',
                'minsearly' => '20',
                'minslate' => '20',
                'minsworkday' => '360',
                'breaktime' => '0',
                'days' => 'FRI'
            ],
            [
                'checkin' => '07:00',
                'checkout' => '12:00',
                'minsearly' => '20',
                'minslate' => '20',
                'minsworkday' => '240',
                'breaktime' => '0',
                'days' => 'SAT'
            ],
            [
                'checkin' => '0',
                'checkout' => '0',
                'minsearly' => '0',
                'minslate' => '0',
                'minsworkday' => '0',
                'breaktime' => '0',
                'days' => 'SUN'
            ]
        ];
        foreach ($datas as $data) {
            \DB::table('attendance_settings')->insert($data);
        }
    }
}
