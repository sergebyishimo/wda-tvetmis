<?php

use Illuminate\Database\Seeder;

class SourceStaffWorkStatusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('source_staff_work_status')->delete();
        
        \DB::table('source_staff_work_status')->insert(array (
            0 => 
            array (
                'id' => 1,
                'work_status' => 'Contract',
            ),
            1 => 
            array (
                'id' => 2,
                'work_status' => 'Part-time',
            ),
            2 => 
            array (
                'id' => 3,
                'work_status' => 'Permanent',
            ),
        ));
        
        
    }
}