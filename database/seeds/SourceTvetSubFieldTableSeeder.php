<?php

use Illuminate\Database\Seeder;

class SourceTvetSubFieldTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('source_tvet_sub_field')->delete();
        
        \DB::table('source_tvet_sub_field')->insert(array (
            0 => 
            array (
                'id' => 2,
                'sub_field_name' => 'VTC - Arts & Sculptures',
                'sub_field_code' => 'VTC002',
                'tvet_field' => 17,
            ),
            1 => 
            array (
                'id' => 3,
                'sub_field_name' => 'VTC - Beauty Therapy',
                'sub_field_code' => 'VTC003',
                'tvet_field' => 19,
            ),
            2 => 
            array (
                'id' => 4,
                'sub_field_name' => 'VTC - Carpentry',
                'sub_field_code' => 'VTC004',
                'tvet_field' => 17,
            ),
            3 => 
            array (
                'id' => 5,
                'sub_field_name' => 'VTC - Crochet Embroidery',
                'sub_field_code' => 'VTC005',
                'tvet_field' => 15,
            ),
            4 => 
            array (
                'id' => 6,
                'sub_field_name' => 'VTC - Domestic Electricity',
                'sub_field_code' => 'VTC006',
                'tvet_field' => 10,
            ),
            5 => 
            array (
                'id' => 7,
                'sub_field_name' => 'VTC - Dressmaking',
                'sub_field_code' => 'VTC007',
                'tvet_field' => 15,
            ),
            6 => 
            array (
                'id' => 8,
                'sub_field_name' => 'VTC - Film making',
                'sub_field_code' => 'VTC008',
                'tvet_field' => 14,
            ),
            7 => 
            array (
                'id' => 9,
            'sub_field_name' => 'VTC - Forestry ( For technicians)',
                'sub_field_code' => 'VTC009',
                'tvet_field' => 11,
            ),
            8 => 
            array (
                'id' => 10,
                'sub_field_name' => 'VTC - Hairdressing - Aesthetics ',
                'sub_field_code' => 'VTC010',
                'tvet_field' => 19,
            ),
            9 => 
            array (
                'id' => 11,
                'sub_field_name' => 'VTC - Hospitality and Tourism',
                'sub_field_code' => 'VTC011',
                'tvet_field' => 12,
            ),
            10 => 
            array (
                'id' => 12,
                'sub_field_name' => 'VTC - ICT',
                'sub_field_code' => 'VTC012',
                'tvet_field' => 14,
            ),
            11 => 
            array (
                'id' => 13,
                'sub_field_name' => 'VTC - Leather Craft',
                'sub_field_code' => 'VTC013',
                'tvet_field' => 15,
            ),
            12 => 
            array (
                'id' => 14,
                'sub_field_name' => 'VTC - Masonry',
                'sub_field_code' => 'VTC014',
                'tvet_field' => 17,
            ),
            13 => 
            array (
                'id' => 15,
                'sub_field_name' => 'VTC - Motor Vehicle Mechanics',
                'sub_field_code' => 'VTC015',
                'tvet_field' => 10,
            ),
            14 => 
            array (
                'id' => 16,
                'sub_field_name' => 'VTC - Plumbing',
                'sub_field_code' => 'VTC016',
                'tvet_field' => 17,
            ),
            15 => 
            array (
                'id' => 17,
                'sub_field_name' => 'VTC - Pottery',
                'sub_field_code' => 'VTC017',
                'tvet_field' => 15,
            ),
            16 => 
            array (
                'id' => 18,
                'sub_field_name' => 'VTC - Screen Printing',
                'sub_field_code' => 'VTC018',
                'tvet_field' => 14,
            ),
            17 => 
            array (
                'id' => 19,
                'sub_field_name' => 'VTC - Sport and Medical Massage',
                'sub_field_code' => 'VTC019',
                'tvet_field' => 18,
            ),
            18 => 
            array (
                'id' => 20,
                'sub_field_name' => 'VTC - Tailoring & Embroidery',
                'sub_field_code' => 'VTC020',
                'tvet_field' => 15,
            ),
            19 => 
            array (
                'id' => 21,
                'sub_field_name' => 'VTC - Welding and Fabrication',
                'sub_field_code' => 'VTC021',
                'tvet_field' => 17,
            ),
            20 => 
            array (
                'id' => 22,
                'sub_field_name' => 'VTC - Pastry',
                'sub_field_code' => 'VTC022',
                'tvet_field' => 11,
            ),
            21 => 
            array (
                'id' => 23,
            'sub_field_name' => 'TSS - Accountancy (ACC)',
                'sub_field_code' => 'ACC',
                'tvet_field' => 13,
            ),
            22 => 
            array (
                'id' => 24,
            'sub_field_name' => 'TSS - Agriculture (AGR) - Animal Husbandry',
                'sub_field_code' => 'AGR',
                'tvet_field' => 11,
            ),
            23 => 
            array (
                'id' => 25,
            'sub_field_name' => 'TSS - Graphic Arts (ART)',
                'sub_field_code' => 'ART',
                'tvet_field' => 15,
            ),
            24 => 
            array (
                'id' => 26,
            'sub_field_name' => 'TSS - Carpentry (CAP)',
                'sub_field_code' => 'CAP',
                'tvet_field' => 17,
            ),
            25 => 
            array (
                'id' => 27,
            'sub_field_name' => 'TSS - Computer Electronics (CEL)',
                'sub_field_code' => 'CEL',
                'tvet_field' => 14,
            ),
            26 => 
            array (
                'id' => 28,
            'sub_field_name' => 'TSS - Ceramic-Sculpture (SCE)',
                'sub_field_code' => 'SCE',
                'tvet_field' => 15,
            ),
            27 => 
            array (
                'id' => 30,
            'sub_field_name' => 'TSS - Computer Science (CSC)',
                'sub_field_code' => 'CSC',
                'tvet_field' => 14,
            ),
            28 => 
            array (
                'id' => 31,
            'sub_field_name' => 'TSS - Computer Science & Management (CSM)',
                'sub_field_code' => 'CSM
',
                'tvet_field' => 14,
            ),
            29 => 
            array (
                'id' => 32,
            'sub_field_name' => 'TSS - Construction (CST)',
                'sub_field_code' => 'CST',
                'tvet_field' => 17,
            ),
            30 => 
            array (
                'id' => 33,
            'sub_field_name' => 'TSS - Electricity (ELC)',
                'sub_field_code' => 'ELC',
                'tvet_field' => 10,
            ),
            31 => 
            array (
                'id' => 34,
            'sub_field_name' => 'TSS - Electronics and Telecommunication (ETL)',
                'sub_field_code' => 'ETL',
                'tvet_field' => 10,
            ),
            32 => 
            array (
                'id' => 35,
            'sub_field_name' => 'TSS - Forestry (FOR)',
                'sub_field_code' => 'FOR',
                'tvet_field' => 11,
            ),
            33 => 
            array (
                'id' => 36,
            'sub_field_name' => 'TSS - General Mechanics (GME)',
                'sub_field_code' => 'GME',
                'tvet_field' => 10,
            ),
            34 => 
            array (
                'id' => 37,
            'sub_field_name' => 'TSS - Hotel Operations (HOT)',
                'sub_field_code' => 'HOT',
                'tvet_field' => 12,
            ),
            35 => 
            array (
                'id' => 39,
            'sub_field_name' => 'TSS - Moto Vehicle Mechanics (MVM)',
                'sub_field_code' => 'MVM',
                'tvet_field' => 10,
            ),
            36 => 
            array (
                'id' => 40,
            'sub_field_name' => 'TSS - Public Works (PWO)',
                'sub_field_code' => 'PWO',
                'tvet_field' => 17,
            ),
            37 => 
            array (
                'id' => 41,
            'sub_field_name' => 'TSS - Secretarial (SEC)',
                'sub_field_code' => 'SEC',
                'tvet_field' => 13,
            ),
            38 => 
            array (
                'id' => 42,
            'sub_field_name' => 'TSS - Tourism (TOR)',
                'sub_field_code' => 'TOR',
                'tvet_field' => 12,
            ),
            39 => 
            array (
                'id' => 43,
            'sub_field_name' => 'TSS - Veterinary (VET)',
                'sub_field_code' => 'VET',
                'tvet_field' => 11,
            ),
            40 => 
            array (
                'id' => 44,
            'sub_field_name' => 'Polytechnic - ICT (Advanced Diploma)',
                'sub_field_code' => 'PLT001',
                'tvet_field' => 14,
            ),
            41 => 
            array (
                'id' => 45,
                'sub_field_name' => 'Polytechnic - Hospitality Management',
                'sub_field_code' => 'PLT002',
                'tvet_field' => 12,
            ),
            42 => 
            array (
                'id' => 46,
                'sub_field_name' => 'Polytechnic - Entrepreneurship',
                'sub_field_code' => 'PLT003',
                'tvet_field' => 13,
            ),
            43 => 
            array (
                'id' => 47,
                'sub_field_name' => 'Polytechnic - Civil Engineering',
                'sub_field_code' => 'PLT004',
                'tvet_field' => 10,
            ),
            44 => 
            array (
                'id' => 48,
                'sub_field_name' => 'Polytechnic - Electrical and Electronics Engineering ',
                'sub_field_code' => 'PLT005',
                'tvet_field' => 10,
            ),
            45 => 
            array (
                'id' => 49,
                'sub_field_name' => 'Polytechnic - Mining Engineering',
                'sub_field_code' => 'PLT006',
                'tvet_field' => 10,
            ),
            46 => 
            array (
                'id' => 50,
                'sub_field_name' => 'Polytechnic - Mechanical Engineering',
                'sub_field_code' => 'PLT007',
                'tvet_field' => 10,
            ),
            47 => 
            array (
                'id' => 51,
                'sub_field_name' => 'Polytechnic - Alternative Energy',
                'sub_field_code' => 'PLT008',
                'tvet_field' => 23,
            ),
            48 => 
            array (
                'id' => 52,
                'sub_field_name' => 'Polytechnic - Electricity & Telecommunication',
                'sub_field_code' => 'PLT009',
                'tvet_field' => 10,
            ),
            49 => 
            array (
                'id' => 53,
                'sub_field_name' => 'VTC - Agri Business',
                'sub_field_code' => '',
                'tvet_field' => 11,
            ),
            50 => 
            array (
                'id' => 54,
                'sub_field_name' => 'VTC - Food Processing',
                'sub_field_code' => 'VTC023',
                'tvet_field' => 11,
            ),
            51 => 
            array (
                'id' => 55,
                'sub_field_name' => 'VTC - Bee Keeping',
                'sub_field_code' => '',
                'tvet_field' => 11,
            ),
            52 => 
            array (
                'id' => 56,
                'sub_field_name' => 'VTC - House Keeping',
                'sub_field_code' => 'VTC024',
                'tvet_field' => 12,
            ),
            53 => 
            array (
                'id' => 57,
                'sub_field_name' => 'VTC - Culnary Arts',
                'sub_field_code' => 'VTC026',
                'tvet_field' => 12,
            ),
            54 => 
            array (
                'id' => 58,
                'sub_field_name' => 'VTC - Foods and Beverages',
                'sub_field_code' => 'VTC025',
                'tvet_field' => 12,
            ),
            55 => 
            array (
                'id' => 59,
                'sub_field_name' => 'VTC - Front Desk Management',
                'sub_field_code' => 'VTC027',
                'tvet_field' => 12,
            ),
            56 => 
            array (
                'id' => 60,
                'sub_field_name' => 'VTC - Multimedia',
                'sub_field_code' => '',
                'tvet_field' => 14,
            ),
            57 => 
            array (
                'id' => 62,
                'sub_field_name' => 'VTC - Crop Production',
                'sub_field_code' => 'VTC0029',
                'tvet_field' => 11,
            ),
            58 => 
            array (
                'id' => 63,
                'sub_field_name' => 'VTC - Knitting',
                'sub_field_code' => 'VTC0028',
                'tvet_field' => 15,
            ),
            59 => 
            array (
                'id' => 64,
            'sub_field_name' => 'TSS - Tailoring (TAL)',
                'sub_field_code' => 'TAL',
                'tvet_field' => 15,
            ),
            60 => 
            array (
                'id' => 66,
                'sub_field_name' => 'Polytechnic - Wildlife Management',
                'sub_field_code' => 'PLT010',
                'tvet_field' => 24,
            ),
            61 => 
            array (
                'id' => 67,
                'sub_field_name' => 'Polytechnic - Bio Medical Equipment Technology',
                'sub_field_code' => 'PLT011',
                'tvet_field' => 10,
            ),
            62 => 
            array (
                'id' => 68,
            'sub_field_name' => 'Polytechnic - Water and Sanitation Technology (WAS)',
                'sub_field_code' => 'PLT012',
                'tvet_field' => 10,
            ),
            63 => 
            array (
                'id' => 69,
            'sub_field_name' => 'Polytechnic - Quantity surveying (QUS)',
                'sub_field_code' => 'PLT013',
                'tvet_field' => 10,
            ),
            64 => 
            array (
                'id' => 70,
            'sub_field_name' => 'Polytechnic - Production and Manufacturing Technology (Advanced Diploma)',
                'sub_field_code' => 'PLT014',
                'tvet_field' => 10,
            ),
            65 => 
            array (
                'id' => 71,
                'sub_field_name' => 'Polytechnic - Air conditioning and Refrigeration Technology',
                'sub_field_code' => 'PLT015',
                'tvet_field' => 10,
            ),
            66 => 
            array (
                'id' => 72,
                'sub_field_name' => 'Polytechnic - Construction Technology',
                'sub_field_code' => 'PLT016',
                'tvet_field' => 10,
            ),
            67 => 
            array (
                'id' => 73,
                'sub_field_name' => 'VTC - Music',
                'sub_field_code' => NULL,
                'tvet_field' => 15,
            ),
            68 => 
            array (
                'id' => 74,
                'sub_field_name' => 'VTC - Industrial Installation',
                'sub_field_code' => 'VTC025',
                'tvet_field' => 10,
            ),
            69 => 
            array (
                'id' => 75,
                'sub_field_name' => 'Polytechnic - Automobile Technology',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            70 => 
            array (
                'id' => 76,
            'sub_field_name' => 'Polytechnic - Transportation Engineering (TRA)',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            71 => 
            array (
                'id' => 77,
            'sub_field_name' => 'Polytechnic - Engineering Surveying (ESU)',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            72 => 
            array (
                'id' => 78,
            'sub_field_name' => 'Polytechnic - Electromechanical Technology (EMT)',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            73 => 
            array (
                'id' => 79,
                'sub_field_name' => 'Polytechnic - Construction And Irrigation Technology',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            74 => 
            array (
                'id' => 80,
                'sub_field_name' => 'VTC - Customer Care',
                'sub_field_code' => NULL,
                'tvet_field' => 12,
            ),
            75 => 
            array (
                'id' => 81,
                'sub_field_name' => 'Polytechnic - Business Studies',
                'sub_field_code' => NULL,
                'tvet_field' => 13,
            ),
            76 => 
            array (
                'id' => 82,
                'sub_field_name' => 'TSS - Finance and Banking',
                'sub_field_code' => 'FIB',
                'tvet_field' => 13,
            ),
            77 => 
            array (
                'id' => 83,
            'sub_field_name' => 'TSS - Taxation and Customs (TXC)',
                'sub_field_code' => 'TXC',
                'tvet_field' => 13,
            ),
            78 => 
            array (
                'id' => 84,
                'sub_field_name' => 'VTC - Animal Health',
                'sub_field_code' => NULL,
                'tvet_field' => 11,
            ),
            79 => 
            array (
                'id' => 85,
                'sub_field_name' => 'VTC - Panel Beating and Paiting',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            80 => 
            array (
                'id' => 86,
                'sub_field_name' => 'VTC - Machine Tools',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            81 => 
            array (
                'id' => 87,
            'sub_field_name' => 'TSS - Surveying (SUR)',
                'sub_field_code' => 'SUR',
                'tvet_field' => 10,
            ),
            82 => 
            array (
                'id' => 88,
                'sub_field_name' => 'VTC - Early Childhood Development',
                'sub_field_code' => 'VTC-030',
                'tvet_field' => 18,
            ),
            83 => 
            array (
                'id' => 89,
                'sub_field_name' => 'VTC - Automotive Electricity',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            84 => 
            array (
                'id' => 90,
                'sub_field_name' => 'VTC - Driving',
                'sub_field_code' => NULL,
                'tvet_field' => 10,
            ),
            85 => 
            array (
                'id' => 91,
            'sub_field_name' => 'Polytechnic - Culinary Arts (Advanced Diploma)',
                'sub_field_code' => 'PLT015',
                'tvet_field' => 12,
            ),
            86 => 
            array (
                'id' => 92,
            'sub_field_name' => 'Polytechnic - Electrical Technology (Advanced Diploma)',
                'sub_field_code' => 'PLT005',
                'tvet_field' => 10,
            ),
            87 => 
            array (
                'id' => 93,
            'sub_field_name' => 'Polytechnic - Foods and Beverages (Advanced Diploma)',
                'sub_field_code' => 'PLT002',
                'tvet_field' => 12,
            ),
            88 => 
            array (
                'id' => 94,
            'sub_field_name' => 'Polytechnic - Front Office Operations (Advanced Diploma)',
                'sub_field_code' => 'PLT002',
                'tvet_field' => 12,
            ),
            89 => 
            array (
                'id' => 95,
            'sub_field_name' => 'Polytechnic - House Keeping (Advanced Diploma)',
                'sub_field_code' => 'PLT002',
                'tvet_field' => 12,
            ),
            90 => 
            array (
                'id' => 96,
            'sub_field_name' => 'Polytechnic - IT (Advanced Diploma)',
                'sub_field_code' => 'PLT001',
                'tvet_field' => 14,
            ),
            91 => 
            array (
                'id' => 97,
            'sub_field_name' => 'Polytechnic - Agriculture and Food Processing (Advanced Diploma)',
                'sub_field_code' => 'PLT015',
                'tvet_field' => 11,
            ),
            92 => 
            array (
                'id' => 98,
            'sub_field_name' => 'Polytechnic - Crop Production (Advanced Diploma)',
                'sub_field_code' => 'PLT015',
                'tvet_field' => 11,
            ),
            93 => 
            array (
                'id' => 99,
            'sub_field_name' => 'Polytechnic - Electricity & Telecom (Advanced Diploma)',
                'sub_field_code' => 'PLT009',
                'tvet_field' => 10,
            ),
            94 => 
            array (
                'id' => 100,
            'sub_field_name' => 'Polytechnic - Forest Resources Management (Advanced Diploma)',
                'sub_field_code' => 'PLT015',
                'tvet_field' => 11,
            ),
            95 => 
            array (
                'id' => 101,
            'sub_field_name' => 'Polytechnic - Irrigation and Drainage Technology (Advanced Diploma)',
                'sub_field_code' => 'PLT012',
                'tvet_field' => 10,
            ),
            96 => 
            array (
                'id' => 102,
            'sub_field_name' => 'Polytechnic - Motor Vehicle Mechanics (Advanced Diploma)',
                'sub_field_code' => 'PLT007',
                'tvet_field' => 10,
            ),
            97 => 
            array (
                'id' => 103,
            'sub_field_name' => 'Polytechnic - Veterinary Technology (Advanced Diploma)',
                'sub_field_code' => 'PLT010',
                'tvet_field' => 24,
            ),
            98 => 
            array (
                'id' => 104,
            'sub_field_name' => 'Polytechnic - Wildlife Tourism (Advanced Diploma)',
                'sub_field_code' => 'PLT010',
                'tvet_field' => 24,
            ),
            99 => 
            array (
                'id' => 105,
                'sub_field_name' => 'Not Applicable',
                'sub_field_code' => 'N/A',
                'tvet_field' => 0,
            ),
            100 => 
            array (
                'id' => 106,
                'sub_field_name' => 'TSS - SEE',
                'sub_field_code' => 'SEE',
                'tvet_field' => 17,
            ),
            101 => 
            array (
                'id' => 107,
                'sub_field_name' => 'TSS - LAB',
                'sub_field_code' => 'LAB',
                'tvet_field' => 17,
            ),
            102 => 
            array (
                'id' => 108,
                'sub_field_name' => 'TSS - CAE',
                'sub_field_code' => 'CAE',
                'tvet_field' => 14,
            ),
            103 => 
            array (
                'id' => 109,
                'sub_field_name' => 'TSS - CSA',
                'sub_field_code' => 'CSA',
                'tvet_field' => 17,
            ),
            104 => 
            array (
                'id' => 110,
                'sub_field_name' => 'TSS - CES',
                'sub_field_code' => 'CES',
                'tvet_field' => 14,
            ),
            105 => 
            array (
                'id' => 111,
                'sub_field_name' => 'TSS - Technical Languages',
                'sub_field_code' => 'TECH Languages',
                'tvet_field' => 13,
            ),
            106 => 
            array (
                'id' => 112,
                'sub_field_name' => 'TSS - General Subjects',
                'sub_field_code' => '',
                'tvet_field' => 17,
            ),
            107 => 
            array (
                'id' => 113,
            'sub_field_name' => 'TSS - Food Processing (PRC)',
                'sub_field_code' => 'PRC',
                'tvet_field' => 11,
            ),
            108 => 
            array (
                'id' => 114,
            'sub_field_name' => 'TSS - Agriculture (AGR) - Crop Husbandry',
                'sub_field_code' => 'AGR',
                'tvet_field' => 11,
            ),
            109 => 
            array (
                'id' => 115,
                'sub_field_name' => 'VTC - General Mechanics',
                'sub_field_code' => 'VTC015',
                'tvet_field' => 10,
            ),
            110 => 
            array (
                'id' => 116,
                'sub_field_name' => 'VTC - CANDLE & SOAP MARKING',
                'sub_field_code' => 'VTC007',
                'tvet_field' => 15,
            ),
            111 => 
            array (
                'id' => 117,
                'sub_field_name' => 'VTC - Banana craft',
                'sub_field_code' => 'VTC005',
                'tvet_field' => 15,
            ),
            112 => 
            array (
                'id' => 118,
                'sub_field_name' => 'VTC - Secretary',
                'sub_field_code' => 'VTC027',
                'tvet_field' => 12,
            ),
            113 => 
            array (
                'id' => 119,
                'sub_field_name' => 'VTC - General Subjects',
                'sub_field_code' => 'VTC027',
                'tvet_field' => 12,
            ),
            114 => 
            array (
                'id' => 120,
                'sub_field_name' => 'VTC - Technical Languages',
                'sub_field_code' => 'VTC027',
                'tvet_field' => 12,
            ),
            115 => 
            array (
                'id' => 121,
                'sub_field_name' => 'VTC - Shoe Making',
                'sub_field_code' => 'VTC017',
                'tvet_field' => 15,
            ),
            116 => 
            array (
                'id' => 122,
                'sub_field_name' => 'VTC - TILING AND PLASTERING',
                'sub_field_code' => 'VTC014',
                'tvet_field' => 17,
            ),
            117 => 
            array (
                'id' => 123,
            'sub_field_name' => 'Polytechnic - Cloud Computing (Advanced Diploma)',
                'sub_field_code' => 'PLT001',
                'tvet_field' => 14,
            ),
            118 => 
            array (
                'id' => 124,
                'sub_field_name' => 'Polytechnic - Accounting',
                'sub_field_code' => NULL,
                'tvet_field' => 13,
            ),
            119 => 
            array (
                'id' => 125,
                'sub_field_name' => 'Polytechnic - Logistics and Procurement Management',
                'sub_field_code' => NULL,
                'tvet_field' => 13,
            ),
            120 => 
            array (
                'id' => 126,
                'sub_field_name' => 'Polytechnic - Project Management',
                'sub_field_code' => NULL,
                'tvet_field' => 13,
            ),
            121 => 
            array (
                'id' => 127,
                'sub_field_name' => 'Polytechnic - Tourism and Travel Management',
                'sub_field_code' => '',
                'tvet_field' => 12,
            ),
            122 => 
            array (
                'id' => 128,
                'sub_field_name' => 'VTC - Bamboo Furniture',
                'sub_field_code' => 'VTC002',
                'tvet_field' => 17,
            ),
            123 => 
            array (
                'id' => 129,
                'sub_field_name' => 'VTC - Bamboo Weaving',
                'sub_field_code' => 'VTC002',
                'tvet_field' => 17,
            ),
            124 => 
            array (
                'id' => 130,
                'sub_field_name' => 'VTC - foot wear and leather goods',
                'sub_field_code' => 'VTC013',
                'tvet_field' => 15,
            ),
            125 => 
            array (
                'id' => 132,
                'sub_field_name' => 'Polytechnic - Technical Accounting',
                'sub_field_code' => '',
                'tvet_field' => 13,
            ),
            126 => 
            array (
                'id' => 133,
                'sub_field_name' => 'VTC - Irrigation',
                'sub_field_code' => 'VTC0029',
                'tvet_field' => 11,
            ),
            127 => 
            array (
                'id' => 134,
                'sub_field_name' => 'VTC - Agricultural Mechanics',
                'sub_field_code' => 'VTC0029',
                'tvet_field' => 11,
            ),
        ));
        
        
    }
}