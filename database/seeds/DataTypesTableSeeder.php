<?php

use Illuminate\Database\Seeder;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data_types')->delete();
        
        \DB::table('data_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'User',
                'display_name_plural' => 'Users',
                'icon' => 'voyager-person',
                'model_name' => 'App\\User',
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2018-05-12 18:13:40',
                'updated_at' => '2018-05-12 18:13:40',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'TCG\\Voyager\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2018-05-12 18:13:40',
                'updated_at' => '2018-05-12 18:13:40',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'TCG\\Voyager\\Models\\Role',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2018-05-12 18:13:40',
                'updated_at' => '2018-05-12 18:13:40',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'schools',
                'slug' => 'schools',
                'display_name_singular' => 'School',
                'display_name_plural' => 'Schools',
                'icon' => 'voyager-bookmark',
                'model_name' => 'App\\School',
                'policy_name' => NULL,
                'controller' => 'Voyager\\SchoolsController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-12 19:42:46',
                'updated_at' => '2018-05-12 20:25:17',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'sectors',
                'slug' => 'sectors',
                'display_name_singular' => 'Sector',
                'display_name_plural' => 'Sectors',
                'icon' => 'voyager-magnet',
                'model_name' => 'App\\Sector',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 08:55:42',
                'updated_at' => '2018-05-21 08:55:42',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'sub_sectors',
                'slug' => 'sub-sectors',
                'display_name_singular' => 'Sub Sector',
                'display_name_plural' => 'Sub Sectors',
                'icon' => 'voyager-belt',
                'model_name' => 'App\\SubSector',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 08:59:31',
                'updated_at' => '2018-05-21 08:59:31',
            ),
            6 => 
            array (
                'id' => 8,
                'name' => 'rtqfs',
                'slug' => 'rtqfs',
                'display_name_singular' => 'Rtqf',
                'display_name_plural' => 'Rtqfs',
                'icon' => 'voyager-boat',
                'model_name' => 'App\\Rtqf',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 09:08:48',
                'updated_at' => '2018-05-21 09:08:48',
            ),
            7 => 
            array (
                'id' => 9,
                'name' => 'qualifications',
                'slug' => 'qualifications',
                'display_name_singular' => 'Qualification',
                'display_name_plural' => 'Qualifications',
                'icon' => 'voyager-study',
                'model_name' => 'App\\Qualification',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 09:25:52',
                'updated_at' => '2018-05-21 09:25:52',
            ),
            8 => 
            array (
                'id' => 10,
                'name' => 'qualification_statuses',
                'slug' => 'qualification-statuses',
                'display_name_singular' => 'Qualification Status',
                'display_name_plural' => 'Qualification Statuses',
                'icon' => 'voyager-anchor',
                'model_name' => 'App\\QualificationStatus',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 09:40:22',
                'updated_at' => '2018-05-21 09:40:22',
            ),
            9 => 
            array (
                'id' => 11,
                'name' => 'modules',
                'slug' => 'modules',
                'display_name_singular' => 'Module',
                'display_name_plural' => 'Modules',
                'icon' => 'voyager-book',
                'model_name' => 'App\\Module',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 10:04:16',
                'updated_at' => '2018-05-21 10:04:16',
            ),
            10 => 
            array (
                'id' => 12,
                'name' => 'competence_types',
                'slug' => 'competence-types',
                'display_name_singular' => 'Module Type',
                'display_name_plural' => 'Module Types',
                'icon' => 'voyager-plug',
                'model_name' => 'App\\CompetenceType',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 13:11:11',
                'updated_at' => '2018-05-21 13:16:29',
            ),
            11 => 
            array (
                'id' => 13,
                'name' => 'module_types',
                'slug' => 'module-types',
                'display_name_singular' => 'Module Type',
                'display_name_plural' => 'Module Types',
                'icon' => 'voyager-plug',
                'model_name' => 'App\\ModuleType',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null}',
                'created_at' => '2018-05-21 13:29:54',
                'updated_at' => '2018-05-21 13:29:54',
            ),
        ));
        
        
    }
}