<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAcademicYearToRpAdmissionPrivatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('rp_admission_privates', 'academic_year'))
            return;

        Schema::table('rp_admission_privates', function (Blueprint $table) {
            $table->string('academic_year')->default(getCurrentAcademicYear());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('rp_admission_privates', 'academic_year'))
            return;

        Schema::table('rp_admission_privates', function (Blueprint $table) {
            $table->dropColumn('academic_year');
        });
    }
}
