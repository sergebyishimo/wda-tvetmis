<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditColumnsInMarksCategorizingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marks_categorizings', function (Blueprint $table) {
            $table->string('college_id')->nullable()->change();
            $table->string('lecturer_id')->nullable()->change();
            $table->string('program_id')->nullable()->change();
            $table->string('module_id')->nullable()->change();
            $table->string('academic_year')->nullable()->change();
            $table->string('year_of_study')->nullable()->change();
            $table->string('semester')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marks_categorizings', function (Blueprint $table) {
            $table->string('college_id')->change();
            $table->string('lecturer_id')->change();
            $table->string('program_id')->change();
            $table->string('module_id')->change();
            $table->string('academic_year')->change();
            $table->string('year_of_study')->change();
            $table->string('semester')->change();
        });
    }
}
