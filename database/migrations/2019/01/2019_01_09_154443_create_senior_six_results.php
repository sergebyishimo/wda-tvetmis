<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeniorSixResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senior_six_results', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('exam_year');
            $table->string('family_name', 50);
            $table->string('other_names', 50);
            $table->string('sex', 50);
            $table->string('index', 50);
            $table->string('school_name', 100);

            $table->string('exam1',50);
            $table->string('grade1',50);
            $table->string('exam2',50);
            $table->string('grade2',50);
            $table->string('exam3',50);
            $table->string('grade3',50);
            $table->string('exam4',50);
            $table->string('grade4',50);
            $table->string('exam5',50);
            $table->string('grade5',50);
            $table->string('exam6',50);
            $table->string('grade6',50);
            $table->string('exam7',50);
            $table->string('grade7',50);
            $table->string('exam8',50);
            $table->string('grade8',50);
            $table->string('exam9',50);
            $table->string('grade9',50);
            $table->string('exam10',50);
            $table->string('grade10',50);
            $table->string('exam11',50);
            $table->string('grade11',50);
            $table->string('exam12',50);
            $table->string('grade12',50);
            $table->string('exam13',50);
            $table->string('grade13',50);

            $table->smallInteger('tot');
            $table->string('mention', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('senior_six_results');
    }
}
