<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleColumnToCollegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('colleges', 'roles'))
            Schema::table('colleges', function (Blueprint $table) {
                $table->string('roles')->default('d');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('colleges', 'roles'))
            Schema::table('colleges', function (Blueprint $table) {
                $table->dropColumn('roles');
            });
    }
}
