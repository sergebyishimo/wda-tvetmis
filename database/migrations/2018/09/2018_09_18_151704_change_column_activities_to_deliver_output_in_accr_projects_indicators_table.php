<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnActivitiesToDeliverOutputInAccrProjectsIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accr_mysql')->table('projects_indicators', function (Blueprint $table) {
            $table->text('activities_to_deliver_output')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->table('projects_indicators', function (Blueprint $table) {
            $table->text('activities_to_deliver_output')->change();
        });
    }
}
