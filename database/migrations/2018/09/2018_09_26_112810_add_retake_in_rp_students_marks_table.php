<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRetakeInRpStudentsMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rp_student_marks', function (Blueprint $table) {
            $table->integer('retake')->nullable()->after('academic_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_student_marks', function (Blueprint $table) {
            $table->dropColumn('retake');
        });
    }
}
