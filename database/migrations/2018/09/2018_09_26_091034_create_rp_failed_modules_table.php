<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpFailedModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('rp_failed_modules')) {
            Schema::create('rp_failed_modules', function (Blueprint $table) {
                $table->integer('id')->autoIncrement();
                $table->integer('college_id');
                $table->string('student_reg');
                $table->string('academic_year');
                $table->integer('module_id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rp_failed_modules');
    }
}
