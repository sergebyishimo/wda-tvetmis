<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('programs'))
            return;
        Schema::connection('rp_mysql')->create('programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id');
            $table->string('program_name');
            $table->string('program_code')->nullable();
            $table->integer('program_load')->nullable();;
            $table->integer('rtqf_level')->nullable();
            $table->string('program_is_stem')->nullable();
            $table->string('program_status')->nullable();;
            $table->string('level_applied')->nullable();;
            $table->string('tuition_fees')->nullable();
            $table->string('other_course_fees')->nullable();
            $table->longText('admission_requirements')->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('programs');
    }
}
