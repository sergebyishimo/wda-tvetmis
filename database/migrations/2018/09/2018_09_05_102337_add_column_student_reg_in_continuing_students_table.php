<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStudentRegInContinuingStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasColumn('continuing_students', 'student_reg'))
            return;

        Schema::connection('rp_mysql')->table('continuing_students', function (Blueprint $table) {
            $table->string('student_reg')->unique()->nullable()->after('registration_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('rp_mysql')->hasColumn('continuing_students', 'student_reg'))
            return;

        Schema::connection('rp_mysql')->table('continuing_students', function (Blueprint $table) {
            $table->dropColumn('student_reg');
        });
    }
}
