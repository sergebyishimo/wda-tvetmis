<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPrivateInRpStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasColumn('students', 'sponsorship'))
            return;

        Schema::connection('rp_mysql')->table('students', function (Blueprint $table) {
            $table->string('sponsorship')->nullable()->after('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasColumn('students', 'sponsorship'))
            return;

        Schema::connection('rp_mysql')->table('students', function (Blueprint $table) {
            $table->dropColumn('sponsorship');
        });
    }
}
