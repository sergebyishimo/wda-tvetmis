<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('college_modules', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('college_id');
            $table->integer('option_id');
            $table->integer('year_of_study');
            $table->integer('semester');
            $table->string('module_code');
            $table->string('module_name');
            $table->integer('credits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('college_modules');
    }
}
