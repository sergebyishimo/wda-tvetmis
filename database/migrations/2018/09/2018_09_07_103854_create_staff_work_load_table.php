<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffWorkLoadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_staff_workloads')) {
            return;
        }
        Schema::connection('accr_mysql')->create('source_staff_workloads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('staff_id');
            $table->string('position_id');
            $table->string('compnent_id');
            $table->string('academic_year');
            $table->string('work_load_hours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_staff_workloads');
    }
}
