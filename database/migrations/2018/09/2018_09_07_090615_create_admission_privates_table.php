<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionPrivatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->create('admission_privates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('college_id');
            $table->integer('department_id')->nullable();
            $table->integer('course_id')->nullable();
            $table->string('std_id', 50)->unique();
            $table->string('first_name', 100)->nullable();
            $table->string('other_names', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->integer('phone')->nullable();
            $table->string('gender', 11)->nullable();
            $table->integer('ubudehe')->nullable();
            $table->date('dob')->nullable();
            $table->integer('want_student_loan')->nullable();
            $table->integer('parents_phone')->nullable();
            $table->string('national_id_number', 25)->nullable();
            $table->string('province', 100)->nullable();
            $table->string('district', 100)->nullable();
            $table->string('sector', 100)->nullable();
            $table->string('cell', 100)->nullable();
            $table->string('village', 100)->nullable();
            $table->integer('bank_slip_number')->nullable();
            $table->string('scanned_bank_slip')->nullable();
            $table->string('photo')->nullable();
            $table->string('scan_of_diploma_or_certificate')->nullable();
            $table->integer('your_bank')->nullable();
            $table->string('your_bank_account', 191)->nullable();
            $table->integer('disability')->nullable();
            $table->string('examiner', 191)->nullable();
            $table->string('index_number', 191)->nullable();
            $table->string('school_attended', 191)->nullable();
            $table->string('graduation_year', 5)->nullable();
            $table->integer('option_offered')->nullable();
            $table->integer('aggregates_obtained')->nullable();
            $table->integer('payment_verified')->default(1);
            $table->string('scan_national_id_passport')->nullable();
            $table->string('year_of_study', 191)->nullable();
            $table->string('sponsorship_status')->default('private');
            $table->unsignedInteger('country')->nullable();
            $table->boolean('admission')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('admission_privates');
    }
}
