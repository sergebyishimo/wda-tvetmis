<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBoardingOrDayFieldToVarchar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accr_schools_information', function (Blueprint $table) {
            $table->string('boarding_or_day')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accr_schools_information', function (Blueprint $table) {
            $table->integer('boarding_or_day')->change();
        });
    }
}
