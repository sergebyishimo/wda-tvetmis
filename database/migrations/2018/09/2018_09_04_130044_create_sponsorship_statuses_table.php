<?php

use App\Rp\SponsorshipStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorshipStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('sponsorship_statuses'))
            return;

        Schema::connection('rp_mysql')->create('sponsorship_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        if (DB::connection('rp_mysql')->table('sponsorship_statuses')->count() <= 0) {
            DB::connection('rp_mysql')->table('sponsorship_statuses')->insert([
                ['name' => 'government'],
                ['name' => 'private']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsorship_statuses');
    }
}
