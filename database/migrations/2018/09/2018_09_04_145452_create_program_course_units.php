<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramCourseUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('program_course_units'))
            return;
        Schema::connection('rp_mysql')->create('program_course_units', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->nullable();
            $table->string('course_unit_code')->nullable();
            $table->string('course_unit_name')->nullable();
            $table->integer('credit_unit')->nullable();
            $table->integer('level_of_study_id')->nullable();
            $table->integer('semester_id')->nullable();
            $table->integer('unit_class_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('program_course_units');
    }
}
