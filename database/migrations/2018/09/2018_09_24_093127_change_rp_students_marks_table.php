<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRpStudentsMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('rp_student_marks', 'program_id')) {
            Schema::table('rp_student_marks', function (Blueprint $table) {
                $table->dropColumn('program_id');
            });
        }
        if (!Schema::hasColumn('rp_student_marks', 'college_id')) {
            Schema::table('rp_student_marks', function (Blueprint $table) {
                $table->integer('college_id')->after('id');
            });
        }
        if (!Schema::hasColumn('rp_student_marks', 'semester')) {
            Schema::table('rp_student_marks', function (Blueprint $table) {
                $table->integer('semester')->after('module_id');
            });
        }
        if (Schema::hasColumn('rp_student_marks', 'program_id')) {
            Schema::table('rp_student_marks', function (Blueprint $table) {
                $table->string('program_id')->nullable()->change();
            });
        }
        if (Schema::hasColumn('rp_student_marks', 'module_result')) {
            if (!Schema::hasColumn('rp_student_marks', 'obtained_marks')) {
                Schema::table('rp_student_marks', function (Blueprint $table) {
                    $table->renameColumn('module_result', 'obtained_marks');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_student_marks', function (Blueprint $table) {
            $table->text('program_id');
            $table->dropColumn('college_id');
            $table->dropColumn('semester');
            $table->string('program_id')->nullable(false)->change();
            $table->renameColumn('obtained_marks', 'module_result');
        });
    }
}
