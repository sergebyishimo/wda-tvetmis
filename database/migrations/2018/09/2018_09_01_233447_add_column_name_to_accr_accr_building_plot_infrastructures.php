<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNameToAccrAccrBuildingPlotInfrastructures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasColumn('accr_building_plot_infrastructures', 'name'))
            return;

        Schema::connection('accr_mysql')->table('accr_building_plot_infrastructures', function (Blueprint $table){
            $table->string('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('accr_building_plot_infrastructures', 'name'))
            return;

        Schema::connection('accr_mysql')->table('accr_building_plot_infrastructures', function (Blueprint $table){
            $table->dropColumn('name');
        });
    }
}
