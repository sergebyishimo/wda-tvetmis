<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffWorkLoadComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('source_staff_workload_components', function (Blueprint $table) {
            $table->increments('id');
            $table->string('work_load_component');
            $table->string('position_id');
            $table->string('component_description');
            $table->string('recommended_hours');
            $table->string('recommended_weeks');
            $table->string('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_staff_workload_components');
    }
}
