<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProgramIdToOptionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rp_continuing_students', function (Blueprint $table) {
            $table->renameColumn('program_id', 'option_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_continuing_students', function (Blueprint $table) {
            $table->renameColumn('option_id', 'program_id');
        });
    }
}
