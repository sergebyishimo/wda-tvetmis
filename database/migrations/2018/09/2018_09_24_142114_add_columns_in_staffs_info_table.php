<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInStaffsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->string('sector_taught')->nullable();
            $table->string('sub_sector_taught')->nullable();
            $table->string('qualification_taught')->nullable();
            $table->string('modules_taught')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->dropColumn([
                'sector_taught',
                'sub_sector_taught',
                'qualification_taught',
                'modules_taught'
            ]);
        });
    }
}
