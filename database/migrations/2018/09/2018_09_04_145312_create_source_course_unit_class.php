<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceCourseUnitClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('source_course_unit_class'))
            return;
        Schema::connection('rp_mysql')->create('source_course_unit_class', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_unit_class')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('source_course_unit_class');
    }
}
