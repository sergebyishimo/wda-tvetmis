<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsFromAndReleasedInRpTransferRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('transfer_requests', function (Blueprint $table) {
            $table->string('from')->nullable();
            $table->string('released')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('transfer_requests', function (Blueprint $table) {
            $table->dropColumn('from');
            $table->dropColumn('released');
        });
    }
}
