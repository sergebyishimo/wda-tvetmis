<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTrainersModulesTaught extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_trainers_modules_taught', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('staff_id');
            $table->string('sector_id');
            $table->string('sub_sector_id');
            $table->string('qualification_id');
            $table->string('module_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_trainers_modules_taught');
    }
}
