<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('college_departments'))
            return;

        Schema::connection('rp_mysql')->create('college_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('college_id');
            $table->integer('program_id');
            $table->string('department_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('college_departments');
    }
}
