<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMandatoryInRpSourceFeesCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasColumn('source_fees_category', 'mandatory'))
            return;
        Schema::connection('rp_mysql')->table('source_fees_category', function (Blueprint $table) {
            $table->boolean('mandatory')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('rp_mysql')->hasColumn('source_fees_category', 'mandatory'))
            return;

        Schema::connection('rp_mysql')->table('source_fees_category', function (Blueprint $table) {
            $table->dropColumn('mandatory');
        });
    }
}
