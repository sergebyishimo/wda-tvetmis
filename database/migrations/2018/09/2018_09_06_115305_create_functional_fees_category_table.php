<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionalFeesCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('source_functional_fees_category'))
            return;

        Schema::connection('rp_mysql')->create('source_functional_fees_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fee_id');
            $table->integer('fee_category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('source_functional_fees_category');
    }
}
