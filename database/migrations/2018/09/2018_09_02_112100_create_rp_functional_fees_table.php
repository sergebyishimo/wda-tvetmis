<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpFunctionalFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('functional_fees'))
            return;
        Schema::connection('rp_mysql')->create('functional_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fee_name')->nullable();
            $table->string('govt_sponsored_amount')->nullable();
            $table->string('private_sponsored_amount')->nullable();
            $table->string('paid_when')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('functional_fees');
    }
}
