<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpContinuingStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('rp_mysql')->hasTable('continuing_students'))
            return;
        Schema::connection('rp_mysql')->create('continuing_students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_number');
            $table->string('college_id');
            $table->string('program_id');
            $table->string('sponsor_id');
            $table->string('year_of_study');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('continuing_students');
    }
}
