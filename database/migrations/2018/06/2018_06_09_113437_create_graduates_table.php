<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraduatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('graduates'))
            return;

        Schema::connection('accr_mysql')->create('graduates', function (Blueprint $table) {
            
            $table->integer('id')->autoIncrement();
            $table->uuid('school_id');
            $table->integer('year');
            $table->string('names', 80);
            $table->string('gender', 20);
            $table->integer('trade')->nullable($value = true);;
            $table->integer('rtqf_achieved')->nullable($value = true);;
            $table->string('phone_number', 20)->nullable($value = true);;
            $table->string('employment_status', 50)->nullable($value = true);;
            $table->string('employment_timing', 50)->nullable($value = true);;
            $table->string('monthly_salary', 50)->nullable($value = true);;
            $table->string('name_of_employment_company', 50)->nullable($value = true);;
            $table->integer('sector')->nullable($value = true);;
            $table->integer('sub_sector')->nullable($value = true);;
            $table->string('employment_contact', 70)->nullable($value = true);;
            $table->string('knowledge_level', 50)->nullable($value = true);;
            $table->string('attitude_level', 50)->nullable($value = true);;
            $table->string('skill_level', 50)->nullable($value = true);;

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('graduates');
    }
}
