<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRtqfSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('rtqf_source'))
            return;

        Schema::connection('accr_mysql')->create('rtqf_source', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('level_name',100)->nullable($value = true);
            $table->string('level_description', 500)->nullable($value = true);
            $table->string('qualification_type', 200)->nullable($value = true);

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rtqf_source');
    }
}
