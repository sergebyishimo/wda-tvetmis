<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('ap_indicators'))
            return;

        Schema::connection('accr_mysql')->create('ap_indicators', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('result_id');
            $table->text('name');
            $table->text('baseline')->nullable($value = true);
            $table->string('q_one_target', 20)->nullable($value = true);
            $table->string('q_one_actual', 20)->nullable($value = true);
            $table->string('q_two_target', 20)->nullable($value = true);
            $table->string('q_two_actual', 20)->nullable($value = true);
            $table->string('q_three_target', 20)->nullable($value = true);
            $table->string('q_three_actual', 20)->nullable($value = true);
            $table->string('q_four_target', 20)->nullable($value = true);
            $table->string('q_four_actual', 20)->nullable($value = true);
            $table->text('activities_to_deliver_output')->nullable($value = true);
            $table->text('stakeholders')->nullable($value = true);
            $table->text('available')->nullable($value = true);
            $table->text('amount_spent')->nullable($value = true);
            $table->text('balance')->nullable($value = true);
            $table->text('narrative_support')->nullable($value = true);
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('result_id')->references('id')->on('ap_results')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('ap_indicators');
    }
}
