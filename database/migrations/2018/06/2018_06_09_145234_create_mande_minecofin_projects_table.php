<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandeMinecofinProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('mande_minecofin_projects'))
            return;

        Schema::connection('accr_mysql')->create('mande_minecofin_projects', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('project_name',100);
            $table->string('project_manager',100);
            $table->string('duration',70)->nullable($value = true);
            $table->text('brief_description')->nullable($value = true);
            $table->string('attachment_name',100)->nullable($value = true);
            $table->string('finance_type',100);
            $table->string('development_partner',100);
            $table->string('budget_agency_name',100)->nullable($value = true);
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

        });

        DB::select("INSERT INTO `accr_mande_minecofin_projects` (`id`, `project_name`, `project_manager`, `brief_description`, `attachment_name`, `finance_type`, `development_partner`, `budget_agency_name`) VALUES (1, 'TA Pool Fund', 'James Mwijuke', 'Capacity Building', '', '4;2;1', '3;1', 'Workforce Development Agency (WDA)');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('mande_minecofin_projects');
    }
}
