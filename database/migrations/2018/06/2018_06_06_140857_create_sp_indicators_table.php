<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('sp_indicators'))
            return;

        Schema::connection('accr_mysql')->create('sp_indicators', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('result_id');
            $table->text('name');
            $table->text('baseline')->nullable($value = true);
            $table->string('year_one', 20)->nullable($value = true);
            $table->string('year_one_target', 20)->nullable($value = true);
            $table->string('year_one_actual', 20)->nullable($value = true);
            $table->string('year_two', 20)->nullable($value = true);
            $table->string('year_two_target', 20)->nullable($value = true);
            $table->string('year_two_actual', 20)->nullable($value = true);
            $table->string('year_three', 20)->nullable($value = true);
            $table->string('year_three_target', 20)->nullable($value = true);
            $table->string('year_three_actual', 20)->nullable($value = true);
            $table->string('year_four', 20)->nullable($value = true);
            $table->string('year_four_target', 20)->nullable($value = true);
            $table->string('year_four_actual', 20)->nullable($value = true);
            $table->string('year_five', 20)->nullable($value = true);
            $table->string('year_five_target', 20)->nullable($value = true);
            $table->string('year_five_actual', 20)->nullable($value = true);
            $table->string('year_six', 20)->nullable($value = true);
            $table->string('year_six_target', 20)->nullable($value = true);
            $table->string('year_six_actual', 20)->nullable($value = true);
            $table->string('year_seven', 20)->nullable($value = true);
            $table->string('year_seven_target', 20)->nullable($value = true);
            $table->string('year_seven_actual', 20)->nullable($value = true);
            
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('result_id')->references('id')->on('sp_results')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('sp_indicators');
    }
}
