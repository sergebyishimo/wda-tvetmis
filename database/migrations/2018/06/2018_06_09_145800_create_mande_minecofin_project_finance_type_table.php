<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandeMinecofinProjectFinanceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('mande_minecofin_project_finance_type'))
            return;

        Schema::connection('accr_mysql')->create('mande_minecofin_project_finance_type', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('finance_type');
            $table->integer('development_partner');
            $table->string('start_date', 20);
            $table->string('initial_end_date', 20);
            $table->string('current_end_date', 20);
            $table->biginteger('project_total_cost');
            $table->biginteger('approved_budget_financial_law');
            $table->biginteger('approved_budget_by_development_partners');
            $table->string('reporting_period_end_date', 20);
            $table->biginteger('annual_spending_at_reporting_period_end_date');
            $table->biginteger('cummulative_spending_at_reporting_period_end_date');
            $table->float('annual_execution_rate_financial_law');
            $table->float('cummulative_execution_rate');
            $table->float('time_execution_rate');
            $table->float('estimated_physical_progress');
            $table->text('actual_achievement_each_output');
            $table->text('narrative_project_performance_rating');
            $table->integer('project_name');
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('finance_type')->references('id')->on('mande_minecofin_project_finance_type_source')->onDelete('cascade');
            $table->foreign('development_partner')->references('id')->on('mande_minecofin_development_partner')->onDelete('cascade');
        });



        DB::select("INSERT INTO `accr_mande_minecofin_project_finance_type` (`id`, `finance_type`, `development_partner`, `start_date`, `initial_end_date`, `current_end_date`, `project_total_cost`, `approved_budget_financial_law`, `approved_budget_by_development_partners`, `reporting_period_end_date`, `annual_spending_at_reporting_period_end_date`, `cummulative_spending_at_reporting_period_end_date`, `annual_execution_rate_financial_law`, `cummulative_execution_rate`, `time_execution_rate`, `estimated_physical_progress`, `actual_achievement_each_output`, `narrative_project_performance_rating`, `project_name`) VALUES (6, 4, 3, '2012-07-02', '2016-07-03', '2016-07-22', 500000000, 300000000, 200000000, '2016-07-04', 903000000, 68888888, 38.9, 56, 0, 0, '', '', 1),(7, 2, 1, '2016-07-03', '2016-07-03', '2016-07-29', 2000000000, 1500000000, 0, '2016-07-05', 0, 0, 0, 96, 0, 0, '', '', 1),(8, 1, 2, '2016-07-11', '2016-07-03', '2016-07-08', 0, 0, 0, '2016-07-06', 0, 0, 0, 23, 0, 0, '', '', 1);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('mande_minecofin_project_finance_type');
    }
}
