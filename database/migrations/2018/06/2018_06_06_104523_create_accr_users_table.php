<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('users'))
            return;

        Schema::connection('accr_mysql')->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->nullable($value = true);
            $table->string('district_id',100);
            $table->string('name', 80);
            $table->string('email', 80);
            $table->text('password');
            $table->text('remember_token')->nullable($value = true);

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('users');
    }
}
