<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameStdRegNoColumnToReceiverFromOutboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("outbox", function (Blueprint $table) {
            $table->renameColumn("std_reg_no", "receiver");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outbox', function (Blueprint $table) {
            $table->renameColumn('receiver', 'std_reg_no');
        });
    }
}
