<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceInOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_in_outs', function (Blueprint $table) {
            $table->uuid('id');
            $table->date('date')->nullable();
            $table->time('cometime')->nullable();
            $table->time('leavetime')->nullable();
            $table->uuid('student_id')->nullable();
            $table->uuid('school_id')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_in_outs');
    }
}
