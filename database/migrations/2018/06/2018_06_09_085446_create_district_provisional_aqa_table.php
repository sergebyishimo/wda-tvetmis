<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictProvisionalAqaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('district_provisional_aqa'))
            return;

        Schema::connection('accr_mysql')->create('district_provisional_aqa', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('application_id');
            
            $table->integer('section');
            $table->integer('step');
            $table->text('strength')->nullable($value = true);
            $table->text('weakness')->nullable($value = true);
            $table->text('opportunity')->nullable($value = true);
            $table->text('threat')->nullable($value = true);
            $table->text('recommendation')->nullable($value = true);
            $table->integer('marks')->nullable($value = true);

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('application_id')->references('id')->on('accr_applications')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('district_provisional_aqa');
    }
}
