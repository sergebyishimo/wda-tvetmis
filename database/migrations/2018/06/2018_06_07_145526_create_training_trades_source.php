<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingTradesSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('training_trades_source'))
            return;

        Schema::connection('accr_mysql')->create('training_trades_source', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('sub_field_name',80);
            $table->integer('sector_id');
            $table->string('sub_field_code',50)->nullable($value = true);

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('sector_id')->references('id')->on('training_sectors_source')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('training_trades_source');
    }
}
