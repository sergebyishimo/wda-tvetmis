<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsOfferedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('programs_offered'))
            return;

        Schema::connection('accr_mysql')->create('programs_offered', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();

            $table->uuid('school_id');
            $table->integer('sector_id');
            $table->integer('sub_sector_id');
            $table->integer('qualification_id');
            $table->integer('rtqf_level_id')->nullable($value = true);
            $table->integer('male_trainees')->nullable($value = true);
            $table->integer('female_trainees')->nullable($value = true);
            $table->text('curriculum')->nullable($value = true);
            $table->string('district_verified', 10)->nullable($value = true);
            $table->string('wda_verified', 10)->nullable($value = true);
            
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

//            $table->foreign('school_id')->references('id')->on('schools_information')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('programs_offered');
    }
}
