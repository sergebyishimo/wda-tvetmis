<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('staff_administrators'))
            return;

        Schema::connection('accr_mysql')->create('staff_administrators', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->uuid('school_id');
            $table->string('names',70);
            $table->string('gender',20)->nullable($value = true);
            $table->string('position',30)->nullable($value = true);
            $table->string('academic_qualification',80)->nullable($value = true);
            $table->string('institution_studied',70)->nullable($value = true);
            $table->integer('qualification_level_id');
            $table->text('teaching_experience')->nullable($value = true);
            $table->text('assessor_qualification')->nullable($value = true);
            $table->string('district_verified', 10)->nullable($value = true);
            $table->string('wda_verified', 10)->nullable($value = true);
            
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            
//            $table->foreign('school_id')->references('id')->on('schools_information')->onDelete('cascade');
//            $table->foreign('qualification_level_id')->references('id')->on('qualification_levels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('staffs_administrators');
    }
}
