<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingSectorsSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('training_sectors_source'))
            return;

        Schema::connection('accr_mysql')->create('training_sectors_source', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('tvet_field', 200);
            $table->string('field_code', 100)->nullable($value = true);

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_training_sectors_source');
    }
}
