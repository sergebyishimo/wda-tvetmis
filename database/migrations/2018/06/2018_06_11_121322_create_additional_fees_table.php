<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_fees', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('school_id');
            $table->uuid('department_id')->nullable();
            $table->uuid('student_id')->nullable();
            $table->string('label')->nullable();
            $table->string('amount')->nullable();
            $table->integer('acad_year');
            $table->integer('term')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_fees');
    }
}
