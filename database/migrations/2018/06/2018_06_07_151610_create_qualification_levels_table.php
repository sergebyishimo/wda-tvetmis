<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualificationLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('qualification_levels'))
            return;

        Schema::connection('accr_mysql')->create('qualification_levels', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('name', 70);
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('qualification_levels');
    }
}
