<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('projects_indicators'))
            return;

        Schema::connection('accr_mysql')->create('projects_indicators', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('result_id');
            $table->text('name');
            $table->text('baseline')->nullable($value = true);
            $table->string('year_one', 20)->nullable($value = true);
            $table->string('year_one_target', 20)->nullable($value = true);
            $table->string('year_one_actual', 20)->nullable($value = true);
            $table->string('year_two', 20)->nullable($value = true);
            $table->string('year_two_target', 20)->nullable($value = true);
            $table->string('year_two_actual', 20)->nullable($value = true);
            $table->string('year_three', 20)->nullable($value = true);
            $table->string('year_three_target', 20)->nullable($value = true);
            $table->string('year_three_actual', 20)->nullable($value = true);
            $table->string('year_four', 20)->nullable($value = true);
            $table->string('year_four_target', 20)->nullable($value = true);
            $table->string('year_four_actual', 20)->nullable($value = true);
            $table->string('year_five', 20)->nullable($value = true);
            $table->string('year_five_target', 20)->nullable($value = true);
            $table->string('year_five_actual', 20)->nullable($value = true);
            $table->string('year_six', 20)->nullable($value = true);
            $table->string('year_six_target', 20)->nullable($value = true);
            $table->string('year_six_actual', 20)->nullable($value = true);
            $table->string('year_seven', 20)->nullable($value = true);
            $table->string('year_seven_target', 20)->nullable($value = true);
            $table->string('year_seven_actual', 20)->nullable($value = true);
            
            $table->text('activities_to_deliver_output');
            $table->string('stakeholders', 100);
            $table->integer('budget');
            $table->integer('budget_spent');
            $table->text('narrative_progress');

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('result_id')->references('id')->on('project_results')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('projects_indicators');
    }
}
