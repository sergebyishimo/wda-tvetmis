<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrBuildingsPlotsInfrastructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('accr_building_plot_infrastructures'))
            return;

        Schema::connection('accr_mysql')->create('accr_building_plot_infrastructures', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('building_id');
            $table->integer('infrastructure_id');
            $table->integer('quantity')->nullable($value = true);

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('building_id')->references('id')->on('accr_buildings_and_plots')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_buildings_plots_infrastructures');
    }
}
