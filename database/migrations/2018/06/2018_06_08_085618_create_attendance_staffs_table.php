<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_staffs', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('school_id')->nullable();
            $table->date('date')->nulabble();
            $table->time('cometime')->nullable();
            $table->time('leavetime')->nullable();
            $table->uuid('staff_id')->nullable();
            $table->string('flag')->nullable();
            $table->integer('worktime')->nullable()->comment = "worktime in seconds";
            $table->integer('normal_wk')->nullable();
            $table->integer('late')->nullable();
            $table->integer('early')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_staffs');
    }
}
