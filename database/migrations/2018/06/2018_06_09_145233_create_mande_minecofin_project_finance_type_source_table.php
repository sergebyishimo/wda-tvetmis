<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandeMinecofinProjectFinanceTypeSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('mande_minecofin_project_finance_type_source'))
            return;

        Schema::connection('accr_mysql')->create('mande_minecofin_project_finance_type_source', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('finance_type', 100);
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
        });

        DB::select("INSERT INTO `accr_mande_minecofin_project_finance_type_source` (`id`, `finance_type`) VALUES (1, 'Loan'),(2, 'Grant'),(3, 'FI'),(4, 'CP');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('mande_minecofin_project_finance_type_source');
    }
}
