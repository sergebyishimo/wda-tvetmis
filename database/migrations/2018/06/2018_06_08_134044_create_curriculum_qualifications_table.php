<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculumQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('curriculum_qualifications'))
            return;

        Schema::connection('accr_mysql')->create('curriculum_qualifications', function (Blueprint $table) {
            
            $table->integer('id')->autoIncrement();
            $table->text('qualification_code')->nullable($value = true);
            $table->text('qualification_title');
            $table->integer('sector_id');
            $table->integer('sub_sector_id');
            $table->integer('rtqf_level_id');
            $table->string('credits',8)->nullable($value = true);
            $table->string('release_date', 30)->nullable($value = true);
            $table->integer('status')->nullable($value = true);
            $table->text('description')->nullable($value = true);
            $table->binary('attachment')->nullable($value = true);
            $table->text('occupational_profile_attachment')->nullable($value = true);
            $table->text('qualification_summary_attachment')->nullable($value = true);
            $table->text('job_related_information')->nullable($value = true);
            $table->longtext('entry_requirements')->nullable($value = true);
            $table->text('information_about_pathways')->nullable($value = true);
            $table->text('employability_and_life_skills')->nullable($value = true);
            $table->text('qualification_arrangement')->nullable($value = true);
            $table->text('attachment_name')->nullable($value = true);
            $table->text('occupational_profile_attachment_name')->nullable($value = true);
            $table->text('qualification_summary_attachment_name')->nullable($value = true);
            $table->text('competency_standards')->nullable($value = true);
            $table->text('tog_download')->nullable($value = true);
            $table->text('training_manual')->nullable($value = true);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('curriculum_qualifications');
    }
}
