<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrBuildingsAndPlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('accr_buildings_and_plots'))
            return;

        Schema::connection('accr_mysql')->create('accr_buildings_and_plots', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->uuid('school_id');
            $table->string('name', 70);
            $table->string('class',50)->nullable($value = true);
            $table->text('purpose')->nullable($value = true);
            $table->text('size')->nullable($value = true);
            $table->text('capacity')->nullable($value = true);
            $table->string('harvests_rain_water', 5);
            $table->text('construction_materials');
            $table->text('roofing_materials');
            $table->string('has_water', 5);
            $table->string('has_electricity', 5);
            $table->string('has_internet', 5);
            $table->string('has_extinguisher', 5);
            $table->string('has_lightening_arrestor', 5);
            $table->string('has_external_lighting', 5);
            $table->text('furniture')->nullable($value = true);
            $table->text('equipment')->nullable($value = true);
            $table->string('district_verified', 10)->nullable($value = true);
            $table->string('wda_verified', 10)->nullable($value = true);
            
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
//            $table->dropForeign(['school_id']);
//            $table->foreign('school_id')->references('id')->on('schools_information')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_buildings_and_plots');
    }
}
