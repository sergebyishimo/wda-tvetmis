<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrQualificationsAppliedForTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('accr_qualifications_applied_for'))
            return;

        Schema::connection('accr_mysql')->create('accr_qualifications_applied_for', function (Blueprint $table) {

            $table->integer('id')->autoIncrement();
            $table->integer('application_id');
            $table->integer('sector_id');
            $table->integer('sub_sector_id');
            $table->integer('rtqf_level_id');
            $table->integer('curriculum_qualification_id');

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('application_id')->references('id')->on('accr_applications')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_qualifications_applied_for');
    }
}
