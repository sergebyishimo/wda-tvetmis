<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('notifications'))
            return;

        Schema::connection('accr_mysql')->create('notifications', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->text('message');
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('notifications');
    }
}
