<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('sp_programs'))
            return;

        Schema::connection('accr_mysql')->create('sp_programs', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('rp_id');
            $table->integer('number');
            $table->text('name');
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('rp_id')->references('id')->on('sp_reporting_periods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('sp_programs');
    }
}
