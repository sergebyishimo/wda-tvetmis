<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('trainers'))
            return;

        Schema::connection('accr_mysql')->create('trainers', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->uuid('school_id');
            $table->string('names',70);
            $table->string('gender',10);
            $table->integer('qualification_level_id')->nullable($value = true);
            $table->string('qualification',70)->nullable($value = true);
            $table->integer('certificationId')->nullable($value = true);
            $table->text('experience')->nullable($value = true);
            $table->text('modules_taught')->nullable($value = true);
            $table->string('district_verified', 10)->nullable($value = true);
            $table->string('wda_verified', 10)->nullable($value = true);
            
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

//            $table->foreign('school_id')->references('id')->on('schools_information')->onDelete('cascade');
//            $table->foreign('qualification_level_id')->references('id')->on('qualification_levels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('trainers');
    }
}
