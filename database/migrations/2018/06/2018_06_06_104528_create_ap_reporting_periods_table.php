<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApReportingPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('ap_reporting_periods'))
            return;

        Schema::connection('accr_mysql')->create('ap_reporting_periods', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('period', 50);
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('ap_reporting_periods');
    }
}
