<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('sp_results'))
            return;

        Schema::connection('accr_mysql')->create('sp_results', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('program_id');
            $table->text('name');
            $table->text('verification_means');
            $table->text('assumptions');
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('program_id')->references('id')->on('sp_programs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('sp_results');
    }
}
