<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('accr_attachments'))
            return;

        Schema::connection('accr_mysql')->create('accr_attachments', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('application_id');
            $table->integer('attachment_id');
            $table->text('attachment');
            $table->string('district_verified', 10)->nullable($value = true);
            $table->string('wda_verified', 10)->nullable($value = true);
            $table->timestamps();

            $table->foreign('application_id')->references('id')->on('accr_applications')->onDelete('cascade');
            $table->foreign('attachment_id')->references('id')->on('accr_attachments_source')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_attachments');
    }
}
