<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrCriteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('accr_criterias'))
            return;

        Schema::connection('accr_mysql')->create('accr_criterias', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('criteria_section_id');
            $table->text('criteria');

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

            $table->foreign('criteria_section_id')->references('id')->on('accr_criteria_sections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_criterias');
    }
}
