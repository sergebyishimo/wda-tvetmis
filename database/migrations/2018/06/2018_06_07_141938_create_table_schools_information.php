<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSchoolsInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('schools_information'))
            return;

        Schema::connection('accr_mysql')->create('schools_information', function (Blueprint $table) {

            $table->uuid('id')->autoIncrement();
            $table->string('school_name', 70);
            $table->string('province', 50);
            $table->string('district', 50)->nullable($value = true);
            $table->string('sector', 50)->nullable($value = true);
            $table->string('cell', 50)->nullable($value = true);
            $table->string('village', 50)->nullable($value = true);
            $table->string('latitude', 50)->nullable($value = true);
            $table->string('longitude', 50)->nullable($value = true);
            $table->string('phone', 50)->nullable($value = true);
            $table->string('email', 50)->nullable($value = true);
            $table->string('school_status', 50)->nullable($value = true);
            $table->string('owner_name', 50)->nullable($value = true);
            $table->string('owner_phone', 50)->nullable($value = true);
            $table->string('owner_type', 50)->nullable($value = true);
            $table->string('owner_email', 50)->nullable($value = true);
            $table->string('manager_name', 50)->nullable($value = true);
            $table->string('manager_phone', 50)->nullable($value = true);
            $table->string('manager_email', 50)->nullable($value = true);
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('schools_information');
    }
}
