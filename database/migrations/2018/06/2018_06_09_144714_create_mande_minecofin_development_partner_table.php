<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandeMinecofinDevelopmentPartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('mande_minecofin_development_partner'))
            return;

        Schema::connection('accr_mysql')->create('mande_minecofin_development_partner', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('development_partner',100);
            $table->text('area_of_operation');
            $table->string('contact_person',100);
            $table->string('address_details',200);
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);
            $table->softDeletes();

        });

        DB::select("INSERT INTO `accr_mande_minecofin_development_partner` (`id`, `development_partner`, `area_of_operation`, `contact_person`, `address_details`) VALUES (1, 'KOICA - Korean Cooperation', 'Education and Infrastructure', 'KOICA Resident Represetnative', 'Kacyiru'), (2, 'JICA', 'Education and Infrastructure', 'KOICA Resident Represetnative', 'Kacyiru'), (3, 'GIZ', 'Education and Infrastructure', 'KOICA Resident Represetnative', 'Kacyiru'), (4, 'KfW', 'Education and Infrastructure', 'KOICA Resident Represetnative', 'Kacyiru'), (5, 'NUFFIC', 'Education and Infrastructure', 'KOICA Resident Represetnative', 'Kacyiru');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('mande_minecofin_development_partner');
    }
}
