<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSchoolOwnershipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::select("CREATE TABLE IF NOT EXISTS accr_source_school_ownership (id int(11) NOT NULL,owner varchar(255) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1");
        DB::select("INSERT INTO accr_source_school_ownership (id, owner) VALUES (1, 'AEBR'),(2, 'Protestant'),(3, 'Catholic'),(4, 'Islam'),(5, 'Adventist'),(6, 'Parents Association'),(7, 'NGO'),(8, 'Public'),(9, 'Private'),(10, 'Other'),(11, 'Eglise Vivante Gatenga')");
        DB::select("ALTER TABLE accr_source_school_ownership MODIFY id integer PRIMARY KEY AUTO_INCREMENT");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_school_ownership');
    }
}
