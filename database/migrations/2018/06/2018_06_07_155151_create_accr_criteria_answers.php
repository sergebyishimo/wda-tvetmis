<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrCriteriaAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('accr_criteria_answers'))
            return;

        Schema::connection('accr_mysql')->create('accr_criteria_answers', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->uuid('school_id');
            $table->integer('criteria_id');
            $table->string('answer', 5);

            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null);

//            $table->foreign('school_id')->references('id')->on('schools_information')->onDelete('cascade');
//            $table->foreign('criteria_id')->references('id')->on('accr_criterias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_criteria_answers');
    }
}
