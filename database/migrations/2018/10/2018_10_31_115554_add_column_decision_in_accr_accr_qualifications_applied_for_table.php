<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDecisionInAccrAccrQualificationsAppliedForTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accr_accr_qualifications_applied_for', function (Blueprint $table) {
            $table->string('decision')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accr_accr_qualifications_applied_for', function (Blueprint $table) {
            $table->dropColumn('decision');
        });
    }
}
