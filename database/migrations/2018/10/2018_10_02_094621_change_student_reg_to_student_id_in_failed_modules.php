<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStudentRegToStudentIdInFailedModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rp_failed_modules', function (Blueprint $table) {
            $table->renameColumn('student_reg', 'student_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_failed_modules', function (Blueprint $table) {
            $table->renameColumn('student_id', 'student_reg');
        });
    }
}
