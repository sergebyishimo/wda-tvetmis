<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeWronglyWrittenTableNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('mindeduc_quality_audit_sections','mineduc_quality_audit_sections');
        Schema::rename('mindeduc_quality_audit_indicators','mineduc_quality_audit_indicators');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //revert back
        Schema::rename('mineduc_quality_audit_sections','mindeduc_quality_audit_sections');
        Schema::rename('mineduc_quality_audit_indicators','mindeduc_quality_audit_indicators');
    }
}
