<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelRpStudentRegisteredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->create('student_registered', function (Blueprint $table) {
            $table->increments('id');
            $table->string('academic_year');
            $table->string('college_id');
            $table->string('student_id');
            $table->string('year_of_study');
            $table->boolean('prof')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('student_registered');
    }
}
