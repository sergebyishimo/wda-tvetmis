<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAddressToMultipleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rp_admitted_students', function (Blueprint $table){
            $table->string('address')->nullable();
        });
        Schema::table('rp_admission_privates', function (Blueprint $table){
            $table->string('address')->nullable();
        });
        Schema::table('rp_college_students', function (Blueprint $table){
            $table->string('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_admitted_students', function (Blueprint $table){
            $table->dropColumn('address');
        });
        Schema::table('rp_admission_privates', function (Blueprint $table){
            $table->dropColumn('address');
        });
        Schema::table('rp_college_students', function (Blueprint $table){
            $table->dropColumn('address');
        });
    }
}
