<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMultipleTimeInRpSourceFeesCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('source_fees_category', function (Blueprint $table) {
            $table->boolean('multiple_time')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('source_fees_category', function (Blueprint $table) {
            $table->dropColumn('multiple_time');
        });
    }
}
