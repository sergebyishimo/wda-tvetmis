<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentOutsidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->create('payment_outsides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('college_id');
            $table->string('student_id');
            $table->string('payment_category');
            $table->string('attachment');
            $table->string('academic_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('payment_outsides');
    }
}
