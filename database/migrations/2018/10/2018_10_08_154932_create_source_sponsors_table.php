<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('source_sponsors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sponsor')->nullable()->unique();
            $table->string('percentage')->nullable();
            $table->timestamps();
        });
        $sponsors = [
            'government', 'social', 'FARG', 'private',
            'rnp', 'reb-farg', 'CNLG', 'IPRC West', 'FAWE RWANDA', 'IPRC Kigali',
            'IPRC EAST(Volley ball Players)', 'IPRC EAST', 'CNLG/IPRC East', 'IPRCS/PLAYER',
            'IPRC-PLAYER', 'RDF', 'other'
        ];
        foreach ($sponsors as $sponsor) {
            $sponsor = strtolower($sponsor);
            $check = DB::table('source_sponsors')->where('sponsor', $sponsor)->first();
            if (!$check)
                \App\SourceSponsor::create([
                    'sponsor' => $sponsor
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_sponsors');
    }
}
