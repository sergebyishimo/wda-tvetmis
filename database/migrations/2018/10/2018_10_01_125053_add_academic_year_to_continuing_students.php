<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAcademicYearToContinuingStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rp_continuing_students', function (Blueprint $table) {
            $table->string('academic_year')->nullable(true)->after('year_of_study');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_continuing_students', function (Blueprint $table) {
            $table->dropColumn('academic_year');
        });
    }
}
