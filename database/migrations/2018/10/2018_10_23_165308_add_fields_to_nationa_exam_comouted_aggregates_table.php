<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToNationaExamComoutedAggregatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nationa_exam_computed_aggregates', function (Blueprint $table) {
            $table->string('school_id');
            $table->string('combination_id');
            $table->string('total_marks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nationa_exam_computed_aggregates', function (Blueprint $table) {
            $table->removeColumn('school_id');
            $table->removeColumn('combination_id');
            $table->removeColumn('total_marks');
        });
    }
}
