<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingFieldsOnComputedAggregatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nationa_exam_computed_aggregates', function (Blueprint $table) {
            $table->string("gender")->nullable();
            $table->string("province")->nullable();
            $table->string("district")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nationa_exam_computed_aggregates', function (Blueprint $table) {
            $table->dropColumn("gender");
            $table->dropColumn("province");
            $table->dropColumn("district");
        });
    }
}
