<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRepeatColumnToContinuingStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rp_continuing_students', function (Blueprint $table) {
            $table->boolean('repeat')->after('academic_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_continuing_students', function (Blueprint $table) {
            $table->dropColumn('repeat');
        });
    }
}
