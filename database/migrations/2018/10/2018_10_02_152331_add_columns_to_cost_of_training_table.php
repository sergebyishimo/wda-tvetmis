<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCostOfTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cost_of_training', function (Blueprint $table) {
            $table->string('budget_actually_available_for_salaries_per_year')->nullable();
            $table->string('estimated_operational_budget_required_per_year')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cost_of_training', function (Blueprint $table) {
            $table->dropColumn('budget_actually_available_for_salaries_per_year');
            $table->dropColumn('estimated_operational_budget_required_per_year');
        });
    }
}
