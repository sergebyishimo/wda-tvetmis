<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAmountAndDatePaidInRpPaymentOutsidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rp_payment_outsides', function (Blueprint $table) {
            $table->string('amount')->nullable();
            $table->string('date_paid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rp_payment_outsides', function (Blueprint $table) {
            $table->dropColumn('amount');
            $table->dropColumn('date_paid');
        });
    }
}
