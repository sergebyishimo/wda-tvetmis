<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInTransferRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('transfer_requests', function (Blueprint $table) {
            $table->integer("from_college_id")->nullable();
            $table->integer("from_department_id")->nullable();
            $table->integer("from_course_id")->nullable();
            $table->boolean('from_confirm')->nullable();
            $table->boolean('to_confirm')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('transfer_requests', function (Blueprint $table) {
            $table->dropColumn("from_college_id");
            $table->dropColumn("from_department_id");
            $table->dropColumn("from_course_id");
            $table->dropColumn('from_confirm');
            $table->dropColumn('to_confirm');
        });
    }
}
