<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsCbtToAccrCurriculumQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accr_mysql')->table('curriculum_qualifications', function (Blueprint $table) {
            $table->string('is_cbt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->table('curriculum_qualifications', function (Blueprint $table) {
            $table->dropColumn('is_cbt');
        });
    }
}
