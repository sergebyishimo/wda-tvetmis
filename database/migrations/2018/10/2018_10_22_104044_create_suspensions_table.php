<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuspensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rp_suspenssions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('college_id');
            $table->string('student_reg');
            $table->text('letter');
            $table->string('resuming_academic_year');
            $table->integer('resuming_semester');
            $table->string('decision')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rp_suspenssions');
    }
}
