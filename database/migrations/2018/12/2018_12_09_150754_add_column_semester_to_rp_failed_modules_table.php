<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSemesterToRpFailedModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('rp_failed_modules', 'semester'))
            return;

        Schema::table('rp_failed_modules', function (Blueprint $table) {
            $table->integer('semester')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('rp_failed_modules', 'semester'))
            return;

        Schema::table('rp_failed_modules', function (Blueprint $table) {
            $table->dropColumn('semester');
        });
    }
}
