<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAcademicYearToPendingPaymentInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('pending_payment_invoices', function (Blueprint $table) {
            $table->string('academic_year')->default('2018 - 2019');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('pending_payment_invoices', function (Blueprint $table) {
            $table->dropColumn('academic_year');
        });
    }
}
