<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_marks', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('college_id');
            $table->integer('student_id');
            $table->string('module_id');
            $table->string('category_id');
            $table->integer('semester');
            $table->integer('academic_year');
            $table->integer('year_of_study');
            $table->decimal('marks', 4,1)->default(0.0);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_marks');
    }
}
