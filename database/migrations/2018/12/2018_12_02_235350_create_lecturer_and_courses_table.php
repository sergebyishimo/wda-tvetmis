<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerAndCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_and_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('college_id')->nullable();
            $table->string('lecturer_id')->nullable();
            $table->string('course_id')->nullable();
            $table->string('department_id')->nullable();
            $table->string('program_id')->nullable();
            $table->string('year_of_study')->nullable();
            $table->string('semester')->nullable();
            $table->string('academic_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_and_courses');
    }
}
