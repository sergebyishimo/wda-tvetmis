<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpExamMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::connection('rp_mysql')->create('exam_marks', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('college_id');
            $table->string('student_id');
            $table->string('module_id');
            $table->integer('semester');
            $table->string('academic_year');
            $table->decimal('marks', 4, 1)->default(0.0);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('exam_marks');
    }
}
