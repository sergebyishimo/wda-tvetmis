<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffPositionSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->create('source_college_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position_name');
            $table->string('category')->nullable();
            $table->string('level')->nullable();
            $table->string('index')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->dropIfExists('staff_position_sources');
    }
}
