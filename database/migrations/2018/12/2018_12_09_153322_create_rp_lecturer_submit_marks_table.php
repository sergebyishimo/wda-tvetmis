<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpLecturerSubmitMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rp_lecturer_submit_marks', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('college_id');
            $table->string('lecturer_id');
            $table->string('academic_year');
            $table->string('semester');
            $table->string('module_id');
            $table->boolean('seen')->default(0);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rp_lecturer_submit_marks');
    }
}
