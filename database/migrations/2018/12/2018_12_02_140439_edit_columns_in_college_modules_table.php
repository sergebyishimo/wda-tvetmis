<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditColumnsInCollegeModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('college_modules', function (Blueprint $table) {
            $table->string('college_id')->nullable()->change();
            $table->string('academic_year')->nullable()->change();
            $table->string('department_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('college_modules', function (Blueprint $table) {
            $table->integer('college_id')->change();
            $table->string('academic_year')->change();
            $table->dropColumn('department_id');
        });
    }
}
