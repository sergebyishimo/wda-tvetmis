<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStudentIdToStringInCatMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_marks', function (Blueprint $table) {
            $table->string('student_id')->change();
            $table->string('academic_year')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_marks', function (Blueprint $table) {
            $table->integer('student_id')->change();
            $table->integer('academic_year')->change();
        });
    }
}
