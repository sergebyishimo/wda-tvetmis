<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksCategorizingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks_categorizings', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('college_id');
            $table->string('lecturer_id');
            $table->string('program_id');
            $table->string('module_id');
            $table->string('title');
            $table->string('max_marks');
            $table->string('academic_year');
            $table->string('date_taken')->nullable();
            $table->string('year_of_study');
            $table->string('semester');
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks_categorizings');
    }
}
