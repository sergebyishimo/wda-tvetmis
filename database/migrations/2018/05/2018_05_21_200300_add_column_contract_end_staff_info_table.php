<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnContractEndStaffInfoTable extends Migration
{
    /*/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staffs_info', function ($table){
            $table->date('contract_end')->nullable()->after('contract_start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staffs_info', function ($table){
            $table->dropColumn('contract_end');
        });
    }
}
