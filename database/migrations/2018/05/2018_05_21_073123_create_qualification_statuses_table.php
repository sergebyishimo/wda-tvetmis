<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualificationStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualification_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->uuid('qualification_id');
            $table->string("status")->default("ACTIVE"); #["ACTIVE", "INACTIVE"]
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualification_statuses');
    }
}
