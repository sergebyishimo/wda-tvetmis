<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('school');
            $table->text('prev_schools')->nullable();
            $table->string('reg_no')->unique();
            $table->string('fname');
            $table->string('lname');
            $table->uuid('level_id');
            $table->string('gender')->nullable();
            $table->string('mode')->nullable();
            $table->date('bdate')->nullable();
            $table->string('nationality')->nullable();
            $table->string('province')->nullable();
            $table->string('district')->nullable();
            $table->string('sector')->nullable();
            $table->string('cell')->nullable();
            $table->string('village')->nullable();
            $table->string('ft_name')->nullable();
            $table->string('ft_phone')->nullable();
            $table->string('mt_name')->nullable();
            $table->string('mt_phone')->nullable();
            $table->string('gd_name')->nullable();
            $table->string('gd_phone')->nullable();
            $table->string('orphan')->default('None'); #['None', 'Father', 'Mother', 'Both']
            $table->string('sponsor')->nullable();
            $table->string('sport')->nullable();
            $table->string('religion')->nullable();
            $table->string('status'); #['active', 'deleted', 'graduated']
            $table->year('acad_year')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
