<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('levels', function(Blueprint $table)
		{

			$table->uuid('id');
			$table->uuid('school_id');
			$table->uuid('department_id');
			$table->uuid('rtqf_id');
			$table->uuid('staffs_info_id')->nullable();
			$table->boolean('status')->default(true);
			$table->timestamps();

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('levels');
	}

}
