<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id');
			$table->uuid('level_id');
			$table->uuid('module_id');
			$table->integer('max_point')->nullable();
			$table->uuid('staffs_info_id')->nullable();
			$table->integer('term')->default(1);
			$table->boolean('status')->default(true);
			$table->timestamps();

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('courses');
	}

}
