<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_permissions', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('school_id');
            $table->uuid('student_id');
            $table->string('acad_year');
            $table->integer('term');
            $table->string('reason')->nullable();
            $table->string('destination')->nullable();
            $table->string('observation')->nullable();
            $table->string('issued_by')->nullable();
            $table->string('leaving_datetime')->nullable();
            $table->string('returning_datetime')->nullable();
            $table->string('leaving_sms')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_permissions');
    }
}
