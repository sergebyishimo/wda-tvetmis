<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivitTableStaffPrivileges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("staff_privileges", function (Blueprint $table){
            $table->increments("id");
            $table->uuid("school_id");
            $table->uuid("staff_info_id");
            $table->uuid("school_user_privilege_id");
            $table->boolean("authorized")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("staff_privileges");
    }
}
