<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplineHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discipline_histories', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('school_id');
            $table->uuid('student_id');
            $table->integer('term');
            $table->string('academic_year', 4);
            $table->integer('marks_had');
            $table->integer('marks');
            $table->string('fault');
            $table->string('comment');
            $table->string('done_by')->nullable();
            $table->boolean('sms')->default(true);

            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discipline_histories');
    }
}
