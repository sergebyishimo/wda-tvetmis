<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('qualification_id');
            $table->string('module_code')->unique();
            $table->string("module_title")->unique();
            $table->integer("credits")->default(1);
            $table->longText("competence")->nullable();
            $table->longText('module_class')->nullable();
            $table->string("learning_hours")->nullable();
            $table->string("attachment")->nullable();
            $table->timestamps();

            $table->softDeletes();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
