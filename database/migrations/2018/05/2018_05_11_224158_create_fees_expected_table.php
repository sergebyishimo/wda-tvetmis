<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeesExpectedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fees_expected', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id');
			$table->integer('term');
			$table->uuid('level_id');
			$table->integer('boarding');
			$table->integer('day');
			$table->timestamp('last_update')->default(null);

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('fees_expected');
	}

}
