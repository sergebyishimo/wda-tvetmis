<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('faults', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id');
			$table->text('name', 65535);
			$table->text('first_punishment_info', 65535)->nullable();
			$table->integer('first_marks')->nullable();
			$table->text('second_punishment_info', 65535)->nullable();
			$table->integer('second_marks')->nullable();
			$table->text('third_punishment_info', 65535)->nullable();
			$table->integer('third_marks')->nullable();
			$table->timestamps();

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('faults');
	}

}
