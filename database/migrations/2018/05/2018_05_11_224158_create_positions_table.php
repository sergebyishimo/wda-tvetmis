<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('positions', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id');
			$table->integer('acad_year');
			$table->integer('term');
			$table->string('std_reg_no', 20)->index('std_reg_no');
			$table->integer('position');
			$table->timestamp('last_update')->default(null);

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('positions');
	}

}
