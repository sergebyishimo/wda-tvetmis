<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffsInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staffs_info', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id')->nullable();
			$table->string('first_name', 70);
			$table->string('last_name', 70);
			$table->string('gender')->nullable(); #array('Male','Female')
			$table->string("civil_status");
			$table->string("national_id_number")->nullable();
			$table->string("staff_category")->nullable();
			$table->integer('privilege')->index('level_id');
			$table->string('qualification', 30)->nullable();
			$table->string('carrier', 70)->nullable();
			$table->string('phone_number', 20)->nullable();
			$table->string('email', 70)->nullable();
			$table->string('nationality', 30)->nullable();
			$table->string('province', 60)->nullable();
			$table->string('district', 40)->nullable();
			$table->string('sector', 40)->nullable();
			$table->string("photo")->nullable();
			$table->string("bank_name")->nullable();
			$table->string("bank_account_number")->nullable();
			$table->string("rssb_number")->nullable();
			$table->string("medical_insurance_scheme")->nullable();
			$table->string("medical_card_number")->nullable();
			$table->string("rfid_card")->unique()->nullable();
			$table->string('status')->default('active'); #array('active',"",'deleted')
			$table->timestamps();

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staffs_info');
	}

}
