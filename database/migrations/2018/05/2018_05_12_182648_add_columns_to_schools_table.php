<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function ($table){
            $table->string('acronym')->after('name');
            $table->string('studying_mode')->nullable()->after('acronym');
            $table->integer('discipline_max')->default(40)->after('studying_mode');
            $table->string('province')->nullable()->after('discipline_max');
            $table->string('district')->nullable()->after('province');
            $table->string('sector')->nullable()->after('district');
            $table->string('cell')->nullable()->after('sector');
            $table->string('village')->nullable()->after('cell');
            $table->string('phone_number')->nullable()->after('village');
            $table->string('website')->nullable()->after('email');
            $table->string('status')->nullable()->after('website');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn([
                'acronym', 'studying_mode',  'discipline_max', 'province',
                'district', 'sector', 'cell', 'village', 'phone_number',
                'website', 'status'
            ]);
        });
    }
}
