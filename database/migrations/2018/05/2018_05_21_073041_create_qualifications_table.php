<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualifications', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('qualification_code')->unique();
            $table->string('qualification_title')->unique();
            $table->uuid("sector_id");
            $table->uuid("sub_sector_id");
            $table->uuid("rtqf_id");
            $table->integer("credits")->default(1);
            $table->date('release_date')->nullable();
            $table->string("status")->default("ACTIVE"); #["ACTIVE", "INACTIVE"]
            $table->text("description")->nullable();
            $table->longText('occupational_profile')->nullable();
            $table->longText('qualification_summary')->nullable();
            $table->longText('job_related_information')->nullable();
            $table->longText('entry_requirements')->nullable();
            $table->longText('information_about_pathways')->nullable();
            $table->longText('employability_and_life_skills')->nullable();
            $table->longText('qualification_arrangement')->nullable();
            $table->longText('competency_standards')->nullable();
            $table->string('qualification_attachment')->nullable();
            $table->string('training_manual')->nullable();
            $table->string('trainees_manual')->nullable();
            $table->string('trainers_manual')->nullable();

            $table->timestamps();

            $table->softDeletes();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualifications');
    }
}
