<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeriodicMarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('periodic_marks', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id');
			$table->string('std_reg_no', 10);
			$table->integer('term');
			$table->integer('period');
			$table->string('marks_type', 50);
			$table->integer('course_id')->index('course_id');
			$table->decimal('obt_marks', 4, 1)->nullable();
			$table->integer('total');
			$table->integer('acad_year');
			$table->date('given_date');
			$table->timestamps();

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('periodic_marks');
	}

}
