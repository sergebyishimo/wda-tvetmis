<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotTableForQualificationAndRtqfsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("qualification_rtqfs", function (Blueprint $table){
            $table->increments('id');
            $table->uuid("qualification_id");
            $table->uuid("rtqf_id");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("qualification_rtqfs");
    }
}
