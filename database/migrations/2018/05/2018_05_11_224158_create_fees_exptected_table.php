<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeesExptectedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fees_exptected', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id');
			$table->integer('term');
			$table->integer('level_id')->index('level_id');
			$table->integer('boaring');
			$table->integer('day');

			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('fees_exptected');
	}

}
