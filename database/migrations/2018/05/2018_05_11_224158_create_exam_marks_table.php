<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamMarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exam_marks', function(Blueprint $table)
		{
			$table->uuid('id');
			$table->uuid('school_id');
			$table->string('std_reg_no');
			$table->integer('term');
			$table->uuid('course_id')->index('course_id');
			$table->decimal('obt_marks', 4, 1)->nullable();
			$table->integer('total');
			$table->integer('acad_year');
			$table->date('given_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('exam_marks');
	}

}
