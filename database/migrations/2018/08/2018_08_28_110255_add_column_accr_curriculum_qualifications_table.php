<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAccrCurriculumQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasColumn('curriculum_qualifications', 'uuid'))
            return;

        Schema::connection('accr_mysql')->table('curriculum_qualifications', function (Blueprint $table) {
            $table->string('uuid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('accr_mysql')->hasColumn('curriculum_qualifications', 'uuid'))
            return;

        Schema::connection('accr_mysql')->table('curriculum_qualifications', function (Blueprint $table) {
            $table->dropColumn('uuid');
        });
    }
}
