<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStudentsFemaleToAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'students_female'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('students_female')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'students_female'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('students_female');
            });
    }
}
