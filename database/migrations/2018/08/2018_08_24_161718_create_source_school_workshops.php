<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSchoolWorkshops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_school_workshops'))
            return;
        Schema::connection('accr_mysql')->create('source_school_workshops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('workshop_name');
            $table->uuid('school_id');
            $table->uuid('sector');
            $table->uuid('sub_sector');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_school_workshops');
    }
}
