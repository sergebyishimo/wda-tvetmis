<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNumberOfWorkshopsToAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'number_of_workshops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('number_of_workshops')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'number_of_workshops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_workshops');
            });
    }
}
