<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'has_trees'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('has_trees')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'has_school_farm'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('has_school_farm')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'provides_social_Services'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('provides_social_Services')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'provides_counseling'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('provides_counseling')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'has_functional_health_club'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('has_functional_health_club')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'sports_available_at_school'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('sports_available_at_school')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'number_of_students_fed_at_school'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('number_of_students_fed_at_school')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'parents_participate_feeding_program'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('parents_participate_feeding_program')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'has_feeding_program'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('has_feeding_program')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'number_of_computers_teachers'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('number_of_computers_teachers')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'number_of_computers_learners'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('number_of_computers_learners')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'number_of_computers_administration'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('number_of_computers_administration')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'has_school_development_plan'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('has_school_development_plan')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'has_school_maitanance_plan'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('has_school_maitanance_plan')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'number_of_laptops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('number_of_laptops')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'number_of_projectors'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('number_of_projectors')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'accreditation_status'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('accreditation_status')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'school_setting'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('school_setting')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'date_of_establishment'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->timestamp('date_of_establishment')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'school_ownership'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('school_ownership')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'post_office_box_number'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('post_office_box_number')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'website'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('website')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'boarding_or_day'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('boarding_or_day')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'accreditation_number'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('accreditation_number')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'accreditation_date'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->date('accreditation_date')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'school_activity'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('school_activity')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'school_logo'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('school_logo')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'operator'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('operator')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'discipline_totalmarks'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('discipline_totalmarks')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'sms'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('sms')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'school_acronym'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('school_acronym')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'sms_sender'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('sms_sender')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'school_moto'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('school_moto')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'school_code'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('school_code')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'has_trees'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('has_trees');
            });

        if (Schema::hasColumn('accr_schools_information', 'has_school_farm'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('has_school_farm');
            });

        if (Schema::hasColumn('accr_schools_information', 'provides_social_Services'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('provides_social_Services');
            });

        if (Schema::hasColumn('accr_schools_information', 'provides_counseling'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('provides_counseling');
            });

        if (Schema::hasColumn('accr_schools_information', 'has_functional_health_club'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('has_functional_health_club');
            });

        if (Schema::hasColumn('accr_schools_information', 'sports_available_at_school'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('sports_available_at_school');
            });

        if (Schema::hasColumn('accr_schools_information', 'number_of_students_fed_at_school'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_students_fed_at_school');
            });

        if (Schema::hasColumn('accr_schools_information', 'parents_participate_feeding_program'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('parents_participate_feeding_program');
            });

        if (Schema::hasColumn('accr_schools_information', 'has_feeding_program'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('has_feeding_program');
            });

        if (Schema::hasColumn('accr_schools_information', 'number_of_computers_teachers'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_computers_teachers');
            });

        if (Schema::hasColumn('accr_schools_information', 'number_of_computers_learners'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_computers_learners');
            });

        if (Schema::hasColumn('accr_schools_information', 'number_of_computers_administration'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_computers_administration');
            });

        if (Schema::hasColumn('accr_schools_information', 'has_school_development_plan'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('has_school_development_plan');
            });

        if (Schema::hasColumn('accr_schools_information', 'has_school_maitanance_plan'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('has_school_maitanance_plan');
            });

        if (Schema::hasColumn('accr_schools_information', 'number_of_laptops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_laptops');
            });

        if (Schema::hasColumn('accr_schools_information', 'number_of_projectors'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_projectors');
            });

        if (Schema::hasColumn('accr_schools_information', 'accreditation_status'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('accreditation_status');
            });

        if (Schema::hasColumn('accr_schools_information', 'school_setting'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_setting');
            });

        if (Schema::hasColumn('accr_schools_information', 'date_of_establishment'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('date_of_establishment');
            });

        if (Schema::hasColumn('accr_schools_information', 'school_ownership'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_ownership');
            });

        if (Schema::hasColumn('accr_schools_information', 'post_office_box_number'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('post_office_box_number');
            });

        if (Schema::hasColumn('accr_schools_information', 'website'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('website');
            });

        if (Schema::hasColumn('accr_schools_information', 'boarding_or_day'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('boarding_or_day');
            });

        if (Schema::hasColumn('accr_schools_information', 'accreditation_number'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('accreditation_number');
            });

        if (Schema::hasColumn('accr_schools_information', 'accreditation_date'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('accreditation_date');
            });

        if (Schema::hasColumn('accr_schools_information', 'school_activity'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_activity');
            });

        if (Schema::hasColumn('accr_schools_information', 'school_logo'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_logo');
            });

        if (Schema::hasColumn('accr_schools_information', 'operator'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('operator');
            });

        if (Schema::hasColumn('accr_schools_information', 'discipline_totalmarks'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('discipline_totalmarks');
            });

        if (Schema::hasColumn('accr_schools_information', 'sms'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('sms');
            });

        if (Schema::hasColumn('accr_schools_information', 'school_acronym'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_acronym');
            });

        if (Schema::hasColumn('accr_schools_information', 'sms_sender'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('sms_sender');
            });

        if (Schema::hasColumn('accr_schools_information', 'school_moto'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_moto');
            });

        if (Schema::hasColumn('accr_schools_information', 'school_code'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_code');
            });
    }
}
