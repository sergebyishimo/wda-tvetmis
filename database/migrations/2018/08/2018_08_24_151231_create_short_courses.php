<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('short_courses'))
            return;

        Schema::connection('accr_mysql')->create('short_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->string('course_name');
            $table->longText('course_details');
            $table->string('attachment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('short_courses');
    }
}
