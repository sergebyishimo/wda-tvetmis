<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolInfrastructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('school_infrastructures'))
            return;
        Schema::connection('accr_mysql')->create('school_infrastructures', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->uuid('infrastructure_class');
            $table->longText('infrastructure_description')->nullable();
            $table->string('capacity_total')->nullable();
            $table->string('capacity_male')->nullable();
            $table->string('capacity_female')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('school_infrastructures');
    }
}
