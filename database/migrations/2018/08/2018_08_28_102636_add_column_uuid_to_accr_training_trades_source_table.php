<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUuidToAccrTrainingTradesSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasColumn('training_trades_source', 'uuid'))
            return;

        Schema::connection('accr_mysql')->table('training_trades_source', function (Blueprint $table) {
            $table->string('uuid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('training_trades_source', 'uuid'))
            return;

        Schema::connection('accr_mysql')->table('training_trades_source', function (Blueprint $table) {
            $table->dropColumn('uuid');
        });
    }
}
