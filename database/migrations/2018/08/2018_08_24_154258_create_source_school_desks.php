<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSchoolDesks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_school_desks'))
            return;
        Schema::connection('accr_mysql')->create('source_school_desks', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->string('rtqf_level');
            $table->integer('classrooms');
            $table->integer('desks');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_school_desks');
    }
}
