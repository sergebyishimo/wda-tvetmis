<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffFemaleToAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'staff_female'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('staff_female')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'staff_female'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('staff_female');
            });
    }
}
