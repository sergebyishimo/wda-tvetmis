<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnDateOfEstablishmentInAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
            $table->string('date_of_establishment')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
            $table->timestamp('date_of_establishment')->change();
        });
    }
}
