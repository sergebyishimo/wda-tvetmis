<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsOtherFieldsOnAccrSchoolInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'school_type'))
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->integer('school_type');
            });
        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'school_rating'))
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->integer('school_rating');
            });
        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'school_levels'))
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->string('school_levels');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
            $table->dropColumn(['school_type', 'school_rating', 'school_levels']);
        });
    }
}
