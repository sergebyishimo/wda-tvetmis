<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSchoolActivityReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_school_activity_report'))
            return;
        Schema::connection('accr_mysql')->create('source_school_activity_report', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->longText('activity_description');
            $table->date('implemented_from');
            $table->date('implemented_to');
            $table->string('impact_of_activity');
            $table->string('activity_completed');
            $table->string('activity_progress_details');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_school_activity_report');
    }
}
