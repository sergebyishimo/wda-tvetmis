<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHasBusinessOrStrategicPlanToAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'has_business_or_strategic_plan'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('has_business_or_strategic_plan')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'has_business_or_strategic_plan'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('has_business_or_strategic_plan');
            });
    }
}
