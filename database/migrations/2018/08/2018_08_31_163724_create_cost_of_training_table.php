<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostOfTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cost_of_training', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->integer('trades_id')->nullable();
            $table->string('cost_of_consumables_per_year')->nullable();
            $table->string('estimated_total_salaries_year')->nullable();
            $table->string('budget_available_for_consumables')->nullable();
            $table->string('operational_funds_available_per_year')->nullable();
            $table->string('academic_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cost_of_training');
    }
}
