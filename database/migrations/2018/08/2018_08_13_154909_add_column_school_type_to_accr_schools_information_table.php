<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSchoolTypeToAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'school_type'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('school_type')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'school_type'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('school_type');
            });
    }
}
