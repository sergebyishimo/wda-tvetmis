<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSchoolAdmissionRequirements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_school_admission_requirements'))
            return;
        Schema::connection('accr_mysql')->create('source_school_admission_requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->longText('admission_requirements')->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_school_admission_requirements');
    }
}
