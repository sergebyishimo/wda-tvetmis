<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmitIdColumnOnSchoolSelfAssessimentStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('school_self_assessiment_statuses', 'submit_id'))
            Schema::connection('accr_mysql')->table('school_self_assessiment_statuses', function (Blueprint $table) {
                $table->string('submit_id')->unique()->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('accr_mysql')->hasColumn('school_self_assessiment_statuses', 'submit_id'))
            Schema::connection('accr_mysql')->table('school_self_assessiment_statuses', function (Blueprint $table) {
                $table->dropColumn('submit_id');
            });
    }
}
