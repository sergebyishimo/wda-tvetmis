<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_qualifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school')->nullable();
            $table->string('staff_id')->nullable();
            $table->string("qualification_name")->nullable();
            $table->string("institution_of_study")->nullable();
            $table->date("date_of_award")->nullable();
            $table->string("qualification_level")->nullable();
            $table->text("supporting_doc")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_qualifications');
    }
}
