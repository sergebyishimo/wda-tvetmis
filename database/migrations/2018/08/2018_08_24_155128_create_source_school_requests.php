<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSchoolRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_school_requests'))
            return;
        Schema::connection('accr_mysql')->create('source_school_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('request_class');
            $table->string('required_units');
            $table->string('unit_cost');
            $table->longText('request_description');
            $table->date('required_by_date');
            $table->uuid('school_id');
            $table->string('request_status');
            $table->longText('wda_comments');
            $table->string('request_attachment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_school_requests');
    }
}
