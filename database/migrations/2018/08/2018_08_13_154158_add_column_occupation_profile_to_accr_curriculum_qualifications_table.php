<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOccupationProfileToAccrCurriculumQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_curriculum_qualifications', 'occupation_profile'))
            Schema::table('accr_curriculum_qualifications', function (Blueprint $table) {
                $table->text('occupation_profile')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_curriculum_qualifications', 'occupation_profile'))
            Schema::table('accr_curriculum_qualifications', function (Blueprint $table) {
                $table->dropColumn('occupation_profile');
            });
    }
}
