<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceProductionUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_production_units'))
            return;
        Schema::connection('accr_mysql')->create('source_production_units', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->string('production_unit_name');
            $table->string('operation_field');
            $table->string('production_unit_capital_francs');
            $table->string('annual_turnover_francs');
            $table->string('monthly_revenues_francs');
            $table->integer('employees_male');
            $table->integer('employees_female');
            $table->string('funds_source');
            $table->string('rdb_registration_number')->nullable();
            $table->longText('community_impact');
            $table->string('attachments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_production_units');
    }
}
