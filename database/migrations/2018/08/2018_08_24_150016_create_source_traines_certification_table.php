<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceTrainesCertificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_traines_certification'))
            return;

        Schema::connection('accr_mysql')->create('source_traines_certification', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        $tbl = DB::connection('accr_mysql')->table('source_traines_certification');
        $tbl->truncate();
        $tbl->insert([
            'name'  =>  'TVET Trainer',
            'name'  =>  'TVET Master Trainer'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('accr_mysql')->hasTable('source_traines_certification'))
            return;
        Schema::connection('accr_mysql')->dropIfExists('source_traines_certification');
    }
}
