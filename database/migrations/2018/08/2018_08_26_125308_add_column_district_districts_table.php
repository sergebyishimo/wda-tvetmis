<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDistrictDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('districts', 'district'))
            return;

        Schema::table('districts', function (Blueprint $table) {
            $table->string('district')->before('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('districts', 'district'))
            return;

        Schema::table('districts', function (Blueprint $table) {
            $table->dropColumn('district');
        });
    }
}
