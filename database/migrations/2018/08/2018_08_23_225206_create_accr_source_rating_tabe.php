<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrSourceRatingTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_rating'))
            return;
        Schema::connection('accr_mysql')->create('source_rating', function (Blueprint $table){
            $table->increments('id');
            $table->string('rating');
        });

        if (DB::connection('accr_mysql')->table('source_rating')->select('*')->count() <= 0)
            DB::connection('accr_mysql')->table('source_rating')->insert([
                ['rating' => 'Good'],
                ['rating' => 'Bad'],
                ['rating' => 'Fair'],
                ['rating' => 'Very Good'],
                ['rating' => 'Excellent']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
