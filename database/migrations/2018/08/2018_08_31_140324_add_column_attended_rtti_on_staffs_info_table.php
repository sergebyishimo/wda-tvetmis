<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAttendedRttiOnStaffsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('staffs_info', 'attended_rtti'))
            return;
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->string('attended_rtti')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('staffs_info', 'attended_rtti'))
            return;
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->dropColumn('attended_rtti');
        });
    }
}
