<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRttiDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_rtti_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('staff_id');
            $table->string('spouse_names')->nullable();
            $table->string("number_of_children")->nullable();
            $table->string("age_range_children")->nullable();
            $table->string("children_care_taker")->nullable();
            $table->string("whom_you_stay_with")->nullable();
            $table->string("fathers_names")->nullable();
            $table->string("mothers_names")->nullable();
            $table->string("family_province")->nullable();
            $table->string("family_district")->nullable();
            $table->string("family_sector")->nullable();
            $table->string("number_brothers")->nullable();
            $table->string("number_sisters")->nullable();
            $table->string("about_family")->nullable();
            $table->string("your_family_responsibilities")->nullable();
            $table->string("language_spoken")->nullable();
            $table->string("language_prefered")->nullable();
            $table->string("describe_yourself")->nullable();
            $table->string("sch_name")->nullable();
            $table->string("sch_province")->nullable();
            $table->string("sch_district")->nullable();
            $table->string("sch_sector")->nullable();
            $table->string("sch_level")->nullable();
            $table->string("sch_status")->nullable();
            $table->string("sch_manager_names")->nullable();
            $table->string("sch_manager_phone")->nullable();
            $table->string("sch_phone")->nullable();
            $table->string("sch_email")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_rtti_details');
    }
}
