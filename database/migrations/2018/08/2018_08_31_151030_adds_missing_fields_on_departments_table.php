<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsMissingFieldsOnDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('departments', 'capacity_students'))
            return;
        if (Schema::hasColumn('departments', 'capacity_rooms'))
            return;
        Schema::table('departments', function (Blueprint $table) {
            $table->string('capacity_students')->nullable();
            $table->string('capacity_rooms')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('departments', 'capacity_rooms'))
            return;
        if (!Schema::hasColumn('departments', 'capacity'))
            return;
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn(['capacity_students','capacity_rooms']);
        });
    }
}
