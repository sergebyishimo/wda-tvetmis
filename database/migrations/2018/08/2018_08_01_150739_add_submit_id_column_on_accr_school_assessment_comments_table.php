<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmitIdColumnOnAccrSchoolAssessmentCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('accr_school_assessment_comments', 'submit_id'))
            Schema::connection('accr_mysql')->table('accr_school_assessment_comments', function (Blueprint $table){
                $table->string('submit_id')->nullable()->unique();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('accr_mysql')->hasColumn('accr_school_assessment_comments', 'submit_id'))
            Schema::connection('accr_mysql')->table('accr_school_assessment_comments', function (Blueprint $table){
                $table->dropColumn('submit_id');
            });
    }
}
