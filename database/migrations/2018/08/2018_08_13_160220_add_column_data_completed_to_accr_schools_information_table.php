<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDataCompletedToAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'data_completed'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->integer('data_completed')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'data_completed'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('data_completed');
            });
    }
}
