<?php

use App\StaffRttiTrainingReferenceSource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRttiTrainingReferenceSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_rtti_training_reference_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->text("training_reference");
            $table->timestamps();
        });
        if (StaffRttiTrainingReferenceSource::count() <= 0) {
            DB::table('staff_rtti_training_reference_sources')->insert([
                ['training_reference' => 'Why do you want to enroll on this training course?'],
                ['training_reference' => 'What do you expect to learn from this course / training?'],
                ['training_reference' => 'How will that help you in the near future?'],
                ['training_reference' => 'What are your long term career plans / plans for the next 5 years?'],
                ['training_reference' => 'What do you think will be your main problems /difficulties when being a participant / trainee / learner on this course / training?'],
                ['training_reference' => 'Do you have any worries / anxieties when thinking about your being a participant / trainee / learner on this course / training?'],
                ['training_reference' => 'Explain how you will be able to combine your study with other responsibilities you have? (e.g. work, looking after children, parents, etc)'],
                ['training_reference' => 'How much time a week can you spend on studying / reviewing the resources / doing assignments set / preparing for assessment after training/school hours?'],
                ['training_reference' => 'Where will you do most of your study?\r\nDo you have a space in the place you are staying where you can study undisturbed?'],
                ['training_reference' => 'What special skills or hobbies do you have? (e.g. sport / singing / good in communicating with other persons/ etc.'],
                ['training_reference' => 'Do you have access to: Video; DVD; Radio; Audio Cassette'],
                ['training_reference' => 'What are your main areas of interest outside work?'],
                ['training_reference' => 'What are you expecting from the facilitator/ trainer?'],
                ['training_reference' => 'What is the most important thing your facilitator / trainer should know about you? E.g. personal problems, issues that might affect your study / participation in the course / training.'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_rtti_training_reference_sources');
    }
}
