<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceIncubationCenters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_incubation_centers'))
            return;
        Schema::connection('accr_mysql')->create('source_incubation_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->string('club_name');
            $table->string('club_facilitator');
            $table->integer('incubatees_male');
            $table->integer('incubatees_female');
            $table->integer('average_age');
            $table->string('business_areas');
            $table->string('annual_turn_over_francs');
            $table->string('funds_source');
            $table->string('rdb_registration_number')->nullable();
            $table->integer('number_incubated_clubs');
            $table->string('attachments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_incubation_centers');
    }
}
