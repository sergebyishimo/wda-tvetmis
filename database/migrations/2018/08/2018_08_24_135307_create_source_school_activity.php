<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceSchoolActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_school_activity'))
            return;
        Schema::connection('accr_mysql')->create('source_school_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->boolean('active');
            $table->boolean('inactive');
            $table->date('active_date');
            $table->longText('comment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_school_activity');
    }
}
