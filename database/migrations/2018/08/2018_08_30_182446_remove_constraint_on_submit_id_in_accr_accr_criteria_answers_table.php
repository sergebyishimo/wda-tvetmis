<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveConstraintOnSubmitIdInAccrAccrCriteriaAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
            $table->dropUnique('accr_criteria_answers_submit_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
            $table->string('submit_id')->unique()->change();
        });
    }
}
