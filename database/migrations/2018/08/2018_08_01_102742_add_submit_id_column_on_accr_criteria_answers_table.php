<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmitIdColumnOnAccrCriteriaAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('accr_criteria_answers', 'submit_id'))
            Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
                $table->string('submit_id')->unique()->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('accr_mysql')->hasColumn('accr_criteria_answers', 'submit_id'))
            Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
                $table->dropColumn('submit_id');
            });
    }
}
