<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumnInStaffInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasColumn('staffs_info', 'institution')) {
            Schema::table('staffs_info', function (Blueprint $table) {
                $table->string('institution')->nullable()->after('qualification');
            });
        }
        if (!Schema::hasColumn('staffs_info', 'graduated_year')) {
            Schema::table('staffs_info', function (Blueprint $table) {
                $table->integer('graduated_year')->nullable()->after('institution');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        if (Schema::hasColumn('staffs_info', 'institution')) {
            Schema::table('staffs_info', function (Blueprint $table) {
                $table->dropColumn('institution');
            });
        }
        if (Schema::hasColumn('staffs_info', 'graduated_year')) {
            Schema::table('staffs_info', function (Blueprint $table) {
                $table->dropColumn('graduated_year');
            });
        }
    }
}
