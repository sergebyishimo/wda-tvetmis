<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffMarkerApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_marker_applications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->integer('academic_year')->nullable();
			$table->integer('education_program_id')->nullable();
			$table->integer('trade_marked_before')->nullable();
			$table->integer('school_recommendation')->nullable();
			$table->integer('wda_selection')->nullable();
			$table->integer('wda_confirmation')->nullable();
			$table->integer('scores')->nullable();
			$table->timestamp('application_date')->nullable()->default(null);
			$table->string('first_subject_applied_for', 50);
			$table->integer('first_subject_marked_before');
			$table->string('second_subject_applied_for', 50)->nullable();
			$table->integer('second_subject_marked_before')->nullable();
			$table->string('third_subject_applied_for', 50)->nullable();
			$table->integer('third_subject_marked_before')->nullable();

//			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_marker_applications');
	}

}
