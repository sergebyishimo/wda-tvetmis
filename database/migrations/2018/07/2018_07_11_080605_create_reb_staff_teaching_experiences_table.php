<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRebStaffTeachingExperiencesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('reb_mysql')->hasTable('staff_teaching_experiences')) {
            return;
        }
        Schema::connection('reb_mysql')->create('staff_teaching_experiences', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('staff_id')->nullable();
            $table->integer('tvet_field')->nullable();
            $table->integer('tvet_sub_field')->nullable();
            $table->string('institution_taught')->nullable();
            $table->integer('qualification_name')->nullable();
            $table->integer('teaching_competences')->nullable();
            $table->integer('class_or_level_taught')->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->binary('attachments', 65535)->nullable();
            $table->string('attachment_name', 2000)->nullable();
            $table->string('attachment_size', 200)->nullable();
            $table->string('subject_taught', 100)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('reb_mysql')->drop('staff_teaching_experiences');
    }

}
