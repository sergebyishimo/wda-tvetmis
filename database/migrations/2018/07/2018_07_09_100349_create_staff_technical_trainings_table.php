<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffTechnicalTrainingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_technical_trainings', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->integer('field')->nullable();
			$table->integer('sub_field')->nullable();
			$table->string('providing_institution', 120)->nullable();
			$table->integer('certified')->nullable();
			$table->date('date')->nullable();
			$table->binary('supporting_doc', 65535)->nullable();
			$table->string('attachment_name', 2000)->nullable();
			$table->string('attachment_size', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_technical_trainings');
	}

}
