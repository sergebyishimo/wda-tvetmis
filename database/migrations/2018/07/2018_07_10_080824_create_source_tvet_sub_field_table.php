<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSourceTvetSubFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('source_tvet_sub_field')) { return; }

		Schema::create('source_tvet_sub_field', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sub_field_name')->nullable();
			$table->string('sub_field_code', 100)->nullable();
			$table->integer('tvet_field')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('source_tvet_sub_field');
	}

}
