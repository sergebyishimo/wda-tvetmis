<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceGraduateEmpStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_graduate_emp_statuses'))
            return;

        Schema::connection('accr_mysql')->create('source_graduate_emp_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_status',70);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_graduate_emp_statuses');
    }
}
