<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRebStaffWorkexperienceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('reb_mysql')->hasTable('staff_workexperience')) {
            return;
        }
        Schema::connection('reb_mysql')->create('staff_workexperience', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('staff_id')->nullable();
            $table->string('title')->nullable();
            $table->string('institution')->nullable();
            $table->integer('status')->nullable();
            $table->date('period_from')->nullable();
            $table->date('period_to')->nullable();
            $table->text('responsibility', 65535)->nullable();
            $table->integer('field_of_expertise_sector')->nullable();
            $table->integer('field_of_expertise_trade')->nullable();
            $table->string('supporting_doc')->nullable();
            $table->string('attachment_name', 2000)->nullable();
            $table->string('attachment_size', 200)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('reb_mysql')->drop('staff_workexperience');
    }

}
