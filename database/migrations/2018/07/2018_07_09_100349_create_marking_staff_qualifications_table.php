<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMarkingStaffQualificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('marking_staff_qualifications', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school_id', 36);
			$table->char('staff_id', 36);
			$table->string('name', 200);
			$table->string('institution_of_study', 200);
			$table->date('date_of_award')->nullable();
			$table->integer('qualification_level');
			$table->binary('supporting_doc', 65535)->nullable();
			$table->string('attachment_name', 2000)->nullable();
			$table->string('attachment_size', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('marking_staff_qualifications');
	}

}
