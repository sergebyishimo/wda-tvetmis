<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumnInRebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasColumn('rebs', 'institution')) {
            Schema::table('rebs', function (Blueprint $table) {
                $table->string('institution')->nullable()->after('qualification');
            });
        }
        if (!Schema::hasColumn('rebs', 'graduated_year')) {
            Schema::table('rebs', function (Blueprint $table) {
                $table->integer('graduated_year')->nullable()->after('institution');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        if (Schema::hasColumn('rebs', 'institution')) {
            Schema::table('rebs', function (Blueprint $table) {
                $table->dropColumn('institution');
            });
        }
        if (Schema::hasColumn('rebs', 'graduated_year')) {
            Schema::table('rebs', function (Blueprint $table) {
                $table->dropColumn('graduated_year');
            });
        }
    }
}
