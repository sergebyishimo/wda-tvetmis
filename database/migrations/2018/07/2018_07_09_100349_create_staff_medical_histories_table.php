<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffMedicalHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_medical_histories', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->string('allergies', 2000)->nullable();
			$table->string('disabilities_or_special_needs', 200)->nullable();
			$table->date('last_date_hospitalized')->nullable();
			$table->date('last_time_sick')->nullable();
			$table->text('cause_of_sickness_latest_time', 65535);
			$table->text('medical_history_background', 65535)->nullable();
			$table->text('detailed_medical_information_or_dr_Instructions', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_medical_histories');
	}

}
