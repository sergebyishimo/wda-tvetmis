<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSourceStaffMarkingAssessmentPositionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('source_staff_marking_assessment_positions')) {
            return;
        }
        Schema::create('source_staff_marking_assessment_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position', 25)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_staff_marking_assessment_positions');
    }

}
