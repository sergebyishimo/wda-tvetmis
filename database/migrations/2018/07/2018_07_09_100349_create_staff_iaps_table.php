<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffIapsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_iaps', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->string('company_name', 200)->nullable();
			$table->date('from_date')->nullable();
			$table->date('to_date')->nullable();
			$table->string('place', 200)->nullable();
			$table->integer('days')->nullable();
			$table->binary('supporting_doc', 65535)->nullable();
			$table->string('attachment_name', 2000)->nullable();
			$table->string('attachment_size', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_iaps');
	}

}
