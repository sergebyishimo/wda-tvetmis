<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNecessaryColumnsIntoRebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rebs', function (Blueprint $table) {
            $table->string('school')->nullable();
            $table->string('school_province')->nullable();
            $table->string('school_district')->nullable();
            $table->string('school_master')->nullable();
            $table->string('school_email')->nullable();
            $table->string('school_phone')->nullable();
            $table->string('gender')->nullable(); #array('Male', 'Female')
            $table->string("civil_status")->nullable(); #["Single", "Married", "Widowed", "Separated", "Divorced"]
            $table->string("national_id_number")->nullable();
            $table->string("staff_category")->nullable();
            $table->string('qualification', 30)->nullable();
            $table->string('carrier', 70)->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->string('nationality', 30)->nullable();
            $table->string('province', 60)->nullable();
            $table->string('district', 40)->nullable();
            $table->string('sector', 40)->nullable();
            $table->string("photo")->nullable();
            $table->string("bank_name")->nullable();
            $table->string("bank_account_number")->nullable();
            $table->string("rssb_number")->nullable();
            $table->string("medical_insurance_scheme")->nullable();
            $table->string("medical_card_number")->nullable();
            $table->string('status')->nullable(); #array('active', "", 'deleted')
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rebs', function (Blueprint $table) {
            $table->dropColumn('school');
            $table->dropColumn('school_province');
            $table->dropColumn('school_district');
            $table->dropColumn('school_master');
            $table->dropColumn('school_email');
            $table->dropColumn('school_phone');
            $table->dropColumn('gender');
            $table->dropColumn("civil_status");
            $table->dropColumn("national_id_number");
            $table->dropColumn("staff_category");
            $table->dropColumn('qualification');
            $table->dropColumn('carrier');
            $table->dropColumn('phone_number');
            $table->dropColumn('nationality');
            $table->dropColumn('province');
            $table->dropColumn('district');
            $table->dropColumn('sector');
            $table->dropColumn("photo");
            $table->dropColumn("bank_name");
            $table->dropColumn("bank_account_number");
            $table->dropColumn("rssb_number");
            $table->dropColumn("medical_insurance_scheme");
            $table->dropColumn("medical_card_number");
            $table->dropColumn('status');
        });
    }
}
