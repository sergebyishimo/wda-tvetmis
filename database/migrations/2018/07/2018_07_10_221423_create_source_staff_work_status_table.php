<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSourceStaffWorkStatusTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('source_staff_work_status')) {
            return;
        }
        Schema::create('source_staff_work_status', function (Blueprint $table) {
            $table->integer('id');
            $table->string('work_status')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_staff_work_status');
    }

}
