<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffAssessorQualificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_assessor_qualifications', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school', 36)->nullable();
			$table->char('staff', 36)->nullable();
			$table->string('programme_title', 120)->nullable();
			$table->string('provider', 120)->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->integer('status')->nullable();
			$table->integer('field_of_expertise_field')->nullable();
			$table->integer('field_of_expertise_sub_field')->nullable();
			$table->timestamp('date_created')->nullable()->default(null);
			$table->binary('supporting_doc', 65535)->nullable();
			$table->string('attachment_name', 200)->nullable();
			$table->string('attachment_size', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_assessor_qualifications');
	}

}
