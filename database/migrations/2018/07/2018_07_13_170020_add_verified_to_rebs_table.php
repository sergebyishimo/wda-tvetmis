<?php
/**
 * (c) Lunaweb Ltd. - Josias Montag
 * Date: 27.02.17
 * Time: 17:00
 */


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddVerifiedToRebsTable extends Migration
{
    /**
     * Determine the user table name.
     *
     * @return string
     */
    public function getUserTableName()
    {
        $user_model = config('auth.providers.users.model', App\Reb::class);

        return "rebs";
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn($this->getUserTableName(), 'verified')) {
            Schema::table($this->getUserTableName(), function (Blueprint $table) {
                $table->boolean('verified')->default(false);
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn($this->getUserTableName(), 'verified')) {
            Schema::table($this->getUserTableName(), function (Blueprint $table) {
                $table->dropColumn('verified');
            });
        }
    }
}
