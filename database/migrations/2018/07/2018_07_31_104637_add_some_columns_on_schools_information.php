<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumnsOnSchoolsInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'capacity_male_students')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->integer('capacity_male_students')->nullable()->before('updated_at');
            });
        }

        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'capacity_female_students')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->integer('capacity_female_students')->nullable()->before('updated_at');
            });
        }

        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'required_trainers')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->integer('required_trainers')->nullable()->before('updated_at');
            });
        }

        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'number_of_classrooms')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->integer('number_of_classrooms')->nullable()->before('updated_at');
            });
        }

        if (!Schema::connection('accr_mysql')->hasColumn('schools_information', 'capacity_students_total')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->integer('capacity_students_total')->nullable()->before('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('accr_mysql')->hasColumn('schools_information', 'capacity_male_students')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->dropColumn('capacity_male_students');
            });
        }

        if (Schema::connection('accr_mysql')->hasColumn('schools_information', 'capacity_female_students')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->dropColumn('capacity_female_students');
            });
        }

        if (Schema::connection('accr_mysql')->hasColumn('schools_information', 'required_trainers')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->dropColumn('required_trainers');
            });
        }

        if (Schema::connection('accr_mysql')->hasColumn('schools_information', 'number_of_classrooms')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_classrooms');
            });
        }

        if (Schema::connection('accr_mysql')->hasColumn('schools_information', 'capacity_students_total')) {
            Schema::connection('accr_mysql')->table('schools_information', function (Blueprint $table) {
                $table->dropColumn('capacity_students_total');
            });
        }
    }
}
