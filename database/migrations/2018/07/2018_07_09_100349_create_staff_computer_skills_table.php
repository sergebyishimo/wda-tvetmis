<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffComputerSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_computer_skills', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->integer('internet')->nullable();
			$table->integer('word')->nullable();
			$table->integer('excel')->nullable();
			$table->integer('powerpoint')->nullable();
			$table->binary('supporting_doc', 65535)->nullable();
			$table->string('attachment_name', 2000)->nullable();
			$table->string('attachment_size', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_computer_skills');
	}

}
