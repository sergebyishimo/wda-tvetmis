<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRebStaffMarkingBackgroundsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('reb_mysql')->hasTable('staff_marking_backgrounds')) {
            return;
        }
        Schema::connection('reb_mysql')->create('staff_marking_backgrounds', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('staff_id')->nullable();
            $table->integer('academic_year')->nullable();
            $table->integer('marking_position')->nullable();
            $table->string('marking_institution')->nullable();
            $table->integer('education_program')->nullable();
            $table->string('marking_center')->nullable();
            $table->integer('subject_exam_marked')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('reb_mysql')->drop('staff_marking_backgrounds');
    }

}
