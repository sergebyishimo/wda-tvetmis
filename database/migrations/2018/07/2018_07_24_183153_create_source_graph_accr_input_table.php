<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourceGraphAccrInputTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::connection('accr_mysql')->hasTable('source_graph_accr_input'))
            return;

		Schema::connection('accr_mysql')->create('source_graph_accr_input', function(Blueprint $table)
		{
			$table->integer('id')->nullable();
			$table->string('school_name', 150)->nullable();
			$table->integer('academic_year')->nullable();
			$table->string('Province', 50)->nullable();
			$table->string('district', 50)->nullable();
			$table->string('school_status', 50)->nullable();
			$table->integer('section_one_admin_infrast')->nullable();
			$table->integer('section_one_training_infrast')->nullable();
			$table->integer('section_one_sch_environment')->nullable();
			$table->integer('section_one_didactive_materials_consumables')->nullable();
			$table->float('section_one_avg', 10, 0)->nullable();
			$table->string('section_one_rank', 50)->nullable();
			$table->integer('section_two_marking_gender')->nullable();
			$table->integer('section_two_cba_cbt')->nullable();
			$table->integer('section_two_leadership_capacity')->nullable();
			$table->integer('section_two_admin_capacity')->nullable();
			$table->integer('section_two_documentation')->nullable();
			$table->integer('section_two_partnership_prod_unit')->nullable();
			$table->float('section_two_avg', 10, 0)->nullable();
			$table->string('section_two_rank', 50)->nullable();
			$table->integer('section_three_numb_qual_trainers')->nullable();
			$table->integer('section_three_pedagogical_capacity')->nullable();
			$table->float('section_three_avg', 10, 0)->nullable();
			$table->string('section_three_rank', 50)->nullable();
			$table->integer('boarding_facilities')->nullable();
			$table->integer('school_total_students')->nullable();
			$table->integer('school_total_staff')->nullable();
			$table->integer('school_total_trainers')->nullable();
			$table->string('school_annual_budget', 50)->nullable();
			$table->float('over_all_avg', 10, 0)->nullable();
			$table->string('over_all_rank', 50)->nullable();
			$table->string('final_pass_fail', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('accr_mysql')->dropIfExists('source_graph_accr_input');
	}

}
