<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffProfileAttachmentsFinalsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('staff_profile_attachments_finals')) {
            return;
        }
        Schema::create('staff_profile_attachments_finals', function (Blueprint $table) {
            $table->integer('id', true);
            $table->char('school_id', 36);
            $table->char('staff_id', 36);
            $table->integer('attachment_description');
            $table->string('attachment');
            $table->string('attachment_name', 100)->nullable();
            $table->string('attachment_size', 100)->nullable();
            $table->string('comments', 100)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_profile_attachments_finals');
    }

}
