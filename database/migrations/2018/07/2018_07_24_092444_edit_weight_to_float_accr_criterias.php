<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditWeightToFloatAccrCriterias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('accr_criterias', 'weight'))
            return;

        Schema::connection('accr_mysql')->table('accr_criterias', function (Blueprint $table){
            $table->float('weight')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('accr_criterias', 'weight'))
            return;

        Schema::connection('accr_mysql')->table('accr_criterias', function (Blueprint $table){
            $table->integer('weight')->default(0)->change();
        });
    }
}
