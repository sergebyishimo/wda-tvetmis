<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInAccrCriteriaAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
            $table->boolean('district')->nullable();
            $table->boolean('wda')->nullable();
            $table->string('academic_year')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
            $table->dropColumn('district');
            $table->dropColumn('wda');
            $table->dropColumn('academic_year');
        });
    }
}