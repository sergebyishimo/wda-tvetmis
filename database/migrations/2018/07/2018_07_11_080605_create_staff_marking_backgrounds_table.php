<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffMarkingBackgroundsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('staff_marking_backgrounds')) {
            return;
        }
        Schema::create('staff_marking_backgrounds', function (Blueprint $table) {
            $table->integer('id', true);
            $table->char('school_id', 36)->nullable();
            $table->char('staff_id', 36)->nullable();
            $table->integer('academic_year')->nullable();
            $table->integer('marking_position')->nullable();
            $table->string('marking_institution')->nullable();
            $table->integer('education_program')->nullable();
            $table->string('marking_center')->nullable();
            $table->integer('subject_exam_marked')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_marking_backgrounds');
    }

}
