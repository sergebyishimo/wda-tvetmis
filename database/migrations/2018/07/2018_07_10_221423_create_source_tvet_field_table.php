<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSourceTvetFieldTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('source_tvet_field')) {
            return;
        }
        Schema::create('source_tvet_field', function (Blueprint $table) {
            $table->integer('id');
            $table->string('tvet_field', 200)->nullable();
            $table->string('field_code', 100)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_tvet_field');
    }

}
