<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourceGraphAccrPostGraduationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('source_graph_accr_post_graduation'))
            return;

        Schema::connection('accr_mysql')->create('source_graph_accr_post_graduation', function (Blueprint $table) {
            $table->integer('id')->nullable();
            $table->string('school_name', 150)->nullable();
            $table->integer('academic_year')->nullable();
            $table->string('Province', 50)->nullable();
            $table->string('district', 50)->nullable();
            $table->string('school_status', 50)->nullable();
            $table->integer('emp_before_six_month_male')->nullable();
            $table->integer('emp_before_six_month_female')->nullable();
            $table->integer('emp_before_six_month_total')->nullable();
            $table->integer('emp_after_six_month_male')->nullable();
            $table->integer('emp_after_six_month_female')->nullable();
            $table->integer('emp_after_six_month_total')->nullable();
            $table->integer('employer_satisfied_male')->nullable();
            $table->integer('employer_satisfied_female')->nullable();
            $table->integer('employer_satisfied_total')->nullable();
            $table->integer('employer_not_satisfied_male')->nullable();
            $table->integer('employer_not_satisfied_female')->nullable();
            $table->integer('employer_not_satisfied_total')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('source_graph_accr_post_graduation');
    }

}
