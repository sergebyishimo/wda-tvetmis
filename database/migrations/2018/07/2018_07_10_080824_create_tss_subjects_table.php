<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTssSubjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('tss_subjects')) { return; }

		Schema::create('tss_subjects', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('option_trade', 50)->nullable();
			$table->string('option_code', 50)->nullable();
			$table->string('subject', 200)->nullable();
			$table->string('subject_code', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tss_subjects');
	}

}
