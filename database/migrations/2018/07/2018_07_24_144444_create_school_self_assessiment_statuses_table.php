<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolSelfAssessimentStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('school_self_assessiment_statuses'))
            return;

        Schema::connection('accr_mysql')->create('school_self_assessiment_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->unsignedInteger('function_id');
            $table->boolean('school_confirm')->default(1);
            $table->boolean('district_confirm')->default(0);
            $table->boolean('wda_confirm')->default(0);
            $table->string('academic_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('school_self_assessiment_statuses');
    }
}
