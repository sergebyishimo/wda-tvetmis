<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQualityAreaIdToAccrCriteriaSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasColumn('accr_criteria_sections', 'quality_area_id'))
            return;
        Schema::connection('accr_mysql')->table('accr_criteria_sections', function (Blueprint $table){
            $table->unsignedInteger('quality_area_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('accr_criteria_sections', 'quality_area_id'))
            return;

        Schema::connection('accr_mysql')->table('accr_criteria_sections', function (Blueprint $table){
            $table->dropColumn('quality_area_id');
        });
    }
}
