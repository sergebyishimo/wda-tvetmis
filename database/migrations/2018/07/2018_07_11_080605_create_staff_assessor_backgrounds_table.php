<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffAssessorBackgroundsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('staff_assessor_backgrounds')) {
            return;
        }
        Schema::create('staff_assessor_backgrounds', function (Blueprint $table) {
            $table->integer('id', true);
            $table->char('school_id', 36);
            $table->char('staff_id', 36);
            $table->integer('academic_year');
            $table->integer('education_program_assessed');
            $table->string('assessement_center')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_assessor_backgrounds');
    }

}
