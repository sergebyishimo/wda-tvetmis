<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrSchoolAssessmentCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accr_mysql')->create('accr_school_assessment_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('school_id');
            $table->unsignedInteger('function_id');
            $table->unsignedInteger('quality_id');
            $table->text('strength')->nullable();
            $table->text('weakness')->nullable();
            $table->text('recommendation')->nullable();
            $table->text('timeline')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accr_school_assessment_comments');
    }
}
