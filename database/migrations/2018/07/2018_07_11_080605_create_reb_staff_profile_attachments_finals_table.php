<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRebStaffProfileAttachmentsFinalsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('reb_mysql')->hasTable('staff_profile_attachments_finals')) {
            return;
        }
        Schema::connection('reb_mysql')->create('staff_profile_attachments_finals', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('staff_id')->nullable();
            $table->integer('attachment_description');
            $table->string('attachment');
            $table->string('attachment_name', 100)->nullable();
            $table->string('attachment_size', 100)->nullable();
            $table->string('comments', 100)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('reb_mysql')->drop('staff_profile_attachments_finals');
    }

}
