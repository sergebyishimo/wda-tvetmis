<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_languages', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->integer('english_r')->nullable();
			$table->integer('english_s')->nullable();
			$table->integer('english_w')->nullable();
			$table->integer('french_r')->nullable();
			$table->integer('french_s')->nullable();
			$table->integer('french_w')->nullable();
			$table->integer('kinyarwanda_r')->nullable();
			$table->integer('kinyarwanda_s')->nullable();
			$table->integer('kinyarwanda_w')->nullable();
			$table->integer('swahili_r')->nullable();
			$table->integer('swahili_s')->nullable();
			$table->integer('swahili_w')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_languages');
	}

}
