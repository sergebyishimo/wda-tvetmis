<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasTable('accreditation_types'))
            return;

        Schema::connection('accr_mysql')->create('accreditation_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->timestamps();
        });

        \DB::connection('accr_mysql')->table('accreditation_types')->insert([
            ['type' => 'Initial Accreditation for New TVET Institution'],
            ['type' => 'Accreditation for New Course – Program and/or REQF Level'],
            ['type' => 'Accreditation for Regional and International Course - Program'],
            ['type' => 'Re-Accreditation']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('accreditation_types');
    }
}
