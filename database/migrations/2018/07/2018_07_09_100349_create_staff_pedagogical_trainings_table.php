<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffPedagogicalTrainingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_pedagogical_trainings', function(Blueprint $table)
		{
			$table->integer('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->integer('pedagogy_training_type')->nullable();
			$table->text('description', 16777215)->nullable();
			$table->string('provider', 120)->nullable();
			$table->date('from_date')->nullable();
			$table->date('to_date')->nullable();
			$table->binary('supporting_doc', 65535)->nullable();
			$table->string('attachment_name', 2000)->nullable();
			$table->string('attachment_size', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_pedagogical_trainings');
	}

}
