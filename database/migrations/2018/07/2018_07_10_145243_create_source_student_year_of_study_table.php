<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSourceStudentYearOfStudyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('source_student_year_of_study')) {
            return;
        }
		Schema::create('source_student_year_of_study', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('year_of_study')->nullable();
			$table->integer('school_type')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('source_student_year_of_study');
	}

}
