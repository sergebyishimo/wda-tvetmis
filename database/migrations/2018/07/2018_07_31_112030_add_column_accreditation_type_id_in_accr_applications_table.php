<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAccreditationTypeIdInAccrApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('accr_mysql')->hasColumn('accr_applications', 'accreditation_type_id'))
            Schema::connection('accr_mysql')->table('accr_applications', function (Blueprint $table) {
                $table->unsignedInteger('accreditation_type_id')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('accr_mysql')->hasColumn('accr_applications', 'accreditation_type_id'))
            Schema::connection('accr_mysql')->table('accr_applications', function (Blueprint $table) {
                $table->dropColumn('accreditation_type_id');
            });
    }
}
