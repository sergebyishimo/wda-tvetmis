<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatatypeForDistrictWdaColumnsInAccrCriteriaAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('accr_mysql')->hasColumn('accr_criteria_answers', 'district')) {
            Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
                $table->string('district')->nullable()->change();
            });
        }

        if (Schema::connection('accr_mysql')->hasColumn('accr_criteria_answers', 'wda')) {
            Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
                $table->string('wda')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('accr_mysql')->hasColumn('accr_criteria_answers', 'district')) {
            Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
                $table->boolean('district')->nullable()->change();
            });
        }

        if (Schema::connection('accr_mysql')->hasColumn('accr_criteria_answers', 'wda')) {
            Schema::connection('accr_mysql')->table('accr_criteria_answers', function (Blueprint $table) {
                $table->boolean('wda')->nullable()->change();
            });
        }
    }
}
