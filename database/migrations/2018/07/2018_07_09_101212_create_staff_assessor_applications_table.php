<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffAssessorApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_assessor_applications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('school_id', 36)->nullable();
			$table->char('staff_id', 36)->nullable();
			$table->string('trade', 250)->nullable();
			$table->integer('trade_assessed_before')->nullable();
			$table->text('subject')->nullable();
			$table->integer('subject_assessed_before')->nullable();
			$table->integer('academic_year')->nullable();
			$table->timestamp('application_date')->nullable()->default(null);
			$table->string('first_subject_applied_for', 100)->nullable();
			$table->integer('first_subject_assssed_before')->nullable();
			$table->string('second_subject_applied_for', 100)->nullable();
			$table->integer('second_subject_assssed_before')->nullable();
			$table->string('third_subject_applied_for', 100)->nullable();
			$table->integer('third_subject_assssed_before')->nullable();
			$table->integer('wda_confirmation')->nullable();
			$table->integer('wda_selection')->nullable();
			$table->float('score', 10, 0)->nullable();

//			$table->primary('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('staff_assessor_applications');
	}

}
