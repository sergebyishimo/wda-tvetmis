<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStaffsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->integer('sector_taught')->change();
            $table->integer('sub_sector_taught')->change();
            $table->integer('qualification_taught')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->string('sector_taught')->change();
            $table->string('sub_sector_taught')->change();
            $table->string('qualification_taught')->change();
        });
    }
}
