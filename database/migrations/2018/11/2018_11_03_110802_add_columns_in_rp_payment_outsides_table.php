<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInRpPaymentOutsidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('payment_outsides', function (Blueprint $table) {
            $table->string('payment_type')->nullable();
            $table->string('bank')->default('cogebank');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('payment_outsides', function (Blueprint $table) {
            $table->dropColumn('payment_type');
            $table->dropColumn('bank');
        });
    }
}
