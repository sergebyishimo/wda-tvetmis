<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsOnAccrSchoolsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accr_schools_information', 'number_of_computer_labs'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('number_of_computer_labs')->nullable();
            });

        if (!Schema::hasColumn('accr_schools_information', 'number_of_smart_classrooms'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('number_of_smart_classrooms')->nullable();
            });
        if (!Schema::hasColumn('accr_schools_information', 'number_of_laptops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('number_of_laptops')->nullable();
            });
        if (!Schema::hasColumn('accr_schools_information', 'number_of_positivo_laptops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('number_of_positivo_laptops')->nullable();
            });
        //
        if (!Schema::hasColumn('accr_schools_information', 'has_three_phase_electricity'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('has_three_phase_electricity')->nullable();
            });
        if (!Schema::hasColumn('accr_schools_information', 'number_of_incubation_centers'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('number_of_incubation_centers')->nullable();
            });
        if (!Schema::hasColumn('accr_schools_information', 'type_of_internet_connection'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('type_of_internet_connection')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accr_schools_information', 'number_of_computer_labs'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_computer_labs');
            });

        if (Schema::hasColumn('accr_schools_information', 'number_of_smart_classrooms'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_smart_classrooms');
            });
        if (Schema::hasColumn('accr_schools_information', 'number_of_laptops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_laptops');
            });
        if (Schema::hasColumn('accr_schools_information', 'number_of_positivo_laptops'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_positivo_laptops');
            });
        //
        if (Schema::hasColumn('accr_schools_information', 'has_three_phase_electricity'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->string('has_three_phase_electricity')->nullable();
            });
        if (Schema::hasColumn('accr_schools_information', 'number_of_incubation_centers'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('number_of_incubation_centers');
            });
        if (Schema::hasColumn('accr_schools_information', 'type_of_internet_connection'))
            Schema::table('accr_schools_information', function (Blueprint $table) {
                $table->dropColumn('type_of_internet_connection');
            });
    }
}
