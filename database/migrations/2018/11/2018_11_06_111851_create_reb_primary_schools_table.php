<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRebPrimarySchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reb_primary_schools', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('school_name');
            $table->string('school_acronym');
            $table->string('school_code');
            $table->string('school_status');
            $table->string('school_category');
            $table->string('school_sub_category');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('email');
            $table->string('manager');
            $table->string('manager_gender');
            $table->string('province');
            $table->string('district');
            $table->string('sector');
            $table->string('cell');
            $table->string('village');
            $table->string('ownership');
            $table->string('has_school_feeding_program');
            $table->string('options_offered');
            $table->string('is_boarding');
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reb_primary_schools');
    }
}
