<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQualificationLevelToStaffsInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->integer('qualification_level')->nullable(true)->after('qualification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staffs_info', function (Blueprint $table) {
            $table->dropColumn('qualification_level');
        });
    }
}
