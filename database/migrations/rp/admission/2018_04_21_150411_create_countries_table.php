<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('countries'))
            Schema::connection('admission_mysql')->create('countries', function (Blueprint $table) {
                $table->increments('id');
                $table->string("country_name");
                $table->string("flag")->nullable();
                $table->boolean('enabled')->default(false);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('countries'))
            Schema::connection('admission_mysql')->dropIfExists('countries');
    }
}
