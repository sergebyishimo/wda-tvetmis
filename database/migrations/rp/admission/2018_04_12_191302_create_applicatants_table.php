<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicatantsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('applicatants'))
            Schema::connection('admission_mysql')->create('applicatants', function (Blueprint $table) {
                $table->increments('id');
                $table->string('first_name', 100);
                $table->string('other_names', 100);
                $table->string('email', 100);
                $table->integer('phone');
                $table->integer('gender');
                $table->integer('ubudehe');
                $table->date('dob');
                $table->integer('want_student_loan');
                $table->integer('parents_phone')->nullable();
                $table->string('national_id_number', 25)->nullable();
                $table->string('province', 100)->nullable();
                $table->string('district', 100)->nullable();
                $table->string('sector', 100)->nullable();
                $table->string('cell', 100)->nullable();
                $table->string('village', 100)->nullable();
                $table->integer('bank_slip_number')->nullable();
                $table->string('scanned_bank_slip', 255);
                $table->string('photo', 255)->nullable();
                $table->string('scan_of_diploma_or_certificate', 255)->nullable();
                $table->integer('your_bank')->nullable();
                $table->string('your_bank_account')->nullable();
                $table->integer('disability')->nullable();
                $table->string('examiner')->nullable();
                $table->string('index_number')->nullable();
                $table->string('school_attended')->nullable();
                $table->date('graduation_year')->nullable();
                $table->integer('option_offered')->nullable();
                $table->integer('aggregates_obtained')->nullable();
                $table->integer('first_choice_polytechnic')->nullable();
                $table->integer('first_choice_course')->nullable();
                $table->integer('second_choice_polytechnic')->nullable();
                $table->integer('second_choice_course')->nullable();
                $table->integer('third_choice_polytechnic')->nullable();
                $table->integer('third_choice_course')->nullable();
                $table->integer('first_choice_department');
                $table->integer('second_choice_department');
                $table->integer('third_choice_department');
                $table->integer('payment_verified')->default(1);
                $table->integer('admission')->default(1);
                $table->string('senior_one_attachment', 255)->nullable();
                $table->string('senior_one_attachment_name')->nullable();
                $table->string('senior_one_attachment_size')->nullable();
                $table->string('senior_two_attachment', 255)->nullable();
                $table->string('senior_two_attachment_name')->nullable();
                $table->string('senior_two_attachment_size')->nullable();
                $table->string('senior_three_attachment', 255)->nullable();
                $table->string('senior_three_attachment_name')->nullable();
                $table->string('senior_three_attachment_size')->nullable();
                $table->string('senior_four_attachment', 255)->nullable();
                $table->string('senior_four_attachment_name')->nullable();
                $table->string('senior_four_attachment_name_size')->nullable();
                $table->string('senior_five_attachment', 255)->nullable();
                $table->string('senior_five_attachment_name', 100)->nullable();
                $table->string('senior_five_attachment_size', 100)->nullable();
                $table->string('senior_size_attachment', 255)->nullable();
                $table->string('senior_size_attachment_name', 100)->nullable();
                $table->string('senior_size_attachment_size', 100)->nullable();
                $table->string('scan_national_id_passport', 255)->nullable();
                $table->timestamp('application_date')->nullable()->default(\null);
                $table->timestamps();
            });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('applicatants'))
            Schema::connection('admission_mysql')->drop('applicatants');
    }

}
