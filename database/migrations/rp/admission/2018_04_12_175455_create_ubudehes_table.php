<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbudehesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('ubudehes'))
            Schema::connection('admission_mysql')->create('ubudehes', function (Blueprint $table) {
                $table->increments('id');
                $table->string("ubudehe", 40)->unique();
                $table->string("description", 255)->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('ubudehes'))
            Schema::connection('admission_mysql')->dropIfExists('ubudehes');
    }
}
