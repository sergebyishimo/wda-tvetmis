<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('disabilities'))
            Schema::connection('admission_mysql')->create('disabilities', function (Blueprint $table) {
                $table->increments('id');
                $table->string("type", 255);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('disabilities'))
            Schema::connection('admission_mysql')->dropIfExists('disabilities');
    }
}
