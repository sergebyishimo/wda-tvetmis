<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnYearOfStudyToApplicatants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('admission_mysql')->table('applicatants', function ($table){
            $table->string('year_of_study')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('admission_mysql')->table('applicatants', function ($table){
            $table->dropColumn('year_of_study');
        });
    }
}
