<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('courses'))
            Schema::connection('admission_mysql')->create('courses', function (Blueprint $table) {
                $table->increments('id');
                $table->string("choices");
                $table->unsignedInteger("polytechnic");
                $table->unsignedInteger("department");
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('courses'))
            Schema::connection('admission_mysql')->dropIfExists('courses');
    }
}
