<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('banks'))
            Schema::connection('admission_mysql')->create('banks', function (Blueprint $table) {
                $table->increments('id');
                $table->string('bank_name');
                $table->string("bank_category")->nullable();
                $table->string("description")->nullable();
                $table->string("contact_number")->nullable();
                $table->string("website")->nullable();
                $table->string("email")->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('banks'))
            Schema::connection('admission_mysql')->dropIfExists('banks');
    }
}
