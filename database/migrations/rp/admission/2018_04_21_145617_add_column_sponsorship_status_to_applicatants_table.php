<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSponsorshipStatusToApplicatantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('admission_mysql')->table('applicatants', function ($table){
            $table->string('sponsorship_status', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('admission_mysql')->table('applicatants', function ($table){
            $table->dropColumn('sponsorship_status');
        });
    }
}
