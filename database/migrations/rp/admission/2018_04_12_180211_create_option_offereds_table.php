<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionOfferedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('option_offereds'))
            Schema::connection('admission_mysql')->create('option_offereds', function (Blueprint $table) {
                $table->increments('id');
                $table->string("option_name");
                $table->string("option_code");
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('option_offereds'))
            Schema::connection('admission_mysql')->dropIfExists('option_offereds');
    }
}
