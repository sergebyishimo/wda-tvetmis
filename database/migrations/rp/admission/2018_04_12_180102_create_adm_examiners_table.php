<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmExaminersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::connection('admission_mysql')->hasTable('examiners'))
            Schema::connection('admission_mysql')->create('examiners', function (Blueprint $table) {
                $table->increments('id');
                $table->string("name");
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('examiners'))
            Schema::connection('admission_mysql')->dropIfExists('examiners');
    }
}
