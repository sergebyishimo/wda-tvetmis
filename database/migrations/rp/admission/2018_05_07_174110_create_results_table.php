<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('results'))
            Schema::connection('admission_mysql')->create('results', function (Blueprint $table) {
                $table->increments('id');
                $table->string('file');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('results'))
            Schema::connection('admission_mysql')->dropIfExists('results');
    }
}
