<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('departments'))
            Schema::connection('admission_mysql')->create('departments', function (Blueprint $table) {
                $table->increments('id');
                $table->string("department");
                $table->unsignedInteger("polytechnic");
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('polytechnics'))
            Schema::connection('admission_mysql')->dropIfExists('departments');
    }
}
