<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGenderDatatypeToVarOnAdmApplicatants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasColumn('applicatants', 'gender'))
            return;
        Schema::connection('admission_mysql')->table('applicatants', function ($table){
            $table->string('gender')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::connection('admission_mysql')->hasColumn('applicatants', 'gender'))
            return;
        Schema::connection('admission_mysql')->table('applicatants', function ($table){
            $table->integer('gender')->change();
        });
    }
}
