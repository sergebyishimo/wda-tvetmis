<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('enables'))
            Schema::connection('admission_mysql')->create('enables', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->string("academic_year");
                $table->date("start");
                $table->date("ends");
                $table->boolean("enabled")->default(false);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('enables'))
            Schema::connection('admission_mysql')->dropIfExists('enables');
    }
}
