<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolytechnicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('admission_mysql')->hasTable('polytechnics'))
            Schema::connection('admission_mysql')->create('polytechnics', function (Blueprint $table) {
                $table->increments('id');
                $table->string("polytechnic");
                $table->string("short_name");
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('admission_mysql')->hasTable('polytechnics'))
            Schema::connection('admission_mysql')->dropIfExists('polytechnics');
    }
}
