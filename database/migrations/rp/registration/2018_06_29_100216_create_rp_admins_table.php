<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRpAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('admins'))
            Schema::connection('rp_mysql')->create('admins', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('college_id')->nullable();
                $table->string('name');
                $table->string('email')->unique();
                $table->string('password');
                $table->char('type', 2)->default('AA');
                $table->rememberToken();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('admins'))
            Schema::connection('rp_mysql')->drop('admins');
    }
}
