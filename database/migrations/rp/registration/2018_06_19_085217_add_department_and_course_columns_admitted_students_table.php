<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartmentAndCourseColumnsAdmittedStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('admitted_students', function (Blueprint $table) {
            $table->integer('department_id')->after('college_id')->nullable();
            $table->integer('course_id')->after('department_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('admitted_students', function (Blueprint $table) {
            $table->dropColumn('department_id');
            $table->dropColumn('course_id');
        });
    }
}
