<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('payment_categories'))
            Schema::connection('rp_mysql')->create('payment_categories', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('college_id')->nullable();
                $table->string('name')->unique();
                $table->boolean('active')->default(1);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('payment_categories'))
            Schema::connection('rp_mysql')->dropIfExists('payment_categories');
    }
}
