<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('transfer_requests'))
            Schema::connection('rp_mysql')->create('transfer_requests', function (Blueprint $table) {
                $table->increments('id');
                $table->string("student_id");
                $table->string("letter");
                $table->integer("college_id");
                $table->integer("department_id");
                $table->integer("course_id");
                $table->boolean('read')->default(false);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('transfer_requests'))
            Schema::connection('rp_mysql')->dropIfExists('transfer_requests');
    }
}
