<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToCollegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colleges', function (Blueprint $table){
            $table->string('logo')->nullable();
            $table->string('province')->nullable();
            $table->string('district')->nullable();
            $table->string('email')->nullable();
            $table->string('telephone')->nullable();
            $table->string('po_box')->nullable();
            $table->string('website')->nullable();
            $table->string('location')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colleges', function (Blueprint $table){
            $table->dropColumn('logo');
            $table->dropColumn('province');
            $table->dropColumn('district');
            $table->dropColumn('email');
            $table->dropColumn('telephone');
            $table->dropColumn('po_box');
            $table->dropColumn('website');
            $table->dropColumn('location');
        });
    }
}
