<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('college_payment_details'))
            Schema::connection('rp_mysql')->create('college_payment_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('college_id');
                $table->integer('category_id');
                $table->string('amount');
                $table->string('academic_year');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('college_payment_details'))
            Schema::connection('rp_mysql')->dropIfExists('college_payment_details');
    }
}
