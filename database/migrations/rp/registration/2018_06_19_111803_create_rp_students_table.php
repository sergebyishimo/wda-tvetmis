<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRpStudentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('students'))
            Schema::connection('rp_mysql')->create('students', function (Blueprint $table) {
                $table->string('id', 50);
                $table->string('name', 191);
                $table->string('email', 191)->unique();
                $table->string('password', 191);
                $table->string('telephone', 191)->nullable()->unique();
                $table->boolean('payments')->default(0);
                $table->string('remember_token', 100)->nullable();
                $table->timestamps();
                $table->primary('id');
            });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('students'))
            Schema::connection('rp_mysql')->drop('students');
    }

}
