<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRpPaymentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('college_payments'))
            Schema::connection('rp_mysql')->create('college_payments', function (Blueprint $table) {
                $table->integer('id', true);
                $table->integer('college_id');
                $table->string('std_id', 20);
                $table->integer('year');
                $table->decimal('paid_amount', 10, 0);
                $table->string('bank_slipno', 100);
                $table->date('op_date');
                $table->string('bank_account', 100);
                $table->string('bank', 100);
                $table->string('operator', 100);
                $table->string('pay_mode', 100);
                $table->integer('pay_status');
                $table->string('trans_id', 100);
                $table->timestamps();
            });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('college_payments'))
            Schema::connection('rp_mysql')->drop('college_payments');
    }

}
