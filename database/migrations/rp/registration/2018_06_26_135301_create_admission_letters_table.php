<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('admission_letters'))
            Schema::connection('rp_mysql')->create('admission_letters', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('college_id');
                $table->longText('letter');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('admission_letters'))
            Schema::connection('rp_mysql')->dropIfExists('admission_letters');
    }
}
