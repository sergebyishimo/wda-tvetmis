<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingColumnsToCollegeStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('college_students', function (Blueprint $table) {
            $table->integer('department_id')->nullable();
            $table->string('first_name', 100);
            $table->string('other_names', 100);
            $table->string('email', 100);
            $table->integer('phone');
            $table->string('gender', 11);
            $table->integer('ubudehe');
            $table->date('dob');
            $table->integer('want_student_loan');
            $table->integer('parents_phone')->nullable();
            $table->string('national_id_number', 25)->nullable();
            $table->string('province', 100)->nullable();
            $table->string('district', 100)->nullable();
            $table->string('sector', 100)->nullable();
            $table->string('cell', 100)->nullable();
            $table->string('village', 100)->nullable();
            $table->integer('bank_slip_number')->nullable();
            $table->string('scanned_bank_slip')->nullable();
            $table->string('photo')->nullable();
            $table->string('scan_of_diploma_or_certificate')->nullable();
            $table->integer('your_bank')->nullable();
            $table->string('your_bank_account', 191)->nullable();
            $table->integer('disability')->nullable();
            $table->string('examiner', 191)->nullable();
            $table->string('index_number', 191)->nullable();
            $table->string('school_attended', 191)->nullable();
            $table->string('graduation_year', 5)->nullable();
            $table->integer('option_offered')->nullable();
            $table->integer('aggregates_obtained')->nullable();
            $table->integer('payment_verified')->default(1);
            $table->string('scan_national_id_passport')->nullable();
            $table->string('year_of_study', 191)->nullable();
            $table->integer('country')->unsigned();
            $table->string('sponsorship_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('college_students', function (Blueprint $table) {
            $table->dropColumn('department_id');
            $table->dropColumn('first_name');
            $table->dropColumn('other_names');
            $table->dropColumn('email');
            $table->dropColumn('phone');
            $table->dropColumn('gender');
            $table->dropColumn('ubudehe');
            $table->dropColumn('dob');
            $table->dropColumn('want_student_loan');
            $table->dropColumn('parents_phone');
            $table->dropColumn('national_id_number');
            $table->dropColumn('province');
            $table->dropColumn('district');
            $table->dropColumn('sector');
            $table->dropColumn('cell');
            $table->dropColumn('village');
            $table->dropColumn('bank_slip_number');
            $table->dropColumn('scanned_bank_slip');
            $table->dropColumn('photo');
            $table->dropColumn('scan_of_diploma_or_certificate');
            $table->dropColumn('your_bank');
            $table->dropColumn('your_bank_account');
            $table->dropColumn('disability');
            $table->dropColumn('examiner');
            $table->dropColumn('index_number');
            $table->dropColumn('school_attended');
            $table->dropColumn('graduation_year');
            $table->dropColumn('option_offered');
            $table->dropColumn('aggregates_obtained');
            $table->dropColumn('payment_verified');
            $table->dropColumn('scan_national_id_passport');
            $table->dropColumn('year_of_study');
            $table->dropColumn('country');
            $table->dropColumn('sponsorship_status');
        });
    }
}
