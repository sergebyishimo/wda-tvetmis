<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionColumnsOnCollegeStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('college_students', function (Blueprint $table) {
            $table->string('father_name')->nullable()->after('parents_phone');
            $table->string('father_nation_id')->nullable()->after('father_name');
            $table->string('father_phone')->nullable()->after('father_nation_id');
            //
            $table->string('mother_name')->nullable()->after('father_phone');
            $table->string('mother_nation_id')->nullable()->after('mother_name');
            $table->string('mother_phone')->nullable()->after('mother_nation_id');
            //
            $table->string('guardian_name')->nullable()->after('mother_phone');
            $table->string('guardian_nation_id')->nullable()->after('guardian_name');
            $table->string('guardian_phone')->nullable()->after('guardian_nation_id');
            //
            $table->string('marital_status')->nullable()->after('dob');
            $table->string('student_category')->nullable()->after('course_id');
            $table->string('has_chronic_disease')->nullable()->after('marital_status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('college_students', function (Blueprint $table) {
            $table->dropColumn('father_name');
            $table->dropColumn('father_nation_id');
            $table->dropColumn('father_phone');
            //
            $table->dropColumn('mother_name');
            $table->dropColumn('mother_nation_id');
            $table->dropColumn('mother_phone');
            //
            $table->dropColumn('guardian_name');
            $table->dropColumn('guardian_nation_id');
            $table->dropColumn('guardian_phone');
            //
            $table->dropColumn('marital_status');
            $table->dropColumn('student_category');
            $table->dropColumn('has_chronic_disease');
        });
    }
}
