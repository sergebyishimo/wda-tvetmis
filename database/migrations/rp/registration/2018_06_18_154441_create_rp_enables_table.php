<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRpEnablesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('enables'))
            Schema::connection('rp_mysql')->create('enables', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title', 191)->nullable();
                $table->string('academic_year', 191);
                $table->date('start');
                $table->date('ends');
                $table->boolean('enabled')->default(0);
                $table->timestamps();
            });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('enables'))
            Schema::connection('rp_mysql')->drop('enables');
    }

}
