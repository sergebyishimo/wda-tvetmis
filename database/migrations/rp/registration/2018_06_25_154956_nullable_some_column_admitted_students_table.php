<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableSomeColumnAdmittedStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rp_mysql')->table('admitted_students', function (Blueprint $table) {
            $table->string('first_name', 100)->nullable()->change();
            $table->string('other_names', 100)->nullable()->change();
            $table->string('email', 100)->nullable()->change();
            $table->integer('phone')->nullable()->change();
            $table->string('gender', 11)->nullable()->change();
            $table->integer('ubudehe')->nullable()->change();
            $table->date('dob')->nullable()->change();
            $table->integer('want_student_loan')->nullable()->change();
            $table->string('sponsorship_status')->nullable()->change();
            $table->unsignedInteger('country')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('rp_mysql')->table('admitted_students', function (Blueprint $table) {
            $table->string('first_name', 100)->change();
            $table->string('other_names', 100)->change();
            $table->string('email', 100)->change();
            $table->integer('phone')->change();
            $table->string('gender', 11)->change();
            $table->integer('ubudehe')->change();
            $table->date('dob')->change();
            $table->integer('want_student_loan')->change();
            $table->string('sponsorship_status')->change();
            $table->unsignedInteger('country')->change();
        });
    }
}
