<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('college_students'))
            Schema::connection('rp_mysql')->create('college_students', function (Blueprint $table) {
                $table->increments('id');
                $table->string('student_reg')->unique();
                $table->string('college_id');
                $table->string("course_id")->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('college_students'))
            Schema::connection('rp_mysql')->dropIfExists('college_students');
    }
}
