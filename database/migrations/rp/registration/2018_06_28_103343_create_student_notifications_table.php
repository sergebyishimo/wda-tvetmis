<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('rp_mysql')->hasTable('student_notifications'))
            Schema::connection('rp_mysql')->create('student_notifications', function (Blueprint $table) {
                $table->increments('id');
                $table->string("student_id");
                $table->longText("message");
                $table->boolean("read")->default(false);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('rp_mysql')->hasTable('student_notifications'))
            Schema::connection('rp_mysql')->dropIfExists('student_notifications');
    }
}
