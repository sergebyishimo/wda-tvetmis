<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelAccrAuditSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accr_mysql')->create('audit_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->string('quality_id');
            $table->string('indicator_id');
            $table->string('summary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accr_mysql')->dropIfExists('model_accr_audit_summaries');
    }
}
